

import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter


#data from https://www.nodc.noaa.gov/cgi-bin/OC5/gin-seas-climate/ginregcl.pl
annual_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
winter_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
spring_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
summer_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
autumn_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]

annual_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]
winter_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
spring_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s14_04.nc')[5][0]
summer_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
autumn_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s16_04.nc')[5][0]


annual_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_01.nc')[0][0]
winter_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_01.nc')[0][0]
spring_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_01.nc')[0][0]
summer_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_01.nc')[0][0]
autumn_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_01.nc')[0][0]

annual_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_01.nc')[5][0]
winter_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_01.nc')[5][0]
spring_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s14_01.nc')[5][0]
summer_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_01.nc')[5][0]
autumn_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s16_01.nc')[5][0]

# summer_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
# winter_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
# spring_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s14_04.nc')[5][0]
# autumn_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s16_04.nc')[5][0]
# ann_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]

# density_cube.standard_name = 'sea_water_density'
# density_cube.units = 'kg m-3'
#
# density_cube.data = seawater.dens(ann_sal_cube.data,cube.data,1)

try:
    cube.coord('longitude').guess_bounds()
    cube.coord('latitude').guess_bounds()
except:
    print 'cube already has bounds'

try:
    ann_sal_cube.coord('longitude').guess_bounds()
    ann_sal_cube.coord('latitude').guess_bounds()
except:
    print 'cube already has bounds'



lon_west1 = -23.75
lon_east1 = -21.75
lat_south = 66.0
lat_north = 70.0

lon_west2 = -19.75
lon_east2 = -17.75

lon_west3 = -15.75
lon_east3 = -13.75


###################
###################

def extract_meaned_sections(cube,ann_sal_cube,lon_west1,lon_east1,lat_south,lat_north):

    cube_region_tmp = cube.intersection(longitude=(lon_west1, lon_east1))
    ann_cube_t_1 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

    cube_region_tmp = ann_sal_cube.intersection(longitude=(lon_west1, lon_east1))
    ann_cube_s_1 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

    ann_cube_p_1 = ann_cube_t_1.copy()
    ann_cube_p_1.data = seawater.dens(ann_cube_s_1.data,ann_cube_t_1.data,1)


    grid_areas = iris.analysis.cartography.area_weights(ann_cube_t_1)
    ann_cube_t_1_mean = ann_cube_t_1.collapsed(['longitude'], iris.analysis.MEAN, weights=grid_areas)

    grid_areas = iris.analysis.cartography.area_weights(ann_cube_s_1)
    ann_cube_s_1_mean = ann_cube_s_1.collapsed(['longitude'], iris.analysis.MEAN, weights=grid_areas)

    grid_areas = iris.analysis.cartography.area_weights(ann_cube_p_1)
    ann_cube_p_1_mean = ann_cube_p_1.collapsed(['longitude'], iris.analysis.MEAN, weights=grid_areas)

    return ann_cube_t_1_mean,ann_cube_s_1_mean,ann_cube_p_1_mean

def calc_d18O(T,S):
        #S = ABSOLUTE SALINITY psu
        #T = ABSOLUTE T deg' C
        # d18Osw_synth = ((0.61*S)-21.3)
#       d18Osw_synth = ((3.0*S)-105)
        #R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
        #Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
        d18Osw_synth = ((0.55*S)-18.98)
        #LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
        # d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
        # d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
        #Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
        d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
        #From Reynolds - the -27 is to convert between SMOW and vPDB
        return d18Oc_synth

#
# def downsample_cube(t_cube):
#         ds= np.linspace(np.min(t_cube.coord('depth').points), np.max(t_cube.coord('depth').points), np.shape(t_cube)[0])
#         las = np.linspace(np.min(t_cube.coord('latitude').points), np.max(t_cube.coord('latitude').points), np.shape(t_cube)[1])
#         depth = iris.coords.DimCoord(ds, standard_name='depth', units='m',attributes = t_cube.coord('depth').attributes)
#         latitude = iris.coords.DimCoord(las, standard_name='latitude', units='degrees')
#         tmp_cube1 = iris.cube.Cube(np.zeros((len(ds),len(las)), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(depth,0),  (latitude, 1)])
#         tmp_cube1.data =t_cube.data
#
#         depths= 20
#         latis = 20
#         ds= np.linspace(np.min(t_cube.coord('depth').points), np.max(t_cube.coord('depth').points), depths)
#         las = np.linspace(np.min(t_cube.coord('latitude').points), np.max(t_cube.coord('latitude').points), latis)
#         depth = iris.coords.DimCoord(ds, standard_name='depth', units='m',attributes = t_cube.coord('depth').attributes)
#         latitude = iris.coords.DimCoord(las, standard_name='latitude', units='degrees')
#         tmp_cube2 = iris.cube.Cube(np.zeros((len(ds),len(las)), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(depth,0),  (latitude, 1)])
#
#         points=[]
#         values = []
#         for i0,i in enumerate(t_cube.coord('depth').points):
#             for j0,j in enumerate(t_cube.coord('latitude').points):
#                 points.append((i,j))
#                 values.append(t_cube[i0,j0].data)
#
#
#         xi=[]
#         for i0,i in enumerate(ds):
#             for j0,j in enumerate(las):
#                 xi.append((i,j))
#
#         from scipy.interpolate import griddata
#         tmp_cube2.data =  np.reshape(griddata(points,values,xi),(len(ds),len(las)))
#         return tmp_cube2



cubes_t = [winter_cube_t,spring_cube_t,summer_cube_t,autumn_cube_t]
cubes_s = [winter_cube_t,spring_cube_t,summer_cube_t,autumn_cube_t]
cubes_t_1deg = [winter_cube_t_1deg,spring_cube_t_1deg,summer_cube_t_1deg,autumn_cube_t_1deg]
cubes_s_1deg = [winter_cube_t_1deg,spring_cube_t_1deg,summer_cube_t_1deg,autumn_cube_t_1deg]




###################
###################


#####
# temp/density figure
#####

my_cmap = 'bwr'


minv=-3.0
maxv=3.0
minv1=-3.0
maxv1=3.0


"""
plt.close('all')
plt.figure(figsize = (9,15))

gs1 = gridspec.GridSpec(4, 3)

for i,dummey in enumerate(cubes_t):

    ann_cube_t_1_mean,ann_cube_s_1_mean,ann_cube_p_1_mean = extract_meaned_sections(cubes_t[i],cubes_s[i],lon_west1,lon_east1,lat_south,lat_north)
    ann_cube_t_2_mean,ann_cube_s_2_mean,ann_cube_p_2_mean = extract_meaned_sections(cubes_t[i],cubes_s[i],lon_west2,lon_east2,lat_south,lat_north)
    ann_cube_t_3_mean,ann_cube_s_3_mean,ann_cube_p_3_mean = extract_meaned_sections(cubes_t[i],cubes_s[i],lon_west3,lon_east3,lat_south,lat_north)

    # ann_cube_t_1_mean_1deg,ann_cube_s_1_mean_1deg,ann_cube_p_1_mean_1deg = extract_meaned_sections(cubes_t_1deg[i],cubes_s_1deg[i],lon_west1,lon_east1,lat_south,lat_north)
    # ann_cube_t_2_mean_1deg,ann_cube_s_2_mean_1deg,ann_cube_p_2_mean_1deg = extract_meaned_sections(cubes_t_1deg[i],cubes_s_1deg[i],lon_west2,lon_east2,lat_south,lat_north)
    # ann_cube_t_3_mean_1deg,ann_cube_s_3_mean_1deg,ann_cube_p_3_mean_1deg = extract_meaned_sections(cubes_t_1deg[i],cubes_s_1deg[i],lon_west3,lon_east3,lat_south,lat_north)

    ax1 = plt.subplot(gs1[i,0])
    iplt.pcolormesh(ann_cube_t_1_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
    iplt.contour(ann_cube_p_1_mean,20,colors='k',linewidths=0.5)
    # iplt.contour(downsample_cube(ann_cube_p_1_mean),20,colors='k',linewidths=0.5)
    plt.title('West')
    plt.ylim([1500,0])

    ax2 = plt.subplot(gs1[i,1])
    iplt.pcolormesh(ann_cube_t_2_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
    iplt.contour(ann_cube_p_2_mean,20,colors='k',linewidths=0.5)
    # iplt.contour(downsample_cube(ann_cube_p_2_mean),20,colors='k',linewidths=0.5)
    plt.title('Middle')
    plt.ylim([1500,0])

    ax2 = plt.subplot(gs1[i,2])
    iplt.pcolormesh(ann_cube_t_3_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
    iplt.contour(ann_cube_p_3_mean,20,colors='k',linewidths=0.5)
    # iplt.contour(downsample_cube(ann_cube_p_3_mean),20,colors='k',linewidths=0.5)
    plt.title('East')
    plt.ylim([1500,0])





plt.tight_layout()
plt.show(block=True)

"""

plt.close('all')
plt.figure(figsize = (15,8))

# gs1 = gridspec.GridSpec(1, 10)

i=0

ann_cube_t_1_mean,ann_cube_s_1_mean,ann_cube_p_1_mean = extract_meaned_sections(annual_cube_t,annual_cube_s,lon_west1,lon_east1,lat_south,lat_north)
ann_cube_t_2_mean,ann_cube_s_2_mean,ann_cube_p_2_mean = extract_meaned_sections(annual_cube_t,annual_cube_s,lon_west2,lon_east2,lat_south,lat_north)
ann_cube_t_3_mean,ann_cube_s_3_mean,ann_cube_p_3_mean = extract_meaned_sections(annual_cube_t,annual_cube_s,lon_west3,lon_east3,lat_south,lat_north)


p_min = 1027.2
p_max = 1028.2
p_levs = 21
c_levels = np.linspace(p_min,p_max,p_levs)

ax1 = plt.subplot2grid((2,14),(i,0),colspan=4)
iplt.pcolormesh(ann_cube_t_1_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
# d18O_cube = ann_cube_t_1_mean.copy()
# d18O_cube.data = calc_d18O(ann_cube_t_1_mean.data+273.15,ann_cube_s_1_mean.data)
# iplt.pcolormesh(d18O_cube,cmap = 'viridis',vmin = d18Omin,vmax=d18Omax)
CS2 = iplt.contour(ann_cube_p_1_mean,np.linspace(p_min,p_max,p_levs),colors='k',linewidths=0.5)
plt.clabel(CS2,[c_levels[14],c_levels[15],c_levels[16]],inline=1,fmt='%1.2f')
plt.title('West Iceland (annual mean)')
plt.ylim([1500,0])
plt.ylabel('depth (m)')
plt.xlabel('latitude ($^\circ$N)')

ax2 = plt.subplot2grid((2,14),(i,1*4),colspan=4)
iplt.pcolormesh(ann_cube_t_2_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
# d18O_cube.data = calc_d18O(ann_cube_t_2_mean.data+273.15,ann_cube_s_2_mean.data)
# iplt.pcolormesh(d18O_cube,cmap = 'viridis',vmin = d18Omin,vmax=d18Omax)
CS3 = iplt.contour(ann_cube_p_2_mean,np.linspace(p_min,p_max,p_levs),colors='k',linewidths=0.5)
plt.clabel(CS3,[c_levels[14],c_levels[15],c_levels[16]],inline=1,fmt='%1.2f')
plt.title('Central Iceland (annual mean)')
plt.ylim([1500,0])
plt.ylabel('depth (m)')
plt.xlabel('latitude ($^\circ$N)')

ax3 = plt.subplot2grid((2,14),(i,2*4),colspan=4)
cs = iplt.pcolormesh(ann_cube_t_3_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
# d18O_cube.data = calc_d18O(ann_cube_t_3_mean.data+273.15,ann_cube_s_3_mean.data)
# cs = iplt.pcolormesh(d18O_cube,cmap = 'viridis',vmin = d18Omin,vmax=d18Omax)
CS4 = iplt.contour(ann_cube_p_3_mean,np.linspace(p_min,p_max,p_levs),colors='k',linewidths=0.5)
plt.clabel(CS4,[c_levels[14],c_levels[15],c_levels[16]],inline=1,fmt='%1.2f')
plt.title('East Iceland (annual mean)')
plt.ylim([1500,0])
plt.ylabel('depth (m)')
plt.xlabel('latitude ($^\circ$N)')

ax4 = plt.subplot2grid((2,14),(i,3*4))
cb1 = plt.colorbar(cs, cax=ax4,extend='both')
cb1.set_label('temperature ($^\circ$C)')


ax1.set_facecolor((0.85, 0.85, 0.85))
ax2.set_facecolor((0.85, 0.85, 0.85))
ax3.set_facecolor((0.85, 0.85, 0.85))

#
# d18Omin=4.2
# d18Omax=4.8
#
# i=1
#
# ax5 = plt.subplot2grid((1,14),(i,0),colspan=4)
# # iplt.pcolormesh(ann_cube_t_1_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
# d18O_cube = ann_cube_t_1_mean.copy()
# d18O_cube.data = calc_d18O(ann_cube_t_1_mean.data+273.15,ann_cube_s_1_mean.data)
# iplt.pcolormesh(d18O_cube,cmap = 'viridis',vmin = d18Omin,vmax=d18Omax)
# iplt.contour(ann_cube_p_1_mean,20,colors='k',linewidths=0.5)
# plt.title('West Iceland')
# plt.ylim([1500,0])
# plt.ylabel('depth (m)')
# plt.xlabel('latitude ($^\circ$N)')
#
# ax6 = plt.subplot2grid((1,14),(i,1*4),colspan=4)
# # iplt.pcolormesh(ann_cube_t_2_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
# d18O_cube.data = calc_d18O(ann_cube_t_2_mean.data+273.15,ann_cube_s_2_mean.data)
# iplt.pcolormesh(d18O_cube,cmap = 'viridis',vmin = d18Omin,vmax=d18Omax)
# iplt.contour(ann_cube_p_2_mean,20,colors='k',linewidths=0.5)
# plt.title('Central Iceland')
# plt.ylim([1500,0])
# plt.ylabel('depth (m)')
# plt.xlabel('latitude ($^\circ$N)')
#
# ax7 = plt.subplot2grid((1,14),(i,2*4),colspan=4)
# # cs = iplt.pcolormesh(ann_cube_t_3_mean,cmap = my_cmap,vmin = minv1,vmax=maxv1)
# d18O_cube.data = calc_d18O(ann_cube_t_3_mean.data+273.15,ann_cube_s_3_mean.data)
# cs2 = iplt.pcolormesh(d18O_cube,cmap = 'viridis',vmin = d18Omin,vmax=d18Omax)
# iplt.contour(ann_cube_p_3_mean,20,colors='k',linewidths=0.5)
# plt.title('East Iceland')
# plt.ylim([1500,0])
# plt.ylabel('depth (m)')
# plt.xlabel('latitude ($^\circ$N)')
#
# ax8 = plt.subplot2grid((1,14),(i,3*4))
# cb1 = plt.colorbar(cs2, cax=ax8,extend='both')
# cb1.set_label('temperature ($^\circ$C)')
#
#
# ax5.set_facecolor((0.85, 0.85, 0.85))
# ax6.set_facecolor((0.85, 0.85, 0.85))
# ax7.set_facecolor((0.85, 0.85, 0.85))


#################
# iceland map ###
#################

lat = 66.0+(31.59/60.0)
lon = -1.0 * (18.0+(11.74/60.0))

# plt.close('all')
# fig = plt.figure()
# ax = fig.add_subplot(1, 1, 1)
ax5 = plt.subplot2grid((2,14),(1,4),colspan=4, projection=ccrs.PlateCarree())

ax5.set_extent([-24, -12, 63, 73], crs=ccrs.PlateCarree())

iplt.pcolormesh(winter_cube_t[0],cmap = my_cmap,vmin = minv1,vmax=maxv1)

coasts_10m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',
                                        edgecolor='k',facecolor='none')

# coasts_high = cfeature.GSHHSFeature(scale='full', levels=None)

land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                        edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])

ax5.add_feature(land_10m)
ax5.add_feature(coasts_10m)
plt.title('winter sea surface temperature')


lon_west1 = -23.75
lon_east1 = -21.75
lat_south = 66.0
lat_north = 70.0

x = [lon_west1,lon_east1,lon_east1,lon_west1,lon_west1]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
ax5.plot(x, y,'k')
ax5.fill(x, y, color='k', alpha=0.1)


lon_west2 = -19.75
lon_east2 = -17.75

x = [lon_west2,lon_east2,lon_east2,lon_west2,lon_west2]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
ax5.plot(x, y,'k')
ax5.fill(x, y, color='k', alpha=0.1)

lon_west3 = -15.75
lon_east3 = -13.75

x = [lon_west3,lon_east3,lon_east3,lon_west3,lon_west3]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
ax5.plot(x, y,'k')
ax5.fill(x, y, color='k', alpha=0.1)


plt.scatter(lon,lat,color='g',edgecolor='k',marker='*',s=150)

ax5.set_xticks(range(-28,-10,5), crs=ccrs.PlateCarree())
ax5.set_yticks(range(63,73,3), crs=ccrs.PlateCarree())
lon_formatter = LongitudeFormatter(zero_direction_label=True)
lat_formatter = LatitudeFormatter()
ax5.xaxis.set_major_formatter(lon_formatter)
ax5.yaxis.set_major_formatter(lat_formatter)

plt.tight_layout()

plt.savefig('/home/ph290/Documents/figures/GIN_seas_off_iceland.png')
plt.show(block=False)

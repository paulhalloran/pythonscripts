import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import iris.plot as iplt
import cartopy.crs as ccrs
import numpy as np

tmm_alk = iris.load_cube('ALK.nc')
glodap_alk = iris.load_cube('GLODAPv2_Mapped_Climatologies/GLODAPv2.talk.nc','seawater alkalinity expressed as mole equivalent per unit mass')
woa_cube =  iris.load_cube('temperature_annual_1deg.nc','sea_water_temperature')

plt.close('all')

plt.figure(figsize=(12, 5))
plt.subplot(121)

proj = ccrs.PlateCarree(central_longitude=0.0)
plt.subplot(121, projection=proj)
qplt.contourf(tmm_alk[0]*1.0e3-150, np.linspace(2100,2450,11))
plt.title('Model (abiotic) - 150.0')
plt.gca().coastlines()

proj = ccrs.PlateCarree(central_longitude=0.0)
plt.subplot(122, projection=proj)
qplt.contourf(glodap_alk[0], np.linspace(2100,2450,11))
plt.gca().coastlines()
plt.title('GLODAP v2')
plt.savefig('/Users/ph290/Downloads/surface_alkalinity.png')
iplt.show(block = False)
# 
# 
# lon_west = -100.0
# lon_east = 20
# lat_south = 10.0
# lat_north = 70.0 
# 
# lon_west = -180
# lon_east = 180
# lat_south = -90
# lat_north = 90 
# 
# 
# cube_region_tmp = tmm_alk.intersection(longitude=(lon_west, lon_east))
# tmm_alk_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
# 
# try:
#     tmm_alk_region.coord('latitude').guess_bounds()
# except:
#     print 'cube already has latitude bounds'
# 
# 
# try:
#     tmm_alk_region.coord('longitude').guess_bounds()
# except:
#     print 'cube already has longitude bounds' 
# 
# grid_areas = iris.analysis.cartography.area_weights(tmm_alk_region)
# tmm_alk_area_avged_cube = tmm_alk_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
# 
# cube_region_tmp = glodap_alk.intersection(longitude=(lon_west, lon_east))
# glodap_alk_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
# 
# data = glodap_alk_region.data.data.copy()
# locations = np.isnan(data)
# locations2 = np.where(data == -999)
# data[locations] = glodap_alk_region.data.fill_value
# data[locations2] = glodap_alk_region.data.fill_value
# data = np.ma.masked_array(data)
# data.mask = glodap_alk_region.data.mask
# data = np.ma.masked_where(data == glodap_alk_region.data.fill_value,data)
# glodap_alk_region.data = data
# # qplt.contourf(glodap_alk_region[0],20)
# # plt.show()
# 
# 
# 
# try:
#     glodap_alk_region.coord('latitude').guess_bounds()
# except:
#     print 'cube already has latitude bounds'
# 
# 
# try:
#     glodap_alk_region.coord('longitude').guess_bounds()
# except:
#     print 'cube already has longitude bounds' 
#     
# 
# grid_areas = iris.analysis.cartography.area_weights(glodap_alk_region)
# glodap_alk_area_avged_cube = glodap_alk_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
# 
# 
# plt.close('all')
# qplt.plot(tmm_alk_area_avged_cube,'b')
# plt.plot(woa_cube.coord('depth').points,glodap_alk_area_avged_cube.data/1.0e3,'k')
# plt.savefig('/Users/ph290/Downloads/global_alkalinity_profile.png')
# 
# # plt.show(block = False)


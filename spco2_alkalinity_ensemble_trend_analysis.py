import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
from cdo import *
cdo = Cdo()
import statsmodels.api as sm
import matplotlib.pyplot as plt
from statsmodels.stats.outliers_influence import summary_table

print 'DATA PRE_PROCESSED WITH: regridding_cmip5_b.py'

#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


def model_names2(directory,details):
	files = glob.glob(directory+'/*'+details+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


def ensmeble_names(directory):
	files = glob.glob(directory+'/'+model+'_*.nc')
	ensmeble_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			print file
			ensmeble_tmp.append(file.split('/')[-1].split('_')[6])
			ensmeble = np.unique(ensmeble_tmp)
	return ensmeble


'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
input_directory = '/data/NAS-ph290/shared/alkalinity/processed/'
output_directory = '/data/NAS-ph290/shared/alkalinity/processed/subselected_yrs/'
output_directory2 = '/data/NAS-ph290/shared/alkalinity/processed/subselected_yrs/ensemble_means/'
output_directory1 = '/data/NAS-ph290/shared/spco2/processed/subselected_yrs/ensemble_means/'

var = 'talk'

models = model_names(input_directory)


'''

for model in models:
	ensmebles = ensmeble_names(input_directory)
	for ensmeble in ensmebles:
		infile = input_directory+model+'_'+var+'_hist_plus_rcp_85_'+ensmeble+'_regridded.nc'
		outfile = output_directory+model+'_'+var+'_hist_plus_rcp_85_'+ensmeble+'_regridded_1990_to_2011.nc'
		cdo.selyear('1991/2011', input = infile, output = outfile)


for model in models:
	print 'calculating ensmeble mean for: '+model
	infile = output_directory+model+'_'+var+'_hist_plus_rcp_85_*_regridded_1990_to_2011.nc'
	outfile = output_directory2+model+'_'+var+'_hist_plus_rcp_85_regridded_1990_to_2011_ens_mean.nc'
	try:
		cdo.ensmean(input = infile, output = outfile)
	except:
		print model+' failed in ensmeble mean'
		pass

for model in models:
	ensmebles = ensmeble_names(input_directory)
	for ensmeble in ensmebles:
		infile = input_directory+model+'_'+var+'_hist_plus_rcp_85_'+ensmeble+'_regridded.nc'
		outfile = output_directory+model+'_'+var+'_hist_plus_rcp_85_'+ensmeble+'_regridded_1995_to_2015.nc'
		cdo.selyear('1995/2015', input = infile, output = outfile)


for model in models:
	print 'calculating ensmeble mean for: '+model
	infile = output_directory+model+'_'+var+'_hist_plus_rcp_85_*_regridded_1995_to_2015.nc'
	outfile = output_directory2+model+'_'+var+'_hist_plus_rcp_85_regridded_1995_to_2015_ens_mean.nc'
	try:
		cdo.ensmean(input = infile, output = outfile)
	except:
		print model+' failed in ensmeble mean'
		pass


'''

gradients_co2 = []
preceeding_mean_co2 = []

gradients_alk = []
preceeding_mean_alk = []


models2a = model_names2(output_directory2,'1990_to_2011')
models2b = model_names2(output_directory2,'1995_to_2015')
models2 = np.intersect1d(models2a,models2b)

models3a = model_names2(output_directory1,'1990_to_2011')
models3b = model_names2(output_directory1,'1995_to_2015')
models3 = np.intersect1d(models3a,models3b)

models2 = np.intersect1d(models2,models3)


var = 'spco2'
for model in models2:
	if not((model == 'BNU-ESM')):
		try:
			print model
			cube = iris.load_cube(output_directory1+model+'_'+var+'_hist_plus_rcp_85_regridded_1990_to_2011_ens_mean.nc')
			try:
				cube.coord('latitude').guess_bounds()
			except:
				print 'cube already has latitude bounds' 
			try:
				cube.coord('longitude').guess_bounds()
			except:
				print 'cube already has longitude bounds'
			grid_areas = iris.analysis.cartography.area_weights(cube)
			cube_mean = cube.collapsed(['latitude','longitude'],iris.analysis.MEAN, weights=grid_areas)
			coord = cube.coord('time')
			dt = coord.units.num2date(coord.points)
			year = np.array([coord.units.num2date(value).year for value in coord.points])
			Y = cube_mean.data.data
			X = year
			X2 = sm.add_constant(X)
			s_model = sm.OLS(Y,X2)
			results = s_model.fit()
			gradients_co2.append(results.params[1])
			####
			cube = iris.load_cube(output_directory2+model+'_'+var+'_hist_plus_rcp_85_regridded_1995_to_2015_ens_mean.nc')
			try:
				cube.coord('latitude').guess_bounds()
			except:
				print 'cube already has latitude bounds' 
			try:
				cube.coord('longitude').guess_bounds()
			except:
				print 'cube already has longitude bounds'
			grid_areas = iris.analysis.cartography.area_weights(cube)
			cube_mean = cube.collapsed(['latitude','longitude','time'],iris.analysis.MEAN, weights=grid_areas)
			preceeding_mean_co2.append(float(cube_mean.data))
		except:
			pass



var = 'talk'
for model in models2:
        if not((model == 'BNU-ESM')):
                try:
                        print model
                        cube = iris.load_cube(output_directory2+model+'_'+var+'_hist_plus_rcp_85_regridded_1990_to_2011_ens_mean.nc')
                        try:
                                cube.coord('latitude').guess_bounds()
                        except:
                                print 'cube already has latitude bounds'
                        try:
                                cube.coord('longitude').guess_bounds()
                        except:
                                print 'cube already has longitude bounds'
                        grid_areas = iris.analysis.cartography.area_weights(cube)
                        cube_mean = cube.collapsed(['latitude','longitude'],iris.analysis.MEAN, weights=grid_areas)
                        coord = cube.coord('time')
                        dt = coord.units.num2date(coord.points)
                        year = np.array([coord.units.num2date(value).year for value in coord.points])
                        Y = cube_mean.data.data
                        X = year
                        X2 = sm.add_constant(X)
                        s_model = sm.OLS(Y,X2)
                        results = s_model.fit()  
                        gradients_alk.append(results.params[1])
                        ####
                        cube = iris.load_cube(output_directory2+model+'_'+var+'_hist_plus_rcp_85_regridded_1995_to_2015_ens_mean.nc')
                        try:
                                cube.coord('latitude').guess_bounds()
                        except:
                                print 'cube already has latitude bounds'
                        try:
                                cube.coord('longitude').guess_bounds()
                        except:
                                print 'cube already has longitude bounds'
                        grid_areas = iris.analysis.cartography.area_weights(cube)
                        cube_mean = cube.collapsed(['latitude','longitude','time'],iris.analysis.MEAN, weights=grid_areas)
                        preceeding_mean_alk.append(float(cube_mean.data))
                except:
                        pass






'''		
takahashi_co2 = iris.load('/data/NAS-ph290/ph290/misc_data/TALK_TCO2_pCO2_GLOB_Grid_Dat.nc')[1]
cube_region_tmp = takahashi_co2.intersection(longitude=(-75, 5))
takahashi_co2_region = cube_region_tmp.intersection(latitude=(10, 70))
try:
	takahashi_co2_region.coord('latitude').guess_bounds()
except:
	print 'cube already has latitude bounds'


try:
	takahashi_co2_region.coord('longitude').guess_bounds()
except:
	print 'cube already has longitude bounds'


grid_areas = iris.analysis.cartography.area_weights(takahashi_co2_region)
takahashi_co2_region = takahashi_co2_region.collapsed(['latitude','longitude','time'],iris.analysis.MEAN, weights=grid_areas)

'''

#plt.scatter(preceeding_mean,gradients)
x = np.array(preceeding_mean_alk)
#x /= 0.101325
y = np.array(gradients_co2)
xsort = np.argsort(x)
x = x[xsort]
y = y[xsort]
xb = sm.add_constant(x)
s_model = sm.OLS(y,xb)
results = s_model.fit()
x2 = np.linspace(np.min(x),np.max(x),np.size(x))
y2 = results.predict(sm.add_constant(x2))
st, data, ss2 = summary_table(results, alpha=0.001)
fittedvalues = data[:,2]
predict_mean_se  = data[:,3]
predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
predict_ci_low, predict_ci_upp = data[:,6:8].T
plt.scatter(x,y)
plt.plot(x, fittedvalues, 'k', lw=2)
#plt.plot(x, predict_ci_low, 'r--', lw=2)
#plt.plot(x, predict_ci_upp, 'r--', lw=2)
plt.plot(x, predict_mean_ci_low, 'r--', lw=2)
plt.plot(x, predict_mean_ci_upp, 'r--', lw=2)

#tmp_val = float(takahashi_co2_region.data)
#plt.plot([tmp_val,tmp_val],[np.min(y),np.max(y)],'b',lw=2)

plt.xlabel('mean talk (2005 to 2015 - so comparable with Takahashis 2005 value)')
plt.ylabel('gradient')
plt.show(block = False)

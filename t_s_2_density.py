
import numpy as np
import iris
import glob
import subprocess
import uuid
import os

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
	models = np.unique(models_tmp)
	return models


temporary_file_space = '/data/dataSSD0/ph290/tmp2/'
directory = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'

models=model_names(directory)

for model in models:
	try:
		os.remove(temporary_file_space+'delete.nc')
	except:
		print 'no file to delete'
	subprocess.call(['cdo -P 6  merge -subc,273.15 '+directory+'thetao_mean_'+model+'_past1000_r1i1p1_JJA.nc '+directory+'so_mean_'+model+'_past1000_r1i1p1_JJA.nc '+temporary_file_space+'delete.nc'], shell=True)
	subprocess.call(['cdo -P 6 rhopot -adisit '+temporary_file_space+'delete.nc '+directory+'density_mean_'+model+'_past1000_r1i1p1_JJA.nc'], shell=True)



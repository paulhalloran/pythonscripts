import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
import os
import glob

directory = '/data/dataSSD0/ph290/cmip5/regridded/'

def model_names(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


vars = ['fgco2','nep']
colours = ['r','b']
names = ['surface_downward_mass_flux_of_carbon_dioxide_expressed_as_carbon','surface_net_downward_mass_flux_of_carbon_dioxide_expressed_as_carbon_due_to_all_land_processes_excluding_anthropogenic_land_use_change']

for i,var in enumerate(vars):
    print
    models = model_names(directory,var)
    for model in models:
        cube = iris.load_cube(directory+model+'_'+var+'_rcp26_r1i1p1_regridded_not_vertically.nc',names[i])
        try:
            cube.coord('latitude').guess_bounds()
        except:
            print 'cube already has latitude bounds'
        try:
            cube.coord('longitude').guess_bounds()
        except:
            print 'cube already has longitude bounds'
        grid_areas = iris.analysis.cartography.area_weights(cube)
        cube.data = cube.data * grid_areas
        area_avged_cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
        area_avged_cube_cumsum = (np.cumsum(area_avged_cube.data)*1000.0)/1.0e15
        plt.plot(area_avged_cube_cumsum,colours[i])


plt.show()

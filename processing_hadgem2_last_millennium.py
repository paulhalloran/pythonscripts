import iris
import glob
from cdo import *
cdo = Cdo()
import uuid

variable = 'thetao'
directory = '/data/NAS-ph290/ph290/from_monsoon/'
temporary_file_space1 = '/data/dataSSD0/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD1/ph290/tmp/'

os.system('rm temporary_file_space1/*.nc')
os.system('rm temporary_file_space2/*.nc')


cubes = iris.load(directory+'*.pp')

for cube in cubes:
	iris.fileformats.netcdf.save(cube,temporary_file_space1+str(uuid.uuid4())+'.nc')


cdo.merge(input = temporary_file_space1+'*.nc', output = '/data/NAS-ph290/ph290/from_monsoon/netcdf/hadgem2es_'+variable+'_last_1000.nc',  options = '-P 7')


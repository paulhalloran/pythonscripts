import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
from cdo import *
cdo = Cdo()
import uuid
import subprocess
import glob
import os
import string

model_name = 'GFDL-ESM2M'
folder_name = 'gfdl-esm2m'

#model_name = 'IPSL-CM5A-LR'
#folder_name = 'ipsl-cm5a-lr'

temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph5290/tmp/'
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'


#actually input directory - tidy up...
output_directory = '/data/NAS-ph290/shared/simulation_forced_cmip5/'


final_output_directory = '/data/dataSSD0/ph290/nemo_forcing/'




runs = ['historical','rcp85']

leap_years = [1904, 1908, 1912, 1916, 1920, 1924, 1928, 1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020]
months_to_add_day_no_leap = []
months_to_add_day_leap = [2]
day_to_add_day_no_leap = []
day_to_add_day_leap = [28]



variables = ['tas','huss','uas','vas','pr','prsn','rsds','rlds']
variable_std_names = ['air_temperature','specific_humidity','eastward_wind','northward_wind','precipitation_flux','snowfall_flux','surface_downwelling_shortwave_flux_in_air','surface_downwelling_longwave_flux_in_air']
variables_time_freqs = ['day','day','day','day','day','day','day','day']
scalers_or_vectors = ['scaler','scaler','vector','vector','scaler','scaler','scaler','scaler']


for run in runs:
	filenames = ['tas_360x180-ORCA1_'+model_name+'_'+run+'_tas.nc4','huss_360x180-ORCA1_'+model_name+'_'+run+'_huss.nc4','u10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4','v10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4','pr_360x180-ORCA1_'+model_name+'_'+run+'_pr.nc4','prsn_360x180-ORCA1_'+model_name+'_'+run+'_prsn.nc4','rsds_360x180-ORCA1_'+model_name+'_'+run+'_rsds.nc4','rlds_360x180-ORCA1_'+model_name+'_'+run+'_rlds.nc4']
	final_var_name = ['T2','Q2','U10','V10','TP','SF','SSRD','STRD']
	final_long_name = ['2 metre temperature','2 metre specific humidity','10 metre x-direction wind component','10 metre y-direction wind component','total precipitation','snowfall','surface shortwave radiation downwards','surface longwave radiation downwards']
	for i,filename in enumerate(filenames):
		temp_cube = iris.load_cube('/data/NAS-ph290/shared/simulation_forced_cmip5/'+folder_name+'/merged_and_1x1_regridded/'+model_name+'_'+variables[i]+'_'+run+'.nc')
		cube = iris.load_cube(output_directory+filename)
		cube.coord('time_counter').rename('t')
		cube.coord('t').points = temp_cube.coord('time').points
		cube.coord('t').units = temp_cube.coord('time').units
		cube.long_name = final_long_name[i]
		cube.var_name = final_var_name[i]

		coord = cube.coord('t')
		# dt = coord.units.num2date(coord.points)
		years = np.array([coord.units.num2date(value).year for value in coord.points])
		loc1 = np.where((years >= 1960) & (years <= 2020))[0]

		cube = cube[loc1]
		coord = cube.coord('t')
		# dt = coord.units.num2date(coord.points)
		years = np.array([coord.units.num2date(value).year for value in coord.points])
		months = np.array([coord.units.num2date(value).month for value in coord.points])
		unique_years = np.unique(years)
		for year in unique_years:
			leap_year = year in leap_years
			loc2 = np.where(years == year)[0]
			cube_1_yr = cube[loc2]
			coord = cube_1_yr.coord('t')
			# dt = coord.units.num2date(coord.points)
			months = np.array([coord.units.num2date(value).month for value in coord.points])
			days = np.array([coord.units.num2date(value).day for value in coord.points])
			date_string = 'y'+str(year)+'m'+str(months[0]).zfill(2)+'d'+str(days[0]).zfill(2)
			#test if file exists
			if not(os.path.isfile(final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc')):
				days = np.array([coord.units.num2date(value).day for value in coord.points])
				if leap_year:
					month_to_use = months_to_add_day_leap
					days_to_use = day_to_add_day_leap
				else:
					if np.size(months_to_add_day_no_leap) > 0:
						month_to_use = months_to_add_day_no_leap
						days_to_use = day_to_add_day_no_leap
				segment_start = 0
				if np.size(months_to_add_day_no_leap) > 0:
					for j in np.arange(np.size(month_to_use)):
						loc3 = np.where((months == month_to_use[j]) & (days == days_to_use[j]))[0]
						#if np.size(loc3) > 1:
					#		loc3 = loc3[0]
						segment = cube_1_yr[segment_start:loc3+1]
						segment2 = cube_1_yr[loc3]
						segment_start = loc3+1
						letter = string.ascii_lowercase[j]
						iris.fileformats.netcdf.save(segment,temporary_file_space1+letter+'a'+'.nc',netcdf_format='NETCDF4')
						iris.fileformats.netcdf.save(segment2,temporary_file_space1+letter+'b'+'.nc',netcdf_format='NETCDF4')
					if np.size(days) > 330:
						#This condition is added, because some runs ('+model_name+' historical last year) stop at the end of November, so this failed
						segment = cube_1_yr[loc3+1::]
						letter = string.ascii_lowercase[j+1]
						iris.fileformats.netcdf.save(segment,temporary_file_space1+letter+'a'+'.nc',netcdf_format='NETCDF4')
					cdo.cat(input = temporary_file_space1+'*.nc', output = final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc',  options = '-P 7')
					subprocess.call('rm '+temporary_file_space1+'*.nc', shell=True)
				else:
					iris.fileformats.netcdf.save(cube_1_yr,final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc',netcdf_format='NETCDF4')
					
					
					
					
					

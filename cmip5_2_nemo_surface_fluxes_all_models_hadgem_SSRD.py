'''

execfile('/home/ph290/Documents/python_scripts/cmip5_2_nemo_surface_fluxes_all_models_hadgem_SSRD.py')

NOTE! Run this from the working_directory location...

example for fetching HadGEM2-ES data
ftpCmd_pr_rcp85.sh                                                     psl_6hrPlev_HadGEM2-ES_historical_r1i1p1_200012010600-200112010000.nc
ftpCmd_prsn_hist.sh                                                    psl_6hrPlev_HadGEM2-ES_historical_r1i1p1_200112010600-200212010000.nc
ftpCmd_prsn_rcp85.sh                                                   ta_6hrPlev_HadGEM2-ES_historical_r1i1p1_196012010600-196112010000.nc
ftpCmd_rsds_hist.sh                                                    ta_6hrPlev_HadGEM2-ES_historical_r1i1p1_196612010600-196712010000.nc
ftpCmd_rsds_rcp85.sh

requires:
2 metre temperature (K)
	tas (daily) - not 6 hourly, but 2m  meters is what NEMO needs... nemo also takes 10m
	# ta (take 1st level, units K), 6hrLev NOTE - ise 6hrLev NOT 6hrPLev, which does not have ta at the surface...
2 metre specific humidity (units = 1)
	huss (daily) - not 6 hourly, but 2m  meters is what NEMO needs... nemo also takes 10m
	# hus (take 1st level), 6hrLev (units = 1)
10 metre x-direction wind component m s**-1  Regrid differently...
	uas (daily) - not 6 hourly, but 10m  meters is what NEMO needs... nemo also takes 2m
	# ua, 6hrLev (m s-1)
10 metre y-direction wind component m s**-1
	vas (daily) - not 6 hourly, but 10m  meters is what NEMO needs... nemo also takes 2m
	# va, 6hrLev (m s-1)
total precipitation mm/s
	pr, (kg m-2 s-1), day (note kg m-2 s-1 == mm/s)
snowfall mm/s
	prsn, (kg m-2 s-1), day
surface shortwave radiation downwards W m**-2
	rsds (W m-2), day
STRD (Surface Thermal Radiation Downwards - presumably longwve downwards) W m**-2
	rlds (W m-2), day

#template_cube = iris.load_cube('/data/NAS-ph290/shared/ford_data/home/daford/for_alice_paul/mi-aj885_spinup/mi-aj885_fco2_1980.nc')

'''


import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
from cdo import *
cdo = Cdo()
import uuid
import subprocess
import glob
import os
import string
import time

print '#####################################################'
print '#####################################################'
print 'before running, pace the following into the shell:'
print 'export HDF5_DISABLE_VERSION_CHECK=1'
print '#####################################################'
print '#####################################################'


print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print '#############################################################################################'
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

print 'Output will end up in the directory you are running this from. MAKE SURE that this matches'
print 'what you have specified as the variable working_directory (not final_output_directory) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print '#############################################################################################'
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'



temporary_file_space1 = '/data/data0/ph290/tmp/'
temporary_file_space2 = '/data/data1/ph290/tmp/'
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'

#models_in_list = ['CanESM2','MRI-ESM1','GFDL-ESM2M','IPSL-CM5A-LR','IPSL-CM5A-MR','IPSL-CM5B-LR']
models_in_list = ['HadGEM2-ES']

calendar_list = ['360_day']
for k,model_list in enumerate(models_in_list):
	calendar = calendar_list[k]
	###########################
	# SET MODEL NAME!!!!!!!!!!!
	###########################
	model_name = model_list
	###########################
	###########################


	###########################
	# SET DIRECTORY LOCATOINS!!
	###########################
	input_directory = '/data/NAS-ph290/shared/CURBCO2/SIMULATIONS/INITIAL_CMIP5_SURFACE_FLUXES/'+model_list+'/'
	working_directory = '/data/NAS-ph290/ph290/nemo/output_forcing_files/hadgem2/'
	final_output_directory = '/data/NAS-ph290/shared/CURBCO2/SIMULATIONS/REGRIDDED_CMIP5_SURFACE_FLUXES/'+model_list+'/'
	###########################
	###########################

	#steps:
	#Extract relevent years
	# merge and select level 1 (sea level)
	# Regrid to 360x180
	# Extract names required for fortran namelist
	# produce the namelists
	# regrid to orca grid
	# interpolate to 6 hourly data ON 365 DAY REAL Calendar
	# combine into single netcdf


	###########################
	# SET model's year_type to avoid issues with 360 day years and leap years!!!!!!!!!!!
	#see https://esgf.github.io/esgf-swt/data/2015/08/13/Do-all-CMIP5-models-use-the-same-calendar.html
	#https://github.com/ESGF/esgf-swt/blob/master/_posts/2015-02-09-Do-all-CMIP5-models-use-the-same-calendar.markdown
	###########################

	############
	#SET THIS:
	calendar = calendar
	############

	leap_years = [1804,1808,1812,1816,1820,1824,1828,1832,1836,1840,1844,1848,1852,1856,1860,1864,1868,1872,1876,1880,1884,1888,1892,1896,1904,1908,1912,1916,1920,1924,1928,1932,1936,1940,1944,1948,1952,1956,1960,1964,1968,1972,1976,1980,1984,1988,1992,1996,2000,2004,2008,2012,2016,2020,2024,2028,2032,2036,2040,2044,2048,2052,2056,2060,2064,2068,2072,2076,2080,2084,2088,2092,2096,2104,2108,2112,2116,2120,2124,2128,2132,2136,2140,2144,2148,2152,2156,2160,2164,2168,2172,2176,2180,2184,2188,2192,2196]


#1j31,2f28,3m31,4a30,5m31,6j30,7j31,8a31,9s30,10o31,11n30,12d31
	if calendar == '360_day':
		months_to_add_day_no_leap = [1,7,8,10,11] #not added to mar or apr, 'cos still ahead after feb having 30
		months_to_add_day_leap = [1,3,7,8,10,11]
		day_to_add_day_no_leap = [30,30,30,30,30]
		day_to_add_day_leap = [30,30,30,30,30,30]


	if calendar == '365_day':
		months_to_add_day_no_leap = []
		months_to_add_day_leap = [2]
		day_to_add_day_no_leap = []
		day_to_add_day_leap = [28]


	if calendar == 'gregorian':
		months_to_add_day_no_leap = []
		months_to_add_day_leap = []
		day_to_add_day_no_leap = []
		day_to_add_day_leap = []


	if calendar == 'proleptic_gregorian':
		months_to_add_day_no_leap = []
		months_to_add_day_leap = []
		day_to_add_day_no_leap = []
		day_to_add_day_leap = []



	###########################
	###########################

	runs = ['historical','rcp26','rcp85']
	runs = ['rcp85']
	# variables = ['ta','hus','ua','va','pr','prsn','rsds','rlds']
	# variable_std_names = ['air_temperature','specific_humidity','eastward_wind','northward_wind','precipitation_flux','snowfall_flux','surface_downwelling_shortwave_flux_in_air','surface_downwelling_longwave_flux_in_air']
	# variables_time_freqs = ['6hrLev','6hrLev','6hrLev','6hrLev','day','day','day','day']
	# scalers_or_vectors = ['scaler','scaler','vector','vector','scaler','scaler','scaler','scaler']

	variables = ['tas','huss','uas','vas','pr','prsn','rsds','rlds']
	variables = ['rsds']
	variable_std_names = ['air_temperature','specific_humidity','eastward_wind','northward_wind','precipitation_flux','snowfall_flux','surface_downwelling_shortwave_flux_in_air','surface_downwelling_longwave_flux_in_air']
	variable_std_names = ['surface_downwelling_shortwave_flux_in_air']
	variables_time_freqs = ['day']
	#,'day','day','day','day','day','day','day']
	scalers_or_vectors = ['scaler']
	#,'scaler','vector','vector','scaler','scaler','scaler','scaler']


	for run in runs:
		for i,variable in enumerate(variables):

	# run = runs[0]
	# variable = variables[0]
	# i = 0
			#test to see if output file already exists
			if variable == 'uas':
				filetest1 = np.size(glob.glob(working_directory+'/uraw_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4'))
				if filetest1 > 0:
					print variable+' output files already exists'
			elif variable == 'vas':
				filetest1 = np.size(glob.glob(working_directory+'/vraw_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4'))
				if filetest1 > 0:
					print variable+' output files already exists'
			else:
				filetest1 = np.size(glob.glob(working_directory+'/*_'+model_name+'_'+run+'_'+variable+'.nc4'))
				if filetest1 > 0:
					print variable+' output files already exists'

			print filetest1
			if filetest1 == 0:
				print 'run = '+run
				print 'variable = '+variable

				variable_std_name = variable_std_names[i]
				files1 = glob.glob(input_directory+variable+'_'+variables_time_freqs[i]+'_'+model_name+'*'+run+'*.nc')
				sample_cube = iris.load_raw(files1[0],variable_std_name)[0]

				########
				#extract required info
				########

				variable_name = sample_cube.var_name.encode('ascii', 'ignore')
				variable_long_name = sample_cube.standard_name.encode('ascii', 'ignore')
				unit_info = str(sample_cube.units)
				time_coordinate_name = sample_cube.coord(dimensions=0).standard_name.encode('ascii', 'ignore')
				timestep_description = str(int((sample_cube.coord(dimensions=0).points[1] - sample_cube.coord(dimensions=0).points[0])))+' days'


				if sample_cube.ndim == 4:
					try:
						level_height_or_pressure = str(sample_cube.coord(dimensions=1).points[0])
					except:
						level_height_or_pressure = str(sample_cube.coord('atmosphere_hybrid_height_coordinate').points[0])
				else:
					level_height_or_pressure = 'na'

				########
				#merge files into single file(s - two files needed one for historical and one for rcp95 - see explanation below)
				########

				if level_height_or_pressure <> 'na':
					cdo.select('name='+variable_name+',startdate=1950-01-01T01:01:01,enddate=2100-01-01T01:01:01,level='+level_height_or_pressure,input = files1, output = temporary_file_space1+temp_file1,  options = '-P 7')
					#I would have hoped that this step below was not necessary - but it seems to be... bit of a bug with cdo select
					cdo.selstdname(variable_long_name,input = temporary_file_space1+temp_file1,output  =temporary_file_space2+temp_file2,  options = '-P 7')
					cdo.mergetime(input=temporary_file_space2+temp_file2,output=temporary_file_space2+temp_file1,options = '-P 7')
					os.remove(temporary_file_space2+temp_file2)
					os.remove(temporary_file_space1+temp_file1)
				else:
					# cdo.select('name='+variable_name+',startdate=1960-01-01T01:01:01,enddate=2017-01-01T01:01:01',input = files1, output = temporary_file_space1+temp_file1,  options = '-P 7')
					cdo.mergetime(input=files1,output=temporary_file_space1+temp_file1,options = '-P 7')
					cdo.selstdname(variable_long_name,input = temporary_file_space1+temp_file1,output  =temporary_file_space2+temp_file2,  options = '-P 7')
					cdo.selstdname(variable_long_name,input = temporary_file_space2+temp_file2,output  =temporary_file_space2+temp_file1,  options = '-P 7')
					os.remove(temporary_file_space2+temp_file2)
					os.remove(temporary_file_space1+temp_file1)

				# 			os.remove(temporary_file_space1+temp_file1)
				#NOTE - the historical and RCP runs for had gem2 seem to have some differences in teh way they have been processed for CMIP5, so treating separately at this stage...
				#cdo.mergetime(input=[temporary_file_space2+temp_file1,temporary_file_space2+temp_file2],output=temporary_file_space1+temp_file2,options='-P 7')

				########
				# Regridding files to 360*180
				########

				test = np.float(subprocess.check_output('df -h '+temporary_file_space1, shell=True).split('    ')[2].split('  ')[2].split('G')[0])
				if test > 200:
					temp_working_directory = temporary_file_space1+'/'+model_name
				else:
					temp_working_directory = '/data/data0/ph290/tmp/'+model_name


				subprocess.call('mkdir '+temporary_file_space1+'/'+model_name, shell=True)
				subprocess.call('mkdir '+temporary_file_space1+'/'+model_name+'/merged_and_1x1_regridded', shell=True)
				intermediate_dir_and_file = temporary_file_space1+'/'+model_name+'/merged_and_1x1_regridded/'+model_name+'_'+variable+'_'+run+'.nc'
				cdo.remapbil('r360x180',input = temporary_file_space2+temp_file1, output = intermediate_dir_and_file,  options = '-P 7')


				########
				# create the namelist file to be used in the regridding to the ORCA1 grid
				########

				test = False

				if variable_name == 'uas':
					#u-winds note - vector fields need tp be treated separately. See: Example 4 in http://sosie.sourceforge.net/
					f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_sample_file','r')
					contents = f.read()
					f.close()

					file_to_regrid = intermediate_dir_and_file

					contents = contents.replace("file_to_regrid","'"+file_to_regrid+"'")
					contents = contents.replace("variable_name","'"+variable_name+"'")
					contents = contents.replace("variable_long_name","'"+variable_long_name+"'")
					contents = contents.replace("unit_info","'"+unit_info+"'")
					contents = contents.replace("time_coordinate_name","'"+time_coordinate_name+"'")
					contents = contents.replace("timestep_description","'"+timestep_description+"'")
					contents = contents.replace("extra_text","'"+model_name+'_'+run+"'")
					contents = contents.replace("directory_and_info","'"+variable_name+"'")

					f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file','w')
					f.write(contents)
					f.close()

					subprocess.call('/home/ph290/src/sosie-2.6.4/bin/sosie.x -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file', shell=True)
					test = True


				if variable_name == 'vas':
					#v-winds
					f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_y_sample_file','r')
					contents = f.read()
					f.close()

					file_to_regrid = intermediate_dir_and_file

					contents = contents.replace("file_to_regrid","'"+file_to_regrid+"'")
					contents = contents.replace("variable_name","'"+variable_name+"'")
					contents = contents.replace("variable_long_name","'"+variable_long_name+"'")
					contents = contents.replace("unit_info","'"+unit_info+"'")
					contents = contents.replace("time_coordinate_name","'"+time_coordinate_name+"'")
					contents = contents.replace("timestep_description","'"+timestep_description+"'")
					contents = contents.replace("extra_text","'"+model_name+'_'+run+"'")
					contents = contents.replace("directory_and_info","'"+variable_name+"'")

					f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_y_temporary_file','w')
					f.write(contents)
					f.close()

					subprocess.call('/home/ph290/src/sosie-2.6.4/bin/sosie.x -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_y_temporary_file', shell=True)
					test = True


				if test == False:
					#for all other variables
					f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_sample_file','r')
					contents = f.read()
					f.close()

					file_to_regrid = intermediate_dir_and_file

					contents = contents.replace("file_to_regrid","'"+file_to_regrid+"'")
					contents = contents.replace("variable_name","'"+variable_name+"'")
					contents = contents.replace("variable_long_name","'"+variable_long_name+"'")
					contents = contents.replace("unit_info","'"+unit_info+"'")
					contents = contents.replace("time_coordinate_name","'"+time_coordinate_name+"'")
					contents = contents.replace("timestep_description","'"+timestep_description+"'")
					contents = contents.replace("extra_text","'"+model_name+'_'+run+'_'+variable+"'")
					contents = contents.replace("directory_and_info","'"+variable_name+"'")

					f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_temporary_file','w')
					f.write(contents)
					f.close()

					subprocess.call('/home/ph290/src/sosie-2.6.4/bin/sosie.x -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_temporary_file', shell=True)


				os.remove(temporary_file_space2+temp_file1)


		filetest4 = np.size(glob.glob(working_directory+'/u10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4'))
		if filetest4 == 0:
			print 'final step on vector variables'
			filetest2 = np.size(glob.glob(working_directory+'/uraw_*'+run+'.nc4'))
			filetest3 = np.size(glob.glob(working_directory+'/vraw_*'+run+'.nc4'))
			if (filetest2 > 0) & (filetest3 > 0):
				# Final step in vector regridding (for u and v)
				subprocess.call('/home/ph290/src/sosie-2.6.4/bin/corr_vect.x -x u10 -y v10 -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file -m /home/ph290/Documents/python_scripts/SOSIE/mesh_mask_ORCA1_light.nc', shell=True)

		print 'Processing and regridding of files to ORCA1 grid complete'


		#########################
		#tidying up dimensions, variable names, converting to 365 day calendar with leap years and saving as annual riles with correct filenames
		#########################


		filenames = ['tas_360x180-ORCA1_'+model_name+'_'+run+'_tas.nc4','huss_360x180-ORCA1_'+model_name+'_'+run+'_huss.nc4','u10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4','v10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4','pr_360x180-ORCA1_'+model_name+'_'+run+'_pr.nc4','prsn_360x180-ORCA1_'+model_name+'_'+run+'_prsn.nc4','rsds_360x180-ORCA1_'+model_name+'_'+run+'_rsds.nc4','rlds_360x180-ORCA1_'+model_name+'_'+run+'_rlds.nc4']
		filenames = ['rsds_360x180-ORCA1_'+model_name+'_'+run+'_rsds.nc4']
		final_var_name = ['T2','Q2','U10','V10','TP','SF','SSRD','STRD']
		final_var_name = ['SSRD']
		final_long_name = ['2 metre temperature','2 metre specific humidity','10 metre x-direction wind component','10 metre y-direction wind component','total precipitation','snowfall','surface shortwave radiation downwards','surface longwave radiation downwards']
		final_long_name = ['surface shortwave radiation downwards']
		for i,filename in enumerate(filenames):
			temporary_var = '/'.join((intermediate_dir_and_file.split('/'))[0:8])

			temp_cube = iris.load_cube(temporary_var+'/'+model_name+'_'+variables[i]+'_'+run+'.nc')
			cube = iris.load_cube(working_directory+filename)
			cube.coord('time_counter').rename('t')
			cube.coord('t').points = temp_cube.coord('time').points
			cube.coord('t').units = temp_cube.coord('time').units
			cube.long_name = final_long_name[i]
			cube.var_name = final_var_name[i]

			coord = cube.coord('t')
			# dt = coord.units.num2date(coord.points)
			years = np.array([coord.units.num2date(value).year for value in coord.points])
			loc1 = np.where((years >= 1950) & (years <= 2100))[0]

			cube = cube[loc1]
			coord = cube.coord('t')
			# dt = coord.units.num2date(coord.points)
			years = np.array([coord.units.num2date(value).year for value in coord.points])
			months = np.array([coord.units.num2date(value).month for value in coord.points])
			unique_years = np.unique(years)
			for year in unique_years:
				leap_year = year in leap_years
				loc2 = np.where(years == year)[0]
				cube_1_yr = cube[loc2]
				coord = cube_1_yr.coord('t')
				# dt = coord.units.num2date(coord.points)
				months = np.array([coord.units.num2date(value).month for value in coord.points])
				days = np.array([coord.units.num2date(value).day for value in coord.points])
				date_string = 'y'+str(year)+'m'+str(months[0]).zfill(2)+'d'+str(days[0]).zfill(2)
				#test if file exists
				if not(os.path.isfile(final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc')):
					if leap_year:
						month_to_use = months_to_add_day_leap
						days_to_use = day_to_add_day_leap
					else:
						month_to_use = months_to_add_day_no_leap
						days_to_use = day_to_add_day_no_leap
					segment_start = 0
					if month_to_use <> []:
						try:
							for j in np.arange(np.size(month_to_use)):
								try:
									print j
									loc3 = np.where((months == month_to_use[j]) & (days == days_to_use[j]))[0]
									segment = cube_1_yr[segment_start:loc3+1]
									segment.coord('t').points =segment.coord('t').points + j
									segment2 = cube_1_yr[loc3]
									segment2.coord('t').points = segment2.coord('t').points +(j+1.0)
									segment_start = loc3+1
									letter = string.ascii_lowercase[j]
									iris.fileformats.netcdf.save(segment,temporary_file_space1+letter+'a'+'.nc',netcdf_format='NETCDF4')
									iris.fileformats.netcdf.save(segment2,temporary_file_space1+letter+'b'+'.nc',netcdf_format='NETCDF4')
								except:
									print 'does not start in January'
							if np.size(days) > 330:
								#This condition is added, because some runs (HadGEM2-ES historical last year) stop at the end of November, so this failed
								segment = cube_1_yr[loc3+1::]
								letter = string.ascii_lowercase[j+1]
								iris.fileformats.netcdf.save(segment,temporary_file_space1+letter+'a'+'.nc',netcdf_format='NETCDF4')
							cdo.cat(input = temporary_file_space1+'*.nc', output = final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc',  options = '-P 7')
							subprocess.call('rm '+temporary_file_space1+'*.nc', shell=True)
						except:
							print 'no days to insert'
							iris.fileformats.netcdf.save(cube_1_yr,final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc',netcdf_format='NETCDF4')
					else:
						print 'no days to insert'
						iris.fileformats.netcdf.save(cube_1_yr,final_output_directory+model_name+'_'+run+final_var_name[i]+'_'+date_string+'.nc',netcdf_format='NETCDF4')


		subprocess.call('rm -rf /data/dataSSD1/ph290/tmp/'+model_name, shell=True)
		subprocess.call('rm -rf /data/NAS-ph290/ph290/nemo/output_forcing_files/*_'+model_name+'_*.nc4', shell=True)

	try:
		temporary_var = '/'.join((intermediate_dir_and_file.split('/'))[0:5])
		subprocess.call('rm -rf '+temporary_var+'/'+model_name, shell=True)
	except:
		print 'none to delete'












"""


import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
from cdo import *
cdo = Cdo()
import uuid
import subprocess
import glob
import os
import string

runs = ['historical','rcp85']

for run in runs:
	model_name = 'GFDL-ESM2M'
	filetest4 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/u10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4'))
	if filetest4 == 0:
	        print 'final step on vector variables'
	        filetest2 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/uraw_*'+run+'.nc4'))
	        filetest3 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/vraw_*'+run+'.nc4'))
	        if (filetest2 > 0) & (filetest3 > 0):
	                # Final step in vector regridding (for u and v)
					subprocess.call('/home/ph290/src/sosie-2.6.4/bin/corr_vect.x -x u10 -y v10 -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file -m /home/ph290/Documents/python_scripts/SOSIE/mesh_mask_ORCA1_light.nc', shell=True)
	model_name = 'IPSL-CM5A-LR'
	filetest4 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/u10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4'))
	if filetest4 == 0:
	        print 'final step on vector variables'
	        filetest2 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/uraw_*'+run+'.nc4'))
	        filetest3 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/vraw_*'+run+'.nc4'))
	        if (filetest2 > 0) & (filetest3 > 0):
	                # Final step in vector regridding (for u and v)
	                subprocess.call('/home/ph290/src/sosie-2.6.4/bin/corr_vect.x -x u10 -y v10 -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file -m /home/ph290/Documents/python_scripts/SOSIE/mesh_mask_ORCA1_light.nc', shell=True)
"""

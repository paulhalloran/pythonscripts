
import numpy as np
import iris
import matplotlib.pyplot as plt
import glob
from scipy import signal
import scipy
import numpy.ma as ma
import os
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
import running_mean as rm
from scipy.signal import freqz
from scipy.signal import butter, lfilter



def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a







def smoothing_script(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    np.hanning, np.hamming, np.bartlett, np.blackman, np.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y


start_year = 1200
start_year = 1000
end_year = 1800

smoothing = 5

# b1, a1 = butter_lowpass(1.0/smoothing, 1.0,6)
b2, a2 = butter_highpass(1.0/smoothing, 1.0,6)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################




#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
# bivalve_data_initial = scipy.signal.filtfilt(b1, a1, bivalve_data_initial)
bivalve_data_initial = scipy.signal.filtfilt(b2, a2, bivalve_data_initial)

bivalve_data_initial -= np.mean(bivalve_data_initial)
bivalve_data_initial /= np.std(bivalve_data_initial)



##########################################
# European summer temps #
##########################################

# r_data_file = '/data/NAS-ph290/ph290/misc_data/Guiot_et_al_2005/guiot05_eurot.txt'
# r_data = np.genfromtxt(r_data_file,skip_header = 7,delimiter = ',')
# tmp = r_data[:,2]
# loc = np.where(tmp > -9.99990000e+02)
# tmp_yr = r_data[loc[0],1]
# euro_summ_t = tmp[loc[0]]
# euro_summ_t_year = tmp_yr
# euro_summ_t=scipy.signal.detrend(euro_summ_t)
# euro_summ_t -= np.mean(euro_summ_t)
# euro_summ_t /= np.std(euro_summ_t)

##########################################
# N. hem temps #
##########################################


# r_data_file = '/data/NAS-geo01/ph290/misc_data/wilson_2016.csv'
# r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
# tmp = r_data[:,1]
# loc = np.where((np.logical_not(np.isnan(tmp))))
# tmp = tmp[loc[0]]
# tmp_yr = r_data[loc[0],0]
# n_hem_t = tmp
# n_hem_t_year = tmp_yr

##########################################
# Read in Masse 2008 seaice data #
##########################################

m_data_file = '/data/NAS-ph290/ph290/misc_data/masse_sea_ice.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ',')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
ip25_data_initial = tmp
ip25_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
ip25_data_initial=(ip25_data_initial[::-1])
# ip25_data_initial=scipy.signal.detrend(ip25_data_initial)
ip25_data_initial -= np.mean(ip25_data_initial)
ip25_data_initial /= np.std(ip25_data_initial)


##########################################
# Read in Halfar 2014 seaice data # https://www1.ncdc.noaa.gov/pub/data/paleo/contributions_by_author/halfar2014/halfar2014sic.txt
##########################################

# m_data_file = '/data/NAS-ph290/ph290/misc_data/halfar2014sic.txt'
# m_data = np.genfromtxt(m_data_file,skip_header = 85,delimiter = '\t')
# tmp = m_data[:,1]
# loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
# tmp = tmp[loc[0]]
# tmp_yr = m_data[loc[0],0]
# Halfar_data_initial = tmp
# Halfar_year = tmp_yr # reverse because original d18O data has time starting from the present day
# # Halfar_data_initial=scipy.signal.detrend(Halfar_data_initial)
# # Halfar_data_initial = scipy.signal.filtfilt(b1, a1, Halfar_data_initial)
# Halfar_data_initial = scipy.signal.filtfilt(b2, a2, Halfar_data_initial)
# 
# Halfar_data_initial -= np.mean(Halfar_data_initial)
# Halfar_data_initial /= np.std(Halfar_data_initial)



##########################################
# solar
##########################################

# m_data_file = '/data/NAS-geo01/ph290/misc_data/solar_cmip5/tsi_VK.txt'
# m_data = np.genfromtxt(m_data_file,skip_header = 4,delimiter = ' ')
# tmp = m_data[:,1]
# loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
# tmp = tmp[loc[0]]
# tmp_yr = m_data[loc[0],0]
# solar_data = tmp
# solar_year = tmp_yr


##########################################
# volc
##########################################

# m_data_file = '/data/NAS-ph290/ph290/cmip5/forcing_data/ICI5_030N_AOD_c.txt'
# m_data = np.genfromtxt(m_data_file,skip_header = 0,delimiter = '\t')
# tmp = m_data[:,1]
# loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
# tmp = tmp[loc[0]]
# tmp_yr = m_data[loc[0],0]
# volc_data = tmp
# volc_year = tmp_yr


##########################################
# insolation
##########################################

# m_data_file = '/data/NAS-geo01/ph290/misc_data/insol91.jun_dec'
# m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ' ')
# tmp1 = m_data[:,2]
# tmp2 = m_data[:,9]
# tmp_yr = m_data[:,0]
# f1 = scipy.interpolate.interp1d(tmp_yr, tmp1)
# f2 = scipy.interpolate.interp1d(tmp_yr, tmp2)
# ins_year = np.linspace(start_year,end_year,(end_year - start_year + 1))
# ins_june = f1(ins_year)
# ins_dec = f2(ins_year)


##########################################
# Anthro radiative forcing
##########################################

# m_data_file = '/data/NAS-geo01/ph290/misc_data/20THCENTURY_MIDYEAR_RADFORCING.DAT'
# m_data = np.genfromtxt(m_data_file,skip_header = 61,delimiter = ',')
# tmp1 = m_data[:,5]
# tmp_yr = m_data[:,1]
# anthro_forcing = tmp1
# anthro_year = tmp_yr


##########################################
# Mann AMO data
##########################################
# amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
# amo = np.genfromtxt(amo_file, skip_header = 2)
# amo_yr = amo[:,0]
# amo_data = amo[:,1]
# amo_data_minus_2sigma = amo[:,2]
# amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
# loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
# amo_yr = amo_yr[loc]
# amo_data = amo_data[loc]
# amo_data_sigma_diff = amo_data_sigma_diff[loc]
# #amo_data -= np.min(amo_data)
# #amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
# #amo_data /= np.max(amo_data)
# #amo_data_sigma_diff /= np.max(amo_data)
# amo_data=signal.detrend(amo_data)

##########################################
# Mann AMO data
##########################################
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)
# amo_data = scipy.signal.filtfilt(b2, a2, amo_data)

amo_data -= np.mean(amo_data)
amo_data /= np.std(amo_data)



##########################################
# Greenland T https://www.nature.com/articles/s41598-017-01451-7#Sec8
##########################################
greenland_file = '/data/NAS-geo01/ph290/misc_data/greenland_t_Kobashi_2017.csv'
greenland = np.genfromtxt(greenland_file, skip_header = 2,delimiter=',')
greenland[0,:] = 1950 - greenland[0,:]
greenland_yr = greenland[:,0]
greenland_data = greenland[:,1]
greenland_data_minus_2sigma = greenland[:,2]
greenland_data_sigma_diff = greenland_data_minus_2sigma + greenland_data
loc = np.where((np.logical_not(np.isnan(greenland_data_sigma_diff))) & (np.logical_not(np.isnan(greenland_data))) & (greenland_yr >= start_year) & (greenland_yr <= end_year))
greenland_yr = greenland_yr[loc]
greenland_data = greenland_data[loc]
greenland_data_sigma_diff = greenland_data_sigma_diff[loc]
#greenland_data -= np.min(greenland_data)
#greenland_data_sigma_diff-= np.min(greenland_data_sigma_diff)
#greenland_data /= np.max(greenland_data)
#greenland_data_sigma_diff /= np.max(greenland_data)
greenland_data=signal.detrend(greenland_data)
# greenland_data = scipy.signal.filtfilt(b2, a2, greenland_data)

greenland_data -= np.mean(greenland_data)
greenland_data /= np.std(greenland_data)


##########################################
# Greenland STACK T https://www.clim-past.net/12/171/2016/cp-12-171-2016.pdf
#
#positive is warm:
# see text: "We conclude that any conversion of the NG stack..."
#
##########################################

greenland_stack_file = '/data/NAS-geo01/ph290/misc_data/Weissbach_2016/datasets/NG-stack.tab'
greenland_stack = np.genfromtxt(greenland_stack_file, skip_header = 14,delimiter='\t')
greenland_stack_yr = greenland_stack[:,1]
greenland_stack_data = greenland_stack[:,2]
loc = np.where((np.logical_not(np.isnan(greenland_stack_data))) & (greenland_stack_yr >= start_year) & (greenland_stack_yr <= end_year))
greenland_stack_yr = greenland_stack_yr[loc]
greenland_stack_data = greenland_stack_data[loc]
#greenland_stack_data -= np.min(greenland_stack_data)
#greenland_stack_data_sigma_diff-= np.min(greenland_stack_data_sigma_diff)
#greenland_stack_data /= np.max(greenland_stack_data)
#greenland_stack_data_sigma_diff /= np.max(greenland_stack_data)
greenland_stack_data=signal.detrend(greenland_stack_data)
greenland_stack_data = scipy.signal.filtfilt(b2, a2, greenland_stack_data)

greenland_stack_data -= np.mean(greenland_stack_data)
greenland_stack_data /= np.std(greenland_stack_data)



##########################################
# fennoscandinavia d13C
##########################################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
#s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]


loc = np.where((np.logical_not(np.isnan(s_d))) & (s_y >= start_year) & (s_y <= end_year))
s_y = s_y[loc[0]]
s_d = s_d[loc[0]]
# s_d=scipy.signal.detrend(s_d)
# s_d = scipy.signal.filtfilt(b1, a1, s_d)
s_d = scipy.signal.filtfilt(b2, a2, s_d)

s_d -= np.mean(s_d)
s_d /= np.std(s_d)



##################
# figure
##################


plt.close('all')
# plt.figure(figsize=(6, 10))

# fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
# fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
fig, (ax1) = plt.subplots(1, 1, sharex=True)

ax1.plot(s_y, s_d, color='black')
# ax1.plot(Halfar_year, Halfar_data_initial, color='red')
# ax2.plot(ip25_year, ip25_data_initial*-1, color='green')
ax1.plot(bivalve_year, bivalve_data_initial , color='blue')
# ax1.plot(euro_summ_t_year, euro_summ_t[np.floor(smoothing/2):np.floor(smoothing/2)*-1] * -1.0, color='green')


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/proxy_timeseries1.png')
plt.show(block = False)



##################
# figure
##################


plt.close('all')
# plt.figure(figsize=(6, 10))

# fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
# fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
fig, (ax1) = plt.subplots(1, 1, sharex=True)

# ax1.plot(s_y, s_d, color='black')
# ax1.plot(amo_yr, amo_data * -1.0, color='black')
ax1.plot(greenland_stack_yr, greenland_stack_data*-1.0, color='black')


# ax1.plot(Halfar_year, Halfar_data_initial, color='red')
# ax2.plot(ip25_year, ip25_data_initial*-1, color='green')
ax1.plot(bivalve_year, bivalve_data_initial , color='blue')
# ax1.plot(euro_summ_t_year, euro_summ_t[np.floor(smoothing/2):np.floor(smoothing/2)*-1] * -1.0, color='green')


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/proxy_timeseries2.png')
plt.show(block = False)

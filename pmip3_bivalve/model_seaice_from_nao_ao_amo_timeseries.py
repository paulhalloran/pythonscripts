
import numpy as np
import iris
import matplotlib.pyplot as plt
import glob
from scipy import signal
import scipy
import numpy.ma as ma
import os
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
import running_mean as rm
from scipy.signal import freqz
from scipy.signal import butter, lfilter

import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
import scipy.stats as stats
import pandas as pd
import matplotlib.cm as mpl_cm
from matplotlib.patches import ConnectionPatch


def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a







def smoothing_script(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    np.hanning, np.hamming, np.bartlett, np.blackman, np.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y


start_year = 950
end_year = 1850

smoothing = 7

# b1, a1 = butter_lowpass(1.0/smoothing, 1.0,6)
b2, a2 = butter_highpass(1.0/smoothing, 1.0,6)



#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory,variable):
	files = glob.glob(directory+'/*'+variable+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


#################
# correlations
#################




def smoothed_correlations(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_not_sig = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
	        smoothing1 = np.int(smoothing1)
	        print smoothing_no1,' out of ',np.size(smoothings1)
	        window_type = 'boxcar'
	        for smoothing_no2, smoothing2 in enumerate(smoothings2):
	                smoothing2 = np.int(smoothing2)
	                x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
	                y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
	                loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
	                x = x[loc]
	                y = y[loc]
	                slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	                r2 = r_value**2
	                coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	                if p_value > 0.001:
	                        coeff_det_a_b_not_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_not_sig



'''
#Main bit of code follows...
'''

#N Hem
lon_west1 = 0
lon_east1 = 360
lat_south1 = 0.0
lat_north1 = 90.0

#Atl sector of Arctic
lon_west1 = -45.0
lon_east1 = 25.0
lat_south1 = 0.0
lat_north1 = 90.0

region1 = iris.Constraint(longitude=lambda v: lon_west1 <= v <= lon_east1,latitude=lambda v: lat_south1 <= v <= lat_north1)


variables = np.array(['sic'])
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.

################
# sea-Ice
################

input_directory2 = '/data/NAS-ph290/ph290/cmip5/last1000/monthly/'

# modelsb = model_names(input_directory2,variables[0])
modelsb = models = ['bcc-csm1-1','CSIRO-Mk3L-1-2','MIROC-ESM','MPI-ESM-P']

cube1 = iris.load_cube(input_directory2+modelsb[0]+'*'+variables[0]+'*.nc')[0]

modelbs2 = []
cubes_n_hem = []
ts_n_hem = []

for model in modelsb:
	print 'processing: '+model
	try:
		cube = iris.load_cube(input_directory2+model+'*'+variables[0]+'*.nc')
	except:
		cube = iris.load(input_directory2+model+'*'+variables[0]+'*.nc')
		cube = cube[0]
	# iris.coord_categorisation.add_month(cube, 'time', name='month')
	# cube = cube[np.where(cube.coord('month').points == 'Mar')]
	iris.coord_categorisation.add_year(cube, 'time', name='year')
	# iris.coord_categorisation.add_season(cube, 'time', name='season')
	# iris.coord_categorisation.add_season_year(cube, 'time', name='season_year')
	# cube = cube.aggregated_by(['season_year','season'], iris.analysis.MEAN)
	# cube = cube[np.where(cube.coord('season').points == 'son')]
	cube = cube.aggregated_by(['year'], iris.analysis.MEAN)
	if model == ('MRI-CGCM3'):
		for i in range(cube.shape[0]):
			cube.data.mask[i] = cube1.data.mask
	tmp1 = cube.extract(region1)
	cubes_n_hem.append(tmp1)
	#qplt.contourf(tmp1[0])
	#plt.show()
	ts_n_hem.append(tmp1.collapsed(['latitude','longitude'],iris.analysis.MEAN))



tmp = np.shape(cubes_n_hem[0].data)
# tmp_data = np.ma.empty([np.size(cubes_n_hem),tmp[0],tmp[1],tmp[2]])
tmp_data = np.ma.empty([np.size(cubes_n_hem),1000,tmp[1],tmp[2]])

for i in np.arange(np.size(cubes_n_hem)):
	tmp_data[i] = cubes_n_hem[i].data[0:1000]



coord = ts_n_hem[0].coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])



data = np.zeros([1000,np.size(ts_n_hem)])
j=0
for i,ts in enumerate(ts_n_hem):
	print modelsb[i]
	data[:,j] = signal.detrend(ts.data[0:1000])
	# data[:,j] = (ts.data[0:1000])
	j += 1

multimodel_mean = data.mean(axis = 1)
multimodel_max = data.max(axis = 1)
multimodel_min = data.min(axis = 1)


###########
# nao     #
###########


"""
bcc-csm1-1_psl_past1000_r1i1p1_regridded.nc      IPSL-CM5A-LR_psl_past1000_r1i1p1_regridded.nc
CSIRO-Mk3L-1-2_psl_past1000_r1i1p1_regridded.nc  MIROC-ESM_psl_past1000_r1i1p1_regridded.nc
HadGEM2-ES_psl_past1000_r1i1p1_regridded.nc      MPI-ESM-P_psl_past1000_r1i1p1_regridded.nc
"""

def normalise(input):
    out = (input - np.nanmin(input))/(np.nanmax(input) - np.nanmin(input))
    return out

psl_dir = '/data/NAS-ph290/ph290/cmip5/last1000/psl_regridded_winter/'

reykjavik_lat,reykjavik_lon = 64.1466,-21.9426
lisbon_lat,lisbon_lon = 38.7223,-9.1393

variable= 'psl'
nao_dict = []

for model in modelsb:
    print 'processing: '+model
    try:
    	cube = iris.load_cube(psl_dir+model+'*'+variable+'*.nc')
    except:
    	cube = iris.load(psl_dir+model+'*'+variable+'*.nc')
    	cube = cube[0]
    lat = cube.coord('latitude')
    lon = cube.coord('longitude')
    lat_coord1 = lat.nearest_neighbour_index(reykjavik_lat)
    lon_coord1 = lon.nearest_neighbour_index(reykjavik_lon)
    reykjavik_timeseries = normalise(cube.data[:,lat_coord1,lon_coord1])
    lat_coord1 = lat.nearest_neighbour_index(lisbon_lat)
    lon_coord1 = lon.nearest_neighbour_index(lisbon_lon)
    lisbon_timeseries = normalise(cube.data[:,lat_coord1,lon_coord1])
    nao_dict[model] = lisbon_timeseries - reykjavik_timeseries


for i,dummy in enumerate(modelsb):
    plt.plot(data[:,i],nao_dict[modelsb[i]])
    plt.show()


import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas as pd
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*r1i1p1*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


def smoothing(a,smoothing):
	b=np.zeros(len(a))
	b[:] = np.NAN
	tmp = np.mean(rolling_window(a, smoothing),-1)
	start_pos = int(np.floor((len(b) - len(tmp))/2.0))
	b[start_pos:start_pos+len(tmp)]=tmp
	return b



def calculate_correlations(models,var_name,my_dir,ts_year,ts_data):
	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	correlations = var
	for i,model in enumerate(models):
		print i
		#FGOALS-s2_psl_past1000_r1i1p1_regridded.nc
		c1 = iris.load_cube(my_dir + model+'_'+var_name+'_past1000_*r1i1p1*.nc')
		iris.coord_categorisation.add_year(c1, 'time', name='year')
		try:
			c1 = c1.aggregated_by('year', iris.analysis.MEAN)
		except:
			subprocess.call('cdo -P 8 yearmean '+my_dir + model+'_'+var_name+'_piControl_*.nc /data/dataSSD0/ph290/tmp/delete.nc', shell=True)
			c1 = iris.load_cube('/data/dataSSD0/ph290/tmp/delete.nc')
		# qplt.contourf(c1[0],31)
		# plt.gca().coastlines()
		# plt.show()
		model_years = c1.coord('year').points
		common_years = np.intersect1d(model_years,ts_year)
		common_years_locations = np.in1d(model_years,ts_year)
		c1 = c1[common_years_locations]
		ts_data_tmp = []
		for common_year in common_years:
			ts_data_tmp.append(ts_data[np.where(ts_year == common_year)[0][0]])
		try:
			missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
		except:
			missing_data = np.where(c1[0].data.data == 0.0)
		cube_data = c1.data
		cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
		# cube_data_detrended = np.apply_along_axis(smoothing, 0, cube_data_detrended)
		c1.data = cube_data_detrended
		#
		ts = ts_data_tmp
		# ts = rm.running_mean(ts,smoothing)
		# ts = smoothing(np.array(ts))
		# ts = ts[smoothing/2:-smoothing/2]
		cube = c1.copy()
		# cube = c1[smoothing/2:-smoothing/2].copy()
		cube = iris.analysis.maths.multiply(cube, 0.0)
		ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
		cube = iris.analysis.maths.add(cube, ts2)
		out_cube = iris.analysis.stats.pearsonr(c1, cube, corr_coords=['time'])
		# out_cube = iris.analysis.stats.pearsonr(c1[smoothing/2:-smoothing/2], cube, corr_coords=['time'])
		correlations[i] = out_cube.data
		try:
			subprocess.call('rm /data/dataSSD0/ph290/tmp/delete.nc', shell=True)
		except:
			pass
	return correlations


def ensemble_timeseries(models,var_name,my_dir,start_date,end_date):
	var = np.zeros([np.size(models),len(range(start_date,end_date))])
	var[:] = np.NAN
	for i,model in enumerate(models):
		print i
		try:
			cube = iris.load_cube(my_dir + model+'_'+var_name+'_past1000_*.nc')
		except:
			cube = iris.load(my_dir + model+'_'+var_name+'_past1000_*.nc')[0]
		iris.coord_categorisation.add_year(cube, 'time', name='year')
		iris.coord_categorisation.add_season(cube, 'time', name='season')
		cube = cube[np.where(cube.coord('season').points == 'jja')]
		cube = cube.aggregated_by(['year'], iris.analysis.MEAN)
		lon_west = 10
		lon_east = 20
		lat_south = 65
		lat_north = 75
		# lon_west = -28
		# lon_east = 10
		# lat_south = 65
		# lat_north = 80
		cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
		cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		print model
		# qplt.contourf(cube[0],21)
		# plt.gca().coastlines()
		# plt.show()
		try:
		    cube.coord('latitude').guess_bounds()
		except:
		    print 'cube already has latitude bounds'
		try:
		    cube.coord('longitude').guess_bounds()
		except:
		    print 'cube already has longitude bounds'
		grid_areas = iris.analysis.cartography.area_weights(cube)
		area_avged_cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		for j,year in enumerate(range(start_date,end_date)):
			loc = np.where(cube.coord('year').points == year)[0][0]
			var[i,j] = area_avged_cube[loc].data
	return var

dir_psl = '/data/NAS-ph290/ph290/cmip5/last1000/monthly/'

var_name = 'rsds'
# var_name = 'psl'
# var_name = 'pr'

# var_name = 'sic'

# m0 = model_names(dir_amoc,'msftmyz')
m1 = model_names(dir_psl,var_name)
# m2 = model_names(dir_tas,'tas')


models = m1
# models = np.intersect1d(m0,m1)



models = list(models)
# models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
# models.remove('CESM1-CAM5')
# models.remove('CESM1-FASTCHEM')
# models.remove('CESM1-WACCM')
# # models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
# models.remove('inmcm4')
# models.remove('MPI-ESM-LR')
# models.remove('MPI-ESM-P')
# models.remove('FGOALS-gl') # due to repetition of models
models.remove('FGOALS-s2') # due to repetition of models
# models.remove('bcc-csm1-1')
# models.remove('HadGEM2-ES')
models.remove('CSIRO-Mk3L-1-2')


print models

##########################################
# fennoscandinavia d13C
##########################################
start_year = 1000
end_year = 1850

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]


loc = np.where((np.logical_not(np.isnan(s_d))) & (s_y >= start_year) & (s_y <= end_year))
s_y = s_y[loc[0]]
s_d = s_d[loc[0]]
s_d=scipy.signal.detrend(s_d)
# s_d = scipy.signal.filtfilt(b1, a1, s_d)
# s_d = scipy.signal.filtfilt(b2, a2, s_d)

s_d -= np.mean(s_d)
s_d /= np.std(s_d)

##########################################
# End fennoscandinavia d13C
##########################################

# ##########################################
# # Mann AMO data
# ##########################################
# amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
# amo = np.genfromtxt(amo_file, skip_header = 2)
# amo_yr = amo[:,0]
# amo_data = amo[:,1]
# amo_data_minus_2sigma = amo[:,2]
# amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
# loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
# amo_yr = amo_yr[loc]
# amo_data = amo_data[loc]
# amo_data_sigma_diff = amo_data_sigma_diff[loc]
# #amo_data -= np.min(amo_data)
# #amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
# #amo_data /= np.max(amo_data)
# #amo_data_sigma_diff /= np.max(amo_data)
# amo_data=signal.detrend(amo_data)
# # amo_data = scipy.signal.filtfilt(b2, a2, amo_data)
#
# amo_data -= np.mean(amo_data)
# amo_data /= np.std(amo_data)
#
# ##########################################
# # End Mann AMO data
# ##########################################


mm_timeseries = ensemble_timeseries(models,var_name,dir_psl,start_year,end_year)



mmm_timeseries = np.nanmean(mm_timeseries,axis=0)

from scipy import signal



def butter_lowpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


def butter_highpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

# b, a = butter_lowpass(1.0/5.0, 1.0,2)
b2, a2 = butter_highpass(1.0/150, 1.0,2)


# y1 = smoothing(mmm_timeseries,5)
y1=mmm_timeseries
# y1 = scipy.signal.filtfilt(b, a,y1)
y1 = scipy.signal.filtfilt(b2, a2,y1)
y1 = smoothing(y1,5)
y1 -= np.nanmean(y1)
y1 /= np.nanstd(y1)

# y2 = smoothing(s_d,5)
y2=s_d
# y2 = scipy.signal.filtfilt(b, a,y2)
y2 = scipy.signal.filtfilt(b2, a2,y2)
y2 = smoothing(y2,5)
y2 -= np.nanmean(y2)
y2 /= np.nanstd(y2)

plt.plot(range(start_year,end_year),y1*(1.0),'b',alpha=0.5)
plt.plot(s_y,y2,'g',alpha=0.5)
plt.show()

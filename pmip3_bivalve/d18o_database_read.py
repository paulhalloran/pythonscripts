import numpy as np
import iris
import iris.coord_categorisation


file = '/data/NAS-geo01/ph290/misc_data/dbO18.txt'

with open(file) as f:
    content = f.readlines()


content = content[3::]

lat = np.zeros(np.size(content))
lat[:] = np.NAN
lon = lat.copy()
year = lat.copy()
depth = lat.copy()
month = lat.copy()
d18o = lat.copy()
t = lat.copy()
s = lat.copy()

for i,line in enumerate(content):
    lon[i] = float(line[0:7])
    lat[i] = float(line[7:13])
    year[i] = int(line[15:19])
    month[i] = int(line[14:15])
    depth[i] =  float(line[20:24])
    t[i] = float(line[24:30])
    s[i] = float(line[30:36])
    d18o[i] = float(line[36:42])


year[np.where(year == -999.0)] = np.NAN
lon[np.where(lon == -999.0)] = np.NAN
lat[np.where(lat == -999.0)] = np.NAN
month[np.where(month == -999.0)] = np.NAN
depth[np.where(depth == -999.0)] = np.NAN
t[np.where(t == -999.0)] = np.NAN
s[np.where(s == -999.0)] = np.NAN
d18o[np.where(d18o == -999.0)] = np.NAN



#Creating an iris cube to hola ll of this:

time = iris.coords.DimCoord(range(0, 12*31, 31), standard_name='time', units='days since 2000-01-01 00:00')
latitude = iris.coords.DimCoord(range(-90, 90, 1), standard_name='latitude', units='degrees')
longitude = iris.coords.DimCoord(range(0, 360, 1), standard_name='longitude', units='degrees')
cube = iris.cube.Cube(np.zeros((12,180, 360), np.float32), long_name='delta 18O seawater', var_name='d18O',dim_coords_and_dims=[(time,0), (latitude, 1), (longitude, 2)])
# cube.data[:] = np.NAN
iris.coord_categorisation.add_month(cube, 'time', name='month')
iris.coord_categorisation.add_season_year(cube, 'time', name='season_year')
iris.coord_categorisation.add_season_number(cube, 'time', name='season_number')

cube_data = cube.data.copy()
cube_data_count = cube_data.copy()
# cube_data_count[:] = 0

for i,mon in enumerate(month):
    lat_tmp = int(np.round(lat[i]))
    lon_tmp = int(np.round(lon[i]))
    d18o_tmp = d18o[i]
    mon = int(mon) -1
    if ((np.isfinite(mon)) & (np.isfinite(lat_tmp)) & (np.isfinite(lon_tmp)) & (np.isfinite(d18o_tmp)) & (d18o_tmp >= -1.0)):
        cube_data[mon,lat_tmp+89,lon_tmp+179] += d18o_tmp
        cube_data_count[mon,lat_tmp+89,lon_tmp+179] += 1.0




cube_data = cube_data / cube_data_count

cube_data = np.ma.masked_where(np.logical_not(np.isfinite(cube_data)),cube_data)

cube.data = np.roll(cube_data.copy(),180,axis = 2)

cube_seasonal = cube.aggregated_by(['season_number'], iris.analysis.MEAN)
# cube = cube.collapsed('time',iris.analysis.MEAN)

plt.close('all')
plt.figure(0)
qplt.pcolormesh(cube_seasonal[0],vmin=-0.3,vmax=0.3)
plt.gca().coastlines()
plt.show(block = False)
plt.figure(1)
qplt.pcolormesh(cube_seasonal[1],vmin=-0.3,vmax=0.3)
plt.gca().coastlines()
plt.show(block = False)
plt.figure(2)
qplt.pcolormesh(cube_seasonal[2],vmin=-0.3,vmax=0.3)
plt.gca().coastlines()
plt.show(block = False)
plt.figure(3)
qplt.pcolormesh(cube_seasonal[3],vmin=-0.3,vmax=0.3)
plt.gca().coastlines()
plt.show(block = False)



lon_west = -100.0
lon_east = 20
lat_south = 60.0
lat_north = 90.0

cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

try:
    cube_region.coord('longitude').guess_bounds()
except:
    print 'cube already has longitude bounds'


try:
    cube_region.coord('latitude').guess_bounds()
except:
    print 'cube already has latitude bounds'

grid_areas = iris.analysis.cartography.area_weights(cube_region)
area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

plt.scatter(range(1,13),area_avged_cube.data)
plt.title('monthly regional average')
plt.show(block = False)

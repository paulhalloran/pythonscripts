"""
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
import numpy as np
import iris
import matplotlib.pyplot as plt
import glob
from scipy import signal
import scipy
import numpy.ma as ma
import os
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
import running_mean as rm
from scipy.signal import freqz
from scipy.signal import butter, lfilter
import iris
import iris.coord_categorisation




def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


start_year = 950
end_year = 1750

smoothing = 5

# b1, a1 = butter_lowpass(1.0/smoothing, 1.0,6)
b2, a2 = butter_highpass(1.0/smoothing, 1.0,6)




#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory,variable):
	files = glob.glob(directory+'/*'+variable+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


input_directory = '/data/NAS-ph290/ph290/cmip5/last1000/'

variables = np.array(['tos'])
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.

'''
#Main bit of code follows...
'''


##########################################
#Volcanic forcing
##########################################


expected_years = np.arange(start_year,end_year)

file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

# data1 = np.genfromtxt(file1)
# data2 = np.genfromtxt(file2)

data1 = np.genfromtxt(file2)
data2 = np.genfromtxt(file4)

data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]
data_tmp[:,1] = data2[:,1]

data = np.mean(data_tmp,axis = 1)
data_final = data1.copy()
data_final[:,1] = data

volc_years = data_final[:,0]

loc = np.where((volc_years >= 850) & (volc_years <= 1849))[0]
volc_years = data_final[loc,0]
volc_data = data_final[loc,1]

volc_data_yrly = expected_years.copy() * np.nan

for i,yr in enumerate(expected_years):
	loc = np.where(np.floor(volc_years) == yr)
	if np.size(loc) > 0:
		volc_data_yrly[i] = np.nanmean(volc_data[loc[0]])




#rest

models = model_names(input_directory,variables[0])


lon_west1 = -75.0+360
lon_east1 = -7.5+360
lat_south1 = 0.0
lat_north1 = 60.0

region1 = iris.Constraint(longitude=lambda v: lon_west1 <= v <= lon_east1,latitude=lambda v: lat_south1 <= v <= lat_north1)

models = list(models)
# models.remove('BNU-ESM')
# models.remove('CanESM2')
# models.remove('MIROC5')
# models.remove('MRI-CGCM3')
# models.remove('CSIRO-Mk3-6-0')
# models.remove('GFDL-CM3')
# models.remove('GFDL-ESM2G')
# models.remove('GFDL-ESM2M')
models.remove('FGOALS-gl')
import running_mean


cube1 = iris.load_cube(input_directory+models[0]+'*'+variables[0]+'*.nc')[0]

models2 = []
cubes_amo = []
cubes_n = []
ts_amo = []
ts_n = []

for model in models:
	print 'processing: '+model
	try:
		cube = iris.load_cube(input_directory+model+'*'+variables[0]+'*.nc')
	except:
		cube = iris.load(input_directory+model+'*'+variables[0]+'*.nc')
		cube = cube[0]
	if model == ('EC-EARTH' or 'MRI-CGCM3' or 'NorESM1-ME'):
		for i in range(cube.shape[0]):
			cube.data.mask[i] = cube1.data.mask
	tmp1 = cube.extract(region1)
	try:
		tmp1.coord('latitude').guess_bounds()
	except:
		print 'has bounds'
	try:
		tmp1.coord('longitude').guess_bounds()
	except:
		print 'has bounds'
	grid_areas = iris.analysis.cartography.area_weights(tmp1)
	cubes_amo.append(tmp1)
	ts_amo.append(tmp1.collapsed(['latitude','longitude'],iris.analysis.MEAN,weights=grid_areas))





##########################################
# Mann AMO data
##########################################
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)
amo_data_filtered = scipy.signal.filtfilt(b2, a2, amo_data.copy(),padlen=20)

amo_data -= np.mean(amo_data)
amo_data /= np.std(amo_data)

amo_data_filtered -= np.mean(amo_data_filtered)
amo_data_filtered /= np.std(amo_data_filtered)

amo_scr_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo_scr = np.genfromtxt(amo_scr_file, skip_header = 2)
amo_scr_yr = amo_scr[:,0]
amo_scr_data = amo_scr[:,1]
amo_scr_data_minus_2sigma = amo_scr[:,2]
amo_scr_data_sigma_diff = amo_scr_data_minus_2sigma + amo_scr_data
loc = np.where((np.logical_not(np.isnan(amo_scr_data_sigma_diff))) & (np.logical_not(np.isnan(amo_scr_data))) & (amo_scr_yr >= start_year) & (amo_scr_yr <= end_year))
amo_scr_yr = amo_scr_yr[loc]
amo_scr_data = amo_scr_data[loc]
amo_scr_data_sigma_diff = amo_scr_data_sigma_diff[loc]
#amo_scr_data -= np.min(amo_scr_data)
#amo_scr_data_sigma_diff-= np.min(amo_scr_data_sigma_diff)
#amo_scr_data /= np.max(amo_scr_data)
#amo_scr_data_sigma_diff /= np.max(amo_scr_data)
amo_scr_data=signal.detrend(amo_scr_data)
amo_scr_data_filtered = scipy.signal.filtfilt(b2, a2, amo_scr_data.copy(),padlen=0)

amo_scr_data -= np.mean(amo_scr_data)
amo_scr_data /= np.std(amo_scr_data)

amo_scr_data_filtered -= np.mean(amo_scr_data_filtered)
amo_scr_data_filtered /= np.std(amo_scr_data_filtered)



#
# mann_file = '/home/ph290/data0/misc_data/iamo_mann.dat'
# mann_data = np.genfromtxt(mann_file,skip_header = 4)
#
# mann_file2 = '/home/ph290/data0/misc_data/amoscr.txt'
# mann_data2 = np.genfromtxt(mann_file2)
#
"""

data= np.zeros([end_year-start_year,np.size(models)])
data[:] = np.nan
data2 = data.copy()
data3 = data.copy()
data4 = data.copy()

years = np.arange(start_year,end_year)
j=0
for i,ts in enumerate(ts_amo):
	print models[i]
	try:
		iris.coord_categorisation.add_year(ts, 'time', name='year')
	except:
		print 'year coord already exists'
	m_years = ts.coord('year').points
	for j,m_yr in enumerate(m_years):
		loc = np.where(years == m_yr)
		if np.size(loc) > 0:
			data[loc[0][0],i] = ts.data[j]


for i,ts in enumerate(ts_amo):
	tmp = signal.detrend(data[np.where(np.isfinite(data[:,i]))[0],i])
	tmp_norm = tmp.copy()
	tmp_norm -= np.mean(tmp_norm)
	tmp_norm /= np.std(tmp_norm)
	tmp_filtered = scipy.signal.filtfilt(b2, a2, tmp.copy(),padlen=0)
	tmp_filtered_norm = tmp_filtered.copy()
	tmp_filtered_norm -= np.mean(tmp_filtered_norm)
	tmp_filtered_norm /= np.std(tmp_filtered_norm)
	data2[np.where(np.isfinite(data[:,i]))[0],i] = tmp
	data3[np.where(np.isfinite(data[:,i]))[0],i] = tmp_norm
	data4[np.where(np.isfinite(data[:,i]))[0],i] = tmp_filtered_norm



multimodel_mean = scipy.signal.filtfilt(b2, a2, np.nanmean(data3,axis = 1).copy(),padlen=0)
multimodel_max = scipy.signal.filtfilt(b2, a2, np.nanmax(data3,axis = 1).copy(),padlen=0)
multimodel_min = scipy.signal.filtfilt(b2, a2, np.nanmin(data3,axis = 1).copy(),padlen=0)
multimodel_max = np.nanmax(data3,axis = 1)
multimodel_min = np.nanmin(data3,axis = 1)

#####################
# plot              #
#####################


plt.close('all')
fig = plt.subplots(1,1,figsize=(9, 3))

ax1 = plt.subplot(1,1,1)


x_fill = np.concatenate([years,np.flipud(years)])
y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)
#ax2 = ax1.twinx()

ax1.plot(amo_yr,amo_data_filtered,'k',linewidth = 2, alpha=0.5,label = 'Mann et al., AMO index')
ax1.plot(amo_scr_yr,amo_scr_data_filtered,'k--',linewidth = 2, alpha=0.5,label = 'Mann et al., AMO index (screened)')
ax1.set_ylabel('N. Atlantic SST (normalised))')

ax1.plot(years,multimodel_mean,'b',linewidth = 2, alpha=0.5,label = 'CMIP5 multimodel mean')

plt.legend(loc=2,ncol=2,prop={'size':12}).draw_frame(False)


ax2 = ax1.twinx()
ax2.plot(years,volc_data_yrly,color = '0.5',alpha=0.8,label='Volcanic index')
# ax2.plot(expected_years,volc_data_yrly2,'k',alpha=0.8)
plt.legend(loc = 3,prop={'size':12}).draw_frame(False)
ax2.set_ylabel('Volcanic index')


#ax1.set_ylabel('blue = CMIP5')
# ax1.set_ylim([-0.5,0.5])
# ax1.set_xlim([1300,1850])
ax1.set_ylim([-4,4])
ax2.set_ylim([0,1])

# ax1.plot([1300,1850],[0,0],'k')
ax1.set_xlabel('year')

plt.savefig('/home/ph290/Documents/figures/multi_model_mean_preindustrial_AMO_renolds_mann_2.png')
plt.show(block = False)

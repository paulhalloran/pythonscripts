"""
import iris.plot as iplt
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import iris
import glob
import iris.experimental.concatenate
import iris.analysis
import iris.quickplot as qplt
import iris.analysis.cartography
import cartopy.crs as ccrs
import subprocess
from iris.coords import DimCoord
import iris.coord_categorisation
import matplotlib as mpl
import gc
import scipy
import scipy.signal as signal
from scipy.signal import kaiserord, lfilter, firwin, freqz
import monthly_to_yearly as m2yr
from matplotlib import mlab
import matplotlib.mlab as ml
import cartopy
import running_mean
import matplotlib.cm as mpl_cm
from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta


def model_names(directory,var):
        files = glob.glob(directory+'/*_'+var+'_*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


#palaeo_amo_prop_fig4.py


''''
#models
''''

template_cube = iris.load_cube(directory+'HadCM3'+'_tas_past1000_r1i1p1_regridded.nc')
template_cube = template_cube.collapsed('depth',iris.analysis.MEAN)

coord = template_cube.coord('time')
dt = coord.units.num2date(coord.points)
expected_years = np.array([coord.units.num2date(value).year for value in coord.points])

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


models = model_names(directory,'tas')
models = list(models)
models.remove('FGOALS-gl')
models.remove('HadGEM2-ES')


mm_array = np.empty([np.size(models),np.size(expected_years),180,360]) * np.nan

for i,model in enumerate(models):
	print model
	# model = models[0]
	cube = iris.load_cube(directory+model+'_tas_past1000_r1i1p1_regridded.nc')
	cube_data = cube.data
	cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
	cube.data = cube_data_detrended
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	for j,yr in enumerate(expected_years):
# 		print yr
		loc = np.where(year == yr)
		if np.size(loc) > 0:
			mm_array[i,j,:,:] = cube[loc[0][0],:,:].data



mm_mean_tas = np.nanmean(mm_array,axis=0)

template_cube.data = mm_mean_tas

"""
			
'''
#Volcanic forcing
'''

file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

# data1 = np.genfromtxt(file1)
# data2 = np.genfromtxt(file2)

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)

data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]
data_tmp[:,1] = data2[:,1]

data = np.mean(data_tmp,axis = 1)
data_final = data1.copy()
data_final[:,1] = data

volc_years = data_final[:,0]

loc = np.where((volc_years >= 850) & (volc_years <= 1849))[0]
volc_years = data_final[loc,0]
volc_data = data_final[loc,1]

volc_data_yrly = expected_years.copy() * np.nan

for i,yr in enumerate(expected_years):
	loc = np.where(np.floor(volc_years) == yr)
	if np.size(loc) > 0:
		volc_data_yrly[i] = np.nanmean(volc_data[loc[0]])


# plt.plot(volc_data_yrly)


volc_yrs = np.where(volc_data_yrly > 0.075)




plt.close('all')

plt.figure(figsize=(18, 10), dpi=100)
for i in range(4*5):
	print 'plotting '+str(i)
	yr = i*2
	index = volc_yrs[0]+yr
	index = index[np.where(index < np.shape(template_cube)[0])]
	template_cube2 = template_cube.copy()[0:np.size(index)]
	template_cube2.data = template_cube.data[index,:,:]
	plt.subplot(4, 5, i+1)
	iplt.contourf(template_cube2.collapsed('time',iris.analysis.MEAN) - template_cube.collapsed('time',iris.analysis.MEAN),np.linspace(-0.2,0.2,31),extend='both')
	plt.gca().coastlines()
	plt.title(yr)


plt.savefig('/home/ph290/Documents/figures/tas_response_to_volcanoes_lag.png')
plt.show(block =False)






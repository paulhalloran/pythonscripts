

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation
import numpy as np
import glob
import scipy
from scipy import signal
import os

def detrend_cube(cube):
    cube2 = cube.collapsed('time',iris.analysis.MEAN)
    cube_data = cube.data
    cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    cube.data = cube_data_detrended
    return cube+cube2

def model_names(directory,var):
	files = glob.glob(directory+'*'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models

def extract_region(cube,lon_west,lon_east,lat_south,lat_north):
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    return cube_region_tmp.intersection(latitude=(lat_south, lat_north))


directory = '/data/NAS-ph290/ph290/cmip5/historicalNat/regridded/'

models1 = list(model_names(directory,'tos'))
models2 = list(model_names(directory,'sic'))
models = list(np.intersect1d(models1,models2))
models.remove('GFDL-CM3')
models.remove('GFDL-ESM2M')
# models.remove('BNU-ESM')
models.remove('CSIRO-Mk3-6-0') #does not do tos under seaice


# for models in models_all:
#     models = [models]


lon_west = -180.0
lon_east = 180.0
lat_south = 45.0
lat_north = 90.0

#####################
# sic
#####################

cube = iris.load_cube(directory+models[1]+'_sic_historicalNat_r1i1p1_regridded.nc')
reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)

reg_cube_shape = np.shape(reg_cube)
data1 = np.zeros([len(models),reg_cube_shape[1],reg_cube_shape[2]])
data1[:] = np.nan
data_sic = {}
data_sic[0] = data1.copy()
data_sic[1] = data1.copy()
data_sic[2] = data1.copy()
data_sic[3] = data1.copy()



for i,model in enumerate(models):
    print model
    cube = iris.load_cube(directory+model+'_sic_historicalNat_r1i1p1_regridded.nc')
    iris.coord_categorisation.add_year(cube, 'time', name='year')
    iris.coord_categorisation.add_season(cube, 'time', name='season')
    # iris.coord_categorisation.add_month(cube, 'time', name='month')
    # cube = cube[np.where(cube.coord('year').points <= 1850)]
    reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
    # reg_cube = reg_cube[np.where(cube.coord('month').points == 'Mar')]
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    # reg_cube = detrend_cube(reg_cube)
    loc1 = np.where(((reg_cube.coord('year').points >= 1983) & (reg_cube.coord('year').points <= 1986)) | ((reg_cube.coord('year').points >= 1992) & (reg_cube.coord('year').points <= 1995)))
    # loc1 = np.where(((reg_cube.coord('year').points >= 1985) & (reg_cube.coord('year').points <= 1988)) | ((reg_cube.coord('year').points >= 1994) & (reg_cube.coord('year').points <= 1997)))

    loc2 = np.where(((reg_cube.coord('year').points >= 1977) & (reg_cube.coord('year').points <= 1981)) | ((reg_cube.coord('year').points >= 1986) & (reg_cube.coord('year').points <= 1990)) | ((reg_cube.coord('year').points >= 2000) & (reg_cube.coord('year').points <= 2005)))
    loc3 = np.append(loc1,loc2)
    #
    # loc2 = np.where((reg_cube.coord('year').points >= 1200) & (reg_cube.coord('year').points < 1600))
    # loc3 = np.where((reg_cube.coord('year').points >= 1600))
    # loc4 = np.where((reg_cube.coord('year').points >= 0))
    # locs = [loc1,loc2,loc3,loc4]
    locs = [loc1,loc2,loc3]
    for j,loc in enumerate(locs):
        tmp_data1 = reg_cube[loc].collapsed('time',iris.analysis.MEAN)
        try:
            fill_value = tmp_data1.data.fill_value
        except:
            fill_value = np.nan
        data_tmp = tmp_data1.data
        try:
            data_tmp[np.where(data_tmp.data == fill_value)] = np.nan
            data_tmp[np.where(data_tmp.data > 100.0)] = np.nan
            data_tmp[np.where(data_tmp.data == 0.0)] = np.nan
        except:
            data_tmp[np.where(data_tmp == fill_value)] = np.nan
            data_tmp[np.where(data_tmp > 100.0)] = np.nan
            data_tmp[np.where(data_tmp == 0.0)] = np.nan
        data_sic[j][i,:,:] = data_tmp




#####################
# tos
#####################

cube = iris.load_cube(directory+models[7]+'_tos_historicalNat_r1i1p1_regridded.nc')
reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)

reg_cube_shape = np.shape(reg_cube)
data1 = np.zeros([len(models),reg_cube_shape[1],reg_cube_shape[2]])
data1[:] = np.nan
data_tos = {}
data_tos[0] = data1.copy()
data_tos[1] = data1.copy()
data_tos[2] = data1.copy()
data_tos[3] = data1.copy()

for i,model in enumerate(models):
    print model
    cube = iris.load_cube(directory+model+'_tos_historicalNat_r1i1p1_regridded.nc')
    iris.coord_categorisation.add_year(cube, 'time', name='year')
    iris.coord_categorisation.add_season(cube, 'time', name='season')
    # iris.coord_categorisation.add_month(cube, 'time', name='month')
    # cube = cube[np.where(cube.coord('year').points <= 1850)]
    reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
    reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
    reg_cube = reg_cube.aggregated_by(['year'], iris.analysis.MEAN)
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    # reg_cube = reg_cube[np.where(cube.coord('season').points == 'mam')]
    # reg_cube = reg_cube[np.where(cube.coord('month').points == 'Mar')]
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    # reg_cube = detrend_cube(reg_cube)
    loc1 = np.where(((reg_cube.coord('year').points >= 1983) & (reg_cube.coord('year').points <= 1986)) | ((reg_cube.coord('year').points >= 1992) & (reg_cube.coord('year').points <= 1995)))
    # loc1 = np.where(((reg_cube.coord('year').points >= 1985) & (reg_cube.coord('year').points <= 1988)) | ((reg_cube.coord('year').points >= 1994) & (reg_cube.coord('year').points <= 1997)))

    loc2 = np.where(((reg_cube.coord('year').points >= 1977) & (reg_cube.coord('year').points <= 1981)) | ((reg_cube.coord('year').points >= 1986) & (reg_cube.coord('year').points <= 1990)) | ((reg_cube.coord('year').points >= 2000) & (reg_cube.coord('year').points <= 2005)))
    loc3 = np.append(loc1,loc2)
    #
    # loc2 = np.where((reg_cube.coord('year').points >= 1200) & (reg_cube.coord('year').points < 1600))
    # loc3 = np.where((reg_cube.coord('year').points >= 1600))
    # loc4 = np.where((reg_cube.coord('year').points >= 0))
    # locs = [loc1,loc2,loc3,loc4]
    locs = [loc1,loc2,loc3]
    for j,loc in enumerate(locs):
        tmp_data1 = reg_cube[loc].collapsed('time',iris.analysis.MEAN)
        try:
            fill_value = tmp_data1.data.fill_value
        except:
            fill_value = np.nan
        data_tmp = tmp_data1.data
        try:
            data_tmp[np.where(data_tmp.data == fill_value)] = np.nan
            data_tmp[np.where(data_tmp.data > 300.0)] = np.nan
            data_tmp[np.where(data_tmp.data < -10.0)] = np.nan
        except:
            data_tmp[np.where(data_tmp == fill_value)] = np.nan
            data_tmp[np.where(data_tmp > 300.0)] = np.nan
            data_tmp[np.where(data_tmp < -10.0)] = np.nan
        data_tos[j][i,:,:] = data_tmp


##########
# plotting
##########



for j,model in enumerate(models):
    print model
    plotting_cubes={}
    plotting_cubes[0] = template_cube.copy()
    plotting_cubes[1] = template_cube.copy()
    plotting_cubes[2] = template_cube.copy()
    plotting_cubes2={}
    plotting_cubes2[0] = template_cube.copy()
    plotting_cubes2[1] = template_cube.copy()
    plotting_cubes2[2] = template_cube.copy()

    for i in range(3):
        # tmp_data = np.nanmean(data[i],axis=0)
        tmp_data = data_tos[i][j]
        tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
        plotting_cubes[i].data =tmp_data

    for i in range(3):
        # tmp_data = np.nanmean(data[i],axis=0)
        tmp_data = data_sic[i][j]
        tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
        plotting_cubes2[i].data =tmp_data




    # for k,dummy in enumerate(models):
    #     for i in range(3):
    #         tmp_data = data[i][k]
    #         tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    #         plotting_cubes[i].data =tmp_data

    import iris
    import iris.plot as iplt
    import iris.quickplot as qplt
    import matplotlib.pyplot as plt
    import numpy as np
    import cartopy.crs as ccrs
    import iris.analysis.cartography
    import cartopy.feature as cfeature

    min_color_value = -0.5
    max_color_value = 0.5
    min_color_value2 = -3
    max_color_value2 = 15.0
    cmap = plt.get_cmap('YlGnBu')
    cmap2 = plt.get_cmap('seismic')

    my_extent = [-50, 50, 45, 90]
    no_levs=101

    plt.close('all')
    fig = plt.figure(figsize=(16,4))

    ax0 = plt.subplot(131,projection=ccrs.PlateCarree())
    # ax0 = plt.subplot(131,projection=ccrs.Mollweide())
    ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[2]-273.15,np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
    my_plot2 = iplt.contour(plotting_cubes2[2],np.linspace(0,100,no_levs/10.0),colors='k')
    ax0.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('full climatology')

    ax1 = plt.subplot(132,projection=ccrs.PlateCarree())
    ax1.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[2],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[0]-plotting_cubes2[2],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax1.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('post-volcano')

    ax2 = plt.subplot(133,projection=ccrs.PlateCarree())
    ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[2],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[1]-plotting_cubes2[2],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax2.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('pre-volcano')

    # ax3 = plt.subplot(133,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
    # ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
    # # my_plot = iplt.contourf(plotting_cubes[2]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    # my_plot = iplt.contourf(plotting_cubes[2],np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
    # ax3.add_feature(cfeature.LAND)
    # plt.gca().coastlines('10m')
    # bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    # bar.set_label('tos')
    # plt.title('phase 3')

    # ax3 = plt.subplot(224,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
    # ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    # ax3.add_feature(cfeature.LAND)
    # plt.gca().coastlines('10m')
    # bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    # bar.set_label('tos')
    # plt.title('full climatology')

    # plt.savefig('/home/ph290/Documents/figures/model'+models[0]+'.png')
    # plt.savefig('/home/ph290/Documents/figures/hist_nat_volc_tos_composites.png')
    plt.show(block = True)


####
# multi model means
###

plotting_cubes={}
plotting_cubes[0] = template_cube.copy()
plotting_cubes[1] = template_cube.copy()
plotting_cubes[2] = template_cube.copy()
plotting_cubes2={}
plotting_cubes2[0] = template_cube.copy()
plotting_cubes2[1] = template_cube.copy()
plotting_cubes2[2] = template_cube.copy()

for i in range(3):
    tmp_data = np.nanmean(data_tos[i],axis=0)
    # tmp_data = data_tos[i][j]
    tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    plotting_cubes[i].data =tmp_data

for i in range(3):
    tmp_data = np.nanmean(data_sic[i],axis=0)
    # tmp_data = data_sic[i][j]
    tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    plotting_cubes2[i].data =tmp_data




# for k,dummy in enumerate(models):
#     for i in range(3):
#         tmp_data = data[i][k]
#         tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
#         plotting_cubes[i].data =tmp_data

import iris
import iris.plot as iplt
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import iris.analysis.cartography
import cartopy.feature as cfeature

min_color_value = -0.5
max_color_value = 0.5
min_color_value2 = -3
max_color_value2 = 15.0
cmap = plt.get_cmap('YlGnBu')
cmap2 = plt.get_cmap('seismic')

my_extent = [-50, 50, 45, 90]
no_levs=101

plt.close('all')
fig = plt.figure(figsize=(16,4))

ax0 = plt.subplot(131,projection=ccrs.PlateCarree())
# ax0 = plt.subplot(131,projection=ccrs.Mollweide())
ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[2]-273.15,np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
my_plot2 = iplt.contour(plotting_cubes2[2],np.linspace(0,100,no_levs/5.0),colors='k')
ax0.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('full climatology')

ax1 = plt.subplot(132,projection=ccrs.PlateCarree())
ax1.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[2],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
my_plot2 = iplt.contour(plotting_cubes2[0]-plotting_cubes2[2],np.linspace(-10,10,no_levs/5.0),colors='k')
ax1.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('post-volcano')

ax2 = plt.subplot(133,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[2],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
my_plot2 = iplt.contour(plotting_cubes2[1]-plotting_cubes2[2],np.linspace(-10,10,no_levs/5.0),colors='k')
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('pre-volcano')

# ax3 = plt.subplot(133,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
# ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
# # my_plot = iplt.contourf(plotting_cubes[2]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
# my_plot = iplt.contourf(plotting_cubes[2],np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
# ax3.add_feature(cfeature.LAND)
# plt.gca().coastlines('10m')
# bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
# bar.set_label('tos')
# plt.title('phase 3')

# ax3 = plt.subplot(224,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
# ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
# ax3.add_feature(cfeature.LAND)
# plt.gca().coastlines('10m')
# bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
# bar.set_label('tos')
# plt.title('full climatology')

# plt.savefig('/home/ph290/Documents/figures/model'+models[0]+'.png')
plt.savefig('/home/ph290/Documents/figures/hist_nat_volc_tos_composites_with ice.png')
plt.show(block = True)

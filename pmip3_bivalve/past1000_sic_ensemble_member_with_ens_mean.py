
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
import scipy.stats as stats
import pandas as pd

#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory,variable):
	files = glob.glob(directory+'/*'+variable+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


#################
# correlations
#################




def smoothed_correlations(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_not_sig = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
	        smoothing1 = np.int(smoothing1)
	        print smoothing_no1,' out of ',np.size(smoothings1)
	        window_type = 'boxcar'
	        for smoothing_no2, smoothing2 in enumerate(smoothings2):
	                smoothing2 = np.int(smoothing2)
	                x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
	                y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
	                loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
	                x = x[loc]
	                y = y[loc]
	                slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	                r2 = r_value**2
	                coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	                if p_value > 0.001:
	                        coeff_det_a_b_not_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_not_sig



'''
#Main bit of code follows...
'''

#N Hem
lon_west1 = 0
lon_east1 = 360
lat_south1 = 0.0
lat_north1 = 90.0

#Atl sector of Arctic
lon_west1 = -45.0
lon_east1 = 25.0
lat_south1 = 0.0
lat_north1 = 90.0

region1 = iris.Constraint(longitude=lambda v: lon_west1 <= v <= lon_east1,latitude=lambda v: lat_south1 <= v <= lat_north1)


variables = np.array(['sic'])
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.


input_directory2 = '/data/NAS-ph290/ph290/cmip5/last1000/monthly/'

modelsb = model_names(input_directory2,variables[0])

cube1 = iris.load_cube(input_directory2+modelsb[0]+'*'+variables[0]+'*.nc')[0]

modelbs2 = []
cubes_n_hem = []
ts_n_hem = []

for model in modelsb:
	print 'processing: '+model
	try:
		cube = iris.load_cube(input_directory2+model+'*'+variables[0]+'*.nc')
	except:
		cube = iris.load(input_directory2+model+'*'+variables[0]+'*.nc')
		cube = cube[0]
	# iris.coord_categorisation.add_month(cube, 'time', name='month')
	# cube = cube[np.where(cube.coord('month').points == 'Mar')]
	iris.coord_categorisation.add_year(cube, 'time', name='year')
	# iris.coord_categorisation.add_season(cube, 'time', name='season')
	# iris.coord_categorisation.add_season_year(cube, 'time', name='season_year')
	# cube = cube.aggregated_by(['season_year','season'], iris.analysis.MEAN)
	# cube = cube[np.where(cube.coord('season').points == 'son')]
	cube = cube.aggregated_by(['year'], iris.analysis.MEAN)
	if model == ('MRI-CGCM3'):
		for i in range(cube.shape[0]):
			cube.data.mask[i] = cube1.data.mask
	tmp1 = cube.extract(region1)
	cubes_n_hem.append(tmp1)
	#qplt.contourf(tmp1[0])
	#plt.show()
	ts_n_hem.append(tmp1.collapsed(['latitude','longitude'],iris.analysis.MEAN))



# reynolds_file = '/home/ph290/data0/reynolds/ultra_data.csv'
reynolds_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
reynolds_data = np.genfromtxt(reynolds_file,skip_header = 1,delimiter = ',')

tmp = np.shape(cubes_n_hem[0].data)
# tmp_data = np.ma.empty([np.size(cubes_n_hem),tmp[0],tmp[1],tmp[2]])
tmp_data = np.ma.empty([np.size(cubes_n_hem),1000,tmp[1],tmp[2]])

for i in np.arange(np.size(cubes_n_hem)):
	tmp_data[i] = cubes_n_hem[i].data[0:1000]



coord = ts_n_hem[0].coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])



data = np.zeros([1000,np.size(ts_n_hem)])
j=0
for i,ts in enumerate(ts_n_hem):
	print modelsb[i]
	data[:,j] = signal.detrend(ts.data[0:1000])
	# data[:,j] = (ts.data[0:1000])
	j += 1

multimodel_mean = data.mean(axis = 1)
multimodel_max = data.max(axis = 1)
multimodel_min = data.min(axis = 1)



###########
# plotting_correlations with multi model mean
###########

def smoothed_correlations2(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_all = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
		smoothing1 = np.int(smoothing1)
		print smoothing_no1,' out of ',np.size(smoothings1)
		window_type = 'boxcar'
		for smoothing_no2, smoothing2 in enumerate(smoothings2):
			smoothing2 = np.int(smoothing2)
			x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
			y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
			loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
			x = x[loc]
			y = y[loc]
			slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
			r2 = r_value**2
			coeff_det_a_b_all[smoothing_no1,smoothing_no2] = r_value
			if p_value < 0.001:
				coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_all

smoothings1 = np.linspace(2,102,26)
smoothings2 = smoothings1

plt.close('all')


for i in range(np.shape(data)[1]):
	corr = smoothed_correlations2(data[:,i],multimodel_mean,smoothings1,smoothings2)
	coeff_det_a_b_sig = np.max(corr[0],axis=1)
	#this [np.max(corr[0],axis=1)] is calculating the maximum correlation for any smoothing of the ensmble member for a certain smopothing of the ensmble mean. axis=0 would be the other way round
	plt.scatter(smoothings1,coeff_det_a_b_sig,label=modelsb[i])


plt.legend()
plt.xlabel('smoothing window (years)')
plt.ylabel('maximum correlation between ensembel member\nand ensemble mean (R, p < 0.001)')
plt.savefig('/home/ph290/Documents/figures/ensemble_member_with_ensmeble_mean.png')
plt.show(block = False)

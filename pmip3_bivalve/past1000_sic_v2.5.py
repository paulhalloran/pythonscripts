
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
import scipy.stats as stats
import pandas as pd
import matplotlib.cm as mpl_cm
from matplotlib.patches import ConnectionPatch
from statsmodels.stats.outliers_influence import summary_table

#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory,variable):
	files = glob.glob(directory+'/*'+variable+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


#################
# correlations
#################




def smoothed_correlations(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_not_sig = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
	        smoothing1 = np.int(smoothing1)
	        print smoothing_no1,' out of ',np.size(smoothings1)
	        window_type = 'boxcar'
	        for smoothing_no2, smoothing2 in enumerate(smoothings2):
	                smoothing2 = np.int(smoothing2)
	                # x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
	                # y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
	                x = pd.DataFrame({'a':a.copy()}).rolling(smoothing1,win_type=window_type,center=True).sum().values
	                y = pd.DataFrame({'b':b.copy()}).rolling(smoothing2,win_type=window_type,center=True).sum().values
	                loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
	                x = x[loc]
	                y = y[loc]
	                slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	                r2 = r_value**2
	                coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	                if p_value > 0.001:
	                        coeff_det_a_b_not_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_not_sig




'''
#Main bit of code follows...
'''

#N Hem
lon_west1 = 0
lon_east1 = 360
lat_south1 = 0.0
lat_north1 = 90.0

#Atl sector of Arctic
lon_west1 = -45.0
lon_east1 = 25.0
lat_south1 = 0.0
lat_north1 = 90.0

region1 = iris.Constraint(longitude=lambda v: lon_west1 <= v <= lon_east1,latitude=lambda v: lat_south1 <= v <= lat_north1)


variables = np.array(['sic'])
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.


input_directory2 = '/data/NAS-ph290/ph290/cmip5/last1000/monthly/'

modelsb = model_names(input_directory2,variables[0])
modelsb = list(modelsb)
modelsb.remove('HadCM3') # just removing for now as now have HadGEM2-ES

cube1 = iris.load_cube(input_directory2+modelsb[0]+'*'+variables[0]+'*.nc')[0]

modelbs2 = []
cubes_n_hem = []
ts_n_hem = []

for model in modelsb:
	print 'processing: '+model
	try:
		cube = iris.load_cube(input_directory2+model+'*'+variables[0]+'*.nc')
	except:
		cube = iris.load(input_directory2+model+'*'+variables[0]+'*.nc')
		cube = cube[0]
	# iris.coord_categorisation.add_month(cube, 'time', name='month')
	# cube = cube[np.where(cube.coord('month').points == 'Mar')]
	iris.coord_categorisation.add_year(cube, 'time', name='year')
	cube=cube[np.where((cube.coord('year').points >= 950) & (cube.coord('year').points < 1850))]
	# iris.coord_categorisation.add_season(cube, 'time', name='season')
	# iris.coord_categorisation.add_season_year(cube, 'time', name='season_year')
	# cube = cube.aggregated_by(['season_year','season'], iris.analysis.MEAN)
	# cube = cube[np.where(cube.coord('season').points == 'son')]
	cube = cube.aggregated_by(['year'], iris.analysis.MEAN)
	if model == ('MRI-CGCM3'):
		for i in range(cube.shape[0]):
			cube.data.mask[i] = cube1.data.mask
	tmp1 = cube.extract(region1)
	cubes_n_hem.append(tmp1)
	#qplt.contourf(tmp1[0])
	#plt.show()
	ts_n_hem.append(tmp1.collapsed(['latitude','longitude'],iris.analysis.MEAN))



# reynolds_file = '/home/ph290/data0/reynolds/ultra_data.csv'
reynolds_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
reynolds_data = np.genfromtxt(reynolds_file,skip_header = 1,delimiter = ',')

tmp = np.shape(cubes_n_hem[0].data)
# tmp_data = np.ma.empty([np.size(cubes_n_hem),tmp[0],tmp[1],tmp[2]])
tmp_data = np.ma.empty([np.size(cubes_n_hem),900,tmp[1],tmp[2]])

for i in np.arange(np.size(cubes_n_hem)):
	tmp_data[i] = cubes_n_hem[i].data[0:900]



coord = ts_n_hem[0].coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])



data = np.zeros([900,np.size(ts_n_hem)])
j=0
for i,ts in enumerate(ts_n_hem):
	print modelsb[i]
	data[:,j] = signal.detrend(ts.data[0:900])
	# data[:,j] = (ts.data[0:1000])
	j += 1

multimodel_mean = data.mean(axis = 1)
multimodel_max = data.max(axis = 1)
multimodel_min = data.min(axis = 1)





##########
# correlations
##########

reynolds_data2 = np.flipud(reynolds_data)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)

smoothings1 = range(1,150,2)
smoothings2 = smoothings1
year2 = year[0]+np.arange(900)


loc1 = np.where((year2 <= 1200) & (year2 >= 960) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1200) & (reynolds_data2[:,0] >= 960) )[0]

phase1_corr_sig,phase1_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)

loc1 = np.where((year2 <= 1600) & (year2 >= 1200) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1600) & (reynolds_data2[:,0] >= 1200) )[0]

phase2_corr_sig,phase2_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)

loc1 = np.where((year2 <= 1850) & (year2 >= 1600) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1849) & (reynolds_data2[:,0] >= 1600) )[0]

phase3_corr_sig,phase3_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)



#####
# plotting
#####

my_linewidth = 0.8

plt.close('all')
plt.figure(figsize = (8,6.5))

#plt1

ax1 = plt.subplot2grid((3,3),(0,0),rowspan=1,colspan=3)

bins = [900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,1950]
x=[0.1,0.7,0.2,0.0,7.8,6.0,2.8,3.2,22.6,25.3,40.8,8.6] # duration of sea-ice (in weeks) * extend along N. Iceland coast
#not also https://www.jstor.org/tc/accept?origin=%2Fstable%2Fpdf%2F520132.pdf says cereal and grain cultivatoin ceased in north and beast before end of 12 century, and altogether near end of 16th C
ax1.step(bins,x,lw=3)
ax1.set_ylabel('Ice off Iceland')
# plt.text(1200, 32, 'cereal and grain\ncultivation ceased\nin North and East\n(Schell, 1961)', rotation=90)
# plt.text(1600, 47, 'cereal and grain\ncultivation ceased\naltogether\n(Schell, 1961)', rotation=90)

ax1.set_xlim([900,1900])
ax1.set_ylim([0.0,50])


########

# #plt 2
# ax2 = plt.subplot2grid((3,3),(1,0),rowspan=1,colspan=3)
#
# year2 = year[0]+np.arange(1000)
# # y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
# y = multimodel_mean-np.nanmean(multimodel_mean)
# y /= np.std(y)
# ax2.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean seaice')
# x_fill = np.concatenate([year2,np.flipud(year2)])
# y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# # ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)
#
# ax2b = ax2.twinx()
# # loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
#
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
#
# ax2.set_ylabel('sea-ice')
# ax2b.set_ylabel('bivalve $\delta^{18}$O')
# ax2b.set_xlim([900,1850])
# ax2.set_ylim([-4.0,4.0])
# ax2b.set_ylim([4.0,-4.0])
# # ax2.plot([900,1850],[0,0],'k')
#
#
#
# #plt 3
# ax3 = plt.subplot2grid((3,3),(2,0),rowspan=1,colspan=3)
#
# lab1 = ax3.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
# x_fill = np.concatenate([year2,np.flipud(year2)])
# y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
# # ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)
#
# # ax2 = ax1.twinx()
# # loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# # y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# # lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
# ax3b = ax3.twinx()
# # loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# loc = np.where((reynolds_data[:,0] <= 1600) & (reynolds_data[:,0] >= 1200) )[0]
#
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# # y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)
#
# y_reynolds2 /= np.std(y_reynolds2)
# lab2 = ax3b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve d18O')
#
# ax3.set_ylabel('sea-ice')
# ax3b.set_ylabel('bivalve $\delta^{18}$O')
# ax3.set_xlim([900,1850])
# ax3.set_ylim([-4.0,4.0])
# ax3b.set_ylim([-4.0,4.0])
# # ax3.plot([900,1850],[0,0],'k')
# ax3.set_xlabel('year')
#
# # ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# # ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)
# ax3.legend(prop={'size':10},loc=1).draw_frame(False)
# ax3b.legend(prop={'size':10},loc=4).draw_frame(False)



########

#plt 2
ax2 = plt.subplot2grid((3,42),(1,0),rowspan=1,colspan=12)

year2 = year[0]+np.arange(900)
# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b = ax2.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

ax2.set_ylabel('multi-model\nmean sea-ice')
ax2.yaxis.label.set_color('b')

# ax2b.set_ylabel('bivalve $\delta^{18}$O')
ax2b.set_xlim([900,1200])
ax2.set_ylim([-4.0,4.0])
ax2b.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off

ax2.spines['right'].set_visible(False)
ax2b.spines['right'].set_visible(False)

#plt 2_right
ax2_right = plt.subplot2grid((3,42),(1,30),rowspan=1,colspan=12)

# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2_right.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b_right = ax2_right.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# ax2_right.set_ylabel('sea-ice')
ax2b_right.set_ylabel('bivalve $\delta^{18}$O')
ax2b_right.yaxis.label.set_color('r')
ax2b_right.set_xlim([1600,1900])
ax2_right.set_ylim([-4.0,4.0])
ax2b_right.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=True,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax2b_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=True) # labels along the bottom edge are off

ax2_right.spines['left'].set_visible(False)
ax2b_right.spines['left'].set_visible(False)

# ax2_right.legend(prop={'size':10},loc=1).draw_frame(False)
# ax2b_right.legend(prop={'size':10},loc=4).draw_frame(False)

#plt 3
ax3 = plt.subplot2grid((3,42),(1,13),rowspan=1,colspan=16)
ax3.set_facecolor('none')

lab1 = ax3.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

# ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
ax3b = ax3.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1600) & (reynolds_data[:,0] >= 1200) )[0]

y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
lab2 = ax3b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve d18O')

# ax3.set_ylabel('sea-ice')
# ax3b.set_ylabel('bivalve $\delta^{18}$O')
ax3.set_xticklabels(['',1300,1400,1500,''])
ax3.set_xlim([1200,1600])
ax3.set_ylim([-4.0,4.0])
ax3b.set_ylim([-4.0,4.0])
# ax3.plot([900,1850],[0,0],'k')
ax3.set_xlabel('year')

# ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)

ax3.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3.spines['left'].set_visible(False)
ax3b.spines['left'].set_visible(False)
ax3.spines['right'].set_visible(False)
ax3b.spines['right'].set_visible(False)

ax3.text(1400, 3, '$\delta^{18}$O y-axis inverted',horizontalalignment='center',color='r')

# ax3.legend(prop={'size':10},loc=2).draw_frame(False)
# ax3b.legend(prop={'size':10},loc=3).draw_frame(False)

########

#plotting scatter subplots


def prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2):
	window_type = 'boxcar'
	loc1 = np.where((year2 <= max_yr) & (year2 >= min_yr))[0]
	loc2 = np.where((reynolds_data2[:,0] <= max_yr) & (reynolds_data2[:,0] >= min_yr) )[0]
	a_tmp = scipy.signal.detrend(y[loc1].copy())
	a_tmp /= np.nanstd(a_tmp)
	b_tmp = scipy.signal.detrend(reynolds_data2[:,2][loc2].copy())
	b_tmp /= np.nanstd(b_tmp)
	dates = pd.period_range(start='1/1/'+str(int(min_yr)), end='1/1/'+str(int(max_yr)),freq='Y')
	df = pd.DataFrame({'model':a_tmp,'bivalve':b_tmp,'Datetime':dates})
	df = df.set_index('Datetime')
	y_plot = df.model.rolling(smoothing1,win_type=window_type,center=True).mean().values
	x_plot = df.bivalve.rolling(smoothing2,win_type=window_type,center=True).mean().values
	loc = np.where(((np.isfinite(x_plot)) & (np.isfinite(y_plot))))
	x_plot = x_plot[loc]
	y_plot = y_plot[loc]
	x2_plot = sm.add_constant(x_plot)
	model = sm.OLS(y_plot,x2_plot)
	results = model.fit()
	x2 = np.linspace(np.min(x2_plot),np.max(x2_plot),np.size(x2_plot))
	y2 = results.predict(sm.add_constant(x2))
	st, data, ss2 = summary_table(results, alpha=0.01)
	fittedvalues = data[:,2]
	predict_mean_se = data[:,3]
	predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
	predict_ci_low, predict_ci_upp = data[:,6:8].T
	return x_plot,y_plot,predict_mean_ci_low, predict_mean_ci_upp,predict_ci_low, predict_ci_upp,fittedvalues

# smoothing1 = 12
smoothing1 = 20
smoothing2 = 20
range_min,range_max = -1.5,1.5
my_marker_size=5

min_yr = 960
max_yr = 1200

ax4 = plt.subplot2grid((3,42),(2,1),rowspan=1,colspan=11)
x_plot,y_plot,predict_mean_ci_low, predict_mean_ci_upp,predict_ci_low, predict_ci_upp,fittedvalues = prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2)
ax4.scatter(x_plot,y_plot, s=my_marker_size, edgecolors='b',alpha=0.8)
# ax4.plot(x_plot,results.params[1]*x_plot+results.params[0])
ax4.plot(x_plot, fittedvalues, 'r', lw=2,alpha=0.5)
# ax4.plot(x_plot, predict_ci_low, 'r--', lw=0.5)
# ax4.plot(x_plot, predict_ci_upp, 'r--', lw=0.5)
# ax4.plot(x_plot, predict_mean_ci_low, 'r--', lw=0.5) # note that this is the confidence interval on the regression, rather than the prediction interval - above
#I'm not sure if the confidence intervals are meaningful because the data is smoothed
# ax4.plot(x_plot, predict_mean_ci_upp, 'r--', lw=0.5)
ax4.set_xlim([range_min,range_max])
ax4.set_ylim([range_min,range_max])
ax4.set_xlabel('bivalve $\delta^{18}$O' )
ax4.set_ylabel('model sea-ice' )


min_yr = 1200
max_yr = 1600

ax5 = plt.subplot2grid((3,42),(2,15),rowspan=1,colspan=11)
x_plot,y_plot,predict_mean_ci_low, predict_mean_ci_upp,predict_ci_low, predict_ci_upp,fittedvalues = prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2)
ax5.scatter(x_plot,y_plot, s=my_marker_size, edgecolors='b',alpha=0.8)
# ax4.plot(x_plot,results.params[1]*x_plot+results.params[0])
ax5.plot(x_plot, fittedvalues, 'r', lw=2,alpha=0.5)
# ax4.plot(x_plot, predict_ci_low, 'r--', lw=0.5)
# ax4.plot(x_plot, predict_ci_upp, 'r--', lw=0.5)
# ax4.plot(x_plot, predict_mean_ci_low, 'r--', lw=0.5) # note that this is the confidence interval on the regression, rather than the prediction interval - above
#I'm not sure if the confidence intervals are meaningful because the data is smoothed
# ax4.plot(x_plot, predict_mean_ci_upp, 'r--', lw=0.5)
ax5.set_xlim([range_min,range_max])
ax5.set_ylim([range_min,range_max])
ax5.set_xlabel('bivalve $\delta^{18}$O' )
# ax5.set_ylabel('model sea-ice' )


min_yr = 1600
max_yr = 1849

ax6 = plt.subplot2grid((3,42),(2,31),rowspan=1,colspan=11)
x_plot,y_plot,predict_mean_ci_low, predict_mean_ci_upp,predict_ci_low, predict_ci_upp,fittedvalues = prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2)
ax6.scatter(x_plot,y_plot, s=my_marker_size, edgecolors='b',alpha=0.8)
# ax4.plot(x_plot,results.params[1]*x_plot+results.params[0])
ax6.plot(x_plot, fittedvalues, 'r', lw=2,alpha=0.5)
# ax4.plot(x_plot, predict_ci_low, 'r--', lw=0.5)
# ax4.plot(x_plot, predict_ci_upp, 'r--', lw=0.5)
# ax4.plot(x_plot, predict_mean_ci_low, 'r--', lw=0.5) # note that this is the confidence interval on the regression, rather than the prediction interval - above
#I'm not sure if the confidence intervals are meaningful because the data is smoothed
# ax4.plot(x_plot, predict_mean_ci_upp, 'r--', lw=0.5)
ax6.set_xlim([range_min,range_max])
ax6.set_ylim([range_min,range_max])
ax6.set_xlabel('bivalve $\delta^{18}$O' )
# ax6.set_ylabel('model sea-ice' )


'''
# plotting correlations #
import matplotlib.cm as mpl_cm

cmap1 = mpl_cm.get_cmap('bwr')
cmap2 = mpl_cm.get_cmap('bwr')


#plt 1
ax4 = plt.subplot2grid((3,42),(2,2),rowspan=1,colspan=10)

cont1 = ax4.contourf(smoothings1,smoothings2,phase1_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax4.contourf(smoothings1,smoothings2,phase1_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax4.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=1.0)
ax4.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=1.0)
ax4.set_xlim(0,100)
ax4.set_ylim(0,100)
ax4.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
ax4.set_ylabel('model sea-ice smoothing\n(years)')
# plt.colorbar(cont1)

#plt 2
ax5 = plt.subplot2grid((3,42),(2,16),rowspan=1,colspan=10)

cont1 = ax5.contourf(smoothings1,smoothings2,phase2_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax5.contourf(smoothings1,smoothings2,phase2_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax5.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=1.0)
ax5.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=1.0)
ax5.set_xlim(0,100)
ax5.set_ylim(0,100)
ax5.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
ax5.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=True,      # ticks along the bottom edge are off
    labelbottom=True, # labels along the bottom edge are off
    left=False,      # ticks along the bottom edge are off
    labelleft=False) # labels along the bottom edge are off

#plt 3
ax6 = plt.subplot2grid((3,42),(2,30),rowspan=1,colspan=10)

cont1 = ax6.contourf(smoothings1,smoothings2,phase3_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax6.contourf(smoothings1,smoothings2,phase3_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax6.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=1.0)
ax6.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=1.0)
ax6.set_xlim(0,100)
ax6.set_ylim(0,100)
ax6.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
ax6.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=True,      # ticks along the bottom edge are off
    labelbottom=True, # labels along the bottom edge are off
    left=False,      # ticks along the bottom edge are off
    labelleft=False) # labels along the bottom edge are off

'''

####
# grey boxes
####
import matplotlib

# ax8 = plt.subplot2grid((3,42),(0,12),rowspan=3,colspan=1)
# ax8.add_patch(matplotlib.patches.Rectangle((0,0), 100, 100, color="grey"))
from matplotlib.patches import Ellipse

# ax1.add_patch(plt.Rectangle((1185,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
# ax1.add_patch(plt.Rectangle((1592,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
ax1.add_patch(plt.Rectangle((1200,50),400, -170,facecolor='silver',
                              clip_on=False,linewidth = 0,alpha=0.2))
# ax1.add_patch(plt.Rectangle((900,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))
# ax1.add_patch(plt.Rectangle((1600,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))

#####
# colorbar
#####


# ax7 = plt.subplot2grid((3,42),(2,41),rowspan=1,colspan=1)
# cb1 = plt.colorbar(cont1, cax=ax7,extend='both',orientation='vertical')
# cb1.set_label('R')
# ax9.yaxis.set_ticks_position('left')
# ax9.yaxis.set_label_position('left')




plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations.png')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations.pdf')
plt.show(block = False)

print 'phase 1 correlation'
print np.nanmin(phase1_corr_sig)
print 'phase 2 correlation'
print np.nanmax(phase2_corr_sig)
print 'phase 3 correlation'
print np.nanmin(phase3_corr_sig)


"""
###################################################


#### NO HISTORICAL ICE DATA ####

plt.close('all')
plt.figure(figsize = (8,6.5))

#plt1

# ax1 = plt.subplot2grid((3,3),(0,0),rowspan=1,colspan=3)
#
# bins = [900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,1950]
# x=[0.1,0.7,0.2,0.0,7.8,6.0,2.8,3.2,22.6,25.3,40.8,8.6] # duration of sea-ice (in weeks) * extend along N. Iceland coast
# #not also https://www.jstor.org/tc/accept?origin=%2Fstable%2Fpdf%2F520132.pdf says cereal and grain cultivatoin ceased in north and beast before end of 12 century, and altogether near end of 16th C
# ax1.step(bins,x,lw=3)
# ax1.set_ylabel('Ice off Iceland')
# plt.text(1200, 32, 'cereal and grain\ncultivation ceased\nin North and East\n(Schell, 1961)', rotation=90)
# plt.text(1600, 47, 'cereal and grain\ncultivation ceased\naltogether\n(Schell, 1961)', rotation=90)
#
# ax1.set_xlim([900,1900])
# ax1.set_ylim([0.0,50])

#plt 2
ax2 = plt.subplot2grid((3,42),(1,0),rowspan=1,colspan=12)

year2 = year[0]+np.arange(900)
# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b = ax2.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

ax2.set_ylabel('multi-model\nmean sea-ice')
ax2.yaxis.label.set_color('b')

# ax2b.set_ylabel('bivalve $\delta^{18}$O')
ax2b.set_xlim([900,1200])
ax2.set_ylim([-4.0,4.0])
ax2b.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off

ax2.spines['right'].set_visible(False)
ax2b.spines['right'].set_visible(False)

#plt 2_right
ax2_right = plt.subplot2grid((3,42),(1,30),rowspan=1,colspan=12)

# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2_right.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b_right = ax2_right.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# ax2_right.set_ylabel('sea-ice')
ax2b_right.set_ylabel('bivalve $\delta^{18}$O')
ax2b_right.yaxis.label.set_color('r')
ax2b_right.set_xlim([1600,1900])
ax2_right.set_ylim([-4.0,4.0])
ax2b_right.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=True,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax2b_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=True) # labels along the bottom edge are off

ax2_right.spines['left'].set_visible(False)
ax2b_right.spines['left'].set_visible(False)

# ax2_right.legend(prop={'size':10},loc=1).draw_frame(False)
# ax2b_right.legend(prop={'size':10},loc=4).draw_frame(False)

#plt 3
ax3 = plt.subplot2grid((3,42),(1,13),rowspan=1,colspan=16)
ax3.set_facecolor('none')

lab1 = ax3.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

# ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
ax3b = ax3.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1600) & (reynolds_data[:,0] >= 1200) )[0]

y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
lab2 = ax3b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve d18O')

# ax3.set_ylabel('sea-ice')
# ax3b.set_ylabel('bivalve $\delta^{18}$O')
ax3.set_xticklabels(['',1300,1400,1500,''])
ax3.set_xlim([1200,1600])
ax3.set_ylim([-4.0,4.0])
ax3b.set_ylim([-4.0,4.0])
# ax3.plot([900,1850],[0,0],'k')
ax3.set_xlabel('year')

# ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)

ax3.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3.spines['left'].set_visible(False)
ax3b.spines['left'].set_visible(False)
ax3.spines['right'].set_visible(False)
ax3b.spines['right'].set_visible(False)

ax3.text(1400, 3, '$\delta^{18}$O y-axis inverted',horizontalalignment='center',color='r')

# ax3.legend(prop={'size':10},loc=2).draw_frame(False)
# ax3b.legend(prop={'size':10},loc=3).draw_frame(False)

########


# plotting correlations #
import matplotlib.cm as mpl_cm

cmap1 = mpl_cm.get_cmap('bwr')
cmap2 = mpl_cm.get_cmap('bwr')


#plt 1
ax4 = plt.subplot2grid((3,42),(2,2),rowspan=1,colspan=10)

cont1 = ax4.contourf(smoothings1,smoothings2,phase1_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax4.contourf(smoothings1,smoothings2,phase1_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax4.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
ax4.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
ax4.set_xlim(0,100)
ax4.set_ylim(0,100)
ax4.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
ax4.set_ylabel('model sea-ice smoothing\n(years)')
# plt.colorbar(cont1)

#plt 2
ax5 = plt.subplot2grid((3,42),(2,16),rowspan=1,colspan=10)

cont1 = ax5.contourf(smoothings1,smoothings2,phase2_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax5.contourf(smoothings1,smoothings2,phase2_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax5.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
ax5.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
ax5.set_xlim(0,100)
ax5.set_ylim(0,100)
ax5.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
ax5.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=True,      # ticks along the bottom edge are off
    labelbottom=True, # labels along the bottom edge are off
    left=False,      # ticks along the bottom edge are off
    labelleft=False) # labels along the bottom edge are off

#plt 3
ax6 = plt.subplot2grid((3,42),(2,30),rowspan=1,colspan=10)

cont1 = ax6.contourf(smoothings1,smoothings2,phase3_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax6.contourf(smoothings1,smoothings2,phase3_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax6.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
ax6.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
ax6.set_xlim(0,100)
ax6.set_ylim(0,100)
ax6.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
ax6.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=True,      # ticks along the bottom edge are off
    labelbottom=True, # labels along the bottom edge are off
    left=False,      # ticks along the bottom edge are off
    labelleft=False) # labels along the bottom edge are off


####
# grey boxes
####
import matplotlib

# ax8 = plt.subplot2grid((3,42),(0,12),rowspan=3,colspan=1)
# ax8.add_patch(matplotlib.patches.Rectangle((0,0), 100, 100, color="grey"))
from matplotlib.patches import Ellipse

# ax1.add_patch(plt.Rectangle((1185,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
# ax1.add_patch(plt.Rectangle((1592,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
ax1.add_patch(plt.Rectangle((1200,50),400, -170,facecolor='silver',
                              clip_on=False,linewidth = 0,alpha=0.2))
# ax1.add_patch(plt.Rectangle((900,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))
# ax1.add_patch(plt.Rectangle((1600,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))

#####
# colorbar
#####


ax7 = plt.subplot2grid((3,42),(2,41),rowspan=1,colspan=1)
cb1 = plt.colorbar(cont1, cax=ax7,extend='both',orientation='vertical')
cb1.set_label('R')
# ax9.yaxis.set_ticks_position('left')
# ax9.yaxis.set_label_position('left')




plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations1.png')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations1.pdf')
plt.show(block = False)

####################################################


#### NO correlation 	 DATA ####

plt.close('all')
plt.figure(figsize = (8,6.5))

#plt1

# ax1 = plt.subplot2grid((3,3),(0,0),rowspan=1,colspan=3)
#
# bins = [900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,1950]
# x=[0.1,0.7,0.2,0.0,7.8,6.0,2.8,3.2,22.6,25.3,40.8,8.6] # duration of sea-ice (in weeks) * extend along N. Iceland coast
# #not also https://www.jstor.org/tc/accept?origin=%2Fstable%2Fpdf%2F520132.pdf says cereal and grain cultivatoin ceased in north and beast before end of 12 century, and altogether near end of 16th C
# ax1.step(bins,x,lw=3)
# ax1.set_ylabel('Ice off Iceland')
# plt.text(1200, 32, 'cereal and grain\ncultivation ceased\nin North and East\n(Schell, 1961)', rotation=90)
# plt.text(1600, 47, 'cereal and grain\ncultivation ceased\naltogether\n(Schell, 1961)', rotation=90)
#
# ax1.set_xlim([900,1900])
# ax1.set_ylim([0.0,50])

#plt 2
ax2 = plt.subplot2grid((3,42),(1,0),rowspan=1,colspan=12)

year2 = year[0]+np.arange(900)
# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b = ax2.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

ax2.set_ylabel('multi-model\nmean sea-ice')
ax2.yaxis.label.set_color('b')

# ax2b.set_ylabel('bivalve $\delta^{18}$O')
ax2b.set_xlim([900,1200])
ax2.set_ylim([-4.0,4.0])
ax2b.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off

ax2.spines['right'].set_visible(False)
ax2b.spines['right'].set_visible(False)

#plt 2_right
ax2_right = plt.subplot2grid((3,42),(1,30),rowspan=1,colspan=12)

# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2_right.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b_right = ax2_right.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# ax2_right.set_ylabel('sea-ice')
ax2b_right.set_ylabel('bivalve $\delta^{18}$O')
ax2b_right.yaxis.label.set_color('r')
ax2b_right.set_xlim([1600,1900])
ax2_right.set_ylim([-4.0,4.0])
ax2b_right.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=True,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax2b_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=True) # labels along the bottom edge are off

ax2_right.spines['left'].set_visible(False)
ax2b_right.spines['left'].set_visible(False)

# ax2_right.legend(prop={'size':10},loc=1).draw_frame(False)
# ax2b_right.legend(prop={'size':10},loc=4).draw_frame(False)

#plt 3
ax3 = plt.subplot2grid((3,42),(1,13),rowspan=1,colspan=16)
ax3.set_facecolor('none')

lab1 = ax3.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

# ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
ax3b = ax3.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1600) & (reynolds_data[:,0] >= 1200) )[0]

y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
lab2 = ax3b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve d18O')

# ax3.set_ylabel('sea-ice')
# ax3b.set_ylabel('bivalve $\delta^{18}$O')
ax3.set_xticklabels(['',1300,1400,1500,''])
ax3.set_xlim([1200,1600])
ax3.set_ylim([-4.0,4.0])
ax3b.set_ylim([-4.0,4.0])
# ax3.plot([900,1850],[0,0],'k')
ax3.set_xlabel('year')

# ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)

ax3.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3.spines['left'].set_visible(False)
ax3b.spines['left'].set_visible(False)
ax3.spines['right'].set_visible(False)
ax3b.spines['right'].set_visible(False)

ax3.text(1400, 3, '$\delta^{18}$O y-axis inverted',horizontalalignment='center',color='r')

# ax3.legend(prop={'size':10},loc=2).draw_frame(False)
# ax3b.legend(prop={'size':10},loc=3).draw_frame(False)

########


# plotting correlations #
import matplotlib.cm as mpl_cm

cmap1 = mpl_cm.get_cmap('bwr')
cmap2 = mpl_cm.get_cmap('bwr')


#plt 1
# ax4 = plt.subplot2grid((3,42),(2,2),rowspan=1,colspan=10)
# cont1 = ax4.contourf(smoothings1,smoothings2,phase1_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
# ax4.contourf(smoothings1,smoothings2,phase1_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
# ax4.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax4.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax4.set_xlim(0,100)
# ax4.set_ylim(0,100)
# ax4.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
# ax4.set_ylabel('model sea-ice smoothing\n(years)')
# # plt.colorbar(cont1)
#
# #plt 2
# ax5 = plt.subplot2grid((3,42),(2,16),rowspan=1,colspan=10)
#
# cont1 = ax5.contourf(smoothings1,smoothings2,phase2_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
# ax5.contourf(smoothings1,smoothings2,phase2_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
# ax5.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax5.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax5.set_xlim(0,100)
# ax5.set_ylim(0,100)
# ax5.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
# ax5.tick_params(
#     axis='y',          # changes apply to the x-axis
#     which='both',      # both major and minor ticks are affected
#     bottom=True,      # ticks along the bottom edge are off
#     labelbottom=True, # labels along the bottom edge are off
#     left=False,      # ticks along the bottom edge are off
#     labelleft=False) # labels along the bottom edge are off

#plt 3
# ax6 = plt.subplot2grid((3,42),(2,30),rowspan=1,colspan=10)
#
# cont1 = ax6.contourf(smoothings1,smoothings2,phase3_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
# ax6.contourf(smoothings1,smoothings2,phase3_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
# ax6.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax6.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax6.set_xlim(0,100)
# ax6.set_ylim(0,100)
# ax6.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
# ax6.tick_params(
#     axis='y',          # changes apply to the x-axis
#     which='both',      # both major and minor ticks are affected
#     bottom=True,      # ticks along the bottom edge are off
#     labelbottom=True, # labels along the bottom edge are off
#     left=False,      # ticks along the bottom edge are off
#     labelleft=False) # labels along the bottom edge are off


####
# grey boxes
####
import matplotlib

# ax8 = plt.subplot2grid((3,42),(0,12),rowspan=3,colspan=1)
# ax8.add_patch(matplotlib.patches.Rectangle((0,0), 100, 100, color="grey"))
from matplotlib.patches import Ellipse

# ax1.add_patch(plt.Rectangle((1185,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
# ax1.add_patch(plt.Rectangle((1592,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
ax1.add_patch(plt.Rectangle((1200,50),400, -170,facecolor='silver',
                              clip_on=False,linewidth = 0,alpha=0.2))
# ax1.add_patch(plt.Rectangle((900,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))
# ax1.add_patch(plt.Rectangle((1600,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))

#####
# colorbar
#####


# ax7 = plt.subplot2grid((3,42),(2,41),rowspan=1,colspan=1)
# cb1 = plt.colorbar(cont1, cax=ax7,extend='both',orientation='vertical')
# cb1.set_label('R')
# ax9.yaxis.set_ticks_position('left')
# ax9.yaxis.set_label_position('left')




plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations2.png')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations2.pdf')
plt.show(block = False)

####################################################



#### NO model DATA ####

plt.close('all')
plt.figure(figsize = (8,6.5))

#plt1

# ax1 = plt.subplot2grid((3,3),(0,0),rowspan=1,colspan=3)
#
# bins = [900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,1950]
# x=[0.1,0.7,0.2,0.0,7.8,6.0,2.8,3.2,22.6,25.3,40.8,8.6] # duration of sea-ice (in weeks) * extend along N. Iceland coast
# #not also https://www.jstor.org/tc/accept?origin=%2Fstable%2Fpdf%2F520132.pdf says cereal and grain cultivatoin ceased in north and beast before end of 12 century, and altogether near end of 16th C
# ax1.step(bins,x,lw=3)
# ax1.set_ylabel('Ice off Iceland')
# plt.text(1200, 32, 'cereal and grain\ncultivation ceased\nin North and East\n(Schell, 1961)', rotation=90)
# plt.text(1600, 47, 'cereal and grain\ncultivation ceased\naltogether\n(Schell, 1961)', rotation=90)
#
# ax1.set_xlim([900,1900])
# ax1.set_ylim([0.0,50])

#plt 2
ax2 = plt.subplot2grid((3,42),(1,0),rowspan=1,colspan=12)
#
# year2 = year[0]+np.arange(900)
# # y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
# y = multimodel_mean-np.nanmean(multimodel_mean)
# y /= np.std(y)
# ax2.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
# x_fill = np.concatenate([year2,np.flipud(year2)])
# y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# # ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b = ax2.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# ax2.set_ylabel('multi-model\nmean sea-ice')
# ax2.yaxis.label.set_color('b')

# ax2b.set_ylabel('bivalve $\delta^{18}$O')
ax2b.set_xlim([900,1200])
ax2.set_ylim([-4.0,4.0])
ax2b.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off

ax2.spines['right'].set_visible(False)
ax2b.spines['right'].set_visible(False)

#plt 2_right
ax2_right = plt.subplot2grid((3,42),(1,30),rowspan=1,colspan=12)
#
# # y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
# y = multimodel_mean-np.nanmean(multimodel_mean)
# y /= np.std(y)
# ax2_right.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'multi-model mean sea-ice')
# x_fill = np.concatenate([year2,np.flipud(year2)])
# y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# # ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2b_right = ax2_right.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],2]
# y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 /= np.std(y_reynolds2)
# ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax2b_right.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve $\delta^{18}$O')

# ax2_right.set_ylabel('sea-ice')
ax2b_right.set_ylabel('bivalve $\delta^{18}$O')
ax2b_right.yaxis.label.set_color('r')
ax2b_right.set_xlim([1600,1900])
ax2_right.set_ylim([-4.0,4.0])
ax2b_right.set_ylim([4.0,-4.0])
# ax2.plot([900,1850],[0,0],'k')
ax2_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=True,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax2b_right.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=True) # labels along the bottom edge are off

ax2_right.spines['left'].set_visible(False)
ax2b_right.spines['left'].set_visible(False)

# ax2_right.legend(prop={'size':10},loc=1).draw_frame(False)
# ax2b_right.legend(prop={'size':10},loc=4).draw_frame(False)

#plt 3
ax3 = plt.subplot2grid((3,42),(1,13),rowspan=1,colspan=16)
ax3.set_facecolor('none')

# lab1 = ax3.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
# x_fill = np.concatenate([year2,np.flipud(year2)])
# y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

# ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = my_linewidth, alpha=0.5,label = 'd18O')
ax3b = ax3.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1600) & (reynolds_data[:,0] >= 1200) )[0]

y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
lab2 = ax3b.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = my_linewidth, alpha=0.5,label = 'bivalve d18O')

# ax3.set_ylabel('sea-ice')
# ax3b.set_ylabel('bivalve $\delta^{18}$O')
ax3.set_xticklabels(['',1300,1400,1500,''])
ax3.set_xlim([1200,1600])
ax3.set_ylim([-4.0,4.0])
ax3b.set_ylim([-4.0,4.0])
# ax3.plot([900,1850],[0,0],'k')
ax3.set_xlabel('year')

# ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)

ax3.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3b.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    labelleft=False, # labels along the bottom edge are off
    right=False,      # ticks along the bottom edge are off
    labelright=False) # labels along the bottom edge are off
ax3.spines['left'].set_visible(False)
ax3b.spines['left'].set_visible(False)
ax3.spines['right'].set_visible(False)
ax3b.spines['right'].set_visible(False)

# ax3.text(1400, 3, '$\delta^{18}$O y-axis inverted',horizontalalignment='center',color='r')

# ax3.legend(prop={'size':10},loc=2).draw_frame(False)
# ax3b.legend(prop={'size':10},loc=3).draw_frame(False)

########


# plotting correlations #
import matplotlib.cm as mpl_cm

cmap1 = mpl_cm.get_cmap('bwr')
cmap2 = mpl_cm.get_cmap('bwr')


#plt 1
# ax4 = plt.subplot2grid((3,42),(2,2),rowspan=1,colspan=10)
# cont1 = ax4.contourf(smoothings1,smoothings2,phase1_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
# ax4.contourf(smoothings1,smoothings2,phase1_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
# ax4.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax4.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax4.set_xlim(0,100)
# ax4.set_ylim(0,100)
# ax4.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
# ax4.set_ylabel('model sea-ice smoothing\n(years)')
# # plt.colorbar(cont1)
#
# #plt 2
# ax5 = plt.subplot2grid((3,42),(2,16),rowspan=1,colspan=10)
#
# cont1 = ax5.contourf(smoothings1,smoothings2,phase2_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
# ax5.contourf(smoothings1,smoothings2,phase2_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
# ax5.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax5.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax5.set_xlim(0,100)
# ax5.set_ylim(0,100)
# ax5.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
# ax5.tick_params(
#     axis='y',          # changes apply to the x-axis
#     which='both',      # both major and minor ticks are affected
#     bottom=True,      # ticks along the bottom edge are off
#     labelbottom=True, # labels along the bottom edge are off
#     left=False,      # ticks along the bottom edge are off
#     labelleft=False) # labels along the bottom edge are off

#plt 3
# ax6 = plt.subplot2grid((3,42),(2,30),rowspan=1,colspan=10)
#
# cont1 = ax6.contourf(smoothings1,smoothings2,phase3_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
# ax6.contourf(smoothings1,smoothings2,phase3_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
# ax6.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax6.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
# ax6.set_xlim(0,100)
# ax6.set_ylim(0,100)
# ax6.set_xlabel('bivalve $\delta^{18}$O smoothing\n(years)')
# ax6.tick_params(
#     axis='y',          # changes apply to the x-axis
#     which='both',      # both major and minor ticks are affected
#     bottom=True,      # ticks along the bottom edge are off
#     labelbottom=True, # labels along the bottom edge are off
#     left=False,      # ticks along the bottom edge are off
#     labelleft=False) # labels along the bottom edge are off


####
# grey boxes
####
import matplotlib

# ax8 = plt.subplot2grid((3,42),(0,12),rowspan=3,colspan=1)
# ax8.add_patch(matplotlib.patches.Rectangle((0,0), 100, 100, color="grey"))
from matplotlib.patches import Ellipse

# ax1.add_patch(plt.Rectangle((1185,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
# ax1.add_patch(plt.Rectangle((1592,50),25, -172,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.5))
ax1.add_patch(plt.Rectangle((1200,50),400, -170,facecolor='silver',
                              clip_on=False,linewidth = 0,alpha=0.2))
# ax1.add_patch(plt.Rectangle((900,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))
# ax1.add_patch(plt.Rectangle((1600,50),300, -178,facecolor='silver',
#                               clip_on=False,linewidth = 0,alpha=0.4))

#####
# colorbar
#####


# ax7 = plt.subplot2grid((3,42),(2,41),rowspan=1,colspan=1)
# cb1 = plt.colorbar(cont1, cax=ax7,extend='both',orientation='vertical')
# cb1.set_label('R')
# ax9.yaxis.set_ticks_position('left')
# ax9.yaxis.set_label_position('left')




plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations3.png')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean_with_correlations3.pdf')
plt.show(block = False)

####################################################

"""

"""
###playing###

#####################
# CET
#####################
start_year = 950
end_year = 1800
data_file = '/data/NAS-ph290/ph290/observations/ssn_HadCET_mean.txt'
data = np.genfromtxt(data_file, skip_header = 14,delimiter='\t')
data_year = data[:,0]
data_data = data[:,2]
loc = np.where((np.logical_not(np.isnan(data_data))) & (data_year >= start_year) & (data_year <= end_year))
cet_year = data_year[loc]
data_data = data_data[loc]
#data_data -= np.min(data_data)
#data_data_sigma_diff-= np.min(data_data_sigma_diff)
#data_data /= np.max(data_data)
#data_data_sigma_diff /= np.max(data_data)

data_data=scipy.signal.detrend(data_data)
# data_data_filtered = scipy.signal.filtfilt(b2, a2, data_data.copy())
# data_data_filtered = data_data

data_data -= np.mean(data_data)
cet_data = data_data/np.std(data_data)

# data_data_filtered -= np.mean(data_data_filtered)
# cet_filtered = data_data_filtered/ np.std(data_data_filtered)

plt.plot(year2,y,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
plt.plot(cet_year,cet_data,'r',linewidth = my_linewidth, alpha=0.8)
plt.show()

########


##########################################
# Mann AMO data
##########################################
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)
amo_data_filtered = scipy.signal.filtfilt(b2, a2, amo_data.copy(),padlen=20)

amo_data -= np.mean(amo_data)
amo_data /= np.std(amo_data)

amo_data_filtered -= np.mean(amo_data_filtered)
amo_data_filtered /= np.std(amo_data_filtered)

amo_scr_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo_scr = np.genfromtxt(amo_scr_file, skip_header = 2)
amo_scr_yr = amo_scr[:,0]
amo_scr_data = amo_scr[:,1]
amo_scr_data_minus_2sigma = amo_scr[:,2]
amo_scr_data_sigma_diff = amo_scr_data_minus_2sigma + amo_scr_data
loc = np.where((np.logical_not(np.isnan(amo_scr_data_sigma_diff))) & (np.logical_not(np.isnan(amo_scr_data))) & (amo_scr_yr >= start_year) & (amo_scr_yr <= end_year))
amo_scr_yr = amo_scr_yr[loc]
amo_scr_data = amo_scr_data[loc]
amo_scr_data_sigma_diff = amo_scr_data_sigma_diff[loc]
#amo_scr_data -= np.min(amo_scr_data)
#amo_scr_data_sigma_diff-= np.min(amo_scr_data_sigma_diff)
#amo_scr_data /= np.max(amo_scr_data)
#amo_scr_data_sigma_diff /= np.max(amo_scr_data)
amo_scr_data=signal.detrend(amo_scr_data)
amo_scr_data_filtered = scipy.signal.filtfilt(b2, a2, amo_scr_data.copy(),padlen=0)

amo_scr_data -= np.mean(amo_scr_data)
amo_scr_data /= np.std(amo_scr_data)

amo_scr_data_filtered -= np.mean(amo_scr_data_filtered)
amo_scr_data_filtered /= np.std(amo_scr_data_filtered)

my_linewidth=1.5
plt.plot(year2,running_mean.running_mean(y,5)*-1.0,'b',linewidth = my_linewidth, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
plt.plot(amo_scr_yr,amo_scr_data*0.5,'r',linewidth = my_linewidth, alpha=0.8)
# plt.plot(amo_yr,amo_data,'r',linewidth = my_linewidth, alpha=0.8)

plt.show()







def smoothed_correlations(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_not_sig = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
	        smoothing1 = np.int(smoothing1)
	        print smoothing_no1,' out of ',np.size(smoothings1)
	        window_type = 'boxcar'
	        for smoothing_no2, smoothing2 in enumerate(smoothings2):
	                smoothing2 = np.int(smoothing2)
	                # x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
	                # y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
	                x = pd.DataFrame({'a':a.copy()}).rolling(smoothing1,win_type=window_type,center=True).sum().values
	                y = pd.DataFrame({'b':b.copy()}).rolling(smoothing2,win_type=window_type,center=True).sum().values
	                loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
	                x = x[loc]
	                y = y[loc]
	                slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	                r2 = r_value**2
	                coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	                if p_value > 0.001:
	                        coeff_det_a_b_not_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_not_sig


def smoothed_correlations2(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_not_sig = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
	        smoothing1 = np.int(smoothing1)
	        print smoothing_no1,' out of ',np.size(smoothings1)
	        window_type = 'boxcar'
	        for smoothing_no2, smoothing2 in enumerate(smoothings2):
	                smoothing2 = np.int(smoothing2)
	                # x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
	                # y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
	                x = pd.DataFrame({'a':a.copy()}).rolling(smoothing1,win_type=window_type,center=True).sum().values
	                y = pd.DataFrame({'b':b.copy()}).rolling(smoothing2,win_type=window_type,center=True).sum().values
	                loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
	                x = x[loc]
	                y = y[loc]
	                slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	                r2 = r_value**2
	                coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	                if p_value > 0.001:
	                        coeff_det_a_b_not_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_not_sig


data = np.zeros([900,np.size(ts_n_hem)])
j=0
for i,ts in enumerate(ts_n_hem):
	print modelsb[i]
	data[:,j] = signal.detrend(ts.data[0:900])
	# data[:,j] = (ts.data[0:1000])
	j += 1

multimodel_mean = data.mean(axis = 1)

reynolds_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
reynolds_data = np.genfromtxt(reynolds_file,skip_header = 1,delimiter = ',')

reynolds_data2 = np.flipud(reynolds_data)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)

smoothings1 = range(1,150,2)
smoothings2 = smoothings1
year2 = year[0]+np.arange(900)


loc1 = np.where((year2 <= 1200) & (year2 >= 960) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1200) & (reynolds_data2[:,0] >= 960) )[0]

# phase1_corr_sig,phase1_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)
#
# loc1 = np.where((year2 <= 1600) & (year2 >= 1200) )[0]
# loc2 = np.where((reynolds_data2[:,0] <= 1600) & (reynolds_data2[:,0] >= 1200) )[0]
#
# phase2_corr_sig,phase2_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)
#
# loc1 = np.where((year2 <= 1850) & (year2 >= 1600) )[0]
# loc2 = np.where((reynolds_data2[:,0] <= 1849) & (reynolds_data2[:,0] >= 1600) )[0]
#
# phase3_corr_sig,phase3_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)


a,b=y.copy(),reynolds_data2[:,2].copy()
# a = scipy.signal.detrend(a)
# a /= np.nanstd(a)
# b = scipy.signal.detrend(b)
# b /= np.nanstd(b)

min_common_year = np.max([np.min(year2),np.min(reynolds_data2[:,0])])
max_common_year = np.min([np.max(year2),np.max(reynolds_data2[:,0])])
dates = pd.period_range(start='1/1/'+str(int(min_common_year)), end='1/1/'+str(int(max_common_year)),freq='Y')

loc1 = np.where((year2 <= max_common_year) & (year2 >= min_common_year) )[0]
loc2 = np.where((reynolds_data2[:,0] <= max_common_year) & (reynolds_data2[:,0] >= min_common_year) )[0]

min_yr = 1200
max_yr = 1600

# loc1 = np.where((year2 <= max_yr) & (year2 >= min_yr) )[0]
# loc2 = np.where((reynolds_data2[:,0] <= max_yr) & (reynolds_data2[:,0] >= min_yr) )[0]

# b = scipy.signal.detrend(b).copy()
# b /= np.nanstd(b)
# b[loc2] = b[loc2] * -1.0

def prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2):
	window_type = 'boxcar'
	loc1 = np.where((year2 <= max_yr) & (year2 >= min_yr))[0]
	loc2 = np.where((reynolds_data2[:,0] <= max_yr) & (reynolds_data2[:,0] >= min_yr) )[0]
	a_tmp = scipy.signal.detrend(y[loc1].copy())
	a_tmp /= np.nanstd(a_tmp)
	b_tmp = scipy.signal.detrend(reynolds_data2[:,2][loc2].copy())
	b_tmp /= np.nanstd(b_tmp)
	dates = pd.period_range(start='1/1/'+str(int(min_yr)), end='1/1/'+str(int(max_yr)),freq='Y')
	df = pd.DataFrame({'model':a_tmp,'bivalve':b_tmp,'Datetime':dates})
	df = df.set_index('Datetime')
	y_plot = df.model.rolling(smoothing1,win_type=window_type,center=True).mean().values
	x_plot = df.bivalve.rolling(smoothing2,win_type=window_type,center=True).mean().values
	loc = np.where(((np.isfinite(x_plot)) & (np.isfinite(y_plot))))
	x_plot = x_plot[loc]
	y_plot = y_plot[loc]
	x2_plot = sm.add_constant(x_plot)
	model = sm.OLS(y_plot,x2_plot)
	results = model.fit()
	return x_plot,y_plot,results

smoothing1 = 12
smoothing2 = 18
range_min,range_max = -1.5,1.5

min_yr = 960
max_yr = 1200

x_plot,y_plot,results = prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2)
plt.scatter(x_plot,y_plot, s=20, edgecolors='b',alpha=0.8)
plt.plot(x_plot,results.params[1]*x_plot+results.params[0])
plt.xlim([range_min,range_max])
plt.ylim([range_min,range_max])
plt.xlabel('bivalve $\delta^{18}$O' )
plt.ylabel('model seaice' )
plt.show()


min_yr = 1200
max_yr = 1600

x_plot,y_plot,results = prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2)
plt.scatter(x_plot,y_plot, s=20, edgecolors='b',alpha=0.8)
plt.plot(x_plot,results.params[1]*x_plot+results.params[0])
plt.xlim([range_min,range_max])
plt.ylim([range_min,range_max])
plt.show()

min_yr = 1600
max_yr = 1849

x_plot,y_plot,results = prepare_for_scatter(min_yr,max_yr,smoothing1,smoothing2,year2,y,reynolds_data2)
plt.scatter(x_plot,y_plot, s=20, edgecolors='b',alpha=0.8)
plt.plot(x_plot,results.params[1]*x_plot+results.params[0])
plt.xlim([range_min,range_max])
plt.ylim([range_min,range_max])
plt.show()

#
# loc1 = np.where((year2 <= max_yr) & (year2 >= min_yr) )[0]
# loc2 = np.where((reynolds_data2[:,0] <= max_yr) & (reynolds_data2[:,0] >= min_yr) )[0]
#
# a_tmp = scipy.signal.detrend(a[loc1]).copy()
# a_tmp /= np.nanstd(a_tmp)
#
# b_tmp = scipy.signal.detrend(b[loc2]).copy()
# b_tmp /= np.nanstd(b_tmp)
#
#
#
#
#
#
#
#
#
# min_yr = 1200
# max_yr = 1600
#
# loc1 = np.where((year2 <= max_yr) & (year2 >= min_yr) )[0]
# loc2 = np.where((reynolds_data2[:,0] <= max_yr) & (reynolds_data2[:,0] >= min_yr) )[0]
#
# a_tmp = scipy.signal.detrend(a[loc1]).copy()
# a_tmp /= np.nanstd(a_tmp)
#
# b_tmp = scipy.signal.detrend(b[loc2]).copy()
# b_tmp /= np.nanstd(b_tmp)
#
# dates = pd.period_range(start='1/1/'+str(int(min_yr)), end='1/1/'+str(int(max_yr)),freq='Y')
#
# df = pd.DataFrame({'model':a_tmp,'bivalve':b_tmp,'Datetime':dates})
# df = df.set_index('Datetime')
#
#
# x = df.model.rolling(smoothing1,win_type=window_type,center=True).mean().values
# y = df.bivalve.rolling(smoothing2,win_type=window_type,center=True).mean().values
#
# plt.scatter(x,y)
# plt.xlim([-1.4,1.4])
# plt.ylim([-1.4,1.4])
# plt.show()
#
# min_yr = 1600
# max_yr = 1849
#
# loc1 = np.where((year2 <= max_yr) & (year2 >= min_yr) )[0]
# loc2 = np.where((reynolds_data2[:,0] <= max_yr) & (reynolds_data2[:,0] >= min_yr) )[0]
#
# a_tmp = scipy.signal.detrend(a[loc1]).copy()
# a_tmp /= np.nanstd(a_tmp)
#
# b_tmp = scipy.signal.detrend(b[loc2]).copy()
# b_tmp /= np.nanstd(b_tmp)
#
# dates = pd.period_range(start='1/1/'+str(int(min_yr)), end='1/1/'+str(int(max_yr)),freq='Y')
#
# df = pd.DataFrame({'model':a_tmp,'bivalve':b_tmp,'Datetime':dates})
# df = df.set_index('Datetime')
#
#
# x = df.model.rolling(smoothing1,win_type=window_type,center=True).mean().values
# y = df.bivalve.rolling(smoothing2,win_type=window_type,center=True).mean().values
#
# plt.scatter(x,y)
# plt.xlim([-1.4,1.4])
# plt.ylim([-1.4,1.4])
# plt.show()
#
# #
# # smoothings1 = range(1,30,1)
# # p_values = np.zeros([len(smoothings1),np.max(smoothings1)])
# # p_values[:] = np.nan
# # rs = p_values.copy()
# # r2s = p_values.copy()
# #
# #
# # plt.close('all')
# # for i,smoothing in enumerate(smoothings1):
# # 	print i
# # 	#somehow cycle though akll of the chunks that you coudl splot it in to by resampleoing...
# # 	for j in range(smoothing):
# # 		tmp = df[j:].resample(str(smoothing)+'Y').mean()
# # 		slope, intercept, r_value, p_value, std_err = stats.linregress(tmp.model,tmp.bivalve)
# # 		p_values[i,j] = p_value
# # 		rs[i,j] = r_value
# # 		r2s[i,j] = r_value**2
# # 		if ((i==10) & (j==1)):
# # 			plt.plot(tmp.model.values)
# # 			plt.plot(tmp.bivalve.values)
# # 			plt.show(block = True)
# #
# # plt.close('all')
# # fig, ax1 = plt.subplots()
# # ax1.plot(np.nanmin(p_values,axis=1),'b')
# # ax2 = ax1.twinx()
# # ax2.plot(np.nanmax(r2s,axis=1),'r')
# # plt.show(block = False)
"""

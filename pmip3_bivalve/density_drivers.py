
import numpy as np
import iris
import glob
import subprocess
import uuid
import os
import iris.analysis
import iris.analysis.stats
import pickle
import scipy
from scipy import signal
import seawater
import cartopy.crs as ccrs
import iris.plot as iplt


def low_pass_weights(window, cutoff):
    """Calculate weights for a low pass Lanczos filter.

    Args:

    window: int
        The length of the filter window.

    cutoff: float
        The cutoff frequency in inverse time steps.

    """
    order = ((window - 1) // 2) + 1
    nwts = 2 * order + 1
    w = np.zeros([nwts])
    n = nwts // 2
    w[n] = 2 * cutoff
    k = np.arange(1., n)
    sigma = np.sin(np.pi * k / n) * n / (np.pi * k)
    firstfactor = np.sin(2. * np.pi * cutoff * k) / (np.pi * k)
    w[n-1:0:-1] = firstfactor * sigma
    w[n+1:-1] = firstfactor * sigma
    return w[1:-1]


def detrend_cube(cube):
   cube_data = cube.data.data.copy()
   mask = cube.data.mask.copy()
   cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
   tmp = np.ma.masked_array(cube_data_detrended)
   tmp.mask = mask
   cube.data = tmp
   return cube


def getMaxDepth(cube):
	thicknesses = getLevelThickness(cube)
	try:
		levels = np.size(cube.coord('depth').points)
	except:
		levels = np.size(cube.coord('ocean sigma over z coordinate').points)
	depth_data = cube.data
	for level in np.arange(levels):
		arr_shape = np.shape(depth_data)
		depth_data.data[:,level,:,:] = thicknesses[level]
	depth_data[np.where(depth_data.mask)] = np.NAN
	data = np.ma.masked_array(np.nansum(depth_data,axis=1))
	data.mask = cube.data.mask.copy()
	return data


def getLevelThickness(cube):
    """
    Calculate level thicknesses. This is useful as guess_bounds()
    in Iris v1.3 gets the bounds wrong for irregular depth grids
    as it just averages level depths (results in level depth not
    being halfway between bounds)
    """
    depth = 0
    thickness = []
    try:
	    depth_tmp = cube.coord('depth').points
    except:
    	depth_tmp = cube.coord('ocean sigma over z coordinate').points
    for level in depth_tmp:
        thickness.append(2 * (level - depth))
        depth += thickness[-1]
    return thickness



def depth_mean(cube):
	thicknesses = getLevelThickness(cube.copy())
	data = cube.data.copy()
	for level in np.arange(np.size(thicknesses)):
		data[:,level] = data[:,level] * thicknesses[level]
	cube.data = data.copy()
	try:
		cube_averaged = cube.copy().collapsed('depth',iris.analysis.SUM).copy()
		tmp_cube = cube_averaged.copy()
		tmp_cube.data = getMaxDepth(cube.copy())
		cube_averaged /= tmp_cube
	except:
		cube_averaged = cube.copy().collapsed('ocean sigma over z coordinate',iris.analysis.SUM).copy()
		cube_averaged.data /= getMaxDepth(cube.copy())
	return cube_averaged


def model_names(directory,var):
        files = glob.glob(directory+'/*_'+var+'_*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
        models = np.unique(models_tmp)
        return models


def new_cube_filled_with_spatial_mean(cube):
	cube_mask = cube.data.mask.copy()
	cube_data = cube.data.copy()
	tmp_cube = cube.collapsed('time',iris.analysis.MEAN)
	cube_data = np.repeat(tmp_cube.data[np.newaxis,:,:,:],np.shape(cube)[0],axis=0)
	cube_data = np.ma.masked_array(cube_data)
	cube_data.mask = cube_mask
	cube.data = cube_data
	return cube




# NOTE T AND S processed in /home/ph290/Documents/python_scripts/regridding_and_concatenating_cmip5_files_thetao_so3.py
# Here the top 100m have been extracted
temporary_file_space = '/data/dataSSD0/ph290/tmp2/'
directory = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'

models1 = model_names(directory,'thetao')
models2 = model_names(directory,'so')

models1 = list(models1)
models2 = list(models2)

models = filter(set(models1).__contains__, models2)


############
# Calculate density
############

season = 'DJF'

#NOTE THAT thateao and so have been extracted for only the top 100m

for model in models:
    print model
    out_name = model+'_rhopot_piControl_r1i1p1_regridded_'+season+'.nc'
    out_name1 = model+'_rhopot_piControl_r1i1p1_regridded_'+season+'_const_s.nc'
    out_name2 = model+'_rhopot_piControl_r1i1p1_regridded_'+season+'_const_thetao.nc'
    # Density
    test = glob.glob(directory+out_name)
    if np.size(test) == 0:
        cube1_len = np.shape(iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
        cube2_len = np.shape(iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
        if cube1_len == cube2_len:
        	### varying T
            tmp_cube_s = iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc')
            tmp_cube_t = iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc')-273.15
            rho_cube = tmp_cube_s.copy()
            mask = tmp_cube_s.data.mask.copy()
            rho_cube_data = np.ma.masked_array(seawater.dens(tmp_cube_s.data,tmp_cube_t.data,tmp_cube_t.data * 0.0))
            rho_cube_data.mask = mask
            rho_cube.data = rho_cube_data
            iris.save(rho_cube, directory+out_name)
        else:
        	print model+' T and S different number of years'
    # Density - T-driven
    test = glob.glob(directory+out_name1)
    if np.size(test) == 0:
        cube1_len = np.shape(iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
        cube2_len = np.shape(iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
        if cube1_len == cube2_len:
        	### varying T
            tmp_cube_s = new_cube_filled_with_spatial_mean(iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc'))
            tmp_cube_t = iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc')-273.15
            rho_cube = tmp_cube_s.copy()
            mask = tmp_cube_s.data.mask.copy()
            rho_cube_data = np.ma.masked_array(seawater.dens(tmp_cube_s.data,tmp_cube_t.data,tmp_cube_t.data * 0.0))
            rho_cube_data.mask = mask
            rho_cube.data = rho_cube_data
            iris.save(rho_cube, directory+out_name1)
        else:
        	print model+' T and S different number of years'
    # Density - S-driven
    test = glob.glob(directory+out_name2)
    if np.size(test) == 0:
    	cube1_len = np.shape(iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
    	cube2_len = np.shape(iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
    	if cube1_len == cube2_len:
    		### varying T
    		tmp_cube_s = iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc')
    		tmp_cube_t = new_cube_filled_with_spatial_mean(iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc'))-273.15
    		rho_cube = tmp_cube_s.copy()
    		mask = tmp_cube_s.data.mask.copy()
    		rho_cube_data = np.ma.masked_array(seawater.dens(tmp_cube_s.data,tmp_cube_t.data,tmp_cube_t.data * 0.0))
    		rho_cube_data.mask = mask
    		rho_cube.data = rho_cube_data
    		iris.save(rho_cube, directory+out_name2)
    	else:
    		print model+' T and S different number of years'


#########
# previously achived with:
    # 	if np.size(test) == 0:
    # 		cube1_len = np.shape(iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
    # 		cube2_len = np.shape(iris.load_cube(directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc'))[0]
    # 		if cube1_len == cube2_len:
    # 			### varying T+S
    #
    # 			subprocess.call(['cdo -P 6  merge -subc,273.15 '+directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc '+directory+model+'_so_piControl_r1i1p1_regridded_'+season+'.nc '+temporary_file_space+'delete.nc'], shell=True)
    # 			subprocess.call(['cdo -P 6 rhopot -adisit '+temporary_file_space+'delete.nc '+directory+out_name], shell=True)
    # 			try:
    # 				os.remove(temporary_file_space+'delete.nc')
    # 			except:
    # 				print 'no file to delete'
    # 		else:
    # 			print model+' T and S different number of years'
    # 			models.remove(model)

############
# correlate density with T and S
############



tmp_cube = iris.load_cube(directory+model+'_thetao_piControl_r1i1p1_regridded_'+season+'.nc')[0:np.size(models)]
tmp_cube = depth_mean(tmp_cube)
tmp_cube.data *= 0.0
t_rho_cubes = tmp_cube.copy()
s_rho_cubes = tmp_cube.copy()
t_rho_cubes_data = t_rho_cubes.data.copy()
s_rho_cubes_data = s_rho_cubes.data.copy()



for i,model in enumerate(models):
	print model
	t_cube = iris.load_cube(directory+model+'_rhopot_piControl_r1i1p1_regridded_'+season+'_const_s.nc')
	s_cube = iris.load_cube(directory+model+'_rhopot_piControl_r1i1p1_regridded_'+season+'_const_thetao.nc')
	rho_cube = iris.load_cube(directory+model+'_rhopot_piControl_r1i1p1_regridded_'+season+'.nc')
	#depth mean
	t_cube = depth_mean(t_cube)
	s_cube = depth_mean(s_cube)
	rho_cube = depth_mean(rho_cube)
	#detrend
	t_cube = detrend_cube(t_cube)
	s_cube = detrend_cube(s_cube)
	rho_cube = detrend_cube(rho_cube)
		#running mean filter (10 years):
	window = 10
	wgts10 = low_pass_weights(window, 1. / 10.)
	t_cube_filtered = t_cube.copy().rolling_window('time',iris.analysis.SUM,len(wgts10),weights=wgts10)
	s_cube_filtered = s_cube.copy().rolling_window('time',iris.analysis.SUM,len(wgts10),weights=wgts10)
	rho_cube_filtered = rho_cube.copy().rolling_window('time',iris.analysis.SUM,len(wgts10),weights=wgts10)

	#calculate correlations
	t_rho_cubes_data[i,:,:] = iris.analysis.stats.pearsonr(t_cube_filtered, rho_cube_filtered, corr_coords=['time']).data
	s_rho_cubes_data[i,:,:] = iris.analysis.stats.pearsonr(s_cube_filtered, rho_cube_filtered, corr_coords=['time']).data

	t_rho_cubes.data = t_rho_cubes_data
	s_rho_cubes.data = s_rho_cubes_data



with open('/data/NAS-ph290/ph290/cmip5/pickles/density.pickle', 'w') as f:
   pickle.dump([models,t_rho_cubes,s_rho_cubes], f)



with open('/data/NAS-ph290/ph290/cmip5/pickles/density.pickle', 'r') as f:
   [models,t_rho_cubes,s_rho_cubes] = pickle.load(f)


for i in range(np.shape(t_rho_cubes)[0]):
    plt.figure(figsize=(12, 5))
    proj = ccrs.PlateCarree(central_longitude=-0.0)
    plt.subplot(121, projection=proj)
    qplt.contourf(s_rho_cubes[i], np.linspace(-1.0,1.0,11))
    plt.gca().coastlines()
    # Plot #2: contourf with axes longitude from 0 to 360
    proj = ccrs.PlateCarree(central_longitude=0.0)
    plt.subplot(122, projection=proj)
    qplt.contourf(t_rho_cubes[i], np.linspace(-1.0,1.0,11))
    plt.gca().coastlines()
    iplt.show()

"""
import iris
import glob
import iris.coord_categorisation
from scipy import signal


ensmebles = np.array(['VOLC_GRA','SSI_VSK_L','0??'])

data = {}

def mean_and_extract(cube):
    lon_west = -45.0
    lon_east = -25.0
    lat_south = 0.0
    lat_north = 90.0
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
    iris.coord_categorisation.add_year(cube, 'time', name='year')
    return cube.aggregated_by('year', iris.analysis.MEAN)


def area_averag(cube):
    try:
        cube.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'
    try:
        cube.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'
    grid_areas = iris.analysis.cartography.area_weights(cube)
    area_avged_cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
    return area_avged_cube

for i in range(np.size(ensmebles)):
    print i
    data[ensmebles[i]]={}
    data[ensmebles[i]]['cubes']={}
    files = glob.glob('/data/NAS-ph290/ph290/cesm/b.e11.BLMTRC5CN.f19_g16.'+ensmebles[i]+'*.cam.h0.ICEFRAC.085001-184912.nc')
    tmp = mean_and_extract(iris.load_cube(files[0],'Fraction of sfc area covered by sea-ice'))
    multi_member_data = np.zeros([np.size(files),np.shape(tmp)[0],np.shape(tmp)[1],np.shape(tmp)[2]])
    multi_member_data[:] = np.nan
    for j,file in enumerate(files):
        print file
        cube = mean_and_extract(iris.load_cube(file,'Fraction of sfc area covered by sea-ice'))
        multi_member_data[j,:,:,:] = cube.data
        data[ensmebles[i]]['cubes'][j] = cube
    cube.data = np.nanmean(multi_member_data,axis=0)
    data[ensmebles[i]]['cube'] = cube
    data[ensmebles[i]]['data'] = np.nanmean(multi_member_data,axis=0)


###
# reynolds data
###


reynolds_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
reynolds_data = np.genfromtxt(reynolds_file,skip_header = 1,delimiter = ',')

"""


###
# plotting
###

plt.close('all')
fig = plt.subplots(2,1,figsize=(16, 8))

#plt 1
ax1 = plt.subplot(2,1,1)

y = signal.detrend(area_averag(data[ensmebles[0]]['cube']).data)
x = cube.coord('year').points
# ax1.plot(x,y/np.std(y),'b',alpha=0.5,label=ensmebles[0])

y = signal.detrend(area_averag(data[ensmebles[1]]['cube']).data)
x = cube.coord('year').points
# ax1.plot(x,y/np.std(y),'y',alpha=0.5,label=ensmebles[1])

y = signal.detrend(area_averag(data[ensmebles[2]]['cube']).data)
x = cube.coord('year').points
ax1.plot(x,y/np.std(y),'k',alpha=0.5,label=ensmebles[2])



ax2 = ax1.twinx()
loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
reynolds_datab = reynolds_data[loc[0]:loc[-1],0]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)
#

y_reynolds2 /= np.std(y_reynolds2)
y_reynolds2b = np.flipud(y_reynolds2)
reynolds_datab = np.flipud(reynolds_datab)
ax2.plot(reynolds_datab,y_reynolds2b,'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

ax1.set_ylabel('seaice')
ax2.set_ylabel('d18O')
ax1.set_xlim([900,1900])
ax1.set_ylim([-4.0,4.0])
ax2.set_ylim([-4.0,4.0])
ax1.plot([900,1900],[0,0],'k')

###

ax1b = plt.subplot(2,1,2)

y = signal.detrend(area_averag(data[ensmebles[0]]['cube']).data)
x = cube.coord('year').points
# ax1b.plot(x,y/np.std(y),'b',alpha=0.5,label=ensmebles[0])

y = signal.detrend(area_averag(data[ensmebles[1]]['cube']).data)
x = cube.coord('year').points
# ax1b.plot(x,y/np.std(y),'y',alpha=0.5,label=ensmebles[1])

y = signal.detrend(area_averag(data[ensmebles[2]]['cube']).data)
x = cube.coord('year').points
ax1b.plot(x,y/np.std(y),'k',alpha=0.5,label=ensmebles[2])

plt.legend()

ax2b = ax1b.twinx()
loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
reynolds_datab = reynolds_data[loc[0]:loc[-1],0]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
y_reynolds2b = np.flipud(y_reynolds2)
reynolds_datab = np.flipud(reynolds_datab)
ax2b.plot(reynolds_datab,y_reynolds2b * -1.0,'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

ax1b.set_ylabel('seaice')
ax2b.set_ylabel('d18O')
ax1b.set_xlim([900,1900])
ax1b.set_ylim([-4.0,4.0])
ax2b.set_ylim([-4.0,4.0])
ax1b.plot([900,1900],[0,0],'k')


plt.savefig('/home/ph290/Documents/figures/cesm_ensemble.png')
plt.show(block = False)

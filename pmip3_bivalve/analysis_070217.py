
<<<<<<< HEAD

=======
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
<<<<<<< HEAD
=======
import pickle

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
	models = np.unique(models_tmp)
	return models


def calc_d18O(T,S,s_factor):
	#S = ABSOLUTE SALINITY psu
	#T = ABSOLUTE T deg' C
	# d18Osw_synth = ((0.61*S)-21.3)
	# d18Osw_synth = ((3*S)-105)
	#R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
	#Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
	d18Osw_synth = ((s_factor*S)-18.98)
	#LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
	# d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
	# d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
	#Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
	d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
	#From Reynolds - the -27 is to convert between SMOW and vPDB
	return d18Oc_synth



lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

<<<<<<< HEAD
d18O_sens1 = 0.5 # half way between the N. Atlantic and Arctic values in LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
d18O_sens2 = 2.0
=======
# d18O_sens0 = 2.0
d18O_sens1 = 0.5 # half way between the N. Atlantic and Arctic values in LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
d18O_sens1 = 0.1
d18O_sens2 = 2.0
# d18O_sens2 = 0.5
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

out_dir = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'
#from ./palaeo_amo/latest_270716/calculate_T_and_S_no_regridding.py

<<<<<<< HEAD
#~ #########
#~ # intermediate processing step - just run once
#~ #########
#~
# files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*.nc')
#### files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*HadG*.nc')
#
=======
#########
# intermediate processing step - just run once
#########

# files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*.nc')
# ### files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*HadG*.nc')
# 
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
# for i,my_file in enumerate(files):
# 	print 'processing ',i,' in ',np.size(files)
# 	name = my_file.split('/')[-1]
# 	test = glob.glob(out_dir+name)
# 	if np.size(test) == 0:
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = out_dir+name,  options = '-P 7')
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' '+my_file, output = out_dir+name,  options = '-P 7')
<<<<<<< HEAD

=======
# 
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b


out_dir2 = '/data/NAS-ph290/ph290/cmip5/past1000/'

#########
# intermediate processing step - just run once
#########

<<<<<<< HEAD
files = glob.glob(out_dir2+'sic_OImon_*_past1000_r*_merged.nc')
### files = glob.glob('/'.join(out_dir2.split('/')[0:-2])+'/*HadG*.nc')

for i,my_file in enumerate(files):
	print 'processing ',i,' in ',np.size(files)
	name = my_file.split('/')[-1]
	test = glob.glob(out_dir2+'region_extracted_and_meaned_'+name)
	if np.size(test) == 0:
		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 -yearmean '+my_file, output = out_dir2+'region_extracted_and_meaned_regridded_'+name,  options = '-P 7')
		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -yearmean '+my_file, output = out_dir2+'region_extracted_and_meaned_'+name,  options = '-P 7')


# print and do seasonal mean???!!
=======
# files = glob.glob(out_dir2+'sic_OImon_*_past1000_r*_merged.nc')
# ### files = glob.glob('/'.join(out_dir2.split('/')[0:-2])+'/*HadG*.nc')
# 
# for i,my_file in enumerate(files):
# 	print 'processing ',i,' in ',np.size(files)
# 	name = my_file.split('/')[-1]
# 	test = glob.glob(out_dir2+'region_extracted_and_meaned_'+name)
# 	if np.size(test) == 0:
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 -yearmean '+my_file, output = out_dir2+'region_extracted_and_meaned_regridded_'+name,  options = '-P 7')
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -yearmean '+my_file, output = out_dir2+'region_extracted_and_meaned_'+name,  options = '-P 7')
# 
# 
# # print and do seasonal mean???!!
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

#########
# main bit of code
#########



directory = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'
#using /home/ph290/Documents/python_scripts/palaeo_amo/latest_270716/calculate_T_and_S_no_regridding.py

<<<<<<< HEAD
start_year = 950
=======
start_year = 1200
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory)
models = list(models)
# models.remove('MIROC-ESM')
# models.remove('FGOALS-s2')  # No sic variable downloaded at moment...
# models.remove('MPI-ESM-P')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadGEM2-ES') # No sic variable downloaded at moment...
models.remove('HadCM3')
models.remove('bcc-csm1-1')
# models.remove('GISS-E2-R') # No sic variable downloaded at moment...

<<<<<<< HEAD
=======

models = ['MPI-ESM-P']
models = ['MIROC-ESM']

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
##########################################
# Model Data # Summer T and S, to 100m mean
##########################################

# models_list = models
#
# for models in models_list:
# 	models = [models]


try:
	mm_mean_t
	test = False
except NameError:
	test = True


<<<<<<< HEAD
=======
### uncomment if you want to force a new run ###
test = True


>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
if test:
	mm_mean_t = np.zeros([np.size(models),np.size(expected_years)])
	mm_mean_t[:,:] = np.nan
	mm_mean_s = mm_mean_t.copy()
	mm_mean_d18O = mm_mean_t.copy()
	mm_mean_d18O2 = mm_mean_t.copy()
<<<<<<< HEAD
=======
	# mm_mean_d18O0 = mm_mean_t.copy()
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
	mm_mean_d18O_s_only = mm_mean_t.copy()
	mm_mean_d18O_t_only = mm_mean_t.copy()

	for i,model in enumerate(models):
		print i
		c1 = iris.load_cube(directory + 'thetao_mean_'+model+'_past1000_*_JJA.nc')[:,0,0]
		c2 = iris.load_cube(directory + 'so_mean_'+model+'_past1000_*_JJA.nc')[:,0,0]
<<<<<<< HEAD
=======
# 		c1a = iris.load_cube(directory + 'thetao_mean_'+model+'_past1000_*_SON.nc')[:,0,0]
# 		c2a = iris.load_cube(directory + 'so_mean_'+model+'_past1000_*_SON.nc')[:,0,0]
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
		coord = c1.coord('time')
		dt = coord.units.num2date(coord.points)
		year = np.array([coord.units.num2date(value).year for value in coord.points])
		for j,y in enumerate(expected_years):
			loc = np.where(year == y)[0][0]
			if np.size(loc) > 0:
	# 			try:
					mm_mean_t[i,j] = c1[loc].data
					mm_mean_s[i,j] = c2[loc].data
<<<<<<< HEAD
					mm_mean_d18O[i,j] = calc_d18O(c1[loc].data,c2[loc].data,d18O_sens1)
					mm_mean_d18O2[i,j] = calc_d18O(c1[loc].data,c2[loc].data,d18O_sens2)
=======
# 					mm_mean_t[i,j] = np.mean([c1[loc].data,c1a[loc].data])
# 					mm_mean_s[i,j] = np.mean([c2[loc].data,c2a[loc].data])
					# mm_mean_d18O0[i,j] = calc_d18O(c1[loc].data,c2[loc].data,d18O_sens0)
# 					mm_mean_d18O[i,j] = calc_d18O(c1[loc].data,c2[loc].data,d18O_sens1)
# 					mm_mean_d18O2[i,j] = calc_d18O(c1[loc].data,c2[loc].data,d18O_sens2)
# 					mm_mean_d18O[i,j] = calc_d18O(np.mean([c1[loc].data,c1a[loc].data]),np.mean([c2[loc].data,c2a[loc].data]),d18O_sens1)
# 					mm_mean_d18O2[i,j] = calc_d18O(np.mean([c1[loc].data,c1a[loc].data]),np.mean([c2[loc].data,c2a[loc].data]),d18O_sens2)
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
		# 		except:
		# 			print 'year missing'
		#




<<<<<<< HEAD
'''
=======
"""
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

directory2 = out_dir2

mm_mean_sic = np.zeros([np.size(models),np.size(expected_years)])
mm_mean_sic[:,:] = np.nan

for i,model in enumerate(models):
	print i
	c1 = iris.load_cube(directory2 + 'region_extracted_and_meaned_regridded_sic_OImon_'+model+'_past1000_*.nc')[:,0,0]
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	for j,y in enumerate(expected_years):
		loc = np.where(year == y)[0][0]
		if np.size(loc) > 0:
# 			try:
				mm_mean_sic[i,j] = c1[loc].data
	# 		except:
	# 			print 'year missing'
	#

<<<<<<< HEAD
'''
=======
"""
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp[::-1]
tmp = r_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
bivalve_data_initial_c = tmp[::-1]
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
<<<<<<< HEAD
# bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial[::-1])
=======
# bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
# bivalve_data_initial_c=scipy.signal.detrend(bivalve_data_initial_c)
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

##########################################
# Read in Masse 2008 seaice data #
##########################################

m_data_file = '/data/NAS-ph290/ph290/misc_data/masse_sea_ice.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ',')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
ip25_data_initial = tmp
ip25_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# ip25_data_initial=scipy.signal.detrend(ip25_initial[::-1])
ip25_data_initial=(ip25_data_initial[::-1])

<<<<<<< HEAD
=======
##########################################
# ortega2015nao.txt https://www1.ncdc.noaa.gov/pub/data/paleo/contributions_by_author/ortega2015/ortega2015nao.txt #
# http://www.nature.com/nature/journal/v523/n7558/pdf/nature14518.pdf
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/ortega2015nao.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 102)
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
nao_data_initial = tmp
nao_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# ip25_data_initial=scipy.signal.detrend(ip25_initial[::-1])
nao_data_initial=(nao_data_initial[::-1])

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

##########################################
# Pages data - europe and arctic #
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/pages_2k.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 2)
tmp = m_data[:,1]
tmp2 = m_data[:,3]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp2 = tmp2[loc[0]]
tmp_yr = m_data[loc[0],0]
<<<<<<< HEAD
p_data_initial = (tmp + tmp2) / 2.0
p_year = tmp_yr # reverse because original d18O data has time starting from the present day
p_data_initial=(p_data_initial)
=======
# pagesII_data_initial = (tmp + tmp2) / 2.0
pagesII_data_initial = tmp
pagesII_data_initial2 = tmp2
p_year = tmp_yr #
# pagesII_data_initial=scipy.signal.detrend(pagesII_data_initial)
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b





##########################################
# solar
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/solar_cmip5/tsi_VK.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 4,delimiter = ' ')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
solar_data = tmp
solar_year = tmp_yr


##########################################
# volc
##########################################

m_data_file = '/data/NAS-ph290/ph290/cmip5/forcing_data/ICI5_030N_AOD_c.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 0,delimiter = '\t')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
volc_data = tmp
volc_year = tmp_yr


##########################################
# insolation
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/insol91.jun_dec'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ' ')
tmp1 = m_data[:,2]
tmp2 = m_data[:,9]
tmp_yr = m_data[:,0]
f1 = scipy.interpolate.interp1d(tmp_yr, tmp1)
f2 = scipy.interpolate.interp1d(tmp_yr, tmp2)
ins_year = np.linspace(start_year,end_year,(end_year - start_year + 1))
ins_june = f1(ins_year)
ins_dec = f2(ins_year)


##########################################
# Anthro radiative forcing
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/20THCENTURY_MIDYEAR_RADFORCING.DAT'
m_data = np.genfromtxt(m_data_file,skip_header = 61,delimiter = ',')
tmp1 = m_data[:,5]
tmp_yr = m_data[:,1]
anthro_forcing = tmp1
anthro_year = tmp_yr




##########################
# detrending each model individually
##########################


for i,model in enumerate(models):
<<<<<<< HEAD
	mm_mean_t[i,:] = scipy.signal.detrend(mm_mean_t[i,:])
	mm_mean_s[i,:] = scipy.signal.detrend(mm_mean_s[i,:])
	# mm_mean_sic[i,:] = scipy.signal.detrend(mm_mean_sic[i,:])
	#~ mm_mean_d18O[i,:] = scipy.signal.detrend(mm_mean_d18O[i,:])
	mm_mean_d18O[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0, d18O_sens1)
	mm_mean_d18O2[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0, d18O_sens2)
=======
	# mm_mean_t[i,:] = scipy.signal.detrend(mm_mean_t[i,:])
	# mm_mean_s[i,:] = scipy.signal.detrend(mm_mean_s[i,:])
	# mm_mean_sic[i,:] = scipy.signal.detrend(mm_mean_sic[i,:])
	#~ mm_mean_d18O[i,:] = scipy.signal.detrend(mm_mean_d18O[i,:])
# 	mm_mean_d18O[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0, d18O_sens1)
# 	mm_mean_d18O2[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0, d18O_sens2)
	mm_mean_d18O[i,:] = calc_d18O(mm_mean_t[i,:],mm_mean_s[i,:], d18O_sens1)
	mm_mean_d18O2[i,:] = calc_d18O(mm_mean_t[i,:],mm_mean_s[i,:], d18O_sens2)	
	# mm_mean_d18O0[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0, d18O_sens0)
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
	mm_mean_d18O_s_only[i,:] = calc_d18O(273.15+(mm_mean_t[i,:]*0.0) +5.0,mm_mean_s[i,:]+35.0, d18O_sens1)
	mm_mean_d18O_t_only[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,0.0*mm_mean_s[i,:]+35.0,d18O_sens1)
	mm_mean_d18O[i,:] -= np.nanmean(mm_mean_d18O[i,:])
	mm_mean_d18O2[i,:] -= np.nanmean(mm_mean_d18O2[i,:])
<<<<<<< HEAD
=======
	#mm_mean_d18O0[i,:] -= np.nanmean(mm_mean_d18O0[i,:])
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
	mm_mean_d18O_s_only[i,:] -= np.nanmean(mm_mean_d18O_s_only[i,:])
	mm_mean_d18O_t_only[i,:] -= np.nanmean(mm_mean_d18O_t_only[i,:])

##########################
#  band pass filter
##########################



#!!  Do you want to band-pass filter?
band_pass_filter = False



def butter_bandpass(lowcut,  cutoff):
	order = 2
	low = 1/lowcut
	b, a = scipy.signal.butter(order, low , btype=cutoff,analog = False)
	return b, a

def low_pass_filter(cube,limit_years):
		b1, a1 = butter_bandpass(limit_years, 'low')
		output = scipy.signal.filtfilt(b1, a1, cube,axis = 0)
		return output

def high_pass_filter(cube,limit_years):
		b1, a1 = butter_bandpass(limit_years, 'high')
		output = scipy.signal.filtfilt(b1, a1, cube,axis = 0)
		return output

mm_mean_t_filtered = mm_mean_t.copy()
mm_mean_s_filtered = mm_mean_s.copy()
mm_mean_d18O_filtered = mm_mean_d18O.copy()
mm_mean_d18O_filtered2 = mm_mean_d18O2.copy()
<<<<<<< HEAD
=======
# mm_mean_d18O_filtered0 = mm_mean_d18O0.copy()

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
mm_mean_d18O_filtered_s_only = mm_mean_d18O_s_only.copy()
mm_mean_d18O_filtered_t_only = mm_mean_d18O_t_only.copy()
# mm_mean_sic_filtered = mm_mean_sic.copy()

if band_pass_filter:

	upper_limit_years = 100.0
	lower_limit_years = 1.0

	for i,model in enumerate(models):
		y = mm_mean_t[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_t_filtered[i,:] = y
		y = mm_mean_s[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_s_filtered[i,:] = y
		y = mm_mean_d18O[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_d18O_filtered[i,:] = y
		y = mm_mean_d18O_s_only[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_d18O_filtered_s_only[i,:] = y
		y = mm_mean_d18O_t_only[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_d18O_filtered_t_only[i,:] = y
		y = bivalve_data_initial.copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		bivalve_data_initial = y
<<<<<<< HEAD

=======
		y = bivalve_data_initial_c.copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		bivalve_data_initial_c = y
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b



##########################
#  Multi Model mean
##########################

mm_mean_t_m_filtered = np.nanmean(mm_mean_t_filtered,axis = 0)
mm_mean_s_m_filtered = np.nanmean(mm_mean_s_filtered,axis = 0)
mm_mean_d18O_m_filtered = np.nanmean(mm_mean_d18O_filtered,axis = 0)
mm_mean_d18O_m_filtered_s_only = np.nanmean(mm_mean_d18O_filtered_s_only,axis = 0)
mm_mean_d18O_m_filtered_t_only = np.nanmean(mm_mean_d18O_filtered_t_only,axis = 0)

mm_mean_d18O_m_filtered2 = np.nanmean(mm_mean_d18O_filtered2,axis = 0)
<<<<<<< HEAD

# mm_mean_sic_m_filtered = np.nanmean(mm_mean_sic_filtered,axis = 0)

=======
# mm_mean_d18O_m_filtered0 = np.nanmean(mm_mean_d18O_filtered0,axis = 0)

# mm_mean_sic_m_filtered = np.nanmean(mm_mean_sic_filtered,axis = 0)



>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
##########################
#  Normalisation
##########################

y = bivalve_data_initial.copy()
x = bivalve_year.copy()
<<<<<<< HEAD

y -= np.min(y)
y /= np.max(y)

=======
y -= np.min(y)
y /= np.max(y)

y_c = bivalve_data_initial_c.copy()
y_c -= np.min(y_c)
y_c /= np.max(y_c)

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
mm_mean_t_m_filtered -= np.min(mm_mean_t_m_filtered)
mm_mean_t_m_filtered /= np.max(mm_mean_t_m_filtered)

mm_mean_s_m_filtered -= np.min(mm_mean_s_m_filtered)
mm_mean_s_m_filtered /= np.max(mm_mean_s_m_filtered)

mm_mean_d18O_m_filtered -= np.min(mm_mean_d18O_m_filtered)
mm_mean_d18O_m_filtered /= np.max(mm_mean_d18O_m_filtered)

mm_mean_d18O_m_filtered2 -= np.min(mm_mean_d18O_m_filtered2)
mm_mean_d18O_m_filtered2 /= np.max(mm_mean_d18O_m_filtered2)

<<<<<<< HEAD
=======
# mm_mean_d18O_m_filtered0 -= np.min(mm_mean_d18O_m_filtered0)
# mm_mean_d18O_m_filtered0 /= np.max(mm_mean_d18O_m_filtered0)

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
mm_mean_d18O_m_filtered_s_only -= np.min(mm_mean_d18O_m_filtered_s_only)
mm_mean_d18O_m_filtered_s_only /= np.max(mm_mean_d18O_m_filtered_s_only)

mm_mean_d18O_m_filtered_t_only -= np.min(mm_mean_d18O_m_filtered_t_only)
mm_mean_d18O_m_filtered_t_only /= np.max(mm_mean_d18O_m_filtered_t_only)

<<<<<<< HEAD
=======
nao_data_initial1 = nao_data_initial.copy() * -1.0
nao_data_initial1 -= np.min(nao_data_initial1)
nao_data_initial1 /= np.max(nao_data_initial1)

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b
# mm_mean_sic_m_filtered -= np.min(mm_mean_sic_m_filtered)
# mm_mean_sic_m_filtered /= np.max(mm_mean_sic_m_filtered)

##########################
# plotting
<<<<<<< HEAD
=======
#########################
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b



###########################
# d18O sensitivitry to salinity test
###########################


<<<<<<< HEAD

plt.close('all')
fig, ax = plt.subplots(3,1,figsize=(15, 5))


#bivalve d18O
ax[0].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[0].plot(expected_years,mm_mean_d18O_m_filtered,'b',alpha = 0.5,linewidth = 1.5)
#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)
=======
with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VI.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)


models = np.array(models)
loc = np.where(models == model)[0][0]

sf = max_strm_fun_26[loc]
sf -= np.min(sf)
sf /= np.max(sf)


my = model_years[loc]


plt.close('all')
fig, ax = plt.subplots(2,1,figsize=(12, 6))



t0 = start_year
t1 = 1200
# t1 = 1000
t2 = 1500
t3 = 1850

# ax[0].plot([t0,t1],[d18O_sens0,d18O_sens0],'k',alpha = 0.8,linewidth = 1.5)
ax[0].plot([t1,t2],[d18O_sens1,d18O_sens1],'k',alpha = 0.8,linewidth = 1.5)
ax[0].plot([t2,t3],[d18O_sens2,d18O_sens2],'k',alpha = 0.8,linewidth = 1.5)

>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

#bivalve d18O
ax[1].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
<<<<<<< HEAD
ax[1].plot(expected_years,mm_mean_d18O_m_filtered2,'b',alpha = 0.5,linewidth = 1.5)
#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)


#
ax[2].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#
p_data_initial -= np.min(p_data_initial)
p_data_initial /= np.max(p_data_initial)
ax[2].plot(p_year,p_data_initial,'r',alpha = 0.5,linewidth = 1.5)
#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)


#bivalve d18O
ax[0].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#bivalve d13C MOVE FROM READING THROUGH TO THIS POINT!
ax[0].plot(expected_years,mm_mean_d18O_m_filtered2,'b',alpha = 0.5,linewidth = 1.5)
#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)



ax[0].set_xlim(950,1850)
ax[1].set_xlim(950,1850)
ax[2].set_xlim(950,1850)


ax[0].set_title('bivalve d18O NOT DETRENDED (black), model derived d18O low sensitivity (blue)')
# '+models[0])
ax[1].set_title('bivalve d18O NOT DETRENDED  (black), model derived d18O high sensitivity (blue)')
ax[1].set_title('bivalve d18O NOT DETRENDED  (black), pages, europe/arctic (red)')

and add 

# plt.savefig('/home/ph290/Documents/figures/tmp/s_d18O_sens.png')
#'+models[0]+'.png')
plt.show(block = False)
=======
loc1 = np.where((expected_years >= t0) & (expected_years <= t1))
loc2 = np.where((expected_years >= t1) & (expected_years <= t2))
loc3 = np.where((expected_years >= t2) & (expected_years <= t3))

# ax[1].plot(expected_years[loc1],mm_mean_d18O_m_filtered0[loc1],'b',alpha = 0.5,linewidth = 1.5)
ax[1].plot(expected_years[loc2],mm_mean_d18O_m_filtered[loc2],'b',alpha = 0.5,linewidth = 1.5)
ax[1].plot(expected_years[loc3],mm_mean_d18O_m_filtered2[loc3],'b',alpha = 0.5,linewidth = 1.5)
# ax[1].plot(nao_year,nao_data_initial1,'r',alpha = 0.5,linewidth = 1.5)

# ax[1].plot(my,sf,'r',alpha = 0.5,linewidth = 1.5)

#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)
# y2 = 1 + volc_data * -1.0
y2 = volc_data - 0.1
ax[1].plot(volc_year,y2,'r',alpha = 0.8,linewidth = 1.5)


# #bivalve d18O
# ax[1].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
# #Synthetic d18O
# ax[1].plot(expected_years,mm_mean_d18O_m_filtered2,'b',alpha = 0.5,linewidth = 1.5)
# #~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)
solar_data -= np.min(solar_data)
solar_data /= np.max(solar_data)
y2 = 1.2 + solar_data * -0.5
# y2 =solar_data
ax[1].plot(solar_year,y2,color = '#808080',alpha = 0.8,linewidth = 1.5)

"""
#bivalve d18O
ax[2].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#bivalve d13C
ax[2].plot(x,y_c,'g',alpha = 0.8,linewidth = 1.5)
#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)


#
ax[3].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)


pagesII_data_initialb = pagesII_data_initial.copy() * -1.0
pagesII_data_initialb -= np.min(pagesII_data_initialb)
pagesII_data_initialb /= np.max(pagesII_data_initialb)
ax[3].plot(p_year,pagesII_data_initialb,'r',alpha = 0.5,linewidth = 1.5)
pagesII_data_initial2b = pagesII_data_initial2.copy() * -1.0
pagesII_data_initial2b -= np.min(pagesII_data_initial2b)
pagesII_data_initial2b /= np.max(pagesII_data_initial2b)
ax[3].plot(p_year,pagesII_data_initial2b,'y',alpha = 0.5,linewidth = 1.5)
# ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)

"""

ax[0].set_ylim(0,2.5)
ax[1].set_ylim(-0.1,1.2)

ax[0].set_xlim(start_year,1850)
ax[1].set_xlim(start_year,1850)
# ax[2].set_xlim(start_year,1850)
# ax[3].set_xlim(start_year,1850)


ax[0].set_title('d18O S-sensitivity')
# '+models[0])
ax[1].set_title('bivalve d$^{18}$O (black), model derived d$^{18}$O (blue)')
# ax[2].set_title('bivalve d18O NOT DETRENDED  (black), bivalve d13C (green))')
# ax[3].set_title('bivalve d18O NOT DETRENDED  (black), pages, europe/arctic (red)')

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/tmp/s_d18O_sens4.png')
#'+models[0]+'.png')
plt.show(block = False)



'''
for i,model in enumerate(models):
	x = rm.running_mean(mm_mean_d18O_filtered2[i,:],5)
	x -= np.nanmin(x)
	x /= np.nanmax(x)
	plt.plot(x,color = '#d3d3d3',lw=3,alpha=0.5)


x=mm_mean_d18O_m_filtered2
x -= np.nanmin(x)
x /= np.nanmax(x)
plt.plot(x,'k',lw=2)

plt.show()


'''
>>>>>>> 1c69e056c956384ed184d8e4a06cec4d381bcc9b

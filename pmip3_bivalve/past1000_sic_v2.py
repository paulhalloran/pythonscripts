
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
import scipy.stats as stats
import pandas as pd

#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory,variable):
	files = glob.glob(directory+'/*'+variable+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


#################
# correlations
#################




def smoothed_correlations(a,b,smoothings1,smoothings2):
	a = scipy.signal.detrend(a)
	a /= np.nanstd(a)
	b = scipy.signal.detrend(b)
	b /= np.nanstd(b)
	coeff_det_a_b_sig= np.zeros([np.size(smoothings1),np.size(smoothings2)])
	coeff_det_a_b_sig[:] = np.NAN
	coeff_det_a_b_not_sig = coeff_det_a_b_sig.copy()
	for smoothing_no1,smoothing1 in enumerate(smoothings1):
	        smoothing1 = np.int(smoothing1)
	        print smoothing_no1,' out of ',np.size(smoothings1)
	        window_type = 'boxcar'
	        for smoothing_no2, smoothing2 in enumerate(smoothings2):
	                smoothing2 = np.int(smoothing2)
	                x = pd.rolling_window(a.copy(),smoothing1,win_type=window_type,center=True)
	                y = pd.rolling_window(b.copy(),smoothing2,win_type=window_type,center=True)
	                loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
	                x = x[loc]
	                y = y[loc]
	                slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
	                r2 = r_value**2
	                coeff_det_a_b_sig[smoothing_no1,smoothing_no2] = r_value
	                if p_value > 0.001:
	                        coeff_det_a_b_not_sig[smoothing_no1,smoothing_no2] = r_value
	return coeff_det_a_b_sig,coeff_det_a_b_not_sig



'''
#Main bit of code follows...
'''

#N Hem
lon_west1 = 0
lon_east1 = 360
lat_south1 = 0.0
lat_north1 = 90.0

#Atl sector of Arctic
lon_west1 = -45.0
lon_east1 = 25.0
lat_south1 = 0.0
lat_north1 = 90.0

region1 = iris.Constraint(longitude=lambda v: lon_west1 <= v <= lon_east1,latitude=lambda v: lat_south1 <= v <= lat_north1)


variables = np.array(['sic'])
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.


input_directory2 = '/data/NAS-ph290/ph290/cmip5/last1000/monthly/'

modelsb = model_names(input_directory2,variables[0])

cube1 = iris.load_cube(input_directory2+modelsb[0]+'*'+variables[0]+'*.nc')[0]

modelbs2 = []
cubes_n_hem = []
ts_n_hem = []

for model in modelsb:
	print 'processing: '+model
	try:
		cube = iris.load_cube(input_directory2+model+'*'+variables[0]+'*.nc')
	except:
		cube = iris.load(input_directory2+model+'*'+variables[0]+'*.nc')
		cube = cube[0]
	# iris.coord_categorisation.add_month(cube, 'time', name='month')
	# cube = cube[np.where(cube.coord('month').points == 'Mar')]
	iris.coord_categorisation.add_year(cube, 'time', name='year')
	# iris.coord_categorisation.add_season(cube, 'time', name='season')
	# iris.coord_categorisation.add_season_year(cube, 'time', name='season_year')
	# cube = cube.aggregated_by(['season_year','season'], iris.analysis.MEAN)
	# cube = cube[np.where(cube.coord('season').points == 'son')]
	cube = cube.aggregated_by(['year'], iris.analysis.MEAN)
	if model == ('MRI-CGCM3'):
		for i in range(cube.shape[0]):
			cube.data.mask[i] = cube1.data.mask
	tmp1 = cube.extract(region1)
	cubes_n_hem.append(tmp1)
	#qplt.contourf(tmp1[0])
	#plt.show()
	ts_n_hem.append(tmp1.collapsed(['latitude','longitude'],iris.analysis.MEAN))



# reynolds_file = '/home/ph290/data0/reynolds/ultra_data.csv'
reynolds_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
reynolds_data = np.genfromtxt(reynolds_file,skip_header = 1,delimiter = ',')

tmp = np.shape(cubes_n_hem[0].data)
# tmp_data = np.ma.empty([np.size(cubes_n_hem),tmp[0],tmp[1],tmp[2]])
tmp_data = np.ma.empty([np.size(cubes_n_hem),1000,tmp[1],tmp[2]])

for i in np.arange(np.size(cubes_n_hem)):
	tmp_data[i] = cubes_n_hem[i].data[0:1000]



coord = ts_n_hem[0].coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])



data = np.zeros([1000,np.size(ts_n_hem)])
j=0
for i,ts in enumerate(ts_n_hem):
	print modelsb[i]
	data[:,j] = signal.detrend(ts.data[0:1000])
	# data[:,j] = (ts.data[0:1000])
	j += 1

multimodel_mean = data.mean(axis = 1)
multimodel_max = data.max(axis = 1)
multimodel_min = data.min(axis = 1)




###########
# plotting_cubes
###########

plt.close('all')
fig = plt.subplots(2,1,figsize=(16, 8))

#plt 1
ax1 = plt.subplot(2,1,1)
year2 = year[0]+np.arange(1000)
# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax1.plot(year2,y,'b',linewidth = 1.5, alpha=0.8,label = 'model mean seaice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax2 = ax1.twinx()
loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
ax2.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

ax1.set_ylabel('seaice')
ax2.set_ylabel('d18O')
ax1.set_xlim([900,1900])
ax1.set_ylim([-4.0,4.0])
ax2.set_ylim([-4.0,4.0])
ax1.plot([900,1900],[0,0],'k')



#plt 2
ax1 = plt.subplot(2,1,2)
lab1 = ax1.plot(year2,y,'b',linewidth = 1.5, alpha=0.8,label = 'model mean Atl. DJF sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

# ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = 1.5, alpha=0.5,label = 'd18O')
ax2 = ax1.twinx()
loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
lab2 = ax2.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = 1.5, alpha=0.5,label = 'bivalve d18O')

ax1.set_ylabel('seaice')
ax2.set_ylabel('d18O')
ax1.set_xlim([900,1900])
ax1.set_ylim([-4.0,4.0])
ax2.set_ylim([4.0,-4.0])
ax1.plot([900,1900],[0,0],'k')


ax1.set_xlabel('year')

# ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)
ax1.legend(prop={'size':10},loc=1).draw_frame(False)
ax2.legend(prop={'size':10},loc=4).draw_frame(False)
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice.png')
plt.show(block = False)



##############
# plot 2
##############


plt.close('all')
fig = plt.subplots(3,1,figsize=(16, 10))

#plt 1
ax1 = plt.subplot(3,1,1)

bins = [900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,1950]
x=[0.1,0.7,0.2,0.0,7.8,6.0,2.8,3.2,22.6,25.3,40.8,8.6] # duration of sea-ice (in weeks) * extend along N. Iceland coast
#not also https://www.jstor.org/tc/accept?origin=%2Fstable%2Fpdf%2F520132.pdf says cereal and grain cultivatoin ceased in north and beast before end of 12 century, and altogether near end of 16th C
ax1.step(bins,x,lw=3)
ax1.set_ylabel('Ice off Iceland')
plt.text(1200, 60, 'cereal and grain cultivation\nceased in N. and E.\nSchell, 1961', rotation=90)
plt.text(1600, 60, 'cereal and grain cultivation\nceased altogether\nSchell, 1961', rotation=90)

#plt 2
ax2 = plt.subplot(3,1,2)
year2 = year[0]+np.arange(1000)
# y = running_mean.running_mean(multimodel_mean-np.nanmean(multimodel_mean),1)
y = multimodel_mean-np.nanmean(multimodel_mean)
y /= np.std(y)
ax2.plot(year2,y,'b',linewidth = 1.5, alpha=0.8,label = 'model mean seaice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min / np.std(y),np.flipud(multimodel_max) / np.std(y)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

ax3 = ax2.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1200) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax3.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 1600) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
y_reynolds2 /= np.std(y_reynolds2)
ax3.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

ax2.set_ylabel('seaice')
ax3.set_ylabel('d18O')
ax1.set_xlim([900,1900])
ax2.set_xlim([900,1900])
ax1.set_ylim([0.0,50])
ax2.set_ylim([-4.0,4.0])
ax3.set_ylim([4.0,-4.0])
ax2.plot([900,1900],[0,0],'k')



#plt 2
ax1 = plt.subplot(3,1,3)
lab1 = ax1.plot(year2,y,'b',linewidth = 1.5, alpha=0.8,label = 'model mean Atl. ann mean sea-ice')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min,np.flipud(multimodel_max)])
# ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='blue', alpha=0.2)

# ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
# y_reynolds = reynolds_data[loc[0]:loc[-1],1]
# lab2, = ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = 1.5, alpha=0.5,label = 'd18O')
ax2 = ax1.twinx()
# loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
loc = np.where((reynolds_data[:,0] <= 1600) & (reynolds_data[:,0] >= 1200) )[0]

y_reynolds = reynolds_data[loc[0]:loc[-1],2]
y_reynolds2 = signal.detrend(y_reynolds)
# y_reynolds2 = y_reynolds - np.nanmean(y_reynolds)

y_reynolds2 /= np.std(y_reynolds2)
lab2 = ax2.plot(reynolds_data[loc[0]:loc[-1],0],y_reynolds2,'r',linewidth = 1.5, alpha=0.5,label = 'bivalve d18O')

ax1.set_ylabel('seaice')
ax2.set_ylabel('d18O')
ax1.set_xlim([900,1900])
ax1.set_ylim([-4.0,4.0])
ax2.set_ylim([-4.0,4.0])
ax1.plot([900,1900],[0,0],'k')


ax1.set_xlabel('year')

# ax1.legend([lab1],prop={'size':10},loc=1).draw_frame(False)
# ax2.legend([lab2],prop={'size':10},loc=4).draw_frame(False)
ax1.legend(prop={'size':10},loc=1).draw_frame(False)
ax2.legend(prop={'size':10},loc=4).draw_frame(False)
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_version2_ann_mean.png')
plt.show(block = False)

"""

##########

reynolds_data2 = np.flipud(reynolds_data)

smoothings1 = range(1,150,2)
smoothings2 = smoothings1


loc1 = np.where((year2 <= 1200) & (year2 >= 960) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1200) & (reynolds_data2[:,0] >= 960) )[0]

phase1_corr_sig,phase1_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)

loc1 = np.where((year2 <= 1600) & (year2 >= 1200) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1600) & (reynolds_data2[:,0] >= 1200) )[0]

phase2_corr_sig,phase2_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)

loc1 = np.where((year2 <= 1850) & (year2 >= 1600) )[0]
loc2 = np.where((reynolds_data2[:,0] <= 1849) & (reynolds_data2[:,0] >= 1600) )[0]

phase3_corr_sig,phase3_corr_not_sig = smoothed_correlations(y[loc1],reynolds_data2[loc2,2],smoothings1,smoothings2)



# plotting #

import matplotlib.cm as mpl_cm

cmap1 = mpl_cm.get_cmap('bwr')
cmap2 = mpl_cm.get_cmap('bwr')

plt.close('all')
fig = plt.subplots(1,3,figsize=(16, 4))

#plt 1
ax1 = plt.subplot(1,3,1)
cont1 = ax1.contourf(smoothings1,smoothings2,phase1_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax1.contourf(smoothings1,smoothings2,phase1_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax1.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
ax1.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
ax1.set_xlim(0,100)
ax1.set_ylim(0,100)
ax1.set_xlabel('bivalve d$^{18}$O smoothing (years)')
ax1.set_ylabel('model sea-ice smoothing (years)')
plt.colorbar(cont1)

#plt 2
ax2 = plt.subplot(1,3,2)
cont1 = ax2.contourf(smoothings1,smoothings2,phase2_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax2.contourf(smoothings1,smoothings2,phase2_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax2.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
ax2.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
ax2.set_xlim(0,100)
ax2.set_ylim(0,100)
ax2.set_xlabel('bivalve d$^{18}$O smoothing (years)')
ax2.set_ylabel('model sea-ice smoothing (years)')
plt.colorbar(cont1)

#plt 3
ax3 = plt.subplot(1,3,3)
cont1 = ax3.contourf(smoothings1,smoothings2,phase3_corr_sig,np.linspace(-1,1,31),hatches=['.'],cmap=cmap1)
ax3.contourf(smoothings1,smoothings2,phase3_corr_not_sig,np.linspace(-1,1,31),cmap=cmap2)
ax3.fill_between([20,200,200,20],[0,0,180,0],edgecolor = 'none', facecolor='white', alpha=0.8)
ax3.fill_between([0,0,180,0],[20,200,200,20],edgecolor = 'none', facecolor='white', alpha=0.8)
ax3.set_xlim(0,100)
ax3.set_ylim(0,100)
ax3.set_xlabel('bivalve d$^{18}$O smoothing (years)')
ax3.set_ylabel('model sea-ice smoothing (years)')
plt.colorbar(cont1)

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/bivalve_seaie_correlation.png')
plt.show(block = True)




'''
#spatial anlayis
'''


lon_west2 = -60
lon_east2 = +70
lat_south2 = 30.0
lat_north2 = 90.0

region2 = iris.Constraint(longitude=lambda v: lon_west2 <= v <= lon_east2,latitude=lambda v: lat_south2 <= v <= lat_north2)

mean_cube2 = mean_cube.copy()
mean_cube2.data = np.roll(mean_cube.data,180,axis = 2)
mean_cube2.coord('longitude').points = np.linspace(-180,180,360)

mean_cube2 = mean_cube2.extract(region2)

mca_cube = mean_cube2[0:250]
lia_cube = mean_cube2[400:700]

y_mca = y[0:250]
loc1 = np.where(y_mca > stats.nanmean(y))
loc2 = np.where(y_mca < stats.nanmean(y))

mca_cube_diff = mca_cube[loc1].collapsed('time',iris.analysis.MEAN)-mca_cube[loc2].collapsed('time',iris.analysis.MEAN)

y_lia = y[400:700]
loc1b = np.where(y_lia > stats.nanmean(y))
loc2b = np.where(y_lia < stats.nanmean(y))

lia_cube_diff = lia_cube[loc1b].collapsed('time',iris.analysis.MEAN)-lia_cube[loc2b].collapsed('time',iris.analysis.MEAN)

plt.close('all')
plt.figure(1)
qplt.contourf(mca_cube_diff,np.linspace(-15,15,31))
plt.gca().coastlines()
plt.title('high/low seaice in MCA')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_dmca.png')
#plt.show(block = False)


plt.figure(2)
qplt.contourf(lia_cube_diff,np.linspace(-15,15,31))
plt.gca().coastlines()
plt.title('high/low seaice in LIA')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_seaice_dlia.png')
#plt.show(block = False)


np.savetxt('/home/ph290/Documents/figures/seaice_cmip5.txt', np.c_[year2,y])



'''
#precipitation
'''

lon_west3 = -20+360
lon_east3 = -15+360
lat_south3 = 66
lat_north3 = 70

region3 = iris.Constraint(longitude=lambda v: lon_west3 <= v <= lon_east3,latitude=lambda v: lat_south3 <= v <= lat_north3)

modelsb_pr = model_names(input_directory2,variables[1])

cube1_pr = iris.load_cube(input_directory2+modelsb[0]+'*'+variables[1]+'*.nc')[0]

modelbs2_pr = []
cubes_n_hem_pr = []
ts_region_pr = []

for model in modelsb_pr:
        print 'processing: '+model
        try:
                cube = iris.load_cube(input_directory2+model+'*'+variables[1]+'*.nc')
        except:
                cube = iris.load(input_directory2+model+'*'+variables[1]+'*.nc')
                cube = cube[0]
        tmp1 = cube.extract(region1)
        cubes_n_hem_pr.append(tmp1)
        #qplt.contourf(tmp1[0],50)
        #plt.show()
	tmp3 = cube.extract(region3)
        ts_region_pr.append(tmp3.collapsed(['latitude','longitude'],iris.analysis.MEAN))

data = np.zeros([1000,np.size(ts_region_pr)])
j=0
for i,ts in enumerate(ts_region_pr):
        print modelsb_pr[i]
        data[:,j] = signal.detrend(ts.data[0:1000])
        j += 1

multimodel_mean_pr = data.mean(axis = 1)
multimodel_max_pr = data.max(axis = 1)
multimodel_min_pr = data.min(axis = 1)



plt.close('all')
fig = plt.subplots(2,1,figsize=(16, 7))

#plt 1
ax1 = plt.subplot(2,1,1)
year2 = year[0]+np.arange(1000)
y = running_mean.running_mean(multimodel_mean_pr-np.mean(multimodel_mean_pr),1)
ax1.plot(year2,y,'g',linewidth = 1.5, alpha=0.8,label = 'model mean precipitation')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min_pr,np.flipud(multimodel_max_pr)])
ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='green', alpha=0.2)

ax2 = ax1.twinx()
loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],1]
ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

ax1.set_ylabel('precip')
ax2.set_ylabel('d18OT')
ax1.set_xlim([900,1900])
ax1.set_ylim([0.000008,-0.000008])
ax1.plot([900,1900],[0,0],'k')


#plt2
ax1 = plt.subplot(2,1,2)
year2 = year[0]+np.arange(1000)
y = running_mean.running_mean(multimodel_mean_pr-np.mean(multimodel_mean_pr),1)
ax1.plot(year2,y,'g',linewidth = 1.5, alpha=0.8,label = 'model mean precipitation')
x_fill = np.concatenate([year2,np.flipud(year2)])
y_fill = np.concatenate([multimodel_min_pr,np.flipud(multimodel_max_pr)])
ax1.fill_between(x_fill,y_fill,edgecolor = 'none', facecolor='green', alpha=0.2)

ax2 = ax1.twinx()
loc = np.where((reynolds_data[:,0] <= 1850) & (reynolds_data[:,0] >= 850) )[0]
y_reynolds = reynolds_data[loc[0]:loc[-1],2]
ax2.plot(reynolds_data[loc[0]:loc[-1],0],running_mean.running_mean(signal.detrend(y_reynolds),1),'r',linewidth = 1.5, alpha=0.5,label = 'd18O')

ax1.set_ylabel('precip')
ax2.set_ylabel('d18OT')
ax1.set_xlim([900,1900])
ax1.set_ylim([-0.000008,0.000008])
ax1.plot([900,1900],[0,0],'k')

plt.savefig('/home/ph290/Documents/figures/multi_model_mean_precip.png')
#plt.show()



'''
precipitation, spatial
'''

tmp = np.shape(cubes_n_hem_pr[0].data)
tmp_data = np.ma.empty([np.size(cubes_n_hem),1000,tmp[1],tmp[2]])

for i in np.arange(np.size(cubes_n_hem)):
        tmp_data[i] = cubes_n_hem[i].data[0:1000]


mean_cube = cubes_n_hem[0].copy()
mean_cube.data = np.mean(tmp_data,axis = 0)


mca_cube = mean_cube2[0:250]
lia_cube = mean_cube2[400:700]

y_mca = y[0:250]
loc1 = np.where(y_mca > stats.nanmean(y))
loc2 = np.where(y_mca < stats.nanmean(y))

mca_cube_diff = mca_cube[loc1].collapsed('time',iris.analysis.MEAN)-mca_cube[loc2].collapsed('time',iris.analysis.MEAN)

y_lia = y[400:700]
loc1b = np.where(y_lia > stats.nanmean(y))
loc2b = np.where(y_lia < stats.nanmean(y))

lia_cube_diff = lia_cube[loc1b].collapsed('time',iris.analysis.MEAN)-lia_cube[loc2b].collapsed('time',iris.analysis.MEAN)

plt.close('all')
plt.figure(1)
qplt.contourf(mca_cube_diff,np.linspace(-5,5,31))
plt.gca().coastlines()
plt.title('high/low pr in MCA')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_pr_dmca.png')
#plt.show(block = False)


plt.figure(2)
qplt.contourf(lia_cube_diff,np.linspace(-5,5,31))
plt.gca().coastlines()
plt.title('high/low pr in LIA')
plt.savefig('/home/ph290/Documents/figures/multi_model_mean_pr_dlia.png')
#plt.show(block = False)

"""

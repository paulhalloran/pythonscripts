
import numpy as np
import matplotlib.path as mpath
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

def model_names(directory,my_string):
	files = glob.glob(directory+'/*'+my_string+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models



# directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
directory = '/data/dataSSD0/ph290/last1000'

lon_west = -24
lon_east = -13
lat_south = 65
lat_north = 67

start_year = 1200
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory,'pr_')
models = list(models)
models.remove('IPSL-CM5A-LR')
# models.remove('MIROC-ESM')
# models.remove('FGOALS-s2')  # No sic variable downloaded at moment...
# models.remove('MPI-ESM-P')
# models.remove('CSIRO-Mk3L-1-2')
models.remove('HadGEM2-ES') # No sic variable downloaded at moment...
# models.remove('HadCM3')
models.remove('bcc-csm1-1')
# models.remove('GISS-E2-R') # No sic variable downloaded at moment...





try:
	mm_mean_t
	test = False
except NameError:
	test = True


### uncomment if you want to force a new run ###
test = True


if test:
	mm_mean_p = np.zeros([np.size(models),np.size(expected_years)])
	mm_mean_p[:,:] = np.nan
	mm_mean_e = mm_mean_p.copy()
	mm_mean_s = mm_mean_p.copy()
	for i,model in enumerate(models):
		print i
		c0 = iris.load_cube(directory + model+'_pr_past1000_r1i1p1_regridded_not_vertically_Amon.nc')
		try:
			c1 = iris.load_cube(directory + model+'_evspsbl_past1000_r1i1p1_regridded.nc')
		except:
			c1 = iris.load_cube(directory + model+'_evspsbl_past1000_r1i1p121_regridded.nc')
		c2 = iris.load_cube(directory + model+'_so_past1000_r1i1p1_regridded_not_vertically_Omon.nc')[:,0,:,:]
		cube_region_tmp = c0.intersection(longitude=(lon_west, lon_east))
		c0_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		cube_region_tmp = c1.intersection(longitude=(lon_west, lon_east))
		c1_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		cube_region_tmp = c2.intersection(longitude=(lon_west, lon_east))
		c2_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		try:
			c0_region.coord('latitude').guess_bounds()
		except:
			print 'cube already has latitude bounds'
		try:
			c0_region.coord('longitude').guess_bounds()
		except:
			print 'cube already has longitude bounds' 
		try:
			c1_region.coord('latitude').guess_bounds()
		except:
			print 'cube already has latitude bounds'
		try:
			c1_region.coord('longitude').guess_bounds()
		except:
			print 'cube already has longitude bounds' 
		try:
			c2_region.coord('latitude').guess_bounds()
		except:
			print 'cube already has latitude bounds'
		try:
			c2_region.coord('longitude').guess_bounds()
		except:
			print 'cube already has longitude bounds'
		grid_areas = iris.analysis.cartography.area_weights(c0_region)
		c0_region_mean = c0_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		grid_areas = iris.analysis.cartography.area_weights(c1_region)
		c1_region_mean = c1_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		grid_areas = iris.analysis.cartography.area_weights(c2_region)
		c2_region_mean = c2_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		coord = c1.coord('time')
		dt = coord.units.num2date(coord.points)
		year = np.array([coord.units.num2date(value).year for value in coord.points])
		for j,y in enumerate(expected_years):
			loc = np.where(year == y)[0][0]
			if np.size(loc) > 0:
	# 			try:
					mm_mean_p[i,j] = c0_region_mean[loc].data
					mm_mean_e[i,j] = c1_region_mean[loc].data
					mm_mean_s[i,j] = c2_region_mean[loc].data
		# 		except:
		# 			print 'year missing'
		#
		
		
		
mm_mean_p_m = np.mean(mm_mean_p,axis = 0)
mm_mean_e_m = np.mean(mm_mean_e,axis = 0)
mm_mean_s_m = np.mean(mm_mean_s,axis = 0)

mm_mean_p_m -= np.min(mm_mean_p_m)
mm_mean_p_m /= np.max(mm_mean_p_m)

mm_mean_e_m -= np.min(mm_mean_e_m)
mm_mean_e_m /= np.max(mm_mean_e_m)

mm_mean_s_m -= np.min(mm_mean_s_m)
mm_mean_s_m /= np.max(mm_mean_s_m)

plt.plot(expected_years,rm.running_mean(mm_mean_s_m,5),'b')
plt.plot(expected_years,rm.running_mean(mm_mean_e_m,5),'r')
plt.plot(expected_years,rm.running_mean(mm_mean_p_m,5),'g')

plt.show()



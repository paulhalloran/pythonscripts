
from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta
import running_mean
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import scipy.interpolate
import gc
import pickle
import matplotlib.gridspec as gridspec


testing=False
calculate_stream_funct=False
interpolate_to_common_grid=False

resolution = 1

start_date = 850
end_date = 1850

#execfile('cmip5_stm_func_II.py')

base_dir = '/data/NAS-ph290/ph290/cmip5/last1000_vo_amoc/'
file_end = '_vo_past1000_r1i1p1_regridded_not_vertically.nc'

# if calculate_stream_funct:

'''
#producing an Atlantic mask (mask masked and Atlantic has value of 1, elsewhere zero)
'''

def calculate_mask(base_dir,file_end,model,resolution,start_date,end_date):
    input_file = base_dir+model+file_end
    #producing an Atlantic mask (mask masked and Atlantic has value of 1, elsewhere zero) to use in the stream function calculation
    cube = iris.load_cube(input_file)
    cube = cube[0,0]
    cube.data = ma.masked_where(cube.data == 0,cube.data)
    #tmp = cube.lazy_data()
    #tmp = biggus.ma.masked_where(tmp.ndarray() == 0,tmp.masked_array())



    tmp_cube = cube.copy()
    tmp_cube = tmp_cube*0.0

    location = -30/resolution

    print 'masking forwards'

    for y in np.arange(180/resolution):
        print 'lat: ',y,'of ',180/resolution
        flag = 0
        tmp = tmp_cube.data.mask[int(y),:]
        tmp2 = tmp_cube.data[int(y),:]
        for x in np.arange(360/resolution):
            if tmp[int(location)] == True:
                flag = 1
            if ((tmp[int(location)] == False) & (flag == 0)):
                tmp2[int(location)] = 1
            tmp = np.roll(tmp,+1)
            tmp2 = np.roll(tmp2,+1)
        tmp_cube.data.mask[int(y),:] = tmp
        tmp_cube.data.data[int(y),:] = tmp2.data

    location = location+1

    print 'masking backwards'

    for y in np.arange(180/resolution):
        print 'lon: ',y,'of ',180/resolution
        flag = 0
        tmp = tmp_cube.data.mask[int(y),:]
        tmp2 = tmp_cube.data[int(y),:]
        for x in np.arange(360/resolution):
            if tmp[int(location)] == True:
                flag = 1
            if ((tmp[int(location)] == False) & (flag == 0)):
                tmp2[int(location)] = 1
            tmp = np.roll(tmp,-1)
            tmp2 = np.roll(tmp2,-1)
        tmp_cube.data.mask[int(y),:] = tmp
        tmp_cube.data.data[int(y),:] = tmp2.data

    tmp_cube.data.data[150/resolution:180/resolution,:] = 0.0
    tmp_cube.data.data[0:40/resolution,:] = 0.0
    tmp_cube.data.data[:,20/resolution:180/resolution] = 0.0
    tmp_cube.data.data[:,180/resolution:280/resolution] = 0.0

    loc = np.where(tmp_cube.data.data == 0.0)
    tmp_cube.data.mask[loc] = True

    mask1 = tmp_cube.data.mask
    # cube_test = []
    return mask1

# else:
#     print 'stream function calculated so no need to calculate mask'



# plt.pcolormesh(mask1)
# plt.show()

'''
#calculating stream function
'''

#trying with the 1/4 degree dataset rather than the 1x1 - this should make the stram function calculatoi nmore robust
files = glob.glob(base_dir+'*_vo_*r1i1p1_*.nc')
#/media/usb_external1/cmip5/last1000_vo_amoc



models = []
strm_fun = {}
max_strm_fun = []
max_strm_fun_26 = []
max_strm_fun_45 = []
model_years = []



if calculate_stream_funct:

    for file in files:

        model = file.split('/')[-1].split('_')[0]
        if not((model == 'FGOALS-gl') | ((model == 'HadCM3'))):
            #note FGOALS-gl has vertical axis wrong way rond, causing problems
            print model
            models.append(model)

            mask1 = calculate_mask(base_dir,file_end,model,resolution,start_date,end_date)

            if not(testing):
                cube = iris.load_cube(file)
            else:
                cube = iris.load_cube(file)[0:10]

            print 'applying mask'

            try:
                            levels =  np.arange(cube.coord('depth').points.size)
            except:
                            levels = np.arange(cube.coord('ocean sigma over z coordinate').points.size)

        	#for level in levels:
        #		print 'level: '+str(level)
        #		for year in np.arange(cube.coord('time').points.size):
        #			#print 'year: '+str(year)
        #			tmp = cube.lazy_data()
        #			mask2 = tmp[year,level,:,:].masked_array().mask
        #			tmp_mask = np.ma.mask_or(mask1, mask2)
        #			tmp[year,level,:,:].masked_array().mask = tmp_mask

            #variable to hold data from first year of each model to check
            #that the maskls have been applied appropriately

            cube.coord('latitude').guess_bounds()
            cube.coord('longitude').guess_bounds()
            # grid_areas = iris.analysis.cartography.area_weights(cube[0])
            # grid_areas = np.sqrt(grid_areas)

            shape = np.shape(cube)
            tmp = cube[0].copy()
            tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
            tmp = tmp.collapsed('longitude',iris.analysis.SUM)
            collapsed_data = np.tile(tmp.data,[shape[0],1,1])

            mask_cube = cube[0].copy()
            tmp_mask = np.tile(mask1,[shape[1],1,1])
            mask_cube.data.mask = tmp_mask
            mask_cube.data.mask[np.where(mask_cube.data.data == mask_cube.data.fill_value)] = True



            print 'collapsing cube along longitude'
            try:
                    slices = cube.slices(['depth', 'latitude','longitude'])
            except:
                    slices = cube.slices(['ocean sigma over z coordinate', 'latitude','longitude'])
            for i,t_slice in enumerate(slices):
                    #print 'year:'+str(i)


                grid_areas = iris.analysis.cartography.cosine_latitude_weights(cube[0])
                grid_areas *= (40075000.0/(360.0 / resolution))
                # t_slice *= 0.0
                # t_slice += 1.0
                t_slice *= grid_areas
                mask_cube_II = t_slice.data.mask
                t_slice.data.mask = mask_cube.data.mask | mask_cube_II
                t_slice.data.data[np.where(t_slice.data.mask)]=np.nan
                # qplt.contourf(t_slice[0])
                # plt.show()

        # grid_areas = np.sqrt(grid_areas)

        # tmp.data.data[np.where(tmp.data.data == tmp.data.fill_value)] = np.NAN
        # tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
        # tmp *= grid_areas
        # mask_cube_II = tmp.data.mask
        # tmp.data.mask = mask_cube.data.mask | mask_cube_II
        # tmp.data.data[np.where(tmp.data.mask)] = np.NAN
                #if i == 0:
                    #plt.close('all')
                    #qplt.contourf(tmp[0])
                    #plt.savefig('/home/ph290/Documents/figures/test/'+model+'l1.png')
                    #plt.close('all')
                    #qplt.contourf(tmp[10])
                    #plt.savefig('/home/ph290/Documents/figures/test/'+model+'l10.png')

                # tmp = cube[0].copy()

                collapsed_data[i] = t_slice.collapsed('longitude',iris.analysis.SUM).data
                # plt.contourf(cube.coord('latitude').points,cube.coord('depth').points,collapsed_data[i]/1000.0)
                # plt.colorbar()
                # plt.show()
                    # need to test thst this actually gives a proper longitudinal collapse sum - testing 1ith 1 everywhere an value is wrong

            try:
                    depths = cube.coord('depth').points*-1.0
                    bounds = cube.coord('depth').bounds
            except:
                    depths = cube.coord('ocean sigma over z coordinate').points*-1.0
                    bounds = cube.coord('ocean sigma over z coordinate').bounds


            thickness = bounds[:,1] - bounds[:,0]
            test = thickness.mean()
            if test > 1:
                    thickness = bounds[1:,0] - bounds[0:-1,0]
                    thickness = np.append(thickness, thickness[-1])

            thickness = np.flipud(np.rot90(np.tile(thickness,[180/resolution,1])))


            tmp_strm_fun_26 = []
            tmp_strm_fun_45 = []
            tmp_strm_fun_max = []
            tmp_strm_fun = np.zeros(shape[0:3])
            tmp_strm_fun[:] = np.nan
            for i in np.arange(np.size(collapsed_data[:,0,0])):
                tmp = collapsed_data[i].copy()
                tmp = tmp*thickness
                tmp = np.nancumsum(np.flip(tmp,axis=0),axis = 0)
                tmp = np.flip(tmp,axis=0)
                tmp *= -1.0e-6
                tmp.data[np.where(tmp.mask)]=np.nan
                # tmp *= 1029.0 #conversion from m3 to kg
                coord = t_slice.coord('latitude').points
                loc = np.where(coord >= 26)[0][0]
                tmp_strm_fun_26 = np.append(tmp_strm_fun_26,np.nanmax(tmp[:,loc]))
                loc = np.where(coord >= 45)[0][0]
                tmp_strm_fun_45 = np.append(tmp_strm_fun_45,np.nanmax(tmp[:,loc]))
                tmp_strm_fun_max = np.append(tmp_strm_fun_max,np.nanmax(tmp[:,:]))
                tmp_strm_fun[i,:,:]=tmp

            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            years = np.array([coord.units.num2date(value).year for value in coord.points])
            model_years.append(years)

            max_strm_fun_26.append(tmp_strm_fun_26)
            max_strm_fun_45.append(tmp_strm_fun_45)
            max_strm_fun.append(tmp_strm_fun_max)
            strm_fun[model] = tmp_strm_fun


    model_meta = {}

    for file in files:
        model = file.split('/')[-1].split('_')[0]
        print model
        cube = iris.load_cube(file)
        model_meta[model]={}
        try:
            model_meta[model]['depth']=cube.coord('depth').points
        except:
            model_meta[model]['depth']=cube.coord('ocean sigma over z coordinate').points
        model_meta[model]['lats']=cube.coord('latitude').points
        coord = cube.coord('time')
        dt = coord.units.num2date(coord.points)
        years = np.array([coord.units.num2date(value).year for value in coord.points])
        model_meta[model]['years']=years

    with open('/home/ph290/Documents/python_scripts/pickles/amoc_sections.pickle', 'w') as f:
        pickle.dump([models,strm_fun,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,resolution,model_meta], f)

else:
    print 'stream functions aready calculated. reading in from file.'





with open('/home/ph290/Documents/python_scripts/pickles/amoc_sections.pickle', 'r') as f:
    [models,strm_fun,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,resolution,model_meta] = pickle.load(f)

if testing:
    for model in models:
        plt.contourf(model_meta[model]['lats'],model_meta[model]['depth'],np.nanmean(strm_fun[model],axis=0)*1.029*1e9,np.linspace(-3e10,3e10,21))
        plt.title(model)
        plt.ylim([6000,0])
        plt.colorbar()
        plt.show()


# for model in models:
#     print model
#     # model = models[1]
#     tmp2 = np.nanmean(strm_fun[model],axis=0)
#     plt.contourf(model_meta[model]['lats'],model_meta[model]['depth'],tmp2,np.linspace(-30,30,21))
#     plt.ylim([np.max(model_meta[model]['depth']),np.min(model_meta[model]['depth'])])
#     plt.xlim([0,60])
#     plt.colorbar()
#     plt.show()

#
# plt.contourf(model_meta[model]['lats'],model_meta[model]['depth'],np.nancumsum(collapsed_data[0]*thickness,axis=0)*1.0e-6,np.linspace(-30,30,21))
# plt.colorbar()
# plt.show()

#####################################################################
# calculating composites that correspond with sea-ice timeseries    #
#####################################################################

### interpolate onto common grid ###




from gridfill import fill
from scipy.interpolate import RectBivariateSpline


depths_for_interpolation = [5.00000000e+00, 1.50000000e+01, 2.50000000e+01, 3.50000000e+01,
       4.50000000e+01, 5.50000000e+01, 6.50000000e+01, 7.50000000e+01,
       8.50000000e+01, 9.50000000e+01, 1.05000000e+02, 1.15000000e+02,
       1.25000000e+02, 1.35000000e+02, 1.45000000e+02, 1.55000000e+02,
       1.65098389e+02, 1.75479004e+02, 1.86291306e+02, 1.97660294e+02,
       2.09711395e+02, 2.22578293e+02, 2.36408798e+02, 2.51370193e+02,
       2.67654205e+02, 2.85483673e+02, 3.05119202e+02, 3.26868011e+02,
       3.51093506e+02, 3.78227600e+02, 4.08784607e+02, 4.43377686e+02,
       4.82736694e+02, 5.27727966e+02, 5.79372864e+02, 6.38862610e+02,
       7.07563293e+02, 7.87002502e+02, 8.78825195e+02, 9.84705933e+02,
       1.10620398e+03, 1.24456702e+03, 1.40049695e+03, 1.57394592e+03,
       1.76400293e+03, 1.96894397e+03, 2.18645703e+03, 2.41397192e+03,
       2.64900098e+03, 2.88938501e+03, 3.13340503e+03, 3.37979297e+03,
       3.62766992e+03, 3.87645190e+03, 4.12576807e+03, 4.37539160e+03,
       4.62518994e+03, 4.87508301e+03, 5.12502783e+03, 5.37500000e+03]

if interpolate_to_common_grid:

    kw = dict(eps=1e-4, relax=0.6, itermax=1e2, initzonal=False,cyclic=False, verbose=True)



    regridded_array = np.zeros([len(models),1150,len(depths_for_interpolation),180])
    regridded_array[:] = np.nan

    try:
        models.remove('FGOALS-gl') # depth coordinate is the wrong way up, causing the script problems
    except:
        pass


    for i,model in enumerate(models):
        print model
        tmp = strm_fun[model].copy()
        #turn nans into something the code can work with.
        tmp=np.ma.fix_invalid(tmp, copy=True, fill_value=-9.9e9)
        #fill gaps so not trying to interpolate with missing data
        tmp1, converged = fill(tmp, 2, 1, **kw)
        # tmp[np.logical_not(np.isfinite(tmp))] = -9.9e99
        #interpolate to common grid
        for j in range(np.shape(tmp1)[0]):
            tmp2=tmp1[j,:,:]
            f = RectBivariateSpline(model_meta[model]['depth'] , model_meta[model]['lats'] , tmp2)
            for k,d in enumerate(depths_for_interpolation):
                nearest_depth_ind = (np.abs(model_meta[model]['depth'] - d)).argmin()
                for m,l in enumerate(model_meta[model]['lats']):
                    nearest_lat_ind = (np.abs(model_meta[model]['lats'] - l)).argmin()
                    if tmp[0].mask[nearest_depth_ind,nearest_lat_ind]:
                        regridded_array[i,j,k,m] = np.nan
                    else:
                        regridded_array[i,j,k,m] = f(d,l)



    with open('/home/ph290/Documents/python_scripts/pickles/amoc_sectionsII.pickle', 'w') as f:
        pickle.dump([models,regridded_array], f)




with open('/home/ph290/Documents/python_scripts/pickles/amoc_sectionsII.pickle', 'r') as f:
    [models,regridded_array] = pickle.load(f)

# f, axarr = plt.subplots(3, sharex=True)
# a=ax1.pcolormesh(model_meta[model]['lats'],depths_for_interpolation,regridded_array[i,0,:,:],vmin=-30,vmax=30)
# b=ax2.pcolormesh(model_meta[model]['lats'],model_meta[model]['depth'],strm_fun[model][0],vmin=-30,vmax=30)
# b=ax3.plot[0,1]
# plt.colorbar(b)
# plt.show()



#
# def plot_prep(i,composites,c1):
# 	var_mean = np.nanmean(composites[i],axis=0)
# 	mean_var_cube = c1[0]
# 	tmp2 = np.ma.masked_array(var_mean)
# 	mean_var_cube.data = tmp2.data
# 	return mean_var_cube
#




import pandas as pd
df = pd.read_csv('/data/NAS-geo01/ph290/observations/iceland_timeseries.csv')
sea_ice_years = df['seaice_year'].values
sea_ice_extent = df['seaice'].values


# high_ice_years = sea_ice_years[np.where(sea_ice_extent > 0.1)]
# low_ice_years = sea_ice_years[np.where(sea_ice_extent < -0.1)]
high_ice_years = sea_ice_years[np.where(sea_ice_extent > 1.0)]
low_ice_years = sea_ice_years[np.where(sea_ice_extent < -1.0)]

df1 = pd.read_csv('/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/IVI2TotalInjection_501-2000Version2.txt',skiprows=15,delim_whitespace=True,names=['year','nh','sh','nh_and_sh'])
years_to_avoid_tmp = df1.query('nh_and_sh > 15.0')['year'].values

years_to_avoid=[]

#volcanic years + the followinh 10 years
for y in years_to_avoid_tmp:
	for i in range(10):
		years_to_avoid.append(y+i)

years_to_avoid = np.unique(years_to_avoid)

#remove volcanic years +x from high and low ice years
high_ice_years = [x for x in high_ice_years if x not in years_to_avoid]
low_ice_years = [x for x in low_ice_years if x not in years_to_avoid]

### calculate those multi-model mean composites ###

masked_array = np.ma.masked_invalid(regridded_array).copy()
regridded_array = np.nan_to_num(regridded_array).copy()

high_composite = np.ma.masked_array(np.zeros([np.size(models),len(depths_for_interpolation),180]))
high_composite[:] = np.NAN
low_composite = np.ma.masked_array(np.zeros([np.size(models),len(depths_for_interpolation),180]))
low_composite[:] = np.NAN
low_composite_abs = np.ma.masked_array(np.zeros([np.size(models),len(depths_for_interpolation),180]))
low_composite_abs[:] = np.NAN
high_composite_abs = np.ma.masked_array(np.zeros([np.size(models),len(depths_for_interpolation),180]))
high_composite_abs[:] = np.NAN
for i,model in enumerate(models):
# i=0
# model = models[i]
	# try:
    print i
    years = model_meta[model]['years']
    # data_detrended = scipy.signal.detrend(regridded_array[i], axis=0)
    # high_ice_indices = np.where(np.in1d(years, high_ice_years))[0]
    # high_composite[i] = np.mean(data_detrended[high_ice_indices],axis=0)
    # low_ice_indices = np.where(np.in1d(years, low_ice_years))[0]
    # low_composite[i] = np.mean(data_detrended[low_ice_indices],axis=0)
    high_ice_indices = np.where(np.in1d(years, high_ice_years))[0]
    high_composite[i] = np.mean(regridded_array[i][high_ice_indices],axis=0)
    high_composite_abs[i] = np.copy(high_composite[i])
    high_composite[i] /= np.std(high_composite[i])
    low_ice_indices = np.where(np.in1d(years, low_ice_years))[0]
    low_composite[i] = np.mean(regridded_array[i][low_ice_indices],axis=0)
    low_composite_abs[i] = np.copy(low_composite[i])
    low_composite[i] /= np.std(low_composite[i][:,90:180])



low_composite.mask = masked_array[0][0:np.shape(low_composite)[0]].mask
high_composite.mask = masked_array[0][0:np.shape(high_composite)[0]].mask
low_composite_abs.mask = masked_array[0][0:np.shape(low_composite)[0]].mask
high_composite_abs.mask = masked_array[0][0:np.shape(high_composite)[0]].mask

# for i,model in enumerate(models):
#     plt.contourf(model_meta[models[0]]['lats'],depths_for_interpolation,high_composite[i],20)
#     plt.colorbar()
#     plt.show()

# for i,model in enumerate(models):

#
#
# for i,model in enumerate(models):
#     plt.close('all')
#     plt.figure(figsize=(10,10))
#     gs = gridspec.GridSpec(28, 40)
#     ax1 = plt.subplot(gs[0:6,0:38])
#     ax1b = plt.subplot(gs[0:6,39])
#     ax2 = plt.subplot(gs[8:14,0:38])
#     ax2b = plt.subplot(gs[8:14,39])
#     ax3 = plt.subplot(gs[16:22,0:38])
#     ax3b = plt.subplot(gs[16:22,39])
#     a=ax1.pcolormesh(model_meta[models[0]]['lats'],depths_for_interpolation,high_composite[i],vmin=-30,vmax=30)
#     ax1.set_title(model)
#     plt.colorbar(a,cax=ax1b,orientation='vertical')
#     ax1.set_ylim([6000,0])
#     ax1.set_xlim([0,60])
#     b=ax2.pcolormesh(model_meta[models[0]]['lats'],depths_for_interpolation,low_composite[i],vmin=-30,vmax=30)
#     plt.colorbar(b,cax=ax2b,orientation='vertical')
#     ax2.set_ylim([6000,0])
#     ax2.set_xlim([0,60])
#     c=ax3.pcolormesh(model_meta[models[0]]['lats'],depths_for_interpolation,high_composite[i]-low_composite[i],vmin=-0.5,vmax=0.5)
#     plt.colorbar(c,cax=ax3b,orientation='vertical')
#     ax3.set_ylim([6000,0])
#     ax3.set_xlim([0,60])
#     plt.savefig('/home/ph290/Documents/figures/separate_amoc_high_v_low_ice_model_'+model+'.png')
#     # plt.show()



def calculate_agreement(run_composites,agreement):
    # run_composites=run_composites[lag]
    composites_agreement = run_composites[0].copy()
    composites_agreement[:] = 0.0
    for i in range(np.shape(run_composites)[1]):
        for j in range(np.shape(run_composites)[2]):
            tmp1 = run_composites[:,i,j]
            a = np.float(len(np.where(tmp1 > 0.0)[0]))
            b = np.float(len(np.where(tmp1 < 0.0)[0]))
            ratio = np.max([a/len(tmp1),b/len(tmp1)])
            # if b > 0.0:
            #     ratio = (a / b)
            #     # print 'ratio ',ratio
            if ratio > agreement:
                composites_agreement[i,j] = np.NAN
            if run_composites[:,i,j].all() == 0.0:
            	composites_agreement[i,j] = np.NAN
    return composites_agreement


agreement=0.66
high_minus_low_composite_agreement = calculate_agreement(high_composite-low_composite,agreement)
high_minus_low_composite_agreement.mask = masked_array[0][0].mask


minv = -3.0
maxv= 3.0
levs=51


plt.close('all')
plt.figure(figsize=(10,10))
gs = gridspec.GridSpec(28, 1)
ax1 = plt.subplot(gs[0:6,0])
ax2 = plt.subplot(gs[8:14,0])
ax3 = plt.subplot(gs[16:22,0])
ax4 = plt.subplot(gs[25:26, 0])


a=ax1.contourf(model_meta[models[0]]['lats'],depths_for_interpolation,np.mean(high_composite,axis=0),np.linspace(minv,maxv,levs),extend='both',cmap='bwr')
ax1.set_ylim([6000,0])
ax1.set_xlim([0,60])
ax1.set_title('high sea-ice extent multi-model mean normalised AMOC strength')
b=ax2.contourf(model_meta[models[0]]['lats'],depths_for_interpolation,np.ma.mean(low_composite,axis=0),np.linspace(minv,maxv,levs),extend='both',cmap='bwr')
ax2.set_ylim([6000,0])
ax2.set_xlim([0,60])
ax2.set_title('low sea-ice extent multi-model mean normalised AMOC strength')

c=ax3.contourf(model_meta[models[0]]['lats'],depths_for_interpolation,np.ma.mean(high_composite,axis=0)-np.ma.mean(low_composite,axis=0),np.linspace(minv,maxv,levs),extend='both',cmap='bwr')
c2=ax3.contour(model_meta[models[0]]['lats'],depths_for_interpolation,np.ma.mean(high_composite,axis=0)-np.ma.mean(low_composite,axis=0), np.linspace(-1.0,1.0,11),colors='k',linewidths=0.2)
ax3.clabel(c2, c2.levels, inline=True, fontsize=10,fmt='%1.1f')
agreement_result1 = ax3.pcolormesh(model_meta[models[0]]['lats'],depths_for_interpolation, high_minus_low_composite_agreement,cmap='gray',vmin=0,vmax=1,alpha=0.1)
ax3.set_title('high minus low sea-ice extent multi-model mean normalised AMOC strength difference')
ax3.set_ylim([6000,0])
ax3.set_xlim([0,60])
plt.colorbar(a, cax=ax4, orientation='horizontal')

plt.savefig('/home/ph290/Documents/figures/amoc_high_v_low_ice.png')
plt.show(block = False)




###############

my_vmin=-40
my_vmax =40
# model =models[0]
plt.close('all')
plt.figure(figsize=(10,20))
gs = gridspec.GridSpec((len(models)*5 +1) + len(models)*2, 2)
# gs = gridspec.GridSpec(41, 2)

ax=[]
axb=[]
for i,model in enumerate(models):
    ax.append(plt.subplot(gs[(5*i)+i*2:5*(i+1)+i*2,0]))
    axb.append(plt.subplot(gs[(5*i)+i*2:5*(i+1)+i*2,1]))

for i,model in enumerate(models):


    # ax1 =
    # ax2 = plt.subplot(gs[5,0])
    c=ax[i].contourf(model_meta[models[i]]['lats'],depths_for_interpolation,high_composite_abs[i],np.linspace(my_vmin,my_vmax,31),extend='both',cmap='bwr')
    # c2=ax1.contour(model_meta[models[i]]['lats'],depths_for_interpolation,high_composite_abs[i]-low_composite_abs[i],colors='k',linewidths=0.2)
    # ax1.clabel(c2, c2.levels, inline=True, fontsize=10,fmt='%1.1f')
    # agreement_result1 = ax3.pcolormesh(model_meta[models[0]]['lats'],depths_for_interpolation, high_minus_low_composite_agreement,cmap='gray',vmin=0,vmax=1,alpha=0.1)
    ax[i].set_title(model)
    ax[i].set_ylim([6000,0])
    ax[i].set_xlim([0,60])


    c=axb[i].contourf(model_meta[models[i]]['lats'],depths_for_interpolation,low_composite_abs[i],np.linspace(my_vmin,my_vmax,31),extend='both',cmap='bwr')
    # c2=ax1.contour(model_meta[models[i]]['lats'],depths_for_interpolation,high_composite_abs[i]-low_composite_abs[i],colors='k',linewidths=0.2)
    # ax1.clabel(c2, c2.levels, inline=True, fontsize=10,fmt='%1.1f')
    # agreement_result1 = ax3.pcolormesh(model_meta[models[0]]['lats'],depths_for_interpolation, high_minus_low_composite_agreement,cmap='gray',vmin=0,vmax=1,alpha=0.1)
    axb[i].set_title(model)
    axb[i].set_ylim([6000,0])
    axb[i].set_xlim([0,60])

gs = gridspec.GridSpec((len(models)*5 +1) + len(models)*2, 2)
ax_cbar = plt.subplot(gs[-1,0:2])
cbar = plt.colorbar(c, cax=ax_cbar, orientation='horizontal')
cbar.set_label('Sv')

plt.gcf().text(0.3, 0.9, 'high sea-ice composites',horizontalalignment='center', fontsize=14)
plt.gcf().text(0.7, 0.9, 'low sea-ice composites',horizontalalignment='center', fontsize=14)


plt.savefig('/home/ph290/Documents/figures/amoc_high_and_low_abs.png')
# plt.savefig('/home/ph290/Documents/figures/amoc_high_and_low_abs.pdf')
plt.show(block = False)
# ,vmin=-30,vmax=30

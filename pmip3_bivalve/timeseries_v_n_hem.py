
import numpy as np
import iris
import matplotlib.pyplot as plt
import glob
from scipy import signal
import scipy
import numpy.ma as ma
import os
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
import running_mean as rm
from scipy.signal import freqz
from scipy.signal import butter, lfilter



def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a







def smoothing_script(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    np.hanning, np.hamming, np.bartlett, np.blackman, np.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y


start_year = 950
end_year = 1750

smoothing = 5

# b1, a1 = butter_lowpass(1.0/smoothing, 1.0,6)
b2, a2 = butter_highpass(1.0/smoothing, 1.0,6)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################




#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial = bivalve_data_initial[::-1]  # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
bivalve_data_initial_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data_initial.copy())

bivalve_data_initial -= np.mean(bivalve_data_initial)
bivalve_data_initial /= np.std(bivalve_data_initial)

bivalve_data_initial_filtered -= np.mean(bivalve_data_initial_filtered)
bivalve_data_initial_filtered /= np.std(bivalve_data_initial_filtered)

##########################################
# Mann AMO data
##########################################
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)
amo_data_filtered = scipy.signal.filtfilt(b2, a2, amo_data.copy(),padlen=20)

amo_data -= np.mean(amo_data)
amo_data /= np.std(amo_data)

amo_data_filtered -= np.mean(amo_data_filtered)
amo_data_filtered /= np.std(amo_data_filtered)

amo_scr_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo_scr = np.genfromtxt(amo_scr_file, skip_header = 2)
amo_scr_yr = amo_scr[:,0]
amo_scr_data = amo_scr[:,1]
amo_scr_data_minus_2sigma = amo_scr[:,2]
amo_scr_data_sigma_diff = amo_scr_data_minus_2sigma + amo_scr_data
loc = np.where((np.logical_not(np.isnan(amo_scr_data_sigma_diff))) & (np.logical_not(np.isnan(amo_scr_data))) & (amo_scr_yr >= start_year) & (amo_scr_yr <= end_year))
amo_scr_yr = amo_scr_yr[loc]
amo_scr_data = amo_scr_data[loc]
amo_scr_data_sigma_diff = amo_scr_data_sigma_diff[loc]
#amo_scr_data -= np.min(amo_scr_data)
#amo_scr_data_sigma_diff-= np.min(amo_scr_data_sigma_diff)
#amo_scr_data /= np.max(amo_scr_data)
#amo_scr_data_sigma_diff /= np.max(amo_scr_data)
amo_scr_data=signal.detrend(amo_scr_data)
amo_scr_data_filtered = scipy.signal.filtfilt(b2, a2, amo_scr_data.copy(),padlen=0)

amo_scr_data -= np.mean(amo_scr_data)
amo_scr_data /= np.std(amo_scr_data)

amo_scr_data_filtered -= np.mean(amo_scr_data_filtered)
amo_scr_data_filtered /= np.std(amo_scr_data_filtered)



##########################################
# fennoscandinavia d13C
##########################################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]


loc = np.where((np.logical_not(np.isnan(s_d))) & (s_y >= start_year) & (s_y <= end_year))
s_year = s_y[loc[0]]
s_d = s_d[loc[0]]
# s_d=scipy.signal.detrend(s_d)
# s_d = scipy.signal.filtfilt(b1, a1, s_d)

s_d_filtered = scipy.signal.filtfilt(b2, a2, s_d,padlen=0)

s_d -= np.mean(s_d)
s_d /= np.std(s_d)

s_d_filtered -= np.mean(s_d_filtered)
s_d_filtered /= np.std(s_d_filtered)


##########################################
# Greenland STACK T https://www.clim-past.net/12/171/2016/cp-12-171-2016.pdf
#
#positive is warm:
# see text: "We conclude that any conversion of the NG stack..."
#
##########################################

greenland_stack_file = '/data/NAS-geo01/ph290/misc_data/Weissbach_2016/datasets/NG-stack.tab'
greenland_stack = np.genfromtxt(greenland_stack_file, skip_header = 14,delimiter='\t')
greenland_stack_year = greenland_stack[:,1]
greenland_stack_data = greenland_stack[:,2]
loc = np.where((np.logical_not(np.isnan(greenland_stack_data))) & (greenland_stack_year >= start_year) & (greenland_stack_year <= end_year))
greenland_stack_year = greenland_stack_year[loc]
greenland_stack_data = greenland_stack_data[loc]

greenland_stack_data=scipy.signal.detrend(greenland_stack_data)
greenland_stack_data_filtered = scipy.signal.filtfilt(b2, a2, greenland_stack_data.copy())

greenland_stack_data -= np.mean(greenland_stack_data)
greenland_stack_data /= np.std(greenland_stack_data)

greenland_stack_data_filtered -= np.mean(greenland_stack_data_filtered)
greenland_stack_data_filtered /= np.std(greenland_stack_data_filtered)


######################
# nhtemp-darrigo2006.txt
# see https://www.ipcc.ch/publications_and_data/ar4/wg1/en/figure-6-10.html looking ta the two that show this 1000s-1100s cooling
######################

darrigo2006_file = '/data/NAS-geo01/ph290/misc_data/nhtemp-darrigo2006.csv'
darrigo2006 = np.genfromtxt(darrigo2006_file, skip_header = 8,delimiter=',')
darrigo2006_year = darrigo2006[:,0]
darrigo2006_data = darrigo2006[:,2]
loc = np.where((np.logical_not(np.isnan(darrigo2006_data))) & (darrigo2006_year >= start_year) & (darrigo2006_year <= end_year))
darrigo2006_year = darrigo2006_year[loc]
darrigo2006_data = darrigo2006_data[loc]

darrigo2006_data=scipy.signal.detrend(darrigo2006_data)
darrigo2006_data_filtered = scipy.signal.filtfilt(b2, a2, darrigo2006_data.copy())

darrigo2006_data -= np.mean(darrigo2006_data)
darrigo2006_data /= np.std(darrigo2006_data)

darrigo2006_data_filtered -= np.mean(darrigo2006_data_filtered)
darrigo2006_data_filtered /= np.std(darrigo2006_data_filtered)

######################
# esper2002_nhem_temp.txt
# see https://www.ipcc.ch/publications_and_data/ar4/wg1/en/figure-6-10.html looking ta the two that show this 1000s-1100s cooling
######################

esper2002_file = '/data/NAS-geo01/ph290/misc_data/esper2002_nhem_temp.txt'
esper2002 = np.genfromtxt(esper2002_file, skip_header = 53)
esper2002_year = esper2002[:,0]
esper2002_data = esper2002[:,1]
loc = np.where((np.logical_not(np.isnan(esper2002_data))) & (esper2002_year >= start_year) & (esper2002_year <= end_year))
esper2002_year = esper2002_year[loc]
esper2002_data = esper2002_data[loc]

esper2002_data=scipy.signal.detrend(esper2002_data)
esper2002_data_filtered = scipy.signal.filtfilt(b2, a2, esper2002_data.copy())

esper2002_data -= np.mean(esper2002_data)
esper2002_data /= np.std(esper2002_data)

esper2002_data_filtered -= np.mean(esper2002_data_filtered)
esper2002_data_filtered /= np.std(esper2002_data_filtered)


##########################################
#Volcanic forcing
##########################################


expected_years = np.arange(start_year,end_year)

file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

# data1 = np.genfromtxt(file1)
# data2 = np.genfromtxt(file2)

data1 = np.genfromtxt(file2)
data2 = np.genfromtxt(file4)

data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]
data_tmp[:,1] = data2[:,1]

data = np.mean(data_tmp,axis = 1)
data_final = data1.copy()
data_final[:,1] = data

volc_years = data_final[:,0]

loc = np.where((volc_years >= 850) & (volc_years <= 1849))[0]
volc_years = data_final[loc,0]
volc_data = data_final[loc,1]

volc_data_yrly = expected_years.copy() * np.nan

for i,yr in enumerate(expected_years):
	loc = np.where(np.floor(volc_years) == yr)
	if np.size(loc) > 0:
		volc_data_yrly[i] = np.nanmean(volc_data[loc[0]])


##

data1 = np.genfromtxt(file1)

data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]

data_final = data1.copy()
data_final[:,1] = data

volc_years = data_final[:,0]

loc = np.where((volc_years >= 850) & (volc_years <= 1849))[0]
volc_years = data_final[loc,0]
volc_data = data_final[loc,1]

volc_data_yrly2 = expected_years.copy() * np.nan

for i,yr in enumerate(expected_years):
	loc = np.where(np.floor(volc_years) == yr)
	if np.size(loc) > 0:
		volc_data_yrly2[i] = np.nanmean(volc_data[loc[0]])



##################
# figure
##################


plt.close('all')
# plt.figure()
fig = plt.figure(figsize=(9, 11))
gs = gridspec.GridSpec(4, 1)

ax = fig.add_subplot(gs[0, 0])

# fig, (ax1) = plt.subplots(1, 1, sharex=True,figsize=(8,8))

# ax1.plot(s_y, s_d, color='black')
ax.plot(darrigo2006_year, darrigo2006_data, lw=1,alpha = 0.2, color='c')
ax.plot(darrigo2006_year, darrigo2006_data_filtered, lw=2,alpha = 0.5, color='c',label = 'darrigo2006 N. hem')

ax.plot(esper2002_year, esper2002_data, lw=1,alpha = 0.2, color='m')
ax.plot(esper2002_year, esper2002_data_filtered, lw=2,alpha = 0.5, color='m',label = 'esper2002 N. hem')

ax.plot(amo_yr, amo_data, lw=1,alpha = 0.2 , color='k')
ax.plot(amo_yr, amo_data_filtered, lw=2,alpha = 0.5 , color='k',label = 'Mann AMO')



plt.legend(loc=2,ncol=2,prop={'size':12}).draw_frame(False)

plt.xlabel('calendar year')
plt.ylabel('normalised value\n(+ve warmer)')

###
#
# ax2 = ax.twinx()
# ax2.plot(expected_years,volc_data_yrly,'c',alpha=0.8)
# plt.ylabel('cyan=volcanic index')

######################

ax = fig.add_subplot(gs[1, 0])

# fig, (ax1) = plt.subplots(1, 1, sharex=True,figsize=(8,8))

# ax1.plot(s_y, s_d, color='black')
ax.plot(darrigo2006_year, darrigo2006_data, lw=1,alpha = 0.2, color='c')
ax.plot(darrigo2006_year, darrigo2006_data_filtered, lw=2,alpha = 0.5, color='c',label = 'darrigo2006 N. hem')

ax.plot(esper2002_year, esper2002_data, lw=1,alpha = 0.2, color='m')
ax.plot(esper2002_year, esper2002_data_filtered, lw=2,alpha = 0.5, color='m',label = 'esper2002 N. hem')

# ax.plot(bivalve_year, bivalve_data_initial*-1.0, lw=1,alpha = 0.2 , color='r')
# ax.plot(bivalve_year, bivalve_data_initial_filtered*-1.0, lw=2,alpha = 0.5 , color='r',label = '-1 * Reynolds et al. bivalve $\delta^{18}$0')
#


loc1 = np.where((bivalve_year > 950) & (bivalve_year<=1200))[0]
loc2 = np.where((bivalve_year > 1200) & (bivalve_year<=1600))[0]
loc3 = np.where((bivalve_year > 1600) & (bivalve_year<=1850))[0]


ax.plot(bivalve_year[loc1], bivalve_data_initial[loc1]*-1.0, lw=1,alpha = 0.2 , color='r')
ax.plot(bivalve_year[loc1], bivalve_data_initial_filtered[loc1]*-1.0, lw=2,alpha = 0.5 , color='r',label = '-1 * Reynolds et al. bivalve $\delta^{18}$0')
ax.plot(bivalve_year[loc2], bivalve_data_initial[loc2], lw=1,alpha = 0.2 , color='r')
ax.plot(bivalve_year[loc2], bivalve_data_initial_filtered[loc2], lw=2,alpha = 0.5 , color='r',label = '-1 * Reynolds et al. bivalve $\delta^{18}$0')
ax.plot(bivalve_year[loc3], bivalve_data_initial[loc3]*-1.0, lw=1,alpha = 0.2 , color='r')
ax.plot(bivalve_year[loc3], bivalve_data_initial_filtered[loc3]*-1.0, lw=2,alpha = 0.5 , color='r',label = '-1 * Reynolds et al. bivalve $\delta^{18}$0')


plt.legend(loc=2,ncol=2,prop={'size':12}).draw_frame(False)

plt.xlabel('calendar year')
plt.ylabel('normalised value\n(+ve warmer)')

###
#
# ax2 = ax.twinx()
# ax2.plot(expected_years,volc_data_yrly,'c',alpha=0.8)
# plt.ylabel('cyan=volcanic index')

################

ax = fig.add_subplot(gs[2, 0])

ax.plot(darrigo2006_year, darrigo2006_data, lw=1,alpha = 0.2, color='c')
ax.plot(darrigo2006_year, darrigo2006_data_filtered, lw=2,alpha = 0.5, color='c',label = 'darrigo2006 N. hem')

ax.plot(esper2002_year, esper2002_data, lw=1,alpha = 0.2, color='m')
ax.plot(esper2002_year, esper2002_data_filtered, lw=2,alpha = 0.5, color='m',label = 'esper2002 N. hem')

ax.plot(s_year, s_d, lw=1,alpha = 0.2 , color='g')
ax.plot(s_year, s_d_filtered, lw=2,alpha = 0.5 , color='g',label = 'Young et al.\ntree ring $\delta^{13}$C')


plt.legend(loc=3,ncol=2,prop={'size':12}).draw_frame(False)

plt.xlabel('calendar year')
plt.ylabel('normalised value\n(+ve warmer/increased-cloud)')

###
#
# ax2 = ax.twinx()
# ax2.plot(expected_years,volc_data_yrly,'c',alpha=0.8)
# plt.ylabel('cyan=volcanic index')

################

ax = fig.add_subplot(gs[3, 0])

ax.plot(darrigo2006_year, darrigo2006_data, lw=1,alpha = 0.2, color='c')
ax.plot(darrigo2006_year, darrigo2006_data_filtered, lw=2,alpha = 0.5, color='c',label = 'darrigo2006 N. hem')

ax.plot(esper2002_year, esper2002_data, lw=1,alpha = 0.2, color='m')
ax.plot(esper2002_year, esper2002_data_filtered, lw=2,alpha = 0.5, color='m',label = 'esper2002 N. hem')

ax.plot(greenland_stack_year, greenland_stack_data, lw=1,alpha = 0.2 , color='b')
ax.plot(greenland_stack_year, greenland_stack_data_filtered, lw=2,alpha = 0.5 , color='b',label = 'Wei${\ss}$bach et al.,\nGreenland $\delta^{18}$0 stack')

plt.legend(loc=3,ncol=2,prop={'size':12}).draw_frame(False)


plt.xlabel('calendar year')
plt.ylabel('normalised value\n(+ve warmer/increased-cloud)')

###
#
# ax2 = ax.twinx()
# ax2.plot(expected_years,volc_data_yrly,'c',alpha=0.8)
# # ax2.plot(expected_years,volc_data_yrly2,'k',alpha=0.8)
# plt.ylabel('cyan=volcanic index')
#

#############

# plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/timeseries_v_n_hem.png')
plt.show(block = False)


# directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
directory = '/data/dataSSD0/ph290/last1000/'

lon_west = -24
lon_east = -13
lat_south = 65
lat_north = 67

start_year = 1200
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)



lon_west = -40
lon_east = 40
lat_south = 60
lat_north = 90

model = 'MIROC-ESM'
# model = 'MPI-ESM-P'

c0 = iris.load_cube(directory + model+'_pr_past1000_r1i1p1_regridded_not_vertically_Amon.nc')
c1 = iris.load_cube(directory + model+'_sic_past1000_r1i1p1_regridded*.nc')
cube_region_tmp = c0.intersection(longitude=(lon_west, lon_east))
c0_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
cube_region_tmp = c1.intersection(longitude=(lon_west, lon_east))
c1_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))


try:
	c0_region.coord('latitude').guess_bounds()
except:
	print 'cube already has latitude bounds'


try:
	c0_region.coord('longitude').guess_bounds()
except:
	print 'cube already has longitude bounds' 


try:
	c1_region.coord('latitude').guess_bounds()
except:
	print 'cube already has latitude bounds'


try:
	c1_region.coord('longitude').guess_bounds()
except:
	print 'cube already has longitude bounds' 



grid_areas = iris.analysis.cartography.area_weights(c0_region)
c0_region_mean = c0_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
grid_areas = iris.analysis.cartography.area_weights(c1_region)
c1_region_mean = c1_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)



mm_mean_p = np.zeros(np.size(expected_years))
mm_mean_p[:] = np.nan
mm_mean_sic = mm_mean_p.copy()


coord = c1.coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])
for j,y in enumerate(expected_years):
	loc = np.where(year == y)[0][0]
	if np.size(loc) > 0:
			mm_mean_p[j] = c0_region_mean[loc].data
			mm_mean_sic[j] = c1_region_mean[loc].data


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp[::-1]
tmp = r_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
bivalve_data_initial_c = tmp[::-1]
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
# bivalve_data_initial_c=scipy.signal.detrend(bivalve_data_initial_c)


###############
# plot
###############


# mm_mean_p_m = np.mean(mm_mean_p,axis = 0)
# mm_mean_sic_m = np.mean(mm_mean_sic,axis = 0)

mm_mean_p_m = mm_mean_p
mm_mean_sic_m = mm_mean_sic.copy() * -1.0

mm_mean_p_m -= np.nanmin(mm_mean_p_m)
mm_mean_p_m /= np.nanmax(mm_mean_p_m)

mm_mean_sic_m -= np.nanmin(mm_mean_sic_m)
mm_mean_sic_m /= np.nanmax(mm_mean_sic_m)


bivalve_data_initial -= np.nanmin(bivalve_data_initial)
bivalve_data_initial /= np.nanmax(bivalve_data_initial)



plt.close('all')
fig, ax = plt.subplots(1,1,figsize=(12, 6))


ax.plot(expected_years,rm.running_mean(mm_mean_p_m,5),'g',lw=2,alpha=0.75)
ax.plot(expected_years,rm.running_mean(mm_mean_sic_m,5),'b',lw=2,alpha=0.75)
plt.title(model)

# ax[0].plot(bivalve_year,bivalve_data_initial,'k',lw=2,alpha=0.75)

plt.savefig('/home/ph290/Documents/figures/tmp/precip_v_seaice_'+model+'.png')
plt.show(block = False)





"""

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation
import numpy as np
import glob
import scipy
from scipy import signal
import os

def detrend_cube(cube):
    cube2 = cube.collapsed('time',iris.analysis.MEAN)
    cube_data = cube.data
    cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    cube.data = cube_data_detrended
    return cube+cube2

def model_names(directory,var):
	files = glob.glob(directory+'*'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models

def extract_region(cube,lon_west,lon_east,lat_south,lat_north):
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    return cube_region_tmp.intersection(latitude=(lat_south, lat_north))


directory = '/data/NAS-ph290/ph290/cmip5/last1000/monthly/'
var = 'sic'

models = model_names(directory,var)

# for models in models_all:
#     models = [models]


lon_west = -180.0
lon_east = 180.0
lat_south = 45.0
lat_north = 90.0

cube = iris.load_cube(directory+models[0]+'_sic_past1000_r1i1p1_regridded.nc')
reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)

reg_cube_shape = np.shape(reg_cube)
data1 = np.zeros([len(models),reg_cube_shape[1],reg_cube_shape[2]])
data1[:] = np.nan
data = {}
data[0] = data1.copy()
data[1] = data1.copy()
data[2] = data1.copy()
data[3] = data1.copy()



for i,model in enumerate(models):
    cube = iris.load_cube(directory+model+'_sic_past1000_r1i1p1_regridded.nc')
    iris.coord_categorisation.add_year(cube, 'time', name='year')
    iris.coord_categorisation.add_season(cube, 'time', name='season')
    cube = cube[np.where(cube.coord('year').points <= 1850)]
    reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
    # reg_cube = detrend_cube(reg_cube)
    loc1 = np.where((reg_cube.coord('year').points < 1200))
    loc2 = np.where((reg_cube.coord('year').points >= 1200) & (reg_cube.coord('year').points < 1600))
    loc3 = np.where((reg_cube.coord('year').points >= 1600))
    loc4 = np.where((reg_cube.coord('year').points >= 0))
    locs = [loc1,loc2,loc3,loc4]
    for j,loc in enumerate(locs):
        tmp_data1 = reg_cube[loc].collapsed('time',iris.analysis.MEAN)
        try:
            fill_value = tmp_data1.data.fill_value
        except:
            fill_value = np.nan
        tmp_data1.data[np.where(tmp_data1.data == fill_value)] = np.nan
        data[j][i,:,:] = tmp_data1.data




plotting_cubes={}
plotting_cubes[0] = template_cube.copy()
plotting_cubes[1] = template_cube.copy()
plotting_cubes[2] = template_cube.copy()
plotting_cubes[3] = template_cube.copy()

for i in range(4):
    plotting_cubes[i].data = np.nanmean(data[i],axis=0)


##########
# plotting
##########

"""

import iris
import iris.plot as iplt
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import iris.analysis.cartography
import cartopy.feature as cfeature

min_color_value = -10
max_color_value = 10
min_color_value2 = 0
max_color_value2 = 100
cmap = plt.get_cmap('YlGnBu')

my_extent = [-30, 10, 55, 75]

plt.close('all')
fig = plt.figure()
ax1 = plt.subplot(131,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
ax1.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,66),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[0],np.linspace(min_color_value2,max_color_value2,66),cmap=cmap)
ax1.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('sic')
plt.title('phase 1')

ax2 = plt.subplot(132,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,66),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[1],np.linspace(min_color_value2,max_color_value2,66),cmap=cmap)
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('sic')
plt.title('phase 2')

ax3 = plt.subplot(133,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[2]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,66),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[2],np.linspace(min_color_value2,max_color_value2,66),cmap=cmap)
ax3.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('sic')
plt.title('phase 3')

# ax3 = plt.subplot(224,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
# ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,66),cmap=cmap)
# my_plot = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
# ax3.add_feature(cfeature.LAND)
# plt.gca().coastlines('10m')
# bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
# bar.set_label('sic')
# plt.title('full climatology')

# plt.savefig('/home/ph290/Documents/figures/model'+models[0]+'.png')
plt.savefig('/home/ph290/Documents/figures/last_mill_sic_composites.png')
plt.show(block = False)

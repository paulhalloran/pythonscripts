"""
import iris.plot as iplt
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import iris
import glob
import iris.experimental.concatenate
import iris.analysis
import iris.quickplot as qplt
import iris.analysis.cartography
import cartopy.crs as ccrs
import subprocess
from iris.coords import DimCoord
import iris.coord_categorisation
import matplotlib as mpl
import gc
import scipy
import scipy.signal as signal
from scipy.signal import kaiserord, lfilter, firwin, freqz
import monthly_to_yearly as m2yr
from matplotlib import mlab
import matplotlib.mlab as ml
import cartopy
import running_mean
import matplotlib.cm as mpl_cm
from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta


def model_names(directory,var):
        files = glob.glob(directory+'/*_'+var+'_*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


#palaeo_amo_prop_fig4.py


'''
#models
'''

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


template_cube = iris.load_cube(directory+'HadCM3'+'_tas_past1000_r1i1p1_regridded.nc')


coord = template_cube.coord('time')
dt = coord.units.num2date(coord.points)
expected_years = np.array([coord.units.num2date(value).year for value in coord.points])



models1 = set(model_names(directory,'tas'))
models2 = set(model_names(directory,'psl'))
models = list(models1.intersection(models2))
models.remove('FGOALS-gl')
models.remove('HadGEM2-ES')

def mm_array_funct(models,directory,variable,expected_years):
    mm_array = np.empty([np.size(models),np.size(expected_years),180,360]) * np.nan
    for i,model in enumerate(models):
    	print model
    	# model = models[0]
    	cube = iris.load_cube(directory+model+'_'+variable+'_past1000_r1i1p1_regridded.nc')
    	cube_data = cube.data
    	cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    	cube.data = cube_data_detrended
    	coord = cube.coord('time')
    	dt = coord.units.num2date(coord.points)
    	year = np.array([coord.units.num2date(value).year for value in coord.points])
    	for j,yr in enumerate(expected_years):
    # 		print yr
    		loc = np.where(year == yr)
    		if np.size(loc) > 0:
    			mm_array[i,j,:,:] = cube[loc[0][0],:,:].data
    return mm_array


mm_array = mm_array_funct(models,directory,'tas',expected_years)
mm_mean_tas = np.nanmean(mm_array,axis=0)
template_cube.data = mm_mean_tas

psl_cube = template_cube.copy()
mm_array = mm_array_funct(models,directory,'psl',expected_years)
mm_mean_psl = np.nanmean(mm_array,axis=0)
psl_cube.data = mm_mean_psl


##########################################
# Volcanic forcing
##########################################

file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

# data1 = np.genfromtxt(file1)
# data2 = np.genfromtxt(file2)

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)

data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]
data_tmp[:,1] = data2[:,1]

data = np.mean(data_tmp,axis = 1)
data_final = data1.copy()
data_final[:,1] = data

volc_years = data_final[:,0]

loc = np.where((volc_years >= 850) & (volc_years <= 1849))[0]
volc_years = data_final[loc,0]
volc_data = data_final[loc,1]

volc_data_yrly = expected_years.copy() * np.nan

for i,yr in enumerate(expected_years):
	loc = np.where(np.floor(volc_years) == yr)
	if np.size(loc) > 0:
		volc_data_yrly[i] = np.nanmean(volc_data[loc[0]])


# plt.plot(volc_data_yrly)


volc_yrs = np.where(volc_data_yrly > 0.075)



##########
#plotting
##########




lags_cube_temperature = template_cube[0:40].copy()
lags_cube_data = lags_cube_temperature.data
for i in range(np.shape(lags_cube_temperature)[0]):
    print i
    yr = i
    index = volc_yrs[0]+yr
    index = index[np.where(index < np.shape(template_cube)[0])]
    template_cube2 = template_cube.copy()[0:np.size(index)]
    template_cube2.data = template_cube.data[index,:,:]
    lags_cube_data[i,:,:] = (template_cube2.collapsed('time',iris.analysis.MEAN) - template_cube.collapsed('time',iris.analysis.MEAN)).data


lags_cube_temperature.data = lags_cube_data



lags_cube_psl = psl_cube[0:40].copy()
lags_cube_data = lags_cube_psl.data
for i in range(np.shape(lags_cube_psl)[0]):
    print i
    yr = i
    index = volc_yrs[0]+yr
    index = index[np.where(index < np.shape(psl_cube)[0])]
    template_cube2 = template_cube.copy()[0:np.size(index)]
    template_cube2.data = psl_cube.data[index,:,:]
    lags_cube_data[i,:,:] = (template_cube2.collapsed('time',iris.analysis.MEAN) - psl_cube.collapsed('time',iris.analysis.MEAN)).data


lags_cube_psl.data = lags_cube_data



###################

from matplotlib import ticker
import cartopy.feature as cfeature


plt.close('all')
plt.figure(figsize=(12, 10))

#Temperature

#1

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((3, 2), (0, 0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = lags_cube_temperature[0:10].collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('temperature anomaly')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('0-10 years')

#2

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((3, 2), (1,0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = lags_cube_temperature[10:20].collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('temperature anomaly')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('10-20 years')

#3

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((3, 2), (2,0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())


cube1 = lags_cube_temperature[20:30].collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('temperature anomaly')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('20-30 years')

#PSL

#1

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((3, 2), (0,1),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = lags_cube_psl[0:10].collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-15.0,15.0,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('PSL anomaly')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('0-10 years')

#2

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((3, 2), (1, 1),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = lags_cube_psl[10:20].collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-15.0,15.0,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('PSL anomaly')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('10-20 years')

#3

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((3, 2), (2, 1),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())


cube1 = lags_cube_psl[20:30].collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-15.0,15.0,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('PSL anomaly')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('20-30 years')

plt.tight_layout()

plt.savefig('/home/ph290/Documents/figures/tas_response_to_volcanoes_4_psl.png')

plt.show(block = False)


"""
##########################################
#plotting solar
##########################################


cube_temperature = template_cube.copy()
cube_psl = psl_cube.copy()

import iris.coord_categorisation
import iris.analysis.stats
iris.coord_categorisation.add_year(cube_psl, 'time', name='year')


##########################################
# solar
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/solar_cmip5/tsi_VK.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 4,delimiter = ' ')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= np.min(cube_psl.coord('year').points)) & (m_data[:,0] <= np.max(cube_psl.coord('year').points)))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
solar_data = tmp
solar_year = tmp_yr

def calculate_correlations(in_cube,timeseries_yr,timeseries_data):
    in_cube=in_cube[0:1000,:,:]
    correlations = np.zeros([180,360])
    correlations[:] = np.NAN
    ts_cube = in_cube.copy()
    ts_cube = iris.analysis.maths.multiply(ts_cube, 0.0)
    ts2 = np.swapaxes(np.swapaxes(np.tile(timeseries_data,[180,360,1]),1,2),0,1)
    ts_cube = iris.analysis.maths.add(ts_cube, ts2)
    out_cube = iris.analysis.stats.pearsonr(in_cube, ts_cube, corr_coords=['time'])
    correlations = out_cube.data
    out_cube = in_cube[0].copy()
    out_cube.data = correlations
    return out_cube

cube_temperature_correlations = calculate_correlations(cube_temperature,solar_year,solar_data)
cube_psl_correlations = calculate_correlations(cube_psl,solar_year,solar_data)

from matplotlib import ticker
import cartopy.feature as cfeature


plt.close('all')
plt.figure(figsize=(12, 10))

#Temperature

#1

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((2, 1), (0, 0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = cube_temperature_correlations
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('temperature R')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('t')

#2

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((2, 1), (1,0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = cube_psl_correlations
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.1,0.1,31),
			transform=ccrs.PlateCarree(),cmap='jet',extend='both')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('psl r')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('psl')

plt.tight_layout()

plt.savefig('/home/ph290/Documents/figures/tas_response_to_solar_4_psl.png')

plt.show(block = False)

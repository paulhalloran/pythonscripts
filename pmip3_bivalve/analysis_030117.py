

import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
	models = np.unique(models_tmp)
	return models


def calc_d18O(T,S):
	#S = ABSOLUTE SALINITY psu
	#T = ABSOLUTE T deg' C
	d18Osw_synth = ((0.61*S)-21.3)
	# d18Osw_synth = ((3.0*S)-105)
	#R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
	#Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
	# 	d18Osw_synth = ((0.55*S)-18.98)
	#LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
	# d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
	# d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
	#Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
	d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
	#From Reynolds - the -27 is to convert between SMOW and vPDB
	return d18Oc_synth



lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

out_dir = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'
#from ./palaeo_amo/latest_270716/calculate_T_and_S_no_regridding.py

#~ #########
#~ # intermediate processing step - just run once
#~ #########
#~
# files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*.nc')
#### files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*HadG*.nc')
#
# for i,my_file in enumerate(files):
# 	print 'processing ',i,' in ',np.size(files)
# 	name = my_file.split('/')[-1]
# 	test = glob.glob(out_dir+name)
# 	if np.size(test) == 0:
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = out_dir+name,  options = '-P 7')
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' '+my_file, output = out_dir+name,  options = '-P 7')



out_dir2 = '/data/NAS-ph290/ph290/cmip5/past1000/'

#########
# intermediate processing step - just run once
#########

files = glob.glob(out_dir2+'sic_OImon_*_past1000_r*_merged.nc')
### files = glob.glob('/'.join(out_dir2.split('/')[0:-2])+'/*HadG*.nc')

for i,my_file in enumerate(files):
	print 'processing ',i,' in ',np.size(files)
	name = my_file.split('/')[-1]
	test = glob.glob(out_dir2+'region_extracted_and_meaned_'+name)
	if np.size(test) == 0:
		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 -yearmean '+my_file, output = out_dir2+'region_extracted_and_meaned_regridded_'+name,  options = '-P 7')
		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -yearmean '+my_file, output = out_dir2+'region_extracted_and_meaned_'+name,  options = '-P 7')


# print and do seasonal mean???!!

#########
# main bit of code
#########



directory = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'
#using /home/ph290/Documents/python_scripts/palaeo_amo/latest_270716/calculate_T_and_S_no_regridding.py

start_year = 950
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory)
models = list(models)
# models.remove('MIROC-ESM')
# models.remove('FGOALS-s2')  # No sic variable downloaded at moment...
# models.remove('MPI-ESM-P')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadGEM2-ES') # No sic variable downloaded at moment...
# models.remove('HadCM3')
# models.remove('bcc-csm1-1')
# models.remove('GISS-E2-R') # No sic variable downloaded at moment...

##########################################
# Model Data # Summer T and S, to 100m mean
##########################################

# models_list = models
#
# for models in models_list:
# 	models = [models]

'''
mm_mean_t = np.zeros([np.size(models),np.size(expected_years)])
mm_mean_t[:,:] = np.nan
mm_mean_s = mm_mean_t.copy()
mm_mean_d18O = mm_mean_t.copy()
mm_mean_d18O_s_only = mm_mean_t.copy()
mm_mean_d18O_t_only = mm_mean_t.copy()

for i,model in enumerate(models):
	print i
	c1 = iris.load_cube(directory + 'thetao_mean_'+model+'_past1000_*_JJA.nc')[:,0,0]
	c2 = iris.load_cube(directory + 'so_mean_'+model+'_past1000_*_JJA.nc')[:,0,0]
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	for j,y in enumerate(expected_years):
		loc = np.where(year == y)[0][0]
		if np.size(loc) > 0:
# 			try:
				mm_mean_t[i,j] = c1[loc].data
				mm_mean_s[i,j] = c2[loc].data
				mm_mean_d18O[i,j] = calc_d18O(c1[loc].data,c2[loc].data)
	# 		except:
	# 			print 'year missing'
	#


'''

'''

directory2 = out_dir2

mm_mean_sic = np.zeros([np.size(models),np.size(expected_years)])
mm_mean_sic[:,:] = np.nan

for i,model in enumerate(models):
	print i
	c1 = iris.load_cube(directory2 + 'region_extracted_and_meaned_regridded_sic_OImon_'+model+'_past1000_*.nc')[:,0,0]
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	for j,y in enumerate(expected_years):
		loc = np.where(year == y)[0][0]
		if np.size(loc) > 0:
# 			try:
				mm_mean_sic[i,j] = c1[loc].data
	# 		except:
	# 			print 'year missing'
	#

'''

##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial[::-1])

##########################################
# Read in Masse 2008 seaice data #
##########################################

m_data_file = '/data/NAS-ph290/ph290/misc_data/masse_sea_ice.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ',')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
ip25_data_initial = tmp
ip25_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# ip25_data_initial=scipy.signal.detrend(ip25_initial[::-1])
ip25_data_initial=(ip25_data_initial[::-1])


##########################################
# solar
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/solar_cmip5/tsi_VK.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 4,delimiter = ' ')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
solar_data = tmp
solar_year = tmp_yr


##########################################
# volc
##########################################

m_data_file = '/data/NAS-ph290/ph290/cmip5/forcing_data/ICI5_030N_AOD_c.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 0,delimiter = '\t')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
volc_data = tmp
volc_year = tmp_yr


##########################################
# insolation
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/insol91.jun_dec'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ' ')
tmp1 = m_data[:,2]
tmp2 = m_data[:,9]
tmp_yr = m_data[:,0]
f1 = scipy.interpolate.interp1d(tmp_yr, tmp1)
f2 = scipy.interpolate.interp1d(tmp_yr, tmp2)
ins_year = np.linspace(start_year,end_year,(end_year - start_year + 1))
ins_june = f1(ins_year)
ins_dec = f2(ins_year)


##########################################
# Anthro radiative forcing
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/20THCENTURY_MIDYEAR_RADFORCING.DAT'
m_data = np.genfromtxt(m_data_file,skip_header = 61,delimiter = ',')
tmp1 = m_data[:,5]
tmp_yr = m_data[:,1]
anthro_forcing = tmp1
anthro_year = tmp_yr




##########################
# detrending each model individually
##########################


for i,model in enumerate(models):
	mm_mean_t[i,:] = scipy.signal.detrend(mm_mean_t[i,:])
	mm_mean_s[i,:] = scipy.signal.detrend(mm_mean_s[i,:])
	# mm_mean_sic[i,:] = scipy.signal.detrend(mm_mean_sic[i,:])
	#~ mm_mean_d18O[i,:] = scipy.signal.detrend(mm_mean_d18O[i,:])
	mm_mean_d18O[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0)
	mm_mean_d18O_s_only[i,:] = calc_d18O(273.15+(mm_mean_t[i,:]*0.0) +5.0,mm_mean_s[i,:]+35.0)
	mm_mean_d18O_t_only[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,0.0*mm_mean_s[i,:]+35.0)
	mm_mean_d18O[i,:] -= np.nanmean(mm_mean_d18O[i,:])
	mm_mean_d18O_s_only[i,:] -= np.nanmean(mm_mean_d18O_s_only[i,:])
	mm_mean_d18O_t_only[i,:] -= np.nanmean(mm_mean_d18O_t_only[i,:])

##########################
#  band pass filter
##########################



#!!  Do you want to band-pass filter?
band_pass_filter = False



def butter_bandpass(lowcut,  cutoff):
	order = 2
	low = 1/lowcut
	b, a = scipy.signal.butter(order, low , btype=cutoff,analog = False)
	return b, a

def low_pass_filter(cube,limit_years):
		b1, a1 = butter_bandpass(limit_years, 'low')
		output = scipy.signal.filtfilt(b1, a1, cube,axis = 0)
		return output

def high_pass_filter(cube,limit_years):
		b1, a1 = butter_bandpass(limit_years, 'high')
		output = scipy.signal.filtfilt(b1, a1, cube,axis = 0)
		return output

mm_mean_t_filtered = mm_mean_t.copy()
mm_mean_s_filtered = mm_mean_s.copy()
mm_mean_d18O_filtered = mm_mean_d18O.copy()
mm_mean_d18O_filtered_s_only = mm_mean_d18O_s_only.copy()
mm_mean_d18O_filtered_t_only = mm_mean_d18O_t_only.copy()
# mm_mean_sic_filtered = mm_mean_sic.copy()

if band_pass_filter:

	upper_limit_years = 200.0
	lower_limit_years = 5.0

	for i,model in enumerate(models):
		y = mm_mean_t[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_t_filtered[i,:] = y
		y = mm_mean_s[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_s_filtered[i,:] = y
		y = mm_mean_d18O[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_d18O_filtered[i,:] = y
		y = mm_mean_d18O_s_only[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_d18O_filtered_s_only[i,:] = y
		y = mm_mean_d18O_t_only[i,:].copy()
		y = high_pass_filter(y,upper_limit_years)
		y = low_pass_filter(y,lower_limit_years)
		mm_mean_d18O_filtered_t_only[i,:] = y


##########################
#  Multi Model mean
##########################

mm_mean_t_m_filtered = np.nanmean(mm_mean_t_filtered,axis = 0)
mm_mean_s_m_filtered = np.nanmean(mm_mean_s_filtered,axis = 0)
mm_mean_d18O_m_filtered = np.nanmean(mm_mean_d18O_filtered,axis = 0)
mm_mean_d18O_m_filtered_s_only = np.nanmean(mm_mean_d18O_filtered_s_only,axis = 0)
mm_mean_d18O_m_filtered_t_only = np.nanmean(mm_mean_d18O_filtered_t_only,axis = 0)
# mm_mean_sic_m_filtered = np.nanmean(mm_mean_sic_filtered,axis = 0)

##########################
#  Normalisation
##########################

y = bivalve_data_initial.copy()
x = bivalve_year.copy()

y -= np.min(y)
y /= np.max(y)

mm_mean_t_m_filtered -= np.min(mm_mean_t_m_filtered)
mm_mean_t_m_filtered /= np.max(mm_mean_t_m_filtered)

mm_mean_s_m_filtered -= np.min(mm_mean_s_m_filtered)
mm_mean_s_m_filtered /= np.max(mm_mean_s_m_filtered)

mm_mean_d18O_m_filtered -= np.min(mm_mean_d18O_m_filtered)
mm_mean_d18O_m_filtered /= np.max(mm_mean_d18O_m_filtered)

mm_mean_d18O_m_filtered_s_only -= np.min(mm_mean_d18O_m_filtered_s_only)
mm_mean_d18O_m_filtered_s_only /= np.max(mm_mean_d18O_m_filtered_s_only)

mm_mean_d18O_m_filtered_t_only -= np.min(mm_mean_d18O_m_filtered_t_only)
mm_mean_d18O_m_filtered_t_only /= np.max(mm_mean_d18O_m_filtered_t_only)

# mm_mean_sic_m_filtered -= np.min(mm_mean_sic_m_filtered)
# mm_mean_sic_m_filtered /= np.max(mm_mean_sic_m_filtered)

##########################
# plotting



plt.close('all')
fig, ax = plt.subplots(3,1,figsize=(10, 10))


# #bivalve d18O
# ax[0].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
# #Temperature
# ax[0].plot(expected_years,mm_mean_t_m_filtered,'r',alpha = 0.5,linewidth = 1.5)
#
# ax[0].set
#
# #bivalve d18O
# ax[1].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
# #Salinity
# ax[1].plot(expected_years,(mm_mean_s_m_filtered * -1.0) +1.0,'b',alpha = 0.5,linewidth = 1.5)
# # ax[1].plot(expected_years,(mm_mean_s_m_filtered),'b',alpha = 0.5,linewidth = 1.5)



#bivalve d18O
ax[0].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[0].plot(expected_years,mm_mean_d18O_m_filtered,'g',alpha = 0.5,linewidth = 1.5)
#~ ax[3].plot(expected_years,(mm_mean_t_m_filtered* -1.0) +1.0,'r',alpha = 0.5,linewidth = 1.5)

#bivalve d18O
ax[1].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[1].plot(expected_years,mm_mean_d18O_m_filtered_s_only,'b',alpha = 0.5,linewidth = 1.5)
# ax[1].plot(expected_years,(mm_mean_sic_m_filtered * -1.0) + 1.0,'magenta',alpha = 0.5,linewidth = 1.5)
ax[1].axvspan(950, 1200, color='y', alpha=0.2, lw=0)
ax[1].axvspan(1600, 1850, color='y', alpha=0.2, lw=0)

# ax[2].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
# #Synthetic d18O
# ax[2].plot(expected_years,(mm_mean_d18O_m_filtered_s_only * -1.0) + 1.0,'b',alpha = 0.5,linewidth = 1.5)
# # ax[1].plot(expected_years,(mm_mean_sic_m_filtered * -1.0) + 1.0,'magenta',alpha = 0.5,linewidth = 1.5)
# ax[2].axvspan(950, 1200, color='y', alpha=0.2, lw=0)
# ax[2].axvspan(1600, 1850, color='y', alpha=0.2, lw=0)

#
# 	y3 = solar_data
# 	y3 -= np.min(y3)
# 	y3 /= np.max(y3)
# 	ax[1].plot(solar_year,(y3* -1) + 1,'r',alpha = 0.5)
#
# 	import running_mean_post
# 	y4 = volc_data
# 	y4 = running_mean_post.running_mean_post(y4,7*36)
# 	y4 -= np.min(y4)
# 	y4 /= np.max(y4)
# 	ax[1].plot(volc_year,y4,'k',alpha = 0.5)

#bivalve d18O
ax[2].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[2].plot(expected_years,mm_mean_d18O_m_filtered_t_only,'r',alpha = 0.5,linewidth = 1.5)
ax[2].axvspan(1200, 1600, color='y', alpha=0.2, lw=0)

# #bivalve d18O
# ax[4].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
# #Synthetic d18O
# ax[4].plot(expected_years,(mm_mean_d18O_m_filtered_t_only * -1.0 )+1.0,'r',alpha = 0.5,linewidth = 1.5)
# ax[4].axvspan(1200, 1600, color='y', alpha=0.2, lw=0)
#
# 	y3 = solar_data
# 	y3 -= np.min(y3)
# 	y3 /= np.max(y3)
# 	ax[2].plot(solar_year,(y3* -1) + 1,'b',alpha = 0.5)
#
# 	import running_mean_post
# 	y4 = volc_data
# 	y4 = running_mean_post.running_mean_post(y4,7*36)
# 	y4 -= np.min(y4)
# 	y4 /= np.max(y4)
# 	ax[2].plot(volc_year,y4,'k',alpha = 0.5)


# ax2 = ax[2].twinx()
# ax2.plot(anthro_year,anthro_forcing,'g')
# ax2.plot(ins_year,ins_june-np.mean(ins_june),'g')



ax[0].set_xlim(950,1850)
ax[1].set_xlim(950,1850)
ax[2].set_xlim(950,1850)
# ax[3].set_xlim(950,1850)
# ax[4].set_xlim(950,1850)

ax[0].set_title('bivalve d18O (black), model derived d18O (green)')
# '+models[0])
ax[1].set_title('bivalve d18O (black), model salinity-only derived d18O (blue), sea-ice *-1 (magenta)')
# ax[2].set_title('bivalve d18O (black), model salinity-only derived d18O * -1 (blue), sea-ice *-1 (magenta)')
ax[2].set_title('bivalve d18O (black), model temperature-only derived d18O (red)')
# ax[4].set_title('bivalve d18O (black), model temperature-only derived d18O * -1.0 (red)')

# plt.savefig('/home/ph290/Documents/figures/tmp/ens_mean.png')
#'+models[0]+'.png')
plt.show(block = False)


import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import cartopy.crs as ccrs



def model_names(directory,var):
	files = glob.glob(directory+'/*'+var+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


lon_west = 22
lon_east = 52
lat_south = 67
lat_north = 77

in_dir = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'

models1 = model_names(in_dir,'sic')
models1 = list(models1)
models2 = model_names(in_dir,'tos')
models2 = list(models2)

models = filter(set(models1).__contains__, models2)

out_maps = np.ma.masked_array(np.zeros([np.size(models),180,360]))
out_maps_1 = out_maps.copy()
out_maps_2 = out_maps.copy()
out_maps_3 = out_maps.copy()
out_maps_4 = out_maps.copy()

count = 0

try:
	with open('/data/NAS-ph290/ph290/cmip5/pickles/ice_tos.pickle', 'r') as f:
		out_maps,out_maps_1,out_maps_2,out_maps_3,out_maps_4,count = pickle.load(f)
except:
	print 'no pickle to open'


# models = models[0:2]
if count < np.size(models):
	for i in range(count,np.size(models)):
		# 	for i in range(count,10):
		model = models[i]
		print i
		sic = iris.load_cube(in_dir+model+'_sic_piControl*.nc','sea_ice_area_fraction')
		sic_size = np.shape(sic)[0]/12.0
		try:
			tos = iris.load_cube(in_dir+model+'_tos_piControl*.nc','sea_surface_temperature')
		except:
			tos = iris.load_cube(in_dir+model+'_tos_piControl*.nc','surface_temperature')
		tos_size = np.shape(tos)[0]
		if sic_size == tos_size:
			try:
				tos_mask = tos.data.mask
			except:
				tos.data = np.ma.masked_array(tos.data)
				tos.data = np.ma.masked_where(tos.data == 0,tos.data)
				tos_mask = tos.data.mask
			tos_data = tos.data.data
			tos.data = np.ma.masked_array(scipy.signal.detrend(tos_data, axis=0))
			tos.data.mask = tos_mask
			iris.coord_categorisation.add_season_year(sic, 'time', name='season_year')
			iris.coord_categorisation.add_season_number(sic, 'time', name='season_number')
			sic = sic.aggregated_by(['season_year','season_number'], iris.analysis.MEAN)
			iris.coord_categorisation.add_season(sic, 'time', name='season_name')
			loc = np.where(sic.coord('season_name').points == 'djf')[0]
			try:
				sic_mask = sic.data.mask
			except:
				sic.data = np.ma.masked_array(sic.data)
				sic.data = np.ma.masked_where(sic.data == 0,sic.data)
				sic_mask = sic.data.mask
			sic_mean = sic.collapsed('time',iris.analysis.MEAN)
			sic_data = sic.data.data
			sic.data = np.ma.masked_array(scipy.signal.detrend(sic_data, axis=0))
			sic.data.mask = sic_mask
			sic = sic + sic_mean
			sic2 = sic[loc]
			cube = sic2.intersection(longitude=(lon_west, lon_east))
			cube = cube.intersection(latitude=(lat_south, lat_north))
			try:
				cube.coord('latitude').guess_bounds()
			except:
				print 'cube already has latitude bounds'
			try:
				cube.coord('longitude').guess_bounds()
			except:
				print 'cube already has longitude bounds'
			grid_areas = iris.analysis.cartography.area_weights(cube)
			sic_ts = cube.collapsed(['latitude','longitude'],iris.analysis.MEAN, weights=grid_areas)
			if np.max(sic_ts.data) > 1.1:
				loc2 = np.where(sic_ts.data > 50.0)[0][0:-2]
			if np.max(sic_ts.data) < 1.1:
				loc2 = np.where(sic_ts.data > 0.5)[0][0:-2]
			if np.size(loc2) > 0:
		# 				tos_map1 = tos.collapsed('time',iris.analysis.MEAN)
				tos_map2 = tos[loc2].collapsed('time',iris.analysis.MEAN)
		# 				diff = tos_map2 - tos_map1
				out_maps[i] = tos_map2.data.copy()
				#
				tmp_v = 5
				if np.size(loc2) > tmp_v:
					tos_map2 = tos[loc2[0:-tmp_v]+tmp_v].collapsed('time',iris.analysis.MEAN)
					# 					diff = tos_map2 - tos_map1
					out_maps_1[i] = tos_map2.data.copy()
				#
				tmp_v = 20
				if np.size(loc2) > tmp_v:
					tos_map2 = tos[loc2[0:-tmp_v]+tmp_v].collapsed('time',iris.analysis.MEAN)
					# 					diff = tos_map2 - tos_map1
					out_maps_2[i] = tos_map2.data.copy()
				#
				tmp_v = 60
				if np.size(loc2) > tmp_v:
					tos_map2 = tos[loc2[0:-tmp_v]+tmp_v].collapsed('time',iris.analysis.MEAN)
		# 					diff = tos_map2 - tos_map1
					out_maps_3[i] = tos_map2.data.copy()
				#
				tmp_v = 80
				if np.size(loc2) > tmp_v:
					tos_map2 = tos[loc2[0:-tmp_v]+tmp_v].collapsed('time',iris.analysis.MEAN)
		# 					diff = tos_map2 - tos_map1
					out_maps_4[i] = tos_map2.data.copy()
				count = i
				with open('/data/NAS-ph290/ph290/cmip5/pickles/ice_tos.pickle', 'w') as f:
					pickle.dump([out_maps,out_maps_1,out_maps_2,out_maps_3,out_maps_4,count], f)
			else:
				print model+' not enough seaice in Barents sea'
		else:
			print model+'tos, sic diff size'

out_maps = np.ma.masked_where(out_maps == 0.0,out_maps)
out_maps_mean = np.mean(out_maps,axis = 0)




plot_cube = iris.load_cube(in_dir+models[0]+'_tos_piControl*.nc')[0]
for i in np.arange(40):
	plt.close('all')
	try:
		if i <> 2:
			plot_cube.data = out_maps[i]
		if i == 2:
			plot_cube.data = np.roll(out_maps2[i],80,axis=1)
		plt.figure()
		crs_latlon = ccrs.PlateCarree()
		#ax = plt.axes(projection=ccrs.PlateCarree())
		ax = plt.axes(projection=ccrs.NorthPolarStereo())
		ax.set_extent((-80.0, 50.0, 45.0, 90.0), crs=crs_latlon)
		# Add coastlines and meridians/parallels (Cartopy-specific).
		ax.coastlines(linewidth=0.75)
		ax.gridlines(crs=crs_latlon, linestyle='-')
		# Plot the first dataset as a pseudocolour filled plot.
		max_v = np.max([np.min(plot_cube.data) * -1.0,np.max(plot_cube.data)])
		iplt.contourf(plot_cube,np.linspace(max_v * -1.0,max_v,21), cmap='RdBu_r')
		plt.colorbar()
		plt.show(block = True)
	except:
		print 'no data'








import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names_piCont(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


def max_overturning(cube,latitude):
    cube = cube[:,0,:,90+latitude]
    return scipy.signal.detrend(np.max(cube.data,axis = 1))


lags = [0]

def calculate_correlations(models,var_name,my_dir,dir_amoc,lags):
	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	correlations = [var]
	for i,model in enumerate(models):
		print i
		c1 = iris.load_cube(my_dir + model+'_'+var_name+'_piControl_*.nc')
		c2 = iris.load_cube(dir_amoc + model+'_msftmyz_piControl_*.nc')
		if np.shape(c1)[0]/12 == np.shape(c2)[0]:
			iris.coord_categorisation.add_year(c1, 'time', name='year')
			c1 = c1.aggregated_by('year', iris.analysis.MEAN)
		if np.shape(c1)[0] == np.shape(c2)[0]:
			# qplt.contourf(c1[0],31)
			# plt.gca().coastlines()
			# plt.show()
			try:
				missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
			except:
				missing_data = np.where(c1[0].data.data == 0.0)
			cube_data = c1.data
			cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
			c1.data = cube_data_detrended
			#
			for j,lag in enumerate(lags):
				print 'j: ',j
				ts = np.roll(max_overturning(c2,40),int(lag))
				smoothing = 10
				ts = rm.running_mean(ts,smoothing)
				ts = ts[smoothing/2:-smoothing/2]
				cube = c1[smoothing/2:-smoothing/2].copy()
				cube = iris.analysis.maths.multiply(cube, 0.0)
				ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				cube = iris.analysis.maths.add(cube, ts2)
				out_cube = iris.analysis.stats.pearsonr(c1[smoothing/2:-smoothing/2], cube, corr_coords=['time'])
				correlations[j][i] = out_cube.data
	return correlations



dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_psl = '/data/NAS-ph290/ph290/cmip5/piControl/'

m0 = model_names_piCont(dir_amoc,'msftmyz')
m1 = model_names_piCont(dir_psl,'psl')

models = np.intersect1d(m0,m1)

models = list(models)
models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay

#testing with just those in PMIP3
models = ['CCSM4', 'MPI-ESM-P', 'MRI-CGCM3']


print models


var_name = 'psl'


correlations_psl = calculate_correlations(models,var_name,dir_psl,dir_amoc,lags)

"""

# with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c.pickle', 'w') as f:
#     pickle.dump([correlations_psl], f)






with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c.pickle', 'r') as f:
    [correlations_psl] = pickle.load(f)

"""

#the following is obtained by running: picontrol_sic_psl.py
with open('/home/ph290/Documents/python_scripts/pickles/sic_psl.pickle', 'r') as f:
    [correlations_sic_psl] = pickle.load(f)

def plot_prep(i,correlations,c1):
	var_mean = np.nanmean(correlations[i],axis=0)
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube




with open('/home/ph290/Documents/python_scripts/pickles/analysis_fill_length.pickle', 'r') as f:
    [expected_years,mm_mean_d18O_m_filtered,mm_mean_d18O_m_filtered_t_only,mm_mean_d18O_m_filtered_s_only,x,y] = pickle.load(f)

with open('/home/ph290/Documents/python_scripts/pickles/analysis_fill_length2.pickle', 'r') as f:
    [expected_years,mm_mean_d18O_m_filtered,mm_mean_d18O_m_filtered_t_only,mm_mean_d18O_m_filtered_s_only,x,y,mm_mean_t_m_filtered,mm_mean_s_m_filtered] = pickle.load(f)




##########################################
# Read in Reynolds d18O data and detrend #
##########################################

start_year = 950
end_year = 1849


#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial[::-1])

#################################


# bivalve_data_initial -= np.min(bivalve_data_initial)
# bivalve_data_initial /= np.max(bivalve_data_initial)

bivalve_data_initial -= np.mean(bivalve_data_initial)
bivalve_data_initial /= np.std(bivalve_data_initial)



###################
# seaice
###################

def model_names(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*_JJA.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'

start_year = 950
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory,'sic')
models = list(models)
# models.remove('CCSM4') # Maybe does things right all way through
# models.remove('CSIRO-Mk3L-1-2') # Maybe does things right all way through
# models.remove('FGOALS-s2') # no real correlation or anti correlation all wrong
models.remove('GISS-E2-R')  # Maybe does things right NO PLS
# models.remove('HadGEM2-ES') # Looks like inverse... NO TAS
# models.remove('MIROC-ESM') # mixture - maybe mostly inverse
# models.remove('MPI-ESM-P') # perhaps switches sign - looks like mm mean
# models.remove('MRI-CGCM3') #Maybe switches sign, not a lot of correlation
# models.remove('FGOALS-gl')

# models.remove('HadCM3')
models.remove('bcc-csm1-1')


mm_mean_sic = np.zeros([np.size(models),np.size(expected_years)])

lon_west = -35
lon_east = 20
lat_south = 65
lat_north = 90

for i,model in enumerate(models):
	print i
	c1 = iris.load_cube(directory + model+'_sic_past1000_r1i1p1_regridded_JJA.nc')
	c1 = c1.intersection(longitude=(lon_west, lon_east))
	c1 = c1.intersection(latitude=(lat_south, lat_north))
	c1 = c1.collapsed(['latitude','longitude'],iris.analysis.MEAN)
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	for j,y in enumerate(expected_years):
		loc = np.where(year == y)[0][0]
		if np.size(loc) > 0:
# 			try:
				mm_mean_sic[i,j] = c1[loc].data
	# 		except:
	# 			print 'year missing'
	#


for i,model in enumerate(models):
	mm_mean_sic[i,:] = scipy.signal.detrend(mm_mean_sic[i,:])
	mm_mean_sic[i,:] -= np.nanmean(mm_mean_sic[i,:])
	mm_mean_sic[i,:] /= np.nanstd(mm_mean_sic[i,:])



##########################
#  Multi Model mean
##########################

mm_mean_sic_m = np.nanmean(mm_mean_sic,axis = 0)





##################
# paper figure 4
##################

dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_psl = '/data/NAS-ph290/ph290/cmip5/piControl/'

m0 = model_names_piCont(dir_amoc,'msftmyz')
m1 = model_names_piCont(dir_psl,'psl')

models = np.intersect1d(m0,m1)

models = list(models)
models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay

plt.close('all')
plt.figure(figsize=(6, 10))



######### Subplot 1 #########

var_name = 'psl'
c1 = iris.load_cube(dir_psl + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_sic_psl,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((6, 4), (0, 0), colspan=2,rowspan=2,projection= my_projection)

ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.15,0.15,31),
			transform=ccrs.PlateCarree(),cmap='jet', extend="both")

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='#D3D3D3')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)

# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('Sea-ice sea level pressure correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

#66 31.59 N, 18 11.74 W
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())

# x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
ax.gridlines()

######### Subplot 2 #########

var_name = 'psl'
c1 = iris.load_cube(dir_psl + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_psl,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
# ax = plt.subplot(1,2,1,projection= my_projection)
ax = plt.subplot2grid((6, 4), (0, 2), colspan=2,rowspan=2,projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.1,0.1,31),
			transform=ccrs.PlateCarree(),cmap='jet', extend="both")

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='#D3D3D3')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('AMOC sea level pressure correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

#66 31.59 N, 18 11.74 W
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())

# x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
ax.gridlines()


######### subplot 3 #################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]
s_d = rm.running_mean(s_d,5)*-1.0
s_d -= np.nanmean(s_d)
s_d /= np.nanstd(s_d)



ax = plt.subplot2grid((6, 4), (2, 0), colspan=4)

#Synthetic d18O
# ax.plot(expected_years[0:250],mm_mean_d18O_m_filtered_t_only[0:250],'r',alpha = 0.2,linewidth = 1.5)
# p1 = ax.plot(expected_years[250:600],mm_mean_d18O_m_filtered_t_only[250:600],'r',alpha = 0.5,linewidth = 1.5,label = 'Model temperature derived $\delta^{18}$O')
loc = np.where((expected_years >=1200) & (expected_years <= 1800))
y= scipy.signal.detrend(mm_mean_sic_m[loc])
y /= np.nanstd(y)
# p1 = ax.plot(expected_years[loc],scipy.signal.detrend(mm_mean_t_m_filtered[loc]*-1.0),'r',alpha = 0.5,linewidth = 1.5,label = 'Model mixed layer temperature')
p1b = ax.plot(expected_years[loc],y,'c',alpha = 0.5,linewidth = 1.5,label = 'Model ensemble mean summer Atl.-Arctic seaice extent')




# ax.plot(expected_years[600::],mm_mean_d18O_m_filtered_t_only[600::],'r',alpha = 0.2,linewidth = 1.5)

# ax[0].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)
# loc = np.where((s_y >1200) & (s_y <= 1500))
# ax.plot(s_y[loc],s_d[loc],'#2F4F4F',lw=2,alpha = 0.8)
loc2 = np.where((s_y >=1200) & (s_y <= 1800))
# ax2 = ax.twinx()
y= scipy.signal.detrend(s_d[loc2])
y /= np.nanstd(y)
p2 = ax.plot(s_y[loc2],y,'#808080',lw=1.5,alpha = 0.8,label = 'Fennoscandia $\delta^{13}$C (sunshine)')

ax.set_ylabel('Normalised')
# ax2.set_ylabel('Fennoscandia $\delta^{13}$C')
ax.set_xlabel('Calendar year')

ax.yaxis.label.set_color('r')
# ax2.yaxis.label.set_color(color = '#2F4F4F')

ax.set_xlim(1200,1800)
# ax2.set_xlim(1200,1800)
plt.legend(ncol=2,fontsize=6)


ax = plt.subplot2grid((6, 4), (3, 0), colspan=4)

#Synthetic d18O
# ax.plot(expected_years[0:250],mm_mean_d18O_m_filtered_t_only[0:250],'r',alpha = 0.2,linewidth = 1.5)
# p1 = ax.plot(expected_years[250:600],mm_mean_d18O_m_filtered_t_only[250:600],'r',alpha = 0.5,linewidth = 1.5,label = 'Model temperature derived $\delta^{18}$O')
loc = np.where((expected_years >=1200) & (expected_years <= 1800))
y=scipy.signal.detrend(mm_mean_t_m_filtered[loc]*-1.0)
y /= np.nanstd(y)
p1 = ax.plot(expected_years[loc],y,'r',alpha = 0.5,linewidth = 1.5,label = 'Model mixed layer temperature * -1.0')




# ax.plot(expected_years[600::],mm_mean_d18O_m_filtered_t_only[600::],'r',alpha = 0.2,linewidth = 1.5)

# ax[0].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)
# loc = np.where((s_y >1200) & (s_y <= 1500))
# ax.plot(s_y[loc],s_d[loc],'#2F4F4F',lw=2,alpha = 0.8)
loc2 = np.where((s_y >=1200) & (s_y <= 1800))
y=scipy.signal.detrend(s_d[loc2])
y /= np.nanstd(y)
p2 = ax.plot(s_y[loc2],y,'#808080',lw=1.5,alpha = 0.8,label = 'Fennoscandia $\delta^{13}$C')

plt.legend(ncol=2,fontsize=6)

ax.set_ylabel('normalise')
# ax2.set_ylabel('Fennoscandia $\delta^{13}$C')
ax.set_xlabel('Calendar year')

ax.yaxis.label.set_color('r')
# ax2.yaxis.label.set_color(color = '#2F4F4F')

ax.set_xlim(1200,1800)
ax.set_ylim(-3.0,4.0)

# ax2.set_xlim(1200,1800)

# lg = ax.legend(bbox_to_anchor=(1.0, 0.9))
# lg.draw_frame(False)
# lg2 = ax2.legend(bbox_to_anchor=(0.8, 0.9))
# lg2.draw_frame(False)


ax = plt.subplot2grid((6, 4), (4, 0), colspan=4)

loc3 = np.where((bivalve_year >=1200) & (bivalve_year <= 1500))
y= scipy.signal.detrend(bivalve_data_initial[loc3])
y /= np.nanstd(y)
p1b = ax.plot(bivalve_year[loc3],y,'k',alpha = 0.5,linewidth = 1.5,label = 'bivalve $\delta^{18}$O')
loc3 = np.where((bivalve_year >=1500) & (bivalve_year <= 1800))
y= scipy.signal.detrend(bivalve_data_initial[loc3])
y /= np.nanstd(y)
p1b = ax.plot(bivalve_year[loc3],y,'k',alpha = 0.25,linewidth = 1.0)
# loc3 = np.where((bivalve_year >=1500) & (bivalve_year <= 1800))
# p1b = ax.plot(bivalve_year[loc3],scipy.signal.detrend(bivalve_data_initial[loc3]),'b--',alpha = 0.5,linewidth = 1.0,label = 'bivalve $\delta^{18}$O')

# ax2 = ax.twinx()

loc4 = np.where((bivalve_year >=1200) & (bivalve_year <= 1500))
y=scipy.signal.detrend(mm_mean_d18O_m_filtered[loc4])
y /= np.nanstd(y)
p1b = ax.plot(expected_years[loc4],y,'g',alpha = 0.5,linewidth = 1.5,label = 'Model derived $\delta^{18}$O')
loc4 = np.where((bivalve_year >=1500) & (bivalve_year <= 1800))
y=scipy.signal.detrend(mm_mean_d18O_m_filtered[loc4])
y /= np.nanstd(y)
p1b = ax.plot(expected_years[loc4],y,'g',alpha = 0.25,linewidth = 1.0)
# loc4 = np.where((bivalve_year >=1500) & (bivalve_year <= 1800))
# p1a = ax2.plot(expected_years[loc4],scipy.signal.detrend(mm_mean_d18O_m_filtered_t_only[loc4]),'r--',alpha = 0.5,linewidth = 0.5,label = 'Model temperature derived $\delta^{18}$O')
# p1b = ax2.plot(expected_years[loc4],scipy.signal.detrend(mm_mean_d18O_m_filtered[loc4]),'g--',alpha = 0.5,linewidth = 1.0,label = 'Model derived $\delta^{18}$O')



# p2 = ax2.plot(s_y[loc2],scipy.signal.detrend(s_d[loc2]),'#808080',lw=1.5,alpha = 0.8,label = 'Fennoscandia $\delta^{13}$C')

ax.set_ylabel('N. Iceland $\delta^{18}$O')
# ax2.set_ylabel('bivalve N. Iceland $\delta^{18}$O')
ax.set_xlabel('Calendar year')

ax.yaxis.label.set_color('g')
# ax2.yaxis.label.set_color(color = '#0000ff')

ax.set_xlim(1200,1800)
# ax2.set_xlim(1200,1800)
ax.set_ylim(-3.0,4.0)


plt.legend(ncol=2,fontsize=6)




ax = plt.subplot2grid((6, 4), (5, 0), colspan=4)

loc4 = np.where((bivalve_year >=1200) & (bivalve_year <= 1500))
y=scipy.signal.detrend(mm_mean_d18O_m_filtered[loc4])
y /= np.nanstd(y)

y1=scipy.signal.detrend(mm_mean_d18O_m_filtered_t_only[loc4])
y1 /= np.nanstd(y1)

y2=scipy.signal.detrend(mm_mean_d18O_m_filtered_s_only[loc4])
y2 /= np.nanstd(y2)

p1b = ax.plot(expected_years[loc4],y,'g',alpha = 0.5,linewidth = 1.0,label = 'Model derived $\delta^{18}$O')
p1b = ax.plot(expected_years[loc4],y1,'r',alpha = 0.5,linewidth = 1.0,label = 'Model temperature derived $\delta^{18}$O')
p1b = ax.plot(expected_years[loc4],y2,'b',alpha = 0.5,linewidth = 1.0,label = 'Model salinity derived $\delta^{18}$O')



# p2 = ax2.plot(s_y[loc2],scipy.signal.detrend(s_d[loc2]),'#808080',lw=1.5,alpha = 0.8,label = 'Fennoscandia $\delta^{13}$C')

ax.set_ylabel('N. Iceland $\delta^{18}$O')
# ax2.set_ylabel('bivalve N. Iceland $\delta^{18}$O')
ax.set_xlabel('Calendar year')

ax.yaxis.label.set_color('g')
# ax2.yaxis.label.set_color(color = '#0000ff')

ax.set_xlim(1200,1800)
# ax2.set_xlim(1200,1800)
ax.set_ylim(-3.0,4.0)


plt.legend(ncol=3,fontsize=6)


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/amoc_psl_pi_cont_composites_alternative_pmip3_thetao.png')
plt.show(block = False)

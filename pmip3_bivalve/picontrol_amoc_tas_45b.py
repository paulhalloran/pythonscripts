import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


def max_overturning(cube,latitude):
    cube = cube[:,0,:,90+latitude]
    return scipy.signal.detrend(np.max(cube.data,axis = 1))


def calculate_correlations(models,var_name,my_dir,dir_amoc):
	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	var_1 = var.copy()
	var_2 = var.copy()
	lags = np.array([0])
	correlations = [var]
	for i,model in enumerate(models):
		print i
		c1 = iris.load_cube(my_dir + model+'_'+var_name+'_piControl_*.nc')
		c2 = iris.load_cube(dir_amoc + model+'_msftmyz_piControl_*.nc')
		if np.shape(c1)[0]/12 == np.shape(c2)[0]:
			iris.coord_categorisation.add_year(c1, 'time', name='year')
			c1 = c1.aggregated_by('year', iris.analysis.MEAN)
		if np.shape(c1)[0] == np.shape(c2)[0]:
			# qplt.contourf(c1[0],31)
			# plt.gca().coastlines()
			# plt.show()
			try:
				missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
			except:
				missing_data = np.where(c1[0].data.data == 0.0)
			cube_data = c1.data
			cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
			c1.data = cube_data_detrended
			#
			for j,lag in enumerate(lags):
				print 'j: ',j
				ts = np.roll(max_overturning(c2,45),lag)
				ts = rm.running_mean(ts,10)
				ts = ts[5:-5]
				cube = c1[5:-5].copy()
				cube = iris.analysis.maths.multiply(cube, 0.0)
				ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				cube = iris.analysis.maths.add(cube, ts2)
				out_cube = iris.analysis.stats.pearsonr(c1[5:-5], cube, corr_coords=['time'])
				correlations[j][i] = out_cube.data
	return correlations



dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_tas = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_sic = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'


m0 = model_names(dir_amoc)
m1 = model_names(dir_tas)
m2 = model_names(dir_sic)
m3 = np.intersect1d(m0, m1)

models = np.intersect1d(m3, m2)

models = list(models)
models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay

print models

var_name = 'tas'
correlations_tas = calculate_correlations(models,var_name,dir_tas,dir_amoc)
var_name = 'sic'
correlations_sic = calculate_correlations(models,var_name,dir_sic,dir_amoc)


with open('/home/ph290/Documents/python_scripts/pickles/amoc_tas_45b.pickle', 'w') as f:
    pickle.dump([correlations_tas,correlations_sic], f)


"""



with open('/home/ph290/Documents/python_scripts/pickles/amoc_tas_45b.pickle', 'r') as f:
    [correlations_tas,correlations_sic] = pickle.load(f)


"""


def plot_prep(i,correlations,c1):
	var_mean = np.nanmean(correlations[i],axis=0)
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube


# plt.close('all')
# 
# plt.figure(figsize=(8, 10))
# fig, ax = plt.subplots(1,3,figsize=(14, 10))
# 
# mean_var_cube = plot_prep(0,correlations,c1)
# ax1 = plt.subplot(1,3,1)
# qplt.contourf(mean_var_cube,np.linspace(-0.1,0.1,31))
# plt.gca().coastlines()
# plt.title('tas 1')
# 
# mean_var_cube = plot_prep(1,correlations,c1)
# ax2 = plt.subplot(1,3,2)
# qplt.contourf(mean_var_cube,np.linspace(-0.1,0.1,31))
# plt.gca().coastlines()
# plt.title('2')
# 
# mean_var_cube = plot_prep(2,correlations,c1)
# ax3 = plt.subplot(1,3,3)
# qplt.contourf(mean_var_cube,np.linspace(-0.1,0.1,31))
# plt.gca().coastlines()
# plt.title('3')
# 
# plt.savefig('/home/ph290/Documents/figures/amoc_tas_lead_lag_45b.png')
# plt.show()


#################################

##################
# paper figure 3
##################



plt.close('all')
plt.figure(figsize=(12, 5))


######### Subplot 1 #########

var_name = 'tas'
c1 = iris.load_cube(dir_tas + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_tas,c1)

ax = plt.subplot(1,2,1,projection= ccrs.Orthographic(central_longitude=-20, central_latitude=60))
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.5,0.5,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('physical', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('physical', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('physical', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='grey')
ax.add_feature(coast_50m)
# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('Control run AMOC - air temperature correlation')

plt.plot(-18.195667,66.5215, 'ro', transform=ccrs.PlateCarree())
#66 31.59 N, 18 11.74 W

######### Subplot 2 #########

var_name = 'sic'
c1 = iris.load_cube(dir_sic + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_sic,c1)

ax = plt.subplot(1,2,2,projection= ccrs.Orthographic(central_longitude=-20, central_latitude=60))
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('physical', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('physical', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('physical', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='grey')
ax.add_feature(coast_50m)
# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('Control run AMOC - sic temperature correlation')

plt.plot(-18.195667,66.5215, 'ro', transform=ccrs.PlateCarree())
#66 31.59 N, 18 11.74 W

plt.savefig('/home/ph290/Documents/figures/tas_sic_pi_cont_composites.png')
plt.show()

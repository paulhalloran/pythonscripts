
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


with open('/home/ph290/Documents/python_scripts/pickles/analysis_fill_length.pickle', 'r') as f:
    [expected_years,mm_mean_d18O_m_filtered,mm_mean_d18O_m_filtered_t_only,mm_mean_d18O_m_filtered_s_only,x,y] = pickle.load(f)


with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VI.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)

models = np.array(models)

###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}


giss_test = 0

for i,model in enumerate(models):
        if model == 'GISS-E2-R':
                if giss_test == 0:
                        pmip3_str[model] = max_strm_fun_26[i]
                        pmip3_year_str[model] = model_years[i]
                        giss_test += 1
        if model <> 'GISS-E2-R':
                pmip3_str[model] = max_strm_fun_26[i]
                pmip3_year_str[model] = model_years[i]


#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean

start_year = 1200
end_year = 1550
expected_years2 = start_year+np.arange((end_year-start_year)+1)


pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
pmip3_model_streamfunction[:] = np.NAN

for i,model in enumerate(models):
        print model
        tmp = pmip3_str[model]
        loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
        tmp = tmp[loc]
        yrs = pmip3_year_str[model][loc]
        data2=signal.detrend(tmp)
        # data2 = data2-np.min(data2)
        # data3 = data2/(np.max(data2))
        data3 = data2
        for index,y in enumerate(expected_years2):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                        pmip3_model_streamfunction[index,i] = data3[loc2]



pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)

##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial[::-1])


########### plot ##################



plt.close('all')
plt.figure(figsize=(12, 10))



ax = plt.subplot2grid((3, 4), (2, 0), colspan=4)

#Synthetic d18O
# ax.plot(expected_years[0:250],mm_mean_d18O_m_filtered_t_only[0:250],'r',alpha = 0.2,linewidth = 1.5)
# ax.plot(expected_years[250:600],mm_mean_d18O_m_filtered_t_only[250:600],'r',alpha = 0.5,linewidth = 1.5,label = 'Model temperature derived $\delta^{18}$O')
ax.plot(bivalve_year,bivalve_data_initial,'k',alpha = 0.5,linewidth = 1.5,label = 'Bivalve $\delta^{18}$O')
# ax.plot(expected_years[600::],mm_mean_d18O_m_filtered_t_only[600::],'r',alpha = 0.2,linewidth = 1.5)

# ax[0].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)
# loc = np.where((s_y >1200) & (s_y <= 1600))
# ax.plot(s_y[loc],s_d[loc],'#2F4F4F',lw=2,alpha = 0.8)
ax2 = ax.twinx()
plt_strm = pmip3_multimodel_mean_streamfunction.copy()
plt_strm -= np.nanmin(plt_strm)
plt_strm /= np.nanmax(plt_strm)
ax2.plot(expected_years2,plt_strm,'g',lw=1.5,alpha = 0.5)


ax2.set_ylim([1,0])
ax.set_xlabel('calendar year')

ax.set_ylabel('normalised N. Iceland\nbivalve $\delta^{18}$O')
ax2.set_ylabel('normalised PMIP3 milti model\nmean AMOC (inverted axis)')
ax.set_xlabel('calendar year')

ax.yaxis.label.set_color('k')
ax2.yaxis.label.set_color(color = 'g')


# lg = ax.legend(bbox_to_anchor=(1.0, 0.9))
# lg.draw_frame(False)
# lg2 = ax2.legend(bbox_to_anchor=(0.8, 0.9))
# lg2.draw_frame(False)


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/amoc_blvalve.png')
plt.show(block = False)

# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_CCSM4.nc thetao_CCSM4_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_GISS-E2-R.nc thetao_GISS-E2-R_profile.nc
#cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_HadGEM2-ES.nc thetao_HadGEM2-ES_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_MIROC-ESM.nc thetao_MIROC-ESM_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_MPI-ESM-P.nc thetao_MPI-ESM-P_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_MRI-CGCM3.nc thetao_MRI-CGCM3_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_bcc-csm1-1.nc thetao_bcc-csm1-1_profile.nc

# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_CCSM4.nc thetao_CCSM4_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_GISS-E2-R.nc thetao_GISS-E2-R_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_HadGEM2-ES.nc thetao_HadGEM2-ES_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_MIROC-ESM.nc thetao_MIROC-ESM_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_MPI-ESM-P.nc thetao_MPI-ESM-P_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_MRI-CGCM3.nc thetao_MRI-CGCM3_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 thetao_bcc-csm1-1.nc thetao_bcc-csm1-1_section.nc
# cp thetao_*_section.nc /data/NAS-ph290/ph290/cmip5/profiles/

# cdo -P 8 mergetime so_Omon_CCSM4_historical_r1i1p1_*.nc so_CCSM4.nc
# cdo -P 8 mergetime so_Omon_GISS-E2-R_historical_r1i1p1_*.nc so_GISS-E2-R.nc
# cdo -P 8 mergetime so_Omon_HadGEM2-ES_historical_r1i1p1_*.nc so_HadGEM2-ES.nc
# cdo -P 8 mergetime so_Omon_MIROC-ESM_historical_r1i1p1_*.nc so_MIROC-ESM.nc
# cdo -P 8 mergetime so_Omon_MPI-ESM-P_historical_r1i1p1_*.nc so_MPI-ESM-P.nc
# cdo -P 8 mergetime so_Omon_MRI-CGCM3_historical_r1i1p1_*.nc so_MRI-CGCM3.nc
# cdo -P 8 mergetime so_Omon_bcc-csm1-1_historical_r1i1p1_*.nc so_bcc-csm1-1.nc
#
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_CCSM4.nc so_CCSM4_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_GISS-E2-R.nc so_GISS-E2-R_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_HadGEM2-ES.nc so_HadGEM2-ES_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_MIROC-ESM.nc so_MIROC-ESM_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_MPI-ESM-P.nc so_MPI-ESM-P_profile.nc
# cdo -P 8 fldmean -setvals,0.0,1e+20   -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_MRI-CGCM3.nc so_MRI-CGCM3_profile.nc
# cdo -P 8 fldmean -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_bcc-csm1-1.nc so_bcc-csm1-1_profile.nc

# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_CCSM4.nc so_CCSM4_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_GISS-E2-R.nc so_GISS-E2-R_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_HadGEM2-ES.nc so_HadGEM2-ES_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_MIROC-ESM.nc so_MIROC-ESM_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_MPI-ESM-P.nc so_MPI-ESM-P_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_MRI-CGCM3.nc so_MRI-CGCM3_section.nc
# cdo -P 8 sellonlatbox,-30,17,74,78 -remapbil,r360x180 -selyear,1980/2000 so_bcc-csm1-1.nc so_bcc-csm1-1_section.nc
# cp so_*_section.nc /data/NAS-ph290/ph290/cmip5/profiles/

# cdo -P 8 fldmean -setvals,0.0,nan   -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 thetao_MRI-CGCM3.nc thetao_MRI-CGCM3_profile.nc
# cp thetao_MRI-CGCM3_profile.nc /data/NAS-ph290/ph290/cmip5/profiles/
# cdo -P 8 fldmean -setvals,0.0,nan   -sellonlatbox,-20,-10,70,80 -selyear,1980/2000 so_MRI-CGCM3.nc so_MRI-CGCM3_profile.nc
# cp so_MRI-CGCM3_profile.nc /data/NAS-ph290/ph290/cmip5/profiles/

# cdo -P 8 sellonlatbox,-30,17,74,78  -remapbil,r360x180 -setvals,0.0,nan -selyear,1980/2000 thetao_MRI-CGCM3.nc thetao_MRI-CGCM3_section.nc
# cp thetao_MRI-CGCM3_section.nc /data/NAS-ph290/ph290/cmip5/profiles/
# cdo -P 8 sellonlatbox,-30,17,74,78  -remapbil,r360x180 -setvals,0.0,nan -selyear,1980/2000 so_MRI-CGCM3.nc so_MRI-CGCM3_section.nc
# cp so_MRI-CGCM3_section /data/NAS-ph290/ph290/cmip5/profiles/

import iris
import numpy as np
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import matplotlib.pyplot as plt
import iris
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker
# from scipy.interpolate import griddata
from matplotlib.mlab import griddata

def extract_region(cube):
    lon_west = -20.0
    lon_east = -10.0
    lat_south = 70.0
    lat_north = 80.0
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
    return cube_region


def area_avg(cube):
    try:
        cube.coord('latitude').guess_bounds()
    except:
        pass
    try:
        cube.coord('longitude').guess_bounds()
    except:
        pass
    grid_areas = iris.analysis.cartography.area_weights(cube)
    return cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

#data from https://www.nodc.noaa.gov/cgi-bin/OC5/gin-seas-climate/ginregcl.pl
annual_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
# winter_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
# spring_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
# summer_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
# autumn_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]
#
# summer_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
# winter_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
ann_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]

"""

cube_region_aa_t = area_avg(extract_region(annual_cube))
cube_region_aa_s = area_avg(extract_region(ann_sal_cube))

fs = []
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_CCSM4_profile.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_GISS-E2-R_profile.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_HadGEM2-ES_profile.nc')
# fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_MIROC-ESM_profile.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_MPI-ESM-P_profile.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_MRI-CGCM3_profile.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_bcc-csm1-1_profile.nc')

cs = []
[cs.append(iris.load_cube(f1).collapsed(['time','latitude','longitude'],iris.analysis.MEAN)) for f1 in fs]

fs2 = []
fs2.append('/data/NAS-ph290/ph290/cmip5/profiles/so_CCSM4_profile.nc')
fs2.append('/data/NAS-ph290/ph290/cmip5/profiles/so_GISS-E2-R_profile.nc')
fs2.append('/data/NAS-ph290/ph290/cmip5/profiles/so_HadGEM2-ES_profile.nc')
# fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_MIROC-ESM_profile.nc')
fs2.append('/data/NAS-ph290/ph290/cmip5/profiles/so_MPI-ESM-P_profile.nc')
fs2.append('/data/NAS-ph290/ph290/cmip5/profiles/so_MRI-CGCM3_profile.nc')
fs2.append('/data/NAS-ph290/ph290/cmip5/profiles/so_bcc-csm1-1_profile.nc')

cs2 = []
[cs2.append(iris.load_cube(f1).collapsed(['time','latitude','longitude'],iris.analysis.MEAN)) for f1 in fs2]

plt.close('all')
fig, axs = plt.subplots(1, 2, figsize=(6, 3))
ax1=axs[0]
ax1.plot(cube_region_aa_t.data-np.mean(cube_region_aa_t.data),cube_region_aa_t.coord('depth').points * -1.0,'k',lw=3,alpha=0.6)
for c in cs:
    ax1.plot((c.data-273.15)-np.mean(c.data-273.15), c.coord('depth').points * -1.0,'r',lw=1,alpha=0.6)

ax1.set_xlabel('Temperature anomaly from profile mean ($^\circ}$C)')
plt.ylabel('depth (m)')
# plt.title('Observations')
# plt.show(block = False)

ax2=axs[1]
ax2.plot(cube_region_aa_s.data-np.mean(cube_region_aa_s.data),cube_region_aa_s.coord('depth').points * -1.0,'k',lw=3,alpha=0.6)
for c in cs2:
    ax2.plot((c.data)-np.mean(c.data), c.coord('depth').points * -1.0,'b',lw=1,alpha=0.6)
# ax1.set_xlabel('Temperature anomaly from profile mean ($^\circ}$C)')
ax2.set_xlabel('Salinity anomaly from profile mean ')
ax1.set_ylabel('depth (m)')
ax2.set_ylabel('depth (m)')
ax1.set_ylim([-1000,0])
ax2.set_ylim([-1000,0])
# plt.title('Observations')
plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/GIN_profiles.png')
plt.show(block = False)

"""


fs = []
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_CCSM4_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_GISS-E2-R_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_HadGEM2-ES_section.nc')
# fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_MIROC-ESM_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_MPI-ESM-P_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_MRI-CGCM3_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/thetao_bcc-csm1-1_section.nc')

cs = []
[cs.append(iris.load_cube(f1).collapsed('time',iris.analysis.MEAN)) for f1 in fs]

fs = []
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_CCSM4_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_GISS-E2-R_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_HadGEM2-ES_section.nc')
# fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_MIROC-ESM_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_MPI-ESM-P_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_MRI-CGCM3_section.nc')
fs.append('/data/NAS-ph290/ph290/cmip5/profiles/so_bcc-csm1-1_section.nc')

cs2 = []
[cs2.append(iris.load_cube(f1).collapsed('time',iris.analysis.MEAN)) for f1 in fs]

plt.close('all')
plt.figure(figsize = (7,12))


lat_var = 74.79
minv=-4.0
maxv=4.0

count1=0
count2=0

for i,c in enumerate(cs):
    # plt.subplot(4, 2, i+3)
    plt.subplot2grid((4, 8), (count1+1, count2), colspan=4)
    tit=fs[i].split('/')[-1].split('_')[1]
    loc = np.where(c.coord('latitude').points > lat_var)[0][0]
    cs_plot = iplt.pcolormesh(c[:,loc,:]-273.15,cmap = 'bwr',vmin = minv,vmax=maxv)
    # cs2_plot=iplt.contour(cs2[i][:,loc,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
    # plt.clabel(cs2,[34.75,34.875,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels
    plt.ylim([1000,0])
    plt.xlim([-20,15])
    plt.title(tit)
    if count1 == 2:
        plt.xlabel('longitude ($^\circ$N)')
    if count2 == 0:
        plt.ylabel('depth (m)')
    count1 += 1
    if count1==3:
        count1=0
        count2 = 4


c=annual_cube
# plt.subplot(4, 2, 1)
plt.subplot2grid((4, 8), (0, 0), colspan=4)
loc = np.where(c.coord('latitude').points > lat_var)[0][0]
cs_plot = iplt.pcolormesh(c[:,loc,:],cmap = 'bwr',vmin = minv,vmax=maxv)
# cs2_plot=iplt.contour(ann_sal_cube[:,loc,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
plt.ylim([1000,0])
plt.xlim([-20,15])
# plt.xlabel('longitude ($^\circ$N)')
plt.ylabel('depth (m)')
plt.title('Observations')
# axx = plt.subplot(4, 2, 2)
axx = plt.subplot2grid((4, 8), (0, 4))
cb1 = plt.colorbar(cs_plot, cax=axx,extend='both',orientation='vertical')
cb1.set_label('temperature ($^\circ$C)')

plt.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=2.5, hspace=0.5)

plt.savefig('/home/ph290/Documents/figures/arctic_oceanography_models_v_obs.png')
plt.show(block = False)

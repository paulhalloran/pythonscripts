
import numpy as np
import iris
import matplotlib.pyplot as plt
import glob
from scipy import signal
import scipy
import numpy.ma as ma
import os
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
import running_mean as rm
from scipy.signal import freqz
from scipy.signal import butter, lfilter
import pandas as pd


def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a





start_year = 1000
# start_year = 1000
end_year = 1850
# end_year = 2000

smoothing = 5

# b1, a1 = butter_lowpass(1.0/smoothing, 1.0,6)
b2, a2 = butter_highpass(1.0/smoothing, 1.0,6)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################




#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial = bivalve_data_initial[::-1]  # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
bivalve_data_initial_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data_initial.copy())

bivalve_data_initial -= np.mean(bivalve_data_initial)
bivalve_data_initial /= np.std(bivalve_data_initial)

bivalve_data_initial_filtered -= np.mean(bivalve_data_initial_filtered)
bivalve_data_initial_filtered /= np.std(bivalve_data_initial_filtered)

##########################################
# Greenland STACK T https://www.clim-past.net/12/171/2016/cp-12-171-2016.pdf
#
#positive is warm:
# see text: "We conclude that any conversion of the NG stack..."
#
##########################################

greenland_stack_file = '/data/NAS-geo01/ph290/misc_data/Weissbach_2016/datasets/NG-stack.tab'
greenland_stack = np.genfromtxt(greenland_stack_file, skip_header = 14,delimiter='\t')
greenland_stack_yr = greenland_stack[:,1]
greenland_stack_data = greenland_stack[:,2]
loc = np.where((np.logical_not(np.isnan(greenland_stack_data))) & (greenland_stack_yr >= start_year) & (greenland_stack_yr <= end_year))
greenland_stack_yr = greenland_stack_yr[loc]
greenland_stack_data = greenland_stack_data[loc]
#greenland_stack_data -= np.min(greenland_stack_data)
#greenland_stack_data_sigma_diff-= np.min(greenland_stack_data_sigma_diff)
#greenland_stack_data /= np.max(greenland_stack_data)
#greenland_stack_data_sigma_diff /= np.max(greenland_stack_data)

greenland_stack_data=scipy.signal.detrend(greenland_stack_data)
# greenland_stack_data_filtered = scipy.signal.filtfilt(b2, a2, greenland_stack_data.copy())
greenland_stack_data_filtered = greenland_stack_data

greenland_stack_data -= np.mean(greenland_stack_data)
greenland_stack_data /= np.std(greenland_stack_data)

greenland_stack_data_filtered -= np.mean(greenland_stack_data_filtered)
greenland_stack_data_filtered /= np.std(greenland_stack_data_filtered)


##########################################
# Greenland T https://www.nature.com/articles/s41598-017-01451-7#Sec8
##########################################
greenland_file = '/data/NAS-geo01/ph290/misc_data/greenland_t_Kobashi_2017.csv'
greenland = np.genfromtxt(greenland_file, skip_header = 2,delimiter=',')
greenland[:,0] = 1950 - greenland[:,0]
greenland_yr = greenland[:,0]
greenland_data = greenland[:,1]
greenland_data_minus_2sigma = greenland[:,2]
greenland_data_sigma_diff = greenland_data_minus_2sigma + greenland_data
loc = np.where((np.logical_not(np.isnan(greenland_data_sigma_diff))) & (np.logical_not(np.isnan(greenland_data))) & (greenland_yr >= start_year) & (greenland_yr <= end_year))
greenland_yr = greenland_yr[loc]
greenland_data = greenland_data[loc]
greenland_data_sigma_diff = greenland_data_sigma_diff[loc]

greenland_data=signal.detrend(greenland_data)
greenland_data_filtered = scipy.signal.filtfilt(b2, a2, greenland_data.copy())

greenland_data -= np.mean(greenland_data)
greenland_data /= np.std(greenland_data)

greenland_data_filtered -= np.mean(greenland_data_filtered)
greenland_data_filtered1 = greenland_data_filtered/np.std(greenland_data_filtered)
greenland_yr1 = greenland_yr

##########################################
# Greenland ftp://ftp.ncdc.noaa.gov/pub/data/paleo/icecore/greenland/summit/gisp2/isotopes/gisp2-temperature2011.txt
##########################################

greenland_file = '/data/NAS-geo01/ph290/misc_data/gisp2-temperature2011.txt'
greenland = pd.read_csv(greenland_file,header=99,delim_whitespace=True)
# greenland = np.genfromtxt(greenland_file, skip_header = 126)
greenland_yr = greenland.YearAD.values
greenland_data = greenland.MeanT.values
loc = np.where(np.logical_not(np.isnan((np.logical_not(np.isnan(greenland_data))) & (greenland_yr >= start_year) & (greenland_yr <= end_year))))
greenland_yr = greenland_yr[loc]
greenland_data = greenland_data[loc]

greenland_data=signal.detrend(greenland_data)
greenland_data_filtered = scipy.signal.filtfilt(b2, a2, greenland_data.copy())

greenland_data -= np.mean(greenland_data)
greenland_data /= np.std(greenland_data)

greenland_data_filtered -= np.mean(greenland_data_filtered)
greenland_data_filtered /= np.std(greenland_data_filtered)



##########################################
# fennoscandinavia d13C
##########################################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]


loc = np.where((np.logical_not(np.isnan(s_d))) & (s_y >= start_year) & (s_y <= end_year))
s_y = s_y[loc[0]]
s_d = s_d[loc[0]]
# s_d=scipy.signal.detrend(s_d)
# s_d = scipy.signal.filtfilt(b1, a1, s_d)
s_d = scipy.signal.filtfilt(b2, a2, s_d)

s_d -= np.mean(s_d)
s_d /= np.std(s_d)



'''
#Volcanic forcing
'''

expected_years = np.arange(start_year,end_year)

file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

# data1 = np.genfromtxt(file1)
# data2 = np.genfromtxt(file2)

data1 = np.genfromtxt(file2)
data2 = np.genfromtxt(file4)


data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]
data_tmp[:,1] = data2[:,1]

data = np.mean(data_tmp,axis = 1)
data_final = data1.copy()
data_final[:,1] = data

volc_years = data_final[:,0]

loc = np.where((volc_years >= 850) & (volc_years <= 1849))[0]
volc_years = data_final[loc,0]
volc_data = data_final[loc,1]

volc_data_yrly = expected_years.copy() * np.nan

for i,yr in enumerate(expected_years):
	loc = np.where(np.floor(volc_years) == yr)
	if np.size(loc) > 0:
		volc_data_yrly[i] = np.nanmean(volc_data[loc[0]])



##################
# figure
##################




plt.close('all')
# plt.figure()

fig, (ax1) = plt.subplots(1, 1, sharex=True,figsize=(8,4))



ax1.plot(greenland_stack_yr, greenland_stack_data, lw=1,alpha = 0.2,color='b')
ax1.plot(greenland_stack_yr, greenland_stack_data_filtered, lw=2,alpha = 0.7,color='b',label = 'Wei${\ss}$bach et al., Greenland $\delta^{18}$0 stack (+ve = warm)')
# ax1.plot(greenland_yr1, greenland_data_filtered1, lw=2,alpha = 0.7,color='g',label = 'kobashi et al., Greenland $\delta^{18}$0')
# ax1.plot(greenland_yr, greenland_data_filtered, lw=2,alpha = 0.7,color='k',label = 'gisp2 Greenland $\delta^{18}$0')

# ax1.plot(greenland_yr, greenland_data * -1.0, lw=1,alpha = 0.2,color='k')
# ax1.plot(greenland_yr, greenland_data_filtered * -1.0, lw=2,alpha = 0.7,color='k',label = 'Greenland T')


ax1.plot(bivalve_year, bivalve_data_initial*-1.0, lw=1,alpha = 0.2 , color='r')
ax1.plot(bivalve_year, bivalve_data_initial_filtered*-1.0, lw=2,alpha = 0.7 , color='r',label = '-1.0 * Reynolds et al. bivalve $\delta^{18}$0')

plt.legend(ncol=2,prop={'size':12}).draw_frame(False)


ax2 = ax1.twinx()
ax2.plot(expected_years,volc_data_yrly,'k',alpha=0.8)


plt.xlabel('calendar year')
# plt.ylabel('normalised value (+ve warmer)')

plt.xlim([1000,1800])

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/proxy_bivalve_greenland_timeseries.png')
plt.show(block = False)

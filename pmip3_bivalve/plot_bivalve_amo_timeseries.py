
import numpy as np
import iris
import matplotlib.pyplot as plt
import glob
from scipy import signal
import scipy
import numpy.ma as ma
import os
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
import running_mean as rm
from scipy.signal import freqz
from scipy.signal import butter, lfilter



def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a







def smoothing_script(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    np.hanning, np.hamming, np.bartlett, np.blackman, np.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y


start_year = 950
end_year = 1850

smoothing = 7

# b1, a1 = butter_lowpass(1.0/smoothing, 1.0,6)
b2, a2 = butter_highpass(1.0/smoothing, 1.0,6)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################




#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial = bivalve_data_initial[::-1]  # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
bivalve_data_initial_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data_initial.copy())

bivalve_data_initial -= np.mean(bivalve_data_initial)
bivalve_data_initial /= np.std(bivalve_data_initial)

bivalve_data_initial_filtered -= np.mean(bivalve_data_initial_filtered)
bivalve_data_initial_filtered /= np.std(bivalve_data_initial_filtered)

##########################################
# Mann AMO data
##########################################
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)
amo_data_filtered = scipy.signal.filtfilt(b2, a2, amo_data.copy(),padlen=20)

amo_data -= np.mean(amo_data)
amo_data /= np.std(amo_data)

amo_data_filtered -= np.mean(amo_data_filtered)
amo_data_filtered /= np.std(amo_data_filtered)

amo_scr_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo_scr = np.genfromtxt(amo_scr_file, skip_header = 2)
amo_scr_yr = amo_scr[:,0]
amo_scr_data = amo_scr[:,1]
amo_scr_data_minus_2sigma = amo_scr[:,2]
amo_scr_data_sigma_diff = amo_scr_data_minus_2sigma + amo_scr_data
loc = np.where((np.logical_not(np.isnan(amo_scr_data_sigma_diff))) & (np.logical_not(np.isnan(amo_scr_data))) & (amo_scr_yr >= start_year) & (amo_scr_yr <= end_year))
amo_scr_yr = amo_scr_yr[loc]
amo_scr_data = amo_scr_data[loc]
amo_scr_data_sigma_diff = amo_scr_data_sigma_diff[loc]
#amo_scr_data -= np.min(amo_scr_data)
#amo_scr_data_sigma_diff-= np.min(amo_scr_data_sigma_diff)
#amo_scr_data /= np.max(amo_scr_data)
#amo_scr_data_sigma_diff /= np.max(amo_scr_data)
amo_scr_data=signal.detrend(amo_scr_data)
amo_scr_data_filtered = scipy.signal.filtfilt(b2, a2, amo_scr_data.copy(),padlen=0)

amo_scr_data -= np.mean(amo_scr_data)
amo_scr_data /= np.std(amo_scr_data)

amo_scr_data_filtered -= np.mean(amo_scr_data_filtered)
amo_scr_data_filtered /= np.std(amo_scr_data_filtered)



##########################################
# fennoscandinavia d13C
##########################################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]


loc = np.where((np.logical_not(np.isnan(s_d))) & (s_y >= start_year) & (s_y <= end_year))
s_y = s_y[loc[0]]
s_d = s_d[loc[0]]
# s_d=scipy.signal.detrend(s_d)
# s_d = scipy.signal.filtfilt(b1, a1, s_d)
s_d = scipy.signal.filtfilt(b2, a2, s_d)

s_d -= np.mean(s_d)
s_d /= np.std(s_d)



##################
# figure
##################


plt.close('all')
# plt.figure()

fig, (ax1) = plt.subplots(1, 1, sharex=True,figsize=(8,4))

# ax1.plot(s_y, s_d, color='black')
ax1.plot(amo_yr, amo_data, lw=1,alpha = 0.2, color='g',label = 'Mann et al., AMO index')
ax1.plot(amo_scr_yr, amo_scr_data, lw=1,alpha = 0.2,color='g',label = 'Mann et al., AMO index (screened)')
ax1.plot(amo_yr, amo_data_filtered, lw=2,alpha = 0.5, color='g')
ax1.plot(amo_scr_yr, amo_scr_data_filtered, lw=2,alpha = 0.7,color='g')

ax1.plot(bivalve_year, bivalve_data_initial*-1.0, lw=1,alpha = 0.2 , color='r',label = '-1 * Reynolds et al. bivalve $\delta^{18}$0')
ax1.plot(bivalve_year, bivalve_data_initial_filtered*-1.0, lw=2,alpha = 0.7 , color='r')


plt.legend(ncol=2,prop={'size':12}).draw_frame(False)

plt.xlabel('calendar year')
plt.ylabel('normalised value (+ve warmer)')

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/proxy_bivalve_mann_timeseries.png')
plt.show(block = False)

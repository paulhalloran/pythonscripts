
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
	models = np.unique(models_tmp)
	return models


def calc_d18O(T,S):
	#S = ABSOLUTE SALINITY psu
	#T = ABSOLUTE T deg' C
	# d18Osw_synth = ((0.61*S)-21.3)
	d18Osw_synth = ((3.0*S)-105)
	#R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
	#Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
	# 	d18Osw_synth = ((0.55*S)-18.98)
	#LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
	# d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
	# d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
	#Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
	d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
	#From Reynolds - the -27 is to convert between SMOW and vPDB
	return d18Oc_synth


with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VI.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)

models = np.array(models)

###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}


giss_test = 0

for i,model in enumerate(models):
        if model == 'GISS-E2-R':
                if giss_test == 0:
                        pmip3_str[model] = max_strm_fun_26[i]
                        pmip3_year_str[model] = model_years[i]
                        giss_test += 1
        if model <> 'GISS-E2-R':
                pmip3_str[model] = max_strm_fun_26[i]
                pmip3_year_str[model] = model_years[i]



lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

out_dir = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'
#from ./palaeo_amo/latest_270716/calculate_T_and_S_no_regridding.py

#~ #########
#~ # intermediate processing step - just run once
#~ #########
#~
# files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*.nc')
#### files = glob.glob('/'.join(out_dir.split('/')[0:-2])+'/*HadG*.nc')
#
# for i,my_file in enumerate(files):
# 	print 'processing ',i,' in ',np.size(files)
# 	name = my_file.split('/')[-1]
# 	test = glob.glob(out_dir+name)
# 	if np.size(test) == 0:
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = out_dir+name,  options = '-P 7')
# 		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' '+my_file, output = out_dir+name,  options = '-P 7')



directory = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'
#using /home/ph290/Documents/python_scripts/palaeo_amo/latest_270716/calculate_T_and_S_no_regridding.py

start_year = 1200
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory)
models = list(models)
# models.remove('CCSM4') # Maybe does things right all way through
# models.remove('CSIRO-Mk3L-1-2') # Maybe does things right all way through
models.remove('FGOALS-s2') # no real correlation or anti correlation all wrong
models.remove('GISS-E2-R')  # Maybe does things right NO PLS
models.remove('HadGEM2-ES') # Looks like inverse... NO TAS
# models.remove('MIROC-ESM') # mixture - maybe mostly inverse
# models.remove('MPI-ESM-P') # perhaps switches sign - looks like mm mean
# models.remove('MRI-CGCM3') #Maybe switches sign, not a lot of correlation


# models.remove('HadCM3')
models.remove('bcc-csm1-1')

##########################################
# Model Data # Summer T and S, to 100m mean
##########################################

# models_list = models
#
# for models in models_list:
# 	models = [models]



mm_mean_t = np.zeros([np.size(models),np.size(expected_years)])
mm_mean_t[:,:] = np.nan
mm_mean_s = mm_mean_t.copy()
mm_mean_d18O = mm_mean_t.copy()
mm_mean_d18O_s_only = mm_mean_t.copy()
mm_mean_d18O_t_only = mm_mean_t.copy()

for i,model in enumerate(models):
	print i
	c1 = iris.load_cube(directory + 'thetao_mean_'+model+'_past1000_*_JJA.nc')[:,0,0]
	c2 = iris.load_cube(directory + 'so_mean_'+model+'_past1000_*_JJA.nc')[:,0,0]
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	for j,y in enumerate(expected_years):
		loc = np.where(year == y)[0][0]
		if np.size(loc) > 0:
# 			try:
				mm_mean_t[i,j] = c1[loc].data
				mm_mean_s[i,j] = c2[loc].data
				mm_mean_d18O[i,j] = calc_d18O(c1[loc].data,c2[loc].data)
	# 		except:
	# 			print 'year missing'
	#




directory2 = '/data/NAS-ph290/ph290/cmip5/last1000/'

def anomaly_map(var_name,models,ta1,ta2,tb1,tb2,directory2):
	var = np.zeros([np.size(models),180,360])
	for i,model in enumerate(models):
		print i
		c1 = iris.load_cube(directory2 + model+'_'+var_name+'_past1000_r1i1p1_regridded.nc')
		# qplt.contourf(c1[0])
		# plt.gca().coastlines()
		# plt.show()
		iris.coord_categorisation.add_year(c1, 'time', name='year')
		year = c1.coord('year').points
		loc = np.where((year > ta1) & (year <= ta2))
		c1 = c1[loc]
		year = c1.coord('year').points
		cube_data = c1.data
		cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
		c1.data = cube_data_detrended
		loc = np.where((year > tb1) & (year <= tb2))
		tmp = c1[loc].collapsed('time',iris.analysis.MEAN) - c1.collapsed('time',iris.analysis.MEAN)
		var[i] = tmp.data
	var_mean = np.nanmean(var,axis=0)
	mean_var_cube = c1[0]
	mean_var_cube.data = var_mean
	return mean_var_cube



def anomaly_map_sic(var_name,models,ta1,ta2,tb1,tb2,directory2):
	# var_name = 'sic'
	# ta1 = 1200
	# ta2 = 1600
	# tb1 = 1350
	# tb2 = 1375
	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	for i,model in enumerate(models):
		print i
		c1 = iris.load_cube(directory2 + model+'_'+var_name+'_past1000_r1i1p1_regridded.nc')
		# qplt.contourf(c1[0],31)
		# plt.gca().coastlines()
		# plt.show()
		try:
			missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
		except:
			missing_data = np.where(c1[0].data.data == 0.0)
		iris.coord_categorisation.add_year(c1, 'time', name='year')
		year = c1.coord('year').points
		loc = np.where((year > ta1) & (year <= ta2))
		c1 = c1[loc]
		year = c1.coord('year').points
		cube_data = c1.data
		cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
		c1.data = cube_data_detrended
		loc = np.where((year > tb1) & (year <= tb2))
		tmp = c1[loc].collapsed('time',iris.analysis.MEAN) - c1.collapsed('time',iris.analysis.MEAN)
		tmp = tmp.data
		tmp[missing_data] = 0.0
		var[i] = tmp
	var_mean = np.nanmean(var,axis=0)
	c1 = iris.load_cube(directory2 + model+'_'+var_name+'_past1000_r1i1p1_regridded.nc')
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube




a = 1290
b = 1320
mean_tas_cube0	= anomaly_map('tas',models,1200, 1600,a, b,directory2)
mean_psl_cube0	= anomaly_map('psl',models,1200, 1600,a, b,directory2)
mean_sic_cube0	= anomaly_map_sic('sic',models,1200, 1600,a, b,directory2)

a = 1350
b = 1380
mean_tas_cube1	= anomaly_map('tas',models,1200, 1600,a, b,directory2)
mean_psl_cube1	= anomaly_map('psl',models,1200, 1600,a, b,directory2)
mean_sic_cube1	= anomaly_map_sic('sic',models,1200, 1600,a, b,directory2)

a = 1420
b = 1440
mean_tas_cube2	= anomaly_map('tas',models,1200, 1600,a, b,directory2)
mean_psl_cube2	= anomaly_map('psl',models,1200, 1600,a, b,directory2)
mean_sic_cube2	= anomaly_map_sic('sic',models,1200, 1600,a, b,directory2)

a = 1480
b = 1515
mean_tas_cube3	= anomaly_map('tas',models,1200, 1600,a, b,directory2)
mean_psl_cube3	= anomaly_map('psl',models,1200, 1600,a, b,directory2)
mean_sic_cube3	= anomaly_map_sic('sic',models,1200, 1600,a, b,directory2)

mean_tas_cube = mean_tas_cube0.copy()
mean_psl_cube = mean_psl_cube0.copy()
mean_sic_cube = mean_sic_cube0.copy()


var = np.zeros([4,180,360])
var[0] = mean_tas_cube0.data
var[1] = mean_tas_cube1.data
var[2] = mean_tas_cube2.data
var[3] = mean_tas_cube3.data
mean_tas_cube.data = np.nanmean(var,axis=0)

var[0] = mean_psl_cube0.data
var[1] = mean_psl_cube1.data
var[2] = mean_psl_cube2.data
var[3] = mean_psl_cube3.data
mean_psl_cube.data = np.nanmean(var,axis=0)

var[0] = mean_sic_cube0.data
var[1] = mean_sic_cube1.data
var[2] = mean_sic_cube2.data
var[3] = mean_sic_cube3.data
mean_sic_cube.data = np.nanmean(var,axis=0)

pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
pmip3_model_streamfunction[:] = np.NAN

"""

plt.figure(2)
qplt.contourf(mean_tas_cube,np.linspace(-0.5,0.5,31))
plt.gca().coastlines()
plt.show(block = False)

plt.figure(3)
qplt.contourf(mean_psl_cube,np.linspace(-20,20,31))
plt.gca().coastlines()
plt.show(block = False)

plt.figure(4)
qplt.contourf(mean_sic_cube,np.linspace(-3,3,31))
plt.gca().coastlines()
plt.show(block = False)


qplt.contourf(mean_tas_cube0,np.linspace(-1,1,31))
plt.gca().coastlines()
plt.show(block = True)

plt.figure(2)
qplt.contourf(mean_tas_cube1,np.linspace(-1,1,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_tas_cube2,np.linspace(-1,1,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_tas_cube3,np.linspace(-1,1,31))
plt.gca().coastlines()
plt.show(block = True)

plt.figure(2)
qplt.contourf(mean_psl_cube0,np.linspace(-20,20,31))
plt.gca().coastlines()
plt.show(block = True)

plt.figure(2)
qplt.contourf(mean_psl_cube1,np.linspace(-20,20,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_psl_cube2,np.linspace(-20,20,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_psl_cube3,np.linspace(-20,20,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_sic_cube0,np.linspace(-5,5,31))
plt.gca().coastlines()
plt.show(block = True)

plt.figure(2)
qplt.contourf(mean_sic_cube1,np.linspace(-5,5,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_sic_cube2,np.linspace(-5,5,31))
plt.gca().coastlines()
plt.show(block = True)


plt.figure(2)
qplt.contourf(mean_sic_cube3,np.linspace(-5,5,31))
plt.gca().coastlines()
plt.show(block = True)

"""

#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
for i,model in enumerate(models):
	print model
	tmp = pmip3_str[model]
	loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
	tmp = tmp[loc]
	yrs = pmip3_year_str[model][loc]
	data2=signal.detrend(tmp)
	# data2 = data2-np.min(data2)
	# data3 = data2/(np.max(data2))
	data3 = data2
	for index,y in enumerate(expected_years):
		loc2 = np.where(yrs == y)
		if np.size(loc2) != 0:
			pmip3_model_streamfunction[index,i] = data3[loc2]


pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)




##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial[::-1])

###########################################
# Read in Reynolds d18O data raw #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))))
# & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_year_2 = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial_2 = tmp[::-1]
bivalve_data_initial_2 -= np.nanmean(bivalve_data_initial_2)
bivalve_data_initial_2 *= 0.75
bivalve_data_initial_2 += 0.5

##########################################
# European summer temps #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/Guiot_et_al_2005/guiot05_eurot.txt'
r_data = np.genfromtxt(r_data_file,skip_header = 7,delimiter = ',')
tmp = r_data[:,2]
loc = np.where(tmp > -9.99990000e+02)
tmp_yr = r_data[loc[0],1]
euro_summ_t = tmp[loc[0]]
euro_summ_t -= np.min(euro_summ_t)
euro_summ_t /= np.max(euro_summ_t)
euro_summ_t_year = tmp_yr


##########################################
# N. hem temps #
##########################################


r_data_file = '/data/NAS-geo01/ph290/misc_data/wilson_2016.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
n_hem_t = tmp
n_hem_t_year = tmp_yr

##########################################
# Read in Masse 2008 seaice data #
##########################################

m_data_file = '/data/NAS-ph290/ph290/misc_data/masse_sea_ice.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ',')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
ip25_data_initial = tmp
ip25_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# ip25_data_initial=scipy.signal.detrend(ip25_initial[::-1])
ip25_data_initial=(ip25_data_initial[::-1])


##########################################
# solar
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/solar_cmip5/tsi_VK.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 4,delimiter = ' ')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
solar_data = tmp
solar_year = tmp_yr


##########################################
# volc
##########################################

m_data_file = '/data/NAS-ph290/ph290/cmip5/forcing_data/ICI5_030N_AOD_c.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 0,delimiter = '\t')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
volc_data = tmp
volc_year = tmp_yr


##########################################
# insolation
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/insol91.jun_dec'
m_data = np.genfromtxt(m_data_file,skip_header = 1,delimiter = ' ')
tmp1 = m_data[:,2]
tmp2 = m_data[:,9]
tmp_yr = m_data[:,0]
f1 = scipy.interpolate.interp1d(tmp_yr, tmp1)
f2 = scipy.interpolate.interp1d(tmp_yr, tmp2)
ins_year = np.linspace(start_year,end_year,(end_year - start_year + 1))
ins_june = f1(ins_year)
ins_dec = f2(ins_year)


##########################################
# Anthro radiative forcing
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/20THCENTURY_MIDYEAR_RADFORCING.DAT'
m_data = np.genfromtxt(m_data_file,skip_header = 61,delimiter = ',')
tmp1 = m_data[:,5]
tmp_yr = m_data[:,1]
anthro_forcing = tmp1
anthro_year = tmp_yr


##########################################
# Mann AMO data
##########################################
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)


##########################
# detrending each model individually
##########################


for i,model in enumerate(models):
	mm_mean_t[i,:] = scipy.signal.detrend(mm_mean_t[i,:])
	mm_mean_s[i,:] = scipy.signal.detrend(mm_mean_s[i,:])
	#~ mm_mean_d18O[i,:] = scipy.signal.detrend(mm_mean_d18O[i,:])
	mm_mean_d18O[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,mm_mean_s[i,:]+35.0)
	mm_mean_d18O_s_only[i,:] = calc_d18O(273.15+(mm_mean_t[i,:]*0.0) +5.0,mm_mean_s[i,:]+35.0)
	mm_mean_d18O_t_only[i,:] = calc_d18O(273.15+mm_mean_t[i,:] +5.0,0.0*mm_mean_s[i,:]+35.0)
	mm_mean_d18O[i,:] -= np.nanmean(mm_mean_d18O[i,:])
	mm_mean_d18O_s_only[i,:] -= np.nanmean(mm_mean_d18O_s_only[i,:])
	mm_mean_d18O_t_only[i,:] -= np.nanmean(mm_mean_d18O_t_only[i,:])



mm_mean_t_filtered = mm_mean_t.copy()
mm_mean_s_filtered = mm_mean_s.copy()
mm_mean_d18O_filtered = mm_mean_d18O.copy()
mm_mean_d18O_filtered_s_only = mm_mean_d18O_s_only.copy()
mm_mean_d18O_filtered_t_only = mm_mean_d18O_t_only.copy()




##########################
#  Multi Model mean
##########################

mm_mean_t_m_filtered = np.nanmean(mm_mean_t_filtered,axis = 0)
mm_mean_s_m_filtered = np.nanmean(mm_mean_s_filtered,axis = 0)
mm_mean_d18O_m_filtered = np.nanmean(mm_mean_d18O_filtered,axis = 0)
mm_mean_d18O_m_filtered_s_only = np.nanmean(mm_mean_d18O_filtered_s_only,axis = 0)
mm_mean_d18O_m_filtered_t_only = np.nanmean(mm_mean_d18O_filtered_t_only,axis = 0)

##########################
#  Normalisation
##########################

y = bivalve_data_initial.copy()
x = bivalve_year.copy()

y -= np.min(y)
y /= np.max(y)

mm_mean_t_m_filtered -= np.min(mm_mean_t_m_filtered)
mm_mean_t_m_filtered /= np.max(mm_mean_t_m_filtered)

mm_mean_s_m_filtered -= np.min(mm_mean_s_m_filtered)
mm_mean_s_m_filtered /= np.max(mm_mean_s_m_filtered)

mm_mean_d18O_m_filtered -= np.min(mm_mean_d18O_m_filtered)
mm_mean_d18O_m_filtered /= np.max(mm_mean_d18O_m_filtered)

mm_mean_d18O_m_filtered_s_only -= np.min(mm_mean_d18O_m_filtered_s_only)
mm_mean_d18O_m_filtered_s_only /= np.max(mm_mean_d18O_m_filtered_s_only)

mm_mean_d18O_m_filtered_t_only -= np.min(mm_mean_d18O_m_filtered_t_only)
mm_mean_d18O_m_filtered_t_only /= np.max(mm_mean_d18O_m_filtered_t_only)


file = '/data/NAS-ph290/ph290/misc_data/last_mill_precip/data.csv'
p_data = np.genfromtxt(file,skip_header = 1,delimiter = ',')

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]
s_d = rm.running_mean(s_d,5)*-1.0
s_d -= np.nanmin(s_d)
s_d /= np.nanmax(s_d)

plt.close('all')

fig, ax = plt.subplots(4,1,figsize=(14, 10))

ax[0].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
# ax[2].plot(euro_summ_t_year,((euro_summ_t * -1.0)*0.5)+0.5,'b',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[0].plot(expected_years[0:400],mm_mean_d18O_m_filtered_t_only[0:400],'r',alpha = 0.5,linewidth = 1.5)
ax[0].plot(expected_years[400::],mm_mean_d18O_m_filtered_t_only[400::],color = '0.2',alpha = 0.3,linewidth = 1.5)
# ax[0].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)
a = solar_data.copy()
a *= -1.0
a -= np.min(a)
a /= np.max(a)
ax[0].plot(solar_year,a,color = 'b',alpha = 0.5,linewidth = 1.5)
# ax[0].plot(volc_year,(rmp.running_mean_post(volc_data,7*36)*5.0),'k',alpha=0.75)
# ax[0].scatter(p_data[:,0],p_data[:,1])
# ax[0].plot(s_y,s_d,alpha = 0.5)

# ax[0].axvspan(1200, 1600, color='y', alpha=0.2, lw=0)

ax[1].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[1].plot(expected_years[0:400],mm_mean_d18O_m_filtered[0:400],'0.2',alpha = 0.3,linewidth = 1.5)
ax[1].plot(expected_years[400::],mm_mean_d18O_m_filtered[400::],color = 'r',alpha = 0.5,linewidth = 1.5)
# ax[1].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)
# ax[0].axvspan(1200, 1600, color='y', alpha=0.2, lw=0)

# ax[2].plot(x,y,'k',alpha = 0.8,linewidth = 1.5)
pmip3_multimodel_mean_streamfunction -= np.min(pmip3_multimodel_mean_streamfunction)
pmip3_multimodel_mean_streamfunction /= np.max(pmip3_multimodel_mean_streamfunction)
amo_data -= np.min(amo_data)
amo_data /= np.max(amo_data)
a = pmip3_multimodel_mean_streamfunction[0:400]
b=pmip3_multimodel_mean_streamfunction[400::]
ax[2].plot(expected_years[0:400],a,'g',alpha = 0.5,linewidth = 1.5)
ax[2].plot(expected_years[400::],b,color = 'g',alpha = 0.5,linewidth = 1.5)
# ax[2].plot(expected_years[0:400],mm_mean_d18O_m_filtered_t_only[0:400],'r',alpha = 0.5,linewidth = 1.5)
# ax[2].plot(expected_years[400::],mm_mean_d18O_m_filtered[400::],color = 'r',alpha = 0.5,linewidth = 1.5)
# ax[2].plot(amo_yr,amo_data,color = 'b',alpha = 0.5,linewidth = 1.5)
ax[2].plot(volc_year,(rmp.running_mean_post(volc_data,7*36)*-5.0)+0.6,'k',alpha=0.75)
ax[2].plot(expected_years[0:400],(mm_mean_d18O_m_filtered_t_only[0:400] * -1.0 )+1.0,'r',alpha = 0.5,linewidth = 1.5)
# ax[2].plot(expected_years[400::],(mm_mean_d18O_m_filtered[400::]),color = 'r',alpha = 0.5,linewidth = 1.5)

# ax[2].plot(euro_summ_t_year,((euro_summ_t * -1.0)*0.5)+0.5,'b',alpha = 0.8,linewidth = 1.5)
#Synthetic d18O
ax[3].plot(expected_years[0:400],mm_mean_d18O_m_filtered_t_only[0:400],'r',alpha = 0.5,linewidth = 1.5)
ax[3].plot(expected_years[400::],mm_mean_d18O_m_filtered_t_only[400::],color = '0.2',alpha = 0.3,linewidth = 1.5)
# ax[0].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)
loc = np.where((s_y >1200) & (s_y <= 1600))
ax[3].plot(s_y[loc],s_d[loc],alpha = 0.5)

ax[0].set_xlim(1200,1850)
ax[1].set_xlim(1200,1850)
ax[2].set_xlim(1200,1850)
ax[3].set_xlim(1200,1850)

ax[2].set_title('amoc (model)')
ax[3].set_title('scandinavian cloud cover * -1.0')

plt.savefig('/home/ph290/Documents/figures/myts.png')
plt.show(block = False)








##################
# paper figure
##################

import matplotlib
matplotlib.rcParams.update({'font.size': 15})

plt.close('all')

fig, ax = plt.subplots(2,1,figsize=(10,5))

ax[0].plot(x,y,'k',alpha = 0.8,linewidth = 1.5,label = 'Bivalve $\delta^{18}$')
#Synthetic d18O
ax[0].plot(expected_years[0:350],mm_mean_d18O_m_filtered_t_only[0:350],'r',alpha = 0.5,linewidth = 1.5,label = 'Model temperature derived $\delta^{18}$')
# ax[0].plot(expected_years[350::],mm_mean_d18O_m_filtered_t_only[350::],color = '0.2',alpha = 0.3,linewidth = 1.5)
ax[0].plot(expected_years[350::],mm_mean_d18O_m_filtered_t_only[350::],'r',alpha = 0.2,linewidth = 1.5)
# ax[0].plot(amo_yr,(amo_data*-0.8)+0.5,color = 'b',alpha = 0.5,linewidth = 1.5)

ax[1].plot(x,y,'k',alpha = 0.8,linewidth = 1.5,label = 'Bivalve $\delta^{18}$')
#Synthetic d18O
# ax[1].plot(expected_years[0:350],mm_mean_d18O_m_filtered[0:350],'r',alpha = 0.2,linewidth = 1.5)
# ax[1].plot(expected_years[350::],mm_mean_d18O_m_filtered[350::],'r',alpha = 0.5,linewidth = 1.5)
ax[1].plot(expected_years[0:350],mm_mean_d18O_m_filtered_s_only[0:350],'b',alpha = 0.2,linewidth = 1.5)
ax[1].plot(expected_years[350::],mm_mean_d18O_m_filtered_s_only[350::],'b',alpha = 0.5,linewidth = 1.5,label = 'Model salinity derived $\delta^{18}$')


ax[0].set_ylabel('normalised\nN. Iceland $\delta^{18}$')
ax[1].set_ylabel('normalised\nN. Iceland $\delta^{18}$')
ax[1].set_xlabel('calendar year')

ax[0].set_ylim(0,1.2)
ax[1].set_ylim(0,1.2)

#ax[0].legend(bbox_to_anchor=(0.7, 1.6), ncol=3)
lg1 = ax[0].legend(bbox_to_anchor=(1.0, 1.1),ncol=3)
lg1.draw_frame(False)

lg2 = ax[1].legend(bbox_to_anchor=(1.0, 1.1),ncol=3)
lg2.draw_frame(False)

plt.tight_layout()

plt.savefig('/home/ph290/Documents/figures/myts.png')
plt.show(block = False)


ax[0].set_xlim(1200,1850)
ax[1].set_xlim(1200,1850)

ax[0].set_ylim(0,1.2)
ax[1].set_ylim(0,1.2)

#ax[0].legend(bbox_to_anchor=(0.7, 1.6), ncol=3)
lg = ax[0].legend(bbox_to_anchor=(1.0, 1.1),ncol=3)
lg.draw_frame(False)

plt.tight_layout()

plt.savefig('/home/ph290/Documents/figures/myts.png')
plt.show(block = False)







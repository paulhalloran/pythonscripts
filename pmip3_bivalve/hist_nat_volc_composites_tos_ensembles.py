

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation
import numpy as np
import glob
import scipy
from scipy import signal
import os
import os.path

def detrend_cube(cube):
    cube2 = cube.collapsed('time',iris.analysis.MEAN)
    cube_data = cube.data
    cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    cube.data = cube_data_detrended
    return cube+cube2

def model_names(directory,var):
	files = glob.glob(directory+'*'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models

def ensemble_names(directory,var):
	files = glob.glob(directory+'*'+var+'_*i1p1*.nc')
	ensembles_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			ensembles_tmp.append(file.split('/')[-1].split('_')[3])
			ensembles = np.unique(ensembles_tmp)
	return ensembles

def extract_region(cube,lon_west,lon_east,lat_south,lat_north):
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    return cube_region_tmp.intersection(latitude=(lat_south, lat_north))


directory = '/data/NAS-ph290/ph290/cmip5/historicalNat/regridded/'

models1 = list(model_names(directory,'tos'))
models2 = list(model_names(directory,'sic'))
models = list(np.intersect1d(models1,models2))
models.remove('GFDL-CM3')
models.remove('GFDL-ESM2M')
# models.remove('BNU-ESM')
models.remove('CSIRO-Mk3-6-0') #does not do tos under seaice
models.remove('bcc-csm1-1')
models.remove('CESM1-CAM5-1-FV2')
models.remove('GISS-E2-H')
models.remove('IPSL-CM5A-LR')
# models.remove('MIROC-ESM-CHEM')
# models.remove('NorESM1-M')

ensemble1 = list(ensemble_names(directory,'tos'))
ensemble2 = list(ensemble_names(directory,'sic'))
ensembles = list(np.intersect1d(ensemble1,ensemble2))
# ensembles = ensemble1
# for models in models_all:
#     models = [models]
model_ensemble_list = []


lon_west = -180.0
lon_east = 180.0
lat_south = 35.0
lat_north = 90.0

lags = [1,5,7,10,13]

volcanic_years = np.array([1991])

# volcanic_years = np.array([1963,1968,1974,1982,1991])
# pre_volcanic_years = volcanic_years-1

#####################
# sic
#####################



cube = iris.load_cube(directory+models[7]+'_sic_historicalNat_r1i1p1_regridded.nc')
reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)
template_cube.data[:]=np.nan

reg_cube_shape = np.shape(reg_cube)
data1 = np.zeros([len(models) * len(ensembles),reg_cube_shape[1],reg_cube_shape[2]])
data1[:] = np.nan
data_tos = {}
for i,dummy in enumerate(lags):
    data_sic[i] = data1.copy()

for i,model in enumerate(models):
    print model
    for k,ensemble in enumerate(ensembles):
        print ensemble
        path = directory+model+'_sic_historicalNat_'+ensemble+'_regridded.nc'
        if os.path.isfile(path):
            cube = iris.load_cube(path)
            try:
                iris.coord_categorisation.add_year(cube, 'time', name='year')
                iris.coord_categorisation.add_season(cube, 'time', name='season')
                added_year_OK = True
            except:
                added_year_OK = False
            if added_year_OK:
                # iris.coord_categorisation.add_month(cube, 'time', name='month')
                # cube = cube[np.where(cube.coord('year').points <= 1850)]
                reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
                # reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
                loc = np.where((reg_cube.coord('year').points == 1988) | (reg_cube.coord('year').points == 1989) | (reg_cube.coord('year').points == 1990))
                if np.size(loc) > 0:
                    mean_reg_cube = reg_cube.collapsed('time',iris.analysis.MEAN)
                    reg_cube = reg_cube-mean_reg_cube
                    if (1990 in reg_cube.coord('year').points) & (1960 in reg_cube.coord('year').points):
                        locs={}
                        for m,dummy in enumerate(lags):
                            locs[m] = [np.where(reg_cube.coord('year').points == yr)[0][0] for yr in volcanic_years+lags[m]]
                        for j,loc in enumerate(locs):
                            try:
                                locs[j]
                                loc_flag = True
                            except:
                                loc_flag = False
                            if loc_flag:
                                tmp_data1 = reg_cube[locs[j]].collapsed('time',iris.analysis.MEAN)
                                try:
                                    fill_value = tmp_data1.data.fill_value
                                except:
                                    fill_value = np.nan
                                data_tmp = tmp_data1.data
                                try:
                                    data_tmp[np.where(data_tmp.data == fill_value)] = np.nan
                                    data_tmp[np.where(data_tmp.data > 100.0)] = np.nan
                                    data_tmp[np.where(data_tmp.data < -100.0)] = np.nan
                                except:
                                    data_tmp[np.where(data_tmp == fill_value)] = np.nan
                                    data_tmp[np.where(data_tmp > 100.0)] = np.nan
                                    data_tmp[np.where(data_tmp < -100.0)] = np.nan
                                data_sic[j][(i*len(ensembles))+k,:,:] = data_tmp






#####################
# tos
#####################

cube = iris.load_cube(directory+models[7]+'_tos_historicalNat_r1i1p1_regridded.nc')
reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)
template_cube.data[:]=np.nan

reg_cube_shape = np.shape(reg_cube)
data1 = np.zeros([len(models) * len(ensembles),reg_cube_shape[1],reg_cube_shape[2]])
data1[:] = np.nan
data_tos = {}
for i,dummy in enumerate(lags):
    data_tos[i] = data1.copy()




for i,model in enumerate(models):
    print model
    for k,ensemble in enumerate(ensembles):
        print ensemble
        path = directory+model+'_tos_historicalNat_'+ensemble+'_regridded.nc'
        if os.path.isfile(path):
            cube = iris.load_cube(path)
            try:
                iris.coord_categorisation.add_year(cube, 'time', name='year')
                iris.coord_categorisation.add_season(cube, 'time', name='season')
                added_year_OK = True
            except:
                added_year_OK = False
            if added_year_OK:
                # iris.coord_categorisation.add_month(cube, 'time', name='month')
                reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
                # reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
                reg_cube = reg_cube.aggregated_by(['year'], iris.analysis.MEAN)
                # reg_cube = reg_cube[np.where(cube.coord('season').points == 'd')]
                # reg_cube = reg_cube[np.where(cube.coord('month').points == 'Mar')]
                loc = np.where((reg_cube.coord('year').points == 1988) | (reg_cube.coord('year').points == 1989) | (reg_cube.coord('year').points == 1990))
                if np.size(loc) > 0:
                    mean_reg_cube = reg_cube.collapsed('time',iris.analysis.MEAN)
                    reg_cube = reg_cube-mean_reg_cube
                    if (1990 in reg_cube.coord('year').points) & (1960 in reg_cube.coord('year').points):
                        locs={}
                        for m,dummy in enumerate(lags):
                            locs[m] = [np.where(reg_cube.coord('year').points == yr)[0][0] for yr in volcanic_years+lags[m]]
                        for j,loc in enumerate(locs):
                            try:
                                locs[j]
                                loc_flag = True
                            except:
                                loc_flag = False
                            if loc_flag:
                                tmp_data1 = reg_cube[locs[j]].collapsed('time',iris.analysis.MEAN)
                                try:
                                    fill_value = tmp_data1.data.fill_value
                                except:
                                    fill_value = np.nan
                                data_tmp = tmp_data1.data
                                try:
                                    data_tmp[np.where(data_tmp.data == fill_value)] = np.nan
                                    data_tmp[np.where(data_tmp.data > 100.0)] = np.nan
                                    data_tmp[np.where(data_tmp.data < -100.0)] = np.nan
                                except:
                                    data_tmp[np.where(data_tmp == fill_value)] = np.nan
                                    data_tmp[np.where(data_tmp > 100.0)] = np.nan
                                    data_tmp[np.where(data_tmp < -100.0)] = np.nan
                                data_tos[j][(i*len(ensembles))+k,:,:] = data_tmp
                                if j==0:
                                    model_ensemble_list.append(model+' '+ensemble)

##########
# plotting
##########


"""
for j,model in enumerate(models):
    print model
    plotting_cubes={}
    plotting_cubes[0] = template_cube.copy()
    plotting_cubes[1] = template_cube.copy()
    plotting_cubes[2] = template_cube.copy()
    plotting_cubes2={}
    plotting_cubes2[0] = template_cube.copy()
    plotting_cubes2[1] = template_cube.copy()
    plotting_cubes2[2] = template_cube.copy()

    for i in range(3):
        # tmp_data = np.nanmean(data[i],axis=0)
        tmp_data = data_tos[i][j]
        tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
        plotting_cubes[i].data =tmp_data

    for i in range(3):
        # tmp_data = np.nanmean(data[i],axis=0)
        tmp_data = data_sic[i][j]
        tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
        plotting_cubes2[i].data =tmp_data




    # for k,dummy in enumerate(models):
    #     for i in range(3):
    #         tmp_data = data[i][k]
    #         tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    #         plotting_cubes[i].data =tmp_data

    import iris
    import iris.plot as iplt
    import iris.quickplot as qplt
    import matplotlib.pyplot as plt
    import numpy as np
    import cartopy.crs as ccrs
    import iris.analysis.cartography
    import cartopy.feature as cfeature

    min_color_value = -0.5
    max_color_value = 0.5
    min_color_value2 = -3
    max_color_value2 = 15.0
    cmap = plt.get_cmap('YlGnBu')
    cmap2 = plt.get_cmap('seismic')

    my_extent = [-50, 50, 45, 90]
    no_levs=101

    plt.close('all')
    fig = plt.figure(figsize=(16,4))

    ax0 = plt.subplot(131,projection=ccrs.PlateCarree())
    # ax0 = plt.subplot(131,projection=ccrs.Mollweide())
    ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[2]-273.15,np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
    my_plot2 = iplt.contour(plotting_cubes2[2],np.linspace(0,100,no_levs/10.0),colors='k')
    ax0.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('full climatology')

    ax1 = plt.subplot(132,projection=ccrs.PlateCarree())
    ax1.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[2],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[0]-plotting_cubes2[2],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax1.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('post-volcano')

    ax2 = plt.subplot(133,projection=ccrs.PlateCarree())
    ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[2],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[1]-plotting_cubes2[2],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax2.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('pre-volcano')

    # ax3 = plt.subplot(133,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
    # ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
    # # my_plot = iplt.contourf(plotting_cubes[2]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    # my_plot = iplt.contourf(plotting_cubes[2],np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
    # ax3.add_feature(cfeature.LAND)
    # plt.gca().coastlines('10m')
    # bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    # bar.set_label('tos')
    # plt.title('phase 3')

    # ax3 = plt.subplot(224,projection=ccrs.NorthPolarStereo(central_longitude=0.0))
    # ax3.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    # ax3.add_feature(cfeature.LAND)
    # plt.gca().coastlines('10m')
    # bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    # bar.set_label('tos')
    # plt.title('full climatology')

    # plt.savefig('/home/ph290/Documents/figures/model'+models[0]+'.png')
    # plt.savefig('/home/ph290/Documents/figures/hist_nat_volc_tos_composites.png')
    plt.show(block = True)

"""



####
# multi model means
###

plotting_cubes={}
for i,dummy in enumerate(lags):
    plotting_cubes[i] = template_cube.copy()

plotting_cubes2={}
for i,dummy in enumerate(lags):
    plotting_cubes2[i] = template_cube.copy()

for i,dummy in enumerate(lags):
    tmp_data = np.nanmean(data_tos[i],axis=0)
    tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    plotting_cubes[i].data =tmp_data


for i,dummy in enumerate(lags):
    tmp_data = np.nanmean(data_sic[i],axis=0)
    tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    plotting_cubes2[i].data =tmp_data




# for k,dummy in enumerate(models):
#     for i in range(3):
#         tmp_data = data[i][k]
#         tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
#         plotting_cubes[i].data =tmp_data

import iris
import iris.plot as iplt
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import iris.analysis.cartography
import cartopy.feature as cfeature

min_color_value = -1.0
max_color_value = 1.0
min_color_value2 = -3
max_color_value2 = 15.0
cmap = plt.get_cmap('YlGnBu')
cmap2 = plt.get_cmap('seismic')

my_extent = [-50, 50, 45, 90]
no_levs=31

plt.close('all')
fig = plt.figure(figsize=(20,20))

for i,dummy in enumerate(lags):
    ax0 = plt.subplot(2,5,i+1,projection=ccrs.PlateCarree())
    ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
    my_plot = iplt.contourf(plotting_cubes[i],np.linspace(min_color_value,max_color_value,no_levs),cmap='bwr')
    # my_plot2 = iplt.contourf(plotting_cubes2[i],np.linspace(-10,10,101)
    ax0.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title(str(lags[i]))

for i,dummy in enumerate(lags):
    ax0 = plt.subplot(2,5,i+6,projection=ccrs.PlateCarree())
    ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contour(plotting_cubes[i],np.linspace(min_color_value,max_color_value,no_levs),colors='k',linewidths=0.3)
    my_plot2 = iplt.contourf(plotting_cubes2[i],np.linspace(-50,50,31),cmap='bwr')
    ax0.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot2, orientation='horizontal', extend='both')
    bar.set_label('ice')
    plt.title(str(lags[i]))


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/hist_nat_volc_tos_composites_with ice_ensembles.png')
plt.show(block = False)

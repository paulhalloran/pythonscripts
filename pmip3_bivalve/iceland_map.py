
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker
from eofs.iris import Eof
import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import iris.coord_categorisation
import numpy as np

###########

plt.close('all')
plt.figure(figsize=(6.5, 5))

######### Subplot 1 #########

c1 = iris.load_cube('/data/NAS-geo01/ph290/observations/ostis_clim/ostia_clim_mean.nc','sea_surface_temperature')[0]
#sea ice fraction
c2 = iris.load_cube('/data/NAS-geo01/ph290/observations/ostis_clim/ostia_clim_mean.nc','sea ice fraction')[0]

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((1, 1), (0, 0),projection= my_projection)

ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = c1-273.15
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(0,30,55),transform=ccrs.PlateCarree(),cmap='jet')

cube1 = c2
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data
contour_result12 = ax.contour(lons1, lats1, data1,np.linspace(0,1,20),colors='k',linewidths=0.1,transform=ccrs.PlateCarree())

cb = plt.colorbar(contour_result1,fraction=0.046, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('SST $\circ$C')

coast_10m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor=cfeature.COLORS['land'])
ax.add_feature(land_10m,edgecolor='none',facecolor='#999999')
ax.add_feature(coast_10m,edgecolor='#000000')

plt.plot(-18.195667,66.5215, 'r*',markersize = 20, transform=ccrs.PlateCarree())
# plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())
ax.gridlines()


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/iceland_context.png', dpi=900)
plt.show(block = False)

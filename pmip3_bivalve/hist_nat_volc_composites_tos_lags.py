

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation
import numpy as np
import glob
import scipy
from scipy import signal
import os

def detrend_cube(cube):
    cube2 = cube.collapsed('time',iris.analysis.MEAN)
    cube_data = cube.data
    cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    cube.data = cube_data_detrended
    return cube+cube2

def model_names(directory,var):
	files = glob.glob(directory+'*'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models

def extract_region(cube,lon_west,lon_east,lat_south,lat_north):
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    return cube_region_tmp.intersection(latitude=(lat_south, lat_north))


directory = '/data/NAS-ph290/ph290/cmip5/historicalNat/regridded/'

models = list(model_names(directory,'tos'))
# models2 = list(model_names(directory,'sic'))
# models = list(np.intersect1d(models1,models2))

models.remove('GFDL-CM3')
models.remove('GFDL-ESM2M')
# models.remove('BNU-ESM')
models.remove('CSIRO-Mk3-6-0') #does not do tos under seaice


# for models in models_all:
#     models = [models]


lon_west = -180.0
lon_east = 180.0
lat_south = 00.0
lat_north = 90.0


#####################
# sic
#####################
#
# cube = iris.load_cube(directory+models[1]+'_sic_historicalNat_r1i1p1_regridded.nc')
# reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
# template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)
#
# reg_cube_shape = np.shape(reg_cube)
# data1 = np.zeros([len(models),reg_cube_shape[1],reg_cube_shape[2]])
# data1[:] = np.nan
# data_sic = {}
# data_sic[0] = data1.copy()
# data_sic[1] = data1.copy()
# data_sic[2] = data1.copy()
# data_sic[3] = data1.copy()
# data_sic[4] = data1.copy()
#
#
#
# for i,model in enumerate(models):
#     print model
#     cube = iris.load_cube(directory+model+'_sic_historicalNat_r1i1p1_regridded.nc')
#     iris.coord_categorisation.add_year(cube, 'time', name='year')
#     iris.coord_categorisation.add_season(cube, 'time', name='season')
#     # iris.coord_categorisation.add_month(cube, 'time', name='month')
#     # cube = cube[np.where(cube.coord('year').points <= 1850)]
#     reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
#     # qplt.contourf(reg_cube[0],20)
#     # plt.gca().coastlines()
#     # plt.show()
#     # reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
#     # reg_cube = reg_cube[np.where(cube.coord('month').points == 'Mar')]
#     reg_cube = reg_cube.aggregated_by(['year'], iris.analysis.MEAN)
#     # qplt.contourf(reg_cube[0],20)
#     # plt.gca().coastlines()
#     # plt.show()
#     # reg_cube = detrend_cube(reg_cube)
#     loc1a = np.where(((reg_cube.coord('year').points >= 1992) & (reg_cube.coord('year').points <= 1998)))
#     loc1b = np.where(((reg_cube.coord('year').points >= 1999) & (reg_cube.coord('year').points <= 2006)))
#     loc1c = np.where(((reg_cube.coord('year').points >= 1998) & (reg_cube.coord('year').points <= 2000)))
#     loc1d = np.where(((reg_cube.coord('year').points >= 2001) & (reg_cube.coord('year').points <= 2005)))
#     loc3 = np.where(((reg_cube.coord('year').points >= 1905) & (reg_cube.coord('year').points <= 2005)))
#     locs = [loc1a,loc1b,loc1c,loc1d,loc3]
#     for j,loc in enumerate(locs):
#         tmp_data1 = reg_cube[loc].collapsed('time',iris.analysis.MEAN)
#         try:
#             fill_value = tmp_data1.data.fill_value
#         except:
#             fill_value = np.nan
#         data_tmp = tmp_data1.data
#         try:
#             data_tmp[np.where(data_tmp.data == fill_value)] = np.nan
#             data_tmp[np.where(data_tmp.data > 100.0)] = np.nan
#             data_tmp[np.where(data_tmp.data == 0.0)] = np.nan
#         except:
#             data_tmp[np.where(data_tmp == fill_value)] = np.nan
#             data_tmp[np.where(data_tmp > 100.0)] = np.nan
#             data_tmp[np.where(data_tmp == 0.0)] = np.nan
#         data_sic[j][i,:,:] = data_tmp
#
#


#####################
# tos
#####################

cube = iris.load_cube(directory+models[7]+'_tos_historicalNat_r1i1p1_regridded.nc')
reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
template_cube = reg_cube.collapsed('time',iris.analysis.MEAN)

reg_cube_shape = np.shape(reg_cube)
data1 = np.zeros([len(models),reg_cube_shape[1],reg_cube_shape[2]])
data1[:] = np.nan
data_tos = {}
number = 10
for i in range(number):
    data_tos[i] = data1.copy()


for i,model in enumerate(models):
    print model
    cube = iris.load_cube(directory+model+'_tos_historicalNat_r1i1p1_regridded.nc')
    iris.coord_categorisation.add_year(cube, 'time', name='year')
    iris.coord_categorisation.add_season(cube, 'time', name='season')
    # iris.coord_categorisation.add_month(cube, 'time', name='month')
    # cube = cube[np.where(cube.coord('year').points <= 1850)]
    reg_cube = extract_region(cube,lon_west,lon_east,lat_south,lat_north)
    # reg_cube = reg_cube[np.where(cube.coord('season').points == 'djf')]
    reg_cube = reg_cube.aggregated_by(['year'], iris.analysis.MEAN)
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    # reg_cube = reg_cube[np.where(cube.coord('season').points == 'mam')]
    # reg_cube = reg_cube[np.where(cube.coord('month').points == 'Mar')]
    # qplt.contourf(reg_cube[0],20)
    # plt.gca().coastlines()
    # plt.show()
    # reg_cube = detrend_cube(reg_cube)
    offset0=1
    offset1 =2
    offset2 = 3
    offset3 = 4
    offset4 = 5
    offset5 = 6
    offset6 =7
    offset7 = 8
    offset8 = 9
    offset9 = 10
    #https://en.wikipedia.org/wiki/List_of_Quaternary_volcanic_eruptions#Since_2000_AD
    loc1a = np.where(((reg_cube.coord('year').points == 1991+offset0) | (reg_cube.coord('year').points == 1980+offset0) |(reg_cube.coord('year').points == 1912+offset0) |(reg_cube.coord('year').points == 1902+offset0)  |(reg_cube.coord('year').points == 1886+offset0)  | (reg_cube.coord('year').points == 1883+offset0)))
    loc1b = np.where(((reg_cube.coord('year').points == 1991+offset1) | (reg_cube.coord('year').points == 1980+offset1) |(reg_cube.coord('year').points == 1912+offset1) |(reg_cube.coord('year').points == 1902+offset1)  |(reg_cube.coord('year').points == 1886+offset1)  | (reg_cube.coord('year').points == 1883+offset1)))
    loc1c = np.where(((reg_cube.coord('year').points == 1991+offset2) | (reg_cube.coord('year').points == 1980+offset2) |(reg_cube.coord('year').points == 1912+offset2) |(reg_cube.coord('year').points == 1902+offset2)  |(reg_cube.coord('year').points == 1886+offset2)  | (reg_cube.coord('year').points == 1883+offset2)))
    loc1d = np.where(((reg_cube.coord('year').points == 1991+offset3) | (reg_cube.coord('year').points == 1980+offset3) |(reg_cube.coord('year').points == 1912+offset3) |(reg_cube.coord('year').points == 1902+offset3)  |(reg_cube.coord('year').points == 1886+offset3)  | (reg_cube.coord('year').points == 1883+offset3)))
    loc1e = np.where(((reg_cube.coord('year').points == 1991+offset4) | (reg_cube.coord('year').points == 1980+offset4) |(reg_cube.coord('year').points == 1912+offset4) |(reg_cube.coord('year').points == 1902+offset4)  |(reg_cube.coord('year').points == 1886+offset4)  | (reg_cube.coord('year').points == 1883+offset4)))
    loc1f = np.where(((reg_cube.coord('year').points == 1991+offset5) | (reg_cube.coord('year').points == 1980+offset5) |(reg_cube.coord('year').points == 1912+offset5) |(reg_cube.coord('year').points == 1902+offset5)  |(reg_cube.coord('year').points == 1886+offset5)  | (reg_cube.coord('year').points == 1883+offset5)))
    loc1g = np.where(((reg_cube.coord('year').points == 1991+offset6) | (reg_cube.coord('year').points == 1980+offset6) |(reg_cube.coord('year').points == 1912+offset6) |(reg_cube.coord('year').points == 1902+offset6)  |(reg_cube.coord('year').points == 1886+offset6)  | (reg_cube.coord('year').points == 1883+offset6)))
    loc1h = np.where(((reg_cube.coord('year').points == 1991+offset7) | (reg_cube.coord('year').points == 1980+offset7) |(reg_cube.coord('year').points == 1912+offset7) |(reg_cube.coord('year').points == 1902+offset7)  |(reg_cube.coord('year').points == 1886+offset7)  | (reg_cube.coord('year').points == 1883+offset7)))
    loc1i = np.where(((reg_cube.coord('year').points == 1991+offset8) | (reg_cube.coord('year').points == 1980+offset8) |(reg_cube.coord('year').points == 1912+offset8) |(reg_cube.coord('year').points == 1902+offset8)  |(reg_cube.coord('year').points == 1886+offset8)  | (reg_cube.coord('year').points == 1883+offset8)))

    loc3 = np.where(((reg_cube.coord('year').points >= 1850) & (reg_cube.coord('year').points <= 2006)))
    locs = [loc1a,loc1b,loc1c,loc1d,loc1e,loc1f,loc1g,loc1h,loc1i,loc3]
    for j,loc in enumerate(locs):
        tmp_data1 = reg_cube[loc].collapsed('time',iris.analysis.MEAN)
        try:
            fill_value = tmp_data1.data.fill_value
        except:
            fill_value = np.nan
        data_tmp = tmp_data1.data
        try:
            data_tmp[np.where(data_tmp.data == fill_value)] = np.nan
            data_tmp[np.where(data_tmp.data > 300.0)] = np.nan
            data_tmp[np.where(data_tmp.data < -10.0)] = np.nan
        except:
            data_tmp[np.where(data_tmp == fill_value)] = np.nan
            data_tmp[np.where(data_tmp > 300.0)] = np.nan
            data_tmp[np.where(data_tmp < -10.0)] = np.nan
        data_tos[j][i,:,:] = data_tmp






##########
# plotting
##########
"""


for j,model in enumerate(models):
    print model

    plotting_cubes={}
    plotting_cubes[0] = template_cube.copy()
    plotting_cubes[1] = template_cube.copy()
    plotting_cubes[2] = template_cube.copy()
    plotting_cubes[3] = template_cube.copy()
    plotting_cubes[9] = template_cube.copy()


    plotting_cubes2={}
    plotting_cubes2[0] = template_cube.copy()
    plotting_cubes2[1] = template_cube.copy()
    plotting_cubes2[2] = template_cube.copy()
    plotting_cubes2[3] = template_cube.copy()
    plotting_cubes2[4] = template_cube.copy()


    for i in range(5):
        # tmp_data = np.nanmean(data_tos[i],axis=0)
        tmp_data = data_tos[i][j]
        tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
        plotting_cubes[i].data =tmp_data

    for i in range(5):
        # tmp_data = np.nanmean(data_sic[i],axis=0)
        tmp_data = data_sic[i][j]
        tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
        plotting_cubes2[i].data =tmp_data




    # for k,dummy in enumerate(models):
    #     for i in range(3):
    #         tmp_data = data[i][k]
    #         tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    #         plotting_cubes[i].data =tmp_data

    import iris
    import iris.plot as iplt
    import iris.quickplot as qplt
    import matplotlib.pyplot as plt
    import numpy as np
    import cartopy.crs as ccrs
    import iris.analysis.cartography
    import cartopy.feature as cfeature

    min_color_value = -0.5
    max_color_value = 0.5
    min_color_value2 = -3
    max_color_value2 = 15.0
    cmap = plt.get_cmap('YlGnBu')
    cmap2 = plt.get_cmap('seismic')

    my_extent = [-50, 50, 45, 90]
    no_levs=31

    plt.close('all')
    fig = plt.figure(figsize=(16,8))

    ax0 = plt.subplot(231,projection=ccrs.PlateCarree())
    # ax0 = plt.subplot(131,projection=ccrs.Mollweide())
    ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[9]-273.15,np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
    my_plot2 = iplt.contour(plotting_cubes2[4],np.linspace(0,100,no_levs/10.0),colors='k')
    ax0.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('full climatology')

    ax1 = plt.subplot(232,projection=ccrs.PlateCarree())
    ax1.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[0]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax1.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('lag1')

    ax2 = plt.subplot(233,projection=ccrs.PlateCarree())
    ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[1]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax2.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('lag2')

    ax2 = plt.subplot(234,projection=ccrs.PlateCarree())
    ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[2]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[2]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax2.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('lag3')

    ax2 = plt.subplot(235,projection=ccrs.PlateCarree())
    ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
    # my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
    # my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
    my_plot = iplt.contourf(plotting_cubes[3]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
    my_plot2 = iplt.contour(plotting_cubes2[3]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
    ax2.add_feature(cfeature.LAND)
    plt.gca().coastlines('10m')
    bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
    bar.set_label('tos')
    plt.title('lag4')
    plt.show()


"""

####
# multi model means
###

plotting_cubes={}
for i in range(np.size(locs)):
    plotting_cubes[i] = template_cube.copy()



# plotting_cubes2={}
# plotting_cubes2[0] = template_cube.copy()
# plotting_cubes2[1] = template_cube.copy()
# plotting_cubes2[2] = template_cube.copy()
# plotting_cubes2[3] = template_cube.copy()
# plotting_cubes2[4] = template_cube.copy()


for i in range(np.size(locs)):
    tmp_data = np.nanmean(data_tos[i],axis=0)
    # tmp_data = data_tos[i][j]
    tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
    plotting_cubes[i].data =tmp_data

# for i in range(5):
#     tmp_data = np.nanmean(data_sic[i],axis=0)
#     # tmp_data = data_sic[i][j]
#     tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
#     plotting_cubes2[i].data =tmp_data




# for k,dummy in enumerate(models):
#     for i in range(3):
#         tmp_data = data[i][k]
#         tmp_data[np.where(tmp_data == np.nan)] = template_cube.data.fill_value
#         plotting_cubes[i].data =tmp_data

import iris
import iris.plot as iplt
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import iris.analysis.cartography
import cartopy.feature as cfeature

min_color_value = -0.5
max_color_value = 0.5
min_color_value2 = -3
max_color_value2 = 15.0
cmap = plt.get_cmap('YlGnBu')
cmap2 = plt.get_cmap('seismic')

my_extent = [-120, 50, 0, 90]
no_levs=31

plt.close('all')
fig = plt.figure(figsize=(16,8))

ax0 = plt.subplot(431,projection=ccrs.PlateCarree())
# ax0 = plt.subplot(131,projection=ccrs.Mollweide())
ax0.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[9]-273.15,np.linspace(min_color_value2,max_color_value2,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes2[4],np.linspace(0,100,no_levs/10.0),colors='k')
ax0.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('full climatology')

ax1 = plt.subplot(432,projection=ccrs.PlateCarree())
ax1.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[0]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
# my_plot2 = iplt.contour(plotting_cubes2[0]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
ax1.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag1')

ax2 = plt.subplot(433,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
# my_plot2 = iplt.contour(plotting_cubes2[1]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag2')

ax2 = plt.subplot(434,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[2]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
# my_plot2 = iplt.contour(plotting_cubes2[2]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag3')

ax2 = plt.subplot(435,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
# my_plot = iplt.contourf(plotting_cubes[1]-plotting_cubes[3],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap)
# my_plot2 = iplt.contour(plotting_cubes[3],np.linspace(0,99,10),colors='k',linewidths=0.2)
my_plot = iplt.contourf(plotting_cubes[3]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
# my_plot2 = iplt.contour(plotting_cubes2[3]-plotting_cubes2[4],np.linspace(-10,10,no_levs/10.0),colors='k')
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag4')

ax2 = plt.subplot(436,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
my_plot = iplt.contourf(plotting_cubes[4]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag4')

ax2 = plt.subplot(437,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
my_plot = iplt.contourf(plotting_cubes[5]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag4')

ax2 = plt.subplot(438,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
my_plot = iplt.contourf(plotting_cubes[6]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag4')

ax2 = plt.subplot(439,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
my_plot = iplt.contourf(plotting_cubes[7]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag4')

ax2 = plt.subplot(4,3,10,projection=ccrs.PlateCarree())
ax2.set_extent(my_extent, crs=ccrs.PlateCarree())
my_plot = iplt.contourf(plotting_cubes[8]-plotting_cubes[9],np.linspace(min_color_value,max_color_value,no_levs),cmap=cmap2)
ax2.add_feature(cfeature.LAND)
plt.gca().coastlines('10m')
bar = plt.colorbar(my_plot, orientation='horizontal', extend='both')
bar.set_label('tos')
plt.title('lag4')

# plt.savefig('/home/ph290/Documents/figures/model'+models[0]+'.png')
plt.savefig('/home/ph290/Documents/figures/hist_nat_volc_tos_composites_with ice_lags.png')
plt.show(block = True)

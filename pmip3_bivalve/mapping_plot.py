import matplotlib.pyplot as plt
import iris
import cartopy.crs as ccrs
import cartopy.feature as cfeature

plt.close('all')
ax = plt.axes(projection=ccrs.PlateCarree())
ax.set_extent([-35, -10, 60, 75])

# Put a background image on for nice sea rendering.
cube1 = iris.load_cube('/data/data0/ph290/observations/ostia/ostia_clim.nc','sea_surface_temperature')
cube2 = iris.load_cube('/data/data0/ph290/observations/ostia/ostia_clim.nc','sea ice fraction')

cube1 = cube1.collapsed('time',iris.analysis.MEAN)
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

cube2 = cube2.collapsed('time',iris.analysis.MEAN)
lats2 = cube2.coord('latitude').points
lons2 = cube2.coord('longitude').points
data2 = cube2.data

contour_result2 = ax.contour(lons2, lats2, data2,10,
			transform=ccrs.PlateCarree(),
			cmap='spectral')

plt.colorbar(contour_result2)


contour_result1 = ax.contourf(lons1, lats1, data1,80,
			transform=ccrs.PlateCarree())

plt.colorbar(contour_result1)
plt.clabel(contour_result1, inline=1, fontsize=10)

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
river_50m = cfeature.NaturalEarthFeature('physical', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
glacier_50m = cfeature.NaturalEarthFeature('physical', 'glaciated_areas', '10m',facecolor='none')
river_euro_50m = cfeature.NaturalEarthFeature('physical', 'rivers_europe', '10m',facecolor='none')

ax.add_feature(land_50m,facecolor='white')
ax.add_feature(coast_50m)
ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.plot(-18.195667,66.5215, 'ro', transform=ccrs.PlateCarree())
#66 31.59 N, 18 11.74 W
plt.savefig('/home/ph290/Documents/figures/n_iceland.png')
plt.show(block = False)


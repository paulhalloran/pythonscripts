
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models



def calculate_correlations(models,var_name,my_dir,dir_sic,lags):
	lags = [0]
	lon_west = -45.0
	lon_east = 25.0
	lat_south = 0.0
	lat_north = 90.0

	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	correlations = [var]
	for i,model in enumerate(models):
		try:
			print i
			c1 = iris.load_cube(my_dir + model+'_'+var_name+'_piControl_*.nc')
			c2 = iris.load_cube(dir_sic + model+'_sic_piControl_*.nc')
			try:
				iris.coord_categorisation.add_year(c1, 'time', name='year')
				c1 = c1.aggregated_by('year', iris.analysis.MEAN)
			except:
				print ' '
			try:
				iris.coord_categorisation.add_year(c2, 'time', name='year')
				c2 = c2.aggregated_by('year', iris.analysis.MEAN)
			except:
				print ' '
			if np.shape(c1)[0] == np.shape(c2)[0]:
				# qplt.contourf(c1[0],31)
				# plt.gca().coastlines()
				# plt.show()
				try:
					missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
				except:
					missing_data = np.where(c1[0].data.data == 0.0)
				cube_data = c1.data
				#
				# N=5.0
				# #I think N is the order of the filter - i.e. quadratic
				# timestep_between_values=1.0 #years valkue should be '1.0/12.0'
				# low_cutoff=5.0 #years: 1 for filtering at 1 yr. 5 for filtering at 5 years
				# high_cutoff=100.0 #years: 1 for filtering at 1 yr. 5 for filtering at 5 years
				# # middle_cuttoff_low=1.0
				# # middle_cuttoff_high=5.0
				# Wn_low=timestep_between_values/low_cutoff
				# Wn_high=timestep_between_values/high_cutoff
				# # Wn_mid_low=timestep_between_values/middle_cuttoff_low
				# # Wn_mid_high=timestep_between_values/middle_cuttoff_high
				# b, a = scipy.signal.butter(N, Wn_low, btype='low')
				# # b1, a1 = scipy.signal.butter(N, Wn_mid_low, btype='low')
				# b2, a2 = scipy.signal.butter(N, Wn_high, btype='high')
				# cube_data = scipy.signal.filtfilt(b, a, cube_data, axis=0)
				# cube_data = scipy.signal.filtfilt(b2, a2, cube_data, axis=0)
				#
				cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
				c1.data = cube_data_detrended
				#
				cube_region_tmp = c2.intersection(longitude=(lon_west, lon_east))
				c2 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
				try:
					c2.coord('longitude').guess_bounds()
					c2.coord('latitude').guess_bounds()
				except:
					print 'cube already has bounds'
				grid_areas = iris.analysis.cartography.area_weights(c2)
				sic_ts = c2.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
				# for j,lag in enumerate(lags):
				# 	print 'j: ',j
				# 	ts = np.roll(sic_ts,int(lag))
				# 	smoothing = 20
				# 	ts = rm.running_mean(ts,smoothing)
				# 	ts = ts[smoothing/2:-smoothing/2]
				# 	cube = c1[smoothing/2:-smoothing/2].copy()
				# 	cube = iris.analysis.maths.multiply(cube, 0.0)
				# 	ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				# 	cube = iris.analysis.maths.add(cube, ts2)
				# 	out_cube = iris.analysis.stats.pearsonr(c1[smoothing/2:-smoothing/2], cube, corr_coords=['time'])
				# 	correlations[j][i] = out_cube.data
				ts = sic_ts
				cube = c1.copy()
				cube = iris.analysis.maths.multiply(cube, 0.0)
				ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				cube = iris.analysis.maths.add(cube, ts2)
				out_cube = iris.analysis.stats.pearsonr(c1, cube, corr_coords=['time'])
				correlations[0][i] = out_cube.data
		except:
			print 'failed'
	return correlations


def calculate_correlations_amo(models,var_name,my_dir,dir_tos,lags):
	lags = [0]
	lon_west = -75
	lon_east = -7.5
	lat_south = 0.0
	lat_north = 60.0

	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	correlations = [var]
	for i,model in enumerate(models):
		try:
			print i
			c1 = iris.load_cube(my_dir + model+'_'+var_name+'_piControl_*.nc')
			c2 = iris.load_cube(dir_tos + model+'_tos_piControl_*.nc')
			try:
				iris.coord_categorisation.add_year(c1, 'time', name='year')
				c1 = c1.aggregated_by('year', iris.analysis.MEAN)
			except:
				print ' '
			try:
				iris.coord_categorisation.add_year(c2, 'time', name='year')
				c2 = c2.aggregated_by('year', iris.analysis.MEAN)
			except:
				print ' '
			if np.shape(c1)[0] == np.shape(c2)[0]:
				# qplt.contourf(c1[0],31)
				# plt.gca().coastlines()
				# plt.show()
				try:
					missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
				except:
					missing_data = np.where(c1[0].data.data == 0.0)
				cube_data = c1.data
				#
				# N=5.0
				# #I think N is the order of the filter - i.e. quadratic
				# timestep_between_values=1.0 #years valkue should be '1.0/12.0'
				# low_cutoff=5.0 #years: 1 for filtering at 1 yr. 5 for filtering at 5 years
				# high_cutoff=100.0 #years: 1 for filtering at 1 yr. 5 for filtering at 5 years
				# # middle_cuttoff_low=1.0
				# # middle_cuttoff_high=5.0
				# Wn_low=timestep_between_values/low_cutoff
				# Wn_high=timestep_between_values/high_cutoff
				# # Wn_mid_low=timestep_between_values/middle_cuttoff_low
				# # Wn_mid_high=timestep_between_values/middle_cuttoff_high
				# b, a = scipy.signal.butter(N, Wn_low, btype='low')
				# # b1, a1 = scipy.signal.butter(N, Wn_mid_low, btype='low')
				# b2, a2 = scipy.signal.butter(N, Wn_high, btype='high')
				# cube_data = scipy.signal.filtfilt(b, a, cube_data, axis=0)
				# cube_data = scipy.signal.filtfilt(b2, a2, cube_data, axis=0)
				#
				cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
				c1.data = cube_data_detrended
				#
				cube_region_tmp = c2.intersection(longitude=(lon_west, lon_east))
				c2 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
				try:
					c2.coord('longitude').guess_bounds()
					c2.coord('latitude').guess_bounds()
				except:
					print 'cube already has bounds'
				grid_areas = iris.analysis.cartography.area_weights(c2)
				sic_ts = c2.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
				# for j,lag in enumerate(lags):
				# 	print 'j: ',j
				# 	ts = np.roll(sic_ts,int(lag))
				# 	smoothing = 20
				# 	ts = rm.running_mean(ts,smoothing)
				# 	ts = ts[smoothing/2:-smoothing/2]
				# 	cube = c1[smoothing/2:-smoothing/2].copy()
				# 	cube = iris.analysis.maths.multiply(cube, 0.0)
				# 	ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				# 	cube = iris.analysis.maths.add(cube, ts2)
				# 	out_cube = iris.analysis.stats.pearsonr(c1[smoothing/2:-smoothing/2], cube, corr_coords=['time'])
				# 	correlations[j][i] = out_cube.data
				ts = sic_ts
				cube = c1.copy()
				cube = iris.analysis.maths.multiply(cube, 0.0)
				ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				cube = iris.analysis.maths.add(cube, ts2)
				out_cube = iris.analysis.stats.pearsonr(c1, cube, corr_coords=['time'])
				correlations[0][i] = out_cube.data
		except:
			print 'failed'
	return correlations

#https://en.wikipedia.org/wiki/List_of_famines
famine_data = {}
famine_data['England1005'] = {}
famine_data['England1005']['year'] = 1005
famine_data['England1005']['latlon'] = '52.3555N, 1.1743W'
famine_data['Europe1016'] = {}
famine_data['Europe1016']['year'] = 1016
famine_data['Europe1016']['latlon'] = '54.5260N, 15.2551E'
famine_data['Egypt1069'] = {}
famine_data['Egypt1069']['year'] = 1069
famine_data['Egypt1069']['latlon'] = '26.8206N, 30.8025E'
famine_data['France1097'] = {}
famine_data['France1097']['year'] = 1097
famine_data['France1097']['latlon'] = '48.8566N, 2.3522E'
famine_data['Japan1181'] = {}
famine_data['Japan1181']['year'] = 1181
famine_data['Japan1181']['latlon'] = '36.2048N, 138.2529E'
famine_data['Novgorod1230'] = {}
famine_data['Novgorod1230']['year'] = 1230
famine_data['Novgorod1230']['latlon'] = '58.5256N, 31.2742E'
famine_data['Japan1230'] = {}
famine_data['Japan1230']['year'] = 1230
famine_data['Japan1230']['latlon'] = '36.2048N, 138.2529E'
famine_data['England1235'] = {}
famine_data['England1235']['year'] = 1235
famine_data['England1235']['latlon'] = '52.3555N, 1.1743W'
famine_data['Portugal1255'] = {}
famine_data['Portugal1255']['year'] = 1255
famine_data['Portugal1255']['latlon'] = '39.3999N, 8.2245W'
famine_data['Anasazi1277'] = {}
famine_data['Anasazi1277']['year'] = 1277
famine_data['Anasazi1277']['latlon'] = '39.3210N, 111.0937W'
famine_data['Europe1316'] = {}
famine_data['Europe1316']['year'] = 1316
famine_data['Europe1316']['latlon'] = '54.5260N, 15.2551E'
famine_data['China1336'] = {}
famine_data['China1336']['year'] = 1336
famine_data['China1336']['latlon'] = '39.9042N, 116.4074E'
famine_data['India1344'] = {}
famine_data['India1344']['year'] =1344
famine_data['India1344']['latlon'] = '20.5937N, 78.9629E'
famine_data['Anatolia1387'] = {}
famine_data['Anatolia1387']['year'] =1387
famine_data['Anatolia1387']['latlon'] = '39.0N, 35.0E'
famine_data['India1400'] = {}
famine_data['India1400']['year'] =1400
famine_data['India1400']['latlon'] = '20.5937N, 78.9629E'
famine_data['Mexico1441'] = {}
famine_data['Mexico1441']['year'] =1441
famine_data['Mexico1441']['latlon'] = '23.6345N, 102.5528W'
famine_data['Aztec1452'] = {}
famine_data['Aztec1452']['year'] =1452
famine_data['Aztec1452']['latlon'] = '23.6345N, 102.5528W'
famine_data['Japan1460'] = {}
famine_data['Japan1460']['year'] =1460
famine_data['Japan1460']['latlon'] = '36.2048N, 138.2529E'
famine_data['Spain1504'] = {}
famine_data['Spain1504']['year'] =1504
famine_data['Spain1504']['latlon'] = '40.4637N, 3.7492W'
famine_data['Venice1518'] = {}
famine_data['Venice1518']['year'] =1518
famine_data['Venice1518']['latlon'] = '45.4408N, 12.3155E'
famine_data['Languedoc1528'] = {}
famine_data['Languedoc1528']['year'] =1528
famine_data['Languedoc1528']['latlon'] = '43.5912N, 3.2584E'
famine_data['Ethiopia1535'] = {}
famine_data['Ethiopia1535']['year'] =1535
famine_data['Ethiopia1535']['latlon'] = '9.1450N, 40.4897E'
famine_data['Japan1540'] = {}
famine_data['Japan1540']['year'] =1540
famine_data['Japan1540']['latlon'] = '36.2048N, 138.2529E'
famine_data['Harar1568'] = {}
famine_data['Harar1568']['year'] =1568
famine_data['Harar1568']['latlon'] = '9.3126N, 42.1227E'
famine_data['England1586'] = {}
famine_data['England1586']['year'] =1586
famine_data['England1586']['latlon'] = '52.3555N, 1.1743W'
famine_data['Russian1602'] = {}
famine_data['Russian1602']['year'] =1602
famine_data['Russian1602']['latlon'] = '55.7558N, 37.6173E'
famine_data['Europe1630'] = {}
famine_data['Europe1630']['year'] = 1630
famine_data['Europe1630']['latlon'] = '55.7558N, 37.6173E'
famine_data['Japan1619'] = {}
famine_data['Japan1619']['year'] =1619
famine_data['Japan1619']['latlon'] = '36.2048N, 138.2529E'
famine_data['Deccan1630'] = {}
famine_data['Deccan1630']['year'] =1630
famine_data['Deccan1630']['latlon'] = '18.477091N, 73.890686E'
famine_data['Japan1641'] = {}
famine_data['Japan1641']['year'] =1641
famine_data['Japan1641']['latlon'] = '36.2048N, 138.2529E'
famine_data['Poland1654'] = {}
famine_data['Poland1654']['year'] =1654
famine_data['Poland1654']['latlon'] = '51.9194N, 19.1451E'
famine_data['northernEngland'] = {}
famine_data['northernEngland']['year'] =1649
famine_data['northernEngland']['latlon'] = '53.4808N, 2.2426W'
famine_data['France1651'] = {}
famine_data['France1651']['year'] =1651
famine_data['France1651']['latlon'] = '46.6N, 4.8E'
famine_data['Ireland1652'] = {}
famine_data['Ireland1652']['year'] =1652
famine_data['Ireland1652']['latlon'] = '53.1424N, 7.6921W'
famine_data['India1661'] = {}
famine_data['India1661']['year'] =1661
famine_data['India1661']['latlon'] = '20.5937N, 78.9629E'
famine_data['Spain1680'] = {}
famine_data['Spain1680']['year'] =1680
famine_data['Spain1680']['latlon'] = '40.4637N, 3.7492W'
famine_data['Sardinia1680'] = {}
famine_data['Sardinia1680']['year'] =1680
famine_data['Sardinia1680']['latlon'] = '40.1209N, 9.0129E'
famine_data['Sahel1685'] = {}
famine_data['Sahel1685']['year'] =1685
famine_data['Sahel1685']['latlon'] = '19.0643N, 13.5437E'
famine_data['Scotland1695'] = {}
famine_data['Scotland1695']['year'] =1695
famine_data['Scotland1695']['latlon'] = '56.4907N, 4.2026W'
famine_data['French1693'] = {}
famine_data['French1693']['year'] =1693
famine_data['French1693']['latlon'] = '46.2276N, 2.2137E'
famine_data['Estonia1696'] = {}
famine_data['Estonia1696']['year'] =1696
famine_data['Estonia1696']['latlon'] = '58.5953N, 25.0136E'
famine_data['Finland1696'] = {}
famine_data['Finland1696']['year'] =1696
famine_data['Finland1696']['latlon'] = '61.9241N, 25.7482E'
famine_data['Deccan1703'] = {}
famine_data['Deccan1703']['year'] =1703
famine_data['Deccan1703']['latlon'] = '18.477091N, 73.890686E'
famine_data['EastPrussia1709'] = {}
famine_data['EastPrussia1709']['year'] =1709
famine_data['EastPrussia1709']['latlon'] = '54.7104N, 20.4522E'
famine_data['France1709'] = {}
famine_data['France1709']['year'] =1709
famine_data['France1709']['latlon'] = '48.8566N, 2.3522E'
famine_data['Arabia1722'] = {}
famine_data['Arabia1722']['year'] =1722
famine_data['Arabia1722']['latlon'] = '23.8859N, 45.0792E'
famine_data['EnglishMidlands1727'] = {}
famine_data['EnglishMidlands1727']['year'] =1727
famine_data['EnglishMidlands1727']['latlon'] = '52.3449N, 1.9718W'
famine_data['Japan1732'] = {}
famine_data['Japan1732']['year'] =1732
famine_data['Japan1732']['latlon'] = '36.2048N, 138.2529E'
famine_data['Timbuktu1746'] = {}
famine_data['Timbuktu1746']['year'] =1746
famine_data['Timbuktu1746']['latlon'] = '16.7666N, 3.0026W'
famine_data['Ireland1740'] = {}
famine_data['Ireland1740']['year'] =1740
famine_data['Ireland1740']['latlon'] = '53.1424N, 7.6921W'
famine_data['Senegal1753'] = {}
famine_data['Senegal1753']['year'] =1753
famine_data['Senegal1753']['latlon'] = '14.4974N, 14.4524W'
famine_data['Naples1764'] = {}
famine_data['Naples1764']['year'] =1764
famine_data['Naples1764']['latlon'] = '40.8518N, 14.2681E'
famine_data['Bengal1771'] = {}
famine_data['Bengal1771']['year'] =1771
famine_data['Bengal1771']['latlon'] = '22.9868N, 87.8550E'
famine_data['CzechRepublic1770'] = {}
famine_data['CzechRepublic1770']['year'] =1770
famine_data['CzechRepublic1770']['latlon'] = '49.8175N, 15.4730E'
famine_data['Saxony1771'] = {}
famine_data['Saxony1771']['year'] =1771
famine_data['Saxony1771']['latlon'] = '51.3397N, 12.3731E'
famine_data['Sweden1773'] = {}
famine_data['Sweden1773']['year'] =1773
famine_data['Sweden1773']['latlon'] = '60.1282N, 18.6435E'
famine_data['Rabat1779'] = {}
famine_data['Rabat1779']['year'] =1779
famine_data['Rabat1779']['latlon'] = '33.9716N, 6.8498W'
famine_data['Japan1785'] = {}
famine_data['Japan1785']['year'] =1785
famine_data['Japan1785']['latlon'] = '36.2048N, 138.2529E'
# famine_data['Iceland1783'] = {}
# famine_data['Iceland1783']['year'] =1783
# famine_data['Iceland1783']['latlon'] = '64.9631N, 19.0208W'
# remived - because not climatic - see https://en.wikipedia.org/wiki/Laki#Consequences_in_Iceland
famine_data['India1783'] = {}
famine_data['India1783']['year'] =1783
famine_data['India1783']['latlon'] = '20.5937N, 78.9629E'
famine_data['Egypt1784'] = {}
famine_data['Egypt1784']['year'] =1784
famine_data['Egypt1784']['latlon'] = '26.8206N, 30.8025E'
famine_data['Tunisia1784'] = {}
famine_data['Tunisia1784']['year'] =1784
famine_data['Tunisia1784']['latlon'] = '33.8869N, 9.5375E'
famine_data['France1788'] = {}
famine_data['France1788']['year'] =1788
famine_data['France1788']['latlon'] = '48.8566N, 2.3522E'
famine_data['Ethiopia1789'] = {}
famine_data['Ethiopia1789']['year'] =1789
famine_data['Ethiopia1789']['latlon'] = '9.1450N, 40.4897E'
famine_data['India1790'] = {}
famine_data['India1790']['year'] =1790
famine_data['India1790']['latlon'] = '20.5937N, 78.9629E'
famine_data['Madrid1811'] = {}
famine_data['Madrid1811']['year'] =1811
famine_data['Madrid1811']['latlon'] = '40.4168N, 3.7038W'
famine_data['Indonesia1815'] = {}
famine_data['Indonesia1815']['year'] =1815
famine_data['Indonesia1815']['latlon'] = '0.7893S, 113.9213E'
famine_data['Europe1816'] = {}
famine_data['Europe1816']['year'] =1816
famine_data['Europe1816']['latlon'] = '54.5260N, 15.2551E'
famine_data['CapeVerde1831'] = {}
famine_data['CapeVerde1831']['year'] =1831
famine_data['CapeVerde1831']['latlon'] = '16.5388N, 23.0418W'
famine_data['Japan1835'] = {}
famine_data['Japan1835']['year'] =1835
famine_data['Japan1835']['latlon'] = '36.2048N, 138.2529E'
famine_data['Agra1837'] = {}
famine_data['Agra1837']['year'] =1837
famine_data['Agra1837']['latlon'] = '27.1767N, 78.0081E'
famine_data['Ireland1847'] = {}
famine_data['Ireland1847']['year'] =1847
famine_data['Ireland1847']['latlon'] = '53.1424N, 7.6921W'



# dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_psl = '/data/NAS-ph290/ph290/cmip5/piControl/'
dir_tas = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_tos = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_sic = dir_tas
dir_pr = '/data/NAS-ph290/ph290/cmip5/piControl/'
# dir_pr = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'

m0 = model_names(dir_pr,'pr')
m1 = model_names(dir_psl,'psl')
m2 = model_names(dir_tas,'tos')
m3 = model_names(dir_sic,'sic')


models = np.intersect1d(m0,m1)
# models = np.intersect1d(models,m2)
models = np.intersect1d(models,m3)

models = list(models)
# models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
# models.remove('CESM1-CAM5')
# models.remove('CESM1-FASTCHEM')
# models.remove('CESM1-WACCM')
# # models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
# models.remove('inmcm4')
# models.remove('MPI-ESM-LR')
# models.remove('MPI-ESM-P')
# models.remove('NorESM1-ME')


# models = ['ACCESS1-0', 'CCSM4', 'CESM1-CAM5', 'CNRM-CM5-2',  'GFDL-CM3',  'IPSL-CM5A-MR', 'MIROC5',  'MPI-ESM-MR',  'MRI-CGCM3', 'NorESM1-M']

print models

# with open('/home/ph290/Documents/python_scripts/pickles/sic_psl.pickle', 'r') as f:
#     [correlations_pr,correlations_psl] = pickle.load(f)


with open('/home/ph290/Documents/python_scripts/pickles/sic_psl_B.pickle', 'r') as f:
    [correlations_pr,correlations_amo] = pickle.load(f)


"""

lags=[0]
var_name = 'psl'
# correlations_psl = calculate_correlations(models,var_name,dir_psl,dir_sic,lags)
# var_name = 'tas'
# correlations_tas = calculate_correlations(models,var_name,dir_tas,dir_sic,lags)
var_name = 'pr'
correlations_pr = calculate_correlations(models,var_name,dir_pr,dir_sic,lags)
correlations_amo = calculate_correlations_amo(models,var_name,dir_pr,dir_tos,lags)


# with open('/home/ph290/Documents/python_scripts/pickles/sic_psl.pickle', 'w') as f:
#     pickle.dump([correlations_pr,correlations_psl], f)
with open('/home/ph290/Documents/python_scripts/pickles/sic_psl_B.pickle', 'w') as f:
    pickle.dump([correlations_pr,correlations_amo], f)

"""

def plot_prep(i,correlations,c1):
	var_mean = np.nanmean(correlations[i],axis=0)
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube


#################################




import pandas as pd
df = pd.read_csv('/data/NAS-geo01/ph290/observations/iceland_timeseries.csv')
sea_ice_years = df['seaice_year'].values
sea_ice_extent = df['seaice'].values


high_ice_years = sea_ice_years[np.where(sea_ice_extent > 0.1)]
low_ice_years = sea_ice_years[np.where(sea_ice_extent < -0.1)]

def plot_famines(famine_data):
	for key in famine_data:
		lat = famine_data[key]['latlon'].split(', ')[0]
		if lat[-1] == 'N':
			lat = np.float(lat[0:-1])
		else:
			lat = np.float(lat[0:-1]) * -1.0
		lon = famine_data[key]['latlon'].split(', ')[1]
		if lon[-1] == 'E':
			lon = np.float(lon[0:-1])
		else:
			lon = np.float(lon[0:-1]) * -1.0
		if famine_data[key]['year'] in high_ice_years:
			#blue
			plt.plot(lon,lat, color='#6464FA',markeredgecolor='#FFFFFF',marker='o',markeredgewidth=1,markersize = 15,alpha=0.75, transform=ccrs.PlateCarree())
		if famine_data[key]['year'] in low_ice_years:
			#red
			plt.plot(lon,lat, color='#FA6464',markeredgecolor='#FFFFFF',marker='o',markeredgewidth=1,markersize = 15,alpha=0.75, transform=ccrs.PlateCarree())


###########

'''

plt.close('all')
plt.figure(figsize=(8, 10))

my_projection = ccrs.Orthographic(central_longitude=20, central_latitude=50)

my_projection = ccrs.PlateCarree()
my_extent = [-30, 40, 10, 70]
# my_extent = [-50, 40, 10, 90]

# my_extent = [-180, 60, 10, 70]

######### Subplot 1 #########


var_name = 'pr'
c1 = iris.load_cube(dir_pr + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_pr,c1)

ax = plt.subplot2grid((2, 1), (0, 0),projection= my_projection)
ax.set_extent(my_extent, crs=ccrs.PlateCarree())


cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.075,0.075,31),extend='both',
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.7)
plt.title('Sea-ice v. precipitation correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 10, transform=ccrs.PlateCarree())
plot_famines(famine_data)
# plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())
# ax.gridlines()


######### Subplot 2 #########


var_name = 'psl'
c1 = iris.load_cube(dir_psl + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_psl,c1)

ax = plt.subplot2grid((2, 1), (1, 0),projection= my_projection)
ax.set_extent(my_extent, crs=ccrs.PlateCarree())


cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.2,0.2,31),extend='both',
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.7)
plt.title('Sea-ice v sea-level pressure correlation\n(control run)')
plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())
# plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())
# plot_famines(famine_data)
# ax.gridlines()

#
# var_name = 'tas'
# c1 = iris.load_cube(dir_tas + models[0]+'_'+var_name+'_piControl_*.nc')
# mean_var_cube = plot_prep(0,correlations_tas,c1)
#
# my_projection = ccrs.Orthographic(central_longitude=0, central_latitude=30)
# ax = plt.subplot2grid((2, 2), (1, 0),projection= my_projection)
# # ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())
#
# cube1 = mean_var_cube
# lats1 = cube1.coord('latitude').points
# lons1 = cube1.coord('longitude').points
# data1 = cube1.data
#
#
# contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.7,0.7,31),
# 			transform=ccrs.PlateCarree(),cmap='jet')
#
# cb = plt.colorbar(contour_result1, orientation='horizontal')
# tick_locator = ticker.MaxNLocator(nbins=5)
# cb.locator = tick_locator
# cb.update_ticks()
# cb.set_label('R')
# coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
# plt.title('Sea-ice tas correlation\n(control run)')
#
# plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())
# plot_famines(famine_data)
# # plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())
# ax.gridlines()
#



plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/sic_psl_pi_cont_composites_alternative7.png')
plt.show(block = False)


'''

######### Drought rather than famine ##########


file = '/data/NAS-ph290/ph290/observations/owda-orig.nc'

cube1 = iris.load_cube(file)

year = 0




# high_ice_years = sea_ice_years[np.where(sea_ice_extent > 0.1)]
# low_ice_years = sea_ice_years[np.where(sea_ice_extent < -0.1)]

drought_years = cube1.coord('time').points +1.0
loc = np.where((drought_years >= np.min(sea_ice_years)) & (drought_years <= np.max(sea_ice_years)))[0]

cube1 = cube1[:,:,loc]
tmp = cube1.data
tmp[np.where(np.logical_not(np.isfinite(tmp)))] = -999.9
for i in range(np.shape(tmp)[0]):
	for j in range(np.shape(tmp)[1]):
		if np.min(tmp[i,j,:]) == -999.9:
			tmp[i,j,:] = 0.0


N=5.0
#I think N is the order of the filter - i.e. quadratic
timestep_between_values=1.0 #years valkue should be '1.0/12.0'
low_cutoff=5.0 #years: 1 for filtering at 1 yr. 5 for filtering at 5 years
high_cutoff=100.0 #years: 1 for filtering at 1 yr. 5 for filtering at 5 years
# middle_cuttoff_low=1.0
# middle_cuttoff_high=5.0
Wn_low=timestep_between_values/low_cutoff
Wn_high=timestep_between_values/high_cutoff
# Wn_mid_low=timestep_between_values/middle_cuttoff_low
# Wn_mid_high=timestep_between_values/middle_cuttoff_high
b, a = scipy.signal.butter(N, Wn_low, btype='low')
# b1, a1 = scipy.signal.butter(N, Wn_mid_low, btype='low')
b2, a2 = scipy.signal.butter(N, Wn_high, btype='high')

# tmp = scipy.signal.filtfilt(b, a, tmp, axis=2)
# tmp = scipy.signal.filtfilt(b2, a2, tmp, axis=2)

tmp = scipy.signal.detrend(tmp, axis=2)
tmp[np.where(tmp == 0.0)] = np.nan
cube1.data = tmp
drought_years = drought_years[loc]


cutoff=0.20

loc1 = np.where(sea_ice_extent > cutoff)[0]
loc2 = np.where(sea_ice_extent < -1.0*cutoff)[0]

cube_diff = cube1[:,:,loc1].collapsed('time',iris.analysis.MEAN) - cube1[:,:,loc2].collapsed('time',iris.analysis.MEAN)

lats2 = np.flipud(cube1.coord('lat').points)
lons2 = cube1.coord('lon').points
data2 = np.rot90(cube_diff.data)


###################
#  Plot           #
###################

plt.close('all')
plt.figure(figsize=(8, 13))

my_projection = ccrs.Orthographic(central_longitude=20, central_latitude=50)

my_projection = ccrs.PlateCarree()
my_extent = [-11.5, 40, 27, 70]

### precip correlations ###


var_name = 'pr'
c1 = iris.load_cube(dir_pr + models[0]+'_'+var_name+'_piControl_*.nc')

correlations_pr_agreement = correlations_pr[0][0].copy()
correlations_pr_agreement[:] = 0.0

agreement = 1.5

for i in range(np.shape(correlations_pr_agreement)[0]):
	for j in range(np.shape(correlations_pr_agreement)[1]):
		tmp1 = correlations_pr[0][:,i,j]
		a = np.float(len(np.where(tmp1 > 0.0)[0]))
		b = np.float(len(np.where(tmp1 < 0.0)[0]))
		if b > 0.0:
			ratio = (a / b)
			if ((ratio > agreement) | (ratio < 1.0/agreement)):
				correlations_pr_agreement[i,j] = np.NAN


mean_var_cube = plot_prep(0,correlations_pr,c1)


ax = plt.subplot2grid((3, 1), (0, 0),projection= my_projection)
ax.set_extent(my_extent, crs=ccrs.PlateCarree())


cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.pcolormesh(lons1, lats1, data1,vmin=-0.1,vmax=0.1,
			transform=ccrs.PlateCarree(),cmap='bwr')
contour_result2 = ax.pcolormesh(lons1, lats1, correlations_pr_agreement,
			transform=ccrs.PlateCarree(),cmap='gray',vmin=0,vmax=1,alpha=0.3)

data3 = data2.copy()
data3[np.where(np.isfinite(data3))]=-999.99
data3[np.where(np.logical_not(np.isfinite(data3)))]=0.0
data3[np.where(data3 == -999.99)]=np.NAN
contour_result3 = ax.pcolormesh(lons2, lats2, data3,
			transform=ccrs.PlateCarree(),cmap='bwr',vmin=-1,vmax=1,)

cb = plt.colorbar(contour_result1, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.7)
plt.title('Sea-ice v. precipitation correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 10, transform=ccrs.PlateCarree())
# plot_famines(famine_data)

### end precip correlations ###



ax1 = plt.subplot2grid((3, 1), (1, 0),projection= my_projection)
ax1.set_extent(my_extent, crs=ccrs.PlateCarree())

# contour_result1 = ax1.contourf(lons1, lats1, data1,np.linspace(-0.075,0.075,10),extend='both',
# 			transform=ccrs.PlateCarree(),cmap='bwr')
contour_result2 = ax1.pcolormesh(lons2, lats2, data2,
			transform=ccrs.PlateCarree(),cmap='bwr')
# contour_result3 = ax1.pcolormesh(lons1, lats1, correlations_pr_agreement,
# 			transform=ccrs.PlateCarree(),cmap='gray',vmin=0,vmax=1,alpha=0.3)

cb2 = plt.colorbar(contour_result2, orientation='horizontal')

ax1.add_feature(coast_50m,edgecolor='#000000',alpha = 0.7)
# plot_famines(famine_data)


###########


mean_var_cube = plot_prep(0,correlations_amo,c1)


ax = plt.subplot2grid((3, 1), (2, 0),projection= my_projection)
ax.set_extent(my_extent, crs=ccrs.PlateCarree())


cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.pcolormesh(lons1, lats1, data1,vmin=-0.2,vmax=0.2,
			transform=ccrs.PlateCarree(),cmap='bwr')

data3 = data2.copy()
data3[np.where(np.isfinite(data3))]=-999.99
data3[np.where(np.logical_not(np.isfinite(data3)))]=0.0
data3[np.where(data3 == -999.99)]=np.NAN
contour_result3 = ax.pcolormesh(lons2, lats2, data3,
			transform=ccrs.PlateCarree(),cmap='bwr',vmin=-1,vmax=1,)

cb = plt.colorbar(contour_result1, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.7)
plt.title('AMO box SST v. precipitation correlation\n(control run)')

###########

plt.title('Drought index from last millennium\nhigh seaice years minus low seaice years')


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/sic_psl_pi_cont_composites_alternative10.png')
plt.show(block=False)

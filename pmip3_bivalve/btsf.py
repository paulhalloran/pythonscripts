
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import matplotlib.pyplot as plt
import iris
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models
	


directory = '/data/NAS-ph290/ph290/cmip5/last1000/summer/'
#directory = '/data/NAS-ph290/ph290/cmip5/last1000/winter/'
output_directory = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'


models = model_names(directory)

for model in models:

	cube=iris.load_cube(directory+model+'_msftbarot_past1000_r1i1p1_regridded.nc')

	# Baratoropic stream function indice from Winter amplication of the European Little Ice Age cooling by the subpolar gyre Eduardo Moreno-Chamarroet al. Nat. Sci. reports 2017

	lon_west = -60
	lon_east = -10
	lat_south = 50
	lat_north = 65 

	cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
	cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

	ts = cube_region.collapsed(['latitude','longitude'],iris.analysis.MEAN) * -1.0

	iris.fileformats.netcdf.save(ts, output_directory+'SpgBtsf_mean_'+model+'_past1000_r1i1p1_JJA.nc')
	
	
	
	
	

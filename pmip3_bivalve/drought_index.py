import iris
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np

file = '/data/NAS-ph290/ph290/observations/owda-orig.nc'

cube1 = iris.load_cube(file)

year = 0

lats2 = np.flipud(cube1.coord('lat').points)
lons2 = cube1.coord('lon').points
data2 = np.rot90(cube1[:,:,year].data)


plt.close('all')
plt.figure(figsize=(8, 10))

my_projection = ccrs.Orthographic(central_longitude=20, central_latitude=50)

my_projection = ccrs.PlateCarree()
my_extent = [-30, 40, 10, 70]

ax = plt.subplot2grid((2, 1), (0, 0),projection= my_projection)
ax.set_extent(my_extent, crs=ccrs.PlateCarree())


contour_result1 = ax.pcolormesh(lons2, lats2, data2,
			transform=ccrs.PlateCarree(),cmap='jet')

plt.gca().coastlines()

plt.show()

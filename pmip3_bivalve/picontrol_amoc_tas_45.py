import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


def max_overturning(cube,latitude):
    cube = cube[:,0,:,90+latitude]
    return scipy.signal.detrend(np.max(cube.data,axis = 1))


dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_tas = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'

m0 = model_names(dir_amoc)
m1 = model_names(dir_tas)

models = np.intersect1d(m0, m1)

models = list(models)
models.remove('CNRM-CM5-2') # diff no. amoc and tas years



var = np.zeros([np.size(models),180,360])
var[:] = np.NAN
var_1 = var.copy()
var_2 = var.copy()
var_3 = var.copy()
var_4 = var.copy()
var_5 = var.copy()
var_6 = var.copy()
var_7 = var.copy()
var_8 = var.copy()
var_10 = var.copy()
var_20 = var.copy()
lags = np.array([-5,-4,-3,-2,-1,0,1,2,3,4,5])
correlations = [var,var_1,var_2,var_3,var_4,var_5,var_6,var_7,var_8,var_10,var_20]
var_name = 'tas'
for i,model in enumerate(models):
    print i
    c1 = iris.load_cube(dir_tas + model+'_'+var_name+'_piControl_*.nc')
    c2 = iris.load_cube(dir_amoc + model+'_msftmyz_piControl_*.nc')
    if np.shape(c1)[0] == np.shape(c2)[0]:
        iris.coord_categorisation.add_year(c1, 'time', name='year')
        # qplt.contourf(c1[0],31)
        # plt.gca().coastlines()
        # plt.show()
        try:
        	missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
        except:
        	missing_data = np.where(c1[0].data.data == 0.0)
        cube_data = c1.data
        cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
        c1.data = cube_data_detrended
        #
        for j,lag in enumerate(lags):
            print 'j: ',j
            ts = np.roll(max_overturning(c2,45),lag)
            cube = c1.copy()
            cube = iris.analysis.maths.multiply(cube, 0.0)
            ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
            cube = iris.analysis.maths.add(cube, ts2)
            out_cube = iris.analysis.stats.pearsonr(c1, cube, corr_coords=['time'])
            correlations[j][i] = out_cube.data




with open('/home/ph290/Documents/python_scripts/pickles/amoc_tas_45.pickle', 'w') as f:
    pickle.dump([correlations], f)

"""


with open('/home/ph290/Documents/python_scripts/pickles/amoc_tas_45.pickle', 'r') as f:
    [correlations] = pickle.load(f)

"""

var_mean = np.nanmean(correlations[3],axis=0)
c1 = iris.load_cube(dir_tas + model+'_'+var_name+'_piControl_*.nc')
mean_var_cube = c1[0]
tmp2 = np.ma.masked_array(var_mean)
mean_var_cube.data = tmp2.data

plt.close('all')
qplt.contourf(mean_var_cube,51)
plt.gca().coastlines()
plt.savefig('/home/ph290/Documents/figures/amoc_tas.png')





c1 = iris.load_cube(dir_tas + models[0]+'_'+var_name+'_piControl_*.nc')

def plot_prep(i,correlations,c1):
	var_mean = np.nanmean(correlations[i],axis=0)
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube

plt.close('all')

plt.figure(figsize=(8, 10))
fig, ax = plt.subplots(2,3,figsize=(14, 10))

mean_var_cube = plot_prep(0,correlations,c1)
ax1 = plt.subplot(2,3,1)
qplt.contourf(mean_var_cube,np.linspace(-0.3,0.3,31))
plt.gca().coastlines()
plt.title('1')

mean_var_cube = plot_prep(2,correlations,c1)
ax2 = plt.subplot(2,3,2)
qplt.contourf(mean_var_cube,np.linspace(-0.3,0.3,31))
plt.gca().coastlines()
plt.title('2')

mean_var_cube = plot_prep(4,correlations,c1)
ax3 = plt.subplot(2,3,3)
qplt.contourf(mean_var_cube,np.linspace(-0.3,0.3,31))
plt.gca().coastlines()
plt.title('3')

mean_var_cube = plot_prep(6,correlations,c1)
ax4 = plt.subplot(2,3,4)
qplt.contourf(mean_var_cube,np.linspace(-0.3,0.3,31))
plt.gca().coastlines()
plt.title('4')

mean_var_cube = plot_prep(8,correlations,c1)
ax5 = plt.subplot(2,3,5)
qplt.contourf(mean_var_cube,np.linspace(-0.3,0.3,31))
plt.gca().coastlines()
plt.title('5')

mean_var_cube = plot_prep(10,correlations,c1)
ax6 = plt.subplot(2,3,6)
qplt.contourf(mean_var_cube,np.linspace(-0.3,0.3,31))
plt.gca().coastlines()
plt.title('6')

plt.savefig('/home/ph290/Documents/figures/amoc_tas_lead_lag_45.png')
plt.show()

"""
import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import iris.coord_categorisation
import scipy
import scipy.stats
import glob
import os
from scipy.stats import gaussian_kde
import matplotlib.cm as mpl_cm
import scipy.signal
import running_mean as rm
import random



def model_names(directory):
        files = glob.glob(directory+'/*_sic_*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


def ensemble_names(directory):
        files = glob.glob(directory+'/*_sic_*.nc')
        ensembles_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        ensembles_tmp.append(file.split('/')[-1].split('_')[3].split('.')[0])
                        ensemble = np.unique(ensembles_tmp)
        return ensemble



#directory = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
directory = '/data/NAS-ph290/ph290/cmip5/historicalNat/regridded/'

models = model_names(directory)
ensembles = ensemble_names(directory)
# ensembles = ensembles[0:1]

no_yrs = []
models2 = []

count = 0

for i,model in enumerate(models):
	for j,ensemble in enumerate(ensembles):
		print 'counting years in model: '+model
		try:
			try:
				cube = iris.load_cube(directory+'*'+model+'_sic*_'+ensemble+'_*')
			except:
				cube = iris.load_cube(directory+'*'+model+'_sic*_'+ensemble+'_*')
			iris.coord_categorisation.add_year(cube, 'time', name='year')
			no_years = np.size(cube.coord('year').points)
			if no_years > 100:
				no_yrs.append(no_years)
				models2.append(model)
				count +=1
		except:
			print 'no data'

models2 = np.unique(models2)

no_yrs = np.array(no_yrs)
min_years = int(np.min(no_yrs))

print min_years

lon_west = -45.0
lon_east = 25.0
lat_south = 0.0
lat_north = 90.0

smoothing = 10

model_ts = np.zeros([count,min_years-3-smoothing])

count2=0

for i,model in enumerate(models2):
    for j,ensemble in enumerate(ensembles):
        try:
            print 'processing model: '+model
            try:
            	cube = iris.load_cube(directory+'*'+model+'_sic*_'+ensemble+'_*')
            except:
            	cube = iris.load_cube(directory+'*'+model+'_sic*_'+ensemble+'_*')
            cube = cube[1:min_years-2]
            #
            cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
            cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
            #
            cube_region.coord('latitude').guess_bounds()
            cube_region.coord('longitude').guess_bounds()
            grid_areas = iris.analysis.cartography.area_weights(cube_region)
            area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
            area_avged_cube_anom = area_avged_cube - area_avged_cube.collapsed(['time'], iris.analysis.MEAN)
            tmp =  scipy.signal.detrend(rm.running_mean(area_avged_cube_anom.data,smoothing)[smoothing/2:-1*smoothing/2])
            # tmp =  scipy.signal.detrend(area_avged_cube_anom.data)
            model_ts[count2,:] = (tmp - np.min(tmp))/ np.max(tmp)
            count2 += 1
        except:
            print 'no data again'

mm_mean = np.mean(model_ts,axis = 0)

"""

number_runs = 10000

xs=np.zeros([number_runs,count])
ys=np.zeros([number_runs,count])

for count3 in range(number_runs):
    # print count3
    for i in range(count):
        subset = np.zeros([i+1,min_years-3-smoothing])
        # my_list = np.random.randint(np.shape(model_ts)[0], size=i+1)
        my_list = random.sample(range(np.shape(model_ts)[0]), i+1)
        for j in range(i+1):
            subset[j,:] = model_ts[my_list[j],:].copy()
        subset_mean = np.mean(subset,axis = 0)
        r = scipy.stats.pearsonr(subset_mean, mm_mean)
        r2 = r[0]*r[0]
        xs[count3,i] = i
        ys[count3,i] = r[0]
        #plt.scatter(i-1,r[0])

xs2 = np.nanmean(xs,axis = 0)
ys2 = np.nanmean(ys,axis = 0)
xs2 = np.append(0,xs2+1)
ys2 = np.append(0,ys2)

xs = xs.reshape(number_runs * count)
ys = ys.reshape(number_runs * count)


xy = np.vstack([xs,ys])
z = gaussian_kde(xy)(xy)


# plt.xlabel('ensemble size')
# plt.xlabel('r')
# plt.show()



cmap = mpl_cm.get_cmap('gist_yarg')



plt.close('all')
fig, ax = plt.subplots(figsize=(6, 5))
ax.scatter(xs+1, ys, c=z, s=10, edgecolor='',cmap=cmap)
plt.plot(xs2,ys2,'r')
plt.plot([8,8],[-0.2,1],'k',lw=3)
ax.set_xlabel('ensemble size')
ax.set_ylabel('R')
plt.ylim([0.0,1.0])
# plt.xlim([0,17])
ax2 = ax.twinx()
ax2.set_ylim([0.0,1.0])
ax2.set_yticks([0.0,0.2,0.4,0.6,0.8,1.0])
ax2.set_yticklabels(['0.00','0.04','0.16','0.36','0.64','1.0'])
ax2.set_ylabel('R$^2$')
plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/ensemble_size_sic.png')
plt.show(block = False)

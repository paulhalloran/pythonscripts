

import numpy as np
import matplotlib.path as mpath
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

def model_names(directory,my_string):
	files = glob.glob(directory+'/*'+my_string+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models



# directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
#directory = '/data/dataSSD0/ph290/last1000/'
# directory = '/data/dataSSD0/ph290/last1000/monthly/DJF/'
directory = '/data/dataSSD0/ph290/last1000/monthly/SON/'
# directory = '/data/dataSSD0/ph290/last1000/monthly/JJA/'
# directory = '/data/dataSSD0/ph290/last1000/monthly/MAM/'

lon_west = -24
lon_east = -13
lat_south = 65
lat_north = 67


lon_west2 = -40
lon_east2 = 10
lat_south2 = 45
lat_north2 = 90

start_year = 1000
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory,'pr_')
models = list(models)
# models.remove('IPSL-CM5A-LR')
# models.remove('MIROC-ESM')
# models.remove('FGOALS-s2')  # No sic variable downloaded at moment...
# models.remove('MPI-ESM-P')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadGEM2-ES') # No sic variable downloaded at moment...
# models.remove('HadCM3')
# models.remove('bcc-csm1-1')
# models.remove('GISS-E2-R') # No sic variable downloaded at moment...
# models.remove('FGOALS-gl')
# models.remove('FGOALS-s2')

##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp[::-1]
tmp = r_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
bivalve_data_initial_c = tmp[::-1]
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
# bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)
# bivalve_data_initial_c=scipy.signal.detrend(bivalve_data_initial_c)


try:
	mm_mean_t
	test = False
except NameError:
	test = True


### uncomment if you want to force a new run ###
# test = True


if test:
	mm_mean_p = np.zeros([np.size(models),np.size(expected_years)])
	mm_mean_p[:,:] = np.nan
	mm_mean_p_map = np.zeros([np.size(models),np.size(expected_years),180,360])
	mm_mean_p_map[:,:] = np.nan


	for i,model in enumerate(models):
		print i
		c0 = iris.load_cube(directory + model+'_pr_past1000_r1i1p1_regridded.nc')
		cube_region_tmp = c0.intersection(longitude=(lon_west, lon_east))
		c0_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		try:
			c0_region.coord('latitude').guess_bounds()
		except:
			print 'cube already has latitude bounds'
		try:
			c0_region.coord('longitude').guess_bounds()
		except:
			print 'cube already has longitude bounds'
		# try:
		# 	c3_region.coord('latitude').guess_bounds()
		# except:
		# 	print 'cube already has latitude bounds'
		# try:
		# 	c3_region.coord('longitude').guess_bounds()
		# except:
		# 	print 'cube already has longitude bounds'
		grid_areas = iris.analysis.cartography.area_weights(c0_region)
		c0_region_mean = c0_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		# c3_region_mean = c3_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		coord = c0.coord('time')
		dt = coord.units.num2date(coord.points)
		year = np.array([coord.units.num2date(value).year for value in coord.points])
		for j,y in enumerate(expected_years):
			loc = np.where(year == y)[0][0]
			if np.size(loc) > 0:
	# 			try:
					mm_mean_p[i,j] = c0_region_mean[loc].data
					mm_mean_p_map[i,j,:,:] = c0[loc].data

		# 		except:
		# 			print 'year missing'
		#


out_cube = c0[0].copy()
out_cube.data = np.ma.mean(mm_mean_p_map,axis = (0,1))
qplt.contourf(out_cube,31)
plt.gca().coastlines()
plt.show()

out_cube1 = out_cube.copy()
out_cube.data = np.ma.mean(mm_mean_p_map[:,:400,:,:],axis = (0,1))
out_cube1.data = np.ma.mean(mm_mean_p_map[:,450:,:,:],axis = (0,1))
qplt.contourf(out_cube - out_cube1,np.linspace(-0.0000005,0.0000005,50))
plt.gca().coastlines()
plt.show()

# '''
# for i,dummy in enumerate(models):
# 	out_cube.data = np.ma.mean(mm_mean_p_minus_e_map[i,:,:,:],axis = 0)
# 	qplt.contourf(out_cube,31)
# 	plt.gca().coastlines()
# 	plt.show()
# '''
# for i,dummy in enumerate(models):
# 	out_cube.data = np.ma.mean(mm_mean_p_map[i,:400,:,:],axis = 0)
# 	out_cube1.data = np.ma.mean(mm_mean_p_map[i,450:,:,:],axis = 0)
# 	qplt.contourf(out_cube - out_cube1,30)
# 	plt.gca().coastlines()
# 	plt.show()


# for i,model in enumerate(models):
# 	mm_mean_p[i,j] -= np.min(mm_mean_p[i,:])
# 	mm_mean_p[i,:] /= np.max(mm_mean_p[i,:])
# 	mm_mean_e[i,:] -= np.min(mm_mean_e[i,:])
# 	mm_mean_e[i,:] /= np.max(mm_mean_e[i,:])
# 	mm_mean_s[i,:] -= np.min(mm_mean_s[i,:])
# 	mm_mean_s[i,:] /= np.max(mm_mean_s[i,:])
# 	# mm_mean_sic[i,:] -= np.min(mm_mean_sic[i,:])
# 	# mm_mean_sic[i,:] /= np.max(mm_mean_sic[i,:])
# 	mm_mean_p_minus_e[i,:] -= np.nanmin(mm_mean_p_minus_e[i,:])
# 	mm_mean_p_minus_e[i,:] /= np.nanmax(mm_mean_p_minus_e[i,:])

mm_mean_p_m = np.nanmean(mm_mean_p,axis = 0)



mm_mean_p_m -= np.nanmin(mm_mean_p_m)
mm_mean_p_m /= np.nanmax(mm_mean_p_m)



bivalve_data_initial -= np.nanmin(bivalve_data_initial)
bivalve_data_initial /= np.nanmax(bivalve_data_initial)



plt.close('all')
fig, ax = plt.subplots(2,1,figsize=(12, 6))

# plt.plot(expected_years,mm_mean_s_m,'b',lw=2,alpha=0.75)
# plt.plot(expected_years,rm.running_mean(mm_mean_s_m,5),'b',lw=2,alpha=0.75)
# plt.plot(expected_years,rm.running_mean(mm_mean_e_m,5),'r')
ax[0].plot(expected_years,(rm.running_mean(mm_mean_p_m,5)*2.0)-0.2,'g',lw=2,alpha=0.75)
ax[0].plot(bivalve_year,bivalve_data_initial,'k',lw=2,alpha=0.75)

# mm_mean_sic_m *= -1.5
# mm_mean_sic_m += 1.0
# ax[1].plot(expected_years,rm.running_mean(mm_mean_sic_m,5),'b',lw=2,alpha=0.75)
# ax[1].plot(bivalve_year,bivalve_data_initial,'k',lw=2,alpha=0.75)

plt.show(block = True)



# for i,model in enumerate(models):
# 	y = rm.running_mean(mm_mean_p_minus_e[i,:],5)
# 	plt.plot(expected_years,y - np.nanmean(y) ,'b',lw=2,alpha=0.1)
#
#
#
# y = rm.running_mean(mm_mean_p_minus_e_m,5)*0.5
# plt.plot(expected_years,y - np.nanmean(y),'g',lw=2,alpha=0.90)
# plt.plot(bivalve_year,(bivalve_data_initial-np.nanmean(bivalve_data_initial))*0.5,'k',lw=2,alpha=0.90)
# plt.show()

'''
import numpy as np
import matplotlib.path as mpath
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

def model_names(directory,my_string):
	files = glob.glob(directory+'/*'+my_string+'*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models



directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


start_year = 1200
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

models = model_names(directory,'psl')
models = list(models)
# models.remove('MIROC-ESM')
# models.remove('FGOALS-s2')  # No sic variable downloaded at moment...
# models.remove('MPI-ESM-P')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadGEM2-ES') # No sic variable downloaded at moment...
models.remove('HadCM3')
models.remove('bcc-csm1-1')
# models.remove('GISS-E2-R') # No sic variable downloaded at moment...

test_cube = iris.load_cube(directory+models[0]+'_psl_past1000_r1i1p1_regridded.nc')
iris.coord_categorisation.add_year(test_cube, 'time', name='year')
shape = np.shape(test_cube.data)
data_array = np.zeros([np.size(models),np.size(expected_years),shape[1],shape[2]])
data_array = np.ma.masked_array(data_array)

for j,model in enumerate(models):
	print model
	cube = iris.load_cube(directory+model+'_psl_past1000_r1i1p1_regridded.nc')
	iris.coord_categorisation.add_year(cube, 'time', name='year')
	years = cube.coord('year').points
	for i,y in enumerate(expected_years):
		loc = np.where(years == y)
		data_array[j,i,:,:] = cube[loc].data


data_array_mmm = np.mean(data_array,axis = 0)
loc1 = np.where((expected_years >= start_year) & (expected_years < 1500))[0]
loc2 = np.where((expected_years >= 1600) & (expected_years < end_year))[0]
data_array_mmm_0 = np.mean(data_array_mmm[loc1],axis = 0)
data_array_mmm_1 = np.mean(data_array_mmm[loc2],axis = 0)

plotting_cube1 = test_cube[0].copy()
plotting_cube2 = test_cube[0].copy()

plotting_cube1.data = data_array_mmm_0
plotting_cube2.data = data_array_mmm_1

'''

proj = 'Orthographic'
projections = {}
projections['Orthographic'] = ccrs.Orthographic(central_longitude=-20.0, central_latitude=45.0)
projections['NorthPolarStereo'] = ccrs.NorthPolarStereo()
projections['PlateCarree'] = ccrs.PlateCarree()
pcarree = projections['PlateCarree']
new_cube, extent = iris.analysis.cartography.project(plotting_cube1, pcarree,
                                                         nx=400, ny=200)
                                                         
new_cube2, extent = iris.analysis.cartography.project(plotting_cube1 - plotting_cube2, pcarree,
                                                         nx=400, ny=200)

plt.close('all')
fig = plt.figure()
fig.suptitle('PSL')
# Set up axes and title
ax = plt.subplot(projection=projections[proj])
# Set limits
ax.set_global()
# ax.set_extent([-180, 180, 40, 90], ccrs.PlateCarree())# plot with Iris quickplot pcolormesh
# qplt.contourf(new_cube,50)
cs = qplt.contourf(new_cube2,30)
#,colors='k')
# plt.clabel(cs, inline=1, fontsize=10)
# Draw coastlines
ax.coastlines()

theta = np.linspace(0, 2*np.pi, 100)
center, radius = [0.5, 0.5], 0.5
verts = np.vstack([np.sin(theta), np.cos(theta)]).T
circle = mpath.Path(verts * radius + center)
ax.set_boundary(circle, transform=ax.transAxes)

iplt.show(block = False)

#
# qplt.contourf(plotting_cube1,51)
# qplt.contour(plotting_cube1 - plotting_cube2,10,colors='k')
# plt.gca().coastlines()
# plt.show()

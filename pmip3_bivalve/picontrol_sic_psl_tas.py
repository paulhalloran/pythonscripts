"""
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


def max_overturning(cube,latitude):
    cube = cube[:,0,:,90+latitude]
    return scipy.signal.detrend(np.max(cube.data,axis = 1))




def calculate_correlations(models,var_name,my_dir,dir_sic,lags):
	lags = [0]
	lon_west = -80.0
	lon_east = 70
	lat_south = 40
	lat_north = 90
	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	correlations = [var]
	for i,model in enumerate(models):
		try:
			print i
			c1 = iris.load_cube(my_dir + model+'_'+var_name+'_piControl_*.nc')
			c2 = iris.load_cube(dir_sic + model+'_sic_piControl_*.nc')
			try:
				iris.coord_categorisation.add_year(c1, 'time', name='year')
				c1 = c1.aggregated_by('year', iris.analysis.MEAN)
			except:
				print ' '
			try:
				iris.coord_categorisation.add_year(c2, 'time', name='year')
				c2 = c2.aggregated_by('year', iris.analysis.MEAN)
			except:
				print ' '
			if np.shape(c1)[0] == np.shape(c2)[0]:
				# qplt.contourf(c1[0],31)
				# plt.gca().coastlines()
				# plt.show()
				try:
					missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
				except:
					missing_data = np.where(c1[0].data.data == 0.0)
				cube_data = c1.data
				cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
				c1.data = cube_data_detrended
				#
				cube_region_tmp = c2.intersection(longitude=(lon_west, lon_east))
				c2 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
				try:
					c2.coord('longitude').guess_bounds()
					c2.coord('latitude').guess_bounds()
				except:
					print 'cube already has bounds'
				grid_areas = iris.analysis.cartography.area_weights(c2)
				sic_ts = c2.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
				for j,lag in enumerate(lags):
					print 'j: ',j
					ts = np.roll(sic_ts,int(lag))
					smoothing = 10
					ts = rm.running_mean(ts,smoothing)
					ts = ts[smoothing/2:-smoothing/2]
					cube = c1[smoothing/2:-smoothing/2].copy()
					cube = iris.analysis.maths.multiply(cube, 0.0)
					ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
					cube = iris.analysis.maths.add(cube, ts2)
					out_cube = iris.analysis.stats.pearsonr(c1[smoothing/2:-smoothing/2], cube, corr_coords=['time'])
					correlations[j][i] = out_cube.data
		except:
			print 'failed'
	return correlations


dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_psl = '/data/NAS-ph290/ph290/cmip5/piControl/'
dir_tas = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_tos = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_sic = dir_tas

m0 = model_names(dir_amoc,'msftmyz')
m1 = model_names(dir_psl,'psl')
m2 = model_names(dir_tas,'tas')
m3 = model_names(dir_sic,'sic')
m4 = model_names(dir_tos,'tos')


models = np.intersect1d(m0,m1)
models = np.intersect1d(models,m2)
models = np.intersect1d(models,m3)
models = np.intersect1d(models,m4)


models = list(models)
models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
models.remove('CESM1-CAM5')
models.remove('CESM1-FASTCHEM')
models.remove('CESM1-WACCM')
# models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
models.remove('inmcm4')
models.remove('MPI-ESM-LR')
models.remove('MPI-ESM-P')
models.remove('NorESM1-ME')

print models




lags=[0]
var_name = 'psl'
correlations_psl = calculate_correlations(models,var_name,dir_psl,dir_sic,lags)
var_name = 'tas'
correlations_tas = calculate_correlations(models,var_name,dir_tas,dir_sic,lags)


with open('/home/ph290/Documents/python_scripts/pickles/sic_psl.pickle', 'w') as f:
    pickle.dump([correlations_psl], f)




"""

with open('/home/ph290/Documents/python_scripts/pickles/sic_psl.pickle', 'r') as f:
    [correlations_psl] = pickle.load(f)

"""


def plot_prep(i,correlations,c1):
	var_mean = np.nanmean(correlations[i],axis=0)
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube


#################################



# plt.close('all')
# plt.figure(figsize=(12, 5))
#
#
# ######### Subplot 1 #########
#
# var_name = 'psl'
# c1 = iris.load_cube(dir_psl + models[0]+'_'+var_name+'_piControl_*.nc')
# mean_var_cube = plot_prep(0,correlations_psl,c1)
#
# my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
# ax = plt.subplot(1,2,1,projection= my_projection)
# ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())
#
# cube1 = mean_var_cube
# lats1 = cube1.coord('latitude').points
# lons1 = cube1.coord('longitude').points
# data1 = cube1.data
#
#
# contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.5,0.5,31),
# 			transform=ccrs.PlateCarree(),cmap='jet')
#
# cb = plt.colorbar(contour_result1, orientation='horizontal')
# # plt.clabel(contour_result1, inline=1, fontsize=10)
# tick_locator = ticker.MaxNLocator(nbins=5)
# cb.locator = tick_locator
# cb.update_ticks()
#
# # Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# # river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# # land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
# coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# # glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# # river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')
#
# # ax.add_feature(land_50m,facecolor='#D3D3D3')
# ax.add_feature(coast_50m)
# # ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# # ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# # ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)
#
# plt.title('Control run sic sea level pressure correlation\n(lag = 0)')
#
# plt.plot(-18.195667,66.5215, 'ro', transform=ccrs.PlateCarree())
# #66 31.59 N, 18 11.74 W
# plt.plot(15.73,68.80, color='brown',marker='o', transform=ccrs.PlateCarree())
#
# # x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# # ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# # ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
# ax.gridlines()
#
#
#
# plt.tight_layout()
# plt.savefig('/home/ph290/Documents/figures/sic_psl_pi_cont_composites_alternative2.png')
# plt.show(block = False)

"""

###########

plt.close('all')
plt.figure(figsize=(12, 10))

######### Subplot 1 #########

var_name = 'psl'
c1 = iris.load_cube(dir_psl + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_psl,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((2, 1), (0, 0),projection= my_projection)

ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data

contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('Sea-ice PSL correlation\n(control run)')
plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())
ax.gridlines()

######### Subplot 2 #########

var_name = 'tas'
c1 = iris.load_cube(dir_tas + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_tas,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((2, 1), (1, 0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.7,0.7,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
plt.title('Sea-ice tas correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())
ax.gridlines()


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/sic_psl_pi_cont_composites_alternative3.png')
plt.show(block = False)

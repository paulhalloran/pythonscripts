
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post as rmp
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import iris.analysis.stats
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker


def model_names(directory,var):
	files = glob.glob(directory+'/*_'+var+'_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
	models = np.unique(models_tmp)
	return models


def max_overturning(cube,latitude):
    cube = cube[:,0,:,90+latitude]
    return scipy.signal.detrend(np.max(cube.data,axis = 1))


lags = [0]

def calculate_correlations(models,var_name,my_dir,dir_amoc,lags):
	var = np.zeros([np.size(models),180,360])
	var[:] = np.NAN
	correlations = [var]
	for i,model in enumerate(models):
		print i
		c1 = iris.load_cube(my_dir + model+'_'+var_name+'_piControl_*.nc')
		c2 = iris.load_cube(dir_amoc + model+'_msftmyz_piControl_*.nc')
		if np.shape(c1)[0]/12 == np.shape(c2)[0]:
			iris.coord_categorisation.add_year(c1, 'time', name='year')
			try:
				c1 = c1.aggregated_by('year', iris.analysis.MEAN)
			except:
				subprocess.call('cdo -P 8 yearmean '+my_dir + model+'_'+var_name+'_piControl_*.nc /data/dataSSD0/ph290/tmp/delete.nc', shell=True)
				c1 = iris.load_cube('/data/dataSSD0/ph290/tmp/delete.nc')
		if np.shape(c1)[0] == np.shape(c2)[0]:
			# qplt.contourf(c1[0],31)
			# plt.gca().coastlines()
			# plt.show()
			try:
				missing_data = np.where((c1[0].data.data == c1.data.fill_value) | (c1[0].data.data == 0.0))
			except:
				missing_data = np.where(c1[0].data.data == 0.0)
			cube_data = c1.data
			cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
			c1.data = cube_data_detrended
			#
			for j,lag in enumerate(lags):
				print 'j: ',j
				ts = np.roll(max_overturning(c2,40),int(lag))
				smoothing = 10
				ts = rm.running_mean(ts,smoothing)
				ts = ts[smoothing/2:-smoothing/2]
				cube = c1[smoothing/2:-smoothing/2].copy()
				cube = iris.analysis.maths.multiply(cube, 0.0)
				ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
				cube = iris.analysis.maths.add(cube, ts2)
				out_cube = iris.analysis.stats.pearsonr(c1[smoothing/2:-smoothing/2], cube, corr_coords=['time'])
				correlations[j][i] = out_cube.data
		try:
			subprocess.call('rm /data/dataSSD0/ph290/tmp/delete.nc', shell=True)
		except:
			pass
	return correlations



dir_amoc = '/data/NAS-ph290/ph290/cmip5/msftmyz/piControl/'
dir_psl = '/data/NAS-ph290/ph290/cmip5/piControl/'
dir_tas = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_tos = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
dir_sic = dir_tas

m0 = model_names(dir_amoc,'msftmyz')
m1 = model_names(dir_psl,'psl')
m2 = model_names(dir_tas,'tas')
m3 = model_names(dir_sic,'sic')
m4 = model_names(dir_tos,'tos')


models = np.intersect1d(m0,m1)
models = np.intersect1d(models,m2)
models = np.intersect1d(models,m3)
models = np.intersect1d(models,m4)


models = list(models)
models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
models.remove('CESM1-CAM5')
models.remove('CESM1-FASTCHEM')
models.remove('CESM1-WACCM')
# models.remove('CNRM-CM5-2') # diff no. amoc and tas yeay
models.remove('inmcm4')
models.remove('MPI-ESM-LR')
models.remove('MPI-ESM-P')
models.remove('NorESM1-ME')



print models

###
"""
var_name = 'psl'
correlations_psl = calculate_correlations(models,var_name,dir_psl,dir_amoc,lags)
with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c.pickle', 'w') as f:
    pickle.dump([correlations_psl], f)


var_name = 'tas'
correlations_tas = calculate_correlations(models,var_name,dir_tas,dir_amoc,lags)
with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c2.pickle', 'w') as f:
    pickle.dump([correlations_tas], f)


var_name = 'sic'
correlations_sic = calculate_correlations(models,var_name,dir_sic,dir_amoc,lags)
with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c3.pickle', 'w') as f:
    pickle.dump([correlations_sic], f)


var_name = 'tos'
correlations_tos = calculate_correlations(models,var_name,dir_tos,dir_amoc,lags)
with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45d.pickle', 'w') as f:
    pickle.dump([correlations_tos], f)

"""
###

with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c3.pickle', 'r') as f:
    [correlations_sic] = pickle.load(f)

with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c2.pickle', 'r') as f:
    [correlations_tas] = pickle.load(f)


with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45c.pickle', 'r') as f:
    [correlations_psl] = pickle.load(f)

with open('/home/ph290/Documents/python_scripts/pickles/amoc_psl_45d.pickle', 'r') as f:
    [correlations_tos] = pickle.load(f)

#the following is obtained by running: picontrol_sic_psl.py
with open('/home/ph290/Documents/python_scripts/pickles/sic_psl.pickle', 'r') as f:
    [correlations_sic_psl] = pickle.load(f)

def plot_prep(i,correlations,c1):
	var_mean = np.nanmean(correlations[i],axis=0)
	mean_var_cube = c1[0]
	tmp2 = np.ma.masked_array(var_mean)
	mean_var_cube.data = tmp2.data
	return mean_var_cube




with open('/home/ph290/Documents/python_scripts/pickles/analysis_fill_length.pickle', 'r') as f:
    [expected_years,mm_mean_d18O_m_filtered,mm_mean_d18O_m_filtered_t_only,mm_mean_d18O_m_filtered_s_only,x,y] = pickle.load(f)


#################################

##################
# paper figure 4
##################



plt.close('all')
plt.figure(figsize=(12, 10))



######### Subplot 1 #########

var_name = 'sic'
c1 = iris.load_cube(dir_sic + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_sic,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
ax = plt.subplot2grid((2, 2), (0, 0),projection= my_projection)

ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.15,0.15,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='#D3D3D3')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)

# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('AMOC Sea-ice correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

#66 31.59 N, 18 11.74 W
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())

# x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
ax.gridlines()

######### Subplot 2 #########

var_name = 'psl'
c1 = iris.load_cube(dir_psl + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_psl,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
# ax = plt.subplot(1,2,1,projection= my_projection)
ax = plt.subplot2grid((2, 2), (0, 1),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.1,0.1,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='#D3D3D3')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('AMOC sea level pressure correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

#66 31.59 N, 18 11.74 W
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())

# x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
ax.gridlines()


######### subplot 3 #################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]
s_d = rm.running_mean(s_d,5)*-1.0
s_d -= np.nanmin(s_d)
s_d /= np.nanmax(s_d)




var_name = 'tas'
c1 = iris.load_cube(dir_tas + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_tas,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
# ax = plt.subplot(1,2,1,projection= my_projection)
ax = plt.subplot2grid((2, 2), (1, 0),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='#D3D3D3')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('AMOC tas correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

#66 31.59 N, 18 11.74 W
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())

# x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
ax.gridlines()
#

######### subplot 4 #################

file = '/data/NAS-ph290/ph290/misc_data/last_mill_sunshine/forfjorddalen2012.txt'
s_data = np.genfromtxt(file,skip_header = 11,delimiter = ',')
s_y = s_data[:,0]
s_d = s_data[:,4]
s_d = rm.running_mean(s_d,5)*-1.0
s_d -= np.nanmin(s_d)
s_d /= np.nanmax(s_d)




var_name = 'tos'
c1 = iris.load_cube(dir_tos + models[0]+'_'+var_name+'_piControl_*.nc')
mean_var_cube = plot_prep(0,correlations_tos,c1)

my_projection = ccrs.Orthographic(central_longitude=-20, central_latitude=60)
# ax = plt.subplot(1,2,1,projection= my_projection)
ax = plt.subplot2grid((2, 2), (1, 1),projection= my_projection)
ax.set_extent([-180, 180, 35, 90], crs=ccrs.PlateCarree())

cube1 = mean_var_cube
lats1 = cube1.coord('latitude').points
lons1 = cube1.coord('longitude').points
data1 = cube1.data


contour_result1 = ax.contourf(lons1, lats1, data1,np.linspace(-0.3,0.3,31),
			transform=ccrs.PlateCarree(),cmap='jet')

cb = plt.colorbar(contour_result1, orientation='horizontal')
# plt.clabel(contour_result1, inline=1, fontsize=10)
tick_locator = ticker.MaxNLocator(nbins=5)
cb.locator = tick_locator
cb.update_ticks()
cb.set_label('R')

# Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
# river_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_lake_centerlines_scale_rank', '10m',facecolor='none')
# land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',facecolor='none')
coast_50m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',facecolor='none')
# glacier_50m = cfeature.NaturalEarthFeature('phypslal', 'glaciated_areas', '10m',facecolor='none')
# river_euro_50m = cfeature.NaturalEarthFeature('phypslal', 'rivers_europe', '10m',facecolor='none')

# ax.add_feature(land_50m,facecolor='#D3D3D3')
ax.add_feature(coast_50m,edgecolor='#000000',alpha = 0.3)
# ax.add_feature(river_50m, edgecolor='blue',alpha=0.5)
# ax.add_feature(glacier_50m, facecolor='grey',alpha=0.5)
# ax.add_feature(river_euro_50m, edgecolor='blue',alpha=0.5)

plt.title('AMOC tos correlation\n(control run)')

plt.plot(-18.195667,66.5215, 'k*',markersize = 20, transform=ccrs.PlateCarree())

#66 31.59 N, 18 11.74 W
plt.plot(15.73,68.80, color='#808080',marker='o',markersize = 10, transform=ccrs.PlateCarree())

# x, y = [-80.0, -80.0, 70, 70, -80.0], [40, 90, 90, 40, 40]
# ax.plot(x, y,'o',transform=ccrs.PlateCarree())
# ax.fill(x, y, color='coral', transform=ccrs.PlateCarree(), alpha=0.4)
ax.gridlines()

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/amoc_psl_pi_tas_composites.png')
plt.show(block = False)

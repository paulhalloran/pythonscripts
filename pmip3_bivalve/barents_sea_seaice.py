import numpy as np
import matplotlib.path as mpath
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

#
#prepared using:
#sudo cdo remapbil,r360x45 -sellonlatbox,-180,180,45,90 sic_OImon_HadCM3_past1000_r1i1p1_merged.nc sic_OImon_HadCM3_past1000_r1i1p1_merged_n_regrid.nc

# directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
directory = '/data/NAS-ph290/ph290/cmip5/past1000/'

lon_west = 23
lon_east = 53
lat_south = 68
lat_north = 78

# model = 'MPI-ESM-P'
model = 'HadCM3'

# c0 = iris.load_cube(directory + model+'_pr_past1000_r1i1p1_regridded_not_vertically_Amon.nc')
c1 = iris.load_cube(directory +'sic_OImon_'+model+'_past1000_r1i1p1_merged_n_regrid.nc')

iris.coord_categorisation.add_season_year(c1, 'time', name='season_year')
iris.coord_categorisation.add_season_number(c1, 'time', name='season_number')

c1_seasonal = c1.aggregated_by(['season_year','season_number'], iris.analysis.MEAN)
iris.coord_categorisation.add_season(c1_seasonal, 'time', name='season')

loc = np.where(c1_seasonal.coord('season').points == 'djf')
c1_djf = c1_seasonal[loc]


cube_region_tmp = c1_djf.intersection(longitude=(lon_west, lon_east))
c1_djf_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))






qplt.contourf(c1_djf[0])
plt.gca().coastlines()
plt.show()

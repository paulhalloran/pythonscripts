print 'first run picontrol_amoc_psl_40b.py'

###### last millennium psl ts ##############

lon_west = -50.0
lon_east = 20.0
lat_south = 58
lat_north = 68

ta1 = 1200
ta2 = 1550

last_mill_psl_dir = '/data/NAS-ph290/ph290/cmip5/last1000/'

models_psl = ['CCSM4', 'CSIRO-Mk3L-1-2', 'HadCM3', 'MIROC-ESM', 'MPI-ESM-P', 'MRI-CGCM3']
#taken from analysis_alternative_full_length.py

out_data = np.zeros([np.size(models_psl),ta2-ta1])
out_data[:] = np.NAN

for i,model in enumerate(models_psl):
	print i
	cube = iris.load_cube(last_mill_psl_dir + model+'_psl_past1000_*.nc')
	iris.coord_categorisation.add_year(cube, 'time', name='year')
	cube = cube.aggregated_by('year', iris.analysis.MEAN)
	cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
	cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
	try:
		cube.coord('latitude').guess_bounds()
		cube.coord('longitude').guess_bounds()
	except:
		print 'has bounds'
	grid_areas = iris.analysis.cartography.area_weights(cube)
	area_avged_cube_data = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
	detrend_a_avg = scipy.signal.detrend(area_avged_cube_data, axis=0)
	year = cube.coord('year').points
	loc = np.where((year > ta1) & (year <= ta2))
	out_data[i] = detrend_a_avg[loc]


psl_year = year[loc]
psl_ts = np.nanmean(out_data,axis = 0)





plt.close('all')
plt.figure(figsize=(12, 5))

ax = plt.subplot(1,1,1)
loc = np.where((s_y >=1200) & (s_y <= 1550))
ax.plot(s_y[loc],s_d[loc],'#2F4F4F',lw=1.5,alpha = 0.8,label = 'Fennoscandia $\delta^{13}$C')

ax2 = ax.twinx()
psl_ts_norm = psl_ts.copy()
psl_ts_norm -= np.min(psl_ts_norm)
psl_ts_norm /= np.max(psl_ts_norm)
ax2.plot(psl_year,psl_ts_norm)

ax2.set_ylim([0,0.8])

plt.show()

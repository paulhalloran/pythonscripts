import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
import running_mean as rm
import pandas as pd

def model_names(directory):
        files = glob.glob(directory+'/*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[2])
                        models = np.unique(models_tmp)
        return models





'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space = '/data/dataSSD0/ph290/tmp2/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/dataSSD1/ph290/tmp/hemispheric_carbon/'
#Directory where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/NAS-ph290/ph290/cmip5/hemispheric_carbon/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiments = ['piControl']
#comma separated list of the CMIP5 variables that you want to process (e.g. 'tos','fgco2' etc.)
variables = np.array(['nbp','fgco2'])

'''
nbp = This is the net mass flux of carbon between land and atmosphere calculated as photosynthesis MINUS the sum of  plant and soil respiration, carbonfluxes  from fire, harvest, grazing  and land use change. Positive flux  is into the land.
'''

'''
Main bit of code follows...
'''

print 'Processing data from: '+ input_directory
#This runs the function above to come up with a list of models from the filenames
models = model_names(input_directory)
#models = models[::-1] #reversing so 2nd script can work from other end...


ensemble = 'r1i1p1'



#Looping through each model we have
for model in models:
    print 'processing: '+model
    for experiment in experiments:
            print experiment
            files = glob.glob(input_directory+'*'+model+'*.nc')
            #Looping through each variable we have
            for var in variables:
                print 'processing: '+var
                #testing if the output file has alreday been created
                tmp = glob.glob(output_directory+model+'_'+var+'_'+experiment+'*.nc')
                temp_file1 = str(uuid.uuid4())+'.nc'
                temp_file2 = str(uuid.uuid4())+'.nc'
                temp_file3 = str(uuid.uuid4())+'.nc'
                temp_file4 = str(uuid.uuid4())+'.nc'
                if np.size(tmp) == 0:
                        #reads in the files to process
                        print 'reading in: '+var+' '+model
                        files = glob.glob(input_directory+'/'+var+'*_'+model+'_*'+experiment+'*.nc')
                        sizing = np.size(files)
                        #checks that we have some files to work with for this model, experiment and variable
                        if not sizing == 0:
                            if sizing > 1:
                                    #if the data is split across more than one file, it is combined into a single file for ease of processing
                                    files = ' '.join(files)
                                    print 'merging files'
                                    #merge together different files from the same experiment
                                    subprocess.call(['cdo -P 7 mergetime '+files+' '+temporary_file_space+temp_file1], shell=True)
                            if sizing == 1:
                                    print 'no need to merge - only one file present'
                                    subprocess.call(['cp '+files[0]+' '+temporary_file_space+temp_file1], shell=True)
                            #print 'time-meaning data (to annual means if required)'
                            print 'extract and average region'
                            subprocess.call('cdo -P 7 yearmean '+temporary_file_space+temp_file1+' '+temporary_file_space+temp_file2, shell=True)
                            subprocess.call('rm '+temporary_file_space+temp_file1, shell=True)
                            infile = temporary_file_space+temp_file2
                            subprocess.call('cdo -P 7 fldsum -sellonlatbox,-180,180,0,90 -mul '+infile+' -gridarea '+infile+' '+output_directory+model+'_'+var+'_'+experiment+'_northern_.nc', shell=True)
                            subprocess.call('cdo -P 7 fldsum -sellonlatbox,-180,180,-90,0 -mul '+infile+' -gridarea '+infile+' '+output_directory+model+'_'+var+'_'+experiment+'_southern_.nc', shell=True)
                            subprocess.call('rm '+temporary_file_space+temp_file2, shell=True)



# for model in models:
# Now read these files in, add together the land and ocean, average the last 200 yrs and write out to a text file for each hemisphere
# Also check that (e.g.) my weighted averages are working...



models = list(models)
[models.remove(x) for x in ['BNU-ESM','CMCC-CESM','IPSL-CM5A-LR']]

smoothing = 10

dict = {}

for model in models:
    # model = models[3]
    file1 = output_directory+model+'_nbp_piControl_northern_.nc'
    file2 = output_directory+model+'_nbp_piControl_southern_.nc'
    file3 = output_directory+model+'_fgco2_piControl_northern_.nc'
    file4 = output_directory+model+'_fgco2_piControl_southern_.nc'
    if (os.path.isfile(file1) & os.path.isfile(file2) & os.path.isfile(file3) & os.path.isfile(file4)):
        nbp_n = iris.load_cube(file1)[:,0,0]
        nbp_s = iris.load_cube(file2)[:,0,0]
        fgco2_n = iris.load_cube(file3)[:,0,0]
        fgco2_s = iris.load_cube(file4)[:,0,0]
        if model == 'CanESM2':
            fgco2_n /= (44.0/12.0)
            fgco2_s /= (44.0/12.0)
        if len(nbp_n.data) == len(fgco2_n.data):
            dict[model] = {}
            plt.close('all')
            plt.plot(rm.running_mean(nbp_n.data,smoothing),'g',label='land north',alpha = 0.75)
            plt.plot(rm.running_mean(fgco2_n.data,smoothing),'b',label='ocean north',alpha = 0.75)
            plt.plot(rm.running_mean(nbp_s.data,smoothing),'g--',label='land south',alpha = 0.75)
            plt.plot(rm.running_mean(fgco2_s.data,smoothing),'b--',label='ocean north',alpha = 0.75)
            plt.plot(rm.running_mean(fgco2_n.data+nbp_n.data,smoothing),'r',label='combined north',alpha = 0.75,lw=1.5)
            plt.plot(rm.running_mean(fgco2_s.data+nbp_s.data,smoothing),'r--',label='combined south',alpha = 0.75,lw=1.5)
            plt.title(model+' '+str(smoothing)+'yr running mean smoothed')
            plt.xlabel('year of run')
            plt.ylabel('kg C s$^{-1}$')
            plt.legend(loc='best', fancybox=True, framealpha=0.5)
            plt.tight_layout()
            plt.savefig('/home/ph290/Documents/figures/hemispheric_flux/'+model+'.png')
            n = np.nanmean(nbp_n[-200::].data + fgco2_n[-200::].data)
            s = np.nanmean(nbp_s[-200::].data + fgco2_s[-200::].data)
            dict[model]['North'] = n
            dict[model]['South'] =  s


df = pd.DataFrame(data=dict)
df.to_csv('/home/ph290/Documents/figures/hemispheric_flux/flux_data.csv')

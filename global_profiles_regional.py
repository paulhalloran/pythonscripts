import iris
import numpy as np
import glob
import os
import matplotlib.pyplot as pyplot
import iris.quickplot as qplt
import matplotlib as mpl
from cdo import *
cdo = Cdo()
import itertools

in_dir = '/data/dataSSD0/ph290/'
out_dir = '/data/dataSSD1/ph290/'

def model_names(directory):
        files = glob.glob(directory+'/*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[2])
                        models = np.unique(models_tmp)
        return models


def model_names2(directory):
        files = glob.glob(directory+'/*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


def bounds(cube):
    try:
        cube.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'
    try:
        cube.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'
    return cube


#
# models = model_names(in_dir)
# models = list(models)
# models.remove('MRI-ESM1')
# # models.remove('GFDL-ESM2M')
#
#
#
# for model in models:
#     if not(os.path.isfile(out_dir+model+'_talk_global.nc')):
#         cdo.mergetime(input=in_dir+'talk_Oyr_'+model+'_historical_r1i1p1_*.nc',output=out_dir+'tmp.nc',options = '-P 7')
#         cdo.timmean(input= out_dir+'tmp.nc', output=out_dir+model+'_talk_global.nc',options = '-P 7')
#         cdo.fldmean(input= out_dir+model+'_talk_global.nc', output=out_dir+model+'_talk_global_profile.nc',options = '-P 7')
#         os.remove(out_dir+'tmp.nc')
#


models = model_names2(out_dir)
models = list(models)
# models.remove('MRI-ESM1')
# models.remove('GFDL-ESM2M')


glodap_alk = iris.load_cube('/data/NAS-ph290/ph290/misc_data/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TAlk.nc','seawater alkalinity expressed as mole equivalent per unit mass')
glodap_alk_error = iris.load_cube('/data/NAS-ph290/ph290/misc_data/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TAlk.nc','total alkalinity error')
woa_cube =  iris.load_cube('/data/NAS-ph290/ph290/misc_data/temperature_annual_1deg.nc','sea_water_temperature')
tmm_alk = iris.load_cube('/home/ph290/Downloads/ALK.nc')

tmm_alk = bounds(tmm_alk)

grid_areas = iris.analysis.cartography.area_weights(tmm_alk)
tmm_alk_area_avged_cube = tmm_alk.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

glodap_alk = bounds(glodap_alk)
glodap_alk_error = bounds(glodap_alk_error)

mpl.rcdefaults()

fsize = 12

font = {'family' : 'monospace',
        'weight' : 'bold',
        'family' : 'serif',
        'size'   : fsize}

mpl.rc('font', **font)
marker = itertools.cycle(('o', 'v', '^', '<', '>', 's', '8','p'))

lat_lon_coordinates = {}
lat_lon_coordinates['N. ATLANTIC'] = {}
lat_lon_coordinates['N. ATLANTIC']['lon_west'] = 280
lat_lon_coordinates['N. ATLANTIC']['lon_east'] = 350
lat_lon_coordinates['N. ATLANTIC']['lat_south'] = 0
lat_lon_coordinates['N. ATLANTIC']['lat_north'] = 70.0

lat_lon_coordinates['S. ATLANTIC'] = {}
lat_lon_coordinates['S. ATLANTIC']['lon_west'] = 280
lat_lon_coordinates['S. ATLANTIC']['lon_east'] = 350
lat_lon_coordinates['S. ATLANTIC']['lat_south'] = -45
lat_lon_coordinates['S. ATLANTIC']['lat_north'] = 0

lat_lon_coordinates['S. Ocean'] = {}
lat_lon_coordinates['S. Ocean']['lon_west'] = 0
lat_lon_coordinates['S. Ocean']['lon_east'] = 360
lat_lon_coordinates['S. Ocean']['lat_south'] = -90
lat_lon_coordinates['S. Ocean']['lat_north'] = -50

lat_lon_coordinates['N. Pacific'] = {}
lat_lon_coordinates['N. Pacific']['lon_west'] = 120
lat_lon_coordinates['N. Pacific']['lon_east'] = 360-110
lat_lon_coordinates['N. Pacific']['lat_south'] = 0
lat_lon_coordinates['N. Pacific']['lat_north'] = 70

lat_lon_coordinates['S. Pacific'] = {}
lat_lon_coordinates['S. Pacific']['lon_west'] = 120
lat_lon_coordinates['S. Pacific']['lon_east'] = 360-110
lat_lon_coordinates['S. Pacific']['lat_south'] = -45
lat_lon_coordinates['S. Pacific']['lat_north'] = 0

lat_lon_coordinates['Indian'] = {}
lat_lon_coordinates['Indian']['lon_west'] = 35
lat_lon_coordinates['Indian']['lon_east'] = 110
lat_lon_coordinates['Indian']['lat_south'] = -35
lat_lon_coordinates['Indian']['lat_north'] = 20

lat_lon_coordinates['Global'] = {}
lat_lon_coordinates['Global']['lon_west'] = 0
lat_lon_coordinates['Global']['lon_east'] = 360
lat_lon_coordinates['Global']['lat_south'] = -90
lat_lon_coordinates['Global']['lat_north'] = 90



rem_mod = ['MIROC-ESM','MIROC-ESM-CHEM','NorESM1-ME']
#NOTE CDO DOES NOT PICK TEH RIGHT AREA FOR NORESM
           #,'CMCC-CESM','CNRM-CM5','GFDL-ESM2G','GFDL-ESM2M','IPSL-CM5A-LR','IPSL-CM5A-MR','IPSL-CM5B-LR','MPI-ESM-LR','MPI-ESM-MR']

for m in rem_mod:
    models.remove(m)

basins = ['N. ATLANTIC','S. ATLANTIC','S. Ocean','N. Pacific','S. Pacific','Indian','Global']

for basin in basins:

    lon_west = lat_lon_coordinates[basin]['lon_west']
    lon_east = lat_lon_coordinates[basin]['lon_east']
    lat_south = lat_lon_coordinates[basin]['lat_south']
    lat_north = lat_lon_coordinates[basin]['lat_north']

    cube_region_tmp = glodap_alk.intersection(longitude=(lon_west, lon_east))
    cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
    grid_areas = iris.analysis.cartography.area_weights(cube_region)
    glodap_alk_area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)



    """

    plt.close('all')
    plt.figure(figsize=(10, 10))
    for model in models:
        print model
        # cube = iris.load_cube(out_dir+model+'_talk_global_profile.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
        try:
            os.remove(out_dir+'tmp.nc')
        except:
            print 'no file to remove'
        cdo.fldmean(input= '-sellonlatbox,'+str(lon_west)+','+str(lon_east)+','+str(lat_south)+','+str(lat_north)+' '+out_dir+model+'_talk_global.nc', output=out_dir+'tmp.nc',options = '-P 7')
        cube = iris.load_cube(out_dir+'tmp.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
        # try:
        if np.size(np.shape(cube)) == 4:
            print 'plotting '+ model
            cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN)
            try:
                plt.plot(cube.data,cube.coord('depth').points * -1.0,alpha=0.5,lw=1,marker=marker.next(),label = model)
            except:
                print "no 'depth' cooordinate"
                try:
                    plt.plot(cube.data,cube.coord('ocean sigma over z coordinate').points * -1.0,lw=1,alpha=0.5,marker=marker.next(),label = model)
                except:
                    print "no 'ocean sigma over z coordinate' cooordinate"
                # qplt.plot(cube,'b')
        # except:
        #     print 'area averaging within iris failed'

    # plt.plot(tmm_alk_area_avged_cube.data,tmm_alk_area_avged_cube.coord('Depth').points * -1.0,'r',lw=3)

    plt.plot((glodap_alk_area_avged_cube.data/1.0e3)*1.026,woa_cube.coord('depth').points * -1.0,'r',lw=3,label='GLODAP')
    # plt.plot(glodap_alk_error_area_avged_cube.data/1.026e3),woa_cube.coord('depth').points * -1.0,'k',lw=1)

    plt.legend(fontsize='small')
    # plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile_iris_averaging.png')
    plt.xlabel('alkalinity (mole equivalent m$^{-3}$)')
    plt.ylabel('depth (m)')
    plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile.png')
    plt.show(block = True)
    """

    # GLODAP = mole equivalent per unit mass
    # CMIP5 = mole_equivalent / (mol m-3)



    #g = obs initialised
    #r= model initialised
    #b=mixed
    #y = fixed value
    #k = unkown

    models_d = {}
    models_d['BNU-ESM'] = 'k'
    models_d['CESM1-BGC'] = 'b'
    models_d['CMCC-CESM'] = 'g'
    models_d['CNRM-CM5'] = 'b'
    models_d['CanESM2'] = 'b'
    models_d['GFDL-ESM2G'] = 'g'
    models_d['GFDL-ESM2M'] = 'g'
    models_d['HadGEM2-CC'] = 'r'
    models_d['HadGEM2-ES'] = 'r'
    models_d['IPSL-CM5A-LR'] = 'r'
    models_d['IPSL-CM5A-MR'] = 'b'
    models_d['IPSL-CM5B-LR'] = 'b'
    models_d['MIROC-ESM'] = 'g'
    models_d['MIROC-ESM-CHEM'] = 'g'
    models_d['MPI-ESM-LR'] = 'y'
    models_d['MPI-ESM-MR'] = 'y'
    models_d['NorESM1-ME'] = 'g'

    plt.close('all')
    plt.figure(figsize=(10, 10))

    plt.plot((glodap_alk_area_avged_cube.data/1.0e3)*1.026,woa_cube.coord('depth').points * -1.0,'g',lw=3,label='GLODAP')
    # plt.plot(glodap_alk_error_area_avged_cube.data/1.026e3),woa_cube.coord('depth').points * -1.0,'k',lw=1)

    for model in models:
        print model
        try:
            os.remove(out_dir+'tmp.nc')
        except:
            print 'no file to remove'
        cdo.fldmean(input= '-sellonlatbox,'+str(lon_west)+','+str(lon_east)+','+str(lat_south)+','+str(lat_north)+' '+out_dir+model+'_talk_global.nc', output=out_dir+'tmp.nc',options = '-P 7')
        cube = iris.load_cube(out_dir+'tmp.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
        try:
            # grid_areas = iris.analysis.cartography.area_weights(cube)
            if np.size(np.shape(cube)) == 4:
                print 'plotting '+ model
                # cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN, weights=grid_areas)
                cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN)
                try:
                    plt.plot(cube.data,cube.coord('depth').points * -1.0,alpha=0.5,lw=1,marker=marker.next(),color = models_d[model],label = model)
                except:
                    print "no 'depth' cooordinate"
                    try:
                        plt.plot(cube.data,cube.coord('ocean sigma over z coordinate').points * -1.0,lw=1,marker=marker.next(),alpha=0.5,color = models_d[model],label = model)
                    except:
                        print "no 'ocean sigma over z coordinate' cooordinate"
                    # qplt.plot(cube,'b')
        except:
            print 'area averaging within iris failed'

    # plt.plot(tmm_alk_area_avged_cube.data,tmm_alk_area_avged_cube.coord('Depth').points * -1.0,'r',lw=3)

    plt.title(basin)
    plt.legend(fontsize='small')
    # plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile_iris_averaging.png')
    plt.xlabel('alkalinity (mole equivalent m$^{-3}$)')
    plt.ylabel('depth (m)')
    basin2 = basin
    try:
        basin2 = basin.replace(" ","").replace(".","")
    except:
        print 'no spaces in string'
    plt.savefig('/home/ph290/Documents/figures/cmip5_'+basin2+'_alkalinity_profile_by_initialisation.png')
    plt.show(block = False)


import iris
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.io.shapereader import Reader
from cartopy.feature import ShapelyFeature
from shapely.ops import transform as geom_transform
from cartopy.io.img_tiles import StamenTerrain
import cartopy

file = '/data/NAS-geo01/ph290/observations/seaice/median_extent_N_02_1981-2010_polyline_v3.0'
plt.close('all')

#########

plt.close('all')
fig = plt.figure(figsize=[5, 5])
ax = fig.add_subplot(1, 1, 1, projection=ccrs.NorthPolarStereo(central_longitude=-20.0))


# Limit the map to -60 degrees latitude and below.
ax.set_extent([-180, 180, 55, 90], ccrs.PlateCarree())

# ax.add_feature(cfeature.LAND.with_scale('50m'))
# ax.add_feature(cfeature.COASTLINE.with_scale('50m'))
ax.add_feature(cfeature.LAND)
ax.add_feature(cfeature.COASTLINE)
# Compute a circle in axes coordinates, which we can use as a boundary
# for the map. We can pan/zoom as much as we like - the boundary will be
# permanently circular.
theta = np.linspace(0, 2*np.pi, 100)
center, radius = [0.5, 0.5], 0.5
verts = np.vstack([np.sin(theta), np.cos(theta)]).T
circle = mpath.Path(verts * radius + center)

ax.set_boundary(circle, transform=ax.transAxes)


######### shapefile ##########
# ax.add_geometries(Reader(file).geometries(), ccrs.NorthPolarStereo(central_longitude=-45.0),edgecolor='b', facecolor='none', alpha=0.5)
ax.add_geometries(Reader(file).geometries(), ccrs.NorthPolarStereo(central_longitude=-45.0),edgecolor='b', facecolor='none', alpha=0.5)

ax.set_extent([-180, 180, 55, 90], ccrs.PlateCarree())

plt.savefig('/home/ph290/Documents/figures/arctic6.svg')
plt.show(block = False)

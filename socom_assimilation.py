import iris
import netCDF4

def get_datetime(cube):
    return netCDF4.num2date(cube.coord('time').points,cube.coord('time').units.name,cube.coord('time').units.calendar)


variable = 'Surface ocean pCO2'
directory = '/data/NAS-ph290/ac911/'

free_run = 'rosie_mi-ax763_r111640_frdf_medusa_diad.grid_T_monthly.nc'

assimilation_run = 'rosie_mi-ax752_2ls_r111638_frdf_medusa_diad.grid_T_monthly.nc'

c_free = iris.load_cube(directory+free_run,variable)
c_assim = iris.load_cube(directory+assimilation_run,variable)
free_datetime = get_datetime(c_free)
assim_datetime = get_datetime(c_assim)

common_datetimes = [test in assim_datetime for test in free_datetime]
c_free = c_free[common_datetimes]

common_datetimes = [test in free_datetime for test in assim_datetime]
c_assim = c_assim[common_datetimes]

try:
    c_free_mean = c_free.collapsed(['time','Vertical T levels'],iris.analysis.MEAN)
    c_assim_mean = c_assim.collapsed(['time','Vertical T levels'],iris.analysis.MEAN)
except:
    c_free_mean = c_free.collapsed(['time'],iris.analysis.MEAN)
    c_assim_mean = c_assim.collapsed(['time'],iris.analysis.MEAN)

# plt.close('all')
# qplt.contourf(c_assim_mean-c_free_mean,np.linspace(-8,8,51),cmap='RdYlBu_r')
# # plt.gca().coastlines()
# plt.show(block = False)


import matplotlib
import matplotlib.pyplot as plt

import cartopy.crs as ccrs
import iris
import iris.analysis.cartography
import iris.quickplot as qplt

cube = c_assim_mean-c_free_mean
plt.close('all')


# Choose plot projections
projections = {}
projections['Mollweide'] = ccrs.Mollweide()
projections['PlateCarree'] = ccrs.PlateCarree()
projections['NorthPolarStereo'] = ccrs.NorthPolarStereo()
projections['Orthographic'] = ccrs.Orthographic(central_longitude=0,
                                                central_latitude=-90)

pcarree = projections['PlateCarree']
# Transform cube to target projection
new_cube, extent = iris.analysis.cartography.project(cube, pcarree,
                                                     nx=600, ny=300)

# Plot data in each projection
name = 'Mollweide'
fig = plt.figure()
fig.suptitle('assimilation minus no assimilation')
# Set up axes and title
ax = plt.subplot(projection=projections[name])
# Set limits
ax.set_global()
# plot with Iris quickplot pcolormesh
qplt.contourf(new_cube,np.linspace(-8,8,51),cmap='RdYlBu_r')

# Draw coastlines
ax.coastlines()

plt.show(block = False)

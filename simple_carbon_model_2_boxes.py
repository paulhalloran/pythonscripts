#The issue here is that whatever you do, it starts just tracking atm. CO2 very quickly. I gues sthis model needs a dep box as well.

import numpy as np
import mocsy
import matplotlib.pyplot as plt
from scipy import interpolate

######
# Initial Conditions / Setup
######

sal = 36.0
temp = 10.0
# alk = 2.44430536287 # mol/m3 (GLODAP N. Atl. mean)
alk = 2.4 # mol/m^3 (GLODAP N. Atl. mean)
alk = 2.4 # mol/m^3 (GLODAP N. Atl. mean)
# initial_dic = 2.14411821259 #
initial_dic = 2.1 #
dic = initial_dic

alk2 = 2.28539769401 # mol/m^3 (CMIP5 N. Atl. mean)
alk2 = 2.55 #
# alk2 = 2.0 # mol/m^3 (GLODAP N. Atl. mean)
# initial_dic2 = 2.00443704456 # mol/m^3 (GLODAP N. atl. mean)
initial_dic2 = 2.1 # mol/m^3 (GLODAP N. atl. mean)
dic2 = initial_dic2

deep_dic = 2.6
deep_dic2 = 2.6

deep_ocean_box_depth = 4000.0 #m50.0 #m

# mixed_layer_depth1 = 50.0 #m obs summer
# mixed_layer_depth2 = 50.0 #m models summer
mixed_layer_depth1 = 200.0 #m obs summer
mixed_layer_depth2 = 200.0 #m models summer
#mixed_layer_depth1 = 800 #m obs winter lab. sea
#mixed_layer_depth2 = 500 #m models lab. sea

#fraction_dic_lost_by_mixing_per_day = 3.0e-5 # tune this to get the correct avg. air-sea flux
fraction_dic_lost_by_mixing_per_day1 = 1.5e-4 # tune this to get the correct avg. air-sea flux
fraction_dic_lost_by_mixing_per_day2 = 1.5e-4 # tune this to get the correct avg. air-sea flux
mixing = 0.0001

loss_by_biological_export = 0.0e-3 #1.0e-3 # mol/m^2/day

# U101 = 10 # Wind speed (m/s)
# U102 = 10 # Wind speed (m/s)
U101 = 10 # Wind speed (m/s)
U102 = 10 # Wind speed (m/s)

spinup_length = 100000 # days

start_yr = 1850
end_yr = 2100

# Atmospheric CO2 data file
cmip5_atm_co2_file = '/data/data0/ph290/cmip5/forcing_data/rcp_forcing_data/historical_and_rcp85_atm_co2.txt'



######
# Constants
######

yearsec = 60.0 * 60.0 * 24.0 * 365.0
daysec = 60.0 * 60.0 * 24.0
depth_top_model_layer = 10.0 #m
patm = 1.0

######
# Functions
######


def calculate_gas_transfer(temp,U10):
    Sc = 2073.1 - (125.62 * temp) + (3.6276*(temp**2)) - (0.043219*(temp**3)) # from Wanninkhof 1992
    kw = 1.0/3.6e+05 * (0.31 *(U10**2)) * (660/Sc)**0.5
    kw660 = kw * (Sc/660)**0.5
    return kw660


def run_integration(timesteps,xco2_ts,gas_trans_velocity, patm, depth_top_model_layer,temp,sal,alk, dic,loss_by_biological_export,fraction_dic_lost_by_mixing_per_day,deep_dic,deep_ocean_box_depth,mixing,mixed_layer_depth):
    co2flux_array = np.zeros(np.size(timesteps))
    dic_array = np.zeros(np.size(timesteps))
    pco2_array = np.zeros(np.size(timesteps))
    dpco2_array = np.zeros(np.size(timesteps))
    fco2_array = np.zeros(np.size(timesteps))
    revelle_factor_array = np.zeros(np.size(timesteps))
    for i,timestep in enumerate(timesteps):
        xco2 = xco2_ts[i]
        #uses Jim Orr's python/fortran co2 package: http://ocmip5.ipsl.jussieu.fr/mocsy/bkg.html
        co2flux, co2ex, dpco2, ph, pco2, fco2, co2, hco3, co3, OmegaA, OmegaC, BetaD, rhoSW, p, tempis = mocsy.gasx.flxco2(kw660 = gas_trans_velocity, xco2 = xco2, patm = patm, dz1 = depth_top_model_layer, temp = temp, sal = sal,alk = alk, dic = dic, sil = 0, phos = 0, optcon='mol/m3', optt='Tinsitu', optp='db', optb="u74", optk1k2='l', optkf="dg", optgas='Pinsitu')
        #co2flux in mol/(m^2 * s)
        co2flux = co2flux[0]
        co2flux_array[i] = co2flux
        pco2_array[i] = pco2[0]
        dpco2_array[i] = dpco2[0]
        fco2_array[i] = fco2[0]
        dic_array[i] = dic
        mol_co2_m2 = co2flux * daysec
        mol_dic_m2 = dic * mixed_layer_depth
        deep_dic_m2 = deep_dic * deep_ocean_box_depth
        mol_dic_m2 += mol_co2_m2
        mol_dic_m2 -= mol_dic_m2 * fraction_dic_lost_by_mixing_per_day
        mol_dic_m2 -= loss_by_biological_export
        deep_dic += loss_by_biological_export
        deep_dic_gain = mixing * dic
        dic_gain = mixing * deep_dic
#solve ode: http://www.physics.nyu.edu/pine/pymanual/html/chap9/chap9_scipy.html
#explanation: http://tutorial.math.lamar.edu/Classes/DE/SystemsModeling.aspx
#http://web.eng.hawaii.edu/~bsb/me696_f14/assign/Ode_r1.pdf
        deep_dic = deep_dic_m2 / deep_ocean_box_depth
        dic = (mol_dic_m2 / mixed_layer_depth)
        dic = dic - deep_dic_gain + dic_gain
        deep_dic = deep_dic + deep_dic_gain - dic_gain
        revelle_factor_array[i] = BetaD
    return co2flux_array, pco2_array, dpco2_array, fco2_array, dic_array, revelle_factor_array, dic, deep_dic



######
# Main program
######

# Read in atmospheric CO2 data and associated years
years = np.genfromtxt(cmip5_atm_co2_file,delimiter = ',',skip_header=1)[:,0]
xco2_ts = np.genfromtxt(cmip5_atm_co2_file,delimiter = ',',skip_header=1)[:,1]

# Interpolate atmospheric CO2 data to give daily timestep (probably too small, but annual is I think too big...)
f1 = interpolate.interp1d(years, xco2_ts)
f2 = interpolate.interp1d(years, years)
no_days = (end_yr - start_yr +1) * 360.0
xco2_ts = f1(np.linspace(start_yr,end_yr,no_days))
days = f2(np.linspace(start_yr,end_yr,no_days))

# #initialise arrays to hold output for analysis/plotting
# co2flux_array = np.zeros(np.size(years))
# dic_array = np.zeros(np.size(years))
# pco2_array = np.zeros(np.size(years))
# dpco2_array = np.zeros(np.size(years))
# fco2_array = np.zeros(np.size(years))

#calculate teh gas transfer velocity
gas_trans_velocity1 = calculate_gas_transfer(temp,U101)
gas_trans_velocity2 = calculate_gas_transfer(temp,U102)
#gas_trans_velocity = 2.777777777777778e-05
####??? If th gas transfer velocity too low?



#####
# Simulation
#####

# spinup run - to get DIC in equilibrium
spinup_arbitrary_days = np.arange(spinup_length)
xco2_ts_spinup = spinup_arbitrary_days * 0.0 + xco2_ts[0]
co2flux_array_spinup, pco2_array_spinup, dpco2_array_spinup, fco2_array_spinup, revelle_factor_array_spinup, dic_array_spinup, dic_post_spinup, deep_dic_post_spinup = run_integration(spinup_arbitrary_days,xco2_ts_spinup,gas_trans_velocity1, patm, depth_top_model_layer,temp,sal,alk, dic, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day1,deep_dic,deep_ocean_box_depth,mixing,mixed_layer_depth1)

# historical run
# dic_post_spinup = dic

co2flux_array, pco2_array, dpco2_array, fco2_array, dic_array, revelle_factor_array, dic_post_historical, deep_dic_post_historical = run_integration(days,xco2_ts,gas_trans_velocity1, patm, depth_top_model_layer,temp,sal,alk, dic_post_spinup, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day1,deep_dic_post_spinup,deep_ocean_box_depth,mixing,mixed_layer_depth1)

#####
# Additional simulation for comparison
#####

# spinup run - to get DIC in equilibrium
spinup_arbitrary_days = np.arange(spinup_length)
xco2_ts_spinup = spinup_arbitrary_days * 0.0 + xco2_ts[0]
co2flux_array_spinup, pco2_array_spinup, dpco2_array_spinup, fco2_array_spinup, dic_array_spinup, revelle_factor_array_spinup, dic_post_spinup2, deep_dic_post_spinup2 = run_integration(spinup_arbitrary_days,xco2_ts_spinup,gas_trans_velocity2, patm, depth_top_model_layer,temp,sal,alk2, dic2, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day2,deep_dic2,deep_ocean_box_depth,mixing,mixed_layer_depth2)

# deep_dic_post_spinup = deep_dic2

# historical run
# dic_post_spinup = dic2
co2flux_array2, pco2_array2, dpco2_array2, fco2_array2, dic_array2, revelle_factor_array2, dic_post_historical2, deep_dic_post_historical2 = run_integration(days,xco2_ts,gas_trans_velocity2, patm, depth_top_model_layer,temp,sal,alk2, dic_post_spinup2, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day2,deep_dic_post_spinup2,deep_ocean_box_depth,mixing,mixed_layer_depth2)


#####
# Plotting
#####

plot_range = [1900,2100]

loc = np.where(days >= plot_range[0])[0]

plt.close('all')
f, axarr = plt.subplots(1,5, figsize=(20,8))

axarr[0].plot(days,xco2_ts,'b--',label = 'atm. xCO2')
axarr[0].plot(days,fco2_array,'b',label = 'alk = '+str(alk)+' (obs)')
axarr[0].plot(days,fco2_array2,'r',label = 'alk = '+str(alk2)+' (models)')
axarr[0].set_ylabel('fco2 (uatm)')
axarr[0].set_xlabel('Year')
axarr[0].set_xlim(plot_range)
axarr[0].legend(loc=2)

axarr[4].plot(days,fco2_array-fco2_array[loc[0]],'b',label = 'alk = '+str(alk)+' (obs)')
axarr[4].plot(days,fco2_array2-fco2_array2[loc[0]],'r',label = 'alk = '+str(alk2)+' (models)')
axarr[4].set_ylabel('fco2 anomaly (uatm)')
axarr[4].set_xlabel('Year')
axarr[4].set_xlim(plot_range)


a = co2flux_array * yearsec
b = co2flux_array2 * yearsec
axarr[1].plot(days,a,'b')
# axisb = axarr[1].twinx()
axarr[1].plot(days,b,'r')
axarr[1].set_ylabel('air-sea flux (mol/m2/yr)')
axarr[1].set_xlabel('Year')
axarr[1].set_xlim(plot_range)

a = co2flux_array * yearsec
b = co2flux_array2 * yearsec
axarr[2].plot(days,a - a[loc[0]],'b')
# axisb = axarr[1].twinx()
axarr[2].plot(days,b - b[loc[0]],'r')
axarr[2].set_ylabel('air-sea flux anomaly (mol/m2/yr)')
axarr[2].set_xlabel('Year')
axarr[2].set_xlim(plot_range)
# axarr[2].set_ylim([-1,1])

axarr[3].plot(days,revelle_factor_array,'b')
axarr[3].plot(days,revelle_factor_array2,'r')
axarr[3].set_ylabel('Revelle factor')
axarr[3].set_xlabel('Year')
axarr[3].set_xlim(plot_range)

plt.show(block = False)
'''

##############
#plot 2
###############

plot_range = [1850,2100]

loc = np.where(days >= plot_range[0])[0]

plt.close('all')
fig = plt.figure()
ax = fig.add_subplot(111)

fsize = 17
fontsize = 17

a = co2flux_array * yearsec
b = co2flux_array2 * yearsec
ax.plot(days,a - a[loc[0]],'b',lw=5,alpha = 0.5,label = 'alkalinity = '+str(alk))
# axisb = axarr[1].twinx()
ax.plot(days,b - b[loc[0]],'r',lw=5,alpha = 0.5,label = 'alkalinity = '+str(alk2))
ax.set_ylabel('Air-sea CO$_2$ flux anomaly (mol m$^{-2}$yr$^{-1)}$)', fontsize=fsize)
ax.set_xlabel('Year', fontsize=fsize)
ax.set_xlim(plot_range)
ax.legend(loc=2)
ax.tick_params(axis='x')
ax.tick_params(axis='y')


for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontsize(fontsize)


for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontsize(fontsize)
    #plt.show()


plt.tight_layout()
# axarr[2].set_ylim([-1,1])

plt.savefig('/home/ph290/Documents/figures/alk_from_box_model.png')
plt.show(block = False)
'''

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
import glob
import os

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


input_directory = '/data/dataSSD0/ph290/cmip5/spco2_regridded/joined/'

models = model_names(input_directory)
models = list(models)
models.remove('inmcm4')
models = np.array(models)

lon_west = -90.0
lon_east = 20
lat_south = 10
lat_north = 70.0 

model_data = []

for model in models:
	cube = iris.load_cube(input_directory+model+'_*.nc')
	cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
	cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
	try:
		cube_region.coord('latitude').guess_bounds()
	except:
		print 'cube already has latitude bounds' 
	try:
		cube_region.coord('longitude').guess_bounds()
	except:
		print 'cube already has longitude bounds'
	grid_areas = iris.analysis.cartography.area_weights(cube_region)
	area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
	model_data.append(area_avged_cube)

linestyles = [':','-','-.','--',':','-','-.','--',':','-','-.','--',':','-','-.','--',':','-','-.','--',':','-','-.','--',':','-','-.','--']

plt.close('all')
for i,data in enumerate(model_data):
	coord = data.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((year >= 1990) & (year <= 2011))
	data = data[loc]
	data -= data[0:3].collapsed('time',iris.analysis.MEAN)
	plt.plot(year[loc],data.data,linestyle = linestyles[i],label= models[i])
	

plt.ylabel('pCO2 anomaly form 1st 3 yrs (Pa)')
plt.xlabel('year')
plt.legend(bbox_to_anchor=(1.1, 1.0),fontsize='small')
plt.savefig('/home/ph290/figures/n_atl_anoms.png')



import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import matplotlib.pyplot as plt
import iris
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker
# from scipy.interpolate import griddata
from matplotlib.mlab import griddata

def calc_d18O(T,S):
        #S = ABSOLUTE SALINITY psu
        #T = ABSOLUTE T deg' C
        # d18Osw_synth = ((0.61*S)-21.3)
#       d18Osw_synth = ((3.0*S)-105)
        #R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
        #Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
        d18Osw_synth = ((0.55*S)-18.98)
        #LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
        # d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
        # d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
        #Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
        d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
        #From Reynolds - the -27 is to convert between SMOW and vPDB
        return d18Oc_synth


#data from https://www.nodc.noaa.gov/cgi-bin/OC5/gin-seas-climate/ginregcl.pl
annual_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
winter_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
spring_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
summer_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
autumn_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]

summer_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
winter_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
ann_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]

# cube =  winter_cube
# cube =  spring_cube
# cube =  summer_cube
# cube =  autumn_cube
t_cube =  annual_cube
s_cube =  ann_sal_cube

# density_cube = t_cube.copy()
# density_cube.standard_name = 'sea_water_density'
# density_cube.units = 'kg m-3'
#
# density_cube.data = seawater.dens(s_cube.data,t_cube.data,1)

d18O_cube = t_cube.copy()
# d18O_cube.standard_name = 'sea_water_d18O'
# d18O_cube.units = 'kg m-3'

d18O_cube.data = calc_d18O(t_cube.data+273.15,s_cube.data)



# lon_west = -35.0
# lon_east = 15.0
# lat_south = 60
# lat_north = 85.0
#
# cube_region_tmp = d18O_cube.intersection(longitude=(lon_west, lon_east))
# d18O_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
#
# cube_region_tmp = t_cube.intersection(longitude=(lon_west, lon_east))
# t_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
#
# cube_region_tmp = s_cube.intersection(longitude=(lon_west, lon_east))
# s_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

shape = np.shape(s_cube.data)

xi = np.linspace(np.min(s_cube.data),np.max(s_cube.data),100)
yi = np.linspace(np.min(t_cube.data),np.max(t_cube.data),100)
# grid the data.

x = np.reshape(s_cube.data,shape[0]*shape[1]*shape[2])
y = np.reshape(t_cube.data,shape[0]*shape[1]*shape[2])
z = np.reshape(d18O_cube.data,shape[0]*shape[1]*shape[2])


lat_var = 74.79

#####
# plot
#####


loc5 = np.where(t_cube.coord('latitude').points > lat_var)[0][0]
plt.close('all')
plt.figure(figsize = (10,3))


# cs = iplt.pcolormesh(t_cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
# CB = plt.colorbar(cs, shrink=0.99, extend='both')
#
# cs2=iplt.contour(s_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
# plt.clabel(cs2,[34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels
#
# plt.plot([-20,0,0,-20,-20],[300,300,0,0,300],'k',lw=5,alpha=0.5)
#
# plt.ylim(1200,0)
# plt.xlim(-25,15)
# plt.title = 'vertical section at '+str(lat_var)+'$^\circ$ latitude'
# plt.ylabel('depth (m)')
# plt.xlabel('longitude ($^\circ$)')



###################
# d18O and closeup
###################

loc5 = np.where(t_cube.coord('latitude').points > lat_var)[0][0]
# plt.close('all')
# plt.figure(figsize = (12,12))
# plt.subplot(211)

#d18O plot

ax2 = plt.subplot2grid((1,48),(0,0),colspan=16)
cs1 = iplt.pcolormesh(d18O_cube[:,loc5,:],cmap = 'viridis',vmin = 3.5,vmax=5)
# CB = plt.colorbar(cs1, shrink=0.99, extend='both')
# CB.set_label('$\delta^{18}$O')
# frame1 = plt.gca()
# frame1.axes.yaxis.set_ticklabels([])
# frame1.axes.xaxis.set_ticklabels([])
ax2.spines['bottom'].set_color('whitesmoke')
ax2.spines['top'].set_color('whitesmoke')
ax2.spines['left'].set_color('whitesmoke')
ax2.spines['right'].set_color('whitesmoke')
ax2.spines['left'].set_lw(0.1)
ax2.spines['right'].set_lw(0.1)
ax2.spines['top'].set_lw(0.1)
ax2.spines['bottom'].set_lw(0.1)
ax2.tick_params(
    axis='both',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False, # labels along the bottom edge are off
    left=False,      # ticks along the bottom edge are off
    right=False,         # ticks along the top edge are off
    labelleft=False) # labels along the bottom edge are off

#cs2=iplt.contour(s_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.2,colors='k')
#plt.clabel(cs2,[34.5,34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels

# plt.title = 'vertical section at '+str(lat_var)+'$^\circ$ latitude'
plt.ylim(300,0)
plt.xlim(-20,0)
# plt.ylabel('depth (m)')
# plt.xlabel('longitude ($^\circ$)')

#T/S plot


ax3 = plt.subplot2grid((1,48),(0,24),colspan=16)


cs2 = iplt.pcolormesh(t_cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
# CB2 = plt.colorbar(cs2, shrink=0.99, extend='both')
# CB2.set_label('temperature ($^{\circ}$C)')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs3=iplt.contour(s_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.2,colors='k')
plt.clabel(cs3,[34.5,34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels

plt.title = 'vertical section at '+str(lat_var)+'$^\circ$ latitude'
plt.ylim(300,0)
plt.xlim(-20,0)
plt.ylabel('depth (m)')
plt.xlabel('longitude ($^\circ$)')

#####
# colorbars
#####


ax10 = plt.subplot2grid((1,48),(0,42))
cb1 = plt.colorbar(cs2, cax=ax10,extend='both',orientation='vertical')

cb1.set_label('temperature ($^\circ$C)')

ax10 = plt.subplot2grid((1,48),(0,47))
cb2 = plt.colorbar(cs1, cax=ax10,extend='both',orientation='vertical')
cb2.set_label('$\delta^{18}$O')

plt.savefig('/home/ph290/Documents/figures/iceland/GIN_seas_lat_sections_'+str(lat_var)+'_d18O_t_closeup_for_fig.png')
plt.savefig('/home/ph290/Documents/figures/iceland/GIN_seas_lat_sections_'+str(lat_var)+'_d18O_t_closeup_for_fig.pdf')
plt.show(block = False)



import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import matplotlib.pyplot as plt
import iris
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker
# from scipy.interpolate import griddata
from matplotlib.mlab import griddata

def calc_d18O(T,S):
        #S = ABSOLUTE SALINITY psu
        #T = ABSOLUTE T deg' C
        # d18Osw_synth = ((0.61*S)-21.3)
#       d18Osw_synth = ((3.0*S)-105)
        #R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
        #Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
        d18Osw_synth = ((0.55*S)-18.98)
        #LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
        # d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
        # d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
        #Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
        d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
        #From Reynolds - the -27 is to convert between SMOW and vPDB
        return d18Oc_synth


#data from https://www.nodc.noaa.gov/cgi-bin/OC5/gin-seas-climate/ginregcl.pl
annual_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
winter_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
spring_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
summer_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
autumn_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]

summer_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
winter_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
ann_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]

# cube =  winter_cube
# cube =  spring_cube
# cube =  summer_cube
# cube =  autumn_cube
t_cube =  annual_cube
s_cube =  ann_sal_cube

# density_cube = t_cube.copy()
# density_cube.standard_name = 'sea_water_density'
# density_cube.units = 'kg m-3'
#
# density_cube.data = seawater.dens(s_cube.data,t_cube.data,1)

d18O_cube = t_cube.copy()
# d18O_cube.standard_name = 'sea_water_d18O'
# d18O_cube.units = 'kg m-3'

d18O_cube.data = calc_d18O(t_cube.data+273.15,s_cube.data)



# lon_west = -35.0
# lon_east = 15.0
# lat_south = 60
# lat_north = 85.0
#
# cube_region_tmp = d18O_cube.intersection(longitude=(lon_west, lon_east))
# d18O_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
#
# cube_region_tmp = t_cube.intersection(longitude=(lon_west, lon_east))
# t_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
#
# cube_region_tmp = s_cube.intersection(longitude=(lon_west, lon_east))
# s_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

shape = np.shape(s_cube.data)

xi = np.linspace(np.min(s_cube.data),np.max(s_cube.data),100)
yi = np.linspace(np.min(t_cube.data),np.max(t_cube.data),100)
# grid the data.

x = np.reshape(s_cube.data,shape[0]*shape[1]*shape[2])
y = np.reshape(t_cube.data,shape[0]*shape[1]*shape[2])
z = np.reshape(d18O_cube.data,shape[0]*shape[1]*shape[2])

# zi = griddata(x, y, z, xi, yi, interp='linear')
# # zi = griddata(points, values, (grid_x, grid_y), method='nearest')
#
#
#
# # CS = plt.contour(xi,yi,zi,15,linewidths=0.5,colors='k')
# CS = plt.contourf(xi,yi,zi,np.linspace(-5,5,33),cmap=plt.cm.jet)
# plt.colorbar() # draw colorbar
# # plot data points.
# # plt.scatter(x,y,marker='o',c='b',s=0.1)
# # plt.xlim(-2,2)
# # plt.ylim(-2,2)
# # plt.title('griddata test (%d points)' % npts)
# plt.savefig('/home/ph290/Documents/figures/GIN_seas_scatter_d18O.png')
# plt.show()

"""

plt.figure(0)
plt.close('all')
plt.scatter(x, y, c=z,cmap='viridis')
cb1 = plt.colorbar()
plt.xlabel('salinity')
plt.ylabel('temperature $^\circ$C')
cb1.set_label('d$^{18}$O')
plt.show(block = False)


minv1=2.75
maxv1=4.5

my_cmap = 'winter'

depth = 50
loc = np.where( d18O_cube.coord('depth').points > depth)[0][0]

plt.close('all')
plt.figure(1)
qplt.pcolormesh(d18O_cube[loc],cmap = my_cmap,vmin = minv1,vmax=maxv1)
plt.gca().coastlines()
plt.show(block = False)


"""

lat_var = 74.79


loc5 = np.where(t_cube.coord('latitude').points > lat_var)[0][0]
plt.close('all')
plt.figure(figsize = (13,8))

ax1 = plt.subplot2grid((2,2),(0,0),rowspan=2)


cs = iplt.pcolormesh(t_cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
# CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
# CB=plt.colorbar(cs)
CB = plt.colorbar(cs, shrink=0.99, extend='both')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs2=iplt.contour(s_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
plt.clabel(cs2,[34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels
# plt.clabel(cs2,inline=True, fontsize=10)

# plt.colorbar(cs2)
plt.plot([-20,0,0,-20,-20],[300,300,0,0,300],'k',lw=5,alpha=0.5)

# iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
plt.ylim(1200,0)
plt.xlim(-25,15)
# plt.xlim(65,85)
plt.title = 'vertical section at '+str(lat_var)+'$^\circ$ latitude'
plt.ylabel('depth (m)')
plt.xlabel('longitude ($^\circ$)')

# plt.savefig('/home/ph290/Documents/figures/iceland/GIN_seas_lat_sections_'+str(lat_var)+'_temperature.png')
# plt.show(block = True)

###################
# d18O and closeup
###################

loc5 = np.where(t_cube.coord('latitude').points > lat_var)[0][0]
# plt.close('all')
# plt.figure(figsize = (12,12))
# plt.subplot(211)

ax2 = plt.subplot2grid((2,2),(0,1))

cs = iplt.pcolormesh(d18O_cube[:,loc5,:],cmap = 'viridis',vmin = 3.5,vmax=5)
# CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
# CB=plt.colorbar(cs)
CB = plt.colorbar(cs, shrink=0.99, extend='both')
CB.set_label('$\delta^{18}$O')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs2=iplt.contour(s_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.2,colors='k')
plt.clabel(cs2,[34.5,34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels

#
# ###regridding temperature to lower resolution to allow contour plot###
#
# ds= np.linspace(np.min(t_cube.coord('depth').points), np.max(t_cube.coord('depth').points), np.shape(t_cube)[0])
# los = np.linspace(np.min(t_cube.coord('longitude').points), np.max(t_cube.coord('longitude').points), np.shape(t_cube)[2])
# depth = iris.coords.DimCoord(ds, standard_name='depth', units='m',attributes = t_cube.coord('depth').attributes)
# longitude = iris.coords.DimCoord(los, standard_name='longitude', units='degrees')
# tmp_cube1 = iris.cube.Cube(np.zeros((len(ds),len(los)), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(depth,0),  (longitude, 1)])
# tmp_cube1.data =t_cube[:,loc5,:].data
#
# depths=30
# longs = 80
# ds= np.linspace(np.min(t_cube.coord('depth').points), np.max(t_cube.coord('depth').points), depths)
# los = np.linspace(np.min(t_cube.coord('longitude').points), np.max(t_cube.coord('longitude').points), longs)
# depth = iris.coords.DimCoord(ds, standard_name='depth', units='m',attributes = t_cube.coord('depth').attributes)
# longitude = iris.coords.DimCoord(los, standard_name='longitude', units='degrees')
# tmp_cube2 = iris.cube.Cube(np.zeros((len(ds),len(los)), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(depth,0),  (longitude, 1)])
#
# points=[]
# values = []
# for i0,i in enumerate(t_cube.coord('depth').points):
#     for j0,j in enumerate(t_cube.coord('longitude').points):
#         points.append((i,j))
#         values.append(t_cube[i0,loc5,j0].data)
#
#
# xi=[]
# for i0,i in enumerate(ds):
#     for j0,j in enumerate(los):
#         xi.append((i,j))
#
# from scipy.interpolate import griddata
# tmp_cube2.data =  np.reshape(griddata(points,values,xi),(len(ds),len(los)))
#
# ###end regridding temperature to lower resolution to allow contour plot###
#
# cs2=iplt.contour(tmp_cube2,np.linspace(-3,3,20),alpha=0.8,linewidths=0.2,colors='r')


plt.title = 'vertical section at '+str(lat_var)+'$^\circ$ latitude'
plt.ylim(300,0)
plt.xlim(-20,0)
plt.ylabel('depth (m)')
plt.xlabel('longitude ($^\circ$)')
# plt.xlim(65,85)


ax3 = plt.subplot2grid((2,2),(1,1))


cs = iplt.pcolormesh(t_cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
CB = plt.colorbar(cs, shrink=0.99, extend='both')
CB.set_label('temperature ($^{\circ}$C)')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs2=iplt.contour(s_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.2,colors='k')
plt.clabel(cs2,[34.5,34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels

plt.title = 'vertical section at '+str(lat_var)+'$^\circ$ latitude'
plt.ylim(300,0)
plt.xlim(-20,0)
plt.ylabel('depth (m)')
plt.xlabel('longitude ($^\circ$)')

plt.savefig('/home/ph290/Documents/figures/iceland/GIN_seas_lat_sections_'+str(lat_var)+'_d18O_t_closeup.png')
plt.show(block = False)
"""


lat_var = 74.79
loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
plt.close('all')
cs = iplt.pcolormesh(cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
# CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
# CB=plt.colorbar(cs)
CB = plt.colorbar(cs, shrink=0.99, extend='both')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs2=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
plt.clabel(cs2,[34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels
# plt.clabel(cs2,inline=True, fontsize=10)

# plt.colorbar(cs2)

# iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
iplt.title = str(lat_var)
plt.ylim(1000,0)
plt.xlim(-25,15)
# plt.xlim(65,85)

# plt.savefig('/home/ph290/Documents/figures/GIN_seas_lat_sections_'+str(lat_var)+'_temperature.png')
plt.show(block = False)

#
# latitudes = np.linspace(65,80,50)
# cube  = d18O_cube
#
# for i,lat_var in enumerate(latitudes):
#     loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
#     plt.close('all')
#     iplt.pcolormesh(cube[:,loc5,:],cmap = 'viridis',vmin = 0,vmax=5)
#     # CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
#     # plt.clabel(CS, inline=1, fontsize=10)
#     # qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
#     # CS=iplt.contour(density_cube[:,loc5,:],20,alpha=0.8,colors='k',linewidths=0.5)
#     # iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
#     iplt.title = str(lat_var)
#     plt.ylim(1000,0)
#     # plt.xlim(65,85)
#     plt.savefig('/home/ph290/Documents/figures/GIN_seas_lat_sections_'+str(lat_var)+'_d18O.png')
#     # plt.show(block = True)
"""

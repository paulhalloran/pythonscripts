import matplotlib.pyplot as plt
import numpy as np
import scipy
import numpy
import netCDF4
import csv

from numpy import arange, dtype

v1 = []
v2 = []
v3 = []
v4 = []
v5 = []
v8 = []
v9 = []

#0=section
#1=stations   integer
#2=cruise
#3=longitude   float
#4=latitude   float
#5=month
#6=day
#7=year
#8=pressure   float
#9=depth   integer
#10=tco2   float

f = open('discrete-November-06-15-05-34-23-AM.csv', 'r').readlines()

for line in f[1:]:
	fields = line.split(',')
	v1.append(fields[0]) #section
	v2.append(fields[1]) #station
	v3.append(fields[2]) #cruise
	v4.append(np.round(float(fields[3]))) #long
	v5.append(np.round(float(fields[4]))) #lat
	v8.append(fields[9]) #depth
	v9.append(np.float(fields[10])) #tco2

v9 = np.array(v9)
loc = np.where(v9 == -999.)
v9[loc] = np.nan

lon_list = np.arange(360)
lat_list = np.arange(180)

out_array = np.empty([360,180])
out_array[:] = np.nan

for lon in lon_list:
	print lon
	for lat in lat_list:
		location =  np.array(np.where((v4 == lon) & (v5 == lat))[0])
		if np.size(location) > 0:		
			out_array[lon,lat] = np.nanmean(v9[location])


plt.pcolormesh(out_array,vmin=2000, vmax=2400)
plt.show()


import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

def running_mean(x, N):
	y = np.zeros((len(x),))
	for ctr in range(len(x)):
		y[ctr] = np.sum(x[(ctr-np.round(N/2)):ctr+np.round(N/2)])
	out = y/N
	out[0:np.round(N/2)] = np.NAN
	out[-1.0*np.round(N/2)::] = np.NAN
	return out

data_file = '/home/ph290/misc_data/leadlag.txt'

data = np.genfromtxt(data_file,delimiter=',',skip_header=142)

x = data[:,1][::-1] 
y = data[:,2][::-1] 
#reversing because data counts down from present back in time!

smoothings = (np.arange(400))+1
lags = (np.arange(601))-300

output = np.zeros([np.size(smoothings),np.size(lags)])
output.fill(np.NAN) 

for i,smooth in enumerate(smoothings):
	print i
	tmp_x = running_mean(x,smooth)
	tmp_y = running_mean(y,smooth)
	loc = np.logical_not(np.isnan(tmp_x))
	tmp_x = tmp_x[loc]
	tmp_y = tmp_y[loc]
	for j,lag in enumerate(lags):
		if lag > 0: 
			tmp_x_2 = tmp_x[:lag*(-1)]
			tmp_y_2 = tmp_y[lag::]
		if lag < 0: 
			lag2 = lag * (-1.0)
			tmp_x_2 = tmp_x[lag2::]
			tmp_y_2 = tmp_y[:lag2*(-1)]
		if lag == 0:
			tmp_x_2 = tmp_x
			tmp_y_2 = tmp_y
		slope, intercept, r_value, p_value, std_err = stats.linregress(tmp_x_2,tmp_y_2)
		if p_value < 0.001:
			output[i,j] = r_value**2 

#note, a positive lag is x following y, i.e. column 2 following column 3, i.e. tree-ring following d18O

plt.contourf(lags,smoothings,output,101)
plt.xlabel('Lag (years)')
plt.ylabel('running mean smoothing window (years)')
cb = plt.colorbar()
cb.set_label('R$^2$ (where p-value < 0.001)')
plt.savefig('/home/ph290/Documents/figures/dave_plot3.png')

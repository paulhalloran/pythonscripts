
import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
from cdo import *
cdo = Cdo()
import uuid
import subprocess
import glob
import os
import string

temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'
"""""""""""""""""
|||||||||||||||||
"""""""""""""""""
# model_name = 'IPSL-CM5A-LR'
# folder_name = 'ipsl-cm5a-lr'
# run = 'historical'
model_name = 'IPSL-CM5A-LR'
folder_name = 'ipsl-cm5a-lr'
run = 'rcp85'
variable = 'uas'
variable_std_name = 'eastward_wind'
variables_time_freqs = ['day']
i=0
"""""""""""""""""
|||||||||||||||||
"""""""""""""""""
intermediate_dir_and_file = '/data/NAS-ph290/shared/simulation_forced_cmip5/'+folder_name+'/merged_and_1x1_regridded/'+model_name+'_'+variable+'_'+run+'.nc'
file_to_regrid = intermediate_dir_and_file
input_directory = '/data/NAS-ph290/shared/simulation_forced_cmip5/'+folder_name+'/'

files1 = glob.glob(input_directory+variable+'_'+variables_time_freqs[i]+'*'+run+'*.nc')
sample_cube = iris.load_raw(files1[0],variable_std_name)[0]

variable_name = sample_cube.var_name.encode('ascii', 'ignore')
variable_long_name = sample_cube.standard_name.encode('ascii', 'ignore')
unit_info = str(sample_cube.units)
time_coordinate_name = sample_cube.coord(dimensions=0).standard_name.encode('ascii', 'ignore')
timestep_description = str(int((sample_cube.coord(dimensions=0).points[1] - sample_cube.coord(dimensions=0).points[0])))+' days'

#u-winds note - vector fields need tp be treated separately. See: Example 4 in http://sosie.sourceforge.net/
f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_sample_file','r')
contents = f.read()
f.close()

file_to_regrid = intermediate_dir_and_file

contents = contents.replace("file_to_regrid","'"+file_to_regrid+"'")
contents = contents.replace("variable_name","'"+variable_name+"'")
contents = contents.replace("variable_long_name","'"+variable_long_name+"'")
contents = contents.replace("unit_info","'"+unit_info+"'")
contents = contents.replace("time_coordinate_name","'"+time_coordinate_name+"'")
contents = contents.replace("timestep_description","'"+timestep_description+"'")
contents = contents.replace("extra_text","'"+model_name+'_'+run+"'")
contents = contents.replace("directory_and_info","'"+variable_name+"'")

f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file','w')
f.write(contents)
f.close()



filetest4 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/output_forcing_files/u10_1x1-deg-ORCA1_'+model_name+'_'+run+'.nc4'))
if filetest4 == 0:
        print 'final step on vector variables'
        filetest2 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/uraw_*'+model_name+'_'+run+'.nc4'))
        filetest3 = np.size(glob.glob('/data/NAS-ph290/shared/simulation_forced_cmip5/vraw_*'+model_name+'_'+run+'.nc4'))
        if (filetest2 > 0) & (filetest3 > 0):
                # Final step in vector regridding (for u and v)
                subprocess.call('/home/ph290/src/sosie-2.6.4/bin/corr_vect.x -x u10 -y v10 -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_vector_x_temporary_file -m /home/ph290/Documents/python_scripts/SOSIE/mesh_mask_ORCA1_light.nc', shell=True)

import iris
from eofs.iris import Eof
import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import iris.coord_categorisation
import numpy as np
import os
import iris.analysis
import iris.analysis.stats
from scipy import signal
import scipy
import shelve



def intersect(a, b):
    return list(set(a) & set(b))


def model_names(directory,variable,experiment):
        files = glob.glob(directory+'/*'+variable+'_'+experiment+'*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models

def cube_mean(in_cube_list):
    no_cubes = np.size(in_cube_list)
    cube_shape = np.shape(in_cube_list[0])
    out_data = np.zeros([no_cubes,cube_shape[0],cube_shape[1]])
    for i,cube in enumerate(in_cube_list):
        out_data[i] = cube.data
    out_cube = in_cube_list[0].copy()
    out_cube.data = np.mean(out_data,axis=0)
    return out_cube


def cube_std(in_cube_list):
    no_cubes = np.size(in_cube_list)
    cube_shape = np.shape(in_cube_list[0])
    out_data = np.zeros([no_cubes,cube_shape[0],cube_shape[1]])
    for i,cube in enumerate(in_cube_list):
        out_data[i] = cube.data
    out_cube = in_cube_list[0].copy()
    out_cube.data = np.std(out_data,axis=0)
    return out_cube

"""

directory1 = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/'
directory2 = '/data/NAS-ph290/ph290/cmip5/odd_helga/regridded/'


models1 = model_names(directory1,'msftbarot','piControl')
models2 = model_names(directory2,'intpp','piControl')
models = intersect(models1,models2)

models.remove('GFDL-ESM2M')
models.remove('GFDL-ESM2G')

correlation_cubes=[]
eof1_cubes=[]

no_years = []

for model in models:
# model = models[0]
    ############
    #read in barotropic stream function
    ############
    cube_barotropic = iris.load_cube(directory1+model+'_*msftbarot_piControl_r1i1p1*.nc')

    iris.coord_categorisation.add_year(cube_barotropic, 'time', name='year')
    cube_barotropic = cube_barotropic.aggregated_by('year', iris.analysis.MEAN)

    cube_barotropic.data = scipy.signal.detrend(cube_barotropic.data, axis=0)

    coord = cube_barotropic.coord('time')
    dt = coord.units.num2date(coord.points)
    years1 = np.array([coord.units.num2date(value).year for value in coord.points])

    ############
    #read in intpp
    ############

    cube_intpp = iris.load_cube(directory2+model+'_*intpp_piControl_r1i1p1*.nc')
    iris.coord_categorisation.add_year(cube_intpp, 'time', name='year')
    cube_intpp = cube_intpp.aggregated_by('year', iris.analysis.MEAN)

    cube_intpp.data = scipy.signal.detrend(cube_intpp.data, axis=0)

    coord = cube_intpp.coord('time')
    dt = coord.units.num2date(coord.points)
    years2 = np.array([coord.units.num2date(value).year for value in coord.points])

    ############
    #find common years
    ############


    years = intersect(years1,years2)
    year_locations1 = np.in1d(years1, years)
    year_locations2 = np.in1d(years2, years)
    no_years.append(np.size(year_locations1))

    cube_barotropic = cube_barotropic[year_locations1]
    cube_intpp = cube_intpp[year_locations2]

    ############
    #Calculate 1st EOF of barotropic stream function
    ############


    lon_west = -90.0
    lon_east = 5.0
    lat_south = 0.0
    lat_north = 80.0

    cube_barotropic = cube_barotropic.intersection(longitude=(lon_west, lon_east))
    cube_barotropic = cube_barotropic.intersection(latitude=(lat_south, lat_north))


    solver = Eof(cube_barotropic, weights='coslat')
    #eofs = solver.eofs(neofs=5)

    eof1 = solver.eofsAsCorrelation(neofs=1)[0]
    pc1 = solver.pcs(npcs=1, pcscaling=1)

    #intergyre gyre is eofs[0]
    '''
    plt.close('all')
    qplt.contourf(eof1,31)
    plt.gca().coastlines()
    plt.title('eof1')
    plt.show(block = True)
    '''
    #qplt.plot(pc1[:,0]))
    #plt.show()

    ############
    #Calculate correlation with PC1 and intpp
    ############


    ts = pc1[:,0].data
    cube = cube_intpp.copy()
    cube = iris.analysis.maths.multiply(cube, 0.0)
    ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
    cube = iris.analysis.maths.add(cube, ts2)
    out_cube = iris.analysis.stats.pearsonr(cube_intpp, cube, corr_coords=['time'])

    eof1_cubes.append(eof1)
    correlation_cubes.append(out_cube)

    '''
    plt.close('all')
    qplt.contourf(out_cube,31)
    plt.gca().coastlines()
    plt.title('intpp correlation')
    plt.show(block = True)
    '''


filename='/data/dataSSD1/ph290/tmp2/shelve2.out'
my_shelf = shelve.open(filename,'n') # 'n' for new

for key in dir():
    try:
        my_shelf[key] = globals()[key]
    except TypeError:
        #
        # __builtins__, my_shelf, and imported modules can not be shelved.
        #
        print('ERROR shelving: {0}'.format(key))


my_shelf.close()

#########

#########

filename='/data/dataSSD1/ph290/tmp2/shelve2.out'
my_shelf = shelve.open(filename)
for key in my_shelf:
    globals()[key]=my_shelf[key]
my_shelf.close()

"""

correlation_cubes2 = correlation_cubes[0:2]+correlation_cubes[3::]
eof1_cubes2 = eof1_cubes[0:2]+eof1_cubes[3::]

correlation_mean = cube_mean(correlation_cubes2)
eof1_cubes_mean = cube_mean(eof1_cubes2)


correlation_std = cube_std(correlation_cubes2)
eof1_cubes_std = cube_std(eof1_cubes2)

#
# for eof1 in eof1_cubes2:
# 	plt.close('all')
# 	qplt.contourf(eof1,31)
# 	plt.gca().coastlines()
# 	plt.title('eof1')
# 	plt.show(block = True)



plt.close('all')
qplt.contourf(correlation_mean,31)
qplt.contour(correlation_std,10)
plt.gca().coastlines()
plt.title('correlation eof1 with PP')
plt.show(block = True)





import iris.plot as iplt
import cartopy
import matplotlib.gridspec as gridspec

#
# plt.close('all')
# # proj = ccrs.PlateCarree(central_longitude=-20, central_latitude=60)
# proj = ccrs.PlateCarree()
# ax = plt.axes(projection=proj)
# ax.coastlines()
# # ax.set_global()
# ax.set_extent((-80.0, 20.0, 10.0, 80.0))
# iplt.contourf(correlation_mean, np.linspace(-0.4,0.4,31),cmap=plt.cm.RdBu_r)
# plt.colorbar()
# CS = iplt.contour(correlation_std, [0.10,0.2,0.3],colors='k')
# plt.clabel(CS, inline=1, fontsize=10)
# ax.set_title('correlation: intergyre-gyre with\nintegrated primary productivity', fontsize=16)
# ax.add_feature(cartopy.feature.LAND, facecolor='#d3d3d3', edgecolor='black')
# plt.show(block = False)
#


plt.close('all')
fig = plt.figure(figsize = (4,12))

gs = gridspec.GridSpec(100,100,bottom=0.1,left=0.1,right=0.9)
offset=4

for i,correlation_cube in enumerate(correlation_cubes):
	# proj = ccrs.PlateCarree(central_longitude=-20, central_latitude=60)
	ax = plt.subplot(gs[i*offset+13*(i):i*offset+13*(i+1),0:100],projection=ccrs.PlateCarree())
	ax.coastlines()
	# ax.set_global()
	ax.set_extent((-80.0, 20.0, 10.0, 80.0))
	iplt.contourf(correlation_cube, np.linspace(-0.7,0.7,31),cmap=plt.cm.RdBu_r)
	plt.colorbar()
# 	CS = iplt.contour(correlation_std, [0.10,0.2,0.3],colors='k')
# 	plt.clabel(CS, inline=1, fontsize=10)
	ax.set_title(models[i]+' correlation: intergyre-gyre with\nintegrated primary productivity ('+str(no_years[i])+' years)', fontsize=10)
	ax.add_feature(cartopy.feature.LAND, facecolor='#d3d3d3', edgecolor='black')
	ax.set_aspect('auto')


plt.savefig('/home/ph290/Documents/figures/intpp_inter_gyre_gyre.png')
plt.show(block = False)

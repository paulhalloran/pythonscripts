
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
from scipy.stats.stats import pearsonr
###
#Filter
###

# N=5.0
# #N is the order of the filter - i.e. quadratic
# timestep_between_values=1.0 #years value should be '1.0/12.0'
# low_cutoff=100.0
#
# Wn_low=timestep_between_values/low_cutoff
#
# b, a = scipy.signal.butter(N, Wn_low, btype='high')

def butter_bandpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

#b, a = butter_bandpass(1.0/100.0, 1)


# N=5.0
# #N is the order of the filter - i.e. quadratic
# timestep_between_values=1.0 #years value should be '1.0/12.0'
# low_cutoff=100.0
#
# Wn_low=low_cutoff/(timestep_between_values * 0.5)
#
# b, a = scipy.signal.butter(N, Wn_low, btype='high')


b, a = butter_bandpass(1.0/20.0, 1.0,2)






# def butter_bandpass(lowcut, fs, order=5):
#     nyq = fs
#     low = lowcut/nyq
#     b, a = scipy.signal.butter(order, low , btype='high',analog = False)
#     return b, a
#
# b, a = butter_bandpass(1.0/100.0, 1,7)
#
#
# b, a = scipy.signal.butter(2, 0.01 , btype='high')

start_year = 953
#temporarily changed because HadGEM files are not complete - need to reprocess
# start_year = 1049
end_year = 1849

expected_years = start_year+np.arange((end_year-start_year)+1)

##########################################
# solar
##########################################

m_data_file = '/data/NAS-geo01/ph290/misc_data/solar_cmip5/tsi_VK.txt'
m_data = np.genfromtxt(m_data_file,skip_header = 4,delimiter = ' ')
tmp = m_data[:,1]
loc = np.where((np.logical_not(np.isnan(tmp))) & (m_data[:,0] >= start_year) & (m_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = m_data[loc[0],0]
solar_data = tmp
solar_year = tmp_yr


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
bivalve_data_initial = tmp
bivalve_year = tmp_yr[::-1] # reverse because original d18O data has time starting from the present day
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial[::-1])



############
# plotting
############

plt.close('all')
x = np.linspace(-np.pi, np.pi, 12)[0:11]
x = np.tile(x,130)
x1 = x[0:897]
y1 = np.sin(x1)
y1 = np.roll(y1,7)
# plt.plot(np.arange(0,897), np.sin(x1))
# plt.xlabel('Angle [rad]')
# plt.ylabel('sin(x)')
# plt.axis('tight')
# plt.show()

# x = scipy.signal.filtfilt(b, a, solar_data)
# y = scipy.signal.filtfilt(b, a, bivalve_data_initial * -1.0)


x = solar_data
y = bivalve_data_initial * -1.0

plt.close('all')

plt.plot(x)
plt.show(block = True)


#
# plt.close('all')
# plt.plot(x,'b')
# plt.plot(y,'r')
# plt.show(block = True)
#


tmp = np.zeros(15)
tmp2 = np.zeros(15)
for i in range(-7,8):
    r = pearsonr(y1,np.roll(y,i))[0]
    tmp[i+7] = r
    r2 = pearsonr(x,np.roll(y,i))[0]
    tmp2[i+7] = r2

plt.close('all')

plt.plot(range(-7,8),tmp,'r',label = 'lagged correlation with idealised 11 year sine curve')
plt.plot(range(-7,8),tmp2,'b',label = 'lagged correlation with VK solar reconstruction')

x = scipy.signal.filtfilt(b, a, solar_data)
y = scipy.signal.filtfilt(b, a, bivalve_data_initial * -1.0)



tmp = np.zeros(15)
tmp2 = np.zeros(15)
for i in range(-7,8):
    r = pearsonr(y1,np.roll(y,i))[0]
    tmp[i+7] = r
    r2 = pearsonr(x,np.roll(y,i))[0]
    tmp2[i+7] = r2

plt.plot(range(-7,8),tmp,'r--',label = 'lagged correlation with idealised 11 year sine curve (high-pass filtered, 20yr)')
plt.plot(range(-7,8),tmp2,'b--',label = 'lagged correlation with VK solar reconstruction (high-pass filtered, 20yr)')

plt.legend(loc=0)

leg = plt.gca().get_legend()
ltext  = leg.get_texts()  # all the text.Text instance in the legend
llines = leg.get_lines()  # all the lines.Line2D instance in the legend
frame  = leg.get_frame()  # the patch.Rectangle instance surrounding the legend

# frame.set_facecolor('0.80')      # set the frame face color to light gray
plt.setp(ltext, fontsize='small')    # the legend text fontsize
plt.setp(llines, linewidth=1.5)      # the legend linewidth

plt.ylim([-0.2,0.1])
plt.xlabel('lag (years)')
plt.ylabel('r')
plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/solar_corr.png')
plt.show(block = True)

################




from matplotlib import colors as mcolors


maxn = 900
no_steps = 10
g0 = np.linspace(0,maxn,no_steps)
g1 = g0 + maxn/no_steps



for j in np.arange(np.size(g0)):

    x = scipy.signal.filtfilt(b, a, solar_data)
    y = scipy.signal.filtfilt(b, a, bivalve_data_initial * -1.0)

    x = x[g0[j]:g1[j]]
    y = y[g0[j]:g1[j]]

    tmp = np.zeros(15)
    tmp2 = np.zeros(15)
    for i in range(-7,8):
        r2 = pearsonr(x,np.roll(y,i))[0]
        tmp2[i+7] = r2

    plt.plot(range(-7,8),tmp2,color=plt.cm.copper((j*1.0)/np.size(g0)),lw=2,label = np.str(j))


plt.legend(loc=0)

leg = plt.gca().get_legend()
ltext  = leg.get_texts()  # all the text.Text instance in the legend
llines = leg.get_lines()  # all the lines.Line2D instance in the legend
frame  = leg.get_frame()  # the patch.Rectangle instance surrounding the legend

# frame.set_facecolor('0.80')      # set the frame face color to light gray
plt.setp(ltext, fontsize='small')    # the legend text fontsize
plt.setp(llines, linewidth=1.5)      # the legend linewidth

plt.ylim([-0.5,0.5])
plt.xlabel('lag (years)')
plt.ylabel('r')
plt.tight_layout()
plt.title('phase as stepping through millennium')
plt.savefig('/home/ph290/Documents/figures/solar_corr2.png')
plt.show(block = False)

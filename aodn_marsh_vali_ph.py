import iris
import glob
import datetime
from datetime import date, timedelta
import pandas as pd

#read in your list of files with glob
# path = "/Users/ph290/Downloads/jen/*.nc"
path = "/data/NAS-geo01/jm953/AODN/*.nc"
#We need to read in the list of files before we use the file names Try typing this to see what is heald i the variable moor_file to get a feel for how it works
moor_file = glob.glob(path)

surface_depth = 10 # meters below which we consider an observation to be at the surface
bottom_depth = 10 # meters, distance from teh sea bed which we consider an observation to be at the bottom

#make an empty dictionary to hold the data as you read it in
aodn_moorings = {}
start = date(1950,1,1)  # cos in days since the argo reference time (00.00.00 1950)


#loop through the files you've identified with glob
    #read in the file
    #read in the depth, time, lat, lon and data
    #put that data in the dictionary
for x in moor_file:  #NOTE for the time beiong, just looking at teh first 200 files to speed things up.
    try:
        temp_cube = iris.load_cube(x, 'sea_water_temperature')
        depth = temp_cube.attributes['instrument_nominal_depth']
        site_depth = temp_cube.attributes['site_nominal_depth'] # depth of seafloor
        if ((depth < surface_depth) | (site_depth-depth < bottom_depth)): # This is saying, that of the depth of teh instriument is shallower than the defined 'surface' depth OR the depth is close to teh bottom, then carry on within the loop
            # print depth
            # print site_depth-depth
            latitude = temp_cube.attributes['geospatial_lat_max']
            longitude = temp_cube.attributes['geospatial_lon_max']
            mooring_name = temp_cube.attributes['platform_code']
            time = temp_cube.coord('time').points
            # Here I've just taken the file name (i.e. taken the whole path and file name, split this where there are forward slashes, then taken the bit of text after the last forward slash) We can then use this as a unique name for each mooring
            #We need to make another level in teh dictionary hierachy here, because there is not just one entry for each mooring name, there is another list of things you can look up. Try playing round with dictionaries in python opn the command line to see how they work.
            aodn_moorings[mooring_name] = {}
            # aodn_moorings[mooring_name]['temperature'] = temp_cube.data
            # aodn_moorings[mooring_name]['depth'] = depth
            aodn_moorings[mooring_name]['time'] = [start+timedelta(t) for t in time]
            aodn_moorings[mooring_name]['longitude'] = longitude
            aodn_moorings[mooring_name]['latitude'] = latitude
            if depth < surface_depth:
                aodn_moorings[mooring_name]['surface_temp'] = temp_cube.data
            if depth-site_depth < bottom_depth:
                aodn_moorings[mooring_name]['bottom_temp'] = temp_cube.data
            aodn_moorings[mooring_name]['site_depth'] = site_depth
    except:
        print 'error with file - probably that required variables are missing'




#example of how to loop through the dictionary to get things out for each mooring

for key in aodn_moorings:
    # print key
    # if ('surface_temp' in key):
    #     print aodn_moorings[key]['surface_temp']
    if (('surface_temp' in key) & ('bottom_temp' in key)):
        plt.plot(aodn_moorings[key]['time'],aodn_moorings[key]['surface_temp'] - aodn_moorings[key]['bottom_temp'])



data = pd.DataFrame(aodn_moorings)

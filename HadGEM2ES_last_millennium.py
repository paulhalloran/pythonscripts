import iris
import glob
import subprocess

print 'NOTE! This can be fun directly on JASMIN - try: jasmin-sci3.ceda.ac.uk (2TB RAM)'
#Using JASMIN. Disk space: http://www.jasmin.ac.uk/how-to-use-jasmin/group-workspaces/working-with-data/


directory = '/data/NAS-ph290/ph290/from_monsoon/'
output_directory = '/data/NAS-ph290/ph290/cmip5/hadgem2_last_mill_netcdf/'
files = glob.glob(directory+'*.pp')

#######################################
#REMOVE THIS BIT!!!!!!!!!!!!!!!!!!!!
#######################################
files = ['/data/NAS-ph290/ph290/from_monsoon/xhinho.pm18apr.pp','/data/NAS-ph290/ph290/from_monsoon/xhinho.pm18may.pp']
#######################################
#REMOVE THIS BIT!!!!!!!!!!!!!!!!!!!!
#######################################

cube = iris.load_cube(files,'sea_water_potential_temperature')
print 'will also have to merge files from diff job ID, I guess this will not manage that!'
cube.convert_units('K')
cube.var_name = 'thetao'
newCoord = cube.coord('depth')
cube.remove_coord('depth')
cube.remove_coord('model_level_number')
cube.add_dim_coord(newCoord, 1)
subprocess.call('rm '+output_directory+'thetao_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', shell = True)
iris.fileformats.netcdf.save(cube, output_directory+'thetao_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', netcdf_format='NETCDF4')

cube = iris.load_cube(files,'sea_water_salinity')
print 'will also have to merge files from diff job ID, I guess this will not manage that!'
cube += 0.035
cube *= 1000.0
cube.units = 'unknown'
cube.var_name = 'so'
newCoord = cube.coord('depth')
cube.remove_coord('depth')
cube.remove_coord('model_level_number')
cube.add_dim_coord(newCoord, 1)
subprocess.call('rm '+output_directory+'so_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', shell = True)
iris.fileformats.netcdf.save(cube, output_directory+'so_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', netcdf_format='NETCDF4')

cube = iris.load_cube(files,'northward_sea_water_velocity')
print 'will also have to merge files from diff job ID, I guess this will not manage that!'
cube.standard_name = 'sea_water_y_velocity'
cube.var_name = 'vo'
cube.convert_units('m s-1')
newCoord = cube.coord('depth')
cube.remove_coord('depth')
cube.remove_coord('model_level_number')
cube.add_dim_coord(newCoord, 1)
# cube.coord('model_level_number').standard_name = 'depth'
subprocess.call('rm '+output_directory+'vo_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', shell = True)
iris.fileformats.netcdf.save(cube, output_directory+'vo_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', netcdf_format='NETCDF4')

cube = iris.load_cube(files,'surface_air_pressure')
print 'will also have to merge files from diff job ID, I guess this will not manage that!'
cube.standard_name = 'air_pressure_at_sea_level'
cube.convert_units('Pa')
cube.var_name = 'psl'
subprocess.call('rm '+output_directory+'psl_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', shell = True)
iris.fileformats.netcdf.save(cube, output_directory+'psl_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', netcdf_format='NETCDF4')

print 'now regrid with regridding_and_concatenating_cmip5_files_vo_hadgem2.py and regridding_and_concatenating_cmip5_files_others_hadgem2.py'
print 'then run HadGEM2ES_amoc.py'
print 'then get script sorted so can calculate MLDs and densities...'

### NOTE While I think the logic is correct here, there are a few things I'm not 100% sure about how they behave so it needs testing
### I've also just based this on my interpretation of Pete M's DHW description below, which is out of context is slightly ambiguous, so I would want to check that my assumptinos are correct.

# Degree Heating Week Calculations
# Maximum Monthly Mean (MMM) climatology - This climatology, derived from the Polar-orbiting Operational Environmental Satellite (POES) Advanced Very High Resolution Radiometer (AVHRR) SSTs for the period 1985-1993, is static in time but varies in space (Strong et al.,
#  1997) (Table 2). (Mumby, Skirving et al., 2004)
#
# Degree Heating Week (DHW) represents the accumulation of HotSpots for a given location, over a rolling 12-week time period (see Fig. 2). Preliminary indications show that a HotSpot value of less than one degree is insufficient to cause visible stress on corals. Consequently,
#  only HotSpot values P1 C are accumulated (i.e. if we have consecutive HotSpot values of 1.0, 2.0, 0.8 and 1.2, the DHW value will be 4.2 because 0.8 is less than one and therefore does not get used). One DHW is equivalent to one week of HotSpot levels staying
#  at 1 C or half a week of HotSpot levels at 2 C, and so forth. (Mumby, Skirving et al., 2004)


import iris
import iris.coord_categorisation
import numpy as np
import matplotlib.pyplot as plot
import iris.quickplot as qplt



def broadcasting_mean(a, L, S ):  # Window len = L, Stride len/stepsize = S
    nrows = ((a.shape[0]-L)//S)+1
    out = a[S*np.arange(nrows)[:,None] + np.arange(L)]
    return np.mean(out,axis=1)


def broadcasting_sum(a, L, S ):  # Window len = L, Stride len/stepsize = S
    nrows = ((a.shape[0]-L)//S)+1
    out = a[S*np.arange(nrows)[:,None] + np.arange(L)]
    return np.sum(out,axis=1)



directory = '/data/NAS-ph290/ph290/s2p3_rv2.0_output/'

climatology_file = directory+'surface_temperature_iceland_site_1950.nc' # the example here uses just 1 year, but you would want a file that contains all of the years required for teh climatology

full_dataset_file = directory+'surface_temperature_iceland_site.nc'


#################################
# Maximum Monthly Mean (MMM) climatology
#################################

clim_cube = iris.load_cube(climatology_file)

#convert daily data to monthly meaned data
iris.coord_categorisation.add_month_number(clim_cube, 'time', name='month_number')
clim_cube = clim_cube.aggregated_by('month_number', iris.analysis.MEAN)

#collapse the months together, taking the maximum value at each lat-lon grid square
mmm_climatology = clim_cube.collapsed('time',iris.analysis.MAX)

#################################
# Degree Heating Weeks
#################################

#Read in the dataset
cube = iris.load_cube(full_dataset_file)


#subtract the climatology values
#note the next two lines are just needed because the cubes I read in are a little different heer (one had been processed with cdo, one not. I'll leave this in, because it does no harm, other than complicating things a little to understand!)
mmm_climatology_2 = cube[0]
mmm_climatology_2.data = mmm_climatology.data
cube_anomalies = cube - mmm_climatology_2

#set all anomaly values less than or equal to one to zero (i.e. cool periods do not offset warm periods as I understand it)
# Preliminary indications show that a HotSpot value of less than one degree is insufficient to cause visible stress on corals. Consequently, only HotSpot values less or equal to 1 K are accumulated (i.e. if we have consecutive HotSpot values of 1.0, 2.0, 0.8 and 1.2, the DHW value will be 4.2 because 0.8 is less than one and therefore does not get used)
cube_anomalies.data[np.where(cube_anomalies.data <= 1.0)] = 0.0

##
#mean from daily to weekly values
##

#1st subset the data so that it starts on the 1st Monday and send on the last Sunday (so we have full weeks)
iris.coord_categorisation.add_weekday_number(cube_anomalies, 'time', name='weekday_number')
mondays = np.where(cube_anomalies.coord('weekday_number').points == 0)[0]
sundays = np.where(cube_anomalies.coord('weekday_number').points == 6)[0]
cube_anomalies = cube_anomalies[mondays[0]:sundays[-1]]

#Make a new cube to hold the weekly data from just the monday records from the full dataset
cube_anomalies2 = cube_anomalies[np.where(cube_anomalies.coord('weekday_number').points == 0)[0]]

#Do the weekly meaning
cube_anomalies2.data = broadcasting_mean(cube_anomalies.data, 7, 7 )

####
# calculate DHW using 12 week rolling window
#Sum values over a 12-week rolling window
####
window = 12
DHW_cube = cube_anomalies2[0:-(window-1)].copy()
DHW_cube.data = broadcasting_sum(cube_anomalies2.data, window, 1 )

#getting datetime data from cube for x-axis
datetimes = [cell.point for cell in DHW_cube.coord('time').cells()]

#spatially averaging each week of data for the y-axis
y = DHW_cube.collapsed(['latitude','longitude'],iris.analysis.MEAN).data

plt.close('all')
plt.plot(datetimes,y)
plt.xlabel('date')
plt.ylabel('DHW')
plt.show()

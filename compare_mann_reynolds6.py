print 'Tidy this up and put in teh format that I produced previously - this is the one to use...'

import numpy as np
import matplotlib.pyplot as plt
import running_mean as rm
import running_mean_post as rmp
import scipy
import scipy.signal
import scipy.stats as stats
import matplotlib.gridspec as gridspec
import pickle
import os
from timer import Timer
import pandas




start_year = 953
# start_year = 1350
# start_year = 1574
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


############
# Files and read in data
############

amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
# amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)

g_amo_file = '/data/NAS-geo01/ph290/misc_data/gray_2004/amo-gray2004.txt'
g_amo = np.genfromtxt(g_amo_file, skip_header = 102, skip_footer = 8)

r_data_file = '/data/NAS-geo01/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')

window_type = 'boxcar'

#############
# Mann AMO
#############
amo_yr = amo[:,0].copy()
amo_data = amo[:,1].copy()
# 	amo_yr = amo_yr[6::]
# 	amo_data = amo_data[6::]
loc = np.where((amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data = scipy.signal.detrend(amo_data, axis=0)

"""
#############
# GRAY AMO
#############
g_amo_yr = g_amo[:,0].copy()
g_amo_data = g_amo[:,1].copy()
# 	amo_yr = amo_yr[6::]
# 	amo_data = amo_data[6::]
loc = np.where((g_amo_yr >= start_year) & (g_amo_yr <= end_year))
g_amo_yr = g_amo_yr[loc]
g_amo_data = g_amo_data[loc]
g_amo_data = scipy.signal.detrend(g_amo_data, axis=0)
"""

#############
# Bivalve
#############
shift = 10
smoothing2 = 10
bivalve_yr = r_data[:,0].copy()
bivalve_data = r_data[:,1].copy()
bivalve_yr = bivalve_yr[::-1]
bivalve_data = bivalve_data[::-1]
loc = np.where((bivalve_yr >= start_year) & (bivalve_yr <= end_year))
bivalve_yr = bivalve_yr[loc]
bivalve_data = bivalve_data[loc]
bivalve_data = np.roll(bivalve_data,shift)
bivalve_data[0:shift] = np.nan
bivalve_data[-1.0*shift::] = np.nan
bivalve_data = pandas.rolling_window(bivalve_data,smoothing2,win_type=window_type,center=True)



#############
# density
#############
density = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/n_ice_dens.txt',delimiter=',')
shift = 10
smoothing2 = 30
density_yr = density[:,0].copy()
density_data = density[:,1].copy()
loc = np.where((density_yr >= start_year) & (density_yr <= end_year))
density_yr = density_yr[loc]
density_data = density_data[loc]
density_data = np.roll(density_data,shift)
density_data[0:shift] = np.nan
density_data[-1.0*shift::] = np.nan
#density_data = scipy.signal.detrend(density_data, axis=0)
density_data = pandas.rolling_window(density_data,smoothing2,win_type=window_type,center=True)

#############
# AMOC
#############
AMOC = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt',delimiter=',')
# shift = 5
smoothing2 = 10
AMOC_yr = AMOC[:,0].copy()
AMOC_data = AMOC[:,1].copy()
loc = np.where((AMOC_yr >= start_year) & (AMOC_yr <= end_year))
AMOC_yr = AMOC_yr[loc]
AMOC_data = AMOC_data[loc]
AMOC_data = np.roll(AMOC_data,1 * shift)
# AMOC_data[0:shift] = np.nan
# AMOC_data[-1.0*shift::] = np.nan
#AMOC_data = scipy.signal.detrend(AMOC_data, axis=0)
AMOC_data = pandas.rolling_window(AMOC_data,smoothing2,win_type=window_type,center=True)


#############
# Volc. Crow.
#############
file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)
data3 = np.genfromtxt(file3)
data4 = np.genfromtxt(file4)
volc_smoothing = 15
# data_tmp = np.zeros([data1.shape[0],2])
# data_tmp[:,0] = data1[:,1].copy()
# data_tmp[:,1] = data2[:,1].copy()
data_tmp = np.zeros([data1.shape[0],4])
data_tmp[:,0] = data1[:,1].copy()
data_tmp[:,1] = data2[:,1].copy()
data_tmp[:,2] = data3[:,1].copy()
data_tmp[:,3] = data4[:,1].copy()
data = np.mean(data_tmp,axis = 1)
voln_n = data1.copy()
voln_n[:,1] = data
volc_data = voln_n[:,1]
volc_yr = voln_n[:,0]
loc = np.where((volc_yr >= start_year) & (volc_yr <= end_year))
volc_yr = volc_yr[loc]
volc_data = volc_data[loc]
tmp = np.round(volc_yr)
tmp_unique = np.unique(tmp)
volc_yr2 = tmp_unique
volc_data2 = tmp_unique.copy()
for count,yr in enumerate(volc_yr2):
	loc = np.where(tmp == yr)
	volc_data2[count] = np.mean(volc_data[loc])
#calculating volcanic forcing from AOD, following Harris and Highwood 2011
volc_data2 = -11.3 * (1 - np.exp(-0.164*volc_data2))
volc_data2 = pandas.rolling_window(volc_data2,volc_smoothing,win_type=window_type,center=True)

loc2 = np.where(np.logical_not((np.isnan(amo_data)) | (np.isnan(bivalve_data))))

loc2 = np.where(np.logical_not((np.isnan(amo_data)) | (np.isnan(bivalve_data))  | (np.isnan(density_data)) | (np.isnan(volc_data2))))

amo_yr = amo_yr[loc2]
amo_data = amo_data[loc2]
"""
g_amo_yr = g_amo_yr[loc2]
g_amo_data = g_amo_data[loc2]
"""
bivalve_yr = bivalve_yr[loc2]
bivalve_data = bivalve_data[loc2]
density_yr = density_yr[loc2]
density_data = density_data[loc2]
AMOC_yr = AMOC_yr[loc2]
AMOC_data = AMOC_data[loc2]
volc_yr2 = volc_yr2[loc2]
volc_data2 = volc_data2[loc2]

y1 = amo_data
y1 = scipy.signal.detrend(y1, axis=0)
"""
y1b = g_amo_data
y1b = scipy.signal.detrend(y1b, axis=0)*0.15
"""
y2 = bivalve_data
y2 = scipy.signal.detrend(y2, axis=0)
# y3 = density_data * 3.0
y3 = AMOC_data * 0.4e-8
# density_data -= np.min(density_data)
# y3 = density_data * 2.0
# * 5.0
y4 = volc_data2 * 4.0
# y4 -= np.mean(y4)
y5 = y3 + y4
# y5 -= np.mean(y5)
y5 = scipy.signal.detrend(y5, axis=0)
y3 = scipy.signal.detrend(y3, axis=0)
# y5 = scipy.signal.detrend(y5, axis=0)


from scipy.signal import butter, lfilter


def butter_lowpass(cutoff, order=5):
        cutoff = 1.0/cutoff
        b, a = butter(order, cutoff, btype='low', analog=False)
        return b, a

def butter_highpass(cutoff, order=5):
        cutoff = 1.0/cutoff
        b, a = butter(order, cutoff, btype='high', analog=False)
        return b, a

def butter_lowpass_filter(data, cutoff, order=5):
        b, a = butter_lowpass(cutoff, order=order)
        y = lfilter(b, a, data)
        cutoff = int(cutoff)
        y = np.roll(y,(cutoff/2) * -1)
        y[0:cutoff/2] = np.nan
        y[(cutoff/2) * -1::] = np.nan
        return y

def butter_highpass_filter(data, cutoff, order=5):
        b, a = butter_highpass(cutoff, order=order)
        y = lfilter(b, a, data)
        y[0:cutoff/2] = np.nan
        y[(cutoff/2) * -1::] = np.nan
        return y



order = 6
cutoff = 300
y1_2 = butter_highpass_filter(y1.copy(), cutoff, order)
y1_3 = y1 - y1_2

r_value2, p_value2 = stats.spearmanr(y1,y2)
r_value3, p_value2 = stats.spearmanr(y1,y3)
r_value4, p_value2 = stats.spearmanr(y1,y5)
#
# r_value2, p_value2 = stats.pearsonr(y1,y5)


print 'a r-value ',r_value2
print 'b r-value ',r_value3
print 'b r-value ',r_value4

#############
# Plotting
#############


plt.close('all')
plt.rc('legend',**{'fontsize':10})
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(15)

#gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)
#ax1 = plt.subplot(gs[0:30,0:100])
#ax2 = plt.subplot(gs[35:65:100,0:100])
#ax3 = plt.subplot(gs[70:100,0:100])

gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.1,right=0.95)
ax1 = plt.subplot(gs[0:100,0:100])

ax1.plot(amo_yr,y1,'b',lw = 2.0,label = 'Proxy reconstructed AMO index')
# ax1.plot(g_amo_yr,y1b,'y',lw = 2.0,label = 'Proxy reconstructed AMO index')

ax2 = ax1.twinx()
# ax2.plot(bivalve_yr,y2,'r',lw = 2,label = 'Bivalve d$^{18}$O')
ax2.plot(density_yr,y5,'g',lw = 2,label = 'volc+and...')
ax3 = ax1.twinx()
# ax3.plot(density_yr,y1_3,'y',lw = 1,label = 'density')
ax3.plot(density_yr,y3,'k',lw = 1,label = 'density')
# ax3.plot(density_yr,y1_3,'y',lw = 1,label = 'density')
# ax4 = ax1.twinx()
# ax4.plot(density_yr,y2,'r',lw = 1,label = 'bivalve')
ax1.annotate('r-value = '+np.str(np.around(r_value4,2))+', p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center')


ax1.set_ylabel('Normalised anomaly')

ax1.set_xlabel('Calendar year')

ax1.set_xlim([950,end_year])
# ax1.set_xlim([1300,end_year])
ax1.set_ylim([-0.6,0.6])
ax2.set_ylim([-0.4,0.4])
ax3.set_ylim([-0.4,0.4])
# ax4.set_ylim([-3.0,3.0])


# ax1.set_ylim([-0.1,1.1])

# plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/bivalve_AMO.png')
plt.show(block = False)

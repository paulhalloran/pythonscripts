import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
from cdo import *
cdo = Cdo()
#cdo.timmin(input = ifile,    output = ofile,  options = '-P 6')  #python version



#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models

'''
Defining directory locations, variable locatoins etc.
You may well need to edit the text within the quotation marks to adapt the script to work with your data
'''

'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/NAS-ph290/ph290/cmip5/past1000/'
#Directories where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/data1/ph290/cmip5/last_1000_density/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiment = 'past1000'
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.
time_period = 'Omon'

#lats and lons for analysis:
lon1 = -23.0
lon2 = -13.0
lat1 = 64.0
lat2 = 79.0

'''
Main bit of code follows...
'''


print '****************************************'
print '** this can take a long time (days)   **'
print '** grab a cuppa, but keep an eye on   **'
print '** this to make sure it does not fail **'
print '****************************************'

#This runs the function above to come up with a list of models from the filenames
models = model_names(input_directory)

ensemble = 'r1i1p1'

#Looping through each model we have
for model in models:
	print model
	print 'cleaning up temp files... note the script is over zealous about this because of it crashespart way through it can otherwise end upgetting confused.'
	try:
		subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
	except:
		pass
	try:
		subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
	except:
		pass
	try:
		subprocess.call('rm '+temporary_file_space1+temp_file3, shell=True)
	except:
		pass
	try:
		subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
	except:
		pass
	try:
		subprocess.call('rm '+temporary_file_space1+temp_file5, shell=True)
	except:
		pass
	try:
		subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
	except:
		pass
	#testing if the output file has alreday been created
	tmp = glob.glob(output_directory+model+'_annual_mean_sw_density_'+experiment+'_regridded.nc')
	temp_file1 = str(uuid.uuid4())+'.nc'
	temp_file2 = str(uuid.uuid4())+'.nc'
	temp_file3 = str(uuid.uuid4())+'.nc'
	temp_file4 = str(uuid.uuid4())+'.nc'
	temp_file5 = str(uuid.uuid4())+'.nc'
	temp_file6 = str(uuid.uuid4())+'.nc'
	#TOS
	files1 = glob.glob(input_directory+'thetao_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
	files2 = glob.glob(input_directory+'so_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
	if np.size(tmp) == 0:
		#reads in the files to process
		sizing1 = np.size(files1)
		sizing2 = np.size(files2)
		#checks that we have some files to work with for this model, experiment and variable
		if not ((sizing1 == 0) | (sizing2 == 0)):
			print 'reading in: thetao '+model
			if sizing1 > 1:
				#if the data is split across more than one file, it is combined into a single file for ease of processing
				#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
				tmp = []
				for file in files1:
					tmp.append(int(file.split('_')[-1].split('-')[0]))
				order = np.argsort(tmp)
				files1_joined = ' '.join(np.array(files1)[order])
				min_lev = str(cdo.showlevel(input = files1[0])[0]).split(' ')[0]
				print 'merging files'
				#merge together different files from the same experiment
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				except:
					pass
				#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files in right order until otherwise tested).
				cdo.select('level='+min_lev+',name=thetao ',input=files1_joined, output = temporary_file_space1+temp_file1,  options = '-P 7')
			if sizing1 == 1:
				print 'no need to merge - only one file present'
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				except:
					pass
				min_lev = str(cdo.showlevel(input = files1[0])[0]).split(' ')[0]
				cdo.select('level='+min_lev+',name=thetao',input = files1[0], output = temporary_file_space1+temp_file1)
			#######################################
			#                 SOS                 #
			########################################
			#reads in the files to process
			print 'reading in: so '+model
			if sizing2 > 1:
				#if the data is split across more than one file, it is combined into a single file for ease of processing
				#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
				tmp = []
				for file in files2:
					tmp.append(int(file.split('_')[-1].split('-')[0]))
				order = np.argsort(tmp)
				files2_joined = ' '.join(np.array(files2)[order])
				min_lev = str(cdo.showlevel(input = files2[0])[0]).split(' ')[0]
				print 'merging files'
				#merge together different files from the same experiment
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file3, shell=True)
				except:
					pass
				#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files in right order until otherwise tested).
				cdo.select('level='+min_lev+',name=so ',input=files2_joined, output = temporary_file_space1+temp_file3,  options = '-P 7')
			if sizing2 == 1:
				print 'no need to merge - only one file present'
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file3, shell=True)
				except:
					pass
				min_lev = str(cdo.showlevel(input = files2[0])[0]).split(' ')[0]
				cdo.select('level='+min_lev+',name=so',input = files2[0], output = temporary_file_space1+temp_file3)
				# subprocess.call(['cp '+files2[0]+' '+temporary_file_space1+temp_file3], shell=True)
			print 'regridding files horizontally and extracting region and pre-processing'
			#then regrid data onto a 360x180 grid - you could change these values if you wanted to work with different resolurtoin data (lower resolution would make smaller files that would be quicker to work with)
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
			except:
				pass
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass
					#cdo.remapbil('r360x180', input = '-subc,273.15 -setname,to -fldmean -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+ temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file2,  options = '-P 7')
					#cdo.remapbil('r360x180', input = '-setname,sao -fldmean -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+ temporary_file_space1+temp_file3, output = temporary_file_space2+temp_file4,  options = '-P 7')
			cdo.setname('to', input = '-subc,273.15 -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+ temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file2,  options = '-P 7')
			cdo.setname('sao', input = '-setname,sao -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+ temporary_file_space1+temp_file3, output = temporary_file_space2+temp_file4,  options = '-P 7')
			tmp = ' '.join([temporary_file_space2+temp_file2,temporary_file_space2+temp_file4])
			cdo.merge(input = tmp, output = temporary_file_space1+temp_file5,  options = '-P 7')
			cdo.fldmean(input = '-rhopot '+temporary_file_space1+temp_file5, output = temporary_file_space2+temp_file6,  options = '-P 7')
			#Cleaning up
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass
			################################################
			#        calculating various output files      #
			#################################################
			cdo.selseas('JJA', input = '-seasmean ' + temporary_file_space2+temp_file6, output = output_directory+model+'_JJA_sw_density_'+experiment+'_regridded.nc',  options = '-P 7')
			cdo.selseas('SON', input = '-seasmean ' + temporary_file_space2+temp_file6, output = output_directory+model+'_SON_sw_density_'+experiment+'_regridded.nc',  options = '-P 7')
			cdo.selseas('DJF', input = '-seasmean ' + temporary_file_space2+temp_file6, output = output_directory+model+'_DJF_sw_density_'+experiment+'_regridded.nc',  options = '-P 7')
			cdo.selseas('MAM', input = '-seasmean ' + temporary_file_space2+temp_file6, output = output_directory+model+'_MAM_sw_density_'+experiment+'_regridded.nc',  options = '-P 7')
			cdo.yearmean(input = temporary_file_space2+temp_file6, output = output_directory+model+'_annual_mean_sw_density_'+experiment+'_regridded.nc',  options = '-P 7')
			#final clean up
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			except:
				pass

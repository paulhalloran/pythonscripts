##################################
# importing modules we will be using later
##################################

import iris
import numpy as np
import matplotlib.pyplot as plt
import glob
import pandas as pd
import subprocess
import os
import csv
# just for ref. this is what bob's model requires: idum,wind_speed,wind_dir,cloud,airT,airP,humid

##################################
# some functions we will make use of later
##################################

#function that extracts a timeseries from a 2D 'cube' of data for a single latitude and longitue
def extract_timeseries(cube,my_lat,my_lon):
    site_lat_lon = np.array([my_lat,my_lon])
    lat = cube.coord('latitude')
    lon = cube.coord('longitude')
    lat_coord1 = lat.nearest_neighbour_index(site_lat_lon[0])
    lon_coord1 = lon.nearest_neighbour_index(site_lat_lon[1])
    site_timeseries = cube.data[:,lat_coord1,lon_coord1]
    return site_timeseries

def replace_character_in_file(filename,character1,character2):
    with open(filename, 'r') as infile,open(filename+'cleaned_up', 'w') as outfile:
        data = infile.read()
        data = data.replace(character1, character2)
        outfile.write(data)
    subprocess.call(['mv '+filename+'cleaned_up'+' '+filename], shell=True)

##################################
# things that need to be set up for each new set of meterological data you wanyt to produce
##################################

#Specify where the ncep data is stored on your computer. '/data/dataSSD0/ph290/ncep/' refers to a specific directory on my computer where I downloaded the NCEP data to
directory_containing_files_to_process = '/data/dataSSD0/ph290/ncep/'

#Specify the name of the file you ultimately want to produce, which will hold the meterological data to be read in by Bob's model
output_filename = 'meterological_data.dat'

#latitude and longitude for which we want the meterological data
my_lat = 21.0
my_lon = -78.0

##################################
# Other things that need to be defined, but probably not changed
##################################

input_variables = ['vwnd','uwnd','tcdc','rhum','air.2m','pres'] #these are teh names of the variables in teh NCEP files
# North/South wind vector, East/West wind vector, Total cloud cover, relative humidity, 2m air temperature, sea level pressure

##################################
# Tidying up any leftover fiels so that the script does not get confused
##################################

# remove temporary file if it exists (i.e. feftover after previous failed attempts)
try:
    os.remove(directory_containing_files_to_process+'temporary_file.nc')
except:
    print 'no temporary file to remove'

##################################
# pre-processing, reading, and storing sensibly the data from each of NCEP variables, extracting data for the location of interest
##################################

#Make a dictionary which will hold all of the data which will be written out to the text file to drive the model
data={}

#next we will loop through the variables noe by one and read the data in
for single_input_variable in input_variables:
    print 'processing: '+single_input_variable
    #read in the names of all the files for that variable
    files = glob.glob(directory_containing_files_to_process + single_input_variable+'.*.nc')
    #join the filenames into a single line of text (required for the next stage)
    files = ' '.join(files)
    #combine the separate files for that variable into a single file so that it is easier to work with
    subprocess.call(['cdo mergetime '+files+' '+directory_containing_files_to_process+'temporary_file.nc'], shell=True)
    #read that single file in to a variable called cube
    cube = iris.load_cube(directory_containing_files_to_process+'temporary_file.nc')
    #calls the function extract_timeseries which I wrote at the top of the script to extract meterological_data
    #for a timesereis from the nearest grid box to the lat/lon specified
    timeseries = extract_timeseries(cube,my_lat,my_lon)
    #adds this timeseries data into the dictionary we created
    data[single_input_variable] = timeseries
    #removes the temporary file we created
    os.remove(directory_containing_files_to_process+'temporary_file.nc')


##################################
# process the read-in data and get in the right format for output
##################################

#To make things neeter, we putting the data, which has so far been stored in a dictionary into a pandas dataframe
#Once in this format it just makes other thinsg we may want to do easier. See https://www.tutorialspoint.com/python_pandas/python_pandas_dataframe.htm
df = pd.DataFrame(data=data)
#this is an annoyingly complicated looking way of getting the list of year numbers in just the same format
#as they are in the example meterological data file used in the oroginal setup
#Essentially range(1, len(df) + 1) makes a list of numbers from 1 to x, where x is the length of the dataframe (the len(df) bit), i.e. the length of the meterological data we have
#the ''[format(x, ' 5d') for x in r...' bit just takes each of those numbers one by one and formats them so istead of bineg '1', '2'... '10' etc. then are '    1', '    2'... '   10' etc. so that the colums line up correctly in the output file
#This data is then stored in a new column in teh dataframe, called 'day_number'
df['day_number'] = [format(x, ' 5d') for x in range(1, len(df) + 1)]
#this simply converst the data in the 2m air temperature column from K to degrees C
df['air.2m'] = df['air.2m'] - 273.15
#this gets the pressure in the units used by bob's model
df['pres'] = df['pres'] / 100.0
#As far as I could see, NCEP did not supply wind speed, so I've calculated it using pythagoras (square root of the sum of the squares of the x and y vector give teg lenth of the 3rd side of the triangle)
df['wind_speed'] = np.sqrt(np.square(df['uwnd']) + np.square(df['vwnd']))
#similarly, wind direction was not supplied, but this can be calculated using the function arctan2, then converted from radians to degrees
df['wind_direction'] = np.rad2deg((np.arctan2(df['vwnd'],df['uwnd'])) + np.pi)
#In bob's original meterological file the data is all to two decimal places, so the line below rounds all of teh data to two decimal places.
df = df.round(2)

##################################
# Write the data out to teh file
##################################

#this line simply writes out the olumns we are intersted in, in teh order we are intersted in, in the firmat we are intersted in (2 decomal places, 10 characters between columns) to the file we specified at the start
df[['day_number','wind_speed','wind_direction','tcdc','air.2m','pres','rhum']].to_csv(directory_containing_files_to_process+output_filename, index=False, header=False, float_format='%10.2f')
#unfortunately, I could not quickly find a way to successfully write the file without commas between teh columns, so the line below simply strips the columns out from the columns.
replace_character_in_file(directory_containing_files_to_process+output_filename,',','')

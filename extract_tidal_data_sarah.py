import iris

lat = 54.95
lon = 4.92

harmonics = ['M2u_amp','S2u_amp','M2v_amp','S2v_amp','M2u_phase','S2u_phase','M2v_phase','S2v_phase']

file = '/home/ph290/Downloads/harm_const.nc'

def extract_lat_lon(cube,lat,lon):
    site_lat_lon = np.array([lat,lon])
    lat = cube.coord('lat')
    lon = cube.coord('lon')
    lat_coord1 = lat.nearest_neighbour_index(site_lat_lon[0])
    lon_coord1 = lon.nearest_neighbour_index(site_lat_lon[1])
    return cube.data[lat_coord1,lon_coord1]


for h in harmonics:
    cube = iris.load_cube(file,h)
    print extract_lat_lon(cube,lat,lon)


# 1st run
# regridding_and_concatenating_cmip5_files_spco2.py
# and
# join_hist_rcp.py

import iris
import iris.coord_categorisation
import numpy as np
import statsmodels.api as sm
import matplotlib
from cdo import *
cdo = Cdo()
import matplotlib.pyplot as plt
import iris.quickplot as qplt

lon_west = -75.0
lon_east = +5.0
lat_south = 10.0
lat_north = 70.0


#obs

#dave_file = '/data/NAS-ph290/shared/ford_data/main_run/dave_main_run_fco2.nc'
dave_file = '/data/NAS-ph290/shared/ford_data/main_run/dave_main_run_fco2_regridded.nc'
cube = iris.load_cube(dave_file)


cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

try:
	cube_region.coord('latitude').guess_bounds()
except:
	print 'cube already has latitude bounds'


try:
	cube_region.coord('longitude').guess_bounds()
except:
	print 'cube already has longitude bounds'

grid_areas = iris.analysis.cartography.area_weights(cube_region)
cube_region_avg = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

iris.coord_categorisation.add_year(cube_region_avg, 'time', name='year')
cube_region_avg_ann = cube_region_avg.aggregated_by('year', iris.analysis.MEAN)



#obs. 1995 init

dave_file = '/data/NAS-ph290/shared/ford_data/main_run_1995_init/u-ah185_fco2_regrid_year_mean.nc'
cube = iris.load_cube(dave_file)


cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

try:
        cube_region.coord('latitude').guess_bounds()
except: 
        print 'cube already has latitude bounds'


try:
        cube_region.coord('longitude').guess_bounds()
except: 
        print 'cube already has longitude bounds'

grid_areas = iris.analysis.cartography.area_weights(cube_region)
cube_region_avg = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

iris.coord_categorisation.add_year(cube_region_avg, 'time', name='year')
init_cube_region_avg_ann = cube_region_avg.aggregated_by('year', iris.analysis.MEAN)


plt.close('all')

plt.plot(cube_region_avg_ann.coord('year').points,cube_region_avg_ann.data,'g')
plt.plot(init_cube_region_avg_ann.coord('year').points,init_cube_region_avg_ann.data,'r')


plt.savefig('/home/ph290/Documents/figures/fco2_obs_model_timeseries_231116_init.png')



print 'first run regridding_and_concatenating_cmip5_files_thetao_so2.py'
import iris
import numpy as np
import glob
import iris.coord_categorisation

depth_limit = 700 # meters

def getLevelThickness(cube):
    """
    Calculate level thicknesses. This is useful as guess_bounds()
    in Iris v1.3 gets the bounds wrong for irregular depth grids
    as it just averages level depths (results in level depth not
    being halfway between bounds)
    """
    depth = 0
    thickness = []
    for level in cube.coord('depth').points:
        thickness.append(2 * (level - depth))
        depth += thickness[-1]
    return thickness


#directory = '/data/NAS-ph290/ph290/cmip5/historical_thetao/regridded/'
directory = '/data/NAS-ph290/ph290/cmip5/historical_thetao/regridded2/'

files = glob.glob(directory + '*_thetao_historical_r1i1p1_regridded_not_vertically.nc')

years = np.arange(1860,2006)
ensemble = np.zeros([np.size(files),np.size(years)])
ensemble[:,:] = np.nan

for i,file in enumerate(files):
    print file
    try:
        cube = iris.load_cube(file,'sea_water_potential_temperature')
        cube = cube.collapsed(['latitude','longitude'],iris.analysis.MEAN)
        iris.coord_categorisation.add_year(cube, 'time', name='year')
        loc = np.where(cube.coord('depth').points <= depth_limit)
        cube = cube[:,loc[0]]
        thicknesses = getLevelThickness(cube)
        data = cube.data
        for level in np.arange(np.size(thicknesses)):
                data[:,level] = data[:,level] * thicknesses[level]
        cube.data = data
        cube_averaged = cube.collapsed('depth',iris.analysis.SUM)
        cube_averaged /= cube.coord('depth').bounds[-1,1]
        tmp_val = np.mean(cube_averaged.data)
        if np.mean(cube_averaged.data) < 50.0:
            cube_averaged += 273.15
        if np.mean(cube_averaged.data) > 310.0:
            cube_averaged.data[:] = np.nan
        tmp_val = np.mean(cube_averaged.data)
        for j,y in enumerate(cube.coord('year').points):
            loc2 = np.where(y == years)[0]
            if np.size(loc2) > 0:
                ensemble[i,loc2[0]]=cube_averaged[j].data - tmp_val
    except:
        print 'failed: '+file


plt.plot(years,np.swapaxes(ensemble,1,0),'k',lw=3,alpha=0.1)

es_mean = np.nanmean(ensemble,axis=0)

plt.plot(years,es_mean,'k',lw=3)
plt.show()

import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma


#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
			models = np.unique(models_tmp)
	return models

'''
Defining directory locations, variable locatoins etc.
You may well need to edit the text within the quotation marks to adapt the script to work with your data
'''

'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
temporary_file_space3 = '/data/data1/ph290/tmp/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/'
#Directories where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/regridded/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiment = 'past1000'
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.
time_period = 'Omon'

#lats and lons for analysis:
lon1 = -23.0
lon2 = -13.0
lat1 = 64.0
lat2 = 79.0

seasons = ['JJA']

models = model_names(input_directory)
#ensemble = 'r1i1p1'

vars = ['density_mixed_layer','mixed_layer_depth']


for model in models:
	for season in seasons:
		for var in vars:
		# model = models[0]
			print model
			#cleaning up...
			#subprocess.call('rm '+temporary_file_space1+'*.nc', shell=True)
			#subprocess.call('rm '+temporary_file_space2+'*.nc', shell=True)
			temp_file1 = str(uuid.uuid4())+'.nc'
			temp_file2 = str(uuid.uuid4())+'.nc'
			temp_file3 = str(uuid.uuid4())+'.nc'
			temp_file4 = str(uuid.uuid4())+'.nc'
			temp_file5 = str(uuid.uuid4())+'.nc'
			temp_file6 = str(uuid.uuid4())+'.nc'
			files1 = glob.glob(input_directory+var+'_'+model+'_past1000_*_'+season+'.nc')[0]
			#reads in the files to process
			sizing1 = np.size(files1)
			#checks that we have some files to work with for this model, experiment and variable
			if not (sizing1 == 0):
				print 'Regridding ml density '+model
				name = files1.split('/')[-1]
				test = glob.glob(output_directory+name)
				if np.size(test) == 0:
					cdo.remapbil('r360x180', input = files1,output  = output_directory+name,option = 'P -7')

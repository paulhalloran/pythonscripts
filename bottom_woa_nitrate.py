#from https://www.nodc.noaa.gov/OC5/woa13/woa13data.html

import iris
import numpy as np

cube=iris.load('/data/NAS-geo01/ph290/observations/woa13_all_n00_01.nc')[3][0][0:24]
#annual netcdf file (note, coudl use winter file for better values in N. hem and summer file for better value in S. hem) from https://www.nodc.noaa.gov/cgi-bin/OC5/woa13/woa13oxnu.pl
#just taking the top 25 levels so we are only considering the top 200m of the water column

cube2 = cube.copy()
cube2 = cube2[0]
cube2.data.data[:]=cube2.data.fill_value

new_data = cube2.data

for i in range(np.shape(cube2)[0]):
    print i
    for j in range(np.shape(cube2)[1]):
        try:
            new_data[i,j] = cube.data[cube.data.mask[:,i,j]==False][-1,i,j]
        except:
            pass


cube2.data = new_data

#qplt.contourf(cube2,31)
#plt.savefig('woa_bottom_N.png')



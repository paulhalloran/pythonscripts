from cdo import *
cdo = Cdo()
import glob
import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
import os
import subprocess
import numpy.ma as ma

def model_names_thetao(directory):
	files = glob.glob(directory+'thetao_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models
	

input_directory = '/data/NAS-ph290/ph290/cmip5/past1000/'
models = model_names_thetao(input_directory)
models = list(models)
models.remove('MIROC-ESM')
models = np.array(models)

for model in models:
# 	model = 'CSIRO-Mk3L-1-2'
	print model
	ensemble = 'r1i1p1'
	if model == 'GISS-E2-R':
		ensemble = 'r1i1p121'
	temporary_file_space1 = '/data/data1/ph290/tmp/'
	subprocess.call('rm '+temporary_file_space1+'delete*.nc', shell=True)
	files1 = glob.glob(input_directory+'thetao_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
	files2 = glob.glob(input_directory+'so_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
	cdo.select('timestep=1,name=thetao',input = files1[0], output = temporary_file_space1+'delete.nc',  options = '-P 7')
	cdo.select('timestep=1,name=so',input = files2[0], output = temporary_file_space1+'delete2.nc',  options = '-P 7')
	cdo.merge(input = '-setname,tho, -subc,273.15 '+ temporary_file_space1+'delete.nc' + ' -setname,s '+temporary_file_space1+'delete2.nc', output = temporary_file_space1+'delete3.nc',  options = '-P 7')
	cdo.remapbil('r180x90',input = '-rhopot,0 -adisit '+ temporary_file_space1+'delete3.nc', output = temporary_file_space1+'delete4.nc',  options = '-P 7')
	density_cube = iris.load_cube(temporary_file_space1+'delete4.nc')
	density_cube = density_cube.collapsed('time',iris.analysis.MEAN)
	cdo.seltimestep('1',input = files1[0], output = temporary_file_space1+'delete5.nc')
	cdo.remapbil('r180x90',input = temporary_file_space1+'delete5.nc', output = temporary_file_space1+'delete6.nc')
	sample_cube = iris.load_cube(temporary_file_space1+'delete6.nc')	
	sample_cube = sample_cube.collapsed('time',iris.analysis.MEAN)		
	depth_cube = density_cube.copy()
	try:
		depth_cube.data = ma.masked_all_like(density_cube.data.data).copy()
	except:
		depth_cube.data = ma.masked_all_like(density_cube.data).copy()
	try:
		density_cube.coord('depth')
		depths = density_cube.coord('depth').bounds[:,1] # note changed this, because up 'til now it was working with the mid-points of layers, which would have skewed the meaning across levels
	except:
		depths = density_cube.coord('ocean sigma over z coordinate').bounds[:,1]
		#for memorys sake, do one year at a time..
	shape = np.shape(density_cube)
	depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[shape[2],shape[1],1]),0,2))
	try:
		density_cube.coord('depth')
		MLD_out = density_cube.extract(iris.Constraint(depth = np.min(depth_cube.data)))
		MLD_out.data = ma.masked_all_like(density_cube[0,:,:].data.data).copy()
		surface_density_out = MLD_out.copy()
		surface_density_out.data = ma.masked_all_like(density_cube[0,:,:].data.data).copy()
	except:
		MLD_out = density_cube[0,:,:]
		surface_density_out = MLD_out.copy()
		MLD_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
		surface_density_out.data = ma.masked_all_like(density_cube[0,:,:].data.data).copy()
	MLD_out_data = MLD_out.data.copy()
	surface_density_data = surface_density_out.data.copy()
	density_tmp = density_cube.copy()
	density_diff = density_tmp[0].copy()
	density_diff2 = density_tmp[0].copy()
	density_diff += 0.125 # Levitus (1982) density criteria
	density_diff3 = np.tile(density_diff.data,[np.shape(density_cube.data)[0],1,1])
	idx_mld = density_cube.data <= density_diff3
	MLD = density_tmp.data.copy()
	MLD.mask = False
	MLD.data[:] = np.NAN
	#MLD.mask = density_cube[i].data.mask.copy()
	MLD.data[idx_mld] = depth_cube.data[idx_mld]
	MLD = np.ma.masked_invalid(MLD)
	MLD = np.ma.masked_where(sample_cube.data.mask,MLD)
	MLD_out_data = np.ma.max(MLD,axis=0)
	#density
	surface_density = density_tmp.data.copy()
	surface_density.mask = False
	surface_density.data[:] = np.NAN
	surface_density.data[idx_mld] = density_tmp.data[idx_mld]
	surface_density = np.ma.masked_invalid(surface_density)
	surface_density = np.ma.masked_where(sample_cube.data.mask,surface_density)
	thicknesses = MLD.copy()
	thicknesses[1::,:,:] = depth_cube.data[1::,:,:] - depth_cube.data[0:-1,:,:]
	thicknesses[0,:,:] = depth_cube.data[0,:,:]
	surface_density_data = np.ma.sum(surface_density * thicknesses,axis=0) / np.ma.max(MLD,axis=0)
	MLD_out.data = MLD_out_data
	surface_density_out.data = surface_density_data
	qplt.contourf(surface_density_out,np.linspace(1000,1050,21))
	plt.show()

'''
	cube = iris.load_cube(temporary_file_space1+'delete4.nc')
	try:
		cube.coord('latitude').guess_bounds()
	except:
		print 'cube already has latitude bounds' 
	try:
		cube.coord('longitude').guess_bounds()
	except:
		print 'cube already has longitude bounds'
	grid_areas = iris.analysis.cartography.area_weights(cube)
	area_avged_cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
	qplt.plot(area_avged_cube[0])
	ax = plt.gca()
	ax.ticklabel_format(useOffset=False)
	plt.show()
'''

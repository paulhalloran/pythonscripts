import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import glob
import pandas as pd
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator

def put_data_into_cube(df,variable):
    latitudes = np.unique(df['latitude'].values)
    # latitudes = np.linspace(np.nanmin(df['latitude'].values),np.nanmax(df['latitude'].values),200)
    longitudes = np.unique(df['longitude'].values)
    # longitudes = np.linspace(np.nanmin(df['longitude'].values),np.nanmax(df['longitude'].values),100)
    times = np.unique(df['day'].values)
    # depths = np.unique(df['depth'].values)
    latitude = iris.coords.DimCoord(latitudes, standard_name='latitude', units='degrees')
    longitude = iris.coords.DimCoord(longitudes, standard_name='longitude', units='degrees')
    time = iris.coords.DimCoord(times, standard_name='time', units='days')
    # cube = iris.cube.Cube(np.zeros((latitudes.size, longitudes.size), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(latitude, 0), (longitude, 1)])
    cube = iris.cube.Cube(np.zeros((times.size,latitudes.size, longitudes.size), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(time,0), (latitude, 1), (longitude, 2)])
    # X,Y = np.meshgrid(cube.coord('longitude').points,cube.coord('latitude').points)
    Z,X,Y = np.meshgrid(cube.coord('time').points,cube.coord('longitude').points,cube.coord('latitude').points)
    data = cube.data.copy()
    data[:] = -999.99
    days = np.unique(df['day'].values)
    shape = [X.shape[0],X.shape[2]]
    for i,day in enumerate(days):
        print 'processing day: ',i
        df_tmp = df.loc[df['day'] == day]
        data[i,:,:] = np.fliplr(np.rot90(df_tmp[variable].values.reshape(shape),3))
    data = np.ma.masked_where((data.data < -999.9) & (data.data > -1000.0),data)
    # data = np.ma.masked_where(data.data == 0.0,data)
    cube.data = data
    cube.data.fill_value = -999.99
    cube.data = np.ma.masked_where(cube.data == -999.99,cube.data)
    return cube

#########
# Specify the data you want to read in
#########
directory = '/home/ph290/Downloads/'
file = 'output_map'
variable_to_plot = "bottom temperature"
column_names = ["day","longitude", "latitude", "depth", "surface temperature","bottom temperature"]
#column_names = ["longitude", "latitude", "depth", "Hunter-Simpson parameter","stratification","net heat flux","carbon accumulation","temperature surface","temperature bottom","chlorophyll"]
#Note, this is a 2D dataset, not 3D. Depth is a 2D field of the depth of the bottom of the ocean at that point, not a column of level depths.

#########
# Read data in
#########
df = pd.read_csv(directory + file,header=None, sep=r'\s{1,}',names = column_names)

#########
# Put data into an iris cube (can then be worked with simply)
#########

cube_surface_temperature = put_data_into_cube(df,'surface temperature')
cube_bottom_temperature = put_data_into_cube(df,'bottom temperature')
cube_depth = put_data_into_cube(df,'depth')

#########
# Plot the data
#########


cube2 = cube_surface_temperature.collapsed('time',iris.analysis.MEAN) - cube_bottom_temperature.collapsed('time',iris.analysis.MEAN)
#cube2 = cube_surface_temperature.collapsed('time',iris.analysis.MEAN)

land_50m = cfeature.NaturalEarthFeature('physical', 'land', '50m',
                                        edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])


plt.close('all')
# Choose map projection - see http://scitools.org.uk/cartopy/docs/v0.14/crs/projections.html#cartopy-projections
ax = plt.axes(projection=ccrs.PlateCarree())
# Get the data onto the map. Can change things - e.g. colour map used from hot to other - see https://matplotlib.org/examples/color/colormaps_reference.html
levels = MaxNLocator(nbins=50).tick_values(np.nanmin(cube2.data), np.nanmax(cube2.data))
cmap = plt.get_cmap('Reds')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
CS = ax.contour(cube_depth[0].coord('longitude').points, cube_depth[0].coord('latitude').points, cube_depth[0].data,50,
                transform=ccrs.PlateCarree(),colors='k',alpha=0.2)
CS = ax.pcolormesh(cube2.coord('longitude').points, cube2.coord('latitude').points, cube2.data,
                transform=ccrs.PlateCarree(),
                cmap=cmap, norm=norm)
ax.add_feature(land_50m)
ax.coastlines('50m')
plt.title = variable_to_plot
plt.colorbar(CS, ax=ax)
plt.savefig('/home/ph290/Documents/figures/carib_surf_bot_t_diff.png',dpi=1000)
plt.show()

from numpy import *
from matplotlib.pyplot import *
from iris import *
from iris.analysis import *
import iris.quickplot as quickplot
from iris.coord_categorisation import *
from iris.analysis.cartography import *
from scipy.stats import *
from scipy.stats.mstats import *



my_cube = load_cube('/data/dataSSD0/ph290/regridded_temperature.nc')

average_across_time = my_cube.collapsed(['time','depth'],MEAN)

quickplot.contourf(my_cube,31)

plt.show()
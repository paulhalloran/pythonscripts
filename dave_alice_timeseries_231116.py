
# 1st run
# regridding_and_concatenating_cmip5_files_spco2.py
# and
# join_hist_rcp.py

import iris
import iris.coord_categorisation
import numpy as np
import statsmodels.api as sm
import matplotlib
from cdo import *
cdo = Cdo()
import matplotlib.pyplot as plt

lon_west = -75.0
lon_east = +5.0
lat_south = 10.0
lat_north = 70.0


#obs

#dave_file = '/data/NAS-ph290/shared/ford_data/main_run/dave_main_run_fco2.nc'
dave_file = '/data/NAS-ph290/shared/ford_data/main_run/dave_main_run_fco2_regridded.nc'
cube = iris.load_cube(dave_file)


cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

try:
	cube_region.coord('latitude').guess_bounds()
except:
	print 'cube already has latitude bounds'


try:
	cube_region.coord('longitude').guess_bounds()
except:
	print 'cube already has longitude bounds'

grid_areas = iris.analysis.cartography.area_weights(cube_region)
cube_region_avg = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

iris.coord_categorisation.add_year(cube_region_avg, 'time', name='year')
cube_region_avg_ann = cube_region_avg.aggregated_by('year', iris.analysis.MEAN)



#obs. 1995 init

dave_file = '/data/NAS-ph290/shared/ford_data/main_run_1995_init/u-ah185_fco2_regrid_year_mean.nc'
cube = iris.load_cube(dave_file)


cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

try:
        cube_region.coord('latitude').guess_bounds()
except: 
        print 'cube already has latitude bounds'


try:
        cube_region.coord('longitude').guess_bounds()
except: 
        print 'cube already has longitude bounds'

grid_areas = iris.analysis.cartography.area_weights(cube_region)
cube_region_avg = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

iris.coord_categorisation.add_year(cube_region_avg, 'time', name='year')
init_cube_region_avg_ann = cube_region_avg.aggregated_by('year', iris.analysis.MEAN)

#models

model_cubes = iris.load('/data/NAS-ph290/shared/spco2/regrid/joined/*.nc','surface_partial_pressure_of_carbon_dioxide_in_sea_water')

model_cube_region_avg_anns = []

for cube in model_cubes:
	cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
	cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

	try:
		cube_region.coord('latitude').guess_bounds()
	except:
		print 'cube already has latitude bounds'
	try:
		cube_region.coord('longitude').guess_bounds()
	except:
		print 'cube already has longitude bounds'

	grid_areas = iris.analysis.cartography.area_weights(cube_region)
	cube_region_avg = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

	iris.coord_categorisation.add_year(cube_region_avg, 'time', name='year')
	model_cube_region_avg_anns.append(cube_region_avg.aggregated_by('year', iris.analysis.MEAN))


#plotting



def line_int(x,y,colour,alpha_value,lab):
	xsort = np.argsort(x)
	x = x[xsort]
	y = y[xsort]
	xb = sm.add_constant(x)
	model = sm.OLS(y,xb)
	results = model.fit()
	x2 = np.linspace(np.min(x),np.max(x),np.size(x))
	y2 = results.predict(sm.add_constant(x2))
	from statsmodels.stats.outliers_influence import summary_table
	st, data, ss2 = summary_table(results, alpha=0.001)
	fittedvalues = data[:,2]
	predict_mean_se  = data[:,3]
	predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
	predict_ci_low, predict_ci_upp = data[:,6:8].T
	p = ax.plot(x, fittedvalues - fittedvalues[0], colour, lw=3,alpha = alpha_value,label=lab+' trend')
	ax.plot(x, y - fittedvalues[0], colour, lw=1,alpha = alpha_value)
	ax.scatter(x, y - fittedvalues[0], color=colour,alpha = alpha_value,label=lab+' ann. avg.')
	return p

# 	plt.plot(x, predict_ci_low, 'r--', lw=2)
# 	plt.plot(x, predict_ci_upp, 'r--', lw=2)
# 	plt.plot(x, predict_mean_ci_low, 'r--', lw=2)
# 	plt.plot(x, predict_mean_ci_upp, 'r--', lw=2)

############################
#Alice's data
############################




#a_file = '/data/NAS-geo01/ph290/lebehot_data/annual_fco2_with_uncertainty_North_Atlantic_2000_2013.nc'
#a_cube = iris.load(a_file)

a_file = '/data/NAS-geo01/ph290/lebehot_data/new_obs_fco2_1992_2014.txt'
a_data_all = np.genfromtxt(a_file,skip_header = 1)


plt.close('all')

ax = plt.subplot()


import matplotlib.font_manager as font_manager

# Set the font dictionaries (for plot title and axis titles)
title_font = {'fontname':'Arial', 'size':'14',
              'verticalalignment':'bottom'} # Bottom vertical alignment for more space
axis_font = {'fontname':'Arial', 'size':'14'}

for i,cube in enumerate(model_cubes):
	tmp = model_cube_region_avg_anns[i].copy()
	loc = np.where(tmp.coord('year').points == 1992)[0][0]
	tmp = tmp[loc:loc+23]
	if np.size(tmp.data) > 10:
		mn = tmp.collapsed('time',iris.analysis.MEAN)
		if mn.data/ 1.01325e-1 > 300:
	# 		qplt.plot((tmp-mn) / 1.01325e-1,'k',alpha = 0.5)
			y = (tmp) / 1.01325e-1
			x=tmp.coord('year').points
			if i==0:
				p1 = line_int(x,y.data,'r',0.2,'CMIP5 models')
			else:
				p1 = line_int(x,y.data,'r',0.2,'_nolegend_')


#Dave's main run
tmp = cube_region_avg_ann
loc = loc = np.where(tmp.coord('year').points == 1992)[0][0]
tmp = tmp[loc:loc+23]
mn = tmp.collapsed('time',iris.analysis.MEAN)
y = (tmp)
x=tmp.coord('year').points
# plt.plot(x,y.data,'b',lw=3.0)
p2 = line_int(x,y.data,'g',0.5,'Observation-corrected model')


#Dave's run reinitialised at 1995
tmp = init_cube_region_avg_ann
loc = loc = np.where(tmp.coord('year').points == 1995)[0][0]
tmp = tmp[loc:loc+20]
mn = tmp.collapsed('time',iris.analysis.MEAN)
y = (tmp)
x=tmp.coord('year').points
# plt.plot(x,y.data,'b',lw=3.0)
p3 = line_int(x,y.data,'g--',0.5,'Observation-corrected model')

plt.xlim(1992,2014)

for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontname('Arial')
    label.set_fontsize(13)

#a_data = a_cube[0].data
#a_uncertainty = a_cube[1].data
a_data = a_data_all[:,1]
#a_year = a_cube[0].coord('time').points
a_year = a_data_all[:,0]
line_int(a_year,a_data,'b',0.5,'Observation based')

plt.xlabel('year', **axis_font)
plt.ylabel('surface ocean CO$_2$ concentartion anomaly', **axis_font)

l = plt.legend(loc=0, fancybox=True, framealpha=0.5,fontsize=12)


plt.savefig('/home/ph290/Documents/figures/fco2_dave_alice_timeseries_231116_init.png')
plt.show(block = False)



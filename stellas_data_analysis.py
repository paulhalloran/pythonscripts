"""

import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt

data1 = '/data/NAS-ph290/ph290/cmip5/mid_hol/regridded_ann/HadGEM2-ES_tos_midHolocene_regridded.nc'
data2 = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/HadGEM2-ES_tos_piControl_r1i1p1_regridded_not_vertically.nc'
data1b = '/data/NAS-ph290/ph290/cmip5/mid_hol/regridded_ann/MPI-ESM-P_tos_midHolocene_regridded.nc'
data2b = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/MPI-ESM-P_tos_piControl_r1i1p1_regridded_not_vertically.nc'

data1c = '/data/NAS-ph290/ph290/cmip5/mid_hol/regridded_ann/MIROC-ESM_tos_midHolocene_regridded.nc'
data2c = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/MIROC-ESM_tos_piControl_r1i1p1_regridded_not_vertically.nc'

models = ['bcc-csm1-1','HadGEM2-CC','CCSM4','HadGEM2-ES','CNRM-CM5','IPSL-CM5A-LR','MIROC-ESM','MPI-ESM-P','GISS-E2-R','MRI-CGCM3']

'''
for model in models:
	data1 = '/data/NAS-ph290/ph290/cmip5/mid_hol/regridded_ann/'+model+'_tos_midHolocene_regridded.nc'
	data2 = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/'+model+'_tos_piControl_r1i1p1_regridded_not_vertically.nc'
	had_mid_sst = iris.load_cube(data1)
	had_pi_sst = iris.load_cube(data2)
	had_mid_sst_mean = had_mid_sst.collapsed('time',iris.analysis.MEAN)
	had_pi_sst_mean = had_pi_sst.collapsed('time',iris.analysis.MEAN)
	#qplt.contourf(had_mid_sst_mean,30)
	#plt.show()
	#qplt.contourf(had_pi_sst_mean,30)
	#plt.show()
	qplt.contourf(had_mid_sst_mean - had_pi_sst_mean,30)
	plt.title(model)
	plt.gca().coastlines()
	plt.show()
'''

data = np.zeros([180,360,np.size(models)])


for i,model in enumerate(models):
	print i
        data1 = '/data/NAS-ph290/ph290/cmip5/mid_hol/regridded_ann/'+model+'_tos_midHolocene_regridded.nc'
        data2 = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/'+model+'_tos_piControl_r1i1p1_regridded_not_vertically.nc'
        mid_sst = iris.load_cube(data1)
        pi_sst = iris.load_cube(data2)
	mid_sst_mean = mid_sst.collapsed('time',iris.analysis.MEAN)
	pi_sst_mean = pi_sst.collapsed('time',iris.analysis.MEAN)
	tmp = mid_sst_mean - pi_sst_mean
	data[:,:,i] = tmp.data



example_cube = pi_sst_mean
example_cube.data = np.mean(data,axis = 2)
qplt.contourf(example_cube,np.linspace(-1.5,1.5,31))
plt.title('multimodel mean sst Mid Hol minus Pi')
plt.gca().coastlines()
plt.savefig('/home/ph290/Documents/figures/multimodel_mean_sst_MidHol_minus_Pi.png')
plt.show()


example_cube = pi_sst_mean
example_cube.data = np.std(data,axis = 2)
qplt.contourf(example_cube,31)
plt.title('multimodel stdev sst Mid Hol minus Pi')
plt.gca().coastlines()
plt.savefig('/home/ph290/Documents/figures/multimodel_stdev_sst_MidHol_minus_Pi.png')
plt.show()



"""

models = ['GISS-E2-R','IPSL-CM5A-LR','CNRM-CM5','HadGEM2-CC','MIROC-ESM','HadGEM2-ES','MPI-ESM-P']

data = np.zeros([180,360,np.size(models)])

for i,model in enumerate(models):
        print i
        data1 = '/data/NAS-ph290/ph290/cmip5/mid_hol/regridded_ann/'+model+'_sos_midHolocene_regridded.nc'
        data2 = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/'+model+'_sos_piControl_r1i1p1_regridded_not_vertically.nc'
        mid_sst = iris.load_cube(data1)
        pi_sst = iris.load_cube(data2)
        mid_sst_mean = mid_sst.collapsed('time',iris.analysis.MEAN)
        pi_sst_mean = pi_sst.collapsed('time',iris.analysis.MEAN)
        #tmp = mid_sst_mean - pi_sst_mean
        data[:,:,i] = mid_sst_mean.data - pi_sst_mean.data
	#example_cube = pi_sst_mean
	#example_cube.data = data[:,:,i]
	#qplt.contourf(example_cube)
	#plt.show()
	

example_cube = pi_sst_mean
example_cube.data = np.mean(data,axis = 2)
qplt.contourf(example_cube,np.linspace(-1.0,1.0,31))
plt.title('multimodel mean sss Mid Hol minus Pi')
plt.gca().coastlines()
plt.savefig('/home/ph290/Documents/figures/multimodel_mean_sss_MidHol_minus_Pi.png')
plt.show()


example_cube = pi_sst_mean
example_cube.data = np.std(data,axis = 2)
qplt.contourf(example_cube,31)
plt.title('multimodel stdev sss Mid Hol minus Pi')
plt.gca().coastlines()
plt.savefig('/home/ph290/Documents/figures/multimodel_stdev_sss_MidHol_minus_Pi.png')
plt.show()



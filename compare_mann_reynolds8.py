
import numpy as np
import matplotlib.pyplot as plt
import running_mean as rm
import running_mean_post as rmp
import scipy
import scipy.signal
import scipy.stats as stats
import matplotlib.gridspec as gridspec
import pickle
import os
from timer import Timer
import pandas




start_year = 953
# start_year = 1350
# start_year = 1574
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)

window_type = 'boxcar'


############
# Files and read in data
############

amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
# amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)

#############
# Mann AMO
#############
amo_yr = amo[:,0].copy()
amo_data = amo[:,1].copy()
# 	amo_yr = amo_yr[6::]
# 	amo_data = amo_data[6::]
loc = np.where((amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data = scipy.signal.detrend(amo_data, axis=0)


#############
# AMOC
#############
AMOC = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt',delimiter=',')
smoothing2 = 20
# shift = 15
AMOC_yr = AMOC[:,0].copy()
AMOC_data = AMOC[:,1].copy()
loc = np.where((AMOC_yr >= start_year) & (AMOC_yr <= end_year))
AMOC_yr = AMOC_yr[loc]
AMOC_data = AMOC_data[loc]
# AMOC_data = np.roll(AMOC_data,shift)
# AMOC_data[0:shift] = np.nan
# AMOC_data[-1.0*shift::] = np.nan
AMOC_data = pandas.rolling_window(AMOC_data,smoothing2,win_type=window_type,center=True)


#############
# tas
#############
tas = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/pmip3_tas.txt',delimiter=',')
# smoothing2 = 10
tas_yr = tas[:,0].copy()
tas_data = tas[:,1].copy()
loc = np.where((tas_yr >= start_year) & (tas_yr <= end_year))
tas_yr = tas_yr[loc]
tas_data = tas_data[loc]
# tas_data = pandas.rolling_window(tas_data,smoothing2,win_type=window_type,center=True)




#############
# Volc. Crow.
#############
file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)
data3 = np.genfromtxt(file3)
data4 = np.genfromtxt(file4)
volc_smoothing = 20
# data_tmp = np.zeros([data1.shape[0],2])
# data_tmp[:,0] = data1[:,1].copy()
# data_tmp[:,1] = data2[:,1].copy()
data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data2[:,1].copy()
data_tmp[:,1] = data1[:,1].copy()
# data_tmp[:,2] = data3[:,1].copy()
# data_tmp[:,3] = data4[:,1].copy()
data = np.mean(data_tmp,axis = 1)
voln_n = data1.copy()
voln_n[:,1] = data
volc_data = voln_n[:,1]
volc_yr = voln_n[:,0]
loc = np.where((volc_yr >= start_year) & (volc_yr <= end_year))
volc_yr = volc_yr[loc]
volc_data = volc_data[loc]
tmp = np.round(volc_yr)
tmp_unique = np.unique(tmp)
volc_yr2 = tmp_unique
volc_data2 = tmp_unique.copy()
for count,yr in enumerate(volc_yr2):
	loc = np.where(tmp == yr)
	volc_data2[count] = np.mean(volc_data[loc])
#calculating volcanic forcing from AOD, following Harris and Highwood 2011
# volc_data2 = -11.3 * (1 - np.exp(-0.164*volc_data2))
volc_data2 = pandas.rolling_window(volc_data2,volc_smoothing,win_type=window_type,center=True)

######################
# Removing Nans etc.
######################

loc2 = np.where(np.logical_not((np.isnan(amo_data)) | (np.isnan(AMOC_data)) | (np.isnan(tas_data))))

amo_yr = amo_yr[loc2]
amo_data = amo_data[loc2]

AMOC_yr = AMOC_yr[loc2]
AMOC_data = AMOC_data[loc2]
tas_yr = tas_yr[loc2]
tas_data = tas_data[loc2]

amo_data -= np.min(amo_data)
amo_data /= np.max(amo_data)

y1 = amo_data
y1 -= np.mean(y1)
y1 = scipy.signal.detrend(y1, axis=0)


'''
y2 = volc_data2 * 4.0
y3 = AMOC_data * 0.4e-8
'''

y2 = tas_data

y2 -= np.min(y2)
y2 /= np.max(y2)
y2 -= np.mean(y2)
y2 = scipy.signal.detrend(y2, axis=0)



# y3 = (AMOC_data / (1029.0*1.0e6)) * 5.0
y3 = AMOC_data
y3 -= np.min(y3)
y3 /= np.max(y3)
y3 -= np.mean(y3)

y3 = scipy.signal.detrend(y3, axis=0)

y4 = (y2 * 1.0) + (y3 * 0.7)

y4 = scipy.signal.detrend(y4, axis=0)
# y2 = scipy.signal.detrend(y2, axis=0)

# y2 = scipy.signal.detrend(y2, axis=0)

c = 0

r_value1, p_value2 = stats.spearmanr(y1,y2)
r_value2, p_value2 = stats.spearmanr(y1,y3)
r_value3, p_value2 = stats.spearmanr(y1,y4)

# r_value1, p_value2 = stats.pearsonr(y1,y2)
# r_value2, p_value2 = stats.pearsonr(y1,y3)
# r_value3, p_value2 = stats.pearsonr(y1,y4)


print 'volc r-value ',r_value1
print 'amoc r-value ',r_value2
print 'combined r-value ',r_value3

'''

#############
# Plotting
#############


plt.close('all')
plt.rc('legend',**{'fontsize':10})
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(15)

#gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)
#ax1 = plt.subplot(gs[0:30,0:100])
#ax2 = plt.subplot(gs[35:65:100,0:100])
#ax3 = plt.subplot(gs[70:100,0:100])

gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.1,right=0.95)
ax1 = plt.subplot(gs[0:100,0:100])

ax1.plot(amo_yr,y1,'b',lw = 2.0,label = 'Proxy reconstructed AMO index')
# ax1.plot(g_amo_yr,y1b,'y',lw = 2.0,label = 'Proxy reconstructed AMO index')

ax2 = ax1.twinx()
# ax2.plot(bivalve_yr,y2,'r',lw = 2,label = 'Bivalve d$^{18}$O')
ax2.plot(density_yr,y5,'g',lw = 2,label = 'volc+and...')
ax3 = ax1.twinx()
# ax3.plot(density_yr,y1_3,'y',lw = 1,label = 'density')
ax3.plot(density_yr,y3,'k',lw = 1,label = 'density')
# ax3.plot(density_yr,y1_3,'y',lw = 1,label = 'density')
# ax4 = ax1.twinx()
# ax4.plot(density_yr,y2,'r',lw = 1,label = 'bivalve')
ax1.annotate('r-value = '+np.str(np.around(r_value4,2))+', p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center')


ax1.set_ylabel('Normalised anomaly')

ax1.set_xlabel('Calendar year')

ax1.set_xlim([950,end_year])
# ax1.set_xlim([1300,end_year])
ax1.set_ylim([-0.6,0.6])
ax2.set_ylim([-0.4,0.4])
ax3.set_ylim([-0.4,0.4])
# ax4.set_ylim([-3.0,3.0])


# ax1.set_ylim([-0.1,1.1])

# plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/bivalve_AMO.png')
plt.show(block = False)

'''



plt.close('all')
plt.rc('legend',**{'fontsize':10})
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(15)

gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.1,right=0.95)
ax1 = plt.subplot(gs[0:40,0:45])
ax2 = plt.subplot(gs[60:100,0:45])
ax3 = plt.subplot(gs[30:70,55:100])
ax4 = plt.subplot(gs[80:100,55:100])
ax4.set_frame_on(False)
ax4.axes.get_xaxis().set_visible(False)
ax4.axes.get_yaxis().set_visible(False)


yr = amo_yr

ax1.plot(yr,y1,'b',lw = 3)
ax1.plot(yr,y2,'r',lw = 3,label = 'volcanic forcing x -1.0')
ax1.annotate('r-value = '+np.str(np.around(r_value1,2))+', p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center')

ax2.plot(yr,y1,'b',lw = 3)
ax2.plot(yr,y3,'k',lw = 3,label = 'PMIP3 ensmble mean AMOC strength')
ax2.annotate('r-value = '+np.str(np.around(r_value2,2))+', p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center')

ax3.plot(yr,y1,'b',lw = 3,label = 'Proxy reconstructed AMO index')
ax3.plot(yr,y4+c,'g',lw = 3,label = 'volcanic and PMIP3 high-latitude density')
ax3.annotate('r-value = '+np.str(np.around(r_value3,2))+', p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center')

ax1.set_title('(a) Direct volcanic forcing')
ax2.set_title('(b) Forced PMIP3 Atlantic MOC')
ax3.set_title('(c) Combining volcanic and Atlantic MOC')

ax1.set_ylabel('Normalised anomaly')
ax2.set_ylabel('Normalised anomaly')
ax3.set_ylabel('Normalised anomaly')

ax1.set_xlabel('Calendar year')
ax2.set_xlabel('Calendar year')
ax3.set_xlabel('Calendar year')

ax1.set_xlim([960,end_year])
ax2.set_xlim([960,end_year])
ax3.set_xlim([960,end_year])

ax1.set_ylim([-1,1])
ax2.set_ylim([-1,1])
ax3.set_ylim([-1,1])


lines = ax3.get_lines()
lines.extend(ax2.get_lines())
lines.extend(ax1.get_lines())
del lines[-2]
del lines[-3]
labels = [l.get_label() for l in lines]
plt.figlegend(lines,labels, loc = 'lower right', bbox_to_anchor = (-0.14,0.14,1,1),frameon=False)


#see: http://matplotlib.org/examples/pylab_examples/annotation_demo2.html
ax4.annotate('',xy=(0.6, 0.7), xycoords='figure fraction',
                xytext=(0.5, 0.8), textcoords='figure fraction',
                arrowprops=dict(arrowstyle="->",lw=3,
                                connectionstyle="angle,angleA=0,angleB=90,rad=10"),
                )


ax4.annotate('',xy=(0.6, 0.3), xycoords='figure fraction',
                xytext=(0.5, 0.2), textcoords='figure fraction',
                arrowprops=dict(arrowstyle="->",lw=3,
                                connectionstyle="angle,angleA=0,angleB=90,rad=10"),
                )


fig.canvas.draw()

# plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/forced_AMO_AMOC.png')
plt.show(block = False)

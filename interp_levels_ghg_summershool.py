'''
This script processes ocean data (and I think it should work with at least surface level atmospheric data) from teh CMIP5 archive to put it all on the same grid (same horizontal and vertical resolution (nominally 360x180 i.e. 1 degree horizontal, and every 10m in the shallow ocean, and every 500m in the deep ocean - see later if you want to adjust this)). It also convertsaverages monthly data to annual data - again, you could change this.
'''

#First we need to import the modules required to do the analysis
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid


input_directory = '/data/NAS-ph290/ph290/delete/'
#Directory where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/NAS-ph290/ph290/delete/regrid/'

files = glob.glob(input_directory+'*.nc')

for file in files:
	file_name = file.split('/')[0]
	test = False
	proc = subprocess.Popen('ncdump -h '+file,shell=True,stdout=subprocess.PIPE)
	for line in proc.stdout:
		if '"depth"' in line:
			test = True
        	if '"ocean sigma coordinate"' in line:
        		test = True
        	if '"ocean sigma over z coordinate"' in line:
        		test = True
	if test:
		subprocess.call(['cdo -P 8 intlevel,6000,5000,4000,3000,2000,1000,900,800,700,600,500,400,300,200,100,90,80,70,60,50,40,30,20,10,0 '+file+' '+output_directory+file_name], shell=True)
	else:
		subprocess.call('mv '+file+' '+output_directory+file_name, shell=True)


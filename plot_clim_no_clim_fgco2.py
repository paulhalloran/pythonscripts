import iris
import glob
import os

def model_names1(directory):
        files = glob.glob(directory+'/*esmFdbk2*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


def model_names2(directory):
        files = glob.glob(directory+'/*esmFixClim2*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


yearsec = 60.0*60.0*24.0*365.0

directory = '/data/NAS-ph290/ph290/cmip5/co2_on_off/regridded/'
models1 = model_names1(directory)
models2 = model_names2(directory)
models = list(set(list(models1)).intersection(list(models2)))


plt.close('all')

a = []
b = []


for model in models:
    print model

    cube1 = iris.load_cube(directory+model+'*_esmFdbk2*_regridded.nc')
    cube2 = iris.load_cube(directory+model+'*_esmFixClim2*_regridded.nc')
    if model == 'CanESM2':
        cube1 /= (44/12.0)
        cube2 /= (44/12.0)

    try:
        cube1.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'



    try:
        cube1.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'


    try:
        cube2.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'



    try:
        cube2.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'


    cube = cube1.copy()
    cube.data *= 0.0
    cube.data +=1.0
    grid_areas = iris.analysis.cartography.area_weights(cube)
    ocean_area = cube.collapsed(['longitude', 'latitude'], iris.analysis.SUM, weights=grid_areas)

    grid_areas = iris.analysis.cartography.area_weights(cube1)
    esmFdbk2 = cube1.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)*ocean_area[0].data*yearsec*1000.0

    grid_areas = iris.analysis.cartography.area_weights(cube2)
    esmFixClim2 = cube2.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)*ocean_area[0].data*yearsec*1000.0

    a.append(esmFdbk2.data/1.0e15)
    b.append(esmFixClim2.data/1.0e15)
    plt.plot(esmFdbk2.data/1.0e15,color='b')
    plt.plot(esmFixClim2.data/1.0e15,color='r')


plt.show(block = False)


plt.close('all')
fig = plt.figure()
ax = fig.add_subplot(111)

fsize = 17
fontsize = 17
for i,model in enumerate(models):
    print model
    ax.plot(np.arange(1860,1860+np.size(a[i])),a[i]-np.mean(a[i][0:20]),color='b',lw=2,alpha=0.4)
    ax.plot(np.arange(1860,1860+np.size(a[i])),b[i]-np.mean(b[i][0:20]),color='r',lw=2,alpha=0.4)




for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontsize(fontsize)


for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontsize(fontsize)
    #plt.show()


ax.set_xlim([1860,2100])
ax.set_xlabel('year', fontsize=fsize)
ax.set_ylabel('GtC yr$^{-1}$', fontsize=fsize)
plt.title('RCP4.5 air-sea CO$_2$ flux', fontsize=fsize)
plt.savefig('/home/ph290/Documents/figures/co2_clim_feedbacks.png')
plt.show(block = False)

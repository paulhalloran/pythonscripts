
import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis

#######################
# psl
#######################

file = '/data/NAS-geo01/ph290/misc_data/slp.mnmean.nc'
# hadslp2

cube = iris.load_cube(file,'Monthly Mean Sea Level Pressure')
iris.coord_categorisation.add_year(cube, 'time', name='year')
cube = cube.aggregated_by('year', iris.analysis.MEAN)
coord = cube.coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])

#######################
# volcanic data
#######################


file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)

start_year = np.min(year)
end_year = np.max(year)
start_year = 1850
end_year = 2000


data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1].copy()
data_tmp[:,1] = data2[:,1].copy()
data = np.mean(data_tmp,axis = 1)
voln_n = data1.copy()
voln_n[:,1] = data
volc_data = voln_n[:,1]
volc_yr = voln_n[:,0]
loc = np.where((volc_yr >= start_year) & (volc_yr <= end_year))
volc_yr = volc_yr[loc]
volc_data = volc_data[loc]
tmp = np.round(volc_yr)
tmp_unique = np.unique(tmp)
volc_yr2 = tmp_unique
volc_data2 = tmp_unique.copy()
for count,yr in enumerate(volc_yr2):
	loc = np.where(tmp == yr)
	volc_data2[count] = np.mean(volc_data[loc])
#calculating volcanic forcing from AOD, following Harris and Highwood 2011
# volc_data2 = -11.3 * (1 - np.exp(-0.164*volc_data2))
# volc_data2 = rm.running_mean(volc_data2,10)

loc1 = np.where(volc_data2 >= 0.01)
loc2 = np.where(volc_data2 < 0.01)


cmap1 = mpl_cm.get_cmap('RdBu_r')

qplt.contourf(cube[loc1].collapsed('time',iris.analysis.MEAN) - cube[loc2].collapsed('time',iris.analysis.MEAN),np.linspace(-0.3,0.3,31),cmap = cmap1)
plt.gca().coastlines()
plt.show()

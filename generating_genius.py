#########################################
# Edit the following line as instructed #
#########################################

group = 'group1'

#inspired by: http://www.raspberrypi-spy.co.uk/2015/06/basic-temperature-logging-to-the-internet-with-raspberry-pi/
############################################################################
#  Data published to: https://thingspeak.com/channels/103529/private_show  #
#  Username: exetergenius                                                  #
#  Password: universityofexeter                                            #
############################################################################

import time
import os
import sys
import urllib            # URL functions
import urllib2           # URL functions
import Adafruit_DHT

THINGSPEAKKEY = 'UDLYSREMXOLDGZC8'
THINGSPEAKURL = 'https://api.thingspeak.com/update'

#A function to send the data to the internet
def sendData(url,key,field1,temp):
  """
  Send event to internet site
  """
  values = {'api_key' : key,'field1' : temp}
  postdata = urllib.urlencode(values)
  req = urllib2.Request(url, postdata)
  log = time.strftime("%d-%m-%Y,%H:%M:%S") + ","
  log = log + "{:.1f}C".format(temp) + ","
  try:
    # Send data to Thingspeak
    response = urllib2.urlopen(req, None, 5)
    html_string = response.read()
    response.close()
    log = log + 'Update ' + html_string
  except urllib2.HTTPError, e:
    log = log + 'Server could not fulfill the request. Error code: ' + e.code
  except urllib2.URLError, e:
    log = log + 'Failed to reach server. Reason: ' + e.reason
  except:
    log = log + 'Unknown error'
  print log


#details required by sensor
sensor = Adafruit_DHT.DHT22
pin = 4

#Obtaining details required by website
group_details={}
group_details['group1'] = 'field1'
group_details['group2'] = 'field2'
group_details['group3'] = 'field3'
group_details['group4'] = 'field4'
group_details['group5'] = 'field5'
group_details['group6'] = 'field6'
group_details['group7'] = 'field7'
group_details['group8'] = 'field8'

#An infinite loop to keep making measurements and uploading them to the internet
while True:
	try:
		humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
		sendData(THINGSPEAKURL,THINGSPEAKKEY,group_details[group],temperature)
		#wait for one minute before making next measurement
		time.sleep(60)
	except:
		pass

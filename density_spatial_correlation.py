'''
import cartopy.crs as ccrs
import iris
import iris.plot as iplt
import cartopy


import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
	models = np.unique(models_tmp)
	return models

def ts_model_corr(ts,cube2):
	cube3 = cube2.copy()
	cube3 = iris.analysis.maths.multiply(cube3, 0.0)
	ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
	cube3 = iris.analysis.maths.add(cube3, ts2)
	return iris.analysis.stats.pearsonr(cube2, cube3, corr_coords=['time'])




#start_year = 850
start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)





##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=scipy.signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)


#############################################################################################################################
# Models: remove linear trend and normalise variability, then put all models on same time axis and perform multi-model mean #
#############################################################################################################################

input_dir_1 = '/data/data1/ph290/cmip5/last1000/mld/deep/regridded/'
# input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'
input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger/'
# input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_no_spikes/'

models = model_names(input_directory)
models = list(models)
# models.remove('MIROC-ESM')
# models.remove('FGOALS-gl')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadCM3')
# print '*******************************************'
# print '*******************************************'
# print ' As soon as processed bring MIROC BACK IN! '
# print '*******************************************'
# print '*******************************************'
# models.remove('MIROC-ESM') #temporarily until regridded...
#Note previously had two FGOALS models, sp removed one so a better ensmeble
#FGOALS-gl chosen, because there seems to be a problem calculating its mixed layer depths due to teh levels being upside down
models = np.array(models)


multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()

no_smooth_list = []
five_yr_smooth_list = []
ten_yr_smooth_list = []
window_type = 'boxcar'

for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	cube2 = iris.load_cube(input_dir_1+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2 = signal.detrend(data)
	#data2 = data2-np.min(data2)
	#data3 = data2/(np.max(data2))
	#data3 -= np.nanmean(data3)
	data3 = data2
	data3 -= np.nanmean(data3)
# 	window_type = 'boxcar'
# 	data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	coord = cube2.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs2 = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((yrs >= start_year) & (yrs <= end_year))
	loc2 = np.where((yrs2 >= end_year) & (yrs <= end_year))
	cube = cube[loc]
	cube2 = cube2[loc]
	# ts = cube.data
	# no_smooth_list.append(ts_model_corr(ts,cube2))
	ts = pandas.rolling_window(cube.data,5,win_type=window_type,center=True)
	loc3 = np.where(np.logical_not(np.isnan(ts)))
	five_yr_smooth_list.append(ts_model_corr(ts[loc3],cube2[loc3]))
	# ts = pandas.rolling_window(cube.data,10,win_type=window_type,center=True)
	# loc3 = np.where(np.logical_not(np.isnan(ts)))
	# ten_yr_smooth_list.append(ts_model_corr(ts[loc3],cube2[loc3]))
	# qplt.contourf(out_cube)
	# plt.show()


# mean_cube = no_smooth_list[0].copy()
# mean_cube.data = np.mean([x.data for x in no_smooth_list],axis = 0)
# qplt.contourf(mean_cube)
# plt.show()
#
# mean_cube = no_smooth_list[0].copy()
# mean_cube.data = np.mean([x.data for x in five_yr_smooth_list],axis = 0)
# qplt.contourf(mean_cube)
# plt.show()
#
# mean_cube = no_smooth_list[0].copy()
# mean_cube.data = np.mean([x.data for x in ten_yr_smooth_list],axis = 0)
# qplt.contourf(mean_cube)
# plt.show()


'''

####
# plot
###

mean_cube = five_yr_smooth_list[0].copy()
mean_cube.data = np.mean([x.data for x in five_yr_smooth_list],axis = 0)




plt.close('all')
fig = plt.figure()

# Add a matplotlib Axes, specifying the required display projection.
# NOTE: specifying 'projection' (a "cartopy.crs.Projection") makes the
# resulting Axes a "cartopy.mpl.geoaxes.GeoAxes", which supports plotting
# in different coordinate systems.

crs_latlon = ccrs.PlateCarree()
# crs_latlon = ccrs.RotatedPole(pole_longitude=-20, pole_latitude=50.0)

ax = plt.axes(projection=ccrs.PlateCarree())
# ax = plt.axes(projection=ccrs.RotatedPole(pole_longitude=-20, pole_latitude=50.0))


# Set display limits to include a set region of latitude * longitude.
# (Note: Cartopy-specific).
ax.set_extent((-80, 20, 40, 90.0), crs=crs_latlon)

# Add coastlines and meridians/parallels (Cartopy-specific).
# ax.coastlines(linewidth=0.75, color='navy')
ax.add_feature(cartopy.feature.LAND, zorder=0, edgecolor='black')
# ax.gridlines(crs=crs_latlon, linestyle='-')

# iplt.pcolormesh(mean_cube, cmap='RdBu_r')
# iplt.contourf(mean_cube, cmap='RdBu_r')

my_plot = iplt.contourf(mean_cube, np.linspace(0,1.0,50), cmap='Reds')
bar = plt.colorbar(my_plot, orientation='horizontal')
tick_levels = [0.0,0.2,0.4,0.6,0.8,1.0]
bar.set_ticks(tick_levels)
bar.set_label('Correlation Coefficient')
# lons = mean_cube.coord('longitude').points
# lats = mean_cube.coord('latitude').points
# data = mean_cube.data
# ax.contourf(lons, lats, data, 50,
#             transform=ccrs.PlateCarree(),
#             cmap='Reds')

ax.add_feature(cartopy.feature.LAND, edgecolor='black')


x_lower = -24.0
x_upper = -14.0
y_lower = 65
y_upper = 70

# Draw a margin line, some way in from the border of the 'main' data...
box_x_points = x_lower + (x_upper - x_lower) * np.array([0, 1, 1, 0, 0])
box_y_points = y_lower + (y_upper - y_lower) * np.array([0, 0, 1, 1, 0])
# Get the Iris coordinate sytem of the X coordinate (Y should be the same).
#cs_data1 = x_coord.coord_system
# Construct an equivalent Cartopy coordinate reference system ("crs").
#crs_data1 = cs_data1.as_cartopy_crs()


new_cs = iris.coord_systems.GeogCS(iris.fileformats.pp.EARTH_RADIUS)
mean_cube.coord(axis='x').coord_system = new_cs
x_coord, y_coord = mean_cube.coord(axis='x'), mean_cube.coord(axis='y')
cs_data1 = x_coord.coord_system
crs_data1 = cs_data1.as_cartopy_crs()
# Draw the rectangle in this crs, with matplotlib "pyplot.plot".
# NOTE: the 'transform' keyword specifies a non-display coordinate system
# for the plot points (as used by the "iris.plot" functions).
plt.plot(box_x_points, box_y_points, transform=crs_data1, linewidth=2.0, color='k', linestyle='--')


iplt.show(block = False)

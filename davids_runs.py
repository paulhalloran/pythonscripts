import iris
import matplotlib.pyplot as plt

def area_avg(cube):
    lon_west = -70.0
    lon_east = 5
    lat_south = 10.0
    lat_north = 70.0
    cube = cube.intersection(longitude=(lon_west, lon_east))
    cube = cube.intersection(latitude=(lat_south, lat_north))
    try:
        cube.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'
    try:
        cube.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'
    grid_areas = iris.analysis.cartography.area_weights(cube)
    area_avged_cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
    return area_avged_cube



data1 = '/data/NAS-ph290/shared/ford_data/home/daford/for_alice_paul/mi-al303_free/merged_ann_reg.nc'
data2 = '/data/NAS-ph290/shared/ford_data/home/daford/for_alice_paul/mi-al719_chl/merged_ann_reg.nc'
data3 = '/data/NAS-ph290/shared/ford_data/home/daford/for_alice_paul/mi-al720_sst/merged_ann_reg.nc'
data4 = '/data/NAS-ph290/shared/ford_data/home/daford/for_alice_paul/mi-al723_chl_sst_ice/merged_ann_reg.nc'

cube_free = iris.load_cube(data1)
cube_chl = iris.load_cube(data2)
cube_sst = iris.load_cube(data3)
cube_chl_sst = iris.load_cube(data4)

plt.close('all')
qplt.plot(area_avg(cube_free),label = 'free')
qplt.plot(area_avg(cube_chl),label = 'chl assim')
qplt.plot(area_avg(cube_sst),label = 'sst assim')
qplt.plot(area_avg(cube_chl_sst),label = 'chl and sst assim')
plt.legend(loc=2)
plt.show(block = False)

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation
import matplotlib.gridspec as gridspec
import matplotlib as mpl
import matplotlib.cm as mpl_cm
import matplotlib.ticker as ticker
import cartopy.crs as ccrs


d = '/data/NAS-geo01/ph290/abc_fluxes/'
d1 = 'u-ae956'
d2 = 'u-ah673'
# d3 = 'u-ai713' # mistake - used u-ah673 BGC parameters instead of those from u-ae956
d3 = 'u-al492' # correct run
# d3 = 'u-ai697'      # co2 assimilation


names = ['u-ae956 - control', 'u-ah673 - perturbed BGC', 'u-al492 - u-ah673 26N alkaliniy into u-ae956']

names = ['control run', 'perturbed run', 'control run with 26N alkalinity from purturbed run']


ds = [d1,d2,d3]

lon_west = -180.0
lon_east = 180
lat_south = -90.0
lat_north = 90.0

lon_west2 = -90.0
lon_east2 = 10
lat_south2 = 45.0
lat_north2 = 65.0

lon_west3 = -90.0
lon_east3 = 10
lat_south3 = 26.0
lat_north3 = 45.0

cubes = []
data = []
cubesb = []
datab = []
cubesc = []
datac = []

for i,ds_tmp in enumerate(ds):
    print i
    # c = iris.load_cube(d+ds_tmp+'_talk_year_remap.nc')
    c = iris.load_cube(d+ds_tmp+'_fco2_year_remap.nc')
    iris.coord_categorisation.add_year(c, 'time', name='year')
    try:
        c.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'
    try:
        c.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'
    c_region_tmp = c.intersection(longitude=(lon_west, lon_east))
    c_region = c_region_tmp.intersection(latitude=(lat_south, lat_north))
    cubes.append(c_region)
    grid_areas = iris.analysis.cartography.area_weights(c_region)
    data.append(c_region.collapsed(['latitude','longitude'],iris.analysis.MEAN))
    ###
    c_region_tmp = c.intersection(longitude=(lon_west2, lon_east2))
    c_region = c_region_tmp.intersection(latitude=(lat_south2, lat_north2))
    cubesb.append(c_region)
    grid_areas = iris.analysis.cartography.area_weights(c_region)
    datab.append(c_region.collapsed(['latitude','longitude'],iris.analysis.MEAN))
    ###
    c_region_tmp = c.intersection(longitude=(lon_west3, lon_east3))
    c_region = c_region_tmp.intersection(latitude=(lat_south3, lat_north3))
    cubesc.append(c_region)
    grid_areas = iris.analysis.cartography.area_weights(c_region)
    datac.append(c_region.collapsed(['latitude','longitude'],iris.analysis.MEAN))

"""
plt.close('all')


for i,data_single in enumerate(data):
    print i
    plt.plot(cubes[i].coord('year').points,data_single.data,lw=3,alpha=0.8,label = names[i])


plt.xlim([1982,2008])

plt.xlabel('year')
plt.ylabel('N. Atlantic fCO$_2$')
plt.legend(loc=2)
plt.show(block=True)

y1=1982
y2=2008

c0=cubes[0][np.where((cubes[0].coord('year').points>y1) & (cubes[0].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)
c1=cubes[1][np.where((cubes[1].coord('year').points>y1) & (cubes[1].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)
c2=cubes[2][np.where((cubes[2].coord('year').points>y1) & (cubes[2].coord('year').points<=1996))].collapsed('time',iris.analysis.MEAN)
c2b=cubes[2][np.where((cubes[2].coord('year').points>1998) & (cubes[2].coord('year').points<=1999))].collapsed('time',iris.analysis.MEAN)

plt.close('all')
qplt.contourf(c0-c1,31)
plt.show()

"""
plt.close('all')
cmap1 = mpl_cm.get_cmap('RdBu_r')


y1=1982
y2=2010

c0=cubes[0][np.where((cubes[0].coord('year').points>y1) & (cubes[0].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)
c1=cubes[1][np.where((cubes[1].coord('year').points>y1) & (cubes[1].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)

c0b=cubesb[0][np.where((cubes[0].coord('year').points>y1) & (cubes[0].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)
c1b=cubesb[1][np.where((cubes[1].coord('year').points>y1) & (cubes[1].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)

c0c=cubesc[0][np.where((cubes[0].coord('year').points>y1) & (cubes[0].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)
c1c=cubesc[1][np.where((cubes[1].coord('year').points>y1) & (cubes[1].coord('year').points<=y2))].collapsed('time',iris.analysis.MEAN)

# font = {'family' : 'normal',
#         'weight' : 'normal',
#         'size'   : 16}
#
# mpl.rc('font', **font)
SMALL_SIZE = 12
MEDIUM_SIZE = 22
BIGGER_SIZE = 18

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=MEDIUM_SIZE)  # fontsize of the figure title

plt.close('all')
fig = plt.figure()
fig.set_figheight(15)
fig.set_figwidth(15)
gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)

start_yr = 1980
end_yr = 2010
tick_spacing = 10

a=33
b=66
c=100
gap=5

ax1 = plt.subplot(gs[0:a-gap,55:100],projection=ccrs.PlateCarree())
ax2 = plt.subplot(gs[0:a-gap,0:45])
ax3 = plt.subplot(gs[a+gap:b-gap,55:100],projection=ccrs.PlateCarree())
ax4 = plt.subplot(gs[a+gap:b-gap,0:45])
ax5 = plt.subplot(gs[b+gap:c-gap,55:100],projection=ccrs.PlateCarree())
ax6 = plt.subplot(gs[b+gap:c-gap,0:45])
# ax2 = plt.subplot(gs[60:100,0:45],projection=ccrs.PlateCarree())

cube = c0-c1
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
cdata = cube.data
C1 = ax1.contourf(lons, lats, cdata, np.linspace(-200,200,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax1.coastlines()
ax1.set_title('Control minus perturbed')
cb1 = plt.colorbar(C1,ax=ax1,orientation='horizontal',ticks=[-200,-100, 0, 100,200])
cb1.set_label('CO$_2$ difference ($\mu$atm)')

ax2.plot(cubes[0].coord('year').points,data[0].data,lw=3,alpha=0.8,label = names[0])
ax2.plot(cubes[1].coord('year').points,data[1].data,lw=3,alpha=0.8,label = names[1])
ax2.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
ax2.set_xlabel('year')
ax2.set_ylabel('Global fCO$_2$')
ax2.set_xlim([start_yr,end_yr])
plt.legend(loc=2)


cube = c0b-c1b
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
cdata = cube.data
C3 = ax3.contourf(lons, lats, cdata, np.linspace(-200,200,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax3.coastlines()
ax3.set_title('Control minus perturbed')
cb3 = plt.colorbar(C3,ax=ax3,orientation='horizontal',ticks=[-200,-100, 0, 100,200])
cb3.set_label('CO$_2$ difference ($\mu$atm)')

ax4.plot(cubesb[0].coord('year').points,datab[0].data,lw=3,alpha=0.8,label = names[0])
ax4.plot(cubesb[1].coord('year').points,datab[1].data,lw=3,alpha=0.8,label = names[1])
ax4.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
ax4.set_xlabel('year')
ax4.set_ylabel('Subpolar\nN. Atlantic fCO$_2$')
ax4.set_xlim([start_yr,end_yr])
plt.legend(loc=2)

cube = c0c-c1c
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
cdata = cube.data
C5 = ax5.contourf(lons, lats, cdata, np.linspace(-200,200,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax5.coastlines()
ax5.set_title('Control minus perturbed')
cb5 = plt.colorbar(C5,ax=ax5,orientation='horizontal',ticks=[-200,-100, 0, 100,200])
cb5.set_label('CO$_2$ difference ($\mu$atm)')

ax6.plot(cubesc[0].coord('year').points,datac[0].data,lw=3,alpha=0.8,label = names[0])
ax6.plot(cubesc[1].coord('year').points,datac[1].data,lw=3,alpha=0.8,label = names[1])
ax6.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
ax6.set_xlabel('year')
ax6.set_ylabel('Subtropical\nN. Atlantic fCO$_2$')
ax6.set_xlim([start_yr,end_yr])
plt.legend(loc=2)

plt.savefig('/home/ph290/Documents/figures/abc.png')
plt.show(block=False)

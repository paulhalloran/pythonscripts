import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation


d = '/data/NAS-geo01/ph290/abc_fluxes/'
d1 = 'u-ae956_talk_year.nc'
d2 = 'u-ah673_talk_year.nc'
# d3 = 'u-ai713' # mistake - used u-ah673 BGC parameters instead of those from u-ae956
d3 = 'u-al492_alk_1982_ym.nc' # correct run

ds = [d1,d2,d3]

cubes = []
data = []

for i,ds_tmp in enumerate(ds):
    print i
    c = iris.load_cube(d+ds_tmp,'Alkalinity')
    iris.coord_categorisation.add_year(c, 'time', name='year')
    cubes.append(c[np.where(c.coord('year').points == 1988)])
    #c_region_tmp = c.intersection(longitude=(lon_west, lon_east))
    #c_region = c_region_tmp.intersection(latitude=(lat_south, lat_north))
    #grid_areas = iris.analysis.cartography.area_weights(c)
    data.append(c.collapsed(['latitude','longitude'],iris.analysis.MEAN))
    #, weights=grid_areas)


c0=cubes[0].collapsed(['Vertical T levels','time'],iris.analysis.MEAN)
c1=cubes[1].collapsed(['Vertical T levels','time'],iris.analysis.MEAN)
c2=cubes[2].collapsed(['Vertical T levels','time'],iris.analysis.MEAN)

plt.close('all')
qplt.contourf(c1-c0,31)
plt.show()

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt


d = '/data/NAS-geo01/ph290/abc_fluxes/'
d1 = 'u-ae956' # Control run
d2 = 'u-ah673' # Control run with perturbed biogeochemistry
# d3 = 'u-ai713' # mistake - used u-ah673 BGC parameters instead of those from u-ae956
# d3 = 'u-al492' # correct run
d3 = 'u-ap153' # alkalinity and DIC assimilation at rapud array

names = ['u-ae956 - control', 'u-ah673 - perturbed BGC', 'assim']

ds = [d1,d2,d3]

lon_west = -75.0
lon_east = -7.5
lat_south = 10.0
lat_north = 40.0


plt.close('all')

for i,ds_tmp in enumerate(ds):
		print i
#	try:
		c = iris.load_cube(d+ds_tmp+'_fco2_year_remap.nc')
		try:
		    c.coord('latitude').guess_bounds()
		except:
		    print 'cube already has latitude bounds'
		try:
		    c.coord('longitude').guess_bounds()
		except:
		    print 'cube already has longitude bounds'
		c_region_tmp = c.intersection(longitude=(lon_west, lon_east))
		c_region = c_region_tmp.intersection(latitude=(lat_south, lat_north))
		grid_areas = iris.analysis.cartography.area_weights(c_region)
		c_av = c_region.collapsed(['latitude','longitude'],iris.analysis.MEAN, weights=grid_areas)
		qplt.plot(c_av,label = names[i])
#	except:
#		print ds_tmp+' failed'


plt.legend()
plt.show(block=False)

import numpy as np
import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.coord_categorisation



d = '/data/NAS-geo01/ph290/abc_fluxes/'
d1 = 'u-ae956'
d2 = 'u-ah673'
# d3 = 'u-ai713' # mistake - used u-ah673 BGC parameters instead of those from u-ae956
d3 = 'u-al492' # correct run
# d3 = 'u-ai697'      # co2 assimilation
#purturbrd physics is correct.

names = ['u-ae956 - control', 'u-ah673 - perturbed BGC', 'u-al492 - perturbed BGC alk assimilated into copy of u-ae956']

ds = [d1,d2,d3]

lon_west = -75.0
lon_east = -7.5
lat_south = 0.0
lat_north = 60.0



cubes = []
data = []

for i,ds_tmp in enumerate(ds):
    print i
    c = iris.load_cube(d+ds_tmp+'_talk_year.nc')
    c = c.collapsed(['Vertical T levels'],iris.analysis.MEAN)
    iris.coord_categorisation.add_year(c, 'time', name='year')
    cubes.append(c)
    #c_region_tmp = c.intersection(longitude=(lon_west, lon_east))
    #c_region = c_region_tmp.intersection(latitude=(lat_south, lat_north))
    #grid_areas = iris.analysis.cartography.area_weights(c)
    data.append(c.collapsed(['latitude','longitude'],iris.analysis.MEAN))
    #, weights=grid_areas)

plt.close('all')

for i,data_single in enumerate(data):
    print i
    qplt.plot(data_single,label = names[i])


plt.legend()
plt.show(block=True)


a=1984
b=1985
c0=cubes[0][np.where((cubes[0].coord('year').points>a) & (cubes[0].coord('year').points<=b))].collapsed('time',iris.analysis.MEAN)
c1=cubes[1][np.where((cubes[1].coord('year').points>a) & (cubes[1].coord('year').points<=b))].collapsed('time',iris.analysis.MEAN)
c2=cubes[2][np.where((cubes[2].coord('year').points>1995) & (cubes[2].coord('year').points<=1996))].collapsed('time',iris.analysis.MEAN)
c2b=cubes[2][np.where((cubes[2].coord('year').points>1998) & (cubes[2].coord('year').points<=1999))].collapsed('time',iris.analysis.MEAN)

plt.close('all')
qplt.contourf(c2-c2b,31)
plt.show()

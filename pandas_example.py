import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


file ='WCO_E1_phys_oce_1921-1985_edit.csv'

df = pd.read_csv(file, sep='\t')
df['Date/Time'] = pd.to_datetime(df['Date/Time'])
df.index = df['Date/Time']
del df['Date/Time']

seas_mean_0m = df.loc[df['Depth water [m]'] == 0].resample('Q-DEC') # depth == 0 and seasonal mean starting in nov
ann_mean_0m = df.loc[df['Depth water [m]'] == 0].resample('A')



depth_mean = df.resample('D') # 'cos of the data organisation this is also depth means
seas_mean_depth_mean = depth_mean.resample('Q-DEC') # depth == 0 and seasonal mean starting in nov
ann_mean_depth_mean = depth_mean.resample('A')

plt.close('all')
plt.plot(ann_mean_0m.index, ann_mean_0m.filter(regex='Sal'))
plt.show()

plt.plot(ann_mean_0m.index, ann_mean_0m.filter(regex='Temp')) # note this regex thing is required because the degree symbol was in some funny coding, which caused problems
plt.show()

##################
# ExampleOf how to select just one season. It feels like it should be possible to do this without
# first selecting Temperature or another variable, but i cAN'T EASILY  SEE how to do this
#################

season_to_select = 2
temperature_seas_mean_0m_selected_season = seas_mean_0m.filter(regex='Temp')[(pd.Index(seas_mean_0m.index.quarter).isin([season_to_select]))]

plt.plot(temperature_seas_mean_0m_selected_season.index, temperature_seas_mean_0m_selected_season) # note this regex thing is required because the degree symbol was in some funny coding, which caused problems
plt.show()


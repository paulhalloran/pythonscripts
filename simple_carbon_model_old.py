import numpy as np
import mocsy
import matplotlib.pyplot as plt
from scipy import interpolate

######
# Initial Conditions / Setup
######

sal = 35.0
temp = 15.0
alk = 2.3 # mol/m^3
initial_dic = 2.1 # mol/m^3
dic = initial_dic

mixed_layer_depth = 100.0 #m
fraction_dic_lost_by_mixing_per_day = 3.0e-5 # tune this to get the correct avg. air-sea flux 

U10 = 5.0 # Wind speed (m/s)

spinup_length = 50000 # days

######
# C
######

def calculate_
Sc = 2073.1 - 125.62 * temp + 3.6276*(temp*temp) - 0.043219*(temp*temp*temp) # from Wanninkhof 1992
#Sc = 660
gas_trans_velocity_cm_per_hr = (0.31*U10**2)* (660/Sc)**0.5
gas_trans_velocity_m_per_s = (gas_trans_velocity_cm_per_hr / 100.0) / (60.0 * 60.0)
gas_trans_velocity = gas_trans_velocity_m_per_s


yearsec = 60.0 * 60.0 * 24.0 * 365.0
daysec = 60.0 * 60.0 * 24.0

cmip5_atm_co2_file = '/data/data0/ph290/cmip5/forcing_data/rcp_forcing_data/historical_and_rcp85_atm_co2.txt'
years = np.genfromtxt(cmip5_atm_co2_file,delimiter = ',',skip_header=1)[:,0]
years = years
xco2_ts = np.genfromtxt(cmip5_atm_co2_file,delimiter = ',',skip_header=1)[:,1]

f1 = interpolate.interp1d(years, xco2_ts)
f2 = interpolate.interp1d(years, years)

start_yr = 1860
end_yr = 2013
no_days = (end_yr - start_yr) * 360.0

xco2_ts = f1(np.linspace(start_yr,end_yr,no_days))
years = f2(np.linspace(start_yr,end_yr,no_days))


co2flux_array = np.zeros(np.size(years))
dic_array = np.zeros(np.size(years))
pco2_array = np.zeros(np.size(years))
dpco2_array = np.zeros(np.size(years))
fco2_array = np.zeros(np.size(years))

#####
# spinup
#####

for i,yr in enumerate(np.arange(spinup_length)):
    xco2 = xco2_ts[0]
    co2flux, co2ex, dpco2, ph, pco2, fco2, co2, hco3, co3, OmegaA, OmegaC, BetaD, rhoSW, p, tempis = mocsy.gasx.flxco2(kw660 = gas_trans_velocity, xco2 = xco2, patm = 1.0, dz1 = 10.0, temp = temp, sal = sal,alk = alk, dic = dic, sil = 0, phos = 0, optcon='mol/m3', optt='Tinsitu', optp='db', optb="u74", optk1k2='l', optkf="dg", optgas='Pinsitu')
    #co2flux in mol/(m^2 * s)
    co2flux = co2flux[0]
    mol_co2_m2 = co2flux * daysec
    mol_dic_m2 = dic * mixed_layer_depth
    mol_dic_m2 += mol_co2_m2
    mol_dic_m2 -= mol_dic_m2 * fraction_dic_lost_by_mixing_per_day
    dic = (mol_dic_m2 / mixed_layer_depth)


#####
# simulation
#####

for i,yr in enumerate(years):
    xco2 = xco2_ts[i]
    co2flux, co2ex, dpco2, ph, pco2, fco2, co2, hco3, co3, OmegaA, OmegaC, BetaD, rhoSW, p, tempis = mocsy.gasx.flxco2(kw660 = gas_trans_velocity, xco2 = xco2, patm = 1.0, dz1 = 10.0, temp = temp, sal = sal,alk = alk, dic = dic, sil = 0, phos = 0, optcon='mol/m3', optt='Tinsitu', optp='db', optb="u74", optk1k2='l', optkf="dg", optgas='Pinsitu')
    #co2flux in mol/(m^2 * s)
    co2flux = co2flux[0]
    pco2_array[i] = pco2[0]
    dpco2_array[i] = dpco2[0]
    fco2_array[i] = fco2[0]
    co2flux_array[i] = co2flux
    dic_array[i] = dic
    mol_co2_m2 = co2flux * daysec
    mol_dic_m2 = dic * mixed_layer_depth
    mol_dic_m2 += mol_co2_m2
    mol_dic_m2 -= mol_dic_m2 * fraction_dic_lost_by_mixing_per_day
    dic = (mol_dic_m2 / mixed_layer_depth)


'''
plt.close('all')
plt.plot(years,pco2_array,'b')
plt.plot(years,pco2_array-dpco2_array,'r')
plt.show(block = False)
'''
#co2flux_array_tmp = co2flux_array.copy()

plt.close('all')
plt.plot(years,co2flux_array * yearsec,'b')
plt.ylabel('Flux (mol/m2/yr)')
plt.ylabel('Year')
plt.xlim([1850,2013])
#plt.plot(years,co2flux_array_tmp* yearsec,'r')
plt.show(block = False)

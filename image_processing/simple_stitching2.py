import cv2
import numpy as np
import glob

# ============================================================================

def find_overlap_start(left_img, right_img):
  assert left_img.shape == right_img.shape
  height, width = left_img.shape[:2]

  haystack = left_img
  needle = right_img[0:int(height/4),0:int(width/4)]
  res = cv2.matchTemplate(haystack, needle, cv2.TM_CCORR_NORMED)
  min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)


  print('maxloc: ',max_loc)
  print('maxval: ',max_val)
  return max_loc


def find_overlaps(images):
    overlap_starts_x = []
    overlap_starts_y = []
    for i in range(len(images) - 1):
        tmp = find_overlap_start(images[i], images[i+1])
        overlap_starts_x.append(tmp[0])
        overlap_starts_y.append(tmp[1])
    # And the last image is used whole
    overlap_starts_x.append(images[-1].shape[1])
    overlap_starts_y.append(images[-1].shape[0])
    return overlap_starts_x,overlap_starts_y


# Simple stitch, no blending, right hand slice overlays left hand slice
def stitch_images(images, overlap_starts_x,overlap_starts_y):
    height, width = images[0].shape[:2]
    total_width = sum(overlap_starts_x)
    total_height = sum(overlap_starts_y)
    result = np.zeros((total_height, total_width,3), np.uint8)
 
    current_column = 0
    current_row = 0
    for i,dummy in enumerate(overlap_starts_x):
        start_x = overlap_starts_x[i]
        start_y = overlap_starts_y[i]
        result[current_row:current_row+height,current_column:current_column+width] = images[i]
        current_column += start_x
        current_row += start_y

    return result

# ============================================================================

directory = '/data/NAS-ph290/shared/arctica_peelimages/91A2CNS114/vm/'

files = glob.glob(directory+'0*.JPG')

images = [cv2.imread(file) for file in files[0:8]]

#output directory:
output_directory = '/data/dataSSD0/ph290/tmp/'


overlap_starts_x,overlap_starts_y = find_overlaps(images)

cv2.imwrite(output_directory+"delete.png", stitch_images(images, overlap_starts_x,overlap_starts_y))


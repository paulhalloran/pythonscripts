#NOTE:
# - Images are best taken with overlap of at least 20% in one of the corners (e.g. when taking image ideally have the top right, left or bottom right or left segment of the mage overlkapping with the previous image)
# - Do not rotate the peel just move forward/backwars or sideways
# - try to minimise the amount of edge (i.e. resin) in teh image as far as is possible - the matching algorithm can accidently match resin to resin...
#
#python setup to get this working:
#sudp apt-get install pip3
#pip3 install --upgrade pip
#pip3 install wheel
#pip uninstall opencv-contrib-python
#pip3 install opencv-python
#note, used python3 (note not python2.7)
#run with: 
#python3 simple_stitching3.py
#output written to output_directory+"stitched.jpg" 

#Directory where you want the output:
output_directory = '/data/dataSSD0/ph290/tmp/'
#Directory containing the shell images
#directory = '/data/NAS-ph290/shared/arctica_peelimages/91A2CNS114/vm/'
directory = '/home/ph290/Documents/bivalves/91A2CNS116/'


import cv2
import numpy as np
import glob
import os

# ============================================================================

def autocrop(image, threshold=0):
    """Crops any edges below or equal to threshold

    Crops blank image to 1x1.

    Returns cropped image.

    """
    if len(image.shape) == 3:
        flatImage = np.max(image, 2)
    else:
        flatImage = image
    assert len(flatImage.shape) == 2

    rows = np.where(np.max(flatImage, 0) > threshold)[0]
    if rows.size:
        cols = np.where(np.max(flatImage, 1) > threshold)[0]
        image = image[cols[0]: cols[-1] + 1, rows[0]: rows[-1] + 1]
    else:
        image = image[:1, :1]

    return image



def find_overlap_start(left_img, right_img):
#  assert left_img.shape == right_img.shape
#  method = cv2.TM_CCORR_NORMED # options: see http://docs.opencv.org/2.4/modules/imgproc/doc/object_detection.html?highlight=matchtemplate#matchtemplate
  method = cv2.TM_CCOEFF_NORMED
  method_min_max = 'max' # max or min. see http://docs.opencv.org/2.4/modules/imgproc/doc/object_detection.html?highlight=matchtemplate#matchtemplate
  overlap_fraction = 6 #th
  height, width = right_img.shape[:2]
 
  haystack = left_img
  needle = right_img[0:int(height/overlap_fraction),0:int(width/overlap_fraction)]
  res = cv2.matchTemplate(haystack, needle, method)
  min_val1, max_val1, min_loc1, max_loc1 = cv2.minMaxLoc(res)

  haystack = left_img
  needle = right_img[int(height/overlap_fraction)*(overlap_fraction-1):-1,0:int(width/overlap_fraction)]
  res = cv2.matchTemplate(haystack, needle, method)
  min_val2, max_val2, min_loc2, max_loc2 = cv2.minMaxLoc(res)

  haystack = left_img
  needle = right_img[0:int(height/overlap_fraction),int(width/overlap_fraction)*(overlap_fraction-1):-1]
  res = cv2.matchTemplate(haystack, needle, method)
  min_val3, max_val3, min_loc3, max_loc3 = cv2.minMaxLoc(res)

  haystack = left_img
  needle = right_img[int(height/overlap_fraction)*(overlap_fraction-1):-1,int(width/overlap_fraction)*(overlap_fraction-1):-1]
  res = cv2.matchTemplate(haystack, needle, method)
  min_val4, max_val4, min_loc4, max_loc4 = cv2.minMaxLoc(res)

  if method_min_max == 'max':
    max_vars = [max_val1,max_val2,max_val3,max_val4]
    max_locs = [max_loc1,max_loc2,max_loc3,max_loc4]
    loc = np.where(max_vars == np.max(max_vars))[0][0]
    max_loc = max_locs[loc]
  else:
    min_vars = [min_val1,min_val2,min_val3,min_val4]
    min_locs = [min_loc1,min_loc2,min_loc3,min_loc4]
    loc = np.where(min_vars == np.min(min_vars))[0][0]
    max_loc = min_locs[loc]


  if (loc == 1) or (loc == 3):
    max_loc = [max_loc[0], max_loc[1] - int(height/overlap_fraction)*(overlap_fraction-1)]


  if (loc == 2) or (loc == 3):
    max_loc = [max_loc[0] - int(width/overlap_fraction)*(overlap_fraction-1), max_loc[1]]
  
  return max_loc


def find_overlaps(images):
    overlap_starts_x = []
    overlap_starts_y = []
    for i in range(len(images) - 1):
        tmp = find_overlap_start(images[i], images[i+1])
        overlap_starts_x.append(tmp[0])
        overlap_starts_y.append(tmp[1])
    # And the last image is used whole
    overlap_starts_x.append(images[-1].shape[1])
    overlap_starts_y.append(images[-1].shape[0])
    return overlap_starts_x,overlap_starts_y


# Simple stitch, no blending
def stitch_images(images, overlap_starts_x,overlap_starts_y):
    height, width = images[0].shape[:2]
    height*=2
    width*=2
    height2, width2 = images[1].shape[:2]
    total_width = sum(overlap_starts_x)
    total_height = sum(overlap_starts_y)
    result = np.zeros((height+height2*2, width+width2*2,3), np.uint8)
    current_column = int(width/2)
    current_row = int(height/2)
    for i,dummy in enumerate(overlap_starts_x):
        start_x = overlap_starts_x[i]
        start_y = overlap_starts_y[i]
        print(current_row,current_row+images[i].shape[0],current_column,current_column+images[i].shape[1])
        print(np.shape(images[i]))
        result[current_row:current_row+images[i].shape[0],current_column:current_column+images[i].shape[1]] = images[i]
        current_column += start_x
        current_row += start_y

    return result

# ============================================================================

print(directory)
files = glob.glob(directory+'*.JPG')
files.sort()

def read_file(file):
  img = cv2.imread(file)
  img = cv2.GaussianBlur(img, (5, 5), 0) #
  return img


images = [read_file(file) for file in files[0:4]]


cv2.imwrite(output_directory+"tmp.jpg", images[0])

overlap_starts_x,overlap_starts_y = find_overlaps([images[0],images[1]])
cv2.imwrite(output_directory+"tmp.jpg", stitch_images([images[0],images[1]], overlap_starts_x,overlap_starts_y))

for i,image in enumerate(images[0:-3]):
	print(i)
	tmp_img = cv2.imread(output_directory+"tmp.jpg")
	overlap_starts_x,overlap_starts_y = find_overlaps([tmp_img,images[i+2]])
	tmp_out = autocrop(stitch_images([tmp_img,images[i+2]], overlap_starts_x,overlap_starts_y),0)
	cv2.imwrite(output_directory+"tmp.jpg", tmp_out)


tmp_img = cv2.imread(output_directory+"tmp.jpg")
cv2.imwrite(output_directory+"stitched.jpg", tmp_img)


#NOTE:
# - Images are best taken with overlap of at least 20% in one of the corners (e.g. when taking image ideally have the top right, left or bottom right or left segment of the mage overlkapping with the previous image)
# - Do not rotate the peel just move forward/backwars or sideways
# - try to minimise the amount of edge (i.e. resin) in teh image as far as is possible - the matching algorithm can accidently match resin to resin...
#
#python setup to get this working:
#sudp apt-get install pip3
#pip3 install --upgrade pip
#pip3 install wheel
#pip uninstall opencv-contrib-python
#pip3 install opencv-python
#note, used python3 (note not python2.7)
#run with: 
#python3 simple_stitching3.py
#output written to output_directory+"stitched.jpg" 

#Directory where you want the output:
output_directory = '/data/dataSSD0/ph290/tmp/'
#Directory containing the shell images
# directory = '/data/NAS-ph290/shared/arctica_peelimages/91A2CNS114/vm/'
directory = '/home/ph290/Documents/bivalves/91A2CNS116/'



import cv2
import numpy as np
import glob
import numpy as np,sys

# ============================================================================


def find_overlap_start(left_img, right_img):
#  assert left_img.shape == right_img.shape
#  method = cv2.TM_CCORR_NORMED # options: see http://docs.opencv.org/2.4/modules/imgproc/doc/object_detection.html?highlight=matchtemplate#matchtemplate
  method = cv2.TM_CCOEFF_NORMED
  method_min_max = 'max' # max or min. see http://docs.opencv.org/2.4/modules/imgproc/doc/object_detection.html?highlight=matchtemplate#matchtemplate
  overlap_fraction = 5 #th
  height, width = right_img.shape[:2]

  min_val_tmp=[]
  max_val_tmp=[]
  min_loc_tmp=[]
  max_loc_tmp=[]

  for y in range(overlap_fraction):
    for x in range(overlap_fraction):
      haystack = left_img
      vertical_offset = int(height/overlap_fraction)
      horizontal_offset = int(width/overlap_fraction)
      needle = right_img[vertical_offset*y:vertical_offset*(y+1),horizontal_offset*x:horizontal_offset*(x+1)]  
      res = cv2.matchTemplate(haystack, needle, method)  
      min_val1, max_val1, min_loc1, max_loc1 = cv2.minMaxLoc(res)
      max_loc1 = [max_loc1[0], max_loc1[1] - vertical_offset*y]
      max_loc1 = [max_loc1[0] - horizontal_offset*x, max_loc1[1]]
      min_loc1 = [min_loc1[0], min_loc1[1] - vertical_offset*y]
      min_loc1 = [min_loc1[0] - horizontal_offset*x, min_loc1[1]]
      min_val_tmp.append(min_val1)
      max_val_tmp.append(max_val1)
      min_loc_tmp.append(min_loc1)
      max_loc_tmp.append(max_loc1)



  if method_min_max == 'max':
    max_vars = max_val_tmp
    max_locs = max_loc_tmp
    loc = np.where(max_vars == np.max(max_vars))[0][0]
    max_loc = max_locs[loc]
  else:
    min_vars = min_val_tmp
    min_locs = min_loc_tmp
    loc = np.where(min_vars == np.min(min_vars))[0][0]
    max_loc = min_locs[loc]


  return max_loc


def find_overlaps(images):
    overlap_starts_x = []
    overlap_starts_y = []
    print('finding overlaps')
    for i in range(len(images) - 1):
      print(i)
      tmp = find_overlap_start(images[i], images[i+1])
      overlap_starts_x.append(tmp[0])
      overlap_starts_y.append(tmp[1]) 


# And the last image is used whole
    overlap_starts_x.append(images[-1].shape[0])
    overlap_starts_y.append(images[-1].shape[1])
     
    return overlap_starts_x,overlap_starts_y 

    
# Simple stitch, no blending, right hand slice overlays left hand slice
def stitch_images(images, overlap_starts_x,overlap_starts_y):
	height, width = images[0].shape[:2]
	total_width = sum(np.abs(overlap_starts_x))
	total_height = sum(np.abs(overlap_starts_y))    
	result = np.zeros((total_height*4, total_width*4,3), np.uint8)

	current_row = total_height*2
	current_column = total_width*2
	print('stitching images')
	for i,start in enumerate(images):
		result2=result.copy()
		result[current_row:current_row+np.shape(images[i])[0],current_column:current_column+np.shape(images[i])[1]] = images[i]
		result2b = result2.copy()
		resultb = result.copy()
		loc = np.where((result == 0.0) | (result2 == 0.0))
		resultb[loc] = 0.0
		result2b[loc] = 0.0
		cv2.addWeighted(result2b,0.5, resultb, 1 - 0.5,		0, resultb)
		loc = np.where((result > 0.0) & (result2 > 0.0))
		result[loc] = 0.0
		cv2.add(result,resultb, result)
		#cv2.addWeighted(result2, 0.5, result, 1 - 0.5,		0, result)
		current_column += overlap_starts_x[i]
		current_row += overlap_starts_y[i]

	
# 	A=images[0]
# 	B=images[1]
# 	cv2.addWeighted(A, 0.5, B, 1 - 0.5,
# 		0, B)
# 	cv2.imwrite(output_directory+'Pyramid_blending2.jpg',B)

	cv2.imwrite(output_directory+"edges.jpg", find_edge(result2))
	return result

#this needs some work, but my thinking was that if I coudl find the edges on the overlapping section which are true edg in each of teh joining images, I can set the transparence to zero there, and increas to one on teh oposite side. This is just a mater of multiplying each image by the mask that this creates.
def find_edge(image):
    image2 = np.mean(image,axis=2).copy()
    image3 = image2.copy()
    image3[:] = 0.0
    for i in range(np.shape(image2)[0]):
       for j in range(np.shape(image2)[1]):
          try:
            t1 = image2[i+1,j]
          except:
            t1 = image2[i,j]
          try:
            t2 = image2[i-1,j]
          except:
            t2 = image2[i,j]
          try:
            t3 = image2[i,j+1]
          except:
            t3 = image2[i,j]
          try:
            t4 = image2[i,j-1]
          except:
            t4 = image2[i,j]
          if any([t1 == 0.0, t2 == 0.0, t3 == 0.0 , t4 == 0.0]):
              image3[i,j] = 1.0
    return image3
    

def autocrop(image, threshold=0):
    """Crops any edges below or equal to threshold

    Crops blank image to 1x1.

    Returns cropped image.

    """
    if len(image.shape) == 3:
        flatImage = np.max(image, 2)
    else:
        flatImage = image
    assert len(flatImage.shape) == 2

    rows = np.where(np.max(flatImage, 0) > threshold)[0]
    if rows.size:
        cols = np.where(np.max(flatImage, 1) > threshold)[0]
        image = image[cols[0]: cols[-1] + 1, rows[0]: rows[-1] + 1]
    else:
        image = image[:1, :1]

    return image


# ============================================================================


files = glob.glob(directory+'*.JPG')
files.sort()

images = [cv2.imread(file) for file in files[0:2]]

overlap_starts_x,overlap_starts_y = find_overlaps(images)

print(np.shape(images))
print(np.shape(overlap_starts_x))
cv2.imwrite(output_directory+"stitched.jpg", autocrop(stitch_images(images, overlap_starts_x,overlap_starts_y)))



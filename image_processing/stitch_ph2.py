# USAGE
# specify file locatoins etc
# in the directory panorama-stitching run:
# python stitch_ph.py


########################
# import the necessary packages
########################

import argparse
import imutils
import cv2
import glob
import subprocess
import numpy as np
import numpy
import matplotlib.pyplot as plt
import cv2



########################
#  Specify where input image files are located, and where you want the output file
########################

#images located in:
#directory = '/data/NAS-ph290/shared/paul_copy_delete/umbo/'
directory = '/data/NAS-ph290/shared/arctica_peelimages/91A2CNS114/vm/'

#output directory:
output_directory = '/data/dataSSD0/ph290/tmp/'

########################
#  read in file names
########################

files = glob.glob(directory+'????????.JPG')

########################
# put the file names into numerical order
########################

x = np.zeros(np.size(files))
for i,file in enumerate(files):
	x[i] = int(file.split('/')[-1].split('.')[0])

index = np.argsort(x)
files = np.array(files)[index]

########################
# remove any output files with the smae name (would wna tto change this in the loger term...)
########################

try:
	subprocess.call('rm '+output_directory+'delete.JPG', shell=True)
except:
	print('temporary area already clear')


########################
# stitch images
########################


images = []
for file in files:
	images.append(cv2.imread(file))


#########################
# stitch images
#########################

stitcher = cv2.createStitcher(False)
result = stitcher.stitch(images)


#########################
# safe stitched file
#########################

cv2.imwrite(output_directory+'delete.JPG',result[1])

#########################
# quickly plot image
#########################

#imageFinal = cv2.imread(output_directory+'delete.JPG')
#plt.imshow(imageFinal[:,:,0])
#plt.show(block = False)
#plt.title('very quick image to see what has been stitched - proper image stored in file')

print('stitched image written to: '+output_directory+'delete.JPG')
#cv2.imshow("Stiched Image", imageFinal)
#cv2.waitKey(0)



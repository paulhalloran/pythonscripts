import numpy as np
import cv2
import imutils
import argparse

file = '/data/dataSSD0/ph290/tmp/delete.JPG'

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")

	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)


img = cv2.imread(file)
# img = cv2.fastNlMeansDenoisingColored(img,10,7,21)
# img = cv2.fastNlMeansDenoisingColored(img,30,30,101,51)

# img = imgutils.resize(img, width=800)
# img = cv2.medianBlur(img,5)
# kernel = np.ones((1,1),np.float32)/9

# img2 = cv2.filter2D(img,-1,kernel)

imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
imgray = cv2.fastNlMeansDenoising(imgray,100,3,3)
# imgray = cv2.medianBlur(imgray,1)
# imgray = adjust_gamma(imgray, gamma=5.0)
# clahe = cv2.createCLAHE(clipLimit=5.0, tileGridSize=(10,10))
# imgray = clahe.apply(imgray)
imgray = cv2.equalizeHist(imgray)
# bin = cv2.Canny(imgray,5,1)
bin = cv2.adaptiveThreshold(imgray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 51, 3)
# bin = cv2.medianBlur(bin, 5)

# ret,thresh = cv2.threshold(imggray,200,255,cv2.THRESH_TOZERO)

img2, contours, hierarchy = cv2.findContours(bin,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#

contour_list = []
for contour in contours:
    approx = cv2.approxPolyDP(contour,0.2*cv2.arcLength(contour,False),False)
    test = not(cv2.isContourConvex(contour))
    area = cv2.contourArea(contour)
    perimeter = cv2.arcLength(contour,True)
    length = np.size(contour)
    if ((length > 100) & (area > 1000) & ((length / len(approx)) > 1)):
    # & (area >10) ):
        contour_list.append(contour)


img = cv2.cvtColor(imgray,cv2.COLOR_GRAY2RGB)

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.drawContours(img,contour_list,-1,(255,255,0),3)
cv2.imshow("image", imgray)
cv2.imshow("image", img)
# cv2.imgshow("bin", bin)
cv2.waitKey(0)

"""

img = cv2.imgread(file)
# img = imgutils.resize(img, width=800)
imggray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
bin = cv2.adaptiveThreshold(imggray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 31, 10)
bin = imgutils.resize(bin, width=1300)
cv2.imgshow("Keypoints", bin)
cv2.waitKey(0)
"""

# USAGE
# specify file locatoins etc
# in the directory panorama-stitching run:
# python stitch_ph.py


########################
# import the necessary packages
########################

from pyimagesearch.panorama2 import Stitcher
import argparse
import imutils
import cv2
import glob
import subprocess
import numpy as np


########################
#  Specify where input image files are located, and where you want the output file
########################

#images located in:
directory = '/data/NAS-ph290/shared/arctica_images/shell2/'

#output directory:
output_directory = '/data/dataSSD0/ph290/tmp/'

########################
#  read in file names
########################

files = glob.glob(directory+'*.JPG')

########################
# put the file names into numerical order
########################

x = np.zeros(np.size(files))
for i,file in enumerate(files):
	x[i] = int(file.split('/')[-1].split('.')[0])

index = np.argsort(x)
files = np.array(files)[index]

########################
# remove any output files with the smae name (would wna tto change this in the loger term...)
########################

try:
	subprocess.call('rm '+output_directory+'delete.JPG', shell=True)
except:
	print 'temporary area already clear'

########################
# Read in the 1st file as an image
########################

result = cv2.imread(files[0])
cv2.imwrite(output_directory+'delete.JPG',result)

########################
# remove the 1st file from the file list (so that we can loop though all of the remaining files)
########################

files = files[1:]

########################
# Loop through each file, and join it onto th eimage built up tso fir (initially just the 1st file)
########################

for i,file in enumerate(files):
	imageA = cv2.imread(output_directory+'delete.JPG')
	imageB = cv2.imread(file)
	# stitch the images together to create a panorama
	stitcher = Stitcher()
	(result, vis) = stitcher.stitch([imageA, imageB], showMatches=True)
	cv2.imwrite(output_directory+'delete.JPG',result)

########################
# Tell teh user where the output file is located
########################

print 'stitched image written to: '+output_directory+'delete.JPG'

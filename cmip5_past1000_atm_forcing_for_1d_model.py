


#First we need to import the modules required to do the analysis
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
import scipy.interpolate
import pandas as pd

#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models


def ensemble_names(directory):
        files = glob.glob(directory+'/*.nc')
        ensembles_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        ensembles_tmp.append(file.split('/')[-1].split('_')[4].split('.')[0])
                        ensemble = np.unique(ensembles_tmp)
        return ensemble



'''
Defining directory locations, variable locatoins etc.
You may well need to edit the text within the quotation marks to adapt he script to work with your data
'''


'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space = '/data/dataSSD0/ph290/tmp2/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/NAS-ph290/ph290/cmip5/past1000/'
#Directory where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/NAS-ph290/ph290/cmip5/last1000/atm_forcing/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiments = ['past1000']
#comma separated list of the CMIP5 variables that you want to process (e.g. 'tos','fgco2' etc.)
variables = np.array(['hurs','tas','uas','vas','clt','psl'])

#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.
time_period = 'Amon'

'''
Main bit of code follows...
'''

print '****************************************'
print '** this can take a long time (days)   **'
print '** grab a cuppa, but keep an eye on   **'
print '** this to make sure it does not fail **'
print '****************************************'

print 'Processing data from: '+ input_directory
#This runs the function above to come up with a list of models from the filenames
models = model_names(input_directory)
#models = models[::-1] #reversing so 2nd script can work from other end...

# ensembles = ensemble_names(input_directory)
ensembles = ['r1i1p1']

#These lines (and similar later on) just create unique random filenames to be used as temporary filenames during the processing
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'
temp_file3 = str(uuid.uuid4())+'.nc'
temp_file4 = str(uuid.uuid4())+'.nc'

###############################
# cdo remapnn,lon=20.5/lat=42.5 ifile ofile
##############################
def dms_to_dd(d, m, s):
	dd = d + float(m)/60 + float(s)/3600
	return dd


lat = dms_to_dd(66, 31,59)
lon = dms_to_dd(-18, -11,-74)





from itertools import product
from string import ascii_lowercase
alpabetical_string = [''.join(p) for p in product(ascii_lowercase, repeat=3)]



#Looping through each model we have
for model in models:
	for ensemble in ensembles:
		print model + ' ' + ensemble
		#looping through each experiment we have
		for experiment in experiments:
			print experiment
			files = glob.glob(input_directory+'*'+model+'*'+ensemble+'*.nc')
			#Looping through each variable we have
			for i,var in enumerate(variables):
				#testing if the output file has alreday been created
				tmp = glob.glob(output_directory+model+'_'+variables[i]+'_'+experiment+'*'+ensemble+'_timeseries.nc')
				temp_file1 = str(uuid.uuid4())+'.nc'
				temp_file2 = str(uuid.uuid4())+'.nc'
				temp_file3 = str(uuid.uuid4())+'.nc'
				temp_file4 = str(uuid.uuid4())+'.nc'
				if np.size(tmp) == 0:
					#reads in the files to process
					print 'reading in: '+var+'_'+time_period+'_'+model
					files = glob.glob(input_directory+'/'+var+'*'+time_period+'*_'+model+'_*'+experiment+'*'+ensemble+'*.nc')
					for j,file in enumerate(files):
						subprocess.call(['cdo -P 8 remapnn,lon='+str(lon)+'/lat='+str(lat)+' '+file+' '+temporary_file_space+alpabetical_string[j]+temp_file1], shell=True)
					subprocess.call(['cdo -P 8 mergetime '+temporary_file_space+'*'+temp_file1+' '+output_directory+model+'_'+variables[i]+'_'+experiment+'_'+ensemble+'_timeseries.nc'], shell=True)
					subprocess.call('rm '+temporary_file_space+'*'+temp_file1, shell=True)



ensemble = ensembles[0]
experiment = experiments[0]
data={}
min_yr = 900
max_yr=1800
new_x = range((max_yr-min_yr)*365)
for model in models:
	print 'processing '+model
	try:

		for i,var in enumerate(variables):
			print 'processing '+var
			cube = iris.load_cube(output_directory+model+'_'+var+'_'+experiment+'_'+ensemble+'_timeseries.nc')[:,0,0]
			iris.coord_categorisation.add_year(cube, 'time', name='year')
			yrs = cube.coord('year').points
			iris.coord_categorisation.add_month_number(cube, 'time', name='month')
			iris.coord_categorisation.add_day_of_year(cube, 'time', name='day')
			dys = cube.coord('day').points
			days_sequential = ((yrs-min_yr)*365)+dys
			data[var] = scipy.interpolate.interp1d(days_sequential, cube.data, kind='cubic')(new_x)



		##################################
		# process the read-in data and get in the right format for output
		##################################

		#To make things neeter, we putting the data, which has so far been stored in a dictionary into a pandas dataframe
		#Once in this format it just makes other thinsg we may want to do easier. See https://www.tutorialspoint.com/python_pandas/python_pandas_dataframe.htm
		df = pd.DataFrame(data=data)
		df['day_number'] = df.index
		# df['day_number'] = [format(x, ' 5d') for x in range(1, len(df) + 1)]
		#this simply converst the data in the 2m air temperature column from K to degrees C
		df['tas'] = df['tas'] - 273.15
		#this gets the pressure in the units used by bob's model
		df['psl'] = df['psl'] / 100.0
		#As far as I could see, NCEP did not supply wind speed, so I've calculated it using pythagoras (square root of the sum of the squares of the x and y vector give teg lenth of the 3rd side of the triangle)
		df['wind_speed'] = np.sqrt(np.square(df['uas']) + np.square(df['vas']))
		# df['wind_speed'][0:120] = 0.0
		# df['wind_speed'][-120::] = 0.0
		#similarly, wind direction was not supplied, but this can be calculated using the function arctan2, then converted from radians to degrees
		df['wind_direction'] = np.rad2deg((np.arctan2(df['vas'],df['uas'])) + np.pi)
		#In bob's original meterological file the data is all to two decimal places, so the line below rounds all of teh data to two decimal places.
		df = df.round(2)

		df['day_number'] = df.index

		use_cols=['day_number','wind_speed','wind_direction','clt','tas','psl','hurs']

		output_file_name = output_directory+'meterological_data_'+model+'.dat'
		outF = open(output_file_name, "w")
		for j in range(len(df)-1):
		   format=["%9i","%10.2f","%10.2f","%10.2f","%10.2f","%10.2f","%10.2f"]
		   outF.write(''.join(format[i] % df[use_cols[i]].iloc[j+1] for i in range(7)))
		  # write line to output file
		   outF.write("\n")


		outF.close()

	except:
		print 'model '+model+' failed'

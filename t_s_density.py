import seawater as sw
import numpy as np
import matplotlib.pyplot as plt

no1 = 50
no2 = 55

T = np.linspace(-5,30,no1)
S = np.linspace(30,38,no2)
p = np.zeros([no1,no2])

#T+=273.15

for i,t in enumerate(T):
	for j,s in enumerate(S):
		p[i,j]=sw.dens(s,t,0)


plt.close('all')
plt.contourf(S,T,p,100)
CS = plt.contour(S,T,p,15,colors='k')
plt.clabel(CS, inline=1, fontsize=12,color='k')
#plt.plot([33,35,35,33,33],[0,0,5,5,0],'r',lw=3)
plt.xlabel('Salinity (psu)')
plt.ylabel('Temperature ($^o$C)')
plt.title('Seawater density')
plt.savefig('/home/ph290/Documents/figures/t_s_dens2.png')
plt.show(block = False)


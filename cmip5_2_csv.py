import numpy as np
import iris
import iris.coord_categorisation


year = 2100

c=iris.load_cube('tas_Amon_IPSL-CM5A-MR_rcp85_r1i1p1_200601-210012_1deg.nc')
iris.coord_categorisation.add_year(c, 'time', name='year')
loc=np.where(c.coord('year').points==year)[0]
c2 = c[loc].collapsed('time',iris.analysis.MEAN)

lat = c2.coord('latitude').points
lon = c2.coord('longitude').points
data = c2.data

text_file = open("IPSL-CM5A-MR_tas_"+str(year)+".csv", "w")
text_file.write("lat,lon,data\n")
text_file.close()

text_file = open("IPSL-CM5A-MR_tas_"+str(year)+".csv", "a")
for i in lat:
  for j in lon:
  	text_file.write(str(lat[i])+","+str(lon[j])+","+str(data[i,j])+"\n")
  	

text_file.close()

#/data/dataSSD0/ph290/tmp/IPSL-CM5A-MR_tas_2017.csv
#/data/dataSSD0/ph290/tmp/IPSL-CM5A-MR_tas_2100.csv

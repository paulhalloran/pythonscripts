import numpy as np
import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import glob

def ocean_total(cube):
    lower_depths=np.array([50,  120,  220,  360,  550,  790, 1080, 1420, 1810, 2250, 2740, 3280, 3870, 4510, 5200])
    lower_depths2 = np.concatenate([np.array([0]),lower_depths])
    thickness=lower_depths-lower_depths2[0:-1]
    try:
        cube.coord('latitude').guess_bounds()
    except:
        pass
    try:
        cube.coord('longitude').guess_bounds()
    except:
        pass
    grid_areas = iris.analysis.cartography.area_weights(cube)
    cube2 = cube.collapsed(['longitude', 'latitude'], iris.analysis.SUM, weights=grid_areas)
    level_totals = cube2.data.data * thickness
    total = np.sum(level_totals)
    return total


def area_avg(cube):
    try:
        cube.coord('latitude').guess_bounds()
    except:
        pass
    try:
        cube.coord('longitude').guess_bounds()
    except:
        pass
    grid_areas = iris.analysis.cartography.area_weights(cube)
    return cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

GLODAP_tmp = area_avg(iris.load_cube('/data/NAS-ph290/ph290/observations/GLODAPv2_Mapped_Climatologies/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TAlk.nc','seawater alkalinity expressed as mole equivalent per unit mass'))
WOA = area_avg(iris.load_cube('/data/NAS-ph290/ph290/observations/GLODAPv2_Mapped_Climatologies/woa99_temperature_annual_1deg.nc','sea_water_temperature'))[0]

GLODAP_alk = WOA.copy()
GLODAP_alk.data = GLODAP_tmp.data

plt.close('all')

files = glob.glob('/home/ph290/Downloads/low_carbonate_diss/ALK_*.nc')
files2 = glob.glob('/home/ph290/Downloads/low_carbonate_diss/DIC_*.nc')
for i,file in enumerate(files):
    print ocean_total(iris.load_cube(files2[i]))
    c = area_avg(iris.load_cube(file))
   # c = area_avg(iris.load_cube(file))/1.023
    qplt.plot(c,'r')


files = glob.glob('/home/ph290/Downloads/weronika_experiment_one/ALK_*.nc')
files2 = glob.glob('/home/ph290/Downloads/weronika_experiment_one/DIC_*.nc')
for i,file in enumerate(files):
    print ocean_total(iris.load_cube(files2[i]))
    c = area_avg(iris.load_cube(file))
   # c = area_avg(iris.load_cube(file))/1.023
    qplt.plot(c,'b')



files = glob.glob('/home/ph290/Downloads/fish_diss/ALK_*.nc')
files2 = glob.glob('/home/ph290/Downloads/fish_diss/DIC_*.nc')
for i,file in enumerate(files):
    print ocean_total(iris.load_cube(files2[i]))
    c = area_avg(iris.load_cube(file))
   # c = area_avg(iris.load_cube(file))/1.023
    qplt.plot(c,'g')


qplt.plot((GLODAP_alk)/1000.0,'k',lw=3)
plt.show(block = False)

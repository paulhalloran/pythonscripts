'''
line graph of enso variability through time
line graph of preciptation (global average) and enso variability through time
statistics on teh strength of teh enso precip correlation
scatter plot of ENSO v. Precip with a best fit line
Map of teh correlation of ENSO with precip (spatially)
'''



from numpy import *
from matplotlib.pyplot import *
from iris import *
from iris.analysis import *
import iris.quickplot as quickplot
from iris.coord_categorisation import *
from iris.analysis.cartography import *
from scipy.stats import *
from scipy.stats.mstats import *

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
file1 = 'FGOALS-s2_tos_past1000_r1i1p1_regridded_not_vertically.nc'
file2 = 'FGOALS-s2_pr_past1000_r1i1p1_regridded_not_vertically_Omon.nc'

#Reading SST in
sst_cube = load_cube(directory+file1)

#extracting NINO3.4 box
west = -170
east = 120
south = -5
north = 5

temporary_cube = sst_cube.intersection(longitude = (west, east))
my_regional_cube = temporary_cube.intersection(latitude = (south, north))

#Calculates the spatial average
try:
	my_regional_cube.coord('latitude').guess_bounds()
	my_regional_cube.coord('longitude').guess_bounds() #These two lines are just something you sometimes have to do when the original data did not include all of the latitude/longitude data we require
except:
	pass
	
	
grid_areas = area_weights(my_regional_cube)
enso_average_variable = my_regional_cube.collapsed(['latitude', 'longitude'],MEAN, weights=grid_areas) #This does the clever maths to work out the grid box areas 

#Same thing for precipitation:
#Reading Precip in
pr_cube = load_cube(directory+file2)

#Calculates the spatial average
try:
	pr_cube.coord('latitude').guess_bounds()
	pr_cube.coord('longitude').guess_bounds() #These two lines are just something you sometimes have to do when the original data did not include all of the latitude/longitude data we require
except:
	pass
	
	
grid_areas = area_weights(pr_cube)
pr_global_average_variable = pr_cube.collapsed(['latitude', 'longitude'],MEAN, weights=grid_areas) #This does the clever maths to work out the grid box areas 


fig, ax1 = subplots()
ax1.plot(enso_average_variable.data,'red')
ax2 = ax1.twinx()
ax2.plot(pr_global_average_variable.data,'blue')
show()

print spearmanr(enso_average_variable.data,pr_global_average_variable.data)


slope, intercept, r_value, p_value, std_err = linregress(enso_average_variable.data,pr_global_average_variable.data)

scatter(enso_average_variable.data,pr_global_average_variable.data)
x = enso_average_variable.data
y = slope*enso_average_variable.data+intercept
plot(x,y)
show() 

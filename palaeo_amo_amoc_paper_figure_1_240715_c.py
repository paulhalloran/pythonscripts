
from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean as rm
import running_mean_post as rmp
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import scipy.interpolate
import gc
import pickle
import biggus
import seawater
import cartopy.feature as cfeature
import scipy.ndimage
import scipy.ndimage.filters
import gsw
import scipy.stats as stats
import time
import matplotlib.patches as mpatches


save_file = open('/home/ph290/Documents/logs/palaeo_amo_amoc_paper_figures_log_III.txt','w')
time_now = str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)
save_file.write('started: '+time_now)
save_file.close()


# Note - all model data regridded bilinearly on to 1x1 degree unless velocity, which is gridded to 1.4 degree
# Figure 1:
# Models used for AMOC and tas analysis: print all_models
# Steps:
# 1) Regrid v-velocity data to 1/4 degree horizontally (bilinear interpolation), but keeping the same vertical levels (done in other script)
# 2) Create a mask of the Atlantic (1st idem below)
# 3) Makeing the different models, calculate the maximum overturning stream function for those models at 26N (and 45N) (collapse along longitudes, then do cumulative sum top to bottom - or is it vice cerse, should not matter)
# CHECK MASK IS CORRECT WIUTH NEW RESOLUTOIN!
#      this is held in 'max_strm_fun_26'
# 4) Read in model tas and area average this across the AMO box
# 5) Read in Mann AMO data
#    	- select just the years between 850 and 1850
#    	- high-pass filter  to remove variability with a period longer than 100 year
# 	- subtract the min value and divide by the max minus the min (i.e. converting all to a range from 0 to 1)
# 6) Read in crowley unterman volcanic aod data for N and S. hemisphere respectively
# 7) Using all models (but just 1st GISS ens. member (1st ensemble forcing) - NO NOT USING GIS'COS TERRIBLE AMOC DRIFT)
# 	- loop through models:
# 		- select just the years between 850 and 1850
# 		- high-pass filter  to remove variability with a period longer than 100 year
# 		- subtract the min value and divide by the max minus the min (i.e. converting all to a range from 0 to 1)
# 		NO- apply a running mean with a smoothing of 5 years ('smoothing_val')
# 		- apply 3 year low-pass filter
# 		- plot as 'CMIP5/PMIP3 ensemble member'
# 		- mean across ensemble members to plot as 'CMIP5/PMIP3 ensemble mean'
# #DON't do this if I can doload the remeiniug data - trying now... 7b) remove FGOALS-gl from the model list because this does not start in the year 850, it starts in the year 1000 and because other FGOALS model does
# #DON't do this if I can dowload the remeiniug data - trying now... 7c) MRI-CGCM3 removed from the model list because stream function timeseries too short. Look into this was something wrong with input files?
# GISS-E2-R ALSO REMOVED BECAUSE IT's stream fuction  is massively drifting
# Other FGOALES also removed
# #NOTE so_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc is missing and not on the CMIP5 archive. This causes problems
#
# 8) Extract 850 to 1850 years (check why this is not 850-1850) from the Crowley volcanic index
# 	- Apply a 7-year running mean to this data (identified in a monticarlo analysis as having the best explanatory power of the SSTs)
# 	- Interpolate multi-model mean PMIP3 on to the approx. daily timescale of the volcanic data and produce an ordinary least squares linear model explaining this SST with the smoothed volcanic data
# 	- plot the tas and volcanic-based model of tas
# 9) Using all models (but just 1st GISS ens. member (1st ensemble forcing))
# 	- loop through models:
# 		- select just the years between 850 and 1850
# 		- high-pass filter  to remove variability with a period longer than 100 year
# 		- subtract the min value and divide by the max minus the min (i.e. converting all to a range from 0 to 1)
# 		- apply a running mean with a smoothing of 5 years ('smoothing_val')
# 		- plot as 'CMIP5/PMIP3 ensemble member'
# 		- mean across ensemble members to plot as 'CMIP5/PMIP3 ensemble mean', smooth (smoothing of 5 years - why not 10? - smoothing_val = 5) and plot
# 		- NOTE: we are normalising before meaning together. This is why the data does not fill the whole 0-1 range
#
# Figure 2:
# - Identify which models have tos, sos and precipitation - this model list is called: models_tas_sos_pr
# - Define N. Iceland region:
# west = -24
# east = -13
# south = 65
# north = 67
# - Sequentially read in model tas, sos and pr data
# 	- Extract above identified region and area average
# 	- convert temperature from kelvin to Celsius
# 	- Calculate surface ocean density in three ways:
# 		- using raw t and s
# 		- holding T at its mean value throughout the first 1000 years of the run (because some run on to 2005)
# 		- holding salinity at its mean value from throughout the first 1000 years of the run (because some run on to 2005)
# 	- Accumulate all of the data from each iteration (including the non-density data) into a dictionary called 'density_data'
# - Looping through this dictionary I:
# 	- high-pass filter (100 years)
# 	- Normalise non-density data (take of mean and divide by range) - note that the script did not originally do this, so if problems, examine this part of script
# 	- collapse models together to produce multi-model means
# -plotting:
# - Panel 1:
# 	- With a smoothing window of 5 years...
# 	- Read in Reynolds d180 and high-pass filter (100 years)
# 	- perform running mean (5-year - why not longer? - 'tmp = rm.running_mean(tmp,smoothing_val)')
# 	- Plot Reynolds d18O
# 	- Plot tas as in figure 1
# - Panel 2:
# 	- Plot AMOC as in figure 1
# 	- plot multi-model mean density (currently normalised, but might be best not to have this)
# - Panel 3:
# 	- plot multi model mean density, then the same thing but calculated with constant temperature and constant salinity
# - Panel 4:
# 	- plotting multi-model salinity and precipitation and N. hem. volcanoes
#
# Figure 3:
# #ERA NAO
# - Read in ERA-interim 'moisture flux'
# - high-pass filtering this data long the time axis (note this was not done in the previous script)
# - Read in winter NAO from http://www.cpc.ncep.noaa.gov/products/precip/CWlink/pna/JFM_season_nao_index.shtml
# - high-pass filtering this data long the time axis (note this was not done in the previous script)
# - Subtract the mean from the NAO index
# - identify where the NAO index is above and below zero
# - Average together the ERA moisture flux from all of the high NAO years and all of the low NAO years
# - Extract the high-latitude region for plotting
# 	- NOTE - COULD HIGH PASS FILTER THE NAO AND ERA DATA?
# #PMIP3 salinity/precip
# - Using the multi-model mean precip. timeseries calculated for N. Iceland
# 	- avoid the each-end 100 years (to avoid problems with the high-pass filter)
# 	- read in the precip. from each model sequentially
# 		- high-pass filter in the time direction
# 		- Identify the years that correspond to high and low N. Iceland salinity
# 		- Mean these two sets of years independently
# 		- Put into 3-D arrays (model-lat-lon)
# 	- mean together 3-D array along the 'model' axis
# 	- Extract the polar(ish) region, holding results in pr_cube_high_mean and pr_cube_low_mean
# #PMIP3 volcanoes/precip
# 	- Almost exactly as above, but using volcanic index rather than salinity timeseries
# 		- differences:
# 			- (still avoiding 100 years at each end)
# 			- Taking volcanic years that are above or below the median to give a reasonable number
# #Plotting:
# - Plotting low minus high for PMIP3
# - Plotting high minus low for ERA - check sign (remember moisture flux rather than precip. Can we do this a better way?)
#
#
# NOTE: bcc-csm1-1 has a jump in stream function after about 350 years - sort out...
# Either sort out of excluse form analysis. Exlcusing to start with....



###
#Filter
###

# N=5.0
# #N is the order of the filter - i.e. quadratic
# timestep_between_values=1.0 #years value should be '1.0/12.0'
# low_cutoff=100.0
#
# Wn_low=timestep_between_values/low_cutoff
#
# b, a = scipy.signal.butter(N, Wn_low, btype='high')



def extract_years(cube):
    try:
    	iris.coord_categorisation.add_year(cube, 'time', name='year2')
    except:
    	'already has year2'
    start_year = 850
    end_year = 1850
    loc = np.where((cube.coord('year2').points >= start_year) & (cube.coord('year2').points <= end_year))
    loc2 = cube.coord('time').points[loc[0][-1]]
    cube = cube.extract(iris.Constraint(time = lambda time_tmp: time_tmp <= loc2))
    return cube

def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a



def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


def butter_bandpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

#b, a = butter_bandpass(1.0/100.0, 1)

# N=5.0
# #N is the order of the filter - i.e. quadratic
# timestep_between_values=1.0 #years value should be '1.0/12.0'
# low_cutoff=100.0
#
# Wn_low=low_cutoff/(timestep_between_values * 0.5)
#
# b, a = scipy.signal.butter(N, Wn_low, btype='high')

'''
'''
#producing an Atlantic mask (mask masked and Atlantic has value of 1, elsewhere zero) to use in the stream function calculation

"""
#/data/NAS-ph290/ph290/cmip5/last1000_vo_amoc
#input_file = '/data/temp/ph290/regridded/cmip5/last1000_vo_amoc_high_res/CCSM4_vo_past1000_r1i1p1_regridded_not_vertically.nc'
input_file = '/media/usb_external1/tmp/MRI-CGCM3_vo_past1000_r1i1p1_regridded_not_vertically.nc'
cube = iris.load_cube(input_file)
cube = cube[0,0]
cube.data = ma.masked_where(cube.data == 0,cube.data)
#tmp = cube.lazy_data()
#tmp = biggus.ma.masked_where(tmp.ndarray() == 0,tmp.masked_array())

resolution = 0.25

start_date = 850
end_date = 1850

tmp_cube = cube.copy()
tmp_cube = tmp_cube*0.0

location = -30/resolution

print 'masking forwards'

for y in np.arange(180/resolution):
    print 'lat: ',y,'of ',180/resolution
    flag = 0
    tmp = tmp_cube.data.mask[y,:]
    tmp2 = tmp_cube.data[y,:]
    for x in np.arange(360/resolution):
        if tmp[location] == True:
            flag = 1
        if ((tmp[location] == False) & (flag == 0)):
            tmp2[location] = 1
        tmp = np.roll(tmp,+1)
        tmp2 = np.roll(tmp2,+1)
    tmp_cube.data.mask[y,:] = tmp
    tmp_cube.data.data[y,:] = tmp2.data

location = location+1

print 'masking backwards'

for y in np.arange(180/resolution):
    print 'lon: ',y,'of ',180/resolution
    flag = 0
    tmp = tmp_cube.data.mask[y,:]
    tmp2 = tmp_cube.data[y,:]
    for x in np.arange(360/resolution):
        if tmp[location] == True:
            flag = 1
        if ((tmp[location] == False) & (flag == 0)):
            tmp2[location] = 1
        tmp = np.roll(tmp,-1)
        tmp2 = np.roll(tmp2,-1)
    tmp_cube.data.mask[y,:] = tmp
    tmp_cube.data.data[y,:] = tmp2.data

tmp_cube.data.data[150/resolution:180/resolution,:] = 0.0
tmp_cube.data.data[0:40/resolution,:] = 0.0
tmp_cube.data.data[:,20/resolution:180/resolution] = 0.0
tmp_cube.data.data[:,180/resolution:280/resolution] = 0.0

loc = np.where(tmp_cube.data.data == 0.0)
tmp_cube.data.mask[loc] = True

mask1 = tmp_cube.data.mask
cube_test = []

f.write('maske created\n')

'''
#calculating stream function
'''

#trying with the 1/4 degree dataset rather than the 1x1 - this should make the stram function calculatoi nmore robust
files = glob.glob('/data/temp/ph290/regridded/cmip5/last1000_vo_amoc_high_res/*_vo_*.nc')
#/media/usb_external1/cmip5/last1000_vo_amoc



models = []
max_strm_fun = []
max_strm_fun_26 = []
max_strm_fun_45 = []
model_years = []

f.write('files working with:\n')
f.write(str(files)+'\n')

for file in files:

    model = file.split('/')[7].split('_')[0]
    print model
    f.write('processing: '+model+'\n')
    models.append(model)
    cube = iris.load_cube(file)

    print 'applying mask'

    try:
                    levels =  np.arange(cube.coord('depth').points.size)
    except:
                    levels = np.arange(cube.coord('ocean sigma over z coordinate').points.size)

	#for level in levels:
#		print 'level: '+str(level)
#		for year in np.arange(cube.coord('time').points.size):
#			#print 'year: '+str(year)
#			tmp = cube.lazy_data()
#			mask2 = tmp[year,level,:,:].masked_array().mask
#			tmp_mask = np.ma.mask_or(mask1, mask2)
#			tmp[year,level,:,:].masked_array().mask = tmp_mask

    #variable to hold data from first year of each model to check
    #that the maskls have been applied appropriately

    cube.coord('latitude').guess_bounds()
    cube.coord('longitude').guess_bounds()
    grid_areas = iris.analysis.cartography.area_weights(cube[0])
    grid_areas = np.sqrt(grid_areas)

    shape = np.shape(cube)
    tmp = cube[0].copy()
    tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
    tmp = tmp.collapsed('longitude',iris.analysis.SUM)
    collapsed_data = np.tile(tmp.data,[shape[0],1,1])

    mask_cube = cube[0].copy()
    tmp_mask = np.tile(mask1,[shape[1],1,1])
    mask_cube.data.mask = tmp_mask
    mask_cube.data.mask[np.where(mask_cube.data.data == mask_cube.data.fill_value)] = True

    print 'collapsing cube along longitude'
    try:
            slices = cube.slices(['depth', 'latitude','longitude'])
    except:
            slices = cube.slices(['ocean sigma over z coordinate', 'latitude','longitude'])
    for i,t_slice in enumerate(slices):
            #print 'year:'+str(i)
        tmp = t_slice.copy()
        tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
        tmp *= grid_areas
        mask_cube_II = tmp.data.mask
        tmp.data.mask = mask_cube.data.mask | mask_cube_II
        #if i == 0:
            #plt.close('all')
            #qplt.contourf(tmp[0])
            #plt.savefig('/home/ph290/Documents/figures/test/'+model+'l1.png')
            #plt.close('all')
            #qplt.contourf(tmp[10])
            #plt.savefig('/home/ph290/Documents/figures/test/'+model+'l10.png')

        collapsed_data[i] = tmp.collapsed('longitude',iris.analysis.SUM).data

    try:
            depths = cube.coord('depth').points*-1.0
            bounds = cube.coord('depth').bounds
    except:
            depths = cube.coord('ocean sigma over z coordinate').points*-1.0
            bounds = cube.coord('ocean sigma over z coordinate').bounds
    thickness = bounds[:,1] - bounds[:,0]
    test = thickness.mean()
    if test > 1:
            thickness = bounds[1:,0] - bounds[0:-1,0]
            thickness = np.append(thickness, thickness[-1])

    thickness = np.flipud(np.rot90(np.tile(thickness,[180/resolution,1])))

    tmp_strm_fun_26 = []
    tmp_strm_fun_45 = []
    tmp_strm_fun = []
    for i in np.arange(np.size(collapsed_data[:,0,0])):
            tmp = collapsed_data[i].copy()
            tmp = tmp*thickness
            tmp = np.cumsum(tmp,axis = 1)
            tmp = tmp*-1.0*1.0e-3
            tmp *= 1029.0 #conversion from m3 to kg
            #tmp = tmp*1.0e-7*0.8 # no idea why I need to do this conversion - check...
            coord = t_slice.coord('latitude').points
            loc = np.where(coord >= 26)[0][0]
            tmp_strm_fun_26 = np.append(tmp_strm_fun_26,np.max(tmp[:,loc]))
            loc = np.where(coord >= 45)[0][0]
            tmp_strm_fun_45 = np.append(tmp_strm_fun_45,np.max(tmp[:,loc]))
            tmp_strm_fun = np.append(tmp_strm_fun,np.max(tmp[:,:]))

    coord = cube.coord('time')
    dt = coord.units.num2date(coord.points)
    years = np.array([coord.units.num2date(value).year for value in coord.points])
    model_years.append(years)

    max_strm_fun_26.append(tmp_strm_fun_26)
    max_strm_fun_45.append(tmp_strm_fun_45)
    max_strm_fun.append(tmp_strm_fun)


f.write('saving output to /home/ph290/Documents/python_scripts/pickles/palaeo_amo_III.pickle\n')
with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_IIII.pickle', 'w') as f:
    pickle.dump([models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location], f)

f.close()

"""



with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_IIII.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)

###########################################
#    restrict loading to top 1000m        #
###########################################

chosen_levels = lambda cell: cell <= 500
level_above_1000 = iris.Constraint(depth=chosen_levels)

models.remove('MRI-CGCM3') #WOUDL BE GREAT TO SORT THIS OUT!
#models.remove('FGOALS-gl')
models.remove('FGOALS-s2')
# models.remove('bcc-csm1-1')
#T and/or  S filed are funny. making desnity splotch at high lats, and therefor eMLD go screwy
#models.remove('CCSM4')
#Funny last density year - need to resolve...cd Documentspyth
#Had to multiply salinity in last two files by 1000 to get in correct units...

#FGOALS-gl No sea-ice
#models.remove('FGOALS-s2')
#FGOALS models lack the required seaice data...
#and levels are upside down or something odd
#NOTE, now resolved by inverting levels using cdo invertlev ifile ofile

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'

######################################
#   MLD                              #
######################################

def mld(S,thetao,depth_cube,latitude_deg):
	"""Compute the mixed layer depth.
	Parameters
	----------
	SA : array_like
		 Absolute Salinity  [g/kg]
	CT : array_like
		 Conservative Temperature [:math:`^\circ` C (ITS-90)]
	p : array_like
		sea pressure [dbar]
	criterion : str, optional
			   MLD Criteria
	Mixed layer depth criteria are:
	'temperature' : Computed based on constant temperature difference
	criterion, CT(0) - T[mld] = 0.5 degree C.
	'density' : computed based on the constant potential density difference
	criterion, pd[0] - pd[mld] = 0.125 in sigma units.
	`pdvar` : computed based on variable potential density criterion
	pd[0] - pd[mld] = var(T[0], S[0]), where var is a variable potential
	density difference which corresponds to constant temperature difference of
	0.5 degree C.
	Returns
	-------
	MLD : array_like
		  Mixed layer depth
	idx_mld : bool array
			  Boolean array in the shape of p with MLD index.
	Examples
	--------
	>>> import os
	>>> import gsw
	>>> import matplotlib.pyplot as plt
	>>> from oceans import mld
	>>> from gsw.utilities import Bunch
	>>> # Read data file with check value profiles
	>>> datadir = os.path.join(os.path.dirname(gsw.utilities._file_), 'data')
	>>> cv = Bunch(np.load(os.path.join(datadir, 'gsw_cv_v3_0.npz')))
	>>> SA, CT, p = (cv.SA_chck_cast[:, 0], cv.CT_chck_cast[:, 0],
	...              cv.p_chck_cast[:, 0])
	>>> fig, (ax0, ax1, ax2) = plt.subplots(nrows=1, ncols=3, sharey=True)
	>>> l0 = ax0.plot(CT, -p, 'b.-')
	>>> MDL, idx = mld(SA, CT, p, criterion='temperature')
	>>> l1 = ax0.plot(CT[idx], -p[idx], 'ro')
	>>> l2 = ax1.plot(CT, -p, 'b.-')
	>>> MDL, idx = mld(SA, CT, p, criterion='density')
	>>> l3 = ax1.plot(CT[idx], -p[idx], 'ro')
	>>> l4 = ax2.plot(CT, -p, 'b.-')
	>>> MDL, idx = mld(SA, CT, p, criterion='pdvar')
	>>> l5 = ax2.plot(CT[idx], -p[idx], 'ro')
	>>> _ = ax2.set_ylim(-500, 0)
	References
	----------
	.. [1] Monterey, G., and S. Levitus, 1997: Seasonal variability of mixed
	layer depth for the World Ocean. NOAA Atlas, NESDIS 14, 100 pp.
	Washington, D.C.
	"""
	#depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
	try:
		S.coord('depth')
		MLD_out = S.extract(iris.Constraint(depth = np.min(depth_cube.data)))
	except:
		MLD_out = S[:,0,:,:]
	MLD_out_data = MLD_out.data
	for i in range(np.shape(MLD_out)[0]):
		print'calculating mixed layer for year: ',i
        thetao_tmp = thetao[i]
        S_tmp = S[i]
        depth_cube.data = np.abs(depth_cube.data)
        depth_cube = depth_cube * (-1.0)
        p = gsw.p_from_z(depth_cube.data,latitude_deg.data) # dbar
        SA = S_tmp.data*1.004715
        CT = gsw.CT_from_pt(SA,thetao_tmp.data - 273.15)
        SA, CT, p = map(np.asanyarray, (SA, CT, p))
        SA, CT, p = np.broadcast_arrays(SA, CT, p)
        SA, CT, p = map(ma.masked_invalid, (SA, CT, p))
        p_min, idx = p.min(axis = 0), p.argmin(axis = 0)
        sigma = SA.copy()
        to_mask = np.where(sigma == S.data.fill_value)
        sigma = gsw.rho(SA, CT, p_min) - 1000.
        sigma[to_mask] = np.NAN
        sig_diff = sigma[0,:,:].copy()
        sig_diff += 0.125 # Levitus (1982) density criteria
        sig_diff = np.tile(sig_diff,[np.shape(sigma)[0],1,1])
        idx_mld = sigma.data <= sig_diff.data
        #NEED TO SORT THS PIT - COMPARE WWITH OTHER AND FIX!!!!!!!!!!
        MLD = ma.masked_all_like(S_tmp.data)
        MLD[idx_mld] = depth_cube.data[idx_mld] * -1
        MLD_out_data[i,:,:] = np.ma.max(MLD,axis=0)
	return MLD_out_data



ensembles = ['r1i1p1','r1i1p121']

print 'Calculating mixed layer depths'


for ensemble in ensembles:
    for model in models:
        print model
        #try:
        test = glob.glob(directory+model+'_my_mld_'+ensemble+'.nc')
        if np.size(test) == 0:
            try:
                print 'calculating MLD for '+model
                S = extract_years(iris.load_cube(directory+model+'*_so_past1000_'+ensemble+'_*Omon*.nc'))
                thetao = extract_years(iris.load_cube(directory+model+'*_thetao_past1000_'+ensemble+'_*Omon*.nc'))
                #S = S[0:2]
                #thetao = thetao[0:2]
                depth_cube = S[0].copy()
                try:
                    S.coord('depth')
                    depths = depth_cube.coord('depth').points
                except:
                    depths = depth_cube.coord('ocean sigma over z coordinate').points
                    #for memorys sake, do one year at a time..
                depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
                try:
                    S.coord('depth')
                    hm = S.extract(iris.Constraint(depth = np.min(depth_cube.data)))
                except:
                    hm = S[:,0,:,:]
                latitude_deg = depth_cube.copy()
                latitude_deg_data = hm.coord('latitude').points
                latitude_deg.data = np.swapaxes(np.tile(latitude_deg_data,[np.shape(S)[1],360,1]),1,2)
                hm.data = mld(S,thetao,depth_cube,latitude_deg)
                iris.fileformats.netcdf.save(hm,directory+model+'_my_mld_'+ensemble+'.nc')
            except:
            	print ensemble+' '+model+' failed'
        else:
            print 'MLD for '+model+' already exists'



model_data = {}
ensemble = 'r1i1p1'

##############################
#     constants              #
##############################

#number of seconds in a year
yearsec = 60.0 * 60.0 * 24.0 * 360.0
# eddy diffusivity - held constant, following Dong et al. [2009],
k = 500 #m2/s
#  characteristic density of the mixed layer (taken here to be 1027kg m3)
po = 1027.0
# density of sea ice, estimated to be 930 kg m-3
pi = 930.0
# salinity of sea ice, estimated here as 5
Si = 5
#note that upside delta (nabia) is the gradient and can be calculated with np.gradient() but this is calculates in all three dimentsin (time, x and y), so to get what you waht use:
#tmp = np.gradient(variable_of_interest.data)
#gradient_of_variable.data = tmp[1]+tmp[2]
# nabia squared is called the laplacian and can be calculated with scipy.ndimage.filters.laplace() I think


"""
##############################
#     salinity budget        #
##############################


print 'Calculating salinity budget'
save_file = open('/home/ph290/Documents/logs/palaeo_amo_amoc_paper_figures_log_III.txt','a')
time_now = str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)
save_file.write(time_now+': Available models: '+str(models)+'\n')
save_file.close()

#models = ['MRI-CGCM3', 'bcc-csm1-1', 'MPI-ESM-P', 'GISS-E2-R', 'CSIRO-Mk3L-1-2', 'FGOALS-gl', 'HadCM3', 'FGOALS-s2', 'MIROC-ESM', 'CCSM4']
#models = ['MRI-CGCM3', 'bcc-csm1-1', 'MPI-ESM-P', 'GISS-E2-R', 'CSIRO-Mk3L-1-2', 'HadCM3', 'MIROC-ESM', 'CCSM4']
#the FGOALS models lack the required seaice data...

for model in models:
    print model
    save_file = open('/home/ph290/Documents/logs/palaeo_amo_amoc_paper_figures_log_III.txt','a')
    time_now = str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)
    save_file.write(time_now+': Calculating salinity budget for '+str(model)+'\n')
    save_file.close()
    model_data[model] = {}
    ######################################
    # Sea ice concentration              #
    ######################################
    sic = extract_years(iris.load_cube(directory+model+'*_sic_past1000_r1i1p1_*.nc'))
    ######################################
    # hight of the mixed layer (mld)     #
    ######################################
    hm = extract_years(iris.load_cube(directory+model+'_my_mld_'+ensemble+'.nc'))
    ######################################
    # test to see if already calculated  #
    ######################################
    test1 = glob.glob(directory+model+'_mixed_layer_salinity_'+ensemble+'.nc')
    test2 = glob.glob(directory+model+'_EP_contribution_'+ensemble+'.nc')
    test3 = glob.glob(directory+model+'_P_contribution_'+ensemble+'.nc')
    test4 = glob.glob(directory+model+'_E_contribution_'+ensemble+'.nc')
    test5 = glob.glob(directory+model+'_P_ignoring_ice_contribution_'+ensemble+'.nc')
    test6 = glob.glob(directory+model+'_E_ignoring_ice_contribution_'+ensemble+'.nc')
    if ((np.size(test1) == 0) | (np.size(test2) == 0) | (np.size(test3) == 0) | (np.size(test4) == 0) | (np.size(test5) == 0) | (np.size(test6) == 0)):
        ######################################
        # P minus E                          #
        ######################################
        # try:
        #     # mass of water vapor evaporating from the ice-free portion of the ocean
        #     E_no_ice = iris.load_cube(directory+model+'*_evs_past1000_r1i1p1_*Omon*.nc')
        #     # mass of liquid water falling as liquid rain  into the ice-free portion of the ocean
        #     P_no_ice_rain = iris.load_cube(directory+model+'*_pr_past1000_r1i1p1_*Omon*.nc')
        #     # mass of solid water falling as liquid rain  into the ice-free portion of the ocean
        #     P_no_ice_snow = iris.load_cube(directory+model+'*_prsn_past1000_r1i1p1_*Omon*.nc')
        #     P_no_ice = P_no_ice_rain + P_no_ice_snow
        #     P_minus_E_no_ice = P_no_ice - E_no_ice
        #     P_minus_E_no_ice *= yearsec
        #     P = P_no_ice * yearsec
        #     E = E_no_ice * yearsec
        #     #convert into a flux per year
        # except:
        # mass of water vapor evaporating from all portion of the ocean
        #####
        #  NOTE, if fit not as good following this approacht than the above, it is because we are only arrounting for sea-ice impact on annual averaged basis - so rever to above, maybe exploring how well it works just in the models with the above. OR could try and work with monthly data...
        E = extract_years(iris.load_cube(directory+model+'*_evspsbl_past1000_r1i1p1_*.nc'))
        # mass of solid+liquid water falling into the whole of the ocean
        P = extract_years(iris.load_cube(directory+model+'*_pr_past1000_r1i1p1_*Amon*.nc'))
        #convert into a flux per year
        P_ignoring_ice = P.copy() * yearsec
        E_ignoring_ice = E.copy() * yearsec
        P = (P * (((sic*-1.0)+100.0)/100.0)) * yearsec
        E = (E * (((sic*-1.0)+100.0)/100.0)) * yearsec
        P_minus_E_no_ice = (P - E)
        evap_precip_flag = True
        ######################################
        # 3D salinity                        #
        ######################################
        S = extract_years(iris.load_cube(directory+model+'*_so_past1000_r1i1p1_*Omon*.nc'))
        S.coord(dimensions=1).rename('depth')
        S = S.extract(level_above_1000)
        #units = psu = g/kg = kg/1000kg. Convert it to kg m-3
        S /= 1.026 # Change this so it uses the actual grid point density
        ######################################
        # mixed layer salinity               #
        ######################################
        depth_cube = S[0].copy()
        depths = depth_cube.coord('depth').points
        depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
        thickness_cube = S[0].copy()
        thicknesses = depth_cube.coord('depth').bounds[:,1] - depth_cube.coord('depth').bounds[:,0]
        thickness_cube.data = np.ma.masked_array(np.swapaxes(np.tile(thicknesses,[360,180,1]),0,2))
        #  mask everywhere below the mixed layer and calculate the mean mixed layer salinity #
        s_mixed_layer = S.extract(iris.Constraint(depth = depths[0]))
        s_mixed_layer_data = s_mixed_layer.data.copy()
        for time_index,cube_tmp in enumerate(S.slices(['depth','latitude','longitude'])):
        	print time_index
        	thickness_cube2 = thickness_cube.copy()
        	thickness_cube2_data = thickness_cube2.data
        	tmp_data = cube_tmp.data
        	for depth in np.arange(np.size(depths)):
        		tmp_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],tmp_data[depth,:,:])
        		thickness_cube2_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],thickness_cube2_data[depth,:,:])
        	cube_tmp.data = tmp_data
        	cube_tmp *= thickness_cube
        	s_mixed_layer_data[time_index,:,:] = cube_tmp.collapsed(['depth'],iris.analysis.SUM).data
        	thickness_cube2.data = thickness_cube2_data
        	total_thickness_cube = thickness_cube2.collapsed(['depth'],iris.analysis.SUM).data
        	s_mixed_layer_data[time_index,:,:] /= total_thickness_cube.data
        #####################################################################
        # s_mixed_layer contains the average salinity of the mixed layer    #
        #####################################################################
        s_mixed_layer.data = s_mixed_layer_data
        ######################################
        ######################################
        ### Components of salinity change  ###
        ######################################
        ######################################
        #E-P driven salinity change (1st, kg freshwater water added per m-3 per year)
        sw_density = 1.026 # (but should really change to use local ML averaged density)
        EP_contribution = (P_minus_E_no_ice / hm)
        #Fraction reduction in salinity
        EP_contribution = EP_contribution / (1000.0 * sw_density)
        #PSU reduction in salinity associated with P-E
        EP_contribution = (EP_contribution * s_mixed_layer)
        P = (((P / hm) / (1000.0 * sw_density)) * s_mixed_layer) #note *-1 removed after pickle 6 written...
        E = (((E / hm) / (1000.0 * sw_density)) * s_mixed_layer)
        P_ignoring_ice = (((P_ignoring_ice / hm) / (1000.0 * sw_density)) * s_mixed_layer)
        E_ignoring_ice = (((E_ignoring_ice / hm) / (1000.0 * sw_density)) * s_mixed_layer)
        iris.fileformats.netcdf.save(s_mixed_layer,directory+model+'_mixed_layer_salinity_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(EP_contribution,directory+model+'_EP_contribution_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(P,directory+model+'_P_contribution_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(E,directory+model+'_E_contribution_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(P_ignoring_ice,directory+model+'_P_ignoring_ice_contribution_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(E_ignoring_ice,directory+model+'_E_ignoring_ice_contribution_'+ensemble+'.nc')
    model_data[model]['mixed_layer_salinity'] = extract_years(iris.load_cube(directory+model+'_mixed_layer_salinity_'+ensemble+'.nc'))
    #NOTE RUNS DSO FAR HAVE BEEN 1e3 too low - had an spurios divide by 1000. Now removed for any future anlaysusl/...
    model_data[model]['EP_contribution'] = extract_years(iris.load_cube(directory+model+'_EP_contribution_'+ensemble+'.nc'))
    model_data[model]['P'] = extract_years(iris.load_cube(directory+model+'_P_contribution_'+ensemble+'.nc'))
    model_data[model]['E'] = extract_years(iris.load_cube(directory+model+'_E_contribution_'+ensemble+'.nc'))
    model_data[model]['P_ignoring_ice'] = extract_years(iris.load_cube(directory+model+'_P_ignoring_ice_contribution_'+ensemble+'.nc'))
    model_data[model]['E_ignoring_ice'] = extract_years(iris.load_cube(directory+model+'_E_ignoring_ice_contribution_'+ensemble+'.nc'))
    model_data[model]['sic'] = sic
    model_data[model]['hm'] = hm



model_data2 = model_data
with open('/data/NAS-ph290/ph290/cmip5/pickles/salinity_budget_plot_7.pickle', 'w') as f:
    pickle.dump([models,model_data2], f)


#with open('/data/NAS-ph290/ph290/cmip5/pickles/salinity_budget_plot_2.pickle', 'w') as f:
#   pickle.dump([models,model_data], f)


# with open('/data/NAS-ph290/ph290/cmip5/pickles/salinity_budget_plot_7.pickle', 'r') as f:
#    models,model_data = pickle.load(f)

"""

######################################
#   denisty                          #
######################################

# cdo -P 6  merge -subc,273.15 HadCM3_thetao_past1000_r1i1p1_regridded_not_vertically_Omon.nc HadCM3_so_past1000_r1i1p1_regridded_not_vertically_Omon.nc had_ts.nc
# cdo -P 6 rhopot -adisit had_ts.nc had_rho.nc

temporary_file_space = '/data/NAS-geo01/ph290/tmp/'
ensemble = ensembles[0]

for model in models:
    test = glob.glob(directory+model+'_density_past1000_'+ensemble+'.nc')
    if np.size(test) == 0:
        subprocess.call(['cdo -P 6  merge -subc,273.15 '+directory+model+'_thetao_past1000_r1i1p1_*Omon*.nc '+directory+model+'_so_past1000_r1i1p1_*Omon*.nc '+temporary_file_space+'delete.nc'], shell=True)
        subprocess.call(['cdo -P 6 rhopot -adisit '+temporary_file_space+'delete.nc '+directory+model+'_density_past1000_'+ensemble+'.nc'], shell=True)
        subprocess.call(['rm '+temporary_file_space+'delete.nc'], shell=True)


model_data_density = {}

for model in models:
    print model
    save_file = open('/home/ph290/Documents/logs/palaeo_amo_amoc_paper_figures_log_III.txt','a')
    time_now = str(time.localtime().tm_hour)+':'+str(time.localtime().tm_min)
    save_file.write(time_now+': Calculating mld density for '+str(model)+'\n')
    save_file.close()
    model_data_density[model] = {}
    ######################################
    # test to see if already calculated  #
    ######################################
    test1 = glob.glob(directory+model+'_mixed_layer_temperature_'+ensemble+'.nc')
    test2 = glob.glob(directory+model+'_mixed_layer_salinity_'+ensemble+'.nc')
    test3 = glob.glob(directory+model+'_mixed_layer_density_'+ensemble+'.nc')
    if ((np.size(test1) == 0) | (np.size(test2) ==0) | (np.size(test3) == 0)):
        ######################################
        # depth of the mixed layer (mld)     #
        ######################################
        hm = extract_years(iris.load_cube(directory+model+'_my_mld_'+ensemble+'.nc'))
        ########################################
        ########################################
        ##    salinity                        ##
        ########################################
        ########################################
        var = extract_years(iris.load_cube(directory+model+'*_so_past1000_r1i1p1_*Omon*.nc'))
        var.coord(dimensions=1).rename('depth')
        var = var.extract(level_above_1000)
        #units = psu = g/kg = kg/1000kg. Convert it to kg m-3
        #????var /= 1.026 # Change this so it uses the actual grid point density
        ######################################
        # mixed layer salinity               #
        ######################################
        depth_cube = var[0].copy()
        depths = depth_cube.coord('depth').points
        depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
        thickness_cube = var[0].copy()
        thicknesses = depth_cube.coord('depth').bounds[:,1] - depth_cube.coord('depth').bounds[:,0]
        thickness_cube.data = np.ma.masked_array(np.swapaxes(np.tile(thicknesses,[360,180,1]),0,2))
        #  mask everywhere below the mixed layer and calculate the mean mixed layer salinity #
        salinity_mixed_layer = var.extract(iris.Constraint(depth = depths[0]))
        var_mixed_layer_data = salinity_mixed_layer.data.copy()
        for time_index,cube_tmp in enumerate(var.slices(['depth','latitude','longitude'])):
        	print time_index
        	thickness_cube2 = thickness_cube.copy()
        	thickness_cube2_data = thickness_cube2.data
        	tmp_data = cube_tmp.data
        	for depth in np.arange(np.size(depths)):
        		tmp_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],tmp_data[depth,:,:])
        		thickness_cube2_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],thickness_cube2_data[depth,:,:])
        	cube_tmp.data = tmp_data
        	cube_tmp *= thickness_cube
        	var_mixed_layer_data[time_index,:,:] = cube_tmp.collapsed(['depth'],iris.analysis.SUM).data
        	thickness_cube2.data = thickness_cube2_data
        	total_thickness_cube = thickness_cube2.collapsed(['depth'],iris.analysis.SUM).data
        	var_mixed_layer_data[time_index,:,:] /= total_thickness_cube.data
        #####################################################################
        # s_mixed_layer contains the average salinity of the mixed layer    #
        #####################################################################
        salinity_mixed_layer.data = var_mixed_layer_data
        ########################################
        ########################################
        ## temperature                        ##
        ########################################
        ########################################
        var = extract_years(iris.load_cube(directory+model+'*_thetao_past1000_r1i1p1_*Omon*.nc'))
        var.coord(dimensions=1).rename('depth')
        var = var.extract(level_above_1000)
        ######################################
        # mixed layer temperature               #
        ######################################
        depth_cube = var[0].copy()
        depths = depth_cube.coord('depth').points
        depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
        thickness_cube = var[0].copy()
        thicknesses = depth_cube.coord('depth').bounds[:,1] - depth_cube.coord('depth').bounds[:,0]
        thickness_cube.data = np.ma.masked_array(np.swapaxes(np.tile(thicknesses,[360,180,1]),0,2))
        #  mask everywhere below the mixed layer and calculate the mean mixed layer temperature #
        temperature_mixed_layer = var.extract(iris.Constraint(depth = depths[0]))
        var_mixed_layer_data = temperature_mixed_layer.data.copy()
        for time_index,cube_tmp in enumerate(var.slices(['depth','latitude','longitude'])):
        	print time_index
        	thickness_cube2 = thickness_cube.copy()
        	thickness_cube2_data = thickness_cube2.data
        	tmp_data = cube_tmp.data
        	for depth in np.arange(np.size(depths)):
        		tmp_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],tmp_data[depth,:,:])
        		thickness_cube2_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],thickness_cube2_data[depth,:,:])
        	cube_tmp.data = tmp_data
        	cube_tmp *= thickness_cube
        	var_mixed_layer_data[time_index,:,:] = cube_tmp.collapsed(['depth'],iris.analysis.SUM).data
        	thickness_cube2.data = thickness_cube2_data
        	total_thickness_cube = thickness_cube2.collapsed(['depth'],iris.analysis.SUM).data
        	var_mixed_layer_data[time_index,:,:] /= total_thickness_cube.data
        ###############################################################################
        # temperature_mixed_layer contains the average temperature of the mixed layer    #
        ###############################################################################
        temperature_mixed_layer.data = var_mixed_layer_data
        #############################
        # density              #
        #############################
        var = extract_years(iris.load_cube(directory+model+'*_density_past1000_r1i1p1.nc'))
        var.coord(dimensions=1).rename('depth')
        var = var.extract(level_above_1000)
        ######################################
        # mixed layer density               #
        ######################################
        depth_cube = var[0].copy()
        depths = depth_cube.coord('depth').points
        depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
        thickness_cube = var[0].copy()
        thicknesses = depth_cube.coord('depth').bounds[:,1] - depth_cube.coord('depth').bounds[:,0]
        thickness_cube.data = np.ma.masked_array(np.swapaxes(np.tile(thicknesses,[360,180,1]),0,2))
        #  mask everywhere below the mixed layer and calculate the mean mixed layer density #
        density_mixed_layer = var.extract(iris.Constraint(depth = depths[0]))
        var_mixed_layer_data = density_mixed_layer.data.copy()
        for time_index,cube_tmp in enumerate(var.slices(['depth','latitude','longitude'])):
        	print time_index
        	thickness_cube2 = thickness_cube.copy()
        	thickness_cube2_data = thickness_cube2.data
        	tmp_data = cube_tmp.data
        	for depth in np.arange(np.size(depths)):
        		tmp_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],tmp_data[depth,:,:])
        		thickness_cube2_data[depth,:,:] = np.ma.masked_where(hm[time_index,:,:].data < depths[depth],thickness_cube2_data[depth,:,:])
        	cube_tmp.data = tmp_data
        	cube_tmp *= thickness_cube
        	var_mixed_layer_data[time_index,:,:] = cube_tmp.collapsed(['depth'],iris.analysis.SUM).data
        	thickness_cube2.data = thickness_cube2_data
        	total_thickness_cube = thickness_cube2.collapsed(['depth'],iris.analysis.SUM).data
        	var_mixed_layer_data[time_index,:,:] /= total_thickness_cube.data
        ###############################################################################
        # density_mixed_layer contains the average density of the mixed layer    #
        ###############################################################################
        density_mixed_layer.data = var_mixed_layer_data
        #############################
        # save results              #
        #############################
        iris.fileformats.netcdf.save(temperature_mixed_layer,directory+model+'_mixed_layer_temperature_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(salinity_mixed_layer,directory+model+'_mixed_layer_salinity_'+ensemble+'.nc')
        iris.fileformats.netcdf.save(density_mixed_layer,directory+model+'_mixed_layer_density_'+ensemble+'.nc')
    ##############################
    # save results to dictoinary #
    ##############################
    model_data_density[model]['mixed_layer_temperature'] = extract_years(iris.load_cube(directory+model+'_mixed_layer_temperature_'+ensemble+'.nc'))
    model_data_density[model]['mixed_layer_salinity'] = extract_years(iris.load_cube(directory+model+'_mixed_layer_salinity_'+ensemble+'.nc'))
    model_data_density[model]['mixed_layer_density'] = extract_years(iris.load_cube(directory+model+'_mixed_layer_density_'+ensemble+'.nc'))
    with open('/data/NAS-ph290/ph290/cmip5/pickles/mld_density.pickle', 'w') as f:
        pickle.dump([models,model_data_density], f)


model_data_atlantic = {}

for model in models:
    print model
    model_data_atlantic[model] = {}
    model_data_atlantic[model]['tas'] = extract_years(iris.load_cube(directory+model+'*_tas_past1000_r1i1p1_*.nc'))




###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}

giss_test = 0

for i,model in enumerate(models):
	if model == 'GISS-E2-R':
		if giss_test == 0:
			pmip3_str[model] = max_strm_fun_26[i]
			pmip3_year_str[model] = model_years[i]
			giss_test += 1
	if model <> 'GISS-E2-R':
		pmip3_str[model] = max_strm_fun_26[i]
		pmip3_year_str[model] = model_years[i]





print 'have you finished processing the input files for the two following models. Currently downloading extra data'

#hfjohdjklasdjkl
modles = list(models)
#models.remove('FGOALS-gl')
#REMOVE FGOALS-gl BECAUSE STARTS IN yr 1000 not 850
#try:
#    models.remove('FGOALS-s2') #Removed do we don;t have two FGOALS models
#except:
#    print 'FGOALS-s2 already not in list'
#FGOALS-s2 removed because there is a problem with one of teh humidity files
#models.remove('GISS-E2-R')
#NOTE! FGOALS MODEL LEVELS ARE UPSIDE DOWN - DOES THIS MATTER?, check mask figures to explain'
#models.remove('bcc-csm1-1')
#NOTE JUST FIXING SALINITY IN THIS MODEL - REQUIRED AN EXTRA FILE WHICH WAS MISSED OFF CMIP5 ARCHIVE
models = np.array(models)

all_models = models
#added = does this cause problems?

#with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_II.pickle', 'w') as f:
#    pickle.dump([all_models,amo_yr,amo_data,pmip3_str,pmip3_year_str,pmip3_tas,pmip3_year_tas,all_models], f)

#with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_II.pickle') as f:
#ll_models_tas_sos_pr    all_models,amo_yr,amo_data,pmip3_str,pmip3_year_str,pmip3_tas,pmip3_year_tas,all_models = pickle.load(f)


start_year  = 1000
#note that it is 1000 because FGOALS-gl starts in the year 1000
end_year = 1850





##############################################
#            figure                         #
##############################################

plt.rc('legend',**{'fontsize':10})


#specify no. years to low-pass filter at to smooth data
low_pass = 5
#construct filters
b1, a1 = butter_lowpass(1.0/100.0, 1.0,2)
b2, a2 = butter_highpass(1.0/low_pass, 1.0,2)

#read in, linearly detrend and normalise Reynolds d18O data
r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_date))
tmp = tmp[loc]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=signal.detrend(bivalve_data_initial)

#read in, linearly detrend and normalise Mann data
amo_file = '/home/ph290/data0/misc_data/iamo_mann.dat'
#amo_file = '/home/ph290/data0/misc_data/mann_1999_nhem-recon.txt'
amo = np.genfromtxt(amo_file, skip_header = 4)
amo_yr = amo[:,0]
amo_data = amo[:,1]
loc = np.where((np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_date))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data -= np.min(amo_data)
amo_data /= np.max(amo_data)
amo_data=signal.detrend(amo_data)

#read in, linearly detrend and normalise Briffa N. Hem data
nh_briff_file = '/home/ph290/data0/misc_data/briffa_nhemtemp_data.txt'
nh_briff = np.genfromtxt(nh_briff_file, skip_header = 58)
nh_briff_yr = nh_briff[:,0]
nh_briff_data = nh_briff[:,1]
loc = np.where((np.logical_not(np.isnan(nh_briff_data))) & (nh_briff_yr >= start_year) & (nh_briff_yr <= end_date))
nh_briff_yr = nh_briff_yr[loc]
nh_briff_data = nh_briff_data[loc]
nh_briff_data -= np.min(nh_briff_data)
nh_briff_data /= np.max(nh_briff_data)
nh_briff_data=signal.detrend(nh_briff_data)

no_smoothing_steps = 20
coeff_det = np.zeros([np.size(models),no_smoothing_steps])
coeff_det[:] = np.NAN

#f, axarr = plt.subplots(4, 1)
f, axarr = plt.subplots(2, 1)
for plot_no,remove_model in enumerate(models):
    all_models = models.copy()
    all_models= list(all_models)
    all_models.remove(remove_model)
    #make variable to hold multi model mean stream function data
    pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
    pmip3_model_streamfunction[:] = np.NAN
    # and mixed layer density
    pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
    pmip3_atl_tas = pmip3_model_streamfunction.copy()

    #variable holding analysis years
    expected_years = start_year+np.arange((end_year-start_year)+1)

    #Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
    for i,model in enumerate(all_models):
            print model
            tmp = pmip3_str[model]
            loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
            tmp = tmp[loc]
            yrs = pmip3_year_str[model][loc]
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_model_streamfunction[index,i] = data3[loc2]


    pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)

    west = -23
    east = -13
    south = 64
    north = 69

    #as above but for density:
    for i,model in enumerate(all_models):
            print model
            cube = model_data_density[model]['mixed_layer_density']
            cube = cube.intersection(longitude=(west, east))
            cube = cube.intersection(latitude=(south, north))
            try:
                cube.coord('latitude').guess_bounds()
            except:
                print 'cube already has latitude bounds'
            try:
                cube.coord('longitude').guess_bounds()
            except:
                print 'cube already has longitude bounds'
            grid_areas = iris.analysis.cartography.area_weights(cube)
            tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            yrs = np.array([coord.units.num2date(value).year for value in coord.points])
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_mixed_layer_density[index,i] = data3[loc2]


    pmip3_multimodel_mixed_layer_density = np.mean(pmip3_mixed_layer_density, axis = 1)


    west = -75
    east = -7.5
    south = 0
    north = 65
    #west = 0
    #east = 360
    #south = 0
    #north = 90

    #as above but for density:
    for i,model in enumerate(all_models):
            print model
            cube = model_data_atlantic[model]['tas']
            cube = cube.intersection(longitude=(west, east))
            cube = cube.intersection(latitude=(south, north))
            try:
                cube.coord('latitude').guess_bounds()
            except:
                print 'cube already has latitude bounds'
            try:
                cube.coord('longitude').guess_bounds()
            except:
                print 'cube already has longitude bounds'
            grid_areas = iris.analysis.cartography.area_weights(cube)
            tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            yrs = np.array([coord.units.num2date(value).year for value in coord.points])
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_atl_tas[index,i] = data3[loc2]


    pmip3_multimodel_atl_tas = np.mean(pmip3_atl_tas, axis = 1)



    """
    plt.close('all')
    plt.figure(figsize=(10,18))
    plt.subplot(4,1,1)

    '''
    #linearly detrend, normalise and plot all of the individual models
    for i,model in enumerate(models):
        print model
        tmp = pmip3_str[model]
        loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
        y = tmp[loc]
        x = pmip3_year_str[model][loc]
        #y = scipy.signal.filtfilt(b1, a1, y)
        y=signal.detrend(y)
        #y = scipy.signal.filtfilt(b2, a2, y)
        y -= np.min(y)
        y /= np.max(y)
        plt.plot(x,y-np.nanmean(y),'k',alpha = 0.15)
    '''


    #plot multi-model mean detrended mixed layer density data
    y = pmip3_multimodel_mixed_layer_density
    x = start_year+np.arange((end_year-start_year)+1)
    #normalise the final dataset
    y -= np.nanmin(y)
    y /= np.nanmax(y)
    #remove any NaNs
    loc = np.where(np.logical_not(np.isnan(y)))
    #low-pass filter
    y_filtered = scipy.signal.filtfilt(b2, a2, y[loc])
    #plot unfiltered data
    plt.plot(x[loc],y[loc]-np.nanmean(y[loc]),'g',alpha = 0.4,linewidth = 2,label = 'PMIP3 multimodel mean seawater density')
    #plot filtered data
    plt.plot(x[loc],y_filtered-np.nanmean(y_filtered),'g',alpha = 0.75,linewidth = 2,label = 'Low-pass filtered PMIP3 multimodel mean seawater density')

    #low-pass filter bivalve data
    bivalve_data = bivalve_data_initial.copy()
    bivalve_data_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data)
    #plot unfiltered data
    plt.plot(bivalve_year,bivalve_data-np.nanmean(bivalve_data),'r',linewidth = 2,alpha = 0.4,label = 'Bivalve $\delta^{18}$O')
    #plot filtered data
    plt.plot(bivalve_year,bivalve_data_filtered-np.nanmean(bivalve_data_filtered),'r',linewidth = 2,alpha = 0.75,label = 'Low-pass filtered bivalve $\delta^{18}$O')

    plt.xlim([950,1850])
    plt.xlabel('Calendar Year')
    plt.ylabel('Normalised anomaly')
    leg = plt.legend(fancybox=True)
    leg.get_frame().set_alpha(0.5)

    plt.subplot(4,1,2)

    #plot multi-model mean detrended mixed layer density data
    y = pmip3_multimodel_mixed_layer_density
    x = start_year+np.arange((end_year-start_year)+1)
    #normalise the final dataset
    y -= np.nanmin(y)
    y /= np.nanmax(y)
    #remove any NaNs
    loc = np.where(np.logical_not(np.isnan(y)))
    #low-pass filter
    y_filtered = scipy.signal.filtfilt(b2, a2, y[loc])
    y_filtered = scipy.signal.filtfilt(b1, a1, y_filtered)
    #plot un low-pass filtered data
    y = scipy.signal.filtfilt(b1, a1, y[loc])
    plt.plot(x[loc],y-np.nanmean(y),'g',alpha = 0.4,linewidth = 2,label = 'PMIP3 multimodel mean seawater density')
    #plot filtered data
    plt.plot(x[loc],y_filtered-np.nanmean(y_filtered),'g',alpha = 0.75,linewidth = 2,label = 'High and Low-pass filtered PMIP3 multimodel mean seawater density')

    #low-pass filter bivalve data
    bivalve_data = bivalve_data_initial.copy()
    bivalve_data_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data)
    bivalve_data_filtered = scipy.signal.filtfilt(b1, a1, bivalve_data_filtered)
    #plot un low-pass filtered data
    bivalve_data = scipy.signal.filtfilt(b1, a1, bivalve_data)
    plt.plot(bivalve_year,bivalve_data-np.nanmean(bivalve_data),'r',linewidth = 2,alpha = 0.4,label = 'Bivalve $\delta^{18}$O')
    #plot filtered data
    plt.plot(bivalve_year,bivalve_data_filtered-np.nanmean(bivalve_data_filtered),'r',linewidth = 2,alpha = 0.75,label = 'High and Low-pass filtered bivalve $\delta^{18}$O')

    plt.xlim([950,1850])
    plt.xlabel('Calendar Year')
    plt.ylabel('Normalised anomaly')
    leg = plt.legend(fancybox=True)
    leg.get_frame().set_alpha(0.5)

    plt.subplot(4,1,3)

    #plot multi-model mean detrended stream function data
    y = pmip3_multimodel_mean_streamfunction
    x = start_year+np.arange((end_year-start_year)+1)
    #normalise the final dataset
    y -= np.nanmin(y)
    y /= np.nanmax(y)
    #remove any NaNs
    loc = np.where(np.logical_not(np.isnan(y)))
    #low-pass filter
    y_filtered = scipy.signal.filtfilt(b2, a2, y[loc])
    #plot unfiltered data
    plt.plot(x[loc],y[loc]-np.nanmean(y[loc]),'b',alpha = 0.4,linewidth = 2,label = 'PMIP3 multimodel mean AMOC')
    #plot filtered data
    plt.plot(x[loc],y_filtered-np.nanmean(y_filtered),'b',alpha = 0.75,linewidth = 2,label = 'Low-pass filtered PMIP3 multimodel mean AMOC')

    #low-pass filter bivalve data
    bivalve_data = bivalve_data_initial.copy()
    bivalve_data_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data)
    #plot unfiltered data
    plt.plot(bivalve_year,bivalve_data-np.nanmean(bivalve_data),'r',linewidth = 2,alpha = 0.4,label = 'Bivalve $\delta^{18}$O')
    #plot filtered data
    plt.plot(bivalve_year,bivalve_data_filtered-np.nanmean(bivalve_data_filtered),'r',linewidth = 2,alpha = 0.75,label = 'Low-pass filtered bivalve $\delta^{18}$O')


    plt.xlim([950,1850])
    plt.xlabel('Calendar Year')
    plt.ylabel('Normalised anomaly')
    leg = plt.legend(fancybox=True)
    leg.get_frame().set_alpha(0.5)


    plt.subplot(4,1,4)

    #plot multi-model mean detrended stream function data
    y = pmip3_multimodel_mean_streamfunction
    x = start_year+np.arange((end_year-start_year)+1)
    #normalise the final dataset
    y -= np.nanmin(y)
    y /= np.nanmax(y)
    #remove any NaNs
    loc = np.where(np.logical_not(np.isnan(y)))
    #low-pass filter
    y_filtered = scipy.signal.filtfilt(b2, a2, y[loc])
    y_filtered = scipy.signal.filtfilt(b1, a1, y_filtered)
    y[loc] = scipy.signal.filtfilt(b1, a1, y[loc])
    #plot unfiltered data
    plt.plot(x[loc],y[loc]-np.nanmean(y[loc]),'b',alpha = 0.4,linewidth = 2,label = 'PMIP3 multimodel mean AMOC')
    #plot filtered data
    plt.plot(x[loc],y_filtered-np.nanmean(y_filtered),'b',alpha = 0.75,linewidth = 2,label = 'High and Low-pass filtered PMIP3 multimodel mean AMOC')

    #low-pass filter bivalve data
    bivalve_data = bivalve_data_initial.copy()
    bivalve_data_filtered = scipy.signal.filtfilt(b2, a2, bivalve_data)
    bivalve_data_filtered = scipy.signal.filtfilt(b1, a1, bivalve_data_filtered)
    bivalve_data = scipy.signal.filtfilt(b1, a1, bivalve_data)
    #plot unfiltered data
    plt.plot(bivalve_year,bivalve_data-np.nanmean(bivalve_data),'r',linewidth = 2,alpha = 0.4,label = 'Bivalve $\delta^{18}$O')
    #plot filtered data
    plt.plot(bivalve_year,bivalve_data_filtered-np.nanmean(bivalve_data_filtered),'r',linewidth = 2,alpha = 0.75,label = 'High and Low-pass filtered bivalve $\delta^{18}$O')


    plt.xlim([950,1850])
    plt.xlabel('Calendar Year')
    plt.ylabel('Normalised anomaly')
    leg = plt.legend(fancybox=True)
    leg.get_frame().set_alpha(0.5)


    plt.show(block = False)
    #plt.savefig('/home/ph290/Documents/figures/unfiltered_amoc.png')
    """

    '''



    pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
    pmip3_model_streamfunction[:] = np.NAN
    expected_years = start_year+np.arange((end_year-start_year)+1)


    for i,model in enumerate(models):
            print model
            tmp = pmip3_str[model]
    #       tmp = rm.running_mean(tmp,smoothing_val)
            loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
            tmp = tmp[loc]
            yrs = pmip3_year_str[model][loc]
            data2 = tmp
    #       data2 = scipy.signal.filtfilt(b, a, tmp)
            data2 = scipy.signal.filtfilt(b1, a1, tmp)
            #data2 = scipy.signal.filtfilt(b2, a2, data2)
            #data2 = tmp
            x = data2
            data3 = (x-np.min(x))/(np.max(x))
    #         data3 = rm.running_mean(data3,smoothing_val)
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_model_streamfunction[index,i] = data3[loc2]


    plt.xlim([950,1850])


    pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)
    plt.subplot(2, 1, 2)

    b1, a1 = butter_lowpass(1.0/100.0, 1.0,2)

    for i,model in enumerate(models):
            print model
            tmp = pmip3_str[model]
    #       tmp = rm.running_mean(tmp,smoothing_val)
            loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
            tmp = tmp[loc]
            yrs = pmip3_year_str[model][loc]
            data2 = tmp
    #       data2 = scipy.signal.filtfilt(b, a, tmp)
            tmp=signal.detrend(tmp)
            data2 = scipy.signal.filtfilt(b1, a1, tmp)
            #data2 = scipy.signal.filtfilt(b2, a2, data2)
            #data2 = tmp
            x = data2
            data3 = (x-np.min(x))/(np.max(x))
    #         data3 = rm.running_mean(data3,smoothing_val)
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_model_streamfunction[index,i] = data3[loc2]


    pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)



    for i,model in enumerate(models):
        print model
        tmp = pmip3_str[model]
        loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
        y = tmp[loc]
        x = pmip3_year_str[model][loc]
        y=signal.detrend(y)
        y = scipy.signal.filtfilt(b1, a1, y)
        #y = scipy.signal.filtfilt(b2, a2, y)
        y -= np.min(y)
        y /= np.max(y)
        plt.plot(x,y-np.nanmean(y),'k',alpha = 0.15)


    r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv'
    r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
    tmp = r_data[:,1]
    tmp=signal.detrend(tmp)
    tmp = scipy.signal.filtfilt(b1, a1, tmp)
    #tmp = scipy.signal.filtfilt(b2, a2, tmp)
    loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_date))
    tmp = tmp[loc]
    tmp_yr = r_data[loc[0],0]
    tmp -= np.min(tmp)
    tmp /= np.max(tmp)

    y = pmip3_multimodel_mean_streamfunction
    x = start_year+np.arange((end_year-start_year)+1)
    y -= np.nanmin(y)
    y /= np.nanmax(y)
    #plt.plot(x,y,'b',alpha = 0.5,linewidth = 2)
    b2, a2 = butter_highpass(1.0/low_pass, 1.0,2)
    b2b, a2b = butter_highpass(1.0/low_pass2, 1.0,2)
    #b1, a1 = butter_lowpass(1.0/100.0, 1.0,2)
    loc = np.where(np.logical_not(np.isnan(y)))
    #plt.plot(x[loc],y[loc],'b',alpha = 0.5,linewidth = 2)
    y2 = scipy.signal.filtfilt(b2, a2, y[loc])
    #y2 = scipy.signal.filtfilt(b1, a1, y2)
    plt.plot(x[loc],y[loc]-np.nanmean(y[loc]),'b',alpha = 0.4,linewidth = 2)
    plt.plot(x[loc],y2-np.nanmean(y2),'b',alpha = 0.75,linewidth = 2)

    #plt.subplot(2, 1, 2)
    #plt.plot(tmp_yr,tmp+0.1,'r',linewidth = 2,alpha = 0.5,label = 'Reynolds et al. (2014) $\delta^{18}$O')
    y3 = scipy.signal.filtfilt(b2, a2, tmp)
    plt.plot(tmp_yr,tmp-np.nanmean(tmp),'r',linewidth = 2,alpha = 0.4,label = 'Reynolds et al. (2014) $\delta^{18}$O')
    plt.plot(tmp_yr,y3-np.nanmean(y3),'r',linewidth = 2,alpha = 0.75,label = 'Reynolds et al. (2014) $\delta^{18}$O')

    plt.xlim([950,1850])

    plt.show(block = False)

    '''
    #
    # year = []
    # data = []
    #
    # from scipy.stats import gaussian_kde
    # from matplotlib.colors import LinearSegmentedColormap
    #
    # for i,dummy in enumerate(models):
    #     data.append(pmip3_model_streamfunction[:,i])
    #     year.append(expected_years)
    #
    # y = np.array(data)
    # x = np.array(year)
    # loc = np.where(np.logical_not(np.isnan(x)) & np.logical_not(np.isnan(y)))
    # x = x[loc]
    # y = y[loc]
    # xy = np.vstack([x,y])
    # z = gaussian_kde(xy)(xy)
    # ln_tmp = plt.scatter(x, y, c=z, s=10, edgecolor='')
    # plt.show()



    mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
    stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
    #note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
    mean_strm -= np.nanmean(mean_strm)

    mean = np.nanmean(pmip3_mixed_layer_density, axis = 1)
    stdev = np.nanstd(pmip3_mixed_layer_density, axis = 1)

    mean -= np.nanmean(mean)
    #mean *= 3.0
    X = expected_years

    #plt.close('all')
    # fig, ax =plt.subplots(1)
    steps = 50.0
    # for i in np.arange(steps):
    #     ax.fill_between(X, rm.running_mean(mean,20)+rm.running_mean(stdev,20)*(i/steps),rm.running_mean(mean,20)-rm.running_mean(stdev,20)*(i/steps), facecolor='k',edgecolor='', alpha=1.0/(i+1.0))
    smoothing_per = 5
    bivalve_data = bivalve_data_initial.copy()/3.0
    #plot unfiltered data
    y2 = bivalve_data-np.nanmean(bivalve_data)
    axarr[1].plot(expected_years,rm.running_mean(mean,smoothing_per),'k',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 N. Iceland seawater density')
    # axarr[1].plot(expected_years,rm.running_mean(mean,5),'k',linewidth = 2.0,alpha = 0.5)
    axarr[0].plot(expected_years,rm.running_mean(mean_strm,smoothing_per),'g',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 26N AMOC strength')

    # #for smooth_val2 in np.arange(no_smoothing_steps):
    # tmp = y2.copy()
    # #smooth_val2 += 1
    # y5_a = tmp[::-1]
    # smooth_val2 = 10
    # y5_b = rm.running_mean(mean,smooth_val2)
    # loc = np.logical_not(np.isnan(y5_a) | np.isnan(y5_b))
    # y5_a = y5_a[loc]
    # y5_b = y5_b[loc]
    # r = scipy.stats.linregress(y5_a,y5_b)[2]
    # r2 = r*r
    # coeff_det[plot_no,smooth_val2-1] = r2
    # plt.plot(y5_a)
    # plt.plot(y5_b)
    # plt.show()

    # mean_tas = np.nanmean(pmip3_atl_tas, axis = 1)
    # mean_tas -= np.nanmean(mean_tas)
    #axarr[2].plot(expected_years,rm.running_mean(mean_tas,smoothing_per),'y',linewidth = 2.0,alpha = 0.5)
    #axarr[3].plot(expected_years,rm.running_mean(((mean_strm*2.0)+mean_tas)*0.5,smoothing_per),'y',linewidth = 2.0,alpha = 0.5)



bivalve_data = bivalve_data_initial.copy()/3.0
#plot unfiltered data
y2 = bivalve_data-np.nanmean(bivalve_data)
y2 = rm.running_mean(y2,smoothing_per)
axarr[1].plot(bivalve_year,y2,'r',linewidth = 2,alpha = 0.75,label = 'N. Iceland bivalve $\delta^{18}$O')
axarr[0].plot(bivalve_year,y2,'r',linewidth = 2,alpha = 0.75,label = 'N. Iceland bivalve $\delta^{18}$O')
# plt.title('Single models sequentially removed')


red = mpatches.Patch(color='red', label='N. Iceland bivalve $\delta^{18}$O')
green = mpatches.Patch(color='green', label='PMIP3 AMOC strength')
black = mpatches.Patch(color='black', label='PMIP3 N. Iceland density')
axarr[0].legend(handles=[red,green,black])

y3 = amo_data/3.0
y3 = y3-np.nanmean(y3)
# y3 = rm.running_mean(y3,smoothing_per)
#axarr[1].plot(amo_yr,y3,'b',linewidth = 2,alpha = 0.75,label = 'Mann')
#axarr[2].plot(amo_yr,y3,'b',linewidth = 2,alpha = 0.75,label = 'Mann')
#axarr[3].plot(amo_yr,y3,'b',linewidth = 2,alpha = 0.75,label = 'Mann')

'''
y4 = nh_briff_data/3.0
y4 = y4-np.nanmean(y4)
# y3 = rm.running_mean(y3,smoothing_per)
axarr[1].plot(nh_briff_yr,y4,'b',linewidth = 2,alpha = 0.75,label = 'Mann')
axarr[2].plot(nh_briff_yr,y4,'b',linewidth = 2,alpha = 0.75,label = 'Mann')
axarr[3].plot(nh_briff_yr,y4,'b',linewidth = 2,alpha = 0.75,label = 'Mann')
'''
axarr[1].set_xlabel('Calendar Year')
axarr[0].set_ylabel('Variable anomaly')
axarr[1].set_ylabel('Variable anomaly')
axarr[0].set_xlim([1000,1850])
axarr[1].set_xlim([1000,1850])
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/amoc_density.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_density.png')


r2s = np.max(coeff_det,axis = 0)
plt.plot(r2s)
plt.show()

'''
density - d18O
'''


#f, axarr = plt.subplots(4, 1)
for plot_no,remove_model in enumerate(models):
    all_models = models.copy()
    all_models= list(all_models)
    all_models.remove(remove_model)
    #make variable to hold multi model mean stream function data
    pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
    pmip3_model_streamfunction[:] = np.NAN
    # and mixed layer density
    pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
    pmip3_atl_tas = pmip3_model_streamfunction.copy()

    #variable holding analysis years
    expected_years = start_year+np.arange((end_year-start_year)+1)

    #Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
    for i,model in enumerate(all_models):
            print model
            tmp = pmip3_str[model]
            loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
            tmp = tmp[loc]
            yrs = pmip3_year_str[model][loc]
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_model_streamfunction[index,i] = data3[loc2]


    pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)

    west = -23
    east = -13
    south = 64
    north = 69

    #as above but for density:
    for i,model in enumerate(all_models):
            print model
            cube = model_data_density[model]['mixed_layer_density']
            cube = cube.intersection(longitude=(west, east))
            cube = cube.intersection(latitude=(south, north))
            try:
                cube.coord('latitude').guess_bounds()
            except:
                print 'cube already has latitude bounds'
            try:
                cube.coord('longitude').guess_bounds()
            except:
                print 'cube already has longitude bounds'
            grid_areas = iris.analysis.cartography.area_weights(cube)
            tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            yrs = np.array([coord.units.num2date(value).year for value in coord.points])
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_mixed_layer_density[index,i] = data3[loc2]


    pmip3_multimodel_mixed_layer_density = np.mean(pmip3_mixed_layer_density, axis = 1)


    west = -75
    east = -7.5
    south = 0
    north = 65
    #west = 0
    #east = 360
    #south = 0
    #north = 90

    #as above but for density:
    for i,model in enumerate(all_models):
            print model
            cube = model_data_atlantic[model]['tas']
            cube = cube.intersection(longitude=(west, east))
            cube = cube.intersection(latitude=(south, north))
            try:
                cube.coord('latitude').guess_bounds()
            except:
                print 'cube already has latitude bounds'
            try:
                cube.coord('longitude').guess_bounds()
            except:
                print 'cube already has longitude bounds'
            grid_areas = iris.analysis.cartography.area_weights(cube)
            tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            yrs = np.array([coord.units.num2date(value).year for value in coord.points])
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_atl_tas[index,i] = data3[loc2]

    pmip3_multimodel_atl_tas = np.mean(pmip3_atl_tas, axis = 1)

    mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
    stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
    #note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
    mean_strm -= np.nanmean(mean_strm)

    mean = np.nanmean(pmip3_mixed_layer_density, axis = 1)
    stdev = np.nanstd(pmip3_mixed_layer_density, axis = 1)

    mean -= np.nanmean(mean)
    #mean *= 3.0
    X = expected_years

    #plt.close('all')
    # fig, ax =plt.subplots(1)
    steps = 50.0
    # for i in np.arange(steps):
    #     ax.fill_between(X, rm.running_mean(mean,20)+rm.running_mean(stdev,20)*(i/steps),rm.running_mean(mean,20)-rm.running_mean(stdev,20)*(i/steps), facecolor='k',edgecolor='', alpha=1.0/(i+1.0))
    smoothing_per = 5
    plt.plot(expected_years,rm.running_mean(mean,smoothing_per),'k',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 N. Iceland seawater density')


bivalve_data = bivalve_data_initial.copy()/3.0
#plot unfiltered data
y2 = bivalve_data-np.nanmean(bivalve_data)
y2 = rm.running_mean(y2,smoothing_per)
plt.plot(bivalve_year,y2,'r',linewidth = 2,alpha = 0.75,label = 'N. Iceland bivalve $\delta^{18}$O')
# plt.title('Single models sequentially removed')


red = mpatches.Patch(color='red', label='N. Iceland bivalve $\delta^{18}$O')
black = mpatches.Patch(color='black', label='PMIP3 N. Iceland density')
plt.legend(handles=[red,black])


plt.xlabel('Calendar Year')
plt.ylabel('anomaly')
plt.xlim([1000,1850])
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/amoc_density2.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_density2.png')


'''
strm_fun_mann_amo
'''


f, axarr = plt.subplots(2, 1)
for plot_no,remove_model in enumerate(models):
    all_models = models.copy()
    all_models= list(all_models)
    all_models.remove(remove_model)
    #make variable to hold multi model mean stream function data
    pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
    pmip3_model_streamfunction[:] = np.NAN
    # and mixed layer density
    pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
    pmip3_atl_tas = pmip3_model_streamfunction.copy()
    #variable holding analysis years
    expected_years = start_year+np.arange((end_year-start_year)+1)
    #Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
    for i,model in enumerate(all_models):
            print model
            tmp = pmip3_str[model]
            loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
            tmp = tmp[loc]
            yrs = pmip3_year_str[model][loc]
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_model_streamfunction[index,i] = data3[loc2]
    pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)
    west = -75
    east = -7.5
    south = 0
    north = 65
    #west = 0
    #east = 360
    #south = 0
    #north = 90
    #as above but for density:
    for i,model in enumerate(all_models):
            print model
            cube = model_data_atlantic[model]['tas']
            cube = cube.intersection(longitude=(west, east))
            cube = cube.intersection(latitude=(south, north))
            try:
                cube.coord('latitude').guess_bounds()
            except:
                print 'cube already has latitude bounds'
            try:
                cube.coord('longitude').guess_bounds()
            except:
                print 'cube already has longitude bounds'
            grid_areas = iris.analysis.cartography.area_weights(cube)
            tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            yrs = np.array([coord.units.num2date(value).year for value in coord.points])
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_atl_tas[index,i] = data3[loc2]
    mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
    stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
    #note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
    mean_strm -= np.nanmean(mean_strm)
    mean_tas = np.nanmean(pmip3_atl_tas, axis = 1)
    stdev_tas = np.nanstd(pmip3_atl_tas, axis = 1)
    mean_tas -= np.nanmean(mean_tas)
    #mean *= 3.0
    X = expected_years
    #plt.close('all')
    # fig, ax =plt.subplots(1)
    steps = 50.0
    # for i in np.arange(steps):
    #     ax.fill_between(X, rm.running_mean(mean,20)+rm.running_mean(stdev,20)*(i/steps),rm.running_mean(mean,20)-rm.running_mean(stdev,20)*(i/steps), facecolor='k',edgecolor='', alpha=1.0/(i+1.0))
    smoothing_per = 5
    axarr[0].plot(expected_years,rm.running_mean(mean_strm,smoothing_per),'k',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 N. Iceland seawater density')
    axarr[1].plot(expected_years,rm.running_mean(mean_tas,smoothing_per),'y',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 N. Iceland seawater density')

axarr[1].plot(amo_yr,amo_data/3.0,'b',linewidth = 2.0,alpha = 0.8,label = 'Mann AMO index')
axarr[0].plot(amo_yr,amo_data/3.0,'b',linewidth = 2.0,alpha = 0.8,label = 'Mann AMO index')

blue = mpatches.Patch(color='blue', label='Mann AMO index')
black = mpatches.Patch(color='black', label='PMIP3 N. Iceland density')
yellow = mpatches.Patch(color='yellow', label='PMIP3 AMO box surface air temperature')
plt.legend(handles=[blue,black,yellow],loc = 4)


axarr[0].set_xlabel('Calendar Year')
axarr[1].set_ylabel('anomaly')
axarr[0].set_ylabel('anomaly')
axarr[1].set_xlim([1000,1850])
axarr[0].set_xlim([1000,1850])
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/amoc_amo_tas2.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_amo_tas2.png')



'''''
R2 values denisty v. stream function
'''''

print 'this takes agaes to run, so commented out for now...'

"""
smoothings = np.linspace(0,300,30)

max_shift = 80
coeff_det_dens_amoc = np.zeros([np.size(models),max_shift,np.size(smoothings)])
coeff_det_dens_amoc[:] = np.NAN

#f, axarr = plt.subplots(4, 1)

for smoothing_no,smoothing in enumerate(smoothings):
    for plot_no,remove_model in enumerate(models):
        all_models = models.copy()
        all_models= list(all_models)
        all_models.remove(remove_model)
        #make variable to hold multi model mean stream function data
        pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
        pmip3_model_streamfunction[:] = np.NAN
        # and mixed layer density
        pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
        pmip3_atl_tas = pmip3_model_streamfunction.copy()
        #variable holding analysis years
        expected_years = start_year+np.arange((end_year-start_year)+1)
        #Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
        for i,model in enumerate(all_models):
                print model
                tmp = pmip3_str[model]
                loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
                tmp = tmp[loc]
                yrs = pmip3_year_str[model][loc]
                data2=signal.detrend(tmp)
                data2 = data2-np.min(data2)
                data3 = data2/(np.max(data2))
                for index,y in enumerate(expected_years):
                    loc2 = np.where(yrs == y)
                    if np.size(loc2) != 0:
                        pmip3_model_streamfunction[index,i] = data3[loc2]
        pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)
        west = -23
        east = -13
        south = 64
        north = 69
        #as above but for density:
        for i,model in enumerate(all_models):
                print model
                cube = model_data_density[model]['mixed_layer_density']
                cube = cube.intersection(longitude=(west, east))
                cube = cube.intersection(latitude=(south, north))
                try:
                    cube.coord('latitude').guess_bounds()
                except:
                    print 'cube already has latitude bounds'
                try:
                    cube.coord('longitude').guess_bounds()
                except:
                    print 'cube already has longitude bounds'
                grid_areas = iris.analysis.cartography.area_weights(cube)
                tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
                coord = cube.coord('time')
                dt = coord.units.num2date(coord.points)
                yrs = np.array([coord.units.num2date(value).year for value in coord.points])
                data2=signal.detrend(tmp)
                data2 = data2-np.min(data2)
                data3 = data2/(np.max(data2))
                for index,y in enumerate(expected_years):
                    loc2 = np.where(yrs == y)
                    if np.size(loc2) != 0:
                        pmip3_mixed_layer_density[index,i] = data3[loc2]
        pmip3_multimodel_mixed_layer_density = np.mean(pmip3_mixed_layer_density, axis = 1)
        mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
        stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
        #note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
        mean_strm -= np.nanmean(mean_strm)
        mean = np.nanmean(pmip3_mixed_layer_density, axis = 1)
        stdev = np.nanstd(pmip3_mixed_layer_density, axis = 1)
        mean -= np.nanmean(mean)
        X = expected_years
        for shift in np.arange(max_shift):
            if shift == 0:
                x = rm.running_mean(mean[0::],smoothing)
            else:
                x = rm.running_mean(mean[0:-1*shift],smoothing)
            y = rm.running_mean(mean_strm[shift::],smoothing)
            loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
            x = x[loc]
            y = y[loc]
            r = scipy.stats.linregress(x,y)[2]
            r2 = r*r
            coeff_det_dens_amoc[plot_no,shift,smoothing_no] = r2


plt.close('all')
z = np.nanmean(coeff_det_dens_amoc,axis = 0)
xlab , ylab = np.meshgrid(np.arange(max_shift),smoothings)
plt.contourf(xlab , ylab,np.fliplr(np.rot90(z,3)),200)
cb1 = plt.colorbar()
CS = plt.contour(xlab , ylab,np.fliplr(np.rot90(z,3)),15,colors='k')
plt.clabel(CS, fontsize=12, inline=1)
plt.xlabel('lag (years)')
plt.ylabel('smoothing (years)')
cb1.set_label('Variance explained (R$^2$)')
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/amoc_density_lag_fig.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_density_lag_fig.png')
"""


'''''''''''''''
'''''''''''''''



#f, axarr = plt.subplots(4, 1)
plt.close('all')
for plot_no,remove_model in enumerate(models):
    all_models = models.copy()
    all_models= list(all_models)
    all_models.remove(remove_model)
    #make variable to hold multi model mean stream function data
    pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
    pmip3_model_streamfunction[:] = np.NAN
    # and mixed layer density
    pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
    pmip3_atl_tas = pmip3_model_streamfunction.copy()
    #variable holding analysis years
    expected_years = start_year+np.arange((end_year-start_year)+1)
    #Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
    for i,model in enumerate(all_models):
            print model
            tmp = pmip3_str[model]
            loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
            tmp = tmp[loc]
            yrs = pmip3_year_str[model][loc]
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_model_streamfunction[index,i] = data3[loc2]
    pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)
    west = -23
    east = -13
    south = 64
    north = 69
    #as above but for density:
    for i,model in enumerate(all_models):
            print model
            cube = model_data_density[model]['mixed_layer_density']
            cube = cube.intersection(longitude=(west, east))
            cube = cube.intersection(latitude=(south, north))
            try:
                cube.coord('latitude').guess_bounds()
            except:
                print 'cube already has latitude bounds'
            try:
                cube.coord('longitude').guess_bounds()
            except:
                print 'cube already has longitude bounds'
            grid_areas = iris.analysis.cartography.area_weights(cube)
            tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
            coord = cube.coord('time')
            dt = coord.units.num2date(coord.points)
            yrs = np.array([coord.units.num2date(value).year for value in coord.points])
            data2=signal.detrend(tmp)
            data2 = data2-np.min(data2)
            data3 = data2/(np.max(data2))
            for index,y in enumerate(expected_years):
                loc2 = np.where(yrs == y)
                if np.size(loc2) != 0:
                    pmip3_mixed_layer_density[index,i] = data3[loc2]
    pmip3_multimodel_mixed_layer_density = np.mean(pmip3_mixed_layer_density, axis = 1)
    mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
    stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
    #note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
    mean_strm -= np.nanmean(mean_strm)
    mean = np.nanmean(pmip3_mixed_layer_density, axis = 1)
    stdev = np.nanstd(pmip3_mixed_layer_density, axis = 1)
    mean -= np.nanmean(mean)
    #mean *= 3.0
    X = expected_years
    #plt.close('all')
    # fig, ax =plt.subplots(1)
    steps = 50.0
    # for i in np.arange(steps):
    #     ax.fill_between(X, rm.running_mean(mean,20)+rm.running_mean(stdev,20)*(i/steps),rm.running_mean(mean,20)-rm.running_mean(stdev,20)*(i/steps), facecolor='k',edgecolor='', alpha=1.0/(i+1.0))
    smoothing_per = 5
    plt.plot(expected_years+30.0,rm.running_mean(mean,smoothing_per)*1.5,'k',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 N. Iceland seawater density')
    # axarr[1].plot(expected_years,rm.running_mean(mean,5),'k',linewidth = 2.0,alpha = 0.5)
    plt.plot(expected_years,rm.running_mean(mean_strm,smoothing_per),'g',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 26N AMOC strength')
    print 'Do dorrelations with diffreent values for teh offset year and come up with the best lag'




'''
bivalve_data = bivalve_data_initial.copy()/3.0
#plot unfiltered data
y2 = bivalve_data-np.nanmean(bivalve_data)
y2 = rm.running_mean(y2,smoothing_per)
plt.plot(bivalve_year+40.0,y2,'r',linewidth = 2,alpha = 0.75,label = 'N. Iceland bivalve $\delta^{18}$O')
'''


plt.xlabel('Calendar Year')
plt.ylabel('Variable anomaly')
plt.xlim([1000,1850])
green = mpatches.Patch(color='green', label='PMIP3 AMOC strength')
black = mpatches.Patch(color='black', label='PMIP3 N. Iceland density shifted by +30 years on x-axis')
plt.legend(handles=[green,black])
plt.show(block = False)

plt.savefig('/home/ph290/Documents/figures/amoc_density_fig2.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_density_fig2.png')

'''''''''''''''
'''''''''''''''


import iris
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
from cdo import *
cdo = Cdo()
import iris.quickplot as qplt
import statsmodels.api as sm
import cartopy.feature as cfeature
import iris.plot as iplt
import cartopy.crs as ccrs

def model_names(directory):
        files = glob.glob(directory+'*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models



input_dissic = '/data/NAS-ph290/ph290/cmip5/rcp85/dissic/merged/'
input_fgco2 = '/data/NAS-ph290/ph290/cmip5/rcp85/fgco2/merged/'
tmp_dir = '/data/dataSSD0/ph290/tmp/'
output_dir = '/data/NAS-ph290/ph290/cmip5/rcp85/c_transport_divergence/'

start_year = 2016
end_year = 2089

models1 = model_names(input_dissic)
models2 = model_names(input_fgco2)
models = list(set(models1).intersection(set(models2)))
models.remove('inmcm4')
models.remove('BNU-ESM') #no information about level bounds
models.remove('MIROC-ESM-CHEM') # seems to have different horizontal grids for 2d and 3d files? Is this just a mistake with fgco2?
models.remove('GFDL-ESM2G') # removed because isopycnal model, so the approach here will not work...
models = np.array(models)

transport_divergence_cubes = {}

for model in models:
    print model
    #########
    # test if file exists
    ########
    test = os.path.isfile(output_dir+model+'transport_divergence.nc')
    if test == False:
    	#########
    	#Dissic
    	#########
    	dissic_cube = iris.load_cube(input_dissic+'*'+model+'_*.nc','mole_concentration_of_dissolved_inorganic_carbon_in_sea_water')
    	coord = dissic_cube.coord('time')
    	dt = coord.units.num2date(coord.points)
    	year = np.array([coord.units.num2date(value).year for value in coord.points])
    	loc1 = np.where(year == start_year)
    	loc2 = np.where(year == end_year)
    	tmp_cube = dissic_cube[0].copy()
    	tmp_cube_data = tmp_cube.data.copy()
    # 	try:
    	depth_name = str(dissic_cube.coord(dimensions = 1).standard_name)
    	if depth_name == 'None':
    		depth_name = str(dissic_cube.coord(dimensions = 1).long_name)
    # 	except:
    # 		depth_name = 'ocean sigma over z coordinate'
    	for i in range(np.shape(dissic_cube)[1]):
    		thickness = dissic_cube.coord(depth_name).bounds[i,1] - dissic_cube.coord(depth_name).bounds[i,0]
    		tmp_cube_data[i,:,:] *= thickness


    	tmp_cube.data = tmp_cube_data
    	dissic_cube *= tmp_cube
    	dissic_cube_integrated = dissic_cube.collapsed(depth_name,iris.analysis.SUM)

    	dissic_cube_integrated1 = dissic_cube_integrated[loc1[0]]
    	dissic_cube_integrated2 = dissic_cube_integrated[loc2[0]]
    	dissic_cube_integrated_diff = dissic_cube_integrated1.copy()
    	dissic_cube_integrated_diff.data = dissic_cube_integrated2.data - dissic_cube_integrated1.data
    	#converting units to kg/m2:
    	dissic_cube_integrated_diff *= (12.0/1000.0)
    	#the line below is just to get rid of the left over time dimension
    	dissic_cube_integrated_diff = dissic_cube_integrated_diff[0]

    	#########
    	#fgco2
    	#########
    	fgco2_cube = iris.load_cube(input_fgco2+'*'+model+'_*.nc','surface_downward_mass_flux_of_carbon_dioxide_expressed_as_carbon')
    	coord = fgco2_cube.coord('time')
    	dt = coord.units.num2date(coord.points)
    	year2 = np.array([coord.units.num2date(value).year for value in coord.points])
    	month2 = np.array([coord.units.num2date(value).month for value in coord.points])
    	loc1 = np.where((year2 >= start_year) & (month2 >= 6))[0]
    	loc2 = np.where((year2 == end_year) & (month2 < 6))[0]
    	fgco2_cube = fgco2_cube[loc1[0]:loc2[-1]]
    	no_days = fgco2_cube.coord('time').points[-1] - fgco2_cube.coord('time').points[0]
    	day_sec = 60.0 * 60.0 * 24.0
    	# air-sea CO2 flux in kg-C/m2/s, so converting mean value into total in Kg/C/m2
    	fgco2_cube_integrated = fgco2_cube.collapsed('time',iris.analysis.MEAN) * day_sec * no_days

    	#########
    	#combining
    	#########
    	transport_divergence = fgco2_cube[0].copy()
    	transport_divergence.data = dissic_cube_integrated_diff.data - fgco2_cube_integrated.data

    	##########
    	#regridding
    	##########
    	try:
    		os.remove(tmp_dir+'tmp.nc')
    	except:
    		pass

    	try:
    		os.remove(tmp_dir+'tmp2.nc')
    	except:
    		pass


    	cdo.seltimestep('1',input = input_fgco2+'*'+model+'*.nc',output = tmp_dir+'tmp.nc')
    	tmp_cube = iris.load_cube(tmp_dir+'tmp.nc','surface_downward_mass_flux_of_carbon_dioxide_expressed_as_carbon')
    	tmp_cube.data[0,:,:] = transport_divergence.data
    	tmp_cube.rename('carbon transport divergence')
    	tmp_cube.units = 'kg/m-3'
    	iris.fileformats.netcdf.save(tmp_cube,tmp_dir+'tmp2.nc')
    	cdo.remapbil('r360x180',input = tmp_dir+'tmp2.nc',output = output_dir+model+'transport_divergence.nc')
    	os.remove(tmp_dir+'tmp.nc')
    	os.remove(tmp_dir+'tmp2.nc')

    transport_divergence_cubes[model] = iris.load_cube(output_dir+model+'transport_divergence.nc')



	# cube = iris.load_cube(output_dir+model+'transport_divergence.nc')
	# qplt.contourf(cube.collapsed('time',iris.analysis.MEAN),np.linspace(-8,8,31))
	# plt.gca().coastlines()
	# plt.show()


fgco2_cubes = {}
###########
#Regridding fgco2 files for comparison
###########
differencing_years1 = [2006,2026]
differencing_years2 = [2079,2099]

for model in models:
    #########
    # test if file exists
    ########
    test = os.path.isfile('/data/NAS-ph290/ph290/cmip5/rcp85/fgco2/merged/regridded/'+model+'fgco2_regridded.nc')
    if test == False:
        file =  glob.glob('/data/NAS-ph290/ph290/cmip5/rcp85/fgco2/merged/'+model+'_*.nc')
        cdo.remapbil('r360x180',input = file,output = '/data/NAS-ph290/ph290/cmip5/rcp85/fgco2/merged/regridded/'+model+'fgco2_regridded.nc')
    cube = iris.load_cube('/data/NAS-ph290/ph290/cmip5/rcp85/fgco2/merged/regridded/'+model+'fgco2_regridded.nc','surface_downward_mass_flux_of_carbon_dioxide_expressed_as_carbon')
    fgco2_cubes[model] = {}
    fgco2_cubes[model]['all'] = cube
    coord = cube.coord('time')
    dt = coord.units.num2date(coord.points)
    years = np.array([coord.units.num2date(value).year for value in coord.points])
    loc1 = np.where((years >= differencing_years1[0]) & (years < differencing_years1[1]))[0]
    loc2 = np.where((years > differencing_years2[0]) & (years <= differencing_years2[1]))[0]
    cube1 = cube[loc1].collapsed('time',iris.analysis.MEAN)
    cube2 = cube[loc2].collapsed('time',iris.analysis.MEAN)
    fgco2_cubes[model]['difference'] = cube2 - cube1


###########
#calculating the ensemble mean
###########
#transport_divergence
transport_divergence_cubes_data = np.ma.zeros([np.size(models),180,360])
for i,model in enumerate(models):
    transport_divergence_cubes_data[i,:,:] = transport_divergence_cubes[model].data


transport_divergence_cubes_data_mean = np.ma.mean(transport_divergence_cubes_data,axis = 0)
transport_divergence_cubes_mean = cube[0].copy()
transport_divergence_cubes_mean.data = transport_divergence_cubes_data_mean
plt.close('all')
qplt.contourf(transport_divergence_cubes_mean,31)
plt.savefig('/home/ph290/Documents/figures/divergence.png')
transport_divergence_cubes_stdev = cube[0].copy()
transport_divergence_cubes_stdev.data = np.ma.std(transport_divergence_cubes_data,axis = 0)

#fgco2
fgco2_cubes_data = np.ma.zeros([np.size(models),180,360])
for i,model in enumerate(models):
    fgco2_cubes_data[i,:,:] = fgco2_cubes[model]['difference'].data


fgco2_cubes_data_mean = np.ma.mean(fgco2_cubes_data,axis = 0)
fgco2_cubes_mean = cube[0].copy()
fgco2_cubes_stdev = cube[0].copy()
fgco2_cubes_mean.data = fgco2_cubes_data_mean
plt.close('all')
qplt.contourf(fgco2_cubes_mean,31)
plt.savefig('/home/ph290/Documents/figures/fgco2.png')
fgco2_cubes_stdev.data = np.ma.std(fgco2_cubes_data,axis = 0)

###########
#calculating the difference from mean
###########
for i,model in enumerate(models):
    fgco2_cubes[model]['difference_from_mean'] = fgco2_cubes[model]['difference'] - fgco2_cubes_mean


###########
#Read this data in to np array
###########
fgco2_difference_from_mean_data = np.ma.zeros([np.size(models),180,360])
for i,model in enumerate(models):
    fgco2_difference_from_mean_data[i,:,:] = fgco2_cubes[model]['difference_from_mean'].data


yearsec = 60.0*60.0*24.0*360.0
mean_fgco2_diff_from_mean = cube[0].copy()
mean_fgco2_diff_from_mean.data = np.mean(fgco2_difference_from_mean_data * yearsec,axis = 0)


'''
x = fgco2_difference_from_mean_data[4,:,:]
x = np.reshape(x,np.size(x))
x = x[np.where(x.mask == False)]
y = transport_divergence_cubes_data[4,:,:]
y = np.reshape(y,np.size(y))
y = y[np.where(y.mask == False)]
xsort = np.argsort(x)
x = x[xsort]
y = y[xsort]

xb = sm.add_constant(x)
model = sm.OLS(y,xb)
results = model.fit()

x2 = np.linspace(np.min(x),np.max(x),np.size(x))
y2 = results.predict(sm.add_constant(x2))

from statsmodels.stats.outliers_influence import summary_table

st, data, ss2 = summary_table(results, alpha=0.001)
fittedvalues = data[:,2]
predict_mean_se  = data[:,3]
predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
predict_ci_low, predict_ci_upp = data[:,6:8].T

plt.scatter(x,y)
plt.plot(x, fittedvalues, 'k', lw=2)
plt.plot(x, predict_ci_low, 'r--', lw=2)
plt.plot(x, predict_ci_upp, 'r--', lw=2)
plt.plot(x, predict_mean_ci_low, 'r--', lw=2)
plt.plot(x, predict_mean_ci_upp, 'r--', lw=2)
plt.xlim(-10.0e-9,10e-9)
plt.ylim(-50,170)
plt.show()



for i,model in enumerate(models):
    fig = plt.figure(figsize=(12, 5))
    plt.subplot(121)
    qplt.contourf(,31)
    plt.gca().coastlines()
    plt.subplot(122)
    qplt.contourf(, 31)
    plt.gca().coastlines()
    plt.show()


fig = plt.figure(figsize=(12, 5))
plt.subplot(121)
qplt.contourf(transport_divergence_cubes_stdev, np.linspace(0,10,31))
plt.title('stdev carbon trasport divergence beteen 2016 and 2089')
plt.gca().coastlines()
plt.subplot(122)
qplt.contourf(fgco2_cubes_stdev, np.linspace(0,1.2e-9,31))
plt.title('stdev fgco2 change (2006-2226,2079,2099)')
plt.gca().coastlines()
plt.show()

'''

plt.close('all')
fig = plt.figure(figsize=(8, 12))
ax1 = plt.subplot(311,projection=ccrs.PlateCarree(central_longitude=0.0))
plt_cube = fgco2_cubes_stdev
minv = 0.0
maxv = 1.2e-9
plt_cube.data[np.where(plt_cube.data > maxv)] = maxv
plt_cube.data[np.where(plt_cube.data < minv)] = minv
plot1 = iplt.contourf(plt_cube, np.linspace(minv,maxv,31),cmap = 'bwr')
bar1 = plt.colorbar(plot1, orientation='horizontal', extend='both')
ax1.add_feature(cfeature.LAND,facecolor='#f6f6f6')
ax1.add_feature(cfeature.COASTLINE)

ax1.set_title('stdev fgco2 change (2006-2226,2079-2099)')
ax2 = plt.subplot(312,projection=ccrs.PlateCarree(central_longitude=0.0))
plt_cube = transport_divergence_cubes_stdev
minv = 0.0
maxv = 5.0
plt_cube.data[np.where(plt_cube.data > maxv)] = maxv
plt_cube.data[np.where(plt_cube.data < minv)] = minv
plot2 = iplt.contourf(plt_cube, np.linspace(minv,maxv,31),cmap = 'bwr')
bar2 = plt.colorbar(plot2, orientation='horizontal', extend='both')
ax2.add_feature(cfeature.LAND,facecolor='#f6f6f6')
ax2.add_feature(cfeature.COASTLINE)
ax2.set_title('stdev carbon transport divergence beteen 2016 and 2089')

ax3 = plt.subplot(313,projection=ccrs.PlateCarree(central_longitude=0.0))
plt_cube = fgco2_cubes_stdev
minv = 0.0
maxv = 1.2e-9
plt_cube.data[np.where(plt_cube.data > maxv)] = maxv
plt_cube.data[np.where(plt_cube.data < minv)] = minv
plot3 = iplt.contourf(plt_cube, np.linspace(minv,maxv,31),cmap = 'bwr')
bar3 = plt.colorbar(plot3, orientation='horizontal', extend='both')
qplt.contour(transport_divergence_cubes_stdev, np.linspace(0,10,5),colors = 'k',lw = 2.0)
ax3.add_feature(cfeature.LAND,facecolor='#f6f6f6')
ax3.add_feature(cfeature.COASTLINE)
ax3.set_title('stdev fgco2 with trasport divergence on top')
plt.savefig('/home/ph290/Documents/figures/divergence_fgco2.png')
plt.show(block = False)




############
#Comparing carbon flux differences with transport
############
# Perhaps correlate maps of the difference between the deviation for the mean change in CO2 uptake etween teh start and end of teh century, and the carbon transport. The hypotehsis woudl be that models that underestimate the uptake in one area woudl do so becaue they had transport of carbon to that area - so you should see an inverse correlation
# But maybe just strat by plotting some maps side to side to see if ther differences look like teh tranport maps.

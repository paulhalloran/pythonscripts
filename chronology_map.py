import iris
import pandas as pd
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import cartopy


file = '/data/NAS-geo01/ph290/misc_data/bivalve_chronologies/shelfchrons_latlon.csv'

df = pd.read_csv(file,header=0)

plt.close('all')
ax = plt.axes(projection=ccrs.PlateCarree())
ax.set_extent([-20, 10, 48, 68])

coastline = cartopy.feature.NaturalEarthFeature(category='physical',
                                                  name='coastline',
                                                  scale='10m')

land = cartopy.feature.NaturalEarthFeature(category='physical',
                                                  name='land',
                                                  scale='10m')

ax.add_feature(land, color='gray')
ax.add_feature(coastline, edgecolor='black', color='none',lw=0.1)

plt.scatter(df.lon.values,df.lat.values,color='r',edgecolor='k')

plt.title('location of existing bivalve records')
plt.savefig('/home/ph290/Documents/figures/location_of_existing_bivalve_records.pdf')
plt.show(block = False)

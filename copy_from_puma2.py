import time
import subprocess

while True:
	test = 0
        while test == 0:
        #tests to see if the file 'wait' is on PUMA, which tells us that the pp file has not yet completed copying across from monsoon
		test = subprocess.call('ssh ph290@puma.nerc.ac.uk "test -e /home/ph290/staging2/wait"', shell=True)
                time.sleep(10.0)
	subprocess.call('scp ph290@puma.nerc.ac.uk:/home/ph290/staging2/*.pp /data/NAS-ph290/ph290/from_monsoon/', shell=True)
	subprocess.call("ssh ph290@puma.nerc.ac.uk 'rm /home/ph290/staging2/*.pp'", shell=True)


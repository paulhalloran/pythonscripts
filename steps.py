import numpy as np
from random import randint
import matplotlib.pyplot as plt

number_of_points = 50

x = np.arange(number_of_points)
y =  np.random.rand(number_of_points)
lowest = 9.9e9
steps_x = []
steps_y = []



for i in range(number_of_points):
    if ((i > 0) & (i < number_of_points-1)):
        if ((y[i] < y[i-1]) & (y[i] < y[i+1])):
            loc = np.where(y[i] < steps_y)[0]
            steps_y = np.delete(steps_y,loc)
            steps_x = np.delete(steps_x,loc)
            steps_x = np.append(steps_x,x[i])
            steps_y = np.append(steps_y,y[i])


plt.close('all')
plt.plot(x,y)
size = np.size(steps_x)
for i in range(size):
    plt.plot([steps_x[i],number_of_points],[steps_y[i],steps_y[i]])
plt.show(block = False)

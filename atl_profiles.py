import iris
import numpy as np
import glob
import matplotlib.pyplot as pyplot
import iris.quickplot as qplt
import matplotlib as mpl

mpl.rcdefaults()

fsize = 20

font = {'family' : 'monospace',
        'weight' : 'bold',
        'family' : 'serif',
        'size'   : fsize}

mpl.rc('font', **font)

#files from: regridding_and_concatenating_cmip5_files_so.py
#regridding_and_concatenating_cmip5_files_so_hist.py # note rcp2.5 just because less files so easier!
files = glob.glob('/data/NAS-ph290/ph290/cmip5/picontrol/regridded/*rcp26*atlantic_profile.nc')
#glodap_so_file = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/glodap_so_Atlantic_profile.nc'
woa_so_file = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/woa_so_Atlantic_profile.nc'

plt.close('all')
fig = plt.figure(figsize = (10,5))

for file in files:
    c=iris.load_cube(file)
    qplt.plot(c[0,:,0,0],lw=2)

c=iris.load_cube(woa_so_file)
qplt.plot(c[0],lw=5,color='k')

plt.tight_layout
plt.show()

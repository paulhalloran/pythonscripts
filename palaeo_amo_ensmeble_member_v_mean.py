from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean as rm
import running_mean_post as rmp
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import scipy.interpolate
import gc
import pickle
import biggus
import seawater
import cartopy.feature as cfeature
import scipy.ndimage
import scipy.ndimage.filters
import gsw
import scipy.stats as stats
import time
import matplotlib.patches as mpatches
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
from scipy import signal
import numpy as np
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt


input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger6/'

start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


def extract_years(cube):
    try:
    	iris.coord_categorisation.add_year(cube, 'time', name='year2')
    except:
    	'already has year2'
    start_year = 850
    end_year = 1850
    loc = np.where((cube.coord('year2').points >= start_year) & (cube.coord('year2').points <= end_year))
    loc2 = cube.coord('time').points[loc[0][-1]]
    cube = cube.extract(iris.Constraint(time = lambda time_tmp: time_tmp <= loc2))
    return cube

def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a



def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


def butter_bandpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a


print 'stream function etc. calculated in palaeo_amo_paper_fig_amoc_density.py'


### Remove three quotatoin marks below if running for 1st tim eafter load

### Remove three quotatoin marks above if running for 1st tim eafter load


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=signal.detrend(bivalve_data_initial)


with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VII.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files = pickle.load(f)


###########################################
#    restrict loading to top 1000m        #
###########################################

chosen_levels = lambda cell: cell <= 500
level_above_1000 = iris.Constraint(depth=chosen_levels)

# models.remove('MRI-CGCM3') #WOUDL BE GREAT TO SORT THIS OUT!
models.remove('FGOALS-gl')
models.remove('CSIRO-Mk3L-1-2')
models.remove('HadCM3')

models = np.array(models)

#models.remove('FGOALS-s2')
# models.remove('bcc-csm1-1')
#T and/or  S filed are funny. making desnity splotch at high lats, and therefor eMLD go screwy
#models.remove('CCSM4')
#Funny last density year - need to resolve...cd Documentspyth
#Had to multiply salinity in last two files by 1000 to get in correct units...

#FGOALS-gl No sea-ice
#models.remove('FGOALS-s2')
#FGOALS models lack the required seaice data...
#and levels are upside down or something odd
#NOTE, now resolved by inverting levels using cdo invertlev ifile ofile

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


model_data = {}
ensemble = 'r1i1p1'


###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}

giss_test = 0

for i,model in enumerate(models):
	if model == 'GISS-E2-R':
		if giss_test == 0:
			pmip3_str[model] = max_strm_fun_26[i]
			pmip3_year_str[model] = model_years[i]
			giss_test += 1
	if model <> 'GISS-E2-R':
		pmip3_str[model] = max_strm_fun_26[i]
		pmip3_year_str[model] = model_years[i]


multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()
multi_model_density_yrs = multi_model_density.copy()

for i,model in enumerate(models):
	print model
 	period = 'JJA'
# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	smoothing_var = 0
	if smoothing_var > 0:
		data = gaussian_filter1d(data,smoothing_var)
	data2=signal.detrend(data)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	for index,y in enumerate(expected_years):
	    loc2 = np.where(yrs == y)
	    if np.size(loc2) != 0:
	        multi_model_density[index,i] = data3[loc2]
	        multi_model_density_yrs[index,i] = y




#f, axarr = plt.subplots(4, 1)
# plt.close('all')
#make variable to hold multi model mean stream function data
pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
pmip3_model_streamfunction[:] = np.NAN
# and mixed layer density
pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
pmip3_atl_tas = pmip3_model_streamfunction.copy()
#variable holding analysis years
expected_years = start_year+np.arange((end_year-start_year)+1)
#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
for i,model in enumerate(models):
		print model
		tmp = pmip3_str[model]
		loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
		tmp = tmp[loc]
		yrs = pmip3_year_str[model][loc]
		data2=signal.detrend(tmp)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				pmip3_model_streamfunction[index,i] = data2[loc2]



		####################################
		#as above but for density
		####################################
for i,model in enumerate(models):
	#print model
	tmp = multi_model_density[:,i]
	loc = np.where((np.logical_not(np.isnan(tmp))) & (multi_model_density_yrs[:,i] <= end_year) & (multi_model_density_yrs[:,i] >= start_year))
	tmp = tmp[loc]
	#tmp = tmp[1:-1]
	#expected_years2 = expected_years[1:-1]
	yrs = multi_model_density_yrs[:,i]
	data2=signal.detrend(tmp)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.mean(data3)
	for index,y in enumerate(expected_years):
		loc2 = np.where(yrs == y)
		if np.size(loc2) != 0:
			pmip3_mixed_layer_density[index,i] = data3[loc2]



mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
mean_dens = np.nanmean(pmip3_mixed_layer_density, axis = 1)
stdev_dens = np.nanstd(pmip3_mixed_layer_density, axis = 1)



colors = ['#b2bbbf','#804e00','#e6af0f','#5a00e1','#914d99','#007d32','#ff7419','#8c0000','#41ff5a']



### Remove three quotatoin marks below if running for 1st tim eafter load

### Remove three quotatoin marks above if running for 1st tim eafter load


##############################################
#            figure                         #
##############################################



def butter_lowpass(cutoff, order=5):
	cutoff = 1.0/cutoff
	b, a = butter(order, cutoff, btype='low', analog=False)
	return b, a
	
def butter_highpass(cutoff, order=5):
	cutoff = 1.0/cutoff
	b, a = butter(order, cutoff, btype='high', analog=False)
	return b, a

def butter_lowpass_filter(data, cutoff, order=5):
	b, a = butter_lowpass(cutoff, order=order)
	y = lfilter(b, a, data)
	cutoff = int(cutoff)
	y = np.roll(y,(cutoff/2) * -1)
	y[0:cutoff/2] = np.nan
	y[(cutoff/2) * -1::] = np.nan
	return y

def butter_highpass_filter(data, cutoff, order=5):
	b, a = butter_highpass(cutoff, order=order)
	y = lfilter(b, a, data)
	y[0:cutoff/2] = np.nan
	y[(cutoff/2) * -1::] = np.nan
	return y

# Filter requirements.
order = 2
cutoff = 10  # desired cutoff frequency

plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(4)
fig.set_figwidth(10)
# ax1 = plt.subplot2grid((2,1),(1, 0))
#gs = gridspec.GridSpec(2, 1,width_ratios=[1,1],height_ratios =[1,2])
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.85)

# ax1 = plt.subplot(gs[1])
# ax2 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[0:100,0:45])
ax2 = plt.subplot(gs[0:100,55:100])
#ax3 = plt.subplot(gs[0:45,0:100])


# smoothing = 100
# for i in range(np.shape(pmip3_mixed_layer_density)[1]):
# 	cutoff = 50
# 	#y = rm.running_mean(pmip3_mixed_layer_density[:,i],smoothing)
# 	y = butter_lowpass_filter(pmip3_mixed_layer_density[:,i].copy(), cutoff, order)
# 	loc = np.where((np.logical_not(np.isnan(y))) & (expected_years > 850) & (expected_years < 1850))
# 	y = y[loc]
# 	#y = signal.detrend(y)
# 	y = y - np.min(y)
# 	y = y/np.max(y)
# 	ax3.plot(expected_years[loc],y,color = colors[i],alpha = 0.5,linewidth = 2)



max_cutoff = 200
#low pass
for cutoff in np.linspace(1,max_cutoff,20):
# 	cutoff += 1
	r2_low = []
	r2_high = []

	from statsmodels.stats.outliers_influence import summary_table
	for i in range(np.shape(pmip3_mixed_layer_density)[1]):
		y = pmip3_mixed_layer_density[:,i].copy()
		loc = np.where((np.logical_not(np.isnan(y))) & (expected_years > 850) & (expected_years < 1850))
		y = y[loc]
# 		x = bivalve_data_initial[::-1]
# 		x_yr = bivalve_year[::-1]
		x = mean_dens
		x_yr = expected_years
		expected_years_tmp = expected_years[loc]
		common_years = np.intersect1d(x_yr,expected_years_tmp)
		loc_expected = np.where(np.in1d(expected_years_tmp,common_years))
		loc_bivalve = np.where(np.in1d(x_yr,common_years))
		x = x[loc_bivalve]
		y = y[loc_expected]
		x = x-np.min(x)
		y = y-np.min(y)
		#Low pass
		x_tmp = butter_lowpass_filter(x.copy(), cutoff, order)
		y_tmp = butter_lowpass_filter(y.copy(), cutoff, order)
		loc = np.where((np.logical_not(np.isnan(x_tmp))) & (np.logical_not(np.isnan(y_tmp))))
		if np.size(loc) > 0:
			x_tmp = x_tmp[loc]
			y_tmp = y_tmp[loc]
		y_tmp = y_tmp - np.min(y_tmp)
		y_tmp = y_tmp/np.max(y_tmp)
		x_tmp = x_tmp - np.min(x_tmp)
		x_tmp = x_tmp/np.max(x_tmp)
		xb = sm.add_constant(x_tmp)
		model = sm.OLS(y_tmp,xb)
		results = model.fit()
		r2_low.append(results.rsquared)
		#high pass
		x_tmp = butter_highpass_filter(x.copy(), cutoff, order)
		y_tmp = butter_highpass_filter(y.copy(), cutoff, order)
		loc = np.where((np.logical_not(np.isnan(x_tmp))) & (np.logical_not(np.isnan(y_tmp))))
		if np.size(loc) > 0:
			x_tmp = x_tmp[loc]
			y_tmp = y_tmp[loc]
		y_tmp = y_tmp - np.min(y_tmp)
		y_tmp = y_tmp/np.max(y_tmp)
		x_tmp = x_tmp - np.min(x_tmp)
		x_tmp = x_tmp/np.max(x_tmp)
		xb = sm.add_constant(x_tmp)
		model = sm.OLS(y_tmp,xb)
		results = model.fit()
		r2_high.append(results.rsquared)

	for i,tmp in enumerate(r2_high):
		ax1.scatter(cutoff,tmp,color = colors[i],alpha = 0.7,label = models[i])
	for i,tmp in enumerate(r2_low):
		ax2.scatter(cutoff,tmp,color = colors[i],alpha = 0.7,label = models[i])


for i,model in enumerate(models):
	plt.annotate(model, xy=(.17, .85-i*0.05),  xycoords='figure fraction',
                horizontalalignment='left', verticalalignment='center', fontsize=12,color = colors[i])

plt.annotate('a', xy=(.11, .945),  xycoords='figure fraction',
                horizontalalignment='centre', verticalalignment='center', fontsize=20)

plt.annotate('b', xy=(.495, .945),  xycoords='figure fraction',
                horizontalalignment='centre', verticalalignment='center', fontsize=20)


ax1.set_xlim([0,max_cutoff])
ax2.set_xlim([0,max_cutoff])
ax1.set_ylim([0,1])
ax2.set_ylim([0,1])
ax1.set_title('High-pass filtered')
ax2.set_title('Low-pass filtered')
ax1.set_xlabel('Filter length (years)')
ax2.set_xlabel('Filter length (years)')
ax1.set_ylabel('Fraction of variance explained (R$^2$)')


plt.savefig('/home/ph290/Documents/figures/palaeo_amo_ensmeble_member_v_mean.ps')
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_ensmeble_member_v_mean.png')

plt.show(block = False)

'''
plt.rc('legend',**{'fontsize':10})
plt.close('all')
ax = plt.subplots(figsize=(10, 5))


for y,d in enumerate(pmip3_mixed_layer_density):
	plt.plot(expected_years,rm.running_mean(pmip3_mixed_layer_density[:,y],10),'#666666')

plt.plot(expected_years,rm.running_mean(mean_dens,10),'r',lw=2,alpha = 0.6)

plt.xlabel('year')
plt.ylabel('normalised density anomaly')

plt.savefig('/home/ph290/Documents/figures/palaeo_amo_ensmeble_member_v_mean_ts.png')
plt.show()

'''






import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma


#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models

'''
Defining directory locations, variable locatoins etc.
You may well need to edit the text within the quotation marks to adapt the script to work with your data
'''

'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
temporary_file_space3 = '/data/data1/ph290/tmp/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/NAS-ph290/ph290/cmip5/past1000/'
#Directories where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/data1/ph290/cmip5/last_1000_density/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiment = 'past1000'
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.
time_period = 'Omon'

#lats and lons for analysis:
lon1 = -23.0
lon2 = -13.0
lat1 = 64.0
lat2 = 79.0

'''
Main bit of code follows...
'''

models = model_names(input_directory)

ensemble = 'r1i1p1'

#########
# MLD calculation (currently temperature based, but coudl easily change this to density)
#########


seasons = ['JJA','SON','DJF','MAM']

for model in models:
# model = models[0]
	print model
	#cleaning up...
	#subprocess.call('rm '+temporary_file_space1+'*.nc', shell=True)
	#subprocess.call('rm '+temporary_file_space2+'*.nc', shell=True)
	temp_file1 = str(uuid.uuid4())+'.nc'
	temp_file2 = str(uuid.uuid4())+'.nc'
	temp_file3 = str(uuid.uuid4())+'.nc'
	temp_file4 = str(uuid.uuid4())+'.nc'
	temp_file5 = str(uuid.uuid4())+'.nc'
	temp_file6 = str(uuid.uuid4())+'.nc'
	files1 = glob.glob(input_directory+'thetao_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
	files2 = glob.glob(input_directory+'so_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
	#reads in the files to process
	sizing1 = np.size(files1)
	sizing2 = np.size(files2)
	#checks that we have some files to work with for this model, experiment and variable
	if not ((sizing1 == 0) | (sizing2 == 0)):
		#######################################
		#             thetao                  #
		#######################################
		print 'reading in: thetao '+model
		if sizing1 > 1:
			#if the data is split across more than one file, it is combined into a single file for ease of processing
			#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
			tmp = []
			for file in files1:
				tmp.append(int(file.split('_')[-1].split('-')[0]))
			order = np.argsort(tmp)
			files1_joined = ' '.join(np.array(files1)[order])
			levels = str(cdo.showlevel(input = files1[0])[0]).split(' ')[0:25]
			levels = ",".join(levels)
			print 'merging thetao files and selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
			#merge together different files1 from the same experiment
			try:
				subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
			except:
				pass
			#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files1 in right order until otherwise tested).
			cdo.select('level='+levels+',name=thetao ',input=files1_joined, output = temporary_file_space1+temp_file1,  options = '-P 7')
		if sizing1 == 1:
			print 'No need to merge files1, but selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
			try:
				subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
			except:
				pass
			min_lev = str(cdo.showlevel(input = files1[0])[0]).split(' ')[0]
			cdo.select('level='+levels+',name=thetao',input = files1[0], output = temporary_file_space1+temp_file1)
		#######################################
		#                 SO                 #
		#######################################
		print 'reading in: so '+model
		if sizing2 > 1:
			#if the data is split across more than one file, it is combined into a single file for ease of processing
			#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
			tmp = []
			for file in files2:
				tmp.append(int(file.split('_')[-1].split('-')[0]))
			order = np.argsort(tmp)
			files2_joined = ' '.join(np.array(files2)[order])
			levels = str(cdo.showlevel(input = files2[0])[0]).split(' ')[0:25]
			levels = ",".join(levels)
			print 'merging so files and selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
			#merge together different files from the same experiment
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
			except:
				pass
			#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files in right order until otherwise tested).
			cdo.select('level='+levels+',name=so ',input=files2_joined, output = temporary_file_space2+temp_file2,  options = '-P 7')
		if sizing2 == 1:
			print 'No need to merge files, but selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
			try:
				subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
			except:
				pass
			min_lev = str(cdo.showlevel(input = files2[0])[0]).split(' ')[0]
			cdo.select('level='+levels+',name=thetao',input = files2[0], output = temporary_file_space2+temp_file2)
	#######################################
	#               density               #
	#######################################
	print 'preparing files for density calculation'
	#cdo.setname('to', input = '-subc,273.15 '+ temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file2,  options = '-P 7')
	#cdo.setname('sao', input = temporary_file_space3+temp_file2, output = temporary_file_space2+temp_file4,  options = '-P 7')
	#tmp = ' '.join([temporary_file_space3+temp_file1,temporary_file_space3+temp_file2])
	cdo.merge(input = '-setname,tho, -subc,273.15 '+ temporary_file_space1+temp_file1 + ' -setname,s, '+temporary_file_space2+temp_file2, output = temporary_file_space3+temp_file1,  options = '-P 7')
	subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
	subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
	#cdo.merge(input = tmp, output = temporary_file_space3+temp_file3,  options = '-P 7')
	#cdo.rhopot(input = temporary_file_space3+temp_file1, output = temporary_file_space1+temp_file2,  options = '-P 7')		
	subprocess.call('rm '+temporary_file_space1+temp_file2, shell=True)
	######################
	#  annual mean MLD   #
	######################
	#notes = we coudl just wok with a smaller region, then read in the x and y for the depth cube tiling - then if required regrid later - is there really any need to regrid at all?
	print 'calculating density (using potential temperatures as criteria is potential density) for annual mean, then regridding files for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
	cdo.remapbil('r360x180', input = '-yearmean -rhopot -adisit '+ temporary_file_space3+temp_file1, output = temporary_file_space1+temp_file2,  options = '-P 7')
	# = note this is not added at the moment because was going to be difficult to identify the box size after regridding... -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '
	density_cube = iris.load_cube(temporary_file_space1+temp_file2)
	depth_cube = density_cube[0].copy()
	try:
		density_cube.coord('depth')
		depths = depth_cube.coord('depth').points
	except:
		depths = depth_cube.coord('ocean sigma over z coordinate').points
		#for memorys sake, do one year at a time..
	depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[360,180,1]),0,2))
	try:
		density_cube.coord('depth')
		MLD_out = density_cube.extract(iris.Constraint(depth = np.min(depth_cube.data)))
		surface_density_out = MLD_out.copy()
	except:
		MLD_out = density_cube[:,0,:,:]
		surface_density_out = MLD_out.copy()
	MLD_out_data = MLD_out.data
	surface_density_data = surface_density_out.data.copy()
	print'calculating mixed layer depths'
	for i in range(np.shape(MLD_out)[0]):
		density_tmp = density_cube[i]
		density_diff = density_tmp[0,:,:].copy()
		density_diff2 = density_tmp.copy()
		density_diff += 0.125 # Levitus (1982) density criteria
		density_diff2.data = np.flipud(np.tile(density_diff.data,[np.shape(density_cube[i])[0],1,1]))
		idx_mld = density_tmp.data <= density_diff2.data
		MLD = ma.masked_all_like(density_tmp.data.data)
		density = ma.masked_all_like(density_tmp.data.data)
		MLD[idx_mld] = depth_cube.data[idx_mld]
		density[idx_mld] = density_tmp.data[idx_mld]
		MLD_out_data[i,:,:] = np.ma.max(MLD.data,axis=0)
		surface_density_data[i,:,:] = np.ma.mean((MLD.data * MLD.data) / np.ma.max(MLD.data,axis=0),axis=0)
	MLD_out.data = MLD_out_data
	surface_density_out.data = surface_density_data
	MLD_out.data.mask = density_cube.data.mask
	surface_density_out.data.mask = density_cube.data.mask
	#qplt.contourf(MLD_out[0],50)
	#plt.show(block=True)
	iris.fileformats.netcdf.save(MLD_out,'/data/data1/ph290/cmip5/last1000/mld/'+'mixed_later_depth_'+model+'_past1000_'+ensemble+'_'+annual_mean+'.nc')
	iris.fileformats.netcdf.save(MLD_out,'/data/data1/ph290/cmip5/last1000/mld/'+'density_mixed_later_'+model+'_past1000_'+ensemble+'_'+annual_mean+'.nc')	
	###########################
	#  seasonal meaned MLD    #
	###########################
	for season in seasons:
# 		season = seasons[0]
		print season
		subprocess.call('rm '+temporary_file_space1+temp_file2, shell=True)
		print 'calculating density (using potential temperatures as criteria is potential density), selecting season, then regridding files for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
		cdo.remapbil('r360x180', input = '-selseas,'+season+' -seasmean -rhopot -adisit '+ temporary_file_space3+temp_file1, output = temporary_file_space1+temp_file2,  options = '-P 7')
		# = note this is not added at the moment because was going to be difficult to identify the box size after regridding... -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '
		density_cube = iris.load_cube(temporary_file_space1+temp_file2)
		depth_cube = density_cube[0].copy()
		try:
			density_cube.coord('depth')
			MLD_out = density_cube.extract(iris.Constraint(depth = np.min(depth_cube.data)))
			surface_density_out = MLD_out.copy()
		except:
			MLD_out = density_cube[:,0,:,:]
			surface_density_out = MLD_out.copy()
		MLD_out_data = MLD_out.data
		surface_density_data = surface_density_out.data.copy()
		print'calculating mixed layer depths'
		for i in range(np.shape(MLD_out)[0]):
			density_tmp = density_cube[i]
			density_diff = density_tmp[0,:,:].copy()
			density_diff2 = density_tmp.copy()
			density_diff += 0.125 # Levitus (1982) density criteria
			density_diff2.data = np.flipud(np.tile(density_diff.data,[np.shape(density_cube[i])[0],1,1]))
			idx_mld = density_tmp.data <= density_diff2.data
			MLD = ma.masked_all_like(density_tmp.data.data)
			density = ma.masked_all_like(density_tmp.data.data)
			MLD[idx_mld] = depth_cube.data[idx_mld]
			density[idx_mld] = density_tmp.data[idx_mld]
			MLD_out_data[i,:,:] = np.ma.max(MLD.data,axis=0)
			surface_density_data[i,:,:] = np.ma.mean((MLD.data * MLD.data) / np.ma.max(MLD.data,axis=0),axis=0)
		MLD_out.data = MLD_out_data
		surface_density_out.data = surface_density_data
		MLD_out.data.mask = density_cube.data.mask
		surface_density_out.data.mask = density_cube.data.mask
		#qplt.contourf(MLD_out[0],50)
		#plt.show(block=True)
		iris.fileformats.netcdf.save(MLD_out,'/data/data1/ph290/cmip5/last1000/mld/'+'mixed_later_depth_'+model+'_past1000_'+ensemble+'_'+season+'.nc')
		iris.fileformats.netcdf.save(surface_density_out,'/data/data1/ph290/cmip5/last1000/mld/'+'density_mixed_later_'+model+'_past1000_'+ensemble+'_'+season+'.nc')
	subprocess.call('rm '+temporary_file_space1+temp_file2, shell=True)
	subprocess.call('rm '+temporary_file_space3+temp_file1, shell=True)


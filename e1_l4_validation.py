import pandas as pd
import iris
import numpy as np
import datetime
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import netCDF4

#Locations, definitive from Tim
#L4
# lat=50.0 +  15.00/60.0
# lon = -4.0 - 13.02/60.0
# E1
lat =  50.0 + 02.00/60.0
lon = -4.0 - 22.00/60.0

Uk_mooring_directory = '/data/NAS-geo01/ph290/observations/'

l4_file = Uk_mooring_directory + 'western_channel_observatory_l4_ctd_temperature.csv'
e1_file = Uk_mooring_directory + 'western_channel_observatory_e1_ctd_temperature.csv'

#l4
# data = pd.read_csv(l4_file, header = 12,usecols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23])
#e1
data = pd.read_csv(e1_file, header = 10,usecols=[0,1,2,3,4,5,6,7,8,9])
#reads the csv into a pandas array, similar to a dictionary

format_str = '%d/%m/%Y' # The format
#always find the correct date format with the datetime library online before formatting dates

data['Date'] = [datetime.datetime.strptime(i, format_str) for i in data['Date (dd/mm/yyyy)']]
#adds a column with the new datatime format

#l4
# data['AvgDepth'] = data[['45m_ctd_sbe19','50m_ctd_sbe19']].mean(axis = 1)
# data['surf_bottom_diff'] = data['2m_ctd_sbe19']- data['AvgDepth']
#E1
data['AvgDepth'] = data[[' Temperature \xb0C at 40 m',' Temperature \xb0C at 50 m',' Temperature \xb0C at 60 m',' Temperature \xb0C at 70 m',' Temperature \xb0C at 80 m']].mean(axis = 1)
data['surf_bottom_diff'] = data['Temperature \xb0C at 0 m']- data['AvgDepth']

#help(pd.read_csv)
#q to escape

###########Read in Marsh Model Data

UK_Marsh_grid_path = '/data/NAS-ph290/ph290/s2p3_rv2.0_output/'

Marsh_surf = UK_Marsh_grid_path + 'surface_temperature_english_chanel.nc'
Marsh_bot = UK_Marsh_grid_path + 'bottom_temperature_english_chanel.nc'

surf_grid = iris.load_cube(Marsh_surf)
bot_grid = iris.load_cube(Marsh_bot)

temp_diff = surf_grid - bot_grid

index_lat = temp_diff.coord('latitude').nearest_neighbour_index(lat)
index_lon = temp_diff.coord('longitude').nearest_neighbour_index(lon)

e1_Marsh = temp_diff[:,index_lat, index_lon].data
#temperature difference values from the Marsh model at the mooring location

e1_Marsh_dates = netCDF4.num2date(temp_diff.coord('time').points,temp_diff.coord('time').units.origin,temp_diff.coord('time').units.calendar)
#reformat the time dimension so it matches the csv file format

#####my_plot
plt.close("all")
plt.scatter(data['Date'].values, data['surf_bottom_diff'].values,color='red', alpha = .5, label = "Observations")
plt.plot(e1_Marsh_dates, e1_Marsh, alpha = .5, label = "Model")
plt.title("E1 Mooring and Model Validation")
plt.legend()
plt.show(block = False)




e1_Marsh_dates_ordinal = [i.toordinal() for i in e1_Marsh_dates]
marsh_value_for_date = [np.interp(x.toordinal(),e1_Marsh_dates_ordinal,e1_Marsh) for x in data['Date'].dropna()]

plt.close("all")
plt.scatter(data['Date'].values, data['surf_bottom_diff'].values,color='red', alpha = .5, label = "Observations")
plt.plot(e1_Marsh_dates, e1_Marsh, alpha = .5, label = "Model")
#plt.scatter(data['Date'].values, marsh_value_for_date, color='blue',alpha = .5, label = "Model")
plt.title("E1 Mooring and Model Validation")
plt.legend()
# plt.xlim([datetime.date(2002, 1, 1), datetime.date(2016, 1, 1)])
plt.show(block = False)

import iris
import iris.coord_categorisation
import numpy as np

print 'please note, this script has not yet been tested with observations, so perform your own tests to make sure that it can replicate the official index'

#Reading in the MONTHLY SST data from the netcdf file
cube = iris.load_cube('/data/NAS-ph290/ph290/cmip5/historical/tos_Omon_HadCM3_historical_r1i1p1_193412-195911.nc')

iris.coord_categorisation.add_season_year(cube, 'time', name='season_year')
iris.coord_categorisation.add_month_number(cube, 'time', name='month_number')
cube = cube.aggregated_by(['season_year','month_number'], iris.analysis.MEAN)

coord = cube.coord('time')
dt = coord.units.num2date(coord.points)
years = np.array([coord.units.num2date(value).year for value in coord.points])

lon_west = -170
lon_east = -120
lat_south = -0.5
lat_north = 0.5 

cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

timeseries = cube_region.collapsed(['latitude','longitude'],iris.analysis.MEAN).data

years_1 = years[np.where(cube.coord('month_number').points == 1)]
timeseries_1 = timeseries[np.where(cube.coord('month_number').points == 1)]
years_2 = years[np.where(cube.coord('month_number').points == 2)]
timeseries_2 = timeseries[np.where(cube.coord('month_number').points == 2)]
years_3 = years[np.where(cube.coord('month_number').points == 3)]
timeseries_3 = timeseries[np.where(cube.coord('month_number').points == 3)]
years_4 = years[np.where(cube.coord('month_number').points == 4)]
timeseries_4 = timeseries[np.where(cube.coord('month_number').points == 4)]
years_5 = years[np.where(cube.coord('month_number').points == 5)]
timeseries_5 = timeseries[np.where(cube.coord('month_number').points == 5)]
years_6 = years[np.where(cube.coord('month_number').points == 6)]
timeseries_6 = timeseries[np.where(cube.coord('month_number').points == 6)]
years_7 = years[np.where(cube.coord('month_number').points == 7)]
timeseries_7 = timeseries[np.where(cube.coord('month_number').points == 7)]
years_8 = years[np.where(cube.coord('month_number').points == 8)]
timeseries_8 = timeseries[np.where(cube.coord('month_number').points == 8)]
years_9 = years[np.where(cube.coord('month_number').points == 9)]
timeseries_9 = timeseries[np.where(cube.coord('month_number').points == 9)]
years_10 = years[np.where(cube.coord('month_number').points == 10)]
timeseries_10 = timeseries[np.where(cube.coord('month_number').points == 10)]
years_11 = years[np.where(cube.coord('month_number').points == 11)]
timeseries_11 = timeseries[np.where(cube.coord('month_number').points == 11)]
years_12 = years[np.where(cube.coord('month_number').points == 12)]
timeseries_12 = timeseries[np.where(cube.coord('month_number').points == 12)]




thirty_yr_means = {}
thirty_yr_means['1'] = []
thirty_yr_means['2'] = []
thirty_yr_means['3'] = []
thirty_yr_means['4'] = []
thirty_yr_means['5'] = []
thirty_yr_means['6'] = []
thirty_yr_means['7'] = []
thirty_yr_means['8'] = []
thirty_yr_means['9'] = []
thirty_yr_means['10'] = []
thirty_yr_means['11'] = []
thirty_yr_means['12'] = []



meaning_years = {}
meaning_years['1'] = []
meaning_years['2'] = []
meaning_years['3'] = []
meaning_years['4'] = []
meaning_years['5'] = []
meaning_years['6'] = []
meaning_years['7'] = []
meaning_years['8'] = []
meaning_years['9'] = []
meaning_years['10'] = []
meaning_years['11'] = []
meaning_years['12'] = []


for j in np.arange(12)+1:
	if j == 0:
		years_tmp = years_0
		timeseries_tmp = timeseries_0
	if j == 1:
		years_tmp = years_1
		timeseries_tmp = timeseries_1
	if j == 2:
		years_tmp = years_2
		timeseries_tmp = timeseries_2
	if j == 3:
		years_tmp = years_3
		timeseries_tmp = timeseries_3
        if j == 4:
                years_tmp = years_4
                timeseries_tmp = timeseries_4
        if j == 5:
                years_tmp = years_5
                timeseries_tmp = timeseries_5
        if j == 6:
                years_tmp = years_6
                timeseries_tmp = timeseries_6
        if j == 7:
                years_tmp = years_7
                timeseries_tmp = timeseries_7
        if j == 8:
                years_tmp = years_8
                timeseries_tmp = timeseries_8
        if j == 9:
                years_tmp = years_9
                timeseries_tmp = timeseries_9
        if j == 10:
                years_tmp = years_10
                timeseries_tmp = timeseries_10
        if j == 11:
                years_tmp = years_11
                timeseries_tmp = timeseries_11
	for i in years_tmp[::5]:
		meaning_years[str(j)].append(i)
		loc = np.where(years_tmp == i)[0][0]
		try:
			thirty_yr_means[str(j)].append(np.mean(timeseries_tmp[loc-30:loc+30]))
		except:
			thirty_yr_means[str(j)].append(np.NAN)


# meaning_years = np.array(meaning_years)
# thirty_yr_means = np.array(thirty_yr_means)

nino34 = []
for i,timeseries_value in enumerate(timeseries):
	meaning_years_tmp = meaning_years[str(cube.coord('month_number').points[i])]
	thirty_yr_means_tmp = thirty_yr_means[str(cube.coord('month_number').points[i])]
	loc = np.searchsorted(meaning_years_tmp, years[i], side="left")
	if loc >= np.size(meaning_years_tmp):
		loc = np.size(meaning_years_tmp) -1
	if meaning_years_tmp[loc] == np.NAN:
		nino34.append(np.NAN)
	else:
		nino34.append(timeseries_value - thirty_yr_means_tmp[loc])
	

nino34 = np.array(nino34)

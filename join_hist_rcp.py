import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid

files = glob.glob('/data/NAS-ph290/shared/spco2/regrid/*.nc')

m=[]
for file in files:
	m.append(file.split('/')[-1].split('_')[0])

unique_models = np.unique(m)

for model in unique_models:
	print model
	e=[]
	files = glob.glob('/data/NAS-ph290/shared/spco2/regrid/'+model+'_*.nc')
	for file in files:
		e.append(file.split('/')[-1].split('_')[3])
	unique_enss = np.unique(e)
	for e in unique_enss:
		print e
		files = glob.glob('/data/NAS-ph290/shared/spco2/regrid/'+model+'_*_'+e+'_*.nc')
		print ' '.join(files)
		sizing = np.size(files)
		if sizing > 1:
			subprocess.call(['cdo -P 8 mergetime '+' '.join(files)+' /data/NAS-ph290/shared/spco2/regrid/joined/'+model+'_spco2_'+e+'_regridded_not_vertically.nc'], shell=True)	
		if sizing == 1:
			subprocess.call(['cp '+files[0]+' /data/NAS-ph290/shared/spco2/regrid/joined/'+model+'_spco2_'+e+'_regridded_not_vertically.nc'], shell=True)	





#script to produce fco2 data assimilation files from model output etc.

import numpy as np
import iris
import glob
from datetime import timedelta, date
import datetime
import subprocess
import uuid
import os.path

#sample of file file aiming for '/data/NAS-geo01/ph290/misc_data/fCO2.SOCAT.20111226.nc'
# or /data/data0/ph290/observations/fCO2.SOCAT.20111226.nc

SOCATv4 = False
EXISTING_NEMO_RUN = True

def create_assimilation_nc_file(latitudes,longitudes,pCO2s,times,output_directory,date,single_date,name):
	#latitudes = -90 to 90
	#longitudes = 0 to 360
	#pCO2s (really ppm????)
	#times seconds from reference time - presumably time specified in filename
	#date e.g. 20111226
	#name e.g. SOCAT
	size = np.size(pCO2s)
	t = ((datetime.datetime(single_date.year, single_date.month, single_date.day, 0, 0)) - datetime.datetime(1981, 01, 01, 0, 0)).total_seconds()
	time = iris.coords.DimCoord([t], standard_name='time',long_name = "reference time of pCO2 file", units='seconds since 1981-01-01 00:00:00')
	#
	cube0 = iris.cube.Cube(np.zeros((1,size), np.float32), long_name='latitude', var_name='lat', units='degrees_north',aux_coords_and_dims=0)
	cube0.data[:] = latitudes
	#
	cube1 = iris.cube.Cube(np.zeros((1,size), np.float32), long_name='longitude', var_name='lon', units='degrees_east',aux_coords_and_dims=0)
# 	longitudes[15] = 266.51111111
	cube1.data[:] = longitudes
	#
	cube2 = iris.cube.Cube(np.zeros((1,size), np.int32), long_name='time_difference from reference time', var_name='fco2_dtime', units='seconds',dim_coords_and_dims=[(time,0)])
	cube2.data[0,:] = times
	#
	cube3 = iris.cube.Cube(np.zeros((1,size), np.float32), long_name='In-situ surface pCO2', var_name='fco2', units='ppm',dim_coords_and_dims=[(time,0)])
	cube3.data[0,:] = pCO2s
	#
	cube4 = iris.cube.Cube(np.zeros((1,size), np.int8), long_name='confidence flag', var_name='confidence_flag', units='1',dim_coords_and_dims=[(time,0)])
	#
	#cubeB = iris.load('/data/NAS-geo01/ph290/misc_data/fCO2.SOCAT.20111226.nc')
	cubeB = iris.load('/data/data0/ph290/observations/fCO2.SOCAT.20111226.nc')
	cube4.attributes = cubeB[0].attributes
	cube0.attributes = cubeB[1].attributes
	cube3.attributes = cubeB[2].attributes
	cube1.attributes = cubeB[3].attributes
	cube2.attributes = cubeB[4].attributes
	#
	cubes = iris.cube.CubeList([cube0,cube1,cube2,cube3,cube4])
	#
	temp_file = str(uuid.uuid4())+'.nc'
	#
	if os.path.isfile(output_directory + 'fCO2.'+name+'.'+date+'.nc'):
		subprocess.call('rm ' + output_directory + 'fCO2.'+name+'.'+date+'.nc', shell=True)
	iris.fileformats.netcdf.save(cubes, '/data/dataSSD1/ph290/tmp/'+temp_file)
	# 	##'Manually' edit the produced netcdf file do the dimension coordinate is labelled 'ni' not 'dim1' etc. when you look at it with ncdump -h. This seems necessary for the model to read it in
	subprocess.call('ncrename -d dim1,ni /data/dataSSD1/ph290/tmp/'+temp_file, shell=True)
	#remove the 'dim0' dimension to make teh format correct for NEMO to read in
	subprocess.call('ncwa -a dim0 /data/dataSSD1/ph290/tmp/'+temp_file+' '+output_directory + 'fCO2.'+name+'.'+date+'.nc', shell=True)
	subprocess.call('rm /data/dataSSD1/ph290/tmp/'+temp_file, shell=True)




#######################################################
# Convert SOCAT data file to assimilation files
#######################################################

if SOCATv4:

	input_directory = '/data/NAS-ph290/ph290/observations/socat/'
	output_directory = '/data/dataSSD0/ph290/'

	f = open(input_directory+'SOCATv4.tsv', 'r')
	contents = []
	for line in f:
		contents.append(line)


	f.closed

	flag = []
	yr = []
	mon = []
	day = []
	hh = []
	mm = []
	ss = []
	lon = []
	lat = []
	fco2 = []

	print 'reading in SOCAT data'
	for cont in contents[4475::]:
		tmp = cont.split('\t')
		flag.append(tmp[2])
		yr.append(tmp[4])
		mon.append(tmp[5])
		day.append(tmp[6])
		hh.append(tmp[7])
		mm.append(tmp[8])
		ss.append(tmp[9])
		lon.append(tmp[10])
		lat.append(tmp[11])
		fco2.append(tmp[23])


	print 'finished reading in SOCAT data'

	def daterange(start_date, end_date):
	    for n in range(int ((end_date - start_date).days)):
	        yield start_date + timedelta(n)


	print 'processing SOCAT data'

	yr = np.array(yr).astype(int)
	mon = np.array(mon).astype(int)
	day = np.array(day).astype(int)
	hh = np.array(hh).astype(float)
	mm = np.array(mm).astype(float)
	ss = np.array(ss).astype(float)
	lon = np.array(lon).astype(float)
	lat = np.array(lat).astype(float)
	fco2 = np.array(fco2).astype(float)

	first_year = np.min(yr)
	first_mon = np.min(mon)
	first_day = np.min(day)

	last_year = np.max(yr)
	last_mon = np.max(mon)
	last_day = np.max(day)

	#start_date = datetime.date(first_year, first_mon, first_day)
	end_date = datetime.date(last_year, last_mon, last_day)

	#DEBUGGING - commend out two lines below for normal use
	start_date = datetime.date(1979, 2, 22)
	end_date = datetime.date(1979, 2, 23)



	print 'writing SOCAT netcdf files'

	for i,single_date in enumerate(daterange(start_date, end_date)):
		date = single_date.strftime("%Y%m%d")
		print date
		yr_tmp = single_date.year
		mon_tmp = single_date.month
		day_tmp = single_date.day
		loc = np.where((yr == yr_tmp) & (mon == mon_tmp) & (day == day_tmp))
		if np.size(loc) > 0:
			create_assimilation_nc_file(lat[loc],lon[loc],fco2[loc],(hh[loc] * 60 * 60) + (mm[loc] * 60) + ss[loc],output_directory,date,single_date,'SOCATv4')
		# else:
			# create_assimilation_nc_file(np.nan,np.nan,np.nan,np.nan,output_directory,date,'SOCATv4')



#######################################################
# Convert data from an existing NEMO run (daily fCO2) into an assimilatoin file based on some sampling template. In the case set up here the sampling template is the obs in each day of a specified year of SOCAT
#######################################################






if EXISTING_NEMO_RUN:


	input_directory = '/data/dataSSD1/ph290/'
	output_directory = '/data/dataSSD0/ph290/nemo_assimilation/'
	template_year_from_SOCAT = 2013
	run_name_or_identifier = 'u-ah673'
	infile = input_directory+'nemo_hadocc_fco2.nc' #note this must be a single file containing only daily fco2 data (first extract with moo filter, then merge with cdo mergetime)
	PROCESS_FILES_FROM_MASS_TO_PRODUCE_INFILE = False

	if PROCESS_FILES_FROM_MASS_TO_PRODUCE_INFILE:
		files = glob.glob(input_directory+'*Z_ocn_hadocc_diad.grid_T.nc')
		files.sort()

		seg_size = 500

		no_segments = np.size(files)/seg_size
		for i in range(no_segments):
			files_tmp = ' '.join(files[seg_size * i : seg_size * (i+1)])
			subprocess.call('cdo mergetime '+files_tmp+' '+input_directory+'a_tmp_'+str(i)+'.nc', shell=True)


		files_tmp = ' '.join(files[seg_size * i+1 ::])
		subprocess.call('cdo mergetime '+files_tmp+' '+input_directory+'a_tmp_'+str(i+1)+'.nc', shell=True)

		files_tmp = ' '.join(glob.glob(input_directory+'a_tmp_*.nc'))
		subprocess.call('cdo mergetime '+files_tmp+' '+input_directory+'nemo_hadocc_fco2.nc', shell=True)

		subprocess.call('rm '+input_directory+'a_tmp_*.nc', shell=True)


	cube = iris.load_cube(infile)

	#using SOCAT as a template
	# note, whilet based on the SOCAT sampling startegy, to avoid problems the socat lats and lons were floor rounded, so teh precise location of sampling  is not exactly right.

	#input_directory2 = '/data/NAS-ph290/ph290/observations/socat/'
	input_directory2 = '/data/data0/ph290/observations/'

	f = open(input_directory2+'SOCATv4.tsv', 'r')
	contents = []
	for line in f:
		contents.append(line)


	f.closed

	flag = []
	yr = []
	mon = []
	day = []
	hh = []
	mm = []
	ss = []
	lon = []
	lat = []
	fco2 = []

	print 'reading in SOCAT data for sampling template'
	for cont in contents[4475::]:
		tmp = cont.split('\t')
		flag.append(tmp[2])
		yr.append(tmp[4])
		mon.append(tmp[5])
		day.append(tmp[6])
		hh.append(tmp[7])
		mm.append(tmp[8])
		ss.append(tmp[9])
		lon.append(tmp[10])
		lat.append(tmp[11])

	print 'processing SOCAT data'

	yr = np.array(yr).astype(int)
	mon = np.array(mon).astype(int)
	day = np.array(day).astype(int)
	hh = np.array(hh).astype(float)
	mm = np.array(mm).astype(float)
	ss = np.array(ss).astype(float)
	lon = np.array(lon).astype(float)
	lat = np.array(lat).astype(float)


	print 'finished reading in and processing SOCAT data for sampling template'

	loc = np.where(yr == template_year_from_SOCAT)
	mon = mon[loc]
	day = day[loc]
	hh = hh[loc]
	mm = mm[loc]
	ss = ss[loc]
	lon = lon[loc]-180
	lat = lat[loc]


	print 'extracting fco2 data from NEMO simulation following SOCAT template'


	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	cube_year = np.array([coord.units.num2date(value).year for value in coord.points])
	cube_mon = np.array([coord.units.num2date(value).month for value in coord.points])
	cube_day = np.array([coord.units.num2date(value).day for value in coord.points])

	print 'processing and producing netcdfs files'

	mdi_out = -32768.0

	for i,day_count in enumerate(cube_day):
		print 'day number: '+str(i)
		tmp_cube = cube[i]
		coord = tmp_cube.coord('time')
		dt = coord.units.num2date(coord.points)
		tmp_cube_year = np.array([coord.units.num2date(value).year for value in coord.points])
		tmp_cube_mon = np.array([coord.units.num2date(value).month for value in coord.points])
		tmp_cube_day = np.array([coord.units.num2date(value).day for value in coord.points])
		tmp_cube_longs = tmp_cube.coord('longitude').points
		tmp_cube_lats = tmp_cube.coord('latitude').points
		index = np.where((mon == tmp_cube_mon) & (day == tmp_cube_day))
		if np.size(index) > 0:
			lon2 = lon[index]
			lat2 = lat[index]
			fco2 = np.zeros(np.size(lon2)).astype(float)
			fco2[:] = np.nan
			for j,dummy in enumerate(lon2):
				loc2 = np.where((tmp_cube_longs >= np.floor(lon2[j])) & (tmp_cube_lats >= np.floor(lat2[j])))
				#note the -180 because one starts from -180, one from zero
				if np.size(loc2) > 0:
					loc2a = loc2[0][0]
					loc2b = loc2[1][0]
					value = tmp_cube.data.data[loc2a,loc2b]
					if ((value > 0.0) & (value < 5000.0)):
						fco2[j] = tmp_cube.data.data[loc2a,loc2b]
					else:
						fco2[j] = mdi_out
			dt2 = datetime.datetime(year=tmp_cube_year, month=tmp_cube_mon, day=tmp_cube_day)
			date = dt2.strftime("%Y%m%d")
			time = 12.0 * (60.0*60.0) # i.e. mid-day
			if np.size(loc) > 0:
				create_assimilation_nc_file(lat2,lon2,fco2,time,output_directory,date,single_date,run_name_or_identifier)



"""


###
#testing - remove
###
#
# def create_assimilation_nc_file(latitudes,longitudes,pCO2s,times,output_directory,date,name):


import numpy as np
import iris
import glob
from datetime import timedelta, date
import datetime
import subprocess



latitudes= np.array([1,2])
longitudes= np.array([3,4])
pCO2s = np.array([300,400])
times = np.array([10,11])

#latitudes = -90 to 90
#longitudes = 0 to 360
#pCO2s (really ppm????)
#times seconds from reference time - presumably time specified in filename
#date e.g. 20111226
#name e.g. SOCAT
size = np.size(pCO2s)
time = iris.coords.DimCoord(range(1), standard_name='time',long_name = "reference time of pCO2 file", units='seconds since 1981-01-01 00:00:00')

cube0 = iris.cube.Cube(np.zeros((1,size), np.float32), long_name='latitude', var_name='lat', units='degrees_north',aux_coords_and_dims=0)
cube0.data[:] = latitudes

cube1 = iris.cube.Cube(np.zeros((1,size), np.float32), long_name='longitude', var_name='lon', units='degrees_east',aux_coords_and_dims=0)
cube1.data[:] = latitudes

cube2 = iris.cube.Cube(np.zeros((1,size), np.int32), long_name='time_difference from reference time', var_name='fco2_dtime', units='seconds',dim_coords_and_dims=[(time,0)])
cube2.data[0,:] = times

cube3 = iris.cube.Cube(np.zeros((1,size), np.float32), long_name='In-situ surface pCO2', var_name='fco2', units='ppm',dim_coords_and_dims=[(time,0)])
cube3.data[0,:] = pCO2s

cube4 = iris.cube.Cube(np.zeros((1,size), np.int8), long_name='confidence flag', var_name='confidence_flag', units='1',dim_coords_and_dims=[(time,0)])

cubeB = iris.load('/data/NAS-geo01/ph290/misc_data/fCO2.SOCAT.20111226.nc')
cube4.attributes = cubeB[0].attributes
cube0.attributes = cubeB[1].attributes
cube3.attributes = cubeB[2].attributes
cube1.attributes = cubeB[3].attributes
cube2.attributes = cubeB[4].attributes

cubes = iris.cube.CubeList([cube0,cube1,cube2,cube3,cube4])
iris.fileformats.netcdf.save(cubes, '/home/ph290/Downloads/tmp.nc')
# 	##'Manually' edit the produced netcdf file do the dimension coordinate is labelled 'ni' not 'dim1' when you look at it with ncdump -h. This seems necessary for the model to read it in
subprocess.call('ncrename -d dim1,ni /home/ph290/Downloads/tmp.nc', shell=True)
subprocess.call('ncwa -a dim0 /home/ph290/Downloads/tmp.nc /home/ph290/Downloads/test_fCO2.nc', shell=True)

"""

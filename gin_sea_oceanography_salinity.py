"""

import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec

#data from https://www.nodc.noaa.gov/cgi-bin/OC5/gin-seas-climate/ginregcl.pl
annual_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
winter_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
spring_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
summer_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
autumn_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]

summer_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
winter_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
ann_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]

# cube =  winter_cube
# cube =  spring_cube
# cube =  summer_cube
# cube =  autumn_cube
cube =  annual_cube

density_cube = cube.copy()
density_cube.standard_name = 'sea_water_density'
density_cube.units = 'kg m-3'

density_cube.data = seawater.dens(ann_sal_cube.data,cube.data,1)

# lon_west = -35.0
# lon_east = 15.0
# lat_south = 60
# lat_north = 85.0
#
# cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
# cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
#
# cube_region_tmp = ann_sal_cube.intersection(longitude=(lon_west, lon_east))
# ann_sal_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

"""

"""

depth0 = 100.0
depth1 = 200.0

loc0 = np.where(cube.coord('depth').points > depth0)[0][0]

loc1 = np.where(cube.coord('depth').points > depth1)[0][0]

minv=-2.5
maxv=2.5
minv1=-1.0
maxv1=5.0

plt.close('all')
plt.figure(figsize = (12,12))

ax1 = plt.subplot2grid((2,2),(0,0))
qplt.pcolormesh(cube[loc0],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc0],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth0)+'m')

ax2 = plt.subplot2grid((2,2),(0,1))
qplt.pcolormesh(cube[loc1],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth1)+'m')

ax2 = plt.subplot2grid((2,2),(1,0))
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
qplt.pcolormesh(cube[loc1] - cube[loc0],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('T at '+str(depth1)+'m minus '+str(depth0)+'m')

ax2 = plt.subplot2grid((2,2),(1,1))
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
qplt.pcolormesh(cube[loc1] - cube[loc0],cmap = 'bwr',vmin = minv,vmax=maxv)
iplt.pcolormesh(winter_cube[loc0],alpha=0.9,cmap = 'gray',vmin=0.0,vmax=10000.0)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth1)+'m minus '+str(depth0)+'m\nlocation of winter obs in black to indicate ice edge')

plt.savefig('/home/ph290/Documents/figures/GIN_seas_surface_bottom_T_100_300.png')
plt.show(block = True)

"""
"""
#####
# temp figure 2
#####

my_cmap = 'bwr'

depth0 = 10.0
depth1 = 50.0
depth2 = 200.0
depth3 = 1000.0

loc0 = np.where(cube.coord('depth').points > depth0)[0][0]
loc1 = np.where(cube.coord('depth').points > depth1)[0][0]
loc2 = np.where(cube.coord('depth').points > depth2)[0][0]
loc3 = np.where(cube.coord('depth').points > depth3)[0][0]

minv=-3.0
maxv=3.0
minv1=-3.0
maxv1=3.0

plt.close('all')
plt.figure(figsize = (9,15))

gs1 = gridspec.GridSpec(5, 2)

### levels ###

ax1 = plt.subplot(gs1[0,0])
iplt.pcolormesh(cube[loc0],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc0],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth0)+'m')

ax2 = plt.subplot(gs1[1,0])
iplt.pcolormesh(cube[loc1],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth1)+'m')


ax2 = plt.subplot(gs1[2,0])
iplt.pcolormesh(cube[loc2],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth2)+'m')


ax2 = plt.subplot(gs1[3,0])
iplt.pcolormesh(cube[loc3],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth3)+'m')

### diff ###

ax2 = plt.subplot(gs1[1,1])
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
iplt.pcolormesh(cube[loc1] - cube[loc0],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('T at '+str(depth1)+'m minus '+str(depth0)+'m')

ax2 = plt.subplot(gs1[2,1])
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
iplt.pcolormesh(cube[loc2] - cube[loc1],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('T at '+str(depth2)+'m minus '+str(depth1)+'m')

ax2 = plt.subplot(gs1[3,1])
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
c = iplt.pcolormesh(cube[loc3] - cube[loc2],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('T at '+str(depth3)+'m minus '+str(depth2)+'m')

gs2 = gridspec.GridSpec(5, 1)
gs2.update(bottom=0.2, top=0.3)
# gs2.update(bottom=0.2, top=0.3, wspace=0.05)
cax = plt.subplot(gs2[4,0])
plt.colorbar(c, cax=cax, orientation='horizontal')

plt.savefig('/home/ph290/Documents/figures/GIN_seas_surface_bottom_T_4_depths.png')
plt.show(block = True)

#########
#########
### S ###
#########
#########


minv=-2.0
maxv=2.0
minv1=32
maxv1=37

cube = ann_sal_cube

my_cmap = 'winter'

plt.close('all')
plt.figure(figsize = (9,15))

gs1 = gridspec.GridSpec(5, 2)

### levels ###

ax1 = plt.subplot(gs1[0,0])
iplt.pcolormesh(cube[loc0],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc0],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('S at '+str(depth0)+'m')

ax2 = plt.subplot(gs1[1,0])
iplt.pcolormesh(cube[loc1],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('S at '+str(depth1)+'m')


ax2 = plt.subplot(gs1[2,0])
iplt.pcolormesh(cube[loc2],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('S at '+str(depth2)+'m')


ax2 = plt.subplot(gs1[3,0])
c1 = iplt.pcolormesh(cube[loc3],cmap = my_cmap,vmin = minv1,vmax=maxv1)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('S at '+str(depth3)+'m')

### diff ###

ax2 = plt.subplot(gs1[1,1])
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
iplt.pcolormesh(cube[loc1] - cube[loc0],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('S at '+str(depth1)+'m minus '+str(depth0)+'m')

ax2 = plt.subplot(gs1[2,1])
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
iplt.pcolormesh(cube[loc2] - cube[loc1],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('S at '+str(depth2)+'m minus '+str(depth1)+'m')

ax2 = plt.subplot(gs1[3,1])
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
c2 = iplt.pcolormesh(cube[loc3] - cube[loc2],cmap = 'bwr',vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('S at '+str(depth3)+'m minus '+str(depth2)+'m')

gs2 = gridspec.GridSpec(5, 2)
gs2.update(bottom=0.2, top=0.3)
# gs2.update(bottom=0.2, top=0.3, wspace=0.05)
cax = plt.subplot(gs2[4,0])
plt.colorbar(c1, cax=cax, orientation='horizontal')

# gs2.update(bottom=0.2, top=0.3, wspace=0.05)
cax = plt.subplot(gs2[4,1])
plt.colorbar(c2, cax=cax, orientation='horizontal')

plt.savefig('/home/ph290/Documents/figures/GIN_seas_surface_bottom_S_4_depths.png')
plt.show(block = True)
"""

"""

loc3 = np.where(cube.coord('latitude').points > 72)[0][0]
plt.close('all')

qplt.pcolormesh(cube[:,loc3,],cmap = 'jet',vmin = -2.0,vmax=+2.0)
CS=qplt.contour(ann_sal_cube[:,loc3,:],np.linspace(34.0,36.0,10),alpha=0.8,colors='k',lw=0.5)
plt.clabel(CS, inline=1, fontsize=10)
plt.show(block = True)




cube =  summer_cube
longitudes = np.linspace(-25,-5,50)

density_cube = cube.copy()
density_cube.standard_name = 'sea_water_density'
density_cube.units = 'kg m-3'

density_cube.data = seawater.dens(summer_sal_cube.data,cube.data,1)

for i,lon_var in enumerate(longitudes):

    loc4 = np.where(cube.coord('longitude').points > lon_var)[0][0]
    plt.close('all')
    iplt.pcolormesh(cube[:,:,loc4],cmap = 'bwr',vmin = -3.0,vmax=3.0)
    CS=iplt.contour(summer_sal_cube[:,:,loc4],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
    # plt.clabel(CS, inline=1, fontsize=10)
    # qplt.pcolormesh(density_cube[:,:,loc4],cmap = my_cmap)
    CS=iplt.contour(density_cube[:,:,loc4],40,alpha=0.8,colors='k',linewidths=0.5)
    # iplt.pcolormesh(winter_cube[:,:,loc4],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
    iplt.title = str(lon_var)
    plt.ylim(2000,0)
    plt.xlim(65,85)

    plt.savefig('/home/ph290/Documents/figures/summer_GIN_seas_lon_sections_'+str(lon_var)+'.png')
    # plt.show(block = True)




latitudes = np.linspace(65,75,50)

for i,lat_var in enumerate(latitudes):

    loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
    plt.close('all')
    iplt.pcolormesh(cube[:,loc5,:],cmap = 'bwr',vmin = -3.0,vmax=3.0)
    CS=iplt.contour(summer_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
    # plt.clabel(CS, inline=1, fontsize=10)
    # qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
    CS=iplt.contour(density_cube[:,loc5,:],20,alpha=0.8,colors='k',linewidths=0.5)
    # iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
    iplt.title = str(lon_var)
    plt.ylim(2000,0)
    # plt.xlim(65,85)

    plt.savefig('/home/ph290/Documents/figures/summer_GIN_seas_lat_sections_'+str(lat_var)+'.png')
    # plt.show(block = True)




#


# cube =  annual_cube
longitudes = np.linspace(-25,-5,50)

for i,lon_var in enumerate(longitudes):

    loc4 = np.where(cube.coord('longitude').points > lon_var)[0][0]
    plt.close('all')
    iplt.pcolormesh(cube[:,:,loc4],cmap = 'bwr',vmin = -3.0,vmax=3.0)
    CS=iplt.contour(ann_sal_cube[:,:,loc4],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
    # plt.clabel(CS, inline=1, fontsize=10)
    # qplt.pcolormesh(density_cube[:,:,loc4],cmap = my_cmap)
    CS=iplt.contour(density_cube[:,:,loc4],40,alpha=0.8,colors='k',linewidths=0.5)
    # iplt.pcolormesh(winter_cube[:,:,loc4],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
    iplt.title = str(lon_var)
    plt.ylim(2000,0)
    plt.xlim(65,85)

    plt.savefig('/home/ph290/Documents/figures/GIN_seas_lon_sections_'+str(lon_var)+'.png')
    # plt.show(block = True)


"""
"""

latitudes = np.linspace(65,80,50)

for i,lat_var in enumerate(latitudes):

    loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
    plt.close('all')
    cs = iplt.pcolormesh(cube[:,loc5,:],cmap = 'gist_ncar',vmin = -5,vmax=5)
    # CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
    plt.colorbar(cs)
    # plt.clabel(CS, inline=1, fontsize=10)
    # qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
    iplt.contour(ann_sal_cube[:,loc5,:],20,alpha=0.8,colors='k',linewidths=0.5)
    # iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
    iplt.title = str(lat_var)
    plt.ylim(1000,0)
    # plt.xlim(65,85)

    plt.savefig('/home/ph290/Documents/figures/GIN_seas_lat_sections_'+str(lat_var)+'_temperature.png')
    # plt.show(block = True)

"""

lat_var = 74.79
loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
plt.close('all')
plt.figure(figsize = (12,12))

cs = iplt.pcolormesh(cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
# CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
# CB=plt.colorbar(cs)
CB = plt.colorbar(cs, shrink=0.99, extend='both')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs2=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
plt.clabel(cs2,[34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels
# plt.clabel(cs2,inline=True, fontsize=10)

# plt.colorbar(cs2)

# iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
iplt.title = str(lat_var)
plt.ylim(1000,0)
plt.xlim(-25,15)
# plt.xlim(65,85)

plt.savefig('/home/ph290/Documents/figures/GIN_seas_lat_sections_'+str(lat_var)+'_temperature.png')
plt.show(block = True)

###

lat_var = 74.79
loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
plt.close('all')
plt.figure(figsize = (12,5))

cs = iplt.pcolormesh(cube[:,loc5,:],cmap = 'bwr',vmin = -3,vmax=3)
# CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
# CB=plt.colorbar(cs)
CB = plt.colorbar(cs, shrink=0.99, extend='both')

# plt.clabel(CS, inline=1, fontsize=10)
# qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
cs2=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
plt.clabel(cs2,[34.5,34.75,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels
# plt.clabel(cs2,inline=True, fontsize=10)

# plt.colorbar(cs2)

# iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
iplt.title = str(lat_var)
plt.ylim(300,0)
plt.xlim(-20,0)
# plt.xlim(65,85)

plt.savefig('/home/ph290/Documents/figures/GIN_seas_lat_sections_'+str(lat_var)+'_temperature_closeup.png')
plt.show(block = False)

"""
cube = ann_sal_cube

latitudes = np.linspace(65,80,50)

for i,lat_var in enumerate(latitudes):

    loc5 = np.where(cube.coord('latitude').points > lat_var)[0][0]
    plt.close('all')
    iplt.pcolormesh(cube[:,loc5,:],cmap = 'gist_ncar',vmin = 30,vmax=35)
    # CS=iplt.contour(ann_sal_cube[:,loc5,:],np.linspace(34.0,36.0,20),alpha=0.8,colors='c',linewidths=0.5)
    # plt.clabel(CS, inline=1, fontsize=10)
    # qplt.pcolormesh(density_cube[:,loc5,:],cmap = my_cmap)
    CS=iplt.contour(density_cube[:,loc5,:],20,alpha=0.8,colors='k',linewidths=0.5)
    # iplt.pcolormesh(winter_cube[:,loc5,:],alpha=0.5,cmap = 'gray',vmin=0.0,vmax=10000.0)
    iplt.title = str(lat_var)
    plt.ylim(1000,0)
    # plt.xlim(65,85)

    plt.savefig('/home/ph290/Documents/figures/GIN_seas_lat_sections_'+str(lat_var)+'_salinity.png')
    # plt.show(block = True)




###
#salinity
###



cube = ann_sal_cube

depth0 = 1.0
depth1 = 100.0

loc0 = np.where(cube.coord('depth').points > depth0)[0][0]

loc1 = np.where(cube.coord('depth').points > depth1)[0][0]

minv=-5.0
maxv=5.0

plt.close('all')
plt.figure(figsize = (12,12))

ax1 = plt.subplot2grid((2,2),(0,0))
qplt.pcolormesh(cube[loc0],cmap = my_cmap,vmin=30.0,vmax=35.0)
# qplt.contourf(cube[loc0],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth0)+'m')



plt.show(block = False)



#salinity surface/subsurface maps

cube =  ann_sal_cube

depth0 = 10.0
depth1 = 100.0

loc0 = np.where(cube.coord('depth').points > depth0)[0][0]

loc1 = np.where(cube.coord('depth').points > depth1)[0][0]

minv=0.0
maxv=2.0

plt.close('all')
plt.figure(figsize = (12,12))

ax1 = plt.subplot2grid((2,2),(0,0))
qplt.pcolormesh(cube[loc0],cmap = my_cmap,vmin = 33,vmax=35)
# qplt.contourf(cube[loc0],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('S at '+str(depth0)+'m')

ax2 = plt.subplot2grid((2,2),(0,1))
qplt.pcolormesh(cube[loc1],cmap = my_cmap,vmin = 33,vmax=35)
# qplt.contourf(cube[loc1],np.linspace(-2,15,50),cmap = my_cmap)
plt.gca().coastlines('10m')
plt.title('S at '+str(depth1)+'m')

ax2 = plt.subplot2grid((2,2),(1,0))
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
qplt.pcolormesh(cube[loc1] - cube[loc0],cmap = my_cmap,vmin = minv,vmax=maxv)
# qplt.contour(ann_sal_cube[0],np.linspace(30.0,35.0,10),colors='k')
plt.gca().coastlines('10m')
plt.title('S at '+str(depth1)+'m minus '+str(depth0)+'m')

ax2 = plt.subplot2grid((2,2),(1,1))
# qplt.contourf(cube[loc1] - cube[loc0],np.linspace(minv,maxv,50),cmap = 'bwr')
qplt.pcolormesh(cube[loc1] - cube[loc0],cmap = my_cmap,vmin = minv,vmax=maxv)
iplt.pcolormesh(winter_cube[loc0],alpha=0.9,cmap = 'gray',vmin=0.0,vmax=10000.0)
plt.gca().coastlines('10m')
plt.title('T at '+str(depth1)+'m minus '+str(depth0)+'m\nlocation of winter obs in black to indicate ice edge')

plt.savefig('/home/ph290/Documents/figures/GIN_seas_surface_bottom_S.png')
plt.show(block = False)

"""

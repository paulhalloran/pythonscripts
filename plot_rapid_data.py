import matplotlib.pyplot as plt
import iris
import numpy as numpy
import iris.quickplot as qplt

file = '/home/ph290/data1/observations/moc_transports.nc'

data = iris.load(file)

qplt.plot(data[8])
#plt.show()
plt.savefig('/home/ph290/Documents/figures/amoc.svg')

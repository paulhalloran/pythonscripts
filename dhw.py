
import iris
import iris.coord_categorisation
import numpy as np

#file to read in (at present the script assumes that this contains the historical and future data, so you can specify the period from which to claculate the MMM climatoloty, and over which you want DHW calculated)
input_file = '/data/BatCaveNAS/ph290/hadgem2-es_sst/merged_tos.nc'
#name of teh output file containing DHW data
output_file_name = 'dhw_file.nc'

#as the variable names suggest. NOTE the fisrt DHW field will 84 days after the start of the year you specify here because it need to sum the anomaly values from the previous 84 days
years_for_mmm_climatology = [2005,2010]
years_over_which_to_calculate_dhw = [2020,2030]

#read in data and add metadata about the year
input_cube = iris.load_cube(input_file)
iris.coord_categorisation.add_year(input_cube, 'time', name='year')
cube_years = input_cube.coord('year').points

#subset the data into the bit you want to use to calculate the MMM climatology and the bit you want to calculate DHW on
clim_cube = input_cube[np.where((cube_years > years_for_mmm_climatology[0]) & (cube_years < years_for_mmm_climatology[1]))]
main_cube = input_cube[np.where((cube_years > years_over_which_to_calculate_dhw[0]) & (cube_years < years_over_which_to_calculate_dhw[1]))]


#################################
# Maximum Monthly Mean (MMM) climatology
#################################


#convert daily data to monthly meaned data
iris.coord_categorisation.add_month_number(clim_cube, 'time', name='month_number')
clim_cube = clim_cube.aggregated_by('month_number', iris.analysis.MEAN)

#collapse the months together, taking the maximum value at each lat-lon grid square
mmm_climatology = clim_cube.collapsed('time',iris.analysis.MAX)


#################################
# Degree Heating Weeks
#starting from day 84 (12 weeks in), do a running sum of the previous 84 days and divide by 7 to get DHWeeks
#################################

#subtract the monthly mean climatology from the rest of the data
main_cube -= mmm_climatology

#set all values less than 1 to zero
main_cube.data[np.where(main_cube.data <= 1.0)] = 0.0

#make a cube to hold the output data
output_cube = main_cube[84::].copy()
output_cube.data[:] = np.nan
output_cube_data = output_cube.data.copy()

#loop through from day 84 to the end of the dataset
for i in range(output_cube.shape[0]):
    print i,' of ',output_cube.shape[0]
    #sum the temperatures in that 84 day window and divide result by 7 to get in DHWeeks rather than DHdays
    tmp_data = main_cube[i:i+84].collapsed('time',iris.analysis.SUM)/7.0
    output_cube_data[i,:,:] = tmp_data.data

#save the output
output_cube.data = output_cube_data
iris.fileformats.netcdf.save(output_cube,output_file_name)

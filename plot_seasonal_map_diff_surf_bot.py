######################################################
# edit lines here to specify file name and plot type #
######################################################

bott = '/data/NAS-geo01/jm953/GBRPPS_bathy_003d_01r/bottom_temperature.nc'
surf = '/data/NAS-geo01/jm953/GBRPPS_bathy_003d_01r/surface_temperature.nc'
depth_plot = '/data/NAS-geo01/jm953/GBRPPS_bathy_003d_01r/depth.nc'
#two options are provided as examples for analysis here (plotting the time meaned map or plotting a specific day of the run)
#but see http://scitools.org.uk/iris/docs/v1.9.0/html/examples/index.html and http://scitools.org.uk/iris/docs/v1.9.0/html/gallery.html for more examples
plot_name = 'my_plot'
#to plot the time-averaged map (set to False if you want to plot a specific day)
time_average = True

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import numpy as np
import iris.analysis.maths
import iris.coord_categorisation
import iris.quickplot as qplt

#read in file
cube =iris.load_cube(bott)
iris.coord_categorisation.add_season(cube, 'time', name='seasons', seasons=('djf', 'mam', 'jja', 'son'))
if time_average:
    #iris.coord_categorisation.add_season(cube, 'time', name='seasons', seasons=('djf', 'mam', 'jja', 'son'))
    bott_cube = cube.collapsed('seasons',iris.analysis.MEAN)
else:
    bott_cube = cube[seasons['jja']]

cube =iris.load_cube(surf)
if time_average:
    surf_cube = cube.collapsed('time',iris.analysis.MEAN)
else:
    surf_cube = cube[seasons['jja']]

diff = iris.analysis.maths.subtract(surf_cube,bott_cube, dim=None, in_place=True)

depth = iris.load_cube(depth_plot)
#the depth file has teh depth for each day, but obviously that is meaningless, so we can just take the 1st (0th) item in teh list. i.e. the 1st day, and ignore the rest:
depth = depth[0]

#define colour palette and colour levels
blevels = MaxNLocator(nbins=50).tick_values(np.nanmin(bott_cube.data), np.nanmax(bott_cube.data))
slevels = MaxNLocator(nbins=50).tick_values(np.nanmin(surf_cube.data), np.nanmax(surf_cube.data))
cmap = plt.get_cmap('Reds_r')
norm = BoundaryNorm(blevels, ncolors=cmap.N, clip=True)
norm = BoundaryNorm(slevels, ncolors=cmap.N, clip=True)

import cartopy.crs as ccrs
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
import matplotlib.pyplot as plt
import iris.plot as iplt
import cartopy.feature as cfeature


fig = plt.figure(figsize=(8, 10))

# Label axes of a Plate Carree projection with a central longitude of 180:
ax1 = fig.add_subplot(1, 1, 1,
                      projection=ccrs.PlateCarree(central_longitude=180))
plot_info = iplt.pcolormesh(diff,cmap=cmap, norm=norm) #plots color map with iris
plt.colorbar(plot_info, orientation='horizontal', label='Temperature $^\circ$C',format='%1.1f')
con_depth = iplt.contour(depth,np.linspace(0,150,6),colors='k')
plt.clabel(con_depth, fmt='%1.1f', fontsize=10)
ax1.coastlines()
ax1.set_xticks([151.8, 152, 152.2, 152.4, 152.6], crs=ccrs.PlateCarree())
ax1.set_yticks([-23.2, -23.4, -23.6, -23.8], crs=ccrs.PlateCarree())
lon_formatter = LongitudeFormatter(zero_direction_label=True)
lat_formatter = LatitudeFormatter()
ax1.xaxis.set_major_formatter(lon_formatter)
ax1.yaxis.set_major_formatter(lat_formatter)
plt.title("Surface to Bottom Temperature Differences, Winter Heron Island")

coasts_10m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',
                                        edgecolor='k',facecolor='none')

# coasts_high = cfeature.GSHHSFeature(scale='full', levels=None)

#land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                        #edgecolor='face',
                                        #facecolor=(0.85, 0.85, 0.85))

#minorisland_10m = cfeature.NaturalEarthFeature('physical', 'minor_islands', '10m',
                                        #edgecolor='face',
                                        #facecolor=(0.85, 0.85, 0.85))

#reefs_10m = cfeature.NaturalEarthFeature('physical', 'reefs', '10m',
                                        #edgecolor='face',
                                        #facecolor=(0.85, 0.85, 0.85))
#ax1.add_feature(land_10m)
#ax1.add_feature(coasts_10m)
#ax1.add_feature(minorisland_10m)
#ax1.add_feature(reefs_10m)

plt.show()

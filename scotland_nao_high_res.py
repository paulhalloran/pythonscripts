

import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import glob
import subprocess
import datetime
import iris.coord_categorisation
import pandas as pd
import scipy
from scipy import signal


def return_datetime_from_cube(cube):
    #Add year month and day number for each record in the cube
    try:
        iris.coord_categorisation.add_year(cube, 'time', name='year')
    except:
        print 'already have'
    try:
        iris.coord_categorisation.add_month_number(cube, 'time', name='month')
    except:
        print 'already have'
    try:
        iris.coord_categorisation.add_day_of_month(cube, 'time', name='day')
    except:
        print 'already have'
    #Get the dates in 'datetime' format so it will be easy to plot against observations (see https://pymotw.com/2/datetime/)
    dates=[]
    for i in range(np.shape(cube)[0]):
        try:
            dates.append(datetime.datetime(cube.coord('year').points[i], cube.coord('month').points[i], cube.coord('day').points[i]))
        except:
            dates.append(datetime.datetime(cube.coord('year').points[i], 1, 1))
    return dates


def detrend_cube(cube):
    cube2 = cube.collapsed('time',iris.analysis.MEAN)
    cube_data = cube.data
    cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    cube.data = cube_data_detrended
    return cube+cube2


#The lat/lon I'm interested in converted from degrees and minutes to devimal degrees, and degrees W to negative degrees E
#Scotland
lat = 56.0+(37.75/60.0)
lon = -1.0 * (6.0+(24.00/60.0))
#iceland
# lat = 66.0+(31.59/60.0)
# lon = -1.0 * (18.0+(11.74/60.0))


spinup_years = 3
file1 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/surface_temperature_tiree_0point02.nc')
file2 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/bottom_temperature_tiree_0point02.nc')
ts_cube = iris.load_cube(file1)[spinup_years::]
tb_cube = iris.load_cube(file2)[spinup_years::]

try:
    iris.coord_categorisation.add_season(ts_cube, 'time', name='season_name')
except:
    print 'already got'



try:
    iris.coord_categorisation.add_season(tb_cube, 'time', name='season_name')
except:
    print 'already got'


try:
    iris.coord_categorisation.add_month(ts_cube, 'time', name='month_name')
except:
    print 'already got'



try:
    iris.coord_categorisation.add_month(tb_cube, 'time', name='month_name')
except:
    print 'already got'


# ts_cube = ts_cube.extract(iris.Constraint(season_name = 'jja'))
# tb_cube = tb_cube.extract(iris.Constraint(season_name = 'jja'))
ts_cube = ts_cube.extract(iris.Constraint(month_name = 'May'))
tb_cube = tb_cube.extract(iris.Constraint(month_name = 'May'))


try:
    iris.coord_categorisation.add_year(ts_cube, 'time', name='year')
except:
    print 'already got'


ts_cube = ts_cube.aggregated_by('year', iris.analysis.MEAN)



try:
    iris.coord_categorisation.add_year(tb_cube, 'time', name='year')
except:
    print 'already got'


tb_cube = tb_cube.aggregated_by('year', iris.analysis.MEAN)



tb_cube = detrend_cube(tb_cube)
ts_cube = detrend_cube(ts_cube)


dates = return_datetime_from_cube(ts_cube)
dates = return_datetime_from_cube(tb_cube)

years = [tmp.year for tmp in dates]

direct = '/data/NAS-ph290/ph290/observations/'
nao_file = direct+'nao_station_djfm.txt'
# nao_file = direct+'nao_station_annual.txt'
nao = pd.read_csv(nao_file,delim_whitespace=True,names=['year','value'],skiprows=[0])
nao_date = pd.DataFrame(data=nao.year.values,columns=['year'])
nao_date['month'] = (nao.year*0)+1
nao_date['day'] = (nao.year*0)+1
nao.date = pd.to_datetime(nao_date)

positive_years = nao_date.year.values[np.where(nao.value.values > 2.0)]
negative_years = nao_date.year.values[np.where(nao.value.values < -2.0)]

positive_years_common = set(positive_years) & set(years)
negative_years_common = set(negative_years) & set(years)

positive_index = np.in1d(years,list(positive_years_common))
negative_index = np.in1d(years,list(negative_years_common))



###############
# plotting    #
###############

# diff_cube = ts_cube - tb_cube
diff_cube = tb_cube
diff_cube_positive = diff_cube[positive_index].collapsed('time',iris.analysis.MEAN)
diff_cube_negative = diff_cube[negative_index].collapsed('time',iris.analysis.MEAN)
cube = diff_cube_positive - diff_cube_negative

import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator

levels = MaxNLocator(nbins=50).tick_values(-0.5,0.5)
cmap = plt.get_cmap('seismic')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                        edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])

plt.close('all')
fig = plt.figure(figsize=(8, 8), dpi=100)
# ax = plt.axes(projection=ccrs.PlateCarree())
ax = plt.subplot2grid((4, 1), (0, 0),projection=ccrs.PlateCarree())

lons, lats, data = cube.coord('latitude').points,cube.coord('longitude').points,cube.data

#pannel 1
img1 = ax.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax.add_feature(land_10m, edgecolor='gray')
cbar.set_label('+ve minus -ve NAO\nbottom temp diff')

#pannel 2
diff_cube = ts_cube
diff_cube_positive = diff_cube[positive_index].collapsed('time',iris.analysis.MEAN)
diff_cube_negative = diff_cube[negative_index].collapsed('time',iris.analysis.MEAN)
cube = diff_cube_positive - diff_cube_negative
data = cube.data

ax = plt.subplot2grid((4, 1), (1, 0),projection=ccrs.PlateCarree())

img1 = ax.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax.add_feature(land_10m, edgecolor='gray')
cbar.set_label('+ve minus -ve NAO\nsurface temp diff')

#pannel 3
cmap = plt.get_cmap('hot')

levels = MaxNLocator(nbins=50).tick_values(0.0,7.0)
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)


data = (ts_cube.collapsed('time',iris.analysis.MEAN) -  tb_cube.collapsed('time',iris.analysis.MEAN)).data
ax2 = plt.subplot2grid((4, 1), (2, 0),projection=ccrs.PlateCarree())
img1 = ax2.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax2.add_feature(land_10m, edgecolor='gray')
cbar.set_label('mean surface bottom T\ndiff. black=unstratified')

#pannel 3

levels = MaxNLocator(nbins=50).tick_values(5.0,20.0)
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

data = diff_cube_positive.data
data = (tb_cube.collapsed('time',iris.analysis.MEAN)).data
ax3 = plt.subplot2grid((4, 1), (3, 0),projection=ccrs.PlateCarree())
img1 = ax3.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax3.add_feature(land_10m, edgecolor='gray')
cbar.set_label('mean bottom water\ntemperature')

plt.savefig('/home/ph290/Documents/figures/tiree_nao_impacts.png')
plt.show(block = False)

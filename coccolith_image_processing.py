#
#example from http://www.pyimagesearch.com/2014/09/22/getting-started-deep-learning-python/
# import the necessary packages

#note - convert tif images to pngs for the script as set up:
#for f in *.tif; do  echo "Converting $f"; convert "$f"  "$(basename "$f" .png).jpg"; done

from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report
from sklearn import datasets
from nolearn.dbn import DBN
import numpy as np
import cv2
import glob
import time
from pylab import uint8
import matplotlib.pyplot as plt
import os

directory = '/home/ph290/Downloads/pleurochrysis_placolithoides/'
output_directory = '/home/ph290/Documents/python_scripts/tensorflow/tensorflow-image-classifier/training_dataset/pleurochrysis_placolithoides/'

species = glob.glob(directory+'/*')

names = []
for spec in species:
	 names.append(spec.split('/')[-1])


names = np.array(names)

dataset = []
target = []

x_resolution = 1000
y_resolution = 1000

# reshaped_size = np.int(np.round(y_resolution * (1.0 - (1.0/3.8))))

print 'reading in images...'

margin = 0.2

for i,species_directory in enumerate(species):
	try:
		os.mkdir(output_directory+species_directory.split('/')[-1])
	except:
		print 'directory already exists'
	images = glob.glob(species_directory+'/*.png')
	print 'reading in  '+species_directory
	for j,image in enumerate(images):
		print j
		try:
			img = cv2.imread(image,0)
			shape = np.shape(img)
			#remove writing form image
			#img = img[shape[1] - np.int(np.round(shape[1] * (1.0 - (1.0/3.8)))):shape[1],0:shape[0]]
			#crop around image
			maxIntensity = 255.0 # depends on type of image data
			#maxIntensity = np.max(img)
			x = np.arange(maxIntensity)
			# Parameters for manipulating image data
			phi = 1.0
			theta = 1.0
			# Increase intensity such that
			# dark pixels become much brighter,
			# bright pixels become slightly bright
			img = (maxIntensity/phi)*(img/(maxIntensity/theta))**0.5
# 			img = cv2.equalizeHist(img)
			img = np.array(img,dtype=uint8)
			_,thresh = cv2.threshold(img.copy(),100,255,cv2.THRESH_BINARY)
			contours,hierarchy = cv2.findContours(thresh.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
			no_lines = np.shape(contours)[0]
			cnt = contours[0]
			x,y,w,h = cv2.boundingRect(cnt)
			max_square_side = np.max([w,h])
			x1 = np.int(np.round(x - max_square_side * margin))
			x2 = np.int(np.round(x + max_square_side * (1.0 + margin)))
			y1 = np.int(np.round(y - max_square_side * margin))
			y2 = np.int(np.round(y + max_square_side * (1.0 + margin)))
			if not ((y1 < 0) or (x1 < 0) or (y2 > np.shape(img)[0]) or (x2 > np.shape(img)[1])):
				crop = img[y1:y2,x1:x2]
				# plt.imshow(crop)
				# plt.show()
				# plt.close('all')
				# plt.figure(1)
				# plt.contourf(img,21)
				# plt.plot([x1,x2,x2,x1,x1],[y1,y1,y2,y2,y1],'r')
				# plt.show(block = False)
				# plt.figure(2)
				# plt.contourf(crop,21)
				# plt.show(block = False)
				# remove noise form the cropped image
				#crop = cv2.fastNlMeansDenoising(crop,None,10,7,21)
				# plt.figure(3)
				# plt.contourf(crop,21)
				# plt.show(block = True)
				img = cv2.resize(crop, (y_resolution, x_resolution))
				dataset.append(img) #note ravel turns 2D to 1D array
				target.append(i+1)
				cv2.imwrite(output_directory+species_directory.split('/')[-1]+'/im_'+str(j)+'_cropped.png',img) 
		except:
			print 'image too close to edge'

# 
# print 'disaplying images...'
# for data in dataset[1000:1003]:
# 	image = (data * 255).reshape((y_resolution, x_resolution)).astype("uint8")
# 	image = cv2.resize(image, (500,500))
#     #would need to change this to match the image dimensions...
# 	# show the image and prediction
# 	plt.contourf(image,150)
# 	plt.show()


dataset = np.array(dataset)
target = np.array(target)


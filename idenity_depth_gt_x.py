import iris
import numpy as np
import matplotlib.pyplot as plt
import unicodedata


#directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
#file = 'HadCM3_thetao_past1000_r1i1p1_regridded_not_vertically_Omon.nc'

directory = '/home/ph290/Downloads/'
file = 'arag_cube.nc'
critical_value = 1.0 # i.e. identify the depth for which values move from above to below this value

#load the cube in from which you want to calculate the depth data
cube = iris.load_cube(directory + file)



#a bit more complicated so that it can work with cubes of 3 or 4 dimensions, and with cubes that have used various names to describe the depth axis
shape = np.shape(cube)
test = np.size(shape)
if test == 4:
	print 'cube has 4 dimensions, assuming time, depth, latitude, longitude'
	depth_coord = 1
elif test == 3:
	print 'cube has 3 dimensions, assuming depth, latitude, longitude'
	depth_coord = 0
else:
	print 'are you sure cube has a depth dimension?'


#Identify the values for each depth level
depths = cube.coord(dimensions = depth_coord).points
#Make a cube that just holds the depth data
depths_cube = cube.copy()
if test == 4:
	depths_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths, (shape[0],shape[3], shape[2],1)),3,1))
elif test == 3:
	depths_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths, (shape[2],shape[1],1)),2,0))


depths_cube.data.mask = cube.data.mask

#mask that cube were depths are less than your chosen value
mask = np.where(cube.data <= critical_value)
depths_cube.data.mask[mask] = True

#make a cube without a depth dimension to hold the output
depth_name = cube.coord(dimensions = depth_coord).standard_name.encode('ascii','ignore')
output_cube = cube.collapsed(depth_name,iris.analysis.MEAN)

#Find the greatest depth at which data is found above the value of interest
if test == 4:
	output_cube.data = np.max(depths_cube.data,axis = 1)
elif test == 3:
	output_cube.data = np.max(depths_cube.data,axis = 0)


#Give the cube the right metadata
output_cube.standard_name = 'depth'
output_cube.units = 'm'

#output_cube contains the depths

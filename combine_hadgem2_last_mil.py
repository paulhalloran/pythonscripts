
import os
from cdo import *
cdo = Cdo()
import iris

directory1 = '/data/NAS-ph290/ph290/cmip5/hadgem2_last_mill_netcdf/'
directory2 = '/data/NAS-ph290/ph290/cmip5/hadgem2_last_mill_netcdf2/'

variables = ['so']
#,'thetao']
#'psl',

var_long_name = ['sea_water_salinity','sea_water_potential_temperature']

for i,variable in enumerate(variables):
	file = variable+'_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc'
	cubes = iris.load(directory1+file)
	cubelist = []
	for cube in cubes:
		try:
			cube.remove_coord('forecast_reference_time')
		except:
			pass
		cubelist.append(cube)
	output_cube = iris.cube.CubeList(cubelist).concatenate()
	iris.fileformats.netcdf.save(output_cube[0], directory2+file+'1', netcdf_format='NETCDF4')
	iris.fileformats.netcdf.save(output_cube[1], directory2+file+'2', netcdf_format='NETCDF4')
	os.remove(directory1+variable+'_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc')
	cdo.mergetime(input = directory2+file+'1'+' '+directory2+file+'2',output = directory2+'tmp2.nc')
	cube = iris.load_cube(directory2+'tmp2.nc',var_long_name[i])
	iris.fileformats.netcdf.save(cube, directory1+variable+'_Omon_HadGEM2-ES_past1000_r1i1p1_0000-1111.nc', netcdf_format='NETCDF4')
	



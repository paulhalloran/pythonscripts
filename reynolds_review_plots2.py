
import matplotlib
matplotlib.use('Agg')
import iris
import matplotlib.pyplot as plt
import numpy as np
import glob
import os
import iris.analysis.stats
import iris.coord_categorisation
import pandas
import cartopy.feature as cfeature
import iris.plot as iplt
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as mpl_cm


def model_names(directory,variable):
        files = glob.glob(directory+'*'+variable+'*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


def area_avg(my_cube):
	try:
		my_cube.coord('latitude').guess_bounds()
	except:
		pass
	try:
		my_cube.coord('longitude').guess_bounds() #These two lines are just something you sometimes have to do when the original data did not include all of the latitude/longitude data we require
	except:
		pass
	grid_areas = iris.analysis.cartography.area_weights(my_cube)
	return my_cube.collapsed(['latitude', 'longitude'],iris.analysis.MEAN, weights=grid_areas) #This does the clever maths to work out the grid


def spatial_correlation(your_timeseries_data,cube_to_correlate_timeseries_with_2d):
    ts = your_timeseries_data.data
    cube = cube_to_correlate_timeseries_with_2d.copy()
    cube = iris.analysis.maths.multiply(cube, 0.0)
    ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
    cube = iris.analysis.maths.add(cube, ts2)
    return iris.analysis.stats.pearsonr(cube_to_correlate_timeseries_with_2d, cube, corr_coords=['time'])


def spatial_correlation_msftmyz(your_timeseries_data,cube_to_correlate_timeseries_with_2d):
    ts = your_timeseries_data.data
    cube = cube_to_correlate_timeseries_with_2d.copy()
    cube = iris.analysis.maths.multiply(cube, 0.0)
    ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[18,119,1]),1,2),0,1)
    cube = iris.analysis.maths.add(cube, ts2)
    return iris.analysis.stats.pearsonr(cube_to_correlate_timeseries_with_2d, cube, corr_coords=['time'])


directory = '/data/NAS-ph290/ph290/cmip5/reynolds_review/regridded/'

variables = ['tos','sos','msftbarot','msftmyz']

m1 = model_names(directory,variables[0])
m2 = model_names(directory,variables[1])
m3 = model_names(directory,variables[2])
m4 = model_names(directory,variables[3])


m = set(m1) & set(m2) & set(m3) & set(m4)
models = np.array(list(m))

west = -24
east = -14
south = 65
north = 70

grid_cube = iris.load_cube(directory+models[0]+'_msftmyz*.nc')[0,0,:,:]


msftmyz_tos_correlations = []
msftbarot_tos_correlations = []
msftmyz_sos_correlations = []
msftbarot_sos_correlations = []
msftmyz_tos_correlations_data = []
msftbarot_tos_correlations_data = []
msftmyz_sos_correlations_data = []
msftbarot_sos_correlations_data = []
msftmyz_all = []
msftbarot_all = []
msftmyz_all_data = []
msftbarot_all_data = []
np_years = []
models_used = []

to_save_name = ['msftmyz_tos_correlations','msftbarot_tos_correlations','msftmyz_sos_correlations','msftbarot_sos_correlations','msftmyz_tos_correlations_data','msftbarot_tos_correlations_data','msftmyz_sos_correlations_data','msftbarot_sos_correlations_data','msftmyz_all','msftbarot_all','msftmyz_all_data','msftbarot_all_data','np_years','models_used']
to_save_variable = [msftmyz_tos_correlations,msftbarot_tos_correlations,msftmyz_sos_correlations,msftbarot_sos_correlations,msftmyz_tos_correlations_data,msftbarot_tos_correlations_data,msftmyz_sos_correlations_data,msftbarot_sos_correlations_data,msftmyz_all,msftbarot_all,msftmyz_all_data,msftbarot_all_data,np_years,models_used]


# smoothing = 10

smoothings = [0,5,10,15]

smoothed_data = {}

for smoothing in smoothings:
	smoothed_data[str(smoothing)] = {}
	for i,dummy in enumerate(to_save_name):
		 smoothed_data[str(smoothing)][to_save_name[i]] = to_save_variable[i]

new_fill_value = np.NAN

for model in models:
	print model
	tos = iris.load_cube(directory+model+'_tos*.nc')
	iris.coord_categorisation.add_year(tos, 'time', name='year')
	tos = tos.aggregated_by('year', iris.analysis.MEAN)
	tos.data[np.where(tos.data == tos.data.fill_value)] = new_fill_value
	tos.data.fill_value = new_fill_value
	sos = iris.load_cube(directory+model+'_sos*.nc')
	iris.coord_categorisation.add_year(sos, 'time', name='year')
	sos = sos.aggregated_by('year', iris.analysis.MEAN)
	sos.data[np.where(sos.data == sos.data.fill_value)] = new_fill_value
	sos.data.fill_value = new_fill_value
	msftbarot = iris.load_cube(directory+model+'_msftbarot*.nc')
	iris.coord_categorisation.add_year(msftbarot, 'time', name='year')
	msftbarot = msftbarot.aggregated_by('year', iris.analysis.MEAN)
	msftbarot.data[np.where(msftbarot.data == msftbarot.data.fill_value)] = new_fill_value
	msftbarot.data.fill_value = new_fill_value
	msftmyz = iris.load_cube(directory+model+'_msftmyz*.nc')[:,0,:,:]
	iris.coord_categorisation.add_year(msftmyz, 'time', name='year')
	msftmyz = msftmyz.aggregated_by('year', iris.analysis.MEAN)
	msftmyz.data[np.where(msftmyz.data == msftmyz.data.fill_value)] = new_fill_value
	msftmyz.data.fill_value = new_fill_value
	#interpolate latitudes on to a single grid
	samples = [('latitude', np.linspace(-29.5,89.5,119))]
	msftmyz_regrid = iris.analysis.interpolate.linear(msftmyz,samples)
	#region
	temporary_cube = tos.intersection(longitude = (west, east))
	tos_regional = temporary_cube.intersection(latitude = (south, north))
	tos_ts = area_avg(tos_regional)
	temporary_cube = sos.intersection(longitude = (west, east))
	sos_regional = temporary_cube.intersection(latitude = (south, north))
	sos_ts = area_avg(sos_regional)
	for smoothing in smoothings:
		tos_ts2 = tos_ts.copy()
		if smoothing > 0:
			tos_ts2.data = pandas.rolling_window(tos_ts.data,smoothing,win_type='boxcar',center=True)
		loc = np.where(np.logical_not(np.isnan(tos_ts2.data)))
		sos_ts2 = sos_ts.copy()
		if smoothing > 0:
			sos_ts2.data = pandas.rolling_window(sos_ts.data,smoothing,win_type='boxcar',center=True)
		#correlation
		try:
			tmp = spatial_correlation_msftmyz(tos_ts2[loc],msftmyz_regrid[loc])
			smoothed_data[str(smoothing)]['msftmyz_tos_correlations'].append(tmp)
			smoothed_data[str(smoothing)]['msftmyz_tos_correlations_data'].append(np.ma.masked_array(tmp.data))
			tmp = spatial_correlation(tos_ts2[loc],msftbarot[loc])
			smoothed_data[str(smoothing)]['msftbarot_tos_correlations'].append(tmp)
			smoothed_data[str(smoothing)]['msftbarot_tos_correlations_data'].append(np.ma.masked_array(tmp.data))
			tmp = spatial_correlation_msftmyz(sos_ts2[loc],msftmyz_regrid[loc])
			smoothed_data[str(smoothing)]['msftmyz_sos_correlations'].append(tmp)
			smoothed_data[str(smoothing)]['msftmyz_sos_correlations_data'].append(np.ma.masked_array(tmp.data))
			tmp = spatial_correlation(sos_ts2[loc],msftbarot[loc])	
			smoothed_data[str(smoothing)]['msftbarot_sos_correlations'].append(tmp)
			smoothed_data[str(smoothing)]['msftbarot_sos_correlations_data'].append(np.ma.masked_array(tmp.data))
			smoothed_data[str(smoothing)]['np_years'].append(np.size(tos_ts2.coord('time').points))
			smoothed_data[str(smoothing)]['models_used'].append(model)
			tmp = msftmyz_regrid.collapsed('time',iris.analysis.MEAN)
			smoothed_data[str(smoothing)]['msftmyz_all'].append(tmp)
			smoothed_data[str(smoothing)]['msftmyz_all_data'].append(np.ma.masked_array(tmp.data))
			tmp = msftbarot.collapsed('time',iris.analysis.MEAN)
			smoothed_data[str(smoothing)]['msftbarot_all'].append(tmp)
			smoothed_data[str(smoothing)]['msftbarot_all_data'].append(np.ma.masked_array(tmp.data))
		except:
			print model + ' failed'


for smoothing in smoothings:
	msftmyz_tos_correlations = smoothed_data[str(smoothing)]['msftmyz_tos_correlations']
	msftmyz_tos_correlations_data = smoothed_data[str(smoothing)]['msftmyz_tos_correlations_data']
	msftbarot_tos_correlations = smoothed_data[str(smoothing)]['msftbarot_tos_correlations']
	msftbarot_tos_correlations_data = smoothed_data[str(smoothing)]['msftbarot_tos_correlations_data']
	msftmyz_sos_correlations = smoothed_data[str(smoothing)]['msftmyz_sos_correlations']
	msftmyz_sos_correlations_data = smoothed_data[str(smoothing)]['msftmyz_sos_correlations_data']
	msftbarot_sos_correlations = smoothed_data[str(smoothing)]['msftbarot_sos_correlations']
	msftbarot_sos_correlations_data = smoothed_data[str(smoothing)]['msftbarot_sos_correlations_data']
	np_years = smoothed_data[str(smoothing)]['np_years']
	models_used = smoothed_data[str(smoothing)]['models_used']
	msftmyz_all = smoothed_data[str(smoothing)]['msftmyz_all']
	msftmyz_all_data = smoothed_data[str(smoothing)]['msftmyz_all_data']
	msftbarot_all = smoothed_data[str(smoothing)]['msftbarot_all']
	msftbarot_all_data = smoothed_data[str(smoothing)]['msftbarot_all_data']


	masks = []
	for tmp in msftbarot_all:
		masks.append(tmp.data.mask)


	m1 = masks[0]
	for mask in masks:
		m1 = np.ma.mask_or(m1, mask)



	msftmyz_tos_correlations_mean = msftmyz_tos_correlations[0].copy()
	msftmyz_tos_correlations_mean.data = np.nanmean(np.array(msftmyz_tos_correlations_data),axis = 0)
	msftmyz_sos_correlations_mean = msftmyz_sos_correlations[0].copy()
	msftmyz_sos_correlations_mean.data = np.nanmean(np.array(msftmyz_sos_correlations_data),axis = 0)
	msftbarot_tos_correlations_mean = msftbarot_tos_correlations[0].copy()
	msftbarot_tos_correlations_mean.data = np.nanmean(np.array(msftbarot_tos_correlations_data),axis = 0)
	msftbarot_sos_correlations_mean = msftbarot_sos_correlations[0].copy()
	msftbarot_sos_correlations_mean.data = np.nanmean(np.array(msftbarot_sos_correlations_data),axis = 0)

	msftmyz_tos_correlations_std = msftmyz_tos_correlations[0].copy()
	msftmyz_tos_correlations_std.data = np.std(np.array(msftmyz_tos_correlations_data),axis = 0)
	msftmyz_sos_correlations_std = msftmyz_sos_correlations[0].copy()
	msftmyz_sos_correlations_std.data = np.std(np.array(msftmyz_sos_correlations_data),axis = 0)
	msftbarot_tos_correlations_std = msftbarot_tos_correlations[0].copy()
	msftbarot_tos_correlations_std.data = np.std(np.array(msftbarot_tos_correlations_data),axis = 0)
	msftbarot_sos_correlations_std = msftbarot_sos_correlations[0].copy()
	msftbarot_sos_correlations_std.data = np.std(np.array(msftbarot_sos_correlations_data),axis = 0)

	msftmyz_all_mean = msftmyz_all[0].copy()
	msftmyz_all_mean.data = np.nanmean(np.array(msftmyz_all_data),axis = 0)
	msftbarot_all_mean = msftbarot_all[0].copy()
	msftbarot_all_mean.data = np.nanmean(np.array(msftbarot_all_data),axis = 0)


	msftbarot_tos_correlations_mean.data = np.ma.masked_array(msftbarot_tos_correlations_mean.data)
	msftbarot_tos_correlations_mean.data.mask = m1

	msftbarot_sos_correlations_mean.data = np.ma.masked_array(msftbarot_sos_correlations_mean.data)
	msftbarot_sos_correlations_mean.data.mask = m1

	msftbarot_all_mean.data = np.ma.masked_array(msftbarot_all_mean.data)
	msftbarot_all_mean.data.mask = m1

	'''

	plt.close('all')
	fig = plt.figure(figsize = (10,7))
	ax = plt.subplot(111,projection=ccrs.PlateCarree())
	my_plot = iplt.contourf(msftbarot_tos_correlations_mean,np.linspace(-0.4,0.4,100))
	plt.colorbar()
	my_plot2 = iplt.contour(msftbarot_all_mean,np.linspace(-0.7e11,0.7e11,30),colors = 'k')
	ax.add_feature(cfeature.LAND,facecolor='#996600')
	ax.set_extent([-90, 10, 20, 90], ccrs.PlateCarree())
	plt.title('Barotropic SF v. SST')
	plt.savefig('/home/ph290/Documents/figures/reynolds_fig1_not_smoothed.png')
	#plt.show(block = True)

	plt.close('all')
	fig = plt.figure(figsize = (10,7))
	ax = plt.subplot(111,projection=ccrs.PlateCarree())
	my_plot = iplt.contourf(msftbarot_sos_correlations_mean,np.linspace(-0.4,0.4,100))
	plt.colorbar()
	my_plot2 = iplt.contour(msftbarot_all_mean,np.linspace(-0.7e11,0.7e11,30),colors = 'k')
	ax.add_feature(cfeature.LAND,facecolor='#996600')
	ax.set_extent([-90, 10, 20, 90], ccrs.PlateCarree())
	plt.title('Barotropic SF v. SSS')
	plt.savefig('/home/ph290/Documents/figures/reynolds_fig2_not_smoothed.png')
	#plt.show(block = True)


	plt.close('all')
	fig = plt.figure(figsize = (10,7))
	ax = plt.subplot(111)
	my_plot = iplt.contourf(msftmyz_tos_correlations_mean,31)
	plt.colorbar()
	my_plot2 = iplt.contour(msftmyz_all_mean,15,colors = 'k')
	plt.title('Overturning SF v. SST')
	plt.savefig('/home/ph290/Documents/figures/reynolds_fig3_not_smoothed.png')
	#plt.show(block = True)



	plt.close('all')
	fig = plt.figure(figsize = (10,7))
	ax = plt.subplot(111)
	my_plot = iplt.contourf(msftmyz_sos_correlations_mean,31)
	plt.colorbar()
	my_plot2 = iplt.contour(msftmyz_all_mean,15,colors = 'k')
	ax.set_ylim([4000,0])
	plt.title('Overturning SF v. SSS')
	plt.savefig('/home/ph290/Documents/figures/reynolds_fig4_not_smoothed.png')
	#plt.show(block = True)

	## all ##

	'''

	plt.close('all')
	fig = plt.figure(figsize = (10,6))

	plt.rc('legend',**{'fontsize':10})


	gs = gridspec.GridSpec(100,100,bottom=0.1,left=0.1,right=0.9)

	# ax1 = plt.subplot(gs[1])
	# ax2 = plt.subplot(gs[0])
	ax1 = plt.subplot(gs[0:45,0:35],projection=ccrs.PlateCarree())
	ax2 = plt.subplot(gs[0:45,50:85],projection=ccrs.PlateCarree())
	ax3 = plt.subplot(gs[55:100,0:35])
	ax4 = plt.subplot(gs[55:100,50:85])
	ax5 = plt.subplot(gs[0:100,95:100])

	ax1.set_aspect('auto')
	ax2.set_aspect('auto')
	ax3.set_aspect('auto')
	ax4.set_aspect('auto')

	cmap1 = mpl_cm.get_cmap('RdBu_r')

	cube = msftbarot_tos_correlations_mean
	cube.coord('longitude').circular = True
	lons = cube.coord('longitude').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot1 = ax1.contourf(lons, lats, data,np.linspace(-0.4,0.4,100),transform=ccrs.PlateCarree(),
				cmap=cmap1)
	# plt.colorbar()
	cube = msftbarot_all_mean
	cube.coord('longitude').circular = True
	lons = cube.coord('longitude').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot1b = ax1.contour(lons, lats, data,np.linspace(-0.7e11,0.7e11,30),colors = 'k',transform=ccrs.PlateCarree())
	ax1.add_feature(cfeature.LAND,facecolor='#A6A6A6')
	ax1.set_extent([-90, 10, 20, 90], ccrs.PlateCarree())
	ax1.set_title('Barotropic Stream function v. SST')
	#plt.show(block = True)


	cube = msftbarot_sos_correlations_mean
	lons = cube.coord('longitude').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot2 = ax2.contourf(lons, lats, data,np.linspace(-0.4,0.4,100),transform=ccrs.PlateCarree(),
				cmap=cmap1)
	# plt.colorbar()
	cube = msftbarot_all_mean
	lons = cube.coord('longitude').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot2b = ax2.contour(lons, lats, data,np.linspace(-0.7e11,0.7e11,30),colors = 'k',transform=ccrs.PlateCarree())
	ax2.add_feature(cfeature.LAND,facecolor='#A6A6A6')
	ax2.set_extent([-90, 10, 20, 90], ccrs.PlateCarree())
	ax2.set_title('Barotropic Stream function v. SSS')


	cube = msftmyz_tos_correlations_mean
	depth = cube.coord('depth').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot3 = ax3.contourf(lats, depth, data,np.linspace(-0.4,0.4,100),cmap=cmap1)
	# plt.colorbar()
	cube = msftmyz_all_mean
	depth = cube.coord('depth').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot3b = ax3.contour(lats, depth, data,15,colors = 'k')
	ax3.set_ylim([4000,0])
	ax3.set_yticks([500,1500,2500,3500])
	ax3.set_xlabel('latitude')
	ax3.set_ylabel('depth (m)')
	ax3.set_title('Overturning Stream function v. SST')


	cube = msftmyz_sos_correlations_mean
	depth = cube.coord('depth').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot4 = ax4.contourf(lats, depth, data,np.linspace(-0.4,0.4,100),cmap=cmap1)
	# plt.colorbar()
	cube = msftmyz_all_mean
	depth = cube.coord('depth').points
	lats = cube.coord('latitude').points
	data = cube.data
	my_plot4b = ax4.contour(lats, depth, data,15,colors = 'k')
	ax4.set_ylim([4000,0])
	ax4.set_yticks([500,1500,2500,3500])
	ax4.set_xlabel('latitude')
	ax4.set_ylabel('depth (m)')
	ax4.set_title('Overturning Stream function v. SSS')

	cb = plt.colorbar(my_plot1,ax=ax1,cax=ax5, ticks=[-0.4, -0.2, 0.0, 0.2, 0.4])
	cb.set_label('Correlation Coefficient')

	plt.savefig('/home/ph290/Documents/figures/reynolds_all_plots_smoothing_of_'+str(smoothing)+'_yrs.png')

	#plt.show(block = True)

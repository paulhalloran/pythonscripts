import numpy as np

def ap2ep(Au, PHIu, Av, PHIv):
    PHIu = PHIu / 180. * np.pi
    PHIv = PHIv / 180. * np.pi
    # Make complex amplitudes for u and v
    i = np.sqrt(-1+0j)
    u = Au * np.exp(-i * PHIu)
    v = Av * np.exp(-i * PHIv)
    # Calculate complex radius of anticlockwise and clockwise circles
    wp = (u + i * v) / 2. # for anticlockwise circles
    wm = np.conj(u - i * v) / 2. # for clockwise circles
    # and thier amplitudes and angles
    Wp = np.abs(wp)
    Wm = np.abs(wm)
    THETAp = np.angle(wp)
    THETAm = np.angle(wm)
    # calculate the ellipse parameters
    SEMA = Wp + Wm
    SEMI = Wp - Wm
    ECC = SEMI / SEMA
    PHA = (THETAm - THETAp) / 2.
    INC = (THETAm + THETAp) / 2.
    PHA = PHA / np.pi * 180
    INC = INC / np.pi * 180
    THETAp = THETAp / np.pi * 180
    THETAm = THETAm / np.pi * 180
    THETAp[np.where(THETAp < 0)] = THETAp[np.where(THETAp < 0)] + 360
    THETAm[THETAm < 0] = THETAm[THETAm < 0] + 360
    PHA[PHA < 0] = PHA[PHA < 0] + 360
    INC[INC < 0] = INC[INC < 0] + 360
    return(PHA, INC, SEMI, SEMA)


Au = np.array([2.420,2.420])
PHIu = np.array([-4.1,-4.1])
Av = np.array([2.792,2.792])
PHIv = np.array([119.2,119.2])

PHA, INC, SEMI, SEMA = ap2ep(Au, PHIu, Av, PHIv)

print SEMA
print SEMI

import iris
import numpy as np
import glob
import os
import matplotlib.pyplot as pyplot
import iris.quickplot as qplt
import matplotlib as mpl
from cdo import *
cdo = Cdo()
import itertools

in_dir = '/data/dataSSD0/ph290/'
out_dir = '/data/dataSSD1/ph290/'

def model_names(directory):
        files = glob.glob(directory+'/*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[2])
                        models = np.unique(models_tmp)
        return models

def bounds(cube):
    try:
        cube.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'
    try:
        cube.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'
    return cube



models = model_names(in_dir)
models = list(models)
models.remove('MRI-ESM1')
# models.remove('GFDL-ESM2M')


"""
for model in models:
    if not(os.path.isfile(out_dir+model+'_talk_global.nc')):
        cdo.mergetime(input=in_dir+'talk_Oyr_'+model+'_historical_r1i1p1_*.nc',output=out_dir+'tmp.nc',options = '-P 7')
        cdo.timmean(input= out_dir+'tmp.nc', output=out_dir+model+'_talk_global.nc',options = '-P 7')
        cdo.fldmean(input= out_dir+model+'_talk_global.nc', output=out_dir+model+'_talk_global_profile.nc',options = '-P 7')
        os.remove(out_dir+'tmp.nc')
"""


glodap_alk = iris.load_cube('/data/NAS-ph290/ph290/misc_data/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TAlk.nc','seawater alkalinity expressed as mole equivalent per unit mass')
glodap_alk_error = iris.load_cube('/data/NAS-ph290/ph290/misc_data/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TAlk.nc','total alkalinity error')
woa_cube =  iris.load_cube('/data/NAS-ph290/ph290/misc_data/temperature_annual_1deg.nc','sea_water_temperature')
tmm_alk = iris.load_cube('/home/ph290/Downloads/ALK.nc')

tmm_alk = bounds(tmm_alk)

grid_areas = iris.analysis.cartography.area_weights(tmm_alk)
tmm_alk_area_avged_cube = tmm_alk.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

glodap_alk = bounds(glodap_alk)
glodap_alk_error = bounds(glodap_alk_error)

grid_areas = iris.analysis.cartography.area_weights(glodap_alk)
glodap_alk_area_avged_cube = glodap_alk.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

grid_areas = iris.analysis.cartography.area_weights(glodap_alk_error)
glodap_alk_error_area_avged_cube = glodap_alk_error.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)


mpl.rcdefaults()

fsize = 12

font = {'family' : 'monospace',
        'weight' : 'bold',
        'family' : 'serif',
        'size'   : fsize}

mpl.rc('font', **font)
marker = itertools.cycle(('o', 'v', '^', '<', '>', 's', '8','p'))

plt.close('all')
plt.figure(figsize=(10, 10))
for model in models:
    print model
    cube = iris.load_cube(out_dir+model+'_talk_global_profile.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
    # cube = iris.load_cube(out_dir+model+'_talk_global.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
    cube = bounds(cube)
    try:
        # grid_areas = iris.analysis.cartography.area_weights(cube)
        if np.size(np.shape(cube)) == 4:
            print 'plotting '+ model
            # cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN, weights=grid_areas)
            cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN)
            try:
                plt.plot(cube.data,cube.coord('depth').points * -1.0,alpha=0.5,lw=0.001,marker=marker.next(),label = model)
            except:
                print "no 'depth' cooordinate"
                try:
                    plt.plot(cube.data,cube.coord('ocean sigma over z coordinate').points * -1.0,lw=0.001,alpha=0.5,marker=marker.next(),label = model)
                except:
                    print "no 'ocean sigma over z coordinate' cooordinate"
                # qplt.plot(cube,'b')
    except:
        print 'area averaging within iris failed'

# plt.plot(tmm_alk_area_avged_cube.data,tmm_alk_area_avged_cube.coord('Depth').points * -1.0,'r',lw=3)

plt.plot((glodap_alk_area_avged_cube.data/1.0e3)*1.026,woa_cube.coord('depth').points * -1.0,'r',lw=3,label='GLODAP')
# plt.plot(glodap_alk_error_area_avged_cube.data/1.026e3),woa_cube.coord('depth').points * -1.0,'k',lw=1)

plt.legend(fontsize='small')
# plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile_iris_averaging.png')
plt.xlabel('alkalinity (mole equivalent m$^{-3}$)')
plt.ylabel('demth(m)')
plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile.png')
plt.show(block = False)

# GLODAP = mole equivalent per unit mass
# CMIP5 = mole_equivalent / (mol m-3)

#g = obs initialised
#r= model initialised
#b=mixed
#y = fixed value
#k = unkown

models_d = {}
models_d['BNU-ESM'] = 'k'
models_d['CESM1-BGC'] = 'b'
models_d['CMCC-CESM'] = 'g'
models_d['CNRM-CM5'] = 'b'
models_d['CanESM2'] = 'b'
models_d['GFDL-ESM2G'] = 'g'
models_d['GFDL-ESM2M'] = 'g'
models_d['HadGEM2-CC'] = 'r'
models_d['HadGEM2-ES'] = 'r'
models_d['IPSL-CM5A-LR'] = 'r'
models_d['IPSL-CM5A-MR'] = 'b'
models_d['IPSL-CM5B-LR'] = b''
models_d['MIROC-ESM'] = 'g'
models_d['MIROC-ESM-CHEM'] = 'g'
models_d['MPI-ESM-LR'] = 'y'
models_d['MPI-ESM-MR'] = 'y'
models_d['NorESM1-ME'] = 'g'

plt.close('all')
plt.figure(figsize=(10, 10))

plt.plot((glodap_alk_area_avged_cube.data/1.0e3)*1.026,woa_cube.coord('depth').points * -1.0,'g',lw=3,label='GLODAP')
# plt.plot(glodap_alk_error_area_avged_cube.data/1.026e3),woa_cube.coord('depth').points * -1.0,'k',lw=1)

for model in models:
    print model
    cube = iris.load_cube(out_dir+model+'_talk_global_profile.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
    # cube = iris.load_cube(out_dir+model+'_talk_global.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
    cube = bounds(cube)
    try:
        # grid_areas = iris.analysis.cartography.area_weights(cube)
        if np.size(np.shape(cube)) == 4:
            print 'plotting '+ model
            # cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN, weights=grid_areas)
            cube = cube.collapsed(['longitude', 'latitude','time'], iris.analysis.MEAN)
            try:
                plt.scatter(cube.data,cube.coord('depth').points * -1.0,alpha=0.5,lw=0.001,color = models_d[model],label = model)
            except:
                print "no 'depth' cooordinate"
                try:
                    plt.scatter(cube.data,cube.coord('ocean sigma over z coordinate').points * -1.0,lw=0.001,alpha=0.5,color = models_d[model],label = model)
                except:
                    print "no 'ocean sigma over z coordinate' cooordinate"
                # qplt.plot(cube,'b')
    except:
        print 'area averaging within iris failed'

# plt.plot(tmm_alk_area_avged_cube.data,tmm_alk_area_avged_cube.coord('Depth').points * -1.0,'r',lw=3)

plt.legend(fontsize='small')
# plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile_iris_averaging.png')
plt.xlabel('alkalinity (mole equivalent m$^{-3}$)')
plt.ylabel('demth(m)')
plt.savefig('/home/ph290/Documents/figures/cmip5_global_alkalinity_profile_by initialisation.png')
plt.show(block = False)

'''
/home/ph290/Documents/python_scripts/palaeo_amo/latest_270716
'''




import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import scipy
from scipy import signal


dir1 = '/data/data1/ph290/cmip5/last1000/mld/deep_salinity_density/spatial_avg_larger6_varying_t_s/'
dir2 = '/data/data1/ph290/cmip5/last1000/mld/deep_temperature_density/spatial_avg_larger6/'
dir3 = '/data/data1/ph290/cmip5/last1000/mld/deep_salinity_density/spatial_avg_larger6/'


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
	models = np.unique(models_tmp)
	return models


models = model_names(dir1)
models = list(models)
models.remove('CSIRO-Mk3L-1-2')
models.remove('HadCM3')

#############
# remove the line below as soon as teh HadHEM2-ES P and E have been processed.
#############
models.remove('HadGEM2-ES')
print 'hadgem2-es does not appear to have saved a suitable evaporation field to calculate p-e over the ocean (only precip available over the oean)'

#and the following temporarily for wind speed anlaysis
models.remove('CCSM4')
models.remove('GISS-E2-R')

###########################
# T or S?
###########################

plt.close('all')
f, axarr = plt.subplots(5,2)

model_data = {}

for i,model in enumerate(models):
	j = i+1
	print model
	model_data[model] = {}
	x = (5+ j - (np.int(np.ceil(j/5.0))) * 5) - 1
	y = (np.int(np.ceil(j/5.0))) - 1

	f1 = dir1+'density_mixed_layer_'+model+'_past1000_r*_JJA.nc'
	f2 = dir2+'density_mixed_layer_'+model+'_past1000_r*_JJA_const_t.nc'
	f3 = dir3+'density_mixed_layer_'+model+'_past1000_r*_JJA_const_s.nc'

# 	try:
	cube1 = iris.load_cube(f1).collapsed(['latitude','longitude'],iris.analysis.SUM)
	cube2 = iris.load_cube(f2).collapsed(['latitude','longitude'],iris.analysis.SUM)
	cube3 = iris.load_cube(f3).collapsed(['latitude','longitude'],iris.analysis.SUM)

	coord = cube2.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((year >= 950) & (year <= 1850))
	cube1 = cube1[loc]
	cube2 = cube2[loc]
	cube3 = cube3[loc]

	cube1.data = signal.detrend(cube1.data)
	cube2.data = signal.detrend(cube2.data)
	cube3.data = signal.detrend(cube3.data)

	model_data[model]['t_s_varying_density'] = cube1
	model_data[model]['t_const_density'] = cube2
	model_data[model]['s_const_density'] = cube3

	axarr[x,y].plot(rm.running_mean(cube1.data,10),'k')
	axarr[x,y].plot(rm.running_mean(cube2.data,10),'b')
	axarr[x,y].plot(rm.running_mean(cube3.data,10),'r')
	axarr[x,y].set_title(model)
# 	except:
# 		print 'model not working'

print 'red is temperature held constant (so driven by salinity)'
print 'blue is salinity held constant (so driven by temperature)'

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/t_s_density.png')





###########################
# P-E
###########################


lon_west = -24
lon_east = -4
lat_south = 65
lat_north = 70

yearsec = 60.0*60.0*24.0*365


directory = '/data/NAS-ph290/ph290/cmip5/last1000/'

for i,model in enumerate(models):
	ensemble = 'r1i1p1'
	if model == 'GISS-E2-R':
		ensemble = 'r1i1p121'
	p_cube = iris.load_cube(directory+model+'_pr_past1000_'+ensemble+'_regridded_not_vertically_Amon.nc','precipitation_flux') # kg m-2 s-1
	if model == 'HadGEM2-ES':
		e_cube = iris.load_cube(directory+model+'_evspsbl_past1000_'+ensemble+'_regridded.nc','water_potential_evaporation_flux') # kg m-2 s-1
	else:
		e_cube = iris.load_cube(directory+model+'_evspsbl_past1000_'+ensemble+'_regridded.nc','water_evaporation_flux') # kg m-2 s-1
	if  model == 'FGOALS-s2':
		e_cube *= -1.0
	uas_cube = iris.load_cube(directory+model+'_uas_past1000_'+ensemble+'_regridded.nc')
	vas_cube = iris.load_cube(directory+model+'_vas_past1000_'+ensemble+'_regridded.nc')
	ws_cube = iris.analysis.maths.exponentiate(iris.analysis.maths.exponentiate(uas_cube,2) + iris.analysis.maths.exponentiate(vas_cube,2),0.5)

	p_minus_e_cube = p_cube - e_cube
	model_data[model]['p_cube'] = p_cube
	model_data[model]['e_cube'] = e_cube
	model_data[model]['p_minus_e_cube'] = p_minus_e_cube
	model_data[model]['uas_cube'] = uas_cube
	model_data[model]['vas_cube'] = vas_cube
	model_data[model]['ws_cube'] = ws_cube

	cube_region_tmp = p_cube.intersection(longitude=(lon_west, lon_east))
	p_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

	cube_region_tmp = e_cube.intersection(longitude=(lon_west, lon_east))
	e_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

	cube_region_tmp = p_minus_e_cube.intersection(longitude=(lon_west, lon_east))
	p_minus_e_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))


	try:
		p_cube_region.coord('latitude').guess_bounds()
	except:
		print 'cube already has latitude bounds'
	try:
		p_cube_region.coord('longitude').guess_bounds()
	except:
		print 'cube already has longitude bounds'

	grid_areas = iris.analysis.cartography.area_weights(p_cube_region)
	p_cube_region_area_avged = p_cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

	coord = p_cube_region_area_avged.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((year >= 950) & (year <= 1850))
	p_cube_region_area_avged = p_cube_region_area_avged[loc]
	p_cube_region_area_avged.data = signal.detrend(p_cube_region_area_avged.data)

	model_data[model]['p'] = p_cube_region_area_avged

	try:
		e_cube_region.coord('latitude').guess_bounds()
	except:
		print 'cube already has latitude bounds'
	try:
		e_cube_region.coord('longitude').guess_bounds()
	except:
		print 'cube already has longitude bounds'

	grid_areas = iris.analysis.cartography.area_weights(e_cube_region)
	e_cube_region_area_avged = e_cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

	coord = e_cube_region_area_avged.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((year >= 950) & (year <= 1850))
	e_cube_region_area_avged = e_cube_region_area_avged[loc]
	e_cube_region_area_avged.data = signal.detrend(e_cube_region_area_avged.data)

	model_data[model]['e'] = e_cube_region_area_avged

	try:
		p_minus_e_cube_region.coord('latitude').guess_bounds()
	except:
		print 'cube already has latitude bounds'
	try:
		p_minus_e_cube_region.coord('longitude').guess_bounds()
	except:
		print 'cube already has longitude bounds'

	grid_areas = iris.analysis.cartography.area_weights(p_minus_e_cube_region)
	p_minus_e_cube_region_area_avged = e_cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

	coord = p_minus_e_cube_region_area_avged.coord('time')
	dt = coord.units.num2date(coord.points)
	year = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((year >= 950) & (year <= 1850))
	p_minus_e_cube_region_area_avged = p_minus_e_cube_region_area_avged[loc]
	p_minus_e_cube_region_area_avged.data = signal.detrend(p_minus_e_cube_region_area_avged.data)

	model_data[model]['p_minus_e'] = e_cube_region_area_avged




################
# density ensemble mean
################


all_yrs = []

for i,model in enumerate(models):
	c1 = model_data[model]['t_const_density']
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year1 = np.array([coord.units.num2date(value).year for value in coord.points])
	all_yrs = np.append(all_yrs,year1)


all_yrs = np.unique(all_yrs)
max_yrs = np.size(all_yrs)


t_const_density_all_models = np.zeros([np.size(models),max_yrs])
t_const_density_all_models[:] = np.nan
s_const_density_all_models = t_const_density_all_models.copy()
density_all_models = t_const_density_all_models.copy()

t_const_density_all_models_normalised = t_const_density_all_models.copy()
s_const_density_all_models_normalised = t_const_density_all_models.copy()
density_all_models_normalised = t_const_density_all_models.copy()

for i,model in enumerate(models):
	c1 = model_data[model]['t_const_density']
	coord = c1.coord('time')
	dt = coord.units.num2date(coord.points)
	year1 = np.array([coord.units.num2date(value).year for value in coord.points])
	c1 = signal.detrend(model_data[model]['t_const_density'].data)
	c2 = signal.detrend(model_data[model]['s_const_density'].data)
	c3 = signal.detrend(model_data[model]['t_s_varying_density'].data)
	c1b = c1 - np.min(c1)
	c1b /= np.max(c1b)
	c2b = c2 - np.min(c2)
	c2b /= np.max(c2b)
	c3b = c3 - np.min(c3)
	c3b /= np.max(c3b)
	for j,yr in enumerate(all_yrs):
		loc = np.where(year1 == yr)
		if np.size(loc) <> 0:
			t_const_density_all_models[i,j] = c1[loc[0][0]]
			s_const_density_all_models[i,j] = c2[loc[0][0]]
			density_all_models[i,j] = c3[loc[0][0]]
			#
			t_const_density_all_models_normalised[i,j] = c1b[loc[0][0]]
			s_const_density_all_models_normalised[i,j] = c2b[loc[0][0]]
			density_all_models_normalised[i,j] = c3b[loc[0][0]]


t_const_density_mm_mean = np.nanmean(t_const_density_all_models,axis = 0)
s_const_density_mm_mean = np.nanmean(s_const_density_all_models,axis = 0)
density_mm_mean = np.nanmean(density_all_models,axis = 0)


t_const_density_mm_mean_normalised = np.nanmean(t_const_density_all_models_normalised,axis = 0)
s_const_density_mm_mean_normalised = np.nanmean(s_const_density_all_models_normalised,axis = 0)
density_mm_mean_normalised = np.nanmean(density_all_models_normalised,axis = 0)






################
# p-e ensemble mean
################

all_yrs = []

for i,model in enumerate(models):
	try:
		c1 = model_data[model]['p_minus_e']
		coord = c1.coord('time')
		dt = coord.units.num2date(coord.points)
		year1 = np.array([coord.units.num2date(value).year for value in coord.points])
		all_yrs = np.append(all_yrs,year1)
	except:
		print 'failed'

all_yrs = np.unique(all_yrs)
max_yrs = np.size(all_yrs)


p_minus_e_all_models = np.zeros([np.size(models),max_yrs])
p_minus_e_all_models[:] = np.nan
p_minus_e_all_models_normalised = p_minus_e_all_models.copy()



p_minus_e_cube_mm = np.zeros([np.size(models),900,180,360])
p_minus_e_cube_mm[:] = np.nan
p_cube_mm  = p_minus_e_cube_mm.copy()
e_cube_mm  = p_minus_e_cube_mm.copy()
uas_cube_mm  = p_minus_e_cube_mm.copy()
vas_cube_mm  = p_minus_e_cube_mm.copy()
ws_cube_mm  = p_minus_e_cube_mm.copy()

for i,model in enumerate(models):
	c = model_data[model]['p_minus_e_cube']
	c1 = model_data[model]['p_cube']
	c2 = model_data[model]['e_cube']
	c3 = model_data[model]['uas_cube']
	c4 = model_data[model]['vas_cube']
	c5 = model_data[model]['ws_cube']

	coord = c.coord('time')
	dt = coord.units.num2date(coord.points)
	year1 = np.array([coord.units.num2date(value).year for value in coord.points])
	loc1 = np.where(year1 >= np.min(all_yrs))[0][0]
	loc2 = np.where(year1 >= 1849)[0][0]
	c = c[loc1:loc2+1]
	c1 = c1[loc1:loc2+1]
	c2 = c2[loc1:loc2+1]
	c3 = c3[loc1:loc2+1]
	c4 = c4[loc1:loc2+1]
	c5 = c5[loc1:loc2+1]
	c.data = scipy.signal.detrend(c.data, axis=0)
	c1.data = scipy.signal.detrend(c1.data, axis=0)
	c2.data = scipy.signal.detrend(c2.data, axis=0)
	c3.data = scipy.signal.detrend(c3.data, axis=0)
	c4.data = scipy.signal.detrend(c4.data, axis=0)
	c5.data = scipy.signal.detrend(c5.data, axis=0)
	model_data[model]['p_minus_e_cube_trimmed_and_detrended'] = c
	p_minus_e_cube_mm[i,:,:,:] = c.data

	model_data[model]['p_cube_trimmed_and_detrended'] = c1
	p_cube_mm[i,:,:,:] = c1.data

	model_data[model]['e_cube_trimmed_and_detrended'] = c2
	e_cube_mm[i,:,:,:] = c2.data

	model_data[model]['uas_cube_trimmed_and_detrended'] = c3
	uas_cube_mm[i,:,:,:] = c3.data

	model_data[model]['vas_cube_trimmed_and_detrended'] = c4
	vas_cube_mm[i,:,:,:] = c4.data

	model_data[model]['ws_cube_trimmed_and_detrended'] = c5
	ws_cube_mm[i,:,:,:] = c5.data


p_minus_e_cube_mm_mean = model_data[models[0]]['p_minus_e_cube_trimmed_and_detrended']
p_minus_e_cube_mm_mean.data = np.nanmean(p_minus_e_cube_mm,axis = 0)

p_cube_mm_mean = model_data[models[0]]['p_cube_trimmed_and_detrended']
p_cube_mm_mean.data = np.nanmean(p_cube_mm,axis = 0)

e_cube_mm_mean = model_data[models[0]]['e_cube_trimmed_and_detrended']
e_cube_mm_mean.data = np.nanmean(e_cube_mm,axis = 0)

uas_cube_mm_mean = model_data[models[0]]['uas_cube_trimmed_and_detrended']
uas_cube_mm_mean.data = np.nanmean(uas_cube_mm,axis = 0)

vas_cube_mm_mean = model_data[models[0]]['vas_cube_trimmed_and_detrended']
vas_cube_mm_mean.data = np.nanmean(vas_cube_mm,axis = 0)

ws_cube_mm_mean = model_data[models[0]]['ws_cube_trimmed_and_detrended']
ws_cube_mm_mean.data = np.nanmean(ws_cube_mm,axis = 0)


for i,model in enumerate(models):
# 	try:
		c1 = model_data[model]['p_minus_e']
		coord = c1.coord('time')
		dt = coord.units.num2date(coord.points)
		year1 = np.array([coord.units.num2date(value).year for value in coord.points])
		c1 = signal.detrend(model_data[model]['p_minus_e'].data)
		c1b = c1 - np.min(c1)
		c1b /= np.max(c1b)
		for j,yr in enumerate(all_yrs):
			loc = np.where(year1 == yr)
			if np.size(loc) <> 0:
				p_minus_e_all_models[i,j] = c1[loc[0][0]]
				p_minus_e_all_models_normalised[i,j] = c1b[loc[0][0]]
# 	except:
# 		print 'failed'


p_minus_e_mm_mean = np.nanmean(p_minus_e_all_models,axis = 0)
p_minus_e_mm_mean_normalised = np.nanmean(p_minus_e_all_models_normalised,axis = 0)


tmp = p_minus_e_mm_mean
#tmp = rm.running_mean(tmp,10)
tmp -= np.nanmean(tmp)
high_points = np.where(tmp >= np.nanstd(tmp))[0]
low_points = np.where(tmp <= np.nanstd(tmp) * -1.0)[0]


p_minus_e_cube_mm_mean_diff = p_minus_e_cube_mm_mean[high_points[0:-1]].collapsed('time',iris.analysis.MEAN) - p_minus_e_cube_mm_mean[low_points[0:-1]].collapsed('time',iris.analysis.MEAN)
p_cube_mm_mean_diff = p_cube_mm_mean[high_points[0:-1]].collapsed('time',iris.analysis.MEAN) - p_cube_mm_mean[low_points[0:-1]].collapsed('time',iris.analysis.MEAN)
e_cube_mm_mean_diff = e_cube_mm_mean[high_points[0:-1]].collapsed('time',iris.analysis.MEAN) - e_cube_mm_mean[low_points[0:-1]].collapsed('time',iris.analysis.MEAN)

uas_cube_mm_mean_diff = uas_cube_mm_mean[high_points[0:-1]].collapsed('time',iris.analysis.MEAN) - uas_cube_mm_mean[low_points[0:-1]].collapsed('time',iris.analysis.MEAN)
vas_cube_mm_mean_diff = vas_cube_mm_mean[high_points[0:-1]].collapsed('time',iris.analysis.MEAN) - vas_cube_mm_mean[low_points[0:-1]].collapsed('time',iris.analysis.MEAN)
ws_cube_mm_mean_diff = ws_cube_mm_mean[high_points[0:-1]].collapsed('time',iris.analysis.MEAN) - ws_cube_mm_mean[low_points[0:-1]].collapsed('time',iris.analysis.MEAN)


################
# plotting
################


plt.close('all')
plt.plot(rm.running_mean(density_mm_mean,10),'k')
plt.plot(rm.running_mean(t_const_density_mm_mean,10),'b')
plt.plot(rm.running_mean(s_const_density_mm_mean,10),'r')
plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/density_v_precip_mm_mean.png')


plt.close('all')
plt.scatter(density_mm_mean,t_const_density_mm_mean,color = 'b',alpha = 0.2,label='Only salinity varying')
plt.scatter(density_mm_mean,s_const_density_mm_mean,color = 'r',alpha = 0.2,label='Only temperature varying')
plt.plot([-0.25,0.35],[-0.25,0.35],'k')
plt.xlabel('Mixed layer density anomaly')
plt.ylabel('Mixed layer density anomaly calculated with\nonly temperature or salinity varying')
plt.legend(loc='best', fancybox=True, framealpha=0.5)
plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/density_w_or_without_varying_var_mm_mean_scatter.png')
# plt.show(block = False)


plt.close('all')
fig, axarr = plt.subplots(4,2,figsize=[5,15])

for i,model in enumerate(models):
	try:
		fig.add_subplot(4,2,i+1)
		c1 = model_data[model]['t_const_density']
		c2 = model_data[model]['p_minus_e']
		coord = c1.coord('time')
		dt = coord.units.num2date(coord.points)
		year1 = np.array([coord.units.num2date(value).year for value in coord.points])
		coord = c2.coord('time')
		dt = coord.units.num2date(coord.points)
		year2 = np.array([coord.units.num2date(value).year for value in coord.points])
		unique_years = np.unique(list(set(list(year1)).intersection(list(year2))))
		for yr in unique_years:
			loc1 = np.where(year1 == yr)
			loc2 = np.where(year2 == yr)
			plt.scatter(c1.data[loc1],c2.data[loc2] * yearsec)
			plt.title(model)
	except:
		print 'not worked'


plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/density_v_precip.png')
# plt.show()


plt.close('all')
for i,model in enumerate(models):
	c1 = model_data[model]['t_const_density'].data
	c2 = model_data[model]['p_minus_e'].data
	fig, ax1 = plt.subplots()
	ax1.plot(rm.running_mean(c2[0:1000],10),'b')
	ax2 = ax1.twinx()
	ax2.plot(rm.running_mean(c1[0:1000]*yearsec,10),'g')
	plt.title(model)
	# ax2.plot(rmp.running_mean_post(p_minus_e_mm_mean[0:1000]*yearsec,10),'g')
	# plt.show(block = True)


plt.close('all')
model = 'FGOALS-s2'
plt.close('all')
c1 = model_data[model]['t_const_density'].data
c2 = model_data[model]['p_minus_e'].data
fig, ax1 = plt.subplots()
ax1.plot(rm.running_mean(c2[0:1000],10),'b')
ax2 = ax1.twinx()
ax2.plot(rm.running_mean(c1[0:1000]*yearsec * -1.0,10),'g')
plt.title(model)
# ax2.plot(rmp.running_mean_post(p_minus_e_mm_mean[0:1000]*yearsec,10),'g')
# plt.show(block = True)

#######
# contourplot
#######

print 'Add the following to a single plot with the timeseries on it... Then think about maps of P-E'

max_shift = 19
no_shifts = 20
laggings = np.linspace(0,max_shift,no_shifts)
smoothings = np.arange(30)+2
out = np.zeros([np.size(smoothings),np.size(laggings)])
p_array = out.copy()

for i,smoothing in enumerate(smoothings):
	for j,lag in enumerate(laggings):
		y = rm.running_mean(p_minus_e_mm_mean_normalised,smoothing)
		y -= np.nanmin(y)
		y /= np.nanmax(y)
		x = rm.running_mean(t_const_density_mm_mean_normalised,smoothing)
		x = np.roll(x,int(lag))
		x[0:lag] = np.nan
		x -= np.nanmin(x)
		x /= np.nanmax(x)
		loc = np.where((np.isfinite(y)) & (np.isfinite(x)))
		y = y[loc]
		x = x[loc]
		xsort = np.argsort(x)
		x = x[xsort]
		y = y[xsort]
		xb = sm.add_constant(x)
		s_model = sm.OLS(y,xb)
		results = s_model.fit()
		r2 = results.rsquared
		p_var = results.pvalues[0]
		out[i,j] = r2
		p_array[i,j] = p_var

plt.rc('legend',**{'fontsize':10})
plt.close('all')
# fig, ax1 = plt.subplots()
fig = plt.figure()
fig.set_figheight(3)
fig.set_figwidth(9)
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.90,top=0.85)
ax1 = plt.subplot(gs[0:100,0:55])
ax3 = plt.subplot(gs[0:100,75:100])

# cmap = mpl_cm.get_cmap('RdBu_r')
cmap = mpl_cm.get_cmap('Reds')


# fig, axarr = plt.subplots()
y = rm.running_mean(p_minus_e_mm_mean_normalised,5)
y -= np.nanmin(y)
y /= np.nanmax(y)
# x = rm.running_mean(t_const_density_mm_mean_normalised,5)
x = rm.running_mean(t_const_density_mm_mean_normalised,5)
x -= np.nanmin(x)
x /= np.nanmax(x)
ax1.plot(all_yrs,x,'b',lw=2,alpha=0.6)
ax2 = ax1.twinx()
ax2.plot(all_yrs,y,'brown',lw=2,alpha=0.6)
ax1.set_xlabel('Calendar year')
ax1.set_ylabel('Normalised salinity-driven\ndensity anomaly')
ax2.set_ylabel('Normalised precipitation\nminus evaporation')
ax1.yaxis.label.set_color('blue')
ax2.yaxis.label.set_color('brown')

# ax2.plot(rmp.running_mean_post(p_minus_e_mm_mean[0:1000]*yearsec,10),'g')
# plt.show(block = False)


xlab , ylab = np.meshgrid(np.round(np.linspace(0,max_shift,no_shifts)),smoothings)
C = ax3.contourf(xlab,ylab,out,np.linspace(0,0.60,100),cmap=cmap)
# ax3.contour(xlab,ylab,out,np.linspace(0,0.60,10),colors='k')
levels = [0.5,0.4,0.3,0.2,0.1]
C2 = ax3.contour(xlab,ylab,out,levels,colors='k')
ax3.clabel(C2, fontsize=9, inline=1)
cb = plt.colorbar(C,ax=ax3,ticks=[0.0,0.2,0.4,0.6],orientation = 'vertical')

ax3.set_yticks([5,10,15,20])
ax3.set_xticks([0,5,10])
ax3.set_ylim(2,20)
ax3.set_xlim(0,10)
ax3.set_xlabel('Salinity driven density\nlagging P-E (years)')
ax3.set_ylabel('Smoothing (years)')
cb.set_label('Variance explained (R$^2$)')

# plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/density_precip_and_contour.png')


print 'R2 = ', np.max(out)



# plt.show(block = False)



import statsmodels.api as sm
# plt.scatter(t_const_density_mm_mean[0:1000],p_minus_e_mm_mean[0:1000]*yearsec)

plt.close('all')
y = p_minus_e_mm_mean_normalised
y -= np.min(y)
y /= np.max(y)
x = t_const_density_mm_mean_normalised
x -= np.min(x)
x /= np.max(x)
xsort = np.argsort(x)
x = x[xsort]
y = y[xsort]

xb = sm.add_constant(x)
model = sm.OLS(y,xb)
results = model.fit()

x2 = np.linspace(np.min(x),np.max(x),np.size(x))
y2 = results.predict(sm.add_constant(x2))

from statsmodels.stats.outliers_influence import summary_table

st, data, ss2 = summary_table(results, alpha=0.001)
fittedvalues = data[:,2]
predict_mean_se  = data[:,3]
predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
predict_ci_low, predict_ci_upp = data[:,6:8].T

plt.scatter(x,y)
plt.plot(x, fittedvalues, 'k', lw=2)
#plt.plot(x, predict_ci_low, 'r--', lw=2)
#plt.plot(x, predict_ci_upp, 'r--', lw=2)
#plt.plot(x, predict_mean_ci_low, 'r--', lw=2)
#plt.plot(x, predict_mean_ci_upp, 'r--', lw=2)

plt.xlim(0,1)
plt.ylim(0,1)

plt.annotate('R2 = '+ str(np.round(results.rsquared,decimals = 2)), xy=(0.1, 0.9))

plt.ylabel('Precipitation minus evaporation\n(Normalised multimodel mean)')
plt.xlabel('Mixed layer density, salinity only varying\n(Normalised multimodel mean)')
# plt.show(block = False)

print 'R2 = ', results.rsquared


print 'add HadGEM2-ES'


plt.figure(1)
qplt.contourf(p_minus_e_cube_mm_mean_diff,31)
plt.gca().coastlines()
plt.title('P-E')
plt.show(block = False)

plt.figure(2)
qplt.contourf(e_cube_mm_mean_diff,31)
plt.gca().coastlines()
plt.title('E')
plt.show(block = False)

plt.figure(3)
qplt.contourf(p_cube_mm_mean_diff,31)
plt.gca().coastlines()
plt.title('P')
plt.show(block = False)

plt.figure(4)
qplt.contourf(ws_cube_mm_mean_diff,31)
plt.gca().coastlines()
plt.title('WS')
plt.show(block = False)

"""


import cartopy.crs as ccrs
import iris.plot as iplt

plt.close('all')
plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(10)

cmap1 = mpl_cm.get_cmap('RdBu_r')
gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)

ax1 = plt.subplot(gs[0:48,0:48],projection=ccrs.PlateCarree())
ax2 = plt.subplot(gs[0:48,52:100],projection=ccrs.PlateCarree())
ax3 = plt.subplot(gs[52:100,0:48],projection=ccrs.PlateCarree())
ax4 = plt.subplot(gs[52:100,52:100],projection=ccrs.PlateCarree())

lats = uas_cube_mm_mean_diff.coord('latitude').points
lons = vas_cube_mm_mean_diff.coord('longitude').points
x, y = np.meshgrid(lons, lats)


#################
#Precip. plot
#################

ax1.set_extent([-100, 50, 15, 90.0])
ax1.coastlines()
C1 = ax1.contourf(x,y,p_cube_mm_mean_diff.data * yearsec,np.linspace(-80,80,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax1.set_title('Precipitation change',  fontsize=12)
cb1 = plt.colorbar(C1,ax=ax1,orientation='horizontal',ticks=[-60,-30,0,30,60])
cb1.set_label('kg m$^{-2}$ yr$^{-1}$')

#################
#Evap. plot
#################

ax2.set_extent([-100, 50, 15, 90.0])
ax2.coastlines()
C1 = ax2.contourf(x,y,e_cube_mm_mean_diff.data * yearsec,np.linspace(-80,80,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax2.set_title('Evaporation change',  fontsize=12)
cb1 = plt.colorbar(C1,ax=ax2,orientation='horizontal',ticks=[-60,-30,0,30,60])
cb1.set_label('kg m$^{-2}$ yr$^{-1}$')

#################
#wind plot
#################

ax3.set_extent([-100, 50, 15, 90.0])
ax3.coastlines()
C1 = ax3.contourf(x,y,p_minus_e_cube_mm_mean_diff.data * yearsec,np.linspace(-80,80,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax3.set_title('Precip. - Evap. change',  fontsize=12)
cb1 = plt.colorbar(C1,ax=ax3,orientation='horizontal',ticks=[-60,-30,0,30,60])
cb1.set_label('kg m$^{-2}$ yr$^{-1}$')

#################
#wind plot
#################

# vector_crs = ccrs.Geodetic()
vector_crs = ccrs.PlateCarree()
ax4.set_extent([-100, 50, 15, 90.0])
ax4.coastlines()
u = uas_cube_mm_mean_diff.data
v = vas_cube_mm_mean_diff.data

magnitude = (u ** 2 + v ** 2) ** 0.5

C1 = ax4.contourf(x,y,ws_cube_mm_mean_diff.data,np.linspace(-0.4,0.4,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax4.set_title('Wind change',  fontsize=12)
ax4.quiver(x[:,180:360]-360, y[:,180:360], u[:,180:360], v[:,180:360],transform=vector_crs, regrid_shape=20)
ax4.quiver(x, y, u, v,transform=vector_crs, regrid_shape=20)
cb1 = plt.colorbar(C1,ax=ax4,orientation='horizontal',ticks=[-0.4,-0.2,0,0.2,0.4])
cb1.set_label('ms$^{-1}$')

plt.savefig('/home/ph290/Documents/figures/density_driver.png')
plt.show(block = False)

"""

import numpy as np
import matplotlib.pyplot as plt
import running_mean as rm
import seawater as sw
from seawater.library import T90conv
import scipy


f1 = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
f2 = '/data/NAS-ph290/ph290/misc_data/moffa-sanchez2014-poc.txt'
f3 = '/data/NAS-ph290/ph290/misc_data/sicre2011.txt'
f4 = '/data/NAS-ph290/ph290/misc_data/n_ice_densnormalised_no_hadcm3_csiro.txt'
f5 = '/data/NAS-ph290/ph290/misc_data/ortega_data.csv'

d_nao = np.genfromtxt(f5,delimiter = ',',skip_header = 4)
nao_age = d_nao[:,0]
loc = np.where((nao_age <= 1849) & (nao_age >= 1100))[0]
nao_age = nao_age[loc]
nao_data = d_nao[loc,1]

d_dens = np.genfromtxt(f4,delimiter = ',')
dens_age = d_dens[:,0]
loc = np.where((dens_age <= 1849) & (dens_age >= 1100))[0]
dens_age = dens_age[loc]
dens = d_dens[loc,1]
dens -= np.nanmin(dens)
dens/= np.nanmax(dens)

d_ni = np.genfromtxt(f1,delimiter = ',',skip_header = 1)
d_spg = np.genfromtxt(f2,delimiter = '\t',skip_header = 114)
d_ni_alk = np.genfromtxt(f3,delimiter = '          ',skip_header = 147,skip_footer=944-666)
ni_alk_age = d_ni_alk[:,0]
loc = np.where((ni_alk_age <= 1849) & (ni_alk_age >= 950))[0]
ni_alk_age = ni_alk_age[loc]
ni_alk = d_ni_alk[loc,1]
ni_alk = scipy.signal.detrend(ni_alk)
ni_alk2 = ni_alk - np.nanmin(ni_alk)
ni_alk2 /= np.nanmax(ni_alk2)

spg_age = d_spg[:,1]
loc = np.where((spg_age <= 1849) & (spg_age >= 950))[0]
spg_age = spg_age[loc]
spg_t = d_spg[loc,4]
spg_s = d_spg[loc,6]
spg_p = sw.dens(spg_s, spg_t, 0)
spg_p2 = spg_p - np.nanmin(spg_p)
spg_p2 /= np.nanmax(spg_p2)

spg_d18o = d_spg[loc,2]
spg_d18o2 = spg_d18o - np.nanmin(spg_d18o)
spg_d18o2 /= np.nanmax(spg_d18o2)


ni_age = d_ni[:,0]
loc = np.where(ni_age <= 1849)[0]
ni_age = ni_age[loc]
ni_d18o = d_ni[loc,1]
ni_d18o = scipy.signal.detrend(ni_d18o)
ni_d18o_rm = rm.running_mean(ni_d18o,20)

ni_d18o_rm -= np.nanmin(ni_d18o_rm)
ni_d18o_rm /= np.nanmax(ni_d18o_rm)

f = open('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt', 'r')
data = f.read()
lines = data.split('\n')
sf_age = []
sf_v = []
for line in lines:
    try:
        sf_age.append(float(line.split(',')[0]))
    except:
        sf_age.append(np.nan)
    try:
        sf_v.append(float(line.split(',')[1]))
    except:
        sf_v.append(np.nan)


sorting_index = np.argsort(spg_age)

spg_p2_interp = np.interp(ni_age,spg_age[sorting_index],spg_p2[sorting_index])




import numpy as np
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt

def butter_lowpass(cutoff, order=5):
    cutoff = 1.0/cutoff
    b, a = butter(order, cutoff, btype='low', analog=False)
    return b, a


def butter_highpass(cutoff, order=5):
    cutoff = 1.0/cutoff
    b, a = butter(order, cutoff, btype='high', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, order=5):
    b, a = butter_lowpass(cutoff, order=order)
    y = lfilter(b, a, data)
    cutoff = int(cutoff)
    y = np.roll(y,(cutoff/2) * -1)
    y[0:cutoff/2] = np.nan
    y[(cutoff/2) * -1::] = np.nan
    return y


def butter_highpass_filter(data, cutoff, order=5):
    b, a = butter_highpass(cutoff, order=order)
    y = lfilter(b, a, data)
    y[0:cutoff/2] = np.nan
    y[(cutoff/2) * -1::] = np.nan
    return y


# y = butter_lowpass_filter(nao_data, 50, 2)
# y2 = butter_highpass_filter(nao_data,100, 2)
#
# dens2 = butter_highpass_filter(dens,100, 2)
#









plt.close('all')
ax = []

#create upper subplot
ax.append(plt.subplot(111))
#plt.plot(spg_age,spg_p2,'y',lw=1)
# plt.plot(ni_age,spg_p2_interp,'b',lw=3)
# plt.plot(ni_age,spg_p2_interp+ni_d18o_rm,lw=3)



ax.append(ax[0].twinx())
# plt.plot(sf_age[:-1],rm.running_mean(np.array(sf_v[:-1]),20),'k',lw=3)
plt.plot(dens_age,rm.running_mean(np.array(dens),5),'k',lw=3)

# ax.append(ax[0].twinx())
# plt.plot(ni_age,ni_d18o_rm,'r',lw=3)

#ax.append(ax[0].twinx())
#plt.plot(ni_alk_age,rm.running_mean(ni_alk2,10)*-1.0,'g',lw=3)

ax.append(ax[0].twinx())
# plt.plot(nao_age,rm.running_mean(nao_data,10),'g',lw=3)
plt.plot(nao_age,rm.running_mean(nao_data,5),'g',lw=3)

# ax.append(ax[0].twinx())
# plt.plot(spg_age,spg_d18o2,'b',lw=3)

# ax.append(ax[0].twinx())
# plt.plot(expected_years,rm.running_mean(multi_model_mean_density,10),'k',lw=3)

# ax[0].set_ylim(-0,2.0)
# ax[0].set_ylim(0,1)
# ax[1].set_ylim(-0.15,0.15)


plt.show(block = False)


r_values = []
p_values = []

for i in range(100):
    x = np.flipud(nao_data.copy())
    y = dens
    x = rm.running_mean(x,i+2)
    y = rm.running_mean(y,i+2)
    loc = np.where((np.logical_not(np.isnan(x))) & (np.logical_not(np.isnan(y))))[0]
    x = x[loc]
    y = y[loc]
    xsort = np.argsort(x)
    x = x[xsort]
    y = y[xsort]
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
    r_values.append(r_value)
    p_values.append(p_value)


"""

lon1a = -48.7-5.0
lon2a = -48.7+5.0
lat1a = 57.5-5.0
lat2a = 57.5+5.0

lon1a = -43.0
lon2a = -64.0
lat1a = 55.0
lat2a = 65.0

directory= '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_lab_sea2/'
subprocess.call('mkdir '+directory,shell = True)
files = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/density_mixed_layer*JJA.nc')
subprocess.call('mkdir '+directory, shell=True)
for i,my_file in enumerate(files):
        print 'processing ',i,' in ',np.size(files)
        name = my_file.split('/')[-1]
        cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 -setrtomiss,0,1000 '+my_file, output = directory+name,  options = '-P 7')










start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_lab_sea2/'

models = model_names(input_directory)
models = list(models)
models.remove('CSIRO-Mk3L-1-2')
models.remove('HadCM3')

models = np.array(models)

multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()

appending  = 'normalised_no_hadcm3_csiro'
for i,model in enumerate(models):
        print model
        period = 'JJA'
        #       period = 'annual_mean'
        cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
        #try:
        cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
        #except:
        #       cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
        data = cube.data
        coord = cube.coord('time')
        dt = coord.units.num2date(coord.points)
        yrs = np.array([coord.units.num2date(value).year for value in coord.points])
        #smoothing_var = 1
        #if smoothing_var > 0:
#               data = gaussian_filter1d(data,smoothing_var)
        data2 = signal.detrend(data)
        data2 = data2-np.min(data2)
        data3 = data2/(np.max(data2))
        data3 -= np.nanmean(data3)
        #data3 = data2
        #data3 -= np.nanmean(data3)
        #window_type = 'boxcar'
        #data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
        for index,y_tmp in enumerate(expected_years):
                loc2 = np.where(yrs == y_tmp)
                if np.size(loc2) != 0:
                        multi_model_density[index,i] = data3[loc2]



multi_model_mean_density = np.nanmean(multi_model_density, axis = 1)
multi_model_std_density = np.nanstd(multi_model_density, axis = 1)
multi_model_mean_density -= np.nanmin(multi_model_mean_density)
multi_model_mean_density /= np.nanmax(multi_model_mean_density)

"""

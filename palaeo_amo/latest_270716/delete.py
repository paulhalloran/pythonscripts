

import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
	models = np.unique(models_tmp)
	return models

##########################################
# Mian part of code                      #
##########################################

################################
# produce area means if they don;t already exist
################################


################################
# Prepare the plots
################################



#start_year = 850
start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
# r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data_file = '/data/NAS-ph290/ph290/misc_data/Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,2]
tmp=scipy.signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)


#############################################################################################################################
# Models: remove linear trend and normalise variability, then put all models on same time axis and perform multi-model mean #
#############################################################################################################################


input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger6/'

input_directory1 = '/data/data1/ph290/cmip5/last1000/mld/deep_temperature_density/spatial_avg_larger6/'
# input_directory1 = '/data/data1/ph290/cmip5/last1000/mld/'
input_directory2 = '/data/data1/ph290/cmip5/last1000/mld/deep_salinity_density/spatial_avg_larger6/'
input_directory3 = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger6/'


# files = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep_salinity_density/density_mixed_layer*JJA_const_s.nc')

#NOTE in this directoryt THE CRAZY LOW DENSITIES are set TO MISSING DATA

models = model_names(input_directory)
models = list(models)
# models.remove('MIROC-ESM')
#models.remove('FGOALS-gl')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadGEM2-ES')
models.remove('HadCM3')

#models.remove('CCSM4')
#models.remove('FGOALS-s2')
#models.remove('MRI-CGCM3')
#models.remove('bcc-csm1-1')


# models.remove('CSIRO-Mk3L-1-2')
#models.remove('GISS-E2-R')
# models.remove('HadCM3')
#models.remove('HadGEM2-ES')
#models.remove('MIROC-ESM')
#models.remove('MPI-ESM-P')

# print '*******************************************'
# print '*******************************************'
# print ' As soon as processed bring MIROC BACK IN! '
# print '*******************************************'
# print '*******************************************'
# models.remove('MIROC-ESM') #temporarily until regridded...
#Note previously had two FGOALS models, sp removed one so a better ensmeble
#FGOALS-gl chosen, because there seems to be a problem calculating its mixed layer depths due to teh levels being upside down
models = np.array(models)

multi_model_density_s_const = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density_s_const[:] = np.NAN

multi_model_density_t_const = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density_t_const[:] = np.NAN

multi_model_std_density = multi_model_density.copy()

appending  = 'normalised_no_hadcm3_csiro'
for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	# cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	cube_a = iris.load_cube(input_directory1+'density_mixed_layer_'+model+'*_'+period+'_const_t.nc')
	# cube_a = iris.load_cube(input_directory1+'mixed_layer_depth_'+model+'_past1000_*_JJA.nc')
	cube_b = iris.load_cube(input_directory2+'density_mixed_layer_'+model+'*_'+period+'_const_s.nc')
	cube_c = iris.load_cube(input_directory3+'density_mixed_layer_'+model+'*_'+period+'.nc')
	#try:
	cube_a = cube_a.collapsed(['longitude', 'latitude'], iris.analysis.MEAN)
	cube_b = cube_b.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data_a = cube_a.data
	data_b = cube_b.data
	coord = cube_c.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2 = signal.detrend(data_a)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	#data3 = data2
	#data3 -= np.nanmean(data3)
 	#window_type = 'boxcar'
 	#data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	for index,y_tmp in enumerate(expected_years):
		loc2 = np.where(yrs == y_tmp)
		if np.size(loc2) != 0:
			multi_model_density_t_const[index,i] = data3[loc2]
	data2 = signal.detrend(data_b)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	#data3 = data2
	#data3 -= np.nanmean(data3)
 	#window_type = 'boxcar'
 	#data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	for index,y_tmp in enumerate(expected_years):
		loc2 = np.where(yrs == y_tmp)
		if np.size(loc2) != 0:
			multi_model_density_s_const[index,i] = data3[loc2]



#smoothing_var = 1
#if smoothing_var > 0:
#	for i,model in enumerate(models):
#		#multi_model_density[:,i] = rm.running_mean(multi_model_density[:,i],smoothing_var)
#		multi_model_density[:,i] = gaussian_filter1d(multi_model_density[:,i],smoothing_var)

multi_model_mean_density_t_const = np.nanmean(multi_model_density_t_const, axis = 1)
multi_model_mean_density_s_const = np.nanmean(multi_model_density_s_const, axis = 1)
# multi_model_std_density = np.nanstd(multi_model_density, axis = 1)




##########################################
# plotting                               #
##########################################


###



#uncomment if running for first time
#################

#################



def butter_bandpass(lowcut,  cutoff):
    order = 2
    low = 1/lowcut
    b, a = scipy.signal.butter(order, low , btype=cutoff,analog = False)
    return b, a

def low_pass_filter(cube,limit_years):
        b1, a1 = butter_bandpass(limit_years, 'low')
        output = scipy.signal.filtfilt(b1, a1, cube,axis = 0)
        return output

def high_pass_filter(cube,limit_years):
        b1, a1 = butter_bandpass(limit_years, 'high')
        output = scipy.signal.filtfilt(b1, a1, cube,axis = 0)
        return output



plt.close('all')

# upper_limit_years = 100.0
lower_limit_years = 3.0

y2 = multi_model_density_t_const.copy()
y2[:,:] = np.nan

for i in range(9):
	y = multi_model_density_t_const[:,i].copy()
	# y = high_pass_filter(y,upper_limit_years)
	y = low_pass_filter(y,lower_limit_years)
	y2[:,i] = y


y2 = np.nanmean(y2,axis=1)

output_ts = bivalve_data_initial
# output_ts = high_pass_filter(output_ts,upper_limit_years)
output_ts = low_pass_filter(output_ts,lower_limit_years)


fig, ax = plt.subplots(2,1,figsize=(6, 8))
ax[0].plot(bivalve_year,output_ts,'r',alpha = 0.5,linewidth = 2)
ax[0].plot(expected_years,y2*-3.0,'y',alpha = 0.5,linewidth = 2)

###


y2 = multi_model_density_s_const.copy()
y2[:,:] = np.nan

for i in range(9):
	y = multi_model_density_s_const[:,i].copy()
	# y = high_pass_filter(y,upper_limit_years)
	y = low_pass_filter(y,lower_limit_years)
	y2[:,i] = y


y2 = np.nanmean(y2,axis=1)

output_ts = bivalve_data_initial
# output_ts = high_pass_filter(output_ts,upper_limit_years)
output_ts = low_pass_filter(output_ts,lower_limit_years)


ax[1].plot(bivalve_year,output_ts,'r',alpha = 0.5,linewidth = 2)
ax[1].plot(expected_years,y2*-3.0,'g',alpha = 0.5,linewidth = 2)





plt.show(block = True)

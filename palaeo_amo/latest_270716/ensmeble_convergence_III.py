

import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import iris.coord_categorisation
import scipy
import scipy.stats
import glob
import os
from scipy.stats import gaussian_kde
import matplotlib.cm as mpl_cm
import scipy.signal
import running_mean as rm
import random

def model_names(directory):
        files = glob.glob(directory+'/*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


def ensemble_names(directory):
        files = glob.glob(directory+'/*.nc')
        ensembles_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        ensembles_tmp.append(file.split('/')[-1].split('_')[3].split('.')[0])
                        ensemble = np.unique(ensembles_tmp)
        return ensemble



#directory = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'
directory = '/data/NAS-ph290/ph290/cmip5/picontrol/regridded/'

models = model_names(directory)
ensembles = ensemble_names(directory)
ensembles = ensembles[0:1]

no_yrs = []
models2 = []

count = 0

for i,model in enumerate(models):
	for j,ensemble in enumerate(ensembles):
		print 'counting years in model: '+model
		try:
			try:
				cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','sea_surface_temperature')
			except:
				cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','surface_temperature')
			iris.coord_categorisation.add_year(cube, 'time', name='year')
			no_years = np.size(cube.coord('year').points)
			if no_years > 300:
				no_yrs.append(no_years)
				models2.append(model)
				count +=1
		except:
			print 'no data'

models2 = np.unique(models2)

no_yrs = np.array(no_yrs)
min_years = int(np.min(no_yrs))

print min_years

lon_west = -24.0
lon_east = -4.0
lat_south = 65.0
lat_north = 70.0

# lon_west = -75
# lon_east = -7.5
# lat_south = 0.0
# lat_north = 60.0

smoothing = 0 # NOTE SMOOTHING CODE REMOVED

model_ts = np.zeros([count,min_years-3-smoothing])
model_ts = np.zeros([count,min_years-3])

count2=0

for i,model in enumerate(models2):
    for j,ensemble in enumerate(ensembles):
        try:
            print 'processing model: '+model
            try:
            	cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','sea_surface_temperature')
            except:
            	cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','surface_temperature')
            cube = cube[1:min_years-2]
            #
            cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
            cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
            #
            cube_region.coord('latitude').guess_bounds()
            cube_region.coord('longitude').guess_bounds()
            grid_areas = iris.analysis.cartography.area_weights(cube_region)
            area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
            # area_avged_cube_anom = area_avged_cube - area_avged_cube.collapsed(['time'], iris.analysis.MEAN)
            # tmp =  scipy.signal.detrend(rm.running_mean(area_avged_cube_anom.data,smoothing)[smoothing/2:-1*smoothing/2])
            tmp =  scipy.signal.detrend(area_avged_cube.data)
            tmp = (tmp - np.min(tmp))
            model_ts[count2,:] = tmp / np.max(tmp)
            count2 += 1
        except:
            print 'no data again'

mm_mean = np.mean(model_ts,axis = 0)



number_runs = 200

xs=np.zeros([number_runs,count])
ys=np.zeros([number_runs,count])

for count3 in range(number_runs):
    print count3
    for i in range(count):
        subset = np.zeros([i+1,min_years-3-smoothing])
        subset = np.zeros([i+1,min_years-3])
        # my_list = np.random.randint(np.shape(model_ts)[0], size=i+1)
        my_list = random.sample(range(np.shape(model_ts)[0]), i+1)
        for j in range(i+1):
            subset[j,:] = model_ts[my_list[j],:].copy()
        subset_mean = np.mean(subset,axis = 0)
        # r = scipy.stats.pearsonr(subset_mean, mm_mean)
        # r2 = r[0]*r[0]
        xs[count3,i] = i
        # ys[count3,i] = r[0]
        ys[count3,i] =  np.std(subset_mean)
        #plt.scatter(i-1,r[0])

xs2 = np.nanmean(xs,axis = 0)
ys2 = np.nanmean(ys,axis = 0)


xs = xs.reshape(number_runs * count)
ys = ys.reshape(number_runs * count)


xy = np.vstack([xs,ys])
z = gaussian_kde(xy)(xy)


# plt.xlabel('ensemble size')
# plt.xlabel('r')
# plt.show()


cmap = mpl_cm.get_cmap('gist_yarg')




plt.close('all')
fig, ax = plt.subplots(2,1,figsize=(6, 8))
ax[0].scatter(xs+1, ys, c=z, s=50, edgecolor='',cmap=cmap)
ax[0].plot(xs2+1,ys2,'r',label = 'ensemble mean')
ax[0].plot([8,8],[-0.2,1],'k',lw=3)
ax[0].set_xlabel('Ensemble Size')
ax[0].set_ylabel('Magnitude of 1$\sigma$')
ax[0].set_ylim([0,0.20])
ax[0].set_xlim([1,32])
ax[0].scatter(-100,-100, s=50, edgecolor='',color='k',alpha=0.5,label = 'ensemble member')
ax[0].legend(loc='upper right', fancybox=True, framealpha=0.5, fontsize=8)


for i in range(32):
    if i == 0:
        ax[1].plot(model_ts[i,:],'k',lw=1,alpha = 0.1,label = 'ensemble member')
    else:
        ax[1].plot(model_ts[i,:],'k',lw=1,alpha = 0.1)

y1 = 0.5+np.max(ys2)
y2 = 0.5-np.max(ys2)
ax[1].plot([0,300],[y1,y1],'k--',lw=2,alpha = 0.5,label = 'ensemble members +/- 1$\sigma$')
ax[1].plot([0,300],[y2,y2],'k--',lw=2,alpha = 0.5)

ax[1].plot(mm_mean,'r',lw=2,alpha=0.8,label = 'multi-model mean')
y1 = 0.5+np.std(mm_mean)
y2 = 0.5-np.std(mm_mean)
ax[1].plot([0,300],[y1,y1],'r--',lw=2,alpha=0.5,label = 'multi-model mean +/- 1$\sigma$')
ax[1].plot([0,300],[y2,y2],'r--',lw=2,alpha=0.5)

ax[1].set_xlim([0,300])
ax[1].set_ylim([-0.2,1.2])

ax[1].set_ylabel('Normalised\ninternal variability')
ax[1].set_xlabel('Year of run')

plt.legend(loc='upper right', fancybox=True, framealpha=0.5, fontsize=8,ncol=2)

plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/ensemble_size_IV.svg')
plt.show(block = False)

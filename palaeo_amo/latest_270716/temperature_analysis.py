'''
/home/ph290/Documents/python_scripts/palaeo_amo/latest_270716
'''



import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import scipy
from scipy import signal
import pickle

start_year = 950
end_year = 1850

r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=scipy.signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] < end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)


with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VII.pickle', 'r') as f:
    models_other_list,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files = pickle.load(f)


directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
density_dir = '/data/data1/ph290/cmip5/last1000/mld/deep_salinity_density/spatial_avg_larger6_varying_t_s/'
density_dir2 = '/data/data1/ph290/cmip5/last1000/mld/deep_temperature_density/spatial_avg_larger6/'
density_dir3 = '/data/data1/ph290/cmip5/last1000/mld/deep_salinity_density/spatial_avg_larger6/'

models = ['bcc-csm1-1','CCSM4','FGOALS-s2','GISS-E2-R','HadGEM2-ES','MIROC-ESM','MPI-ESM-P','MRI-CGCM3']
models = ['bcc-csm1-1','CCSM4','FGOALS-s2','GISS-E2-R','HadGEM2-ES','MIROC-ESM','MPI-ESM-P','MRI-CGCM3','HadCM3','CSIRO-Mk3L-1-2']

lon_west = -75
lon_east = -7.5
lat_south = 0.0
lat_north = 60.0

lon_west = -24
lon_east = -4
lat_south = 60
lat_north = 65.0


smoothings = [0,20]
# ,45,50,55,60,65,70,75,80,85,90,95,100]

model_ts = np.zeros([np.size(smoothings),np.size(models),1850-950])
model_ts[:,:] = np.nan
model_ts_salinity = np.zeros([np.size(smoothings),np.size(models),1850-950])
model_ts_salinity[:,:] = np.nan
mm_means = np.zeros([np.size(smoothings),1850-950])
mm_means[:,:] = np.nan


model_ts_density = np.zeros([np.size(smoothings),np.size(models),1850-950])
mm_means_density = np.zeros([np.size(smoothings),1850-950])
mm_means_density[:,:] = np.nan

for j,smoothing in enumerate(smoothings):
	print 'smoothing = '+str(smoothing)
	for i,model in enumerate(models):
		#####################
		# N. Iceland SSTs
		#####################
		ensemble = 'r1i1p1'
		if model == 'GISS-E2-R':
			ensemble = 'r1i1p121'
		print 'processing model: '+model
		try:
			cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','sea_surface_temperature')
		except:
			print 'wrong variable name'
		try:
			cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','surface_temperature')
		except:
			print 'wrong variable name'
		try:
			cube = iris.load_cube(directory+'*'+model+'_tos*_'+ensemble+'_*','sea_water_potential_temperature')
		except:
			print 'wrong variable name'
		try:
			cube = cube.collapsed(['depth'], iris.analysis.MEAN)
		except:
			print 'no depth coord'
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		year = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where((year >= 950) & (year < 1850))[0]
		cube = cube[loc]
		#
		cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
		cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		#
		cube_region.coord('latitude').guess_bounds()
		cube_region.coord('longitude').guess_bounds()
		grid_areas = iris.analysis.cartography.area_weights(cube_region)
		area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		tmp =  scipy.signal.detrend(area_avged_cube.data)
		if smoothing > 0:
			tmp = rm.running_mean(tmp,smoothing)
		tmp = (tmp - np.nanmin(tmp))
		model_ts[j,i,:] -= tmp / np.nanmax(tmp)
		#####################
		# N. Iceland box SSs
		#####################
		ensemble = 'r1i1p1'
		if model == 'GISS-E2-R':
			ensemble = 'r1i1p121'
		print 'processing model: '+model
		try:
			cube = iris.load_cube(directory+'*'+model+'_sos*_'+ensemble+'_*')
		except:
			print 'wrong variable name'
		try:
			cube = cube.collapsed(['depth'], iris.analysis.MEAN)
		except:
			print 'no depth coord'
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		year = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where((year >= 950) & (year < 1850))[0]
		cube = cube[loc]
		#
		cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
		cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		#
		cube_region.coord('latitude').guess_bounds()
		cube_region.coord('longitude').guess_bounds()
		grid_areas = iris.analysis.cartography.area_weights(cube_region)
		area_avged_cube = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)
		tmp =  scipy.signal.detrend(area_avged_cube.data)
		if smoothing > 0:
			tmp = rm.running_mean(tmp,smoothing)
		tmp = (tmp - np.nanmin(tmp))
		model_ts_salinity[j,i,:] -= tmp / np.nanmax(tmp)
		#####################
		# N. Iceland density
		#####################
		f1 = density_dir+'density_mixed_layer_'+model+'_past1000_r*_JJA.nc'
		cube2 = iris.load_cube(f1).collapsed(['latitude','longitude'],iris.analysis.SUM)
		#Not the following is simply to get around  abug, where FGOALS years haveen labeled by the processing as sequential months rather than sequetial years
		f2 = density_dir2+'density_mixed_layer_'+model+'_past1000_r*_JJA_const_t.nc'
		cube3 = iris.load_cube(f2).collapsed(['latitude','longitude'],iris.analysis.SUM)
		coord = cube3.coord('time')
		dt = coord.units.num2date(coord.points)
		year = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where((year >= 950) & (year < 1850))
		cube2 = cube2[loc]
		tmp = signal.detrend(cube2.data)
		if smoothing > 0:
			tmp = rm.running_mean(tmp,smoothing)
		tmp = (tmp - np.nanmin(tmp))
		model_ts[j,i,:] -= tmp / np.nanmax(tmp)
		model_ts_density[j,i,:] = tmp
		#####################
		# AMOC
		#####################


	mm_means[j,:] = np.nanmean(model_ts[j,:,:],axis = 0)
	mm_means_density[j,:] = np.nanmean(model_ts_density[j,:,:],axis = 0)


model_ts_amoc = np.zeros([np.size(smoothings),np.size(models),1850-950])
mm_means_amoc = np.zeros([np.size(smoothings),1850-950])
mm_means_amoc[:] = np.nan
model_ts_amoc[:] = np.nan



#####################
# AMOC
#####################

pmip3_str={}

for i,model in enumerate(models_other_list):
		pmip3_str[model]={}
		pmip3_str[model]['data'] = max_strm_fun_26[i]
		pmip3_str[model]['years'] = model_years[i]


year_list = np.arange(950,1850)

for j,smoothing in enumerate(smoothings):
	print 'smoothing = '+str(smoothing)
	for i,model in enumerate(models):
		loc = np.where((pmip3_str[model]['years'] >= 950) & (pmip3_str[model]['years'] < 1850))
		tmp = pmip3_str[model]['data'][loc]
		tmp_year = pmip3_str[model]['years'][loc]
		if smoothing > 0:
			tmp = rm.running_mean(tmp,smoothing)
		tmp = (tmp - np.nanmin(tmp))
		tmp -= tmp / np.nanmax(tmp)
		for k,year in enumerate(tmp_year):
			loc2 = np.where(year_list == year)
			model_ts_amoc[j,i,loc2[0]] = tmp[k]
	mm_means_amoc[j,:] = np.nanmean(model_ts_amoc[j,:,:],axis = 0)



corr_matrix = np.zeros([np.size(smoothings),np.size(models)])
corr_matrix_density = np.zeros([np.size(smoothings),np.size(models)])
corr_matrix_amoc = np.zeros([np.size(smoothings),np.size(models)])



for j,smoothing in enumerate(smoothings):
	for i,model in enumerate(models):
		#####################
		# AMO box SSTs
		#####################
		x = model_ts[j,i,:]
		y = mm_means[j,:]
		loc = np.where((np.isfinite(x)) & (np.isfinite(y)))
		x = x[loc]
		y = y[loc]
		corr_matrix[j,i] = scipy.stats.pearsonr(x,y)[0]
		#####################
		# N. Iceland density
		#####################
		x = model_ts_density[j,i,:]
		y = mm_means_density[j,:]
		loc = np.where((np.isfinite(x)) & (np.isfinite(y)))
		x = x[loc]
		y = y[loc]
		corr_matrix_density[j,i] = scipy.stats.pearsonr(x,y)[0]
		#####################
		# AMOC
		#####################
		x = model_ts_amoc[j,i,:]
		y = mm_means_amoc[j,:]
		loc = np.where((np.isfinite(x)) & (np.isfinite(y)))
		x = x[loc]
		y = y[loc]
		corr_matrix_amoc[j,i] = scipy.stats.pearsonr(x,y)[0]




plt.close('all')
plt.plot(bivalve_year,bivalve_data_initial * -1.0,'k',lw=1)
plt.plot(np.arange(950,1850),(mm_means_amoc[-2,:])-0.8)
plt.show()

"""

#models = ['bcc-csm1-1','CCSM4','FGOALS-s2','GISS-E2-R','HadGEM2-ES','MIROC-ESM','MPI-ESM-P','MRI-CGCM3']
volc_forcing = ['Gao et al. 2008','Gao et al. 2008','Gao et al. 2008','Crowley & Unterman, 2013','Crowley & Unterman, 2013','Crowley & Unterman, 2013','Crowley & Unterman, 2013','Gao et al. 2008']
solar_forcing = ['Vieira and Solanki, 2009','Vieira and Solanki, 2009','Vieira and Solanki, 2009','Steinhilber et al., 2009','Steinhilber et al., 2009','Delaygue and Bard, 2009','Vieira and Solanki, 2009','Delaygue and Bard, 2009']

colours = [None] * np.size(models)
colours2 = [None] * np.size(models)
marker = [None] * np.size(models)

# for i,vf in enumerate(volc_forcing):
# 	if vf == 'Gao et al. 2008':
# 		colours[i] = 'r'
# 	if vf == 'Crowley & Unterman, 2013':
# 		colours[i] = 'b'

for i,vf in enumerate(volc_forcing):
	if vf == 'Gao et al. 2008':
		marker[i] = '--'
	if vf == 'Crowley & Unterman, 2013':
		marker[i] = '-'


for i,vf in enumerate(solar_forcing):
	if vf == 'Vieira and Solanki, 2009':
		colours2[i] = 'red'
	if vf == 'Steinhilber et al., 2009':
		colours2[i] = 'green'
	if vf == 'Delaygue and Bard, 2009':
		colours2[i] = 'blue'

plt.close('all')

fig = plt.figure(figsize=(7, 15))
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.90,top=0.85)
ax1 = plt.subplot(gs[0:28,0:100])
ax2 = plt.subplot(gs[35:63,0:100])
ax3 = plt.subplot(gs[70:98,0:100])
# ax4 = plt.subplot(gs[0:28,55:100])
# ax5 = plt.subplot(gs[35:63,55:100])
# ax6 = plt.subplot(gs[70:98,55:100])

#volc

for i,model in enumerate(models):
	ax1.scatter(smoothings,corr_matrix[:,i],color = colours2[i],label=solar_forcing[i])
	ax1.plot(smoothings,corr_matrix[:,i],color = colours2[i],linestyle=marker[i],label=volc_forcing[i],lw=2)

ax1.legend(fontsize = 'small')


ax1.set_title('AMV box SSTs')
ax1.set_ylabel('correlation coefficient')

for i,model in enumerate(models):
	ax2.scatter(smoothings,corr_matrix_density[:,i],color = colours2[i])
	ax2.plot(smoothings,corr_matrix_density[:,i],color = colours2[i],linestyle=marker[i],lw=2)

ax2.set_title('N. Iceland density')
ax2.set_ylabel('correlation coefficient')

for i,model in enumerate(models):
	ax3.scatter(smoothings,corr_matrix_amoc[:,i],color = colours2[i])
	ax3.plot(smoothings,corr_matrix_amoc[:,i],color = colours2[i],linestyle=marker[i],lw=2)

ax3.set_title('AMOC')
ax3.set_ylabel('correlation coefficient')
ax3.set_xlabel('smoothing (years)')

#solar
#
# for i,model in enumerate(models):
# 	ax4.scatter(smoothings,corr_matrix[:,i],color = colours2[i])
# 	ax4.plot(smoothings,corr_matrix[:,i],color = colours2[i],label=solar_forcing[i])
#
# ax4.legend(fontsize = 'small')
#
# ax4.set_title('AMV box SSTs')
# ax4.set_ylabel('correlation coefficient')
#
# for i,model in enumerate(models):
# 	ax5.scatter(smoothings,corr_matrix_density[:,i],color = colours2[i])
# 	ax5.plot(smoothings,corr_matrix_density[:,i],color = colours2[i])
#
# ax5.set_title('N. Iceland density')
# ax5.set_ylabel('correlation coefficient')
#
# for i,model in enumerate(models):
# 	ax6.scatter(smoothings,corr_matrix_amoc[:,i],color = colours2[i])
# 	ax6.plot(smoothings,corr_matrix_amoc[:,i],color = colours2[i])

ax6.set_title('AMOC')
ax6.set_ylabel('correlation coefficient')
ax6.set_xlabel('smoothing (years)')

ax1.set_xlim(0,40)
ax2.set_xlim(0,40)
ax3.set_xlim(0,40)
# ax4.set_xlim(0,40)
# ax5.set_xlim(0,40)
# ax6.set_xlim(0,40)

ax1.set_ylim(0,1)
ax2.set_ylim(0,1)
ax3.set_ylim(0,1)
# ax4.set_ylim(0,1)
# ax5.set_ylim(0,1)
# ax6.set_ylim(0,1)


plt.savefig('/home/ph290/Documents/figures/correlatoin_with_mean.svg')
plt.show(block = False)

"""

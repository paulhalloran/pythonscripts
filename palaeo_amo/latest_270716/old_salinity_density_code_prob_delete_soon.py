
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import scipy
import gsw

TESTING = False

#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names_thetao(directory):
	files = glob.glob(directory+'thetao_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models


def model_names_so(directory):
	files = glob.glob(directory+'so_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models

'''
Defining directory locations, variable locatoins etc.
You may well need to edit the text within the quotation marks to adapt the script to work with your data
NOTE - this presently seems to be giving very shallow MLDs - check.
'''


'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space1 = '/data/data1/ph290/tmp/'
temporary_file_space2 = '/data/data0/ph290/tmp/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/NAS-ph290/ph290/cmip5/past1000/'
#Directories where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/data1/ph290/cmip5/last1000/mld/deep_temperature_density/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiment = 'past1000'
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.
time_period = 'Omon'

#lats and lons for analysis:
lon1 = -180
lon2 = 180
lat1 = 25
lat2 = 90

'''
Main bit of code follows...
'''

models1 = model_names_thetao(input_directory)
models2 = model_names_so(input_directory)
models = list(np.intersect1d(np.array(models1),np.array(models1)))
models.remove('FGOALS-gl')
models = np.array(models)

#########
# MLD calculation (currently temperature based, but coudl easily change this to density)
#########

max_lev = 1000.0

season = 'JJA'


for model in models:
# model = models[0]
	ensemble = 'r1i1p1'
	if model == 'GISS-E2-R':
		ensemble = 'r1i1p121'
	print model
# 	test1 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_annual_mean.nc'))
# 	test2 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_annual_mean.nc'))
	test3 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+season+'.nc'))
# 	test4 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[0]+'.nc'))
# 	test5 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[1]+'.nc'))
# 	test6 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[1]+'.nc'))
# 	test7 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[2]+'.nc'))
# 	test8 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[2]+'.nc'))
# 	test9 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[3]+'.nc'))
# 	test10 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[3]+'.nc'))
# 	testing = test1 + test2 + test3 + test4 + test5 + test6 + test7 + test8 + test9 + test10
	if test3 == 0:
		#cleaning up...
		# subprocess.call('rm '+temporary_file_space1+'*.nc', shell=True)
		# subprocess.call('rm '+temporary_file_space2+'*.nc', shell=True)
		# subprocess.call('rm '+temporary_file_space3+'*.nc', shell=True)
		temp_file1 = str(uuid.uuid4())+'.nc'
		temp_file1a = str(uuid.uuid4())+'.nc'
		temp_file1z = str(uuid.uuid4())+'.nc'
		temp_file1x = str(uuid.uuid4())+'.nc'
		temp_file1y = str(uuid.uuid4())+'.nc'
		temp_file1b = str(uuid.uuid4())+'.nc'
		temp_file2 = str(uuid.uuid4())+'.nc'
		temp_file2a = str(uuid.uuid4())+'.nc'
		temp_file2b = str(uuid.uuid4())+'.nc'
		temp_file2c = str(uuid.uuid4())+'.nc'
		temp_file3 = str(uuid.uuid4())+'.nc'
		temp_file3b = str(uuid.uuid4())+'.nc'
		temp_file4 = str(uuid.uuid4())+'.nc'
		temp_file5 = str(uuid.uuid4())+'.nc'
		temp_file6 = str(uuid.uuid4())+'.nc'
		files1 = glob.glob(input_directory+'thetao_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
		files2 = glob.glob(input_directory+'so_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
##################################################
# uncomment for testing with just the 1st file   #
##################################################
		if TESTING:
			files1 = files1[0:1]
			files2 = files2[0:1]
##################################################
		#reads in the files to process
		sizing1 = np.size(files1)
		sizing2 = np.size(files2)
		#checks that we have some files to work with for this model, experiment and variable
		if not ((sizing1 == 0) | (sizing2 == 0)):
			#######################################
			#             thetao                  #
			#######################################
			print 'reading in: thetao '+model
			tmp = []
			for file in files1:
				tmp.append(int(file.split('_')[-1].split('-')[0]))
			order = np.argsort(tmp)
			files1_joined = ' '.join(np.array(files1)[order])
			levels = np.array(str(cdo.showlevel(input = files1[0])[0]).split(' ')).astype(np.float)
			loc = np.where(levels < max_lev)
			levels = levels[loc]
			levels = ','.join(['%.5f' % num for num in levels])
			if sizing1 > 1:
				#if the data is split across more than one file, it is combined into a single file for ease of processing
				#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
				print 'merging thetao files and selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
				#merge together different files1 from the same experiment
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				except:
					pass
				#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files1 in right order until otherwise tested).
				cdo.select('level='+levels+',name=thetao ',input=files1_joined, output = temporary_file_space2+temp_file1a,  options = '-P 7')
				cdo.selseas(season, input = '-seasmean '+ temporary_file_space2+temp_file1a, output = temporary_file_space1+temp_file1,  options = '-P 7')
				subprocess.call('rm '+temporary_file_space2+temp_file1a, shell=True)
			if sizing1 == 1:
				print 'No need to merge files1, but selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
				cdo.selseas(season, input = '-seasmean '+ files1[0], output = temporary_file_space1+temp_file1a,  options = '-P 7')
				#min_lev = str(cdo.showlevel(input = files1[0])[0]).split(' ')[0]
				if TESTING:
					cdo.select('level='+levels+',name=thetao -seltimestep,1,2,3 ',input = temporary_file_space1+temp_file1a, output = temporary_file_space1+temp_file1)
				else:
					cdo.select('level='+levels+',name=thetao',input = files1[0], output = temporary_file_space1+temp_file1)
				subprocess.call('rm '+temporary_file_space1+temp_file1a, shell=True)
			new_levs = np.array([float(str(x)) for x in cdo.showlevel(input = temporary_file_space1+temp_file1)[0].split(' ')])
			if new_levs[0] > new_levs[-1]:
				cdo.invertlev(input = temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file6)
				subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				subprocess.call('mv '+temporary_file_space2+temp_file6+' '+temporary_file_space1+temp_file1, shell=True)
				subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
			if model == 'MRI-CGCM3':
				#Note this is added because MRI-CGCM3 uses 0.0 as missing data. Iris does not appear to be able to handle missing values of 0.0, so setting to a different cdo default value
				print 'resetting MRI-CGCM3s missing data value in SO fields'
				subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
				cdo.setctomiss('0.0',input = temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file6, options = '-m -9e+33 -P 7')
				subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				subprocess.call('mv '+temporary_file_space2+temp_file6+' '+temporary_file_space1+temp_file1, shell=True)
			#######################################
			#                 SO                 #
			#######################################
			print 'reading in: so '+model
			#if the data is split across more than one file, it is combined into a single file for ease of processing
			#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
			tmp = []
			for file in files2:
				tmp.append(int(file.split('_')[-1].split('-')[0]))
			order = np.argsort(tmp)
			files2_joined = ' '.join(np.array(files2)[order])
			levels = np.array(str(cdo.showlevel(input = files2[0])[0]).split(' ')).astype(np.float)
			loc = np.where(levels < max_lev)
			levels = levels[loc]
			levels = ','.join(['%.5f' % num for num in levels])
			if sizing2 > 1:
				print 'merging so files and selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
				#merge together different files from the same experiment
				try:
					subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
				except:
					pass
				#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files in right order until otherwise tested).
				cdo.select('level='+levels+',name=so ',input=files2_joined, output = temporary_file_space1+temp_file2a,  options = '-P 7')
				cdo.selseas(season, input = '-seasmean '+ temporary_file_space1+temp_file2a, output = temporary_file_space2+temp_file2,  options = '-P 7')
				subprocess.call('rm '+temporary_file_space1+temp_file2a, shell=True)
			if sizing2 == 1:
				print 'No need to merge files, but selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
				cdo.selseas(season, input = '-seasmean '+ files2[0], output = temporary_file_space2+temp_file2a,  options = '-P 7')
				#min_lev = str(cdo.showlevel(input = files2[0])[0]).split(' ')[0]
				if TESTING:
					cdo.select('level='+levels+',name=so -seltimestep,1,2,3 ',input = temporary_file_space2+temp_file2a, output = temporary_file_space2+temp_file2)
			subprocess.call('rm '+temporary_file_space2+temp_file2a, shell=True)
			new_levs = np.array([float(str(x)) for x in cdo.showlevel(input = temporary_file_space2+temp_file2)[0].split(' ')])
			if new_levs[0] > new_levs[-1]:
				subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
				cdo.invertlev(input = temporary_file_space2+temp_file2, output = temporary_file_space2+temp_file6)
				subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
				subprocess.call('mv '+temporary_file_space2+temp_file6+' '+temporary_file_space2+temp_file2, shell=True)
			if model == 'MRI-CGCM3':
				#Note this is added because MRI-CGCM3 uses 0.0 as missing data. Iris does not appear to be able to handle missing values of 0.0, so setting to a different cdo default value
				print 'resetting MRI-CGCM3s missing data value in SO fields'
				subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
				cdo.setctomiss('0.0',input = temporary_file_space2+temp_file2, output = temporary_file_space2+temp_file6,options = '-m -9e+33 -P 7')
				subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
				subprocess.call('mv '+temporary_file_space2+temp_file6+' '+temporary_file_space2+temp_file2, shell=True)
			#######################################
			#               density               #
			#######################################
			print 'preparing files for density calculation'
			#cdo.setname('to', input = '-subc,273.15 '+ temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file2,  options = '-P 7')
			#cdo.setname('sao', input = temporary_file_space3+temp_file2, output = temporary_file_space2+temp_file4,  options = '-P 7')
			#tmp = ' '.join([temporary_file_space3+temp_file1,temporary_file_space3+temp_file2])
			####cdo replace delete.nc -duplicate,6 -timmean delete.nc delete4.nc
			print 'counting number of timesteps'
			no_t_steps = cdo.ntime(input = temporary_file_space2+temp_file2)[0].encode('ascii', 'ignore')
			print 'doing normal merge'
			cdo.merge(input = '-setname,tho, -subc,273.15 -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+ temporary_file_space1+temp_file1 + ' -setname,s, -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space2+temp_file2, output = temporary_file_space1+temp_file2,  options = '-P 7')
			print 'doing temperature manipulation merge'
			subprocess.call(['cdo -P 7 duplicate,'+no_t_steps+' -timmean -setname,tho -subc,273.15 -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space1+temp_file1+' '+temporary_file_space1+temp_file1z], shell=True)
			#cdo.duplicate(no_t_steps, input= '-timmean -setname,tho, -subc,273.15 -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space1+temp_file1, output = temporary_file_space1+temp_file1z,  options = '-P 7')
			subprocess.call(['cdo -P 7 setname,s -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space2+temp_file2+' '+temporary_file_space2+temp_file5], shell=True)
			# cdo.setname(input = '-setname,s, -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space2+temp_file2, output = temporary_file_space2+temp_file5,  options = '-P 7')
			# cdo.merge(input = '-setname,tho, -subc,273.15 -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+ temporary_file_space1+temp_file1 + ' -replace '+temporary_file_space2+temp_file2+' '+temporary_file_space1+temp_file1z, output = temporary_file_space1+temp_file2c,  options = '-P 7')
			# cdo.merge(input = temporary_file_space1+temp_file1z+' '+temporary_file_space2+temp_file5, output = temporary_file_space1+temp_file2c,  options = '-P 7')
			subprocess.call(['cdo -P 7 merge '+temporary_file_space1+temp_file1z+' '+temporary_file_space2+temp_file5+' '+temporary_file_space1+temp_file2c], shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file5, shell=True)
			#desubprocess.call('rm '+temporary_file_space2+temp_file2b, shell=True)
			#cdo.merge(input = tmp, output = temporary_file_space3+temp_file3,  options = '-P 7')
			#cdo.rhopot(0,input = temporary_file_space3+temp_file1, output = temporary_file_space1+temp_file2,  options = '-P 7')
			subprocess.call('rm '+temporary_file_space2+temp_file1, shell=True)
			print 'calculating density (using potential temperatures as criteria is potential density) '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
			cdo.rhopot(0,input = '-adisit '+ temporary_file_space1+temp_file2, output = temporary_file_space2+temp_file1,  options = '-P 7')
			cdo.rhopot(0,input = '-adisit '+ temporary_file_space1+temp_file2c, output = temporary_file_space2+temp_file1b,  options = '-P 7')
			subprocess.call('rm '+temporary_file_space1+temp_file2c, shell=True)
			#Looks like the potential to in-situe conversin for CSIRO-Mk3L-1-2 is not doing anything here... Maybe it needs pressure?
			######################
			#  annual mean MLD   #
			######################
# 			test1 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_annual_mean.nc'))
# 			test2 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_annual_mean.nc'))
# 			if test1 + test2 < 2:
# 				print 'calculating annual mean for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
# 				cdo.yearmean(input = temporary_file_space1+temp_file2, output = temporary_file_space2+temp_file3,  options = '-P 7')
# 				# = note this is not added at the moment because was going to be difficult to identify the box size after regridding... -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '
# 				density_cube = iris.load_cube(temporary_file_space2+temp_file3)
# 				#density_cube = iris.load_cube(temporary_file_space2+'e394a48e-ce8e-4955-af9b-681b111ee9fc.nc')
# 				depth_cube = density_cube[0].copy()
# 				try:
# 					depth_cube.data = ma.masked_all_like(density_cube[0].data.data).copy()
# 				except:
# 					depth_cube.data = ma.masked_all_like(density_cube[0].data).copy()
# 				try:
# 					density_cube.coord('depth')
# 					depths = depth_cube.coord('depth').points
# 				except:
# 					depths = depth_cube.coord('ocean sigma over z coordinate').points
# 					#for memorys sake, do one year at a time..
# 				shape = np.shape(density_cube)
# 				depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[shape[3],shape[2],1]),0,2))
# 				try:
# 					density_cube.coord('depth')
# 					MLD_out = density_cube.extract(iris.Constraint(depth = np.min(depth_cube.data)))
# 					MLD_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
# 					surface_density_out = MLD_out.copy()
# 					surface_density_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
# 				except:
# 					MLD_out = density_cube[:,0,:,:]
# 					surface_density_out = MLD_out.copy()
# 					MLD_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
# 					surface_density_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
# 				MLD_out_data = MLD_out.data.copy()
# 				surface_density_data = surface_density_out.data.copy()
# 				print'calculating mixed layer depths'+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
# 				for i in range(np.shape(MLD_out)[0]):
# 					density_tmp = density_cube[i].copy()
# 					density_diff = density_tmp[0,:,:].copy()
# 					density_diff2 = density_tmp.copy()
# 					density_diff += 0.125 # Levitus (1982) density criteria
# 					density_diff3 = np.tile(density_diff.data,[np.shape(density_cube.data[i])[0],1,1])
# 					idx_mld = density_cube[i].data <= density_diff3
# 					MLD = ma.masked_all_like(density_tmp.data)
# 					MLD.data[:] = np.NAN
# 					MLD.mask = density_cube[i].data.mask.copy()
# 					MLD.data[idx_mld] = depth_cube.data[idx_mld]
# 					MLD = np.ma.masked_invalid(MLD)
# 					MLD_out_data[i,:,:] = np.ma.max(MLD,axis=0)
# 					surface_density = ma.masked_all_like(density_tmp.data)
# 					surface_density.data[:] = np.NAN
# 					surface_density.mask = density_cube[i].data.mask.copy()
# 					surface_density.data[idx_mld] = density_tmp.data[idx_mld]
# 					surface_density = np.ma.masked_invalid(surface_density)
# 					thicknesses = MLD.copy()
# 					thicknesses[1::,:,:] = MLD[1::,:,:] - MLD[0:-1,:,:]
# 					surface_density_data[i,:,:] = np.ma.sum(surface_density * thicknesses,axis=0) / np.ma.max(MLD,axis=0)
# 				MLD_out.data = MLD_out_data
# 				surface_density_out.data = surface_density_data
# 				iris.fileformats.netcdf.save(MLD_out,output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_annual_mean.nc')
# 				iris.fileformats.netcdf.save(surface_density_out,output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_annual_mean.nc')
			###########################
			#  seasonal meaned MLD    #
			###########################
			subprocess.call('rm '+temporary_file_space1+temp_file2, shell=True)
			# = note this is not added at the moment because was going to be difficult to identify the box size after regridding... -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '
			density_cube = iris.load_cube(temporary_file_space2+temp_file1)
			density_cube_const_t = iris.load_cube(temporary_file_space2+temp_file1b)
			#cdo.seltimestep('1',input = '-select,level='+levels+',name=thetao -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+files1[0], output = temporary_file_space1+temp_file6)
			cdo.select('level='+levels+',name=so,timestep=1',input = files2[0], output = temporary_file_space1+temp_file6)
			try:
				os.remove(temporary_file_space2+temp_file6)
			except:
				pass
			cdo.sellonlatbox(str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2),input = temporary_file_space1+temp_file6, output = temporary_file_space2+temp_file6,  options = '-P 7')
			os.remove(temporary_file_space1+temp_file6)
			if model == 'MRI-CGCM3':
				#Note this is added because MRI-CGCM3 uses 0.0 as missing data. Iris does not appear to be able to handle missing values of 0.0, so setting to a different cdo default value
				print 'resetting MRI-CGCM3s missing data value in SO fields'
				subprocess.call('rm '+temporary_file_space1+temp_file5, shell=True)
				cdo.setctomiss('0.0',input = temporary_file_space2+temp_file6, output = temporary_file_space1+temp_file5,options = '-m -9e+33 -P 7')
				subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
				subprocess.call('mv '+temporary_file_space1+temp_file5+' '+temporary_file_space2+temp_file6, shell=True)
			sample_cube = iris.load_cube(temporary_file_space2+temp_file6)
			try:
				sample_cube = sample_cube.collapsed(['time'],iris.analysis.MEAN)
			except:
				pass
			if model == 'MRI-CGCM3':
				#Note this is added because MRI-CGCM3 uses 0.0 as missing data, and this is not picked up as a np masked array for some reason. However after the density calculation this value is no longer zero, so fund instead by identifying the modal value
				missing_data_value = scipy.stats.mode(density_cube[0][0].data, axis=None)[0][0]
				density_cube.data = np.ma.masked_values(density_cube.data, missing_data_value)
				missing_data_value_const_t = scipy.stats.mode(density_cube_const_t[0][0].data, axis=None)[0][0]
				density_cube_const_t.data = np.ma.masked_values(density_cube_const_t.data, missing_data_value)
			depth_cube = density_cube[0].copy()
			try:
				depth_cube.data = ma.masked_all_like(density_cube[0].data.data).copy()
			except:
				depth_cube.data = ma.masked_all_like(density_cube[0].data).copy()
			try:
				density_cube.coord('depth')
				depths = density_cube.coord('depth').bounds[:,1] # note changed this, because up 'til now it was working with the mid-points of layers, which would have skewed the meaning across levels
			except:
				depths = density_cube.coord('ocean sigma over z coordinate').bounds[:,1]
				#for memorys sake, do one year at a time..
			shape = np.shape(density_cube)
			depth_cube.data = np.ma.masked_array(np.swapaxes(np.tile(depths,[shape[3],shape[2],1]),0,2))
			try:
				density_cube.coord('depth')
				MLD_out = density_cube.extract(iris.Constraint(depth = np.min(depth_cube.data)))
				MLD_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
				surface_density_out = MLD_out.copy()
				surface_density_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
				surface_density_out_const_t = MLD_out.copy()
				surface_density_out_const_t.data = ma.masked_all_like(density_cube_const_t[:,0,:,:].data.data).copy()
			except:
				MLD_out = density_cube[:,0,:,:]
				surface_density_out = MLD_out.copy()
				MLD_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
				surface_density_out.data = ma.masked_all_like(density_cube[:,0,:,:].data.data).copy()
				surface_density_out_const_t = MLD_out.copy()
				surface_density_out_const_t.data = ma.masked_all_like(density_cube_const_t[:,0,:,:].data.data).copy()
			MLD_out_data = MLD_out.data.copy()
			surface_density_data = surface_density_out.data.copy()
			surface_density_data_const_t = surface_density_out_const_t.data.copy()
			print'calculating mixed layer depths'+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
			for i in range(np.shape(MLD_out)[0]):
				density_tmp = density_cube[i].copy()
				density_tmp_const_t = density_cube_const_t[i].copy()
				density_diff = density_tmp[0,:,:].copy()
				density_diff2 = density_tmp.copy()
				density_diff += 0.125 # Levitus (1982) density criteria
				density_diff3 = np.tile(density_diff.data,[np.shape(density_cube.data[i])[0],1,1])
				idx_mld = density_cube[i].data <= density_diff3
				MLD = density_tmp.data.copy()
				MLD.mask = False
				MLD.data[:] = np.NAN
				#MLD.mask = density_cube[i].data.mask.copy()
				MLD.data[idx_mld] = depth_cube.data[idx_mld]
				MLD = np.ma.masked_invalid(MLD)
				MLD = np.ma.masked_where(sample_cube.data.mask,MLD)
				MLD_out_data[i,:,:] = np.ma.max(MLD,axis=0)
				#density
				surface_density = density_tmp.data.copy()
				surface_density.mask = False
				surface_density.data[:] = np.NAN
				surface_density.data[idx_mld] = density_tmp.data[idx_mld]
				surface_density = np.ma.masked_invalid(surface_density)
				surface_density = np.ma.masked_where(sample_cube.data.mask,surface_density)
				#
				surface_density_const_t = density_tmp_const_t.data.copy()
				surface_density_const_t.mask = False
				surface_density_const_t.data[:] = np.NAN
				surface_density_const_t.data[idx_mld] = density_tmp_const_t.data[idx_mld]
				surface_density_const_t = np.ma.masked_invalid(surface_density_const_t)
				surface_density_const_t = np.ma.masked_where(sample_cube.data.mask,surface_density_const_t)
				thicknesses = MLD.copy()
				thicknesses[1::,:,:] = depth_cube.data[1::,:,:] - depth_cube.data[0:-1,:,:]
				thicknesses[0,:,:] = depth_cube.data[0,:,:]
				surface_density_data[i,:,:] = np.ma.sum(surface_density * thicknesses,axis=0) / np.ma.max(MLD,axis=0)
				surface_density_data_const_t[i,:,:] = np.ma.sum(surface_density_const_t * thicknesses,axis=0) / np.ma.max(MLD,axis=0)
			MLD_out.data = MLD_out_data
			surface_density_out.data = surface_density_data
			surface_density_out_const_t.data = surface_density_data_const_t
			extra_text = ''
			if TESTING:
				extra_text='_testing'
			iris.fileformats.netcdf.save(MLD_out,output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+season+extra_text+'.nc')
			iris.fileformats.netcdf.save(surface_density_out,output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+season+extra_text+'.nc')
			iris.fileformats.netcdf.save(surface_density_out_const_t,output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+season+'_const_t'+extra_text+'.nc')
			try:
				os.remove(temporary_file_space2+temp_file6)
			except:
				pass
			subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1a, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1z, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1x, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1y, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1b, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file2, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file2a, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file2b, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file2c, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file3, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file3b, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file4, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file5, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file6, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file1, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file1a, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file1z, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file1x, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file1y, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file1b, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file2a, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file2b, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file2c, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file3, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file3b, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file4, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file5, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)

'''
lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

files = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/*.nc')
for i,my_file in enumerate(files):
	print 'processing ',i,' in ',np.size(files)
	name = my_file.split('/')[-1]
	test = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'+name)
	if np.size(test) == 0:
		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'+name,  options = '-P 7')

'''

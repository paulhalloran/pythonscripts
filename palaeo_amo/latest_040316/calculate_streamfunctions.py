import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import math

 
def distance_on_unit_sphere(lat,resolution):
	lat1 = lat2 = lat
	# Convert latitude and longitude to
	# spherical coordinates in radians.
	degrees_to_radians = math.pi/180.0 
	# phi = 90 - latitude
	phi1 = (90.0 - lat1)*degrees_to_radians
	phi2 = (90.0 - lat2)*degrees_to_radians
	# theta = longitude
	theta1 = 0.0*degrees_to_radians
	theta2 = resolution*degrees_to_radians
	# Compute spherical distance from spherical coordinates.
	# For two locations in spherical coordinates
	# (1, theta, phi) and (1, theta', phi')
	# cosine( arc length ) =
	# sin phi sin phi' cos(theta-theta') + cos phi cos phi'
	# distance = rho * arc length
	cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) +
	math.cos(phi1)*math.cos(phi2))
	arc = math.acos( cos )
	# Remember to multiply arc by the radius of the earth
	# in your favorite set of units to get length.
	return arc * 6.373e6 #to get in m

resolution = 0.25


'''

#producing an Atlantic mask (mask masked and Atlantic has value of 1, elsewhere zero) to use in the stream function calculation


#/data/NAS-ph290/ph290/cmip5/last1000_vo_amoc
#input_file = '/media/usb_external1/tmp/MRI-CGCM3_vo_past1000_r1i1p1_regridded_not_vertically.nc'
input_file = '/data/temp/ph290/regridded/cmip5/last1000_vo_amoc_high_res/CCSM4_vo_past1000_r1i1p1_regridded_not_vertically.nc'

cube = iris.load_cube(input_file)
cube = cube[0,0]
cube.data = ma.masked_where(cube.data == 0,cube.data)
#tmp = cube.lazy_data()
#tmp = biggus.ma.masked_where(tmp.ndarray() == 0,tmp.masked_array())

location = -30/resolution
start_date = 850
end_date = 1850



tmp_cube = cube.copy()
tmp_cube = tmp_cube*0.0


print 'masking forwards'

for y in np.arange(180/resolution):
    print 'lat: ',y,'of ',180/resolution
    flag = 0
    tmp = tmp_cube.data.mask[y,:]
    tmp2 = tmp_cube.data[y,:]
    for x in np.arange(360/resolution):
        if tmp[location] == True:
            flag = 1
        if ((tmp[location] == False) & (flag == 0)):
            tmp2[location] = 1
        tmp = np.roll(tmp,+1)
        tmp2 = np.roll(tmp2,+1)
    tmp_cube.data.mask[y,:] = tmp
    tmp_cube.data.data[y,:] = tmp2.data

location = location+1

print 'masking backwards'

for y in np.arange(180/resolution):
    print 'lon: ',y,'of ',180/resolution
    flag = 0
    tmp = tmp_cube.data.mask[y,:]
    tmp2 = tmp_cube.data[y,:]
    for x in np.arange(360/resolution):
        if tmp[location] == True:
            flag = 1
        if ((tmp[location] == False) & (flag == 0)):
            tmp2[location] = 1
        tmp = np.roll(tmp,-1)
        tmp2 = np.roll(tmp2,-1)
    tmp_cube.data.mask[y,:] = tmp
    tmp_cube.data.data[y,:] = tmp2.data

tmp_cube.data.data[150/resolution:180/resolution,:] = 0.0
tmp_cube.data.data[0:40/resolution,:] = 0.0
tmp_cube.data.data[:,20/resolution:180/resolution] = 0.0
tmp_cube.data.data[:,180/resolution:280/resolution] = 0.0

loc = np.where(tmp_cube.data.data == 0.0)
tmp_cube.data.mask[loc] = True

mask1 = tmp_cube.data.mask
cube_test = []



with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_atl_mask_025.pickle', 'w') as f:
    pickle.dump([mask1], f)



'''
#calculating stream function
'''

'''

#Read in mask to save running the above each time...

with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_atl_mask_025.pickle', 'r') as f:
    mask1 = pickle.load(f)



#trying with the 1/4 degree dataset rather than the 1x1 - this should make the stram function calculatoi nmore robust
files = glob.glob('/data/temp/ph290/regridded/cmip5/last1000_vo_amoc_high_res/*_vo_*.nc')
#/media/usb_external1/cmip5/last1000_vo_amoc



models = []
max_strm_fun = []
max_strm_fun_26 = []
max_strm_fun_45 = []
model_years = []


for file in files:

	model = file.split('/')[7].split('_')[0]
	print model
	models.append(model)
	cube = iris.load_cube(file)

	print 'applying mask'

	try:
					levels =  np.arange(cube.coord('depth').points.size)
	except:
					levels = np.arange(cube.coord('ocean sigma over z coordinate').points.size)

	#for level in levels:
	#		print 'level: '+str(level)
	#		for year in np.arange(cube.coord('time').points.size):
	#			#print 'year: '+str(year)
	#			tmp = cube.lazy_data()
	#			mask2 = tmp[year,level,:,:].masked_array().mask
	#			tmp_mask = np.ma.mask_or(mask1, mask2)
	#			tmp[year,level,:,:].masked_array().mask = tmp_mask

	#variable to hold data from first year of each model to check
	#that the maskls have been applied appropriately

	cube.coord('latitude').guess_bounds()
	cube.coord('longitude').guess_bounds()
	grid_areas = iris.analysis.cartography.area_weights(cube[0])
	grid_areas = np.sqrt(grid_areas) # to get length of the side of a box

	shape = np.shape(cube)
	tmp = cube[0].copy()
	tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
	tmp = tmp.collapsed('longitude',iris.analysis.SUM)
	collapsed_data = np.tile(tmp.data,[shape[0],1,1])

	mask_cube = cube[0].copy()
	tmp_mask = np.tile(mask1,[shape[1],1,1])
	mask_cube.data.mask = tmp_mask
	mask_cube.data.mask[np.where(mask_cube.data.data == mask_cube.data.fill_value)] = True

	# MAKE A 3d array conatining grid box widths
	latitudes_3d = np.swapaxes(np.swapaxes(np.tile(cube.coord('latitude').points,[np.shape(cube)[3],np.shape(cube)[1],1]),1,0),2,1)
	box_widths_3d = latitudes_3d.copy()

	for i in range(np.shape(latitudes_3d)[1]):
		for j in range(np.shape(latitudes_3d)[2]):
			box_widths_3d[:,i,j] = distance_on_unit_sphere(latitudes_3d[0,i,j],resolution)



	print 'collapsing cube along longitude'
	try:
			slices = cube.slices(['depth', 'latitude','longitude'])
	except:
			slices = cube.slices(['ocean sigma over z coordinate', 'latitude','longitude'])
	for i,t_slice in enumerate(slices):
		print np.str(i)
			#print 'year:'+str(i)
		tmp = t_slice.copy()
		tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
		tmp *= box_widths_3d #multiply m/s values in each box by width of box (m), answer m2
		mask_cube_II = tmp.data.mask
		tmp.data.mask = mask_cube.data.mask | mask_cube_II
		#if i == 0:
			#plt.close('all')
			#qplt.contourf(tmp[0])
			#plt.savefig('/home/ph290/Documents/figures/test/'+model+'l1.png')
			#plt.close('all')
			#qplt.contourf(tmp[10])
			#plt.savefig('/home/ph290/Documents/figures/test/'+model+'l10.png')
	# 	collapsed_data[i] = tmp.collapsed('longitude',iris.analysis.SUM).data
		collapsed_data[i] = np.ma.sum(tmp.data,axis = 2)
		#seems OK - numbers are about right.

	try:
			depths = cube.coord('depth').points*-1.0
			bounds = cube.coord('depth').bounds
	except:
			depths = cube.coord('ocean sigma over z coordinate').points*-1.0
			bounds = cube.coord('ocean sigma over z coordinate').bounds
	thickness = bounds[:,1] - bounds[:,0]
	test = thickness.mean()
	if test > 1:
			thickness = bounds[1:,0] - bounds[0:-1,0]
			thickness = np.append(thickness, thickness[-1])
		
	thickness = np.flipud(np.rot90(np.tile(thickness,[180/resolution,1])))
	depths_2d = np.flipud(np.rot90(np.tile(depths,[180/resolution,1])))
	latitudes_2d = np.flipud(np.tile(cube.coord('latitude').points,[np.size(depths),1]))
	#thicknesses are correct

	tmp_strm_fun_26 = []
	tmp_strm_fun_45 = []
	tmp_strm_fun = []
	for i in np.arange(np.size(collapsed_data[:,0,0])):
			tmp2 = collapsed_data[i].copy()
			tmp2 *= thickness
			tmp2 = np.flipud(np.ma.cumsum(np.flipud(tmp2),axis = 0))* -1.0e-6 #*1.0e-6 to get in Sv +ve N
			coord = cube.coord('latitude').points
			loc = np.where(coord >= 26)[0][0]
			tmp_strm_fun_26 = np.append(tmp_strm_fun_26,np.max(tmp2[:,loc]))
			loc = np.where(coord >= 45)[0][0]
			tmp_strm_fun_45 = np.append(tmp_strm_fun_45,np.max(tmp2[:,loc]))
			tmp_strm_fun = np.append(tmp_strm_fun,np.max(tmp2[:,:]))

	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	years = np.array([coord.units.num2date(value).year for value in coord.points])
	model_years.append(years)

	max_strm_fun_26.append(tmp_strm_fun_26)
	max_strm_fun_45.append(tmp_strm_fun_45)
	max_strm_fun.append(tmp_strm_fun)


with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VII.pickle', 'w') as f:
    pickle.dump([models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files], f)
    
    
#Subselect and save out version containing just eh rhight models

#models and order to use are:
#['CCSM4', 'FGOALS-s2', 'GISS-E2-R', 'HadGEM2-ES', 'MIROC-ESM','MPI-ESM-P', 'MRI-CGCM3', 'bcc-csm1-1'],

#models we have are:
#CCSM4, CSIRO-Mk3L-1-2,FGOALS-gl,FGOALS-s2,GISS-E2-R,HadGEM2-ES,bcc-csm1-1,MRI-CGCM3,MPI-ESM-P,MIROC-ESM,HadCM3

#Subselect and save:

"""
models_b = ['CCSM4', 'FGOALS-s2', 'GISS-E2-R', 'HadGEM2-ES', 'MIROC-ESM','MPI-ESM-P', 'MRI-CGCM3', 'bcc-csm1-1']

max_strm_fun_b = []
max_strm_fun_b.append(max_strm_fun[0])
max_strm_fun_b.append(max_strm_fun[3])
max_strm_fun_b.append(max_strm_fun[4])
max_strm_fun_b.append(max_strm_fun[5])
max_strm_fun_b.append(max_strm_fun[9])
max_strm_fun_b.append(max_strm_fun[8])
max_strm_fun_b.append(max_strm_fun[7])
max_strm_fun_b.append(max_strm_fun[6])

max_strm_fun_26_b = []
max_strm_fun_26_b.append(max_strm_fun_26[0])
max_strm_fun_26_b.append(max_strm_fun_26[3])
max_strm_fun_26_b.append(max_strm_fun_26[4])
max_strm_fun_26_b.append(max_strm_fun_26[5])
max_strm_fun_26_b.append(max_strm_fun_26[9])
max_strm_fun_26_b.append(max_strm_fun_26[8])
max_strm_fun_26_b.append(max_strm_fun_26[7])
max_strm_fun_26_b.append(max_strm_fun_26[6])

max_strm_fun_45_b = []
max_strm_fun_45_b.append(max_strm_fun_45[0])
max_strm_fun_45_b.append(max_strm_fun_45[3])
max_strm_fun_45_b.append(max_strm_fun_45[4])
max_strm_fun_45_b.append(max_strm_fun_45[5])
max_strm_fun_45_b.append(max_strm_fun_45[9])
max_strm_fun_45_b.append(max_strm_fun_45[8])
max_strm_fun_45_b.append(max_strm_fun_45[7])
max_strm_fun_45_b.append(max_strm_fun_45[6])

model_years_b = []
model_years_b.append(model_years[0])
model_years_b.append(model_years[3])
model_years_b.append(model_years[4])
model_years_b.append(model_years[5])
model_years_b.append(model_years[9])
model_years_b.append(model_years[8])
model_years_b.append(model_years[7])
model_years_b.append(model_years[6])

with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VII.pickle', 'w') as f:
    pickle.dump([models_b,max_strm_fun_b,max_strm_fun_26_b,max_strm_fun_45_b,model_years_b,mask1,files], f)
    
"""

#plotting streamfunctions:
'''
plt.close('all')
i=4
tmp1 = collapsed_data[i].copy()
tmp2 = tmp1*thickness
tmp3 = np.flipud(np.ma.cumsum(np.flipud(tmp2),axis = 0))
# tmp3 = np.cumsum(tmp2,axis = 0)
plt.contourf(latitudes_2d,depths_2d,tmp3*1.0e-6,np.linspace(-30,30,31))
#np.linspace(-2000,2000,31))
plt.colorbar()
plt.xlim([-20,50])
plt.show(block=False)
'''


strm_1 = iris.load_cube('/data/NAS-ph290/ph290/cmip5/msftmyz/last1000/CCSM4_msftmyz_past1000_regridded.nc')
lats = strm_1.coord('latitude').points
loc = np.where(lats>=45)
strm_1 = strm_1[:,0,:,loc[0][0]]
sf = np.nanmax(strm_1.data,axis = 1)


plt.plot(sf/1026.0*1.0e-6)
plt.plot(max_strm_fun_45[0])
plt.show()

strm_2 = iris.load_cube('/data/NAS-ph290/ph290/cmip5/msftmyz/last1000/MPI-ESM-P_msftmyz_past1000_regridded.nc')
lats = strm_2.coord('grid_latitude').points
loc = np.where(lats>=45)
strm_2 = strm_2[:,0,:,loc[0][0]]
sf2 = np.nanmax(strm_2.data,axis = 1)

plt.plot(sf2/1026.0*1.0e-6)
plt.plot(max_strm_fun_45[8])
plt.show()



strm_3 = iris.load_cube('/data/NAS-ph290/ph290/cmip5/msftmyz/msftmyz/last1000/MRI-CGCM3_msftmyz_past1000_regridded.nc')
lats = strm_3.coord('grid_latitude').points
loc = np.where(lats>=45)
strm_3 = strm_3[:,0,:,loc[0][0]]
sf3 = np.nanmax(strm_3.data,axis = 1)

plt.plot(sf3/1026.0*1.0e-6)
plt.plot(max_strm_fun_45[7])
plt.show()


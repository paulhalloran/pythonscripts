#####################
# COMMENT HERE WHEN RUNNING NOT FOR 1st TIME
#######################

#####################
# COMMENT HERE WHEN RUNNING NOT FOR 1st TIME
#######################


#######################
#Old figure 3
#######################


#Re-look at psl bit - normalise then ifrequired


###
#
#  Data initially processed by /home/ph290/Documents/python_scripts/calculate_mld_big_files_no_regridding_deep_capability.py
#/home/ph290/Documents/python_scripts/process_monthly_density_past1000.py
#
###

#uncomment if running for first time
#################

##################

import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
	models = np.unique(models_tmp)
	return models

##########################################
# Mian part of code                      #
##########################################

################################
# produce area means if they don;t already exist
################################

"""
lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

files = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/*.nc')
for i,my_file in enumerate(files):
        print 'processing ',i,' in ',np.size(files)
        name = my_file.split('/')[-1]
        test = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'+name)
        if np.size(test) == 0:
                cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = '/data/data1/ph290/cmip5/last1000/mld/spatial_avg/'+name,  options = '-P 7')
"""

################################
# Prepare the plots
################################



#start_year = 850
start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=scipy.signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)


#############################################################################################################################
# Models: remove linear trend and normalise variability, then put all models on same time axis and perform multi-model mean #
#############################################################################################################################


input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger6/'
#NOTE in this directoryt THE CRAZY LOW DENSITIES are set TO MISSING DATA

models = model_names(input_directory)
models = list(models)
# models.remove('MIROC-ESM')
#models.remove('FGOALS-gl')
models.remove('CSIRO-Mk3L-1-2')
#models.remove('HadGEM2-ES')
models.remove('HadCM3')

#models.remove('CCSM4')
#models.remove('FGOALS-s2')
#models.remove('MRI-CGCM3')
#models.remove('bcc-csm1-1')


#models.remove('CSIRO-Mk3L-1-2')
#models.remove('GISS-E2-R')
#models.remove('HadCM3')
#models.remove('HadGEM2-ES')
#models.remove('MIROC-ESM')
#models.remove('MPI-ESM-P')

# print '*******************************************'
# print '*******************************************'
# print ' As soon as processed bring MIROC BACK IN! '
# print '*******************************************'
# print '*******************************************'
# models.remove('MIROC-ESM') #temporarily until regridded...
#Note previously had two FGOALS models, sp removed one so a better ensmeble
#FGOALS-gl chosen, because there seems to be a problem calculating its mixed layer depths due to teh levels being upside down
models = np.array(models)

multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()

appending  = 'normalised_no_hadcm3_csiro'
for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2 = signal.detrend(data)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	#data3 = data2
	#data3 -= np.nanmean(data3)
 	#window_type = 'boxcar'
 	#data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	for index,y_tmp in enumerate(expected_years):
		loc2 = np.where(yrs == y_tmp)
		if np.size(loc2) != 0:
			multi_model_density[index,i] = data3[loc2]



#smoothing_var = 1
#if smoothing_var > 0:
#	for i,model in enumerate(models):
#		#multi_model_density[:,i] = rm.running_mean(multi_model_density[:,i],smoothing_var)
#		multi_model_density[:,i] = gaussian_filter1d(multi_model_density[:,i],smoothing_var)

multi_model_mean_density = np.nanmean(multi_model_density, axis = 1)
multi_model_std_density = np.nanstd(multi_model_density, axis = 1)

# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/n_ice_dens'+appending+'.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(multi_model_mean_density[i])+'\n')

f.close()

window_type = 'boxcar'
y = pandas.rolling_window(multi_model_mean_density,5,win_type=window_type,center=True).copy()
y2 = multi_model_std_density.copy()

##########################################
# plotting                               #
##########################################

"""
# plt.close('all')
fig, ax = plt.subplots(figsize=(8, 4))
#y = rm.running_mean(multi_model_mean_density,smoothing_var)
#y2 = rm.running_mean(multi_model_std_density,smoothing_var)
y = multi_model_mean_density
y2 = multi_model_std_density
#data2 = y-np.min(y)
#data3 = data2/(np.max(data2))
#y = data3 - np.nanmean(data3)
###
ax.plot(expected_years,y,'k',lw=1.75,alpha=0.5)
# ax.fill_between(expected_years, y + y2, y  - y2, color="none", facecolor='blue', alpha=0.2)
# steps = 1
# stdevs = 1.0
# tmp = np.arange(float(steps))/float(steps)
# for i in range(steps):
# 	y2b = y2.copy()*(1.0/float(i+1))
# 	#y2b = y2.copy()*tmp[i]
# 	ax.fill_between(expected_years, y + y2b*stdevs, y  - y2b*stdevs, color="none", facecolor='k', alpha=1.0/float(steps))
stdevs = 1.0
ax.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
ax.fill_between(expected_years, y + y2*stdevs*0.5, y  - y2*stdevs*0.5, color="none", facecolor='k', alpha=0.25)
ax2 = ax.twinx()
if smoothing_var > 0:
	#y_bivalve = rm.running_mean(bivalve_data_initial,smoothing_var)
	y_bivalve = gaussian_filter1d(bivalve_data_initial,smoothing_var*1.0)
	#NOTE, I've doubled the smoothing on the bivalve data, because it is not getting the smoothing of an ensemble mean - not sure what the right thing to choose here is. Doubling is a bit arbitrary
else:
	y_bivalve = bivalve_data_initial
ax2.plot(bivalve_year,y_bivalve*0.8,'r',lw=2,alpha=0.5)
# ax2.plot(bivalve_year+20,y_bivalve,'g',lw=2,alpha=0.75)
#ax2.plot(bivalve_year[0:700],y_bivalve[0:700]*0.5,'r',lw=1.75,alpha=0.5)
#ax2.plot(bivalve_year[-200::]+20,y_bivalve[-200::]*0.5,'g',lw=1.75,alpha=0.5)
ax.set_ylim(-0.22,0.22)
ax2.set_ylim(-0.5,0.5)
ax.set_xlabel('Calendar Year')
ax.set_ylabel('PMIP3 density anomaly')
ax2.set_ylabel('bivalve $\delta^{18}$O anomaly')
ax2.yaxis.label.set_color('red')
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_'+period+'_2a_deep_dfj_2.png')
print 'grey = 2 standard deviatins (i.e. +/- 2*stdev)'

"""
###



#uncomment if running for first time
#################

#################





###
# Composite maps
###

# y = multi_model_mean_density.copy()
# y2 = multi_model_std_density.copy()



"""
def return_ensmeble_mean(spatial_files,smoothing_var,y):
	tmp_cube = iris.load_cube(spatial_files[0])[0]
	ensmeble_data = np.zeros([np.size(spatial_files),np.shape(tmp_cube.data)[0],np.shape(tmp_cube.data)[1]])
	ensmeble_data[:] = np.nan
	for i,file in enumerate(spatial_files):
		cube = iris.load_cube(file)
		cube.data = scipy.signal.detrend(cube.data, axis=0)
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where((yrs >= start_year) & (yrs <= end_year))
		cube = cube[loc]
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		# y_tmp = gaussian_filter1d(y,smoothing_var)
		y_tmp = y
		loc2 = np.where(y_tmp >= np.nanmean(y_tmp) + np.nanstd(y_tmp))[0]
		loc3 = np.where(y_tmp <= np.nanmean(y_tmp) - np.nanstd(y_tmp))[0]
		high_yrs = expected_years[loc2]
		low_yrs = expected_years[loc3]
		common_years_high = np.intersect1d(yrs,high_yrs)
		common_years_low = np.intersect1d(yrs,low_yrs)
		loc_high = np.where(np.in1d(yrs,common_years_high))
		loc_low = np.where(np.in1d(yrs,common_years_low))
		cube_high_minus_low = cube[loc_low].collapsed('time',iris.analysis.MEAN) - cube[loc_high].collapsed('time',iris.analysis.MEAN)
 		# qplt.contourf(cube_high_minus_low,np.linspace(-0.2,0.2,21))
#  		plt.show()
		ensmeble_data[i,:,:] = cube_high_minus_low.data
	ensmeble_mean = np.nanmean(ensmeble_data,axis = 0)
	tmp_cube.data = ensmeble_mean
	return tmp_cube
"""

def return_ensmeble_mean(spatial_files,smoothing_var,y):
	tmp_cube = iris.load_cube(spatial_files[0])[0]
	ensmeble_data = np.zeros([np.size(spatial_files),np.shape(tmp_cube.data)[0],np.shape(tmp_cube.data)[1]])
	ensmeble_data[:] = np.nan
	for i,file in enumerate(spatial_files):
		print file
		#Density
		model = file.split('/')[-1].split('_')[0]
		period = 'JJA'
		cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
		cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
		data = cube.data
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs1 = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where((yrs1 >= start_year) & (yrs1 <= end_year))
		cube = cube[loc]
		data2 = signal.detrend(data)
		data3 = data2
		data3 -= np.nanmean(data3)
		window_type = 'boxcar'
	# 		data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
		#AND PSL
		cube1 = iris.load_cube(file)
		cube1.data = scipy.signal.detrend(cube1.data, axis=0)
		coord = cube1.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where((yrs >= start_year) & (yrs <= end_year))
		cube1 = cube1[loc]
		coord = cube1.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		# y_tmp = gaussian_filter1d(y,smoothing_var)
		y_tmp = data3
		loc2 = np.where(y_tmp >= np.nanmean(y_tmp) + np.nanstd(y_tmp))[0]
		loc3 = np.where(y_tmp <= np.nanmean(y_tmp) - np.nanstd(y_tmp))[0]
		high_yrs = yrs1[loc2]
		low_yrs = yrs1[loc3]
		common_years_high = np.intersect1d(yrs,high_yrs)
		common_years_low = np.intersect1d(yrs,low_yrs)
		loc_high = np.where(np.in1d(yrs,common_years_high))
		loc_low = np.where(np.in1d(yrs,common_years_low))
		cube_high_minus_low = cube1[loc_low].collapsed('time',iris.analysis.MEAN) - cube1[loc_high].collapsed('time',iris.analysis.MEAN)
			# qplt.contourf(cube_high_minus_low,np.linspace(-0.2,0.2,21))
	#  		plt.show()
		ensmeble_data[i,:,:] = cube_high_minus_low.data
	ensmeble_mean = np.nanmean(ensmeble_data,axis = 0)
	tmp_cube.data = ensmeble_mean
	return tmp_cube



#smoothing_var = 1

spatial_directory = '/data/NAS-ph290/ph290/cmip5/last1000/psl_regridded_winter/'

spatial_files = []
for model in models:
	try:
		spatial_files.append(glob.glob(spatial_directory+model+'_psl_*.nc')[0])
	except:
		print model+' missing'

ens_mean = return_ensmeble_mean(spatial_files,10,y)
plt.figure(0)
qplt.contourf(ens_mean,21)
#np.linspace(-0.2,0.2,21))
plt.gca().coastlines()
plt.show(block = False)

ncep_mslp = iris.load_cube('/data/NAS-ph290/ph290/reanalysis/NCEP_v2/mslp_mon.nc')
coord = ncep_mslp.coord('time')
dt = coord.units.num2date(coord.points)
yrs = np.array([coord.units.num2date(value).year for value in coord.points])
month = np.array([coord.units.num2date(value).month for value in coord.points])
year_mon = yrs+month/12.0
nao = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/nao_index.tim',skip_header = 10)
nao_year_mon = nao[:,0]+nao[:,1]/12.0
common_dates = np.intersect1d(year_mon,nao_year_mon)
nao_loc = np.where(np.in1d(nao_year_mon,common_dates))
ncep_loc = np.where(np.in1d(year_mon,common_dates))

ts = nao[nao_loc,2]
cube = ncep_mslp[ncep_loc].copy()
cube = iris.analysis.maths.multiply(cube, 0.0)
ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[73,144,1]),1,2),0,1)
cube = iris.analysis.maths.add(cube, ts2)
out_cube = iris.analysis.stats.pearsonr(ncep_mslp[ncep_loc], cube, corr_coords=['time'])

lon_west = -100.0
lon_east = 50.0
lat_south = 15.0
lat_north = 90.0

cube_region_tmp = out_cube.intersection(longitude=(lon_west, lon_east))
out_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = ens_mean.intersection(longitude=(lon_west, lon_east))
ens_mean_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cmap1 = mpl_cm.get_cmap('RdBu_r')

plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(10)
gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)

import cartopy.crs as ccrs

ax2 = plt.subplot(gs[60:100,0:45],projection=ccrs.PlateCarree())
ax1 = plt.subplot(gs[60:100,55:100],projection=ccrs.PlateCarree())
ax3 = plt.subplot(gs[0:45,0:100])


import matplotlib.mlab as ml


# plt.close('all')
# plt.title('PMIP3 Sea Level Pressure composites',  fontsize=10)
# ax = ax1.subplot(122)
cube = ens_mean_region
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
data = cube.data
C1 = ax1.contourf(lons, lats, data, np.linspace(-60,60,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax1.coastlines()
ax1.set_title('PMIP3: high minus\nlow density years',  fontsize=12)
cb1 = plt.colorbar(C1,ax=ax1,orientation='horizontal',ticks=[-60,-30, 0,30,60])
cb1.set_label('Sea Level Pressure (Pa)')

#,cax=ax3)
# plt.colorbar(orientation='horizontal')

#####
# HadSLP analysis
#####


#######################
# psl
#######################

file = '/data/NAS-geo01/ph290/misc_data/slp.mnmean.nc'
# hadslp2

hasdlp_cube = iris.load_cube(file,'Monthly Mean Sea Level Pressure')
iris.coord_categorisation.add_season_year(hasdlp_cube, 'time', name='season_year')
iris.coord_categorisation.add_season_number(hasdlp_cube, 'time', name='season_number')
hasdlp_cube = hasdlp_cube.aggregated_by(['season_year','season_number'], iris.analysis.MEAN)
iris.coord_categorisation.add_season(hasdlp_cube, 'time', name='season_name')
loc = np.where(hasdlp_cube.coord('season_name').points == 'djf')
hasdlp_cube = hasdlp_cube[loc]
#iris.coord_categorisation.add_year(hasdlp_cube, 'time', name='year')
#hasdlp_cube = hasdlp_cube.aggregated_by('year', iris.analysis.MEAN)
coord = hasdlp_cube.coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])

#######################
# volcanic data
#######################


post_volc_years = np.array([1883,1884,1886,1887,1888,1889,1903,1904,1932,1933,1963,1964,1975,1976,1982,1983,1991,1992])
#Following: http://onlinelibrary.wiley.com/store/10.1029/2012JD017607/asset/jgrd18164.pdf;jsessionid=9FA5552A7B4590F363FB33C5586598EE.f01t01?v=1&t=ijercsq6&s=533a6d76025e2bc2a7770af392b2123c8efbb706
#i.e. two years following a major low latitude volcanic erruption

common_years_high = np.intersect1d(year,post_volc_years)
loc1 = np.where(np.in1d(year,common_years_high))
loc2 = np.where(np.logical_not(np.in1d(year,common_years_high)))


out_cube = hasdlp_cube[loc1].collapsed('time',iris.analysis.MEAN) - hasdlp_cube[loc2].collapsed('time',iris.analysis.MEAN)

cube_region_tmp = out_cube.intersection(longitude=(lon_west, lon_east))
out_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

####
# PSL plotting
####


cube = out_cube_region * 100.0 #conversion from mb to Pa
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
data = cube.data
C2 = ax2.contourf(lons, lats, data, np.linspace(-60,60,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax2.coastlines()
ax2.set_title('Observations: eruption\nminus no-eruption',  fontsize=12)
#HadSLP2.
cb2 = plt.colorbar(C2,ax=ax2,orientation='horizontal',ticks=[-60,-30, 0, 30,60])
cb2.set_label('Sea Level Pressure (Pa)')

smoothing_var = 1.0

#y = gaussian_filter1d(y,smoothing_var)
window_type = 'boxcar'
#y = pandas.rolling_window(y,5,win_type=window_type,center=True)
y2 = pandas.rolling_window(y2,5,win_type=window_type,center=True)
# y2 = gaussian_filter1d(y2,smoothing_var)


ax3.plot(expected_years,y,'k',lw=1.75,alpha=0.8)
stdevs = 1.0

# ax3.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
# ax3.fill_between(expected_years, y + y2*stdevs*0.5, y  - y2*stdevs*0.5, color="none", facecolor='k', alpha=0.25)

ax3.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
ax3.fill_between(expected_years, y + y2*stdevs*0.8, y  - y2*stdevs*0.8, color="none", facecolor='k', alpha=0.1)
ax3.fill_between(expected_years, y + y2*stdevs*0.6, y  - y2*stdevs*0.6, color="none", facecolor='k', alpha=0.1)
ax3.fill_between(expected_years, y + y2*stdevs*0.4, y  - y2*stdevs*0.4, color="none", facecolor='k', alpha=0.1)
ax3.fill_between(expected_years, y + y2*stdevs*0.2, y  - y2*stdevs*0.2, color="none", facecolor='k', alpha=0.1)


ax3b = ax3.twinx()
# if smoothing_var > 0:
# 	#y_bivalve = rm.running_mean(bivalve_data_initial,smoothing_var)
# 	y_bivalve = gaussian_filter1d(bivalve_data_initial,smoothing_var*1.0)
# 	#NOTE, I've doubled the smoothing on the bivalve data, because it is not getting the smoothing of an ensemble mean - not sure what the right thing to choose here is. Doubling is a bit arbitrary
# else:

# y_bivalve = rm.running_mean(bivalve_data_initial,5)
bivalve_year = bivalve_year[::-1]
bivalve_data_initial = bivalve_data_initial[::-1]
# y_bivalve = gaussian_filter1d(bivalve_data_initial.copy(),smoothing_var)
y_bivalve = pandas.rolling_window(bivalve_data_initial,5,win_type=window_type,center=True)
# y_bivalve = bivalve_data_initial


# y_bivalve -= np.nanmean(y_bivalve)
ax3b.plot(bivalve_year,y_bivalve,'r',lw=2,alpha=0.6)
# ax2.plot(bivalve_year+20,y_bivalve,'g',lw=2,alpha=0.75)
#ax2.plot(bivalve_year[0:700],y_bivalve[0:700]*0.5,'r',lw=1.75,alpha=0.5)
#ax2.plot(bivalve_year[-200::]+20,y_bivalve[-200::]*0.5,'g',lw=1.75,alpha=0.5)
ax3.set_ylim(-0.15,0.15)
ax3b.set_ylim(-0.6,0.6)

ax3.set_xlim(950,1850)
ax3.set_xlabel('Calendar Year')
ax3.set_ylabel('PMIP3 North Iceland\nnormalsied density anomaly')
ax3b.set_ylabel('bivalve $\delta^{18}$O anomaly')
ax3b.yaxis.label.set_color('red')

plt.annotate('a', xy=(.15, .925),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)

plt.annotate('b', xy=(.15, .4),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)

plt.annotate('c', xy=(.535, .4),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)


plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_with_psl_II_normalised_III_dfj_2.png')
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_with_psl_II_normalised_III_dfj_2.ps')


plt.show(block = False)

"""

y = multi_model_mean_density.copy()

print scipy.stats.pearsonr(y_bivalve[::-1], y)

no = 200
x = np.zeros(no)
for i in np.arange(no)+2:
	x_tmp = rm.running_mean(bivalve_data_initial[::-1],i)
	y_tmp = rm.running_mean(y,j)
	loc = np.where(np.logical_not((np.isnan(x_tmp)) | (np.isnan(y_tmp))))
	x_tmp = x_tmp[loc]
	y_tmp = y_tmp[loc]
	x[i] = scipy.stats.pearsonr(x_tmp,y_tmp)[0]


plt.plot(x)
plt.show()

i=30
y = multi_model_mean_density.copy()
plt.plot(rm.running_mean(bivalve_data_initial[::-1].copy(),i))
plt.plot(rm.running_mean(y,i)*5.0)
plt.show()

scipy.stats.pearsonr(gaussian_filter1d(bivalve_data_initial[::-1],i),gaussian_filter1d(y,i))



max_val = 0.0

for i in np.arange(80)+2:
	for j in np.arange(80)+2:
		x_tmp = rm.running_mean(bivalve_data_initial[::-1],i)
		y_tmp = rm.running_mean(y,j)
		loc = np.where(np.logical_not((np.isnan(x_tmp)) | (np.isnan(y_tmp))))
		x_tmp = x_tmp[loc]
		y_tmp = y_tmp[loc]
		p_r = scipy.stats.pearsonr(x_tmp,y_tmp)
# 		s_r = scipy.stats.spearmanr(x_tmp,y_tmp)
		if p_r > max_val:
			max_val = p_r
			print p_r




smoothings1 = np.linspace(2,31,30.0)
smoothings2 = smoothings1.copy()


coeff_det = np.zeros([np.size(smoothings1),np.size(smoothings1)])
coeff_det[:] = np.NAN
coeff_det_2 = coeff_det.copy()


for smoothing1_no,smoothing1 in enumerate(smoothings1):
	print smoothing1_no,' out of ',np.size(smoothings1)
	x_in = bivalve_data_initial.copy()
	x_in = x_in[::-1]
# 	x_in = x_in[0:-5]
	x_in = rm.running_mean(x_in,smoothing1)
	x_in = (x_in-np.nanmin(x_in))
	x_in /= np.nanmax(x_in)
	for smoothing2_no,smoothing2 in enumerate(smoothings2):
		y_in = y.copy()
# 		y_in = y_in[0:-5]
		y_in = rm.running_mean(y_in,smoothing2)
		y_in = (y_in-np.nanmin(y_in))
		y_in /= np.nanmax(y_in)
		loc = np.where(np.logical_not(np.isnan(x_in) | np.isnan(y_in)))
		if np.size(loc) <> 0:
			x_in2 = x_in[loc].copy()
			y_in2 = y_in[loc].copy()
		else:
			x_in2 = x_in.copy()
			y_in2 = y_in.copy()
		#slope, intercept, r_value, p_value, std_err = stats.linregress(x_in2,y_in2)
# 		r_value, p_value = stats.kendalltau(x_in2,y_in2)
		r_value, p_value = stats.spearmanr(x_in2,y_in2)
		#r2 = r_value**2
		#coeff_det_dens_amoc[i,smoothing_no] = r2
		#if p_value <= 0.001:
		r2 = r_value**2
		coeff_det[smoothing1_no,smoothing2_no] = r_value
		if p_value > 0.001:
			r2 = r_value**2
			coeff_det_2[smoothing1_no,smoothing2_no] = r_value




loc1 = np.where(coeff_det == np.max(coeff_det))
smoothing1 = smoothings1[loc1[1]]
smoothing2 = smoothings1[loc1[0]]

smoothing1 = smoothing2 = 20.0

x_in = bivalve_data_initial.copy()
x_in = x_in[::-1]
# 	x_in = x_in[0:-5]
# x_in = gaussian_filter1d(x_in,smoothing1)
x_in = rm.running_mean(x_in,smoothing1)
x_in = (x_in-np.nanmin(x_in))
x_in /= np.nanmax(x_in)

y_in = y.copy()
# 		y_in = y_in[0:-5]
# y_in = gaussian_filter1d(y_in,smoothing2)
y_in = rm.running_mean(y_in,smoothing2)
y_in = (y_in-np.nanmin(y_in))
y_in /= np.nanmax(y_in)

loc = np.where(np.logical_not(np.isnan(x_in) | np.isnan(y_in)))
x_in2 = x_in[loc].copy()
y_in2 = y_in[loc].copy()

r_value, p_value = stats.spearmanr(x_in2,y_in2)

print smoothings1[loc1[0]]
print r_value, p_value

#######################
# Contour plot part
#######################


cmap1 = mpl_cm.get_cmap('RdBu_r')
cmap2 = mpl_cm.get_cmap('RdBu_r')
#gray_r
#z = np.nanmean(coeff_det_dens_amoc,axis = 0)
z = np.fliplr(np.rot90(coeff_det,3))
z2 = np.fliplr(np.rot90(coeff_det_2,3))
zmin = 0.0
zmax = np.max(z)
xlab , ylab = np.meshgrid(smoothings1,smoothings2)

# fig, axarr = plt.subplots(1, 1, (figsize=(8, 4))
plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(10)
fig.set_figwidth(10)
# ax1 = plt.subplot2grid((2,1),(1, 0))
#gs = gridspec.GridSpec(2, 1,width_ratios=[1,1],height_ratios =[1,2])
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.85)

# ax1 = plt.subplot(gs[1])
# ax2 = plt.subplot(gs[0])
#ax2 = plt.subplot(gs[0:44,0:100])
ax1 = plt.subplot(gs[0:100,0:90])
ax3 = plt.subplot(gs[56:100,95:100])


#, rowspan=2)
C = ax1.contourf(xlab,ylab,z,np.linspace(zmin,zmax,100),extend='both',cmap=cmap1)
cb2 = plt.colorbar(C,ax=ax1,cax=ax3)
ax1.contourf(xlab,ylab,z2,np.linspace(zmin,zmax,100),extend='both',hatches=['.'],cmap=cmap2)
CS = ax1.contour(xlab,ylab,z,10,extend='both',colors='k')
ax1.clabel(CS, fontsize=12, inline=1)
ax1.set_xlabel('Bivalve $\delta^{18}$O smoothing (years)')
ax1.set_ylabel('PMIP3 N. Iceland density smoothing (years)')
cb2.set_label('Variance explained (R$^2$)')

plt.show(block = True)



"""

x1 = bivalve_data_initial.copy()
x2 = multi_model_mean_density.copy()

start_year2 = 953
end_year2 = 1849

expected_years

loc = np.where((bivalve_year >= start_year2) & (bivalve_year <= end_year2))
x1 = x1[loc]
loc = np.where((expected_years >= start_year2) & (expected_years <= end_year2))
x2 = x2[loc]

"""
size = 40.0
out = np.zeros([size,size])
for i in np.arange(40)+1:
	for j in np.arange(40)+1:
		y_bivalve = pandas.rolling_window(x1,i,win_type=window_type,center=True).copy()
		# y_bivalve = pandas.rolling_window(x1,5,win_type=window_type,center=True).copy()
		y_model = pandas.rolling_window(x2,j,win_type=window_type,center=True).copy()
		loc =np.logical_not((np.isnan(y_bivalve)) & (np.isnan(y_model)))
		y_bivalve = y_bivalve[loc]
		y_model = y_model[loc]
		out[i-1,j-1] = stats.spearmanr(y_bivalve,y_model)[0]
"""


y_bivalve = pandas.rolling_window(x1,30,win_type=window_type,center=True).copy()
y_model = pandas.rolling_window(x2,30,win_type=window_type,center=True).copy()
y_bivalve -= np.nanmin(y_bivalve)
y_bivalve /= np.nanmax(y_bivalve)
y_model -= np.nanmin(y_model)
y_model /= np.nanmax(y_model)
#plt.plot(y_bivalve)
#plt.plot(y_model)
#plt.show()

print stats.spearmanr(y_bivalve,y_model)



#######################
#Old figure 2
#######################



from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean as rm
import running_mean_post as rmp
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import scipy.interpolate
import gc
import pickle
import biggus
import seawater
import cartopy.feature as cfeature
import scipy.ndimage
import scipy.ndimage.filters
import gsw
import scipy.stats as stats
import time
import matplotlib.patches as mpatches
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import pandas
import cartopy.crs as ccrs
import iris
import iris.plot as iplt
import cartopy
import iris.analysis
import iris.analysis.stats

# input_directory = '/data/data1/ph290/cmip5/last1000/mld/spatial_avg/'
input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger/'

start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


def ts_model_corr(ts,cube2):
	cube3 = cube2.copy()
	cube3 = iris.analysis.maths.multiply(cube3, 0.0)
	ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[180,360,1]),1,2),0,1)
	cube3 = iris.analysis.maths.add(cube3, ts2)
	return iris.analysis.stats.pearsonr(cube2, cube3, corr_coords=['time'])



def extract_years(cube):
    try:
    	iris.coord_categorisation.add_year(cube, 'time', name='year2')
    except:
    	'already has year2'
    #start_year = 850
	start_year = 955
    #end_year = 1850
	end_year = 1849
    loc = np.where((cube.coord('year2').points >= start_year) & (cube.coord('year2').points <= end_year))
    loc2 = cube.coord('time').points[loc[0][-1]]
    cube = cube.extract(iris.Constraint(time = lambda time_tmp: time_tmp <= loc2))
    return cube

def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a



def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


def butter_bandpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a




'''
#producing an Atlantic mask (mask masked and Atlantic has value of 1, elsewhere zero) to use in the stream function calculation
# and calculating stream function


Calculated in /home/ph290/Documents/python_scripts/palaeo_amo/latest_040316/calculate_streamfunctions.py



'''
### Remove three quotatoin marks below if running for 1st tim eafter load

### Remove three quotatoin marks above if running for 1st tim eafter load
#
# with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_IIII.pickle', 'r') as f:
#     models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)

with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VII.pickle', 'r') as f:
    models_other_list,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files = pickle.load(f)


###########################################
#    restrict loading to top 1000m        #
###########################################

chosen_levels = lambda cell: cell <= 500
level_above_1000 = iris.Constraint(depth=chosen_levels)

# models.remove('MRI-CGCM3') #WOUDL BE GREAT TO SORT THIS OUT!
# models.remove('FGOALS-gl') # already removed...
# models.remove('HadCM3') # already removed...
# models.remove('GISS-E2-R')
models = np.array(models)

#models.remove('FGOALS-s2')
# models.remove('bcc-csm1-1')
#T and/or  S filed are funny. making desnity splotch at high lats, and therefor eMLD go screwy
#models.remove('CCSM4')
#Funny last density year - need to resolve...cd Documentspyth
#Had to multiply salinity in last two files by 1000 to get in correct units...

#FGOALS-gl No sea-ice
#models.remove('FGOALS-s2')
#FGOALS models lack the required seaice data...
#and levels are upside down or something odd
#NOTE, now resolved by inverting levels using cdo invertlev ifile ofile

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


model_data = {}
ensemble = 'r1i1p1'


###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}

giss_test = 0

for i,model in enumerate(models_other_list):
	if model == 'GISS-E2-R':
		if giss_test == 0:
# 			pmip3_str[model] = max_strm_fun_26[i]
			pmip3_str[model] = max_strm_fun_45[i]
			pmip3_year_str[model] = model_years[i]
			giss_test += 1
	if model <> 'GISS-E2-R':
# 		pmip3_str[model] = max_strm_fun_26[i]
		pmip3_str[model] = max_strm_fun_45[i]
		pmip3_year_str[model] = model_years[i]


multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()
multi_model_density_yrs = multi_model_density.copy()


for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2 = signal.detrend(data)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	#data3 = data2
	#data3 -= np.nanmean(data3)
# 	window_type = 'boxcar'
# 	data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	for index,y_tmp in enumerate(expected_years):
	    loc2 = np.where(yrs == y_tmp)
	    if np.size(loc2) != 0:
	        multi_model_density[index,i] = data3[loc2]
	        multi_model_density_yrs[index,i] = y_tmp
#
#
# for i,model in enumerate(models):
# 	print model
# 	period = 'JJA'
# 	# period = 'annual_mean'
# 	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
# 	#try:
# 	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
# 	#except:
# 	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
# 	data = cube.data
# 	coord = cube.coord('time')
# 	dt = coord.units.num2date(coord.points)
# 	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
# 	#smoothing_var = 1
# 	#if smoothing_var > 0:
# #		data = gaussian_filter1d(data,smoothing_var)
# 	data2=signal.detrend(data)
# 	data2 = data2-np.min(data2)
# 	data3 = data2/(np.max(data2))
# 	# data3 = data2
# 	data3 -= np.nanmean(data3)
# 	for index,y in enumerate(expected_years):
# 	    loc2 = np.where(yrs == y)
# 	    if np.size(loc2) != 0:
# 	        multi_model_density[index,i] = data3[loc2]
# 	        multi_model_density_yrs[index,i] = y
#


##############################################
#            figure                         #
##############################################





#########
#density correlation spatial
#########



input_dir_1 = '/data/data1/ph290/cmip5/last1000/mld/deep/regridded/'
# input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'
input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger/'
# input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_no_spikes/'

five_yr_smooth_list = []
window_type = 'boxcar'

for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	cube2 = iris.load_cube(input_dir_1+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2 = signal.detrend(data)
	#data2 = data2-np.min(data2)
	#data3 = data2/(np.max(data2))
	#data3 -= np.nanmean(data3)
	data3 = data2
	data3 -= np.nanmean(data3)
# 	window_type = 'boxcar'
# 	data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	coord = cube2.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs2 = np.array([coord.units.num2date(value).year for value in coord.points])
	loc = np.where((yrs >= start_year) & (yrs <= end_year))
	loc2 = np.where((yrs2 >= end_year) & (yrs <= end_year))
	cube = cube[loc]
	cube2 = cube2[loc]
	ts = pandas.rolling_window(cube.data,5,win_type=window_type,center=True)
	loc3 = np.where(np.logical_not(np.isnan(ts)))
	five_yr_smooth_list.append(ts_model_corr(ts[loc3],cube2[loc3]))



##################
# R2 values denisty v. stream function
##################

print 'this takes agaes to run, so commented out for now...'


smoothings = np.linspace(1,200,200)

max_shift = 80
no_shifts = 40
coeff_det_dens_amoc = np.zeros([no_shifts,np.size(smoothings)])
coeff_det_dens_amoc[:] = np.NAN
coeff_det_dens_amoc2 = coeff_det_dens_amoc.copy()

#f, axarr = plt.subplots(4, 1)

for smoothing_no,smoothing in enumerate(smoothings):
	smoothing = np.int(smoothing)
	print smoothing_no,' out of ',np.size(smoothings)
	#make variable to hold multi model mean stream function data
	pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
	pmip3_model_streamfunction[:] = np.NAN
	# and mixed layer density
	pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
	pmip3_atl_tas = pmip3_model_streamfunction.copy()
	#variable holding analysis years
	expected_years = start_year+np.arange((end_year-start_year)+1)
	#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
	for i,model in enumerate(models):
		#print model
		tmp = pmip3_str[model]
		loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
		tmp = tmp[loc]
		yrs = pmip3_year_str[model][loc]
	# 	data3=signal.detrend(tmp)
	# 	data3 = data3-np.min(data3)
	# 	data3 = data3/(np.max(data3))
		for index,y_tmp in enumerate(expected_years):
			loc2 = np.where(yrs == y_tmp)
			if np.size(loc2) != 0:
				pmip3_model_streamfunction[index,i] = data3[loc2]
	# 	pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)
	####################################
	#as above but for density
	####################################
	for i,model in enumerate(models):
		#print model
		tmp = multi_model_density[:,i]
		#tmp = tmp[1:-1]
		#expected_years2 = expected_years[1:-1]
		yrs = multi_model_density_yrs[:,i]
	# 	data3=signal.detrend(tmp)
	# 	data3 = data3-np.min(data3)
	# 	data3 = data3/(np.max(data3))
		for index,y_tmp in enumerate(expected_years):
			loc2 = np.where(yrs == y_tmp)
			if np.size(loc2) != 0:
				pmip3_mixed_layer_density[index,i] = data3[loc2]
	#pmip3_multimodel_mean_mixed_layer_density = np.mean(pmip3_mixed_layer_density, axis = 1)
	mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
	stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
	#note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
	mean_strm = signal.detrend(mean_strm)
	mean_strm = mean_strm-np.min(mean_strm)
	mean_strm = mean_strm/(np.max(mean_strm))
	mean_strm -= np.nanmean(mean_strm)
	mean = np.nanmean(pmip3_mixed_layer_density, axis = 1)
	stdev = np.nanstd(pmip3_mixed_layer_density, axis = 1)
	mean = signal.detrend(mean)
	mean = mean_strm-np.min(mean)
	mean = mean_strm/(np.max(mean))
	mean -= np.nanmean(mean)
	X = expected_years
	window_type = 'boxcar'
	for i,shift in enumerate(np.round(np.linspace(0,max_shift,no_shifts))):
		if shift == 0:
			x = pandas.rolling_window(mean[0::],smoothing,win_type=window_type,center=True)
		else:
			x = pandas.rolling_window(mean[0:-1*shift],smoothing,win_type=window_type,center=True)
		y = pandas.rolling_window(mean_strm[shift::],smoothing,win_type=window_type,center=True)
		loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
		x = x[loc]
		y = y[loc]
	# 		x = x - np.min(x)
	# 		x = x / (np.max(x))
	# 		y = y - np.min(y)
	# 		y = y / (np.max(y))
		# r = scipy.stats.linregress(x,y)[2]
		slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
		#r2 = r_value**2
		#coeff_det_dens_amoc[i,smoothing_no] = r2
		#if p_value <= 0.001:
		r2 = r_value**2
		coeff_det_dens_amoc2[i,smoothing_no] = r2
		if p_value > 0.001:
			r2 = r_value**2
			coeff_det_dens_amoc[i,smoothing_no] = r2


### Remove three quotatoin marks below if running for 1st time after load

### Remove three quotatoin marks above if running for 1st tim eafter load


#######################
# Contour plot part
#######################

cmap1 = mpl_cm.get_cmap('RdBu_r')
cmap2 = mpl_cm.get_cmap('RdBu_r')

cmap2 = cmap1 = mpl_cm.get_cmap('Reds')
#gray_r
#z = np.nanmean(coeff_det_dens_amoc,axis = 0)
z = np.fliplr(np.rot90(coeff_det_dens_amoc,3))
z2 = np.fliplr(np.rot90(coeff_det_dens_amoc2,3))
zmin = 0.0
zmax = np.max(z2[np.where(np.logical_not(np.isnan(z2)))])
xlab , ylab = np.meshgrid(np.round(np.linspace(0,max_shift,no_shifts)),smoothings)

# fig, axarr = plt.subplots(1, 1, (figsize=(8, 4))
plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(10)
# ax1 = plt.subplot2grid((2,1),(1, 0))
#gs = gridspec.GridSpec(2, 1,width_ratios=[1,1],height_ratios =[1,2])
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.85)

# ax1 = plt.subplot(gs[1])
# ax2 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[0:44,0:100])
ax1 = plt.subplot(gs[56:100,62:95])
ax3 = plt.subplot(gs[56:100,97:100])
ax4 = plt.subplot(gs[56:100,0:33],projection=ccrs.PlateCarree())
ax5 = plt.subplot(gs[56:100,35:38])

#, rowspan=2)
C = ax1.contourf(xlab,ylab,z2,np.linspace(0,1.0,50),cmap=cmap1)
cb2 = plt.colorbar(C,ax=ax1,cax=ax3, ticks=[0.0, 0.2,0.4,0.6,0.8,1.0])
ax1.contourf(xlab,ylab,z,np.linspace(zmin,zmax,100),extend='both',hatches=['.'],cmap=cmap2)
CS = ax1.contour(xlab,ylab,z2,10,colors='k')
ax1.clabel(CS, fontsize=12, inline=1)
ax1.set_xlabel('AMOC lagging density (years)')
ax1.set_ylabel('smoothing (years)')
cb2.set_label('Variance explained (R$^2$)')

# plt.show(block = False)
# plt.savefig('/home/ph290/Documents/figures/amoc_density_lag_fig_111115.ps')
# plt.savefig('/home/ph290/Documents/figures/amoc_density_lag_fig_111115.png')



#######################
# timeseries part
#######################



#f, axarr = plt.subplots(4, 1)
# plt.close('all')
#make variable to hold multi model mean stream function data
pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
pmip3_model_streamfunction[:] = np.NAN
# and mixed layer density
pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
pmip3_atl_tas = pmip3_model_streamfunction.copy()
#variable holding analysis years
expected_years = start_year+np.arange((end_year-start_year)+1)
#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
for i,model in enumerate(models):
		print model
		tmp = pmip3_str[model]
		loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
		tmp = tmp[loc]
		yrs = pmip3_year_str[model][loc]
		data2=signal.detrend(tmp)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		for index,y_tmp in enumerate(expected_years):
			loc2 = np.where(yrs == y_tmp)
			if np.size(loc2) != 0:
				pmip3_model_streamfunction[index,i] = data2[loc2]
		####################################
		#as above but for density
		####################################
		for i,model in enumerate(models):
			#print model
			tmp = multi_model_density[:,i]
			#tmp = tmp[1:-1]
			#expected_years2 = expected_years[1:-1]
			yrs = multi_model_density_yrs[:,i]
			data2=signal.detrend(tmp)
			#data2 = data2-np.min(data2)
			#data3 = data2/(np.max(data2))
			for index,y_tmp in enumerate(expected_years):
				loc2 = np.where(yrs == y_tmp)
				if np.size(loc2) != 0:
					pmip3_mixed_layer_density[index,i] = data2[loc2]





mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
mean_dens = np.nanmean(pmip3_mixed_layer_density, axis = 1)
stdev_dens = np.nanstd(pmip3_mixed_layer_density, axis = 1)



# plt.close('all')


mean_dens -= np.nanmean(mean_dens)
mean_strm -= np.nanmean(mean_strm)

# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(mean_strm[i])+'\n')

f.close()



X = expected_years
smoothing_per = 5
shift = 10


y1 = pandas.rolling_window(mean_dens,smoothing_per,win_type=window_type,center=True)
y1a = y1 + pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)
y1b = y1 - pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)
ax2.plot(expected_years+shift,y1,'k',linewidth = 2.0,alpha = 0.8)
ax2.fill_between(expected_years+shift, y1a, y1b, color="none", facecolor='k', alpha=0.2)
# y1a = y1 + pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)*0.5
# y1b = y1 - pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)*0.5
# ax2.fill_between(expected_years, y1a, y1b, color="none", facecolor='k', alpha=0.2)

ax2b = ax2.twinx()
# mean_strm /= (1029.0*1.0e6) #convert to Sv
# stdev_strm /= (1029.0*1.0e6) #convert to Sv
y2 = pandas.rolling_window(mean_strm,smoothing_per,win_type=window_type,center=True)
y2a = y2 + pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)
y2b = y2 - pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)
ax2b.plot(expected_years,y2,'g',linewidth = 2.0,alpha = 0.8,label = 'PMIP3 26N AMOC strength')
ax2b.fill_between(expected_years, y2a, y2b, color="none", facecolor='g', alpha=0.2)
# y2a = y2 + pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)*0.5
# y2b = y2 - pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)*0.5
# ax2b.fill_between(expected_years, y2a, y2b, color="none", facecolor='g', alpha=0.2)




plt.xlabel('Calendar Year')
ax2.set_ylabel('PMIP3 North Iceland normalised density\nanomaly offset by +'+str(shift)+' years')
#\n(kgm$^{-3}$, shifted by 12 years)')
ax2b.set_ylabel('PMIP3 AMOC anomaly\nat 26$^o$N (Sv)')
ax2.set_xlabel('Calendar Year')
plt.xlim([950,1850])
ax2.set_ylim(-0.20,0.20)
ax2b.set_ylim(-1.5,1.5)
ax2b.yaxis.label.set_color('green')



###
#map plot
###
crs_latlon = ccrs.PlateCarree()
mean_cube = five_yr_smooth_list[0].copy()
mean_cube.data = np.mean([x.data for x in five_yr_smooth_list],axis = 0)


ax4.set_extent((-70, 10, 30, 80.0), crs=crs_latlon)

# Add coastlines and meridians/parallels (Cartopy-specific).
# ax.coastlines(linewidth=0.75, color='navy')
ax4.add_feature(cartopy.feature.LAND, zorder=0, edgecolor='black')
# ax.gridlines(crs=crs_latlon, linestyle='-')

# iplt.pcolormesh(mean_cube, cmap='RdBu_r')
# iplt.contourf(mean_cube, cmap='RdBu_r')

# my_plot = iplt.contourf(mean_cube, np.linspace(0,1.0,50), cmap='Reds')


lons = mean_cube.coord('longitude').points
lats = mean_cube.coord('latitude').points
data = mean_cube.data
my_plot = ax4.contourf(lons, lats, data, np.linspace(0,0.7,50),
            transform=ccrs.PlateCarree(),
            cmap='Reds')



cb3 = plt.colorbar(my_plot,ax=ax4,cax=ax5, ticks=[0.0,0.2,0.4,0.6,0.8,1.0])
# bar = ax4.colorbar(my_plot, orientation='horizontal')
# tick_levels = [0.0,0.2,0.4,0.6,0.8,1.0]
# cb3.set_ticks(tick_levels)
cb3.set_label('Correlation Coefficient')

ax4.add_feature(cartopy.feature.LAND,  facecolor='#D3D3D3',edgecolor='black')




x_lower = -24.0
x_upper = -4.0
y_lower = 65
y_upper = 70



# Draw a margin line, some way in from the border of the 'main' data...
box_x_points = x_lower + (x_upper - x_lower) * np.array([0, 1, 1, 0, 0])
box_y_points = y_lower + (y_upper - y_lower) * np.array([0, 0, 1, 1, 0])
# Get the Iris coordinate sytem of the X coordinate (Y should be the same).
#cs_data1 = x_coord.coord_system
# Construct an equivalent Cartopy coordinate reference system ("crs").
#crs_data1 = cs_data1.as_cartopy_crs()


new_cs = iris.coord_systems.GeogCS(iris.fileformats.pp.EARTH_RADIUS)
mean_cube.coord(axis='x').coord_system = new_cs
x_coord, y_coord = mean_cube.coord(axis='x'), mean_cube.coord(axis='y')
cs_data1 = x_coord.coord_system
crs_data1 = cs_data1.as_cartopy_crs()
# Draw the rectangle in this crs, with matplotlib "pyplot.plot".
# NOTE: the 'transform' keyword specifies a non-display coordinate system
# for the plot points (as used by the "iris.plot" functions).
ax4.plot(box_x_points, box_y_points, transform=crs_data1, linewidth=3.0, color='k', linestyle='-')
ax4.plot(box_x_points, box_y_points, transform=crs_data1, linewidth=1.0, color='y', linestyle='-')

# ax4.scatter(-18.2,66.53,'b')
ax4.scatter(-18.2,66.53, transform=crs_data1,color='b',marker = '*',s=150,lw = 0)
# ax4.scatter(-18.2,66.53, transform=crs_data1,color='y',marker = '*',s=20,lw = 0)
#, transform=crs_data1)


ax4.set_aspect('auto')







plt.annotate('a', xy=(.15, .925),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)
plt.annotate('b', xy=(.15, .5),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)
plt.annotate('c', xy=(.6, .5),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)






plt.savefig('/home/ph290/Documents/figures/amoc_density_fig2_250215_dfj_2.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_density_fig2_250215_dfj_2.png')

plt.show(block = False)

#####################
# COMMENT HERE WHEN RUNNING NOT FOR 1st TIME
#######################

#####################
# COMMENT HERE WHEN RUNNING NOT FOR 1st TIME
#######################


########################
# NEW combined figures 1
#######################


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
	models = np.unique(models_tmp)
	return models


#start_year = 850
start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)

##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=scipy.signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc[0]]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=scipy.signal.detrend(bivalve_data_initial)


#############################################################################################################################
# Models: remove linear trend and normalise variability, then put all models on same time axis and perform multi-model mean #
#############################################################################################################################


input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger6/'
#NOTE in this directoryt THE CRAZY LOW DENSITIES are set TO MISSING DATA

models = model_names(input_directory)
models = list(models)
# models.remove('MIROC-ESM')
#models.remove('FGOALS-gl')
models.remove('CSIRO-Mk3L-1-2')
#models.remove('HadGEM2-ES')
models.remove('HadCM3')

#models.remove('CCSM4')
#models.remove('FGOALS-s2')
#models.remove('MRI-CGCM3')
#models.remove('bcc-csm1-1')


#models.remove('CSIRO-Mk3L-1-2')
#models.remove('GISS-E2-R')
#models.remove('HadCM3')
#models.remove('HadGEM2-ES')
#models.remove('MIROC-ESM')
#models.remove('MPI-ESM-P')

# print '*******************************************'
# print '*******************************************'
# print ' As soon as processed bring MIROC BACK IN! '
# print '*******************************************'
# print '*******************************************'
# models.remove('MIROC-ESM') #temporarily until regridded...
#Note previously had two FGOALS models, sp removed one so a better ensmeble
#FGOALS-gl chosen, because there seems to be a problem calculating its mixed layer depths due to teh levels being upside down
models = np.array(models)

multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()

appending  = 'normalised_no_hadcm3_csiro'
for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2 = signal.detrend(data)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	#data3 = data2
	#data3 -= np.nanmean(data3)
 	#window_type = 'boxcar'
 	#data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
	for index,y_tmp in enumerate(expected_years):
		loc2 = np.where(yrs == y_tmp)
		if np.size(loc2) != 0:
			multi_model_density[index,i] = data3[loc2]



#smoothing_var = 1
#if smoothing_var > 0:
#	for i,model in enumerate(models):
#		#multi_model_density[:,i] = rm.running_mean(multi_model_density[:,i],smoothing_var)
#		multi_model_density[:,i] = gaussian_filter1d(multi_model_density[:,i],smoothing_var)

multi_model_mean_density = np.nanmean(multi_model_density, axis = 1)
multi_model_std_density = np.nanstd(multi_model_density, axis = 1)

# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/n_ice_dens'+appending+'.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(multi_model_mean_density[i])+'\n')

f.close()

window_type = 'boxcar'
y = pandas.rolling_window(multi_model_mean_density,5,win_type=window_type,center=True).copy()
y2 = multi_model_std_density.copy()

smoothing_var = 1.0

#y = gaussian_filter1d(y,smoothing_var)
window_type = 'boxcar'
#y = pandas.rolling_window(y,5,win_type=window_type,center=True)
y2 = pandas.rolling_window(y2,5,win_type=window_type,center=True)
# y2 = gaussian_filter1d(y2,smoothing_var)

cmap1 = mpl_cm.get_cmap('RdBu_r')

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 9}

import matplotlib
matplotlib.rc('font', **font)

# plt.rc('legend',**{'fontsize':8})
plt.close('all')
fig = plt.figure()
fig.set_figheight(2.0)
fig.set_figwidth(8)
gs = gridspec.GridSpec(100,100,bottom=0.2,left=0.10,right=0.85)

import cartopy.crs as ccrs


ax1a = plt.subplot(gs[0:85,0:20],projection=ccrs.PlateCarree())
ax1b = plt.subplot(gs[90:100,0:20])
ax1c = plt.subplot(gs[0:100,40:100])



#####  pannel a ###########
###
#map plot
###
crs_latlon = ccrs.PlateCarree()
mean_cube = five_yr_smooth_list[0].copy()
mean_cube.data = np.mean([x.data for x in five_yr_smooth_list],axis = 0)


ax1a.set_extent((-70, 10, 30, 80.0), crs=crs_latlon)

# Add coastlines and meridians/parallels (Cartopy-specific).
# ax.coastlines(linewidth=0.75, color='navy')
ax1a.add_feature(cartopy.feature.LAND, zorder=0, edgecolor='black')
# ax.gridlines(crs=crs_latlon, linestyle='-')

# iplt.pcolormesh(mean_cube, cmap='RdBu_r')
# iplt.contourf(mean_cube, cmap='RdBu_r')

# my_plot = iplt.contourf(mean_cube, np.linspace(0,1.0,50), cmap='Reds')


lons = mean_cube.coord('longitude').points
lats = mean_cube.coord('latitude').points
data = mean_cube.data
my_plot = ax1a.contourf(lons, lats, data, np.linspace(0,0.7,50),
            transform=ccrs.PlateCarree(),
            cmap='Reds')



cb3 = plt.colorbar(my_plot,ax=ax1a,cax=ax1b, orientation='horizontal', ticks=[0.0,0.2,0.4,0.6,0.8,1.0])
# bar = ax4.colorbar(my_plot, orientation='horizontal')
# tick_levels = [0.0,0.2,0.4,0.6,0.8,1.0]
# cb3.set_ticks(tick_levels)
cb3.set_label('Correlation coefficient')

ax1a.add_feature(cartopy.feature.LAND,  facecolor='#D3D3D3',edgecolor='black')




x_lower = -24.0
x_upper = -4.0
y_lower = 65
y_upper = 70



# Draw a margin line, some way in from the border of the 'main' data...
box_x_points = x_lower + (x_upper - x_lower) * np.array([0, 1, 1, 0, 0])
box_y_points = y_lower + (y_upper - y_lower) * np.array([0, 0, 1, 1, 0])
# Get the Iris coordinate sytem of the X coordinate (Y should be the same).
#cs_data1 = x_coord.coord_system
# Construct an equivalent Cartopy coordinate reference system ("crs").
#crs_data1 = cs_data1.as_cartopy_crs()


new_cs = iris.coord_systems.GeogCS(iris.fileformats.pp.EARTH_RADIUS)
mean_cube.coord(axis='x').coord_system = new_cs
x_coord, y_coord = mean_cube.coord(axis='x'), mean_cube.coord(axis='y')
cs_data1 = x_coord.coord_system
crs_data1 = cs_data1.as_cartopy_crs()
# Draw the rectangle in this crs, with matplotlib "pyplot.plot".
# NOTE: the 'transform' keyword specifies a non-display coordinate system
# for the plot points (as used by the "iris.plot" functions).
ax1a.plot(box_x_points, box_y_points, transform=crs_data1, linewidth=3.0, color='k', linestyle='-')
ax1a.plot(box_x_points, box_y_points, transform=crs_data1, linewidth=1.0, color='y', linestyle='-')

# ax4.scatter(-18.2,66.53,'b')
ax1a.scatter(-18.2,66.53, transform=crs_data1,color='b',marker = '*',s=150,lw = 0)
# ax4.scatter(-18.2,66.53, transform=crs_data1,color='y',marker = '*',s=20,lw = 0)
#, transform=crs_data1)


ax1a.set_aspect('auto')


##### end panel a ###########

#####  panel b ###########


import matplotlib.mlab as ml


smoothing_var = 1.0

#y = gaussian_filter1d(y,smoothing_var)
window_type = 'boxcar'
#y = pandas.rolling_window(y,5,win_type=window_type,center=True)
#y2 = pandas.rolling_window(y2,5,win_type=window_type,center=True)
# y2 = gaussian_filter1d(y2,smoothing_var)

tmp_min = np.nanmin(y)
y -= tmp_min
tmp_max = np.nanmax(y)
y /= tmp_max
tmp_mean = np.nanmean(y)
y -= tmp_mean

y2 -= tmp_min
y2 /= tmp_max
y2 -= tmp_mean

ax1c.plot(expected_years,y,'k',lw=1.5,alpha=0.8)
stdevs = 1.0

# ax3.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
# ax3.fill_between(expected_years, y + y2*stdevs*0.5, y  - y2*stdevs*0.5, color="none", facecolor='k', alpha=0.25)

ax1c.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
ax1c.fill_between(expected_years, y + y2*stdevs*0.8, y  - y2*stdevs*0.8, color="none", facecolor='k', alpha=0.1)
ax1c.fill_between(expected_years, y + y2*stdevs*0.6, y  - y2*stdevs*0.6, color="none", facecolor='k', alpha=0.1)
ax1c.fill_between(expected_years, y + y2*stdevs*0.4, y  - y2*stdevs*0.4, color="none", facecolor='k', alpha=0.1)
ax1c.fill_between(expected_years, y + y2*stdevs*0.2, y  - y2*stdevs*0.2, color="none", facecolor='k', alpha=0.1)


ax1c2 = ax1c.twinx()
# if smoothing_var > 0:
# 	#y_bivalve = rm.running_mean(bivalve_data_initial,smoothing_var)
# 	y_bivalve = gaussian_filter1d(bivalve_data_initial,smoothing_var*1.0)
# 	#NOTE, I've doubled the smoothing on the bivalve data, because it is not getting the smoothing of an ensemble mean - not sure what the right thing to choose here is. Doubling is a bit arbitrary
# else:

# y_bivalve = rm.running_mean(bivalve_data_initial,5)
bivalve_year = bivalve_year[::-1]
bivalve_data_initial = bivalve_data_initial[::-1]
# y_bivalve = gaussian_filter1d(bivalve_data_initial.copy(),smoothing_var)
y_bivalve = pandas.rolling_window(bivalve_data_initial,5,win_type=window_type,center=True)
# y_bivalve = bivalve_data_initial


# y_bivalve -= np.nanmean(y_bivalve)
ax1c2.plot(bivalve_year,y_bivalve,'r',lw=1.5,alpha=0.6)
# ax2.plot(bivalve_year+20,y_bivalve,'g',lw=2,alpha=0.75)
#ax2.plot(bivalve_year[0:700],y_bivalve[0:700]*0.5,'r',lw=1.75,alpha=0.5)
#ax2.plot(bivalve_year[-200::]+20,y_bivalve[-200::]*0.5,'g',lw=1.75,alpha=0.5)
# ax1c.set_ylim(-0.15,0.15)
ax1c.set_ylim(-0.75,0.75)

ax1c2.set_ylim(-0.6,0.6)

ax1c.set_xlim(950,1850)
ax1c.set_xlabel('Calendar Year')
ax1c.set_ylabel('PMIP3 North Iceland\nnormalsied density anomaly')
ax1c2.set_ylabel('Bivalve $\delta^{18}$O anomaly')
ax1c2.yaxis.label.set_color('red')

plt.annotate('a', xy=(.1, .95),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=12)

plt.annotate('b', xy=(.4, .95),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=12)





plt.savefig('/home/ph290/Documents/figures/palaeo_amo_denisty_bivalve_dfj_2.png')
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_denisty_bivalve_dfj_2.ps')

# plt.show(block = True)



########################
# NEW combined figures 2
#######################




cmap1 = mpl_cm.get_cmap('RdBu_r')

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 9}

import matplotlib
matplotlib.rc('font', **font)

# plt.rc('legend',**{'fontsize':8})
plt.close('all')
fig = plt.figure()
fig.set_figheight(6)
fig.set_figwidth(8)
gs = gridspec.GridSpec(100,100,bottom=0.1,left=0.15,right=0.9)

import cartopy.crs as ccrs

ax2c = plt.subplot(gs[0:35,0:55])
ax2a = plt.subplot(gs[0:23,80:100])
ax2b = plt.subplot(gs[33:35,80:100])
ax3a = plt.subplot(gs[55:90,0:45],projection=ccrs.PlateCarree())
ax3b = plt.subplot(gs[55:90,55:100],projection=ccrs.PlateCarree())



##############
# AMOC-density
#############



#######################
# Contour plot part
#######################

cmap1 = mpl_cm.get_cmap('RdBu_r')
cmap2 = mpl_cm.get_cmap('RdBu_r')

cmap2 = cmap1 = mpl_cm.get_cmap('Reds')
#gray_r
#z = np.nanmean(coeff_det_dens_amoc,axis = 0)
z = np.fliplr(np.rot90(coeff_det_dens_amoc,3))
z2 = np.fliplr(np.rot90(coeff_det_dens_amoc2,3))
zmin = 0.0
zmax = np.max(z2[np.where(np.logical_not(np.isnan(z2)))])
xlab , ylab = np.meshgrid(np.round(np.linspace(0,max_shift,no_shifts)),smoothings)


#, rowspan=2)
C = ax2a.contourf(xlab,ylab,z2,np.linspace(0,1.0,50),cmap=cmap1)
cb2 = plt.colorbar(C,ax=ax2a,cax=ax2b, ticks=[0.0, 0.2,0.4,0.6,0.8,1.0],orientation = 'horizontal')
ax2a.contourf(xlab,ylab,z,np.linspace(zmin,zmax,100),extend='both',hatches=['.'],cmap=cmap2)
CS = ax2a.contour(xlab,ylab,z2,8,colors='k')
ax2.clabel(CS, fontsize=6, inline=1)
ax2a.set_xticks([10,30,50,70])
ax2a.set_xlabel('AMOC lagging density (years)')
ax2a.set_ylabel('Smoothing (years)')
cb2.set_label('Variance explained (R$^2$)')

###########
# amoc density timeseries
#############


#f, axarr = plt.subplots(4, 1)
# plt.close('all')
#make variable to hold multi model mean stream function data
pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
pmip3_model_streamfunction[:] = np.NAN
# and mixed layer density
pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
pmip3_atl_tas = pmip3_model_streamfunction.copy()
#variable holding analysis years
expected_years = start_year+np.arange((end_year-start_year)+1)
#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
for i,model in enumerate(models):
		print model
		tmp = pmip3_str[model]
		loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
		tmp = tmp[loc]
		yrs = pmip3_year_str[model][loc]
		data2=signal.detrend(tmp)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		for index,y_tmp in enumerate(expected_years):
			loc2 = np.where(yrs == y_tmp)
			if np.size(loc2) != 0:
				pmip3_model_streamfunction[index,i] = data2[loc2]
		####################################
		#as above but for density
		####################################
		for i,model in enumerate(models):
			#print model
			tmp = multi_model_density[:,i]
			#tmp = tmp[1:-1]
			#expected_years2 = expected_years[1:-1]
			yrs = multi_model_density_yrs[:,i]
# 			data2=signal.detrend(tmp)
			data2 = signal.detrend(tmp)
			data2 = data2-np.min(data2)
			data3 = data2/(np.max(data2))
			data3 -= np.nanmean(data3)
			#data2 = data2-np.min(data2)
			#data3 = data2/(np.max(data2))
			for index,y_tmp in enumerate(expected_years):
				loc2 = np.where(yrs == y_tmp)
				if np.size(loc2) != 0:
					pmip3_mixed_layer_density[index,i] = data3[loc2]





mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
mean_dens = np.nanmean(pmip3_mixed_layer_density, axis = 1)
stdev_dens = np.nanstd(pmip3_mixed_layer_density, axis = 1)



# plt.close('all')


mean_dens -= np.nanmean(mean_dens)
mean_strm -= np.nanmean(mean_strm)



# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(mean_strm[i])+'\n')

f.close()



X = expected_years
smoothing_per = 5
shift = 10


y1 = pandas.rolling_window(mean_dens,smoothing_per,win_type=window_type,center=True)

y1a = y1 + pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)
y1b = y1 - pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)

tmp_min = np.nanmin(y1)
y1 -= tmp_min
tmp_max = np.nanmax(y1)
y1 /= tmp_max
tmp_mean = np.nanmean(y1)
y1 -= tmp_mean

y1a -= tmp_min
y1a /= tmp_max
y1a -= tmp_mean
y1b -= tmp_min
y1b /= tmp_max
y1b -= tmp_mean


ax2c.plot(expected_years+shift,y1,'k',linewidth = 1.5,alpha = 0.8)
ax2c.fill_between(expected_years+shift, y1a, y1b, color="none", facecolor='k', alpha=0.2)
# y1a = y1 + pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)*0.5
# y1b = y1 - pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)*0.5
# ax2.fill_between(expected_years, y1a, y1b, color="none", facecolor='k', alpha=0.2)

ax2c2 = ax2c.twinx()
# mean_strm /= (1029.0*1.0e6) #convert to Sv
# stdev_strm /= (1029.0*1.0e6) #convert to Sv
y2 = pandas.rolling_window(mean_strm,smoothing_per,win_type=window_type,center=True)
y2a = y2 + pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)
y2b = y2 - pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)
ax2c2.plot(expected_years,y2,'g',linewidth = 1.0,alpha = 0.8,label = 'PMIP3 26N AMOC strength')
ax2c2.fill_between(expected_years, y2a, y2b, color="none", facecolor='g', alpha=0.2)
# y2a = y2 + pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)*0.5
# y2b = y2 - pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)*0.5
# ax2b.fill_between(expected_years, y2a, y2b, color="none", facecolor='g', alpha=0.2)




plt.xlabel('Calendar year')
ax2c.set_ylabel('PMIP3 North Iceland normalised density\nanomaly offset by +'+str(shift)+' years')
#\n(kgm$^{-3}$, shifted by 12 years)')
ax2c2.set_ylabel('PMIP3 AMOC anomaly\nat 26$^o$N (Sv)')
ax2c.set_xlabel('Calendar year')
plt.xlim([950,1850])
ax2c.set_ylim(-0.75,0.75)
ax2c2.set_ylim(-1.5,1.5)
ax2c2.yaxis.label.set_color('green')





#################
#psl
################

import matplotlib.mlab as ml
cmap1 = mpl_cm.get_cmap('RdBu_r')


# plt.close('all')
# plt.title('PMIP3 Sea Level Pressure composites',  fontsize=10)
# ax = ax1.subplot(122)
cube = ens_mean_region
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
data = cube.data
C1 = ax3b.contourf(lons, lats, data, np.linspace(-150,150,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax3b.coastlines()
ax3b.set_title('PMIP3: high minus\nlow density years',  fontsize=9)
#,  fontsize=12
cb1 = plt.colorbar(C1,ax=ax3b,orientation='horizontal',ticks=[-120,-60, 0,60,120])
cb1.set_label('Sea Level Pressure (Pa)')

ax3a.set_aspect('auto')
ax3b.set_aspect('auto')

#,cax=ax3)
# plt.colorbar(orientation='horizontal')

#####
# HadSLP analysis
#####
"""
file = '/data/NAS-geo01/ph290/misc_data/slp.mnmean.nc'
# hadslp2

hasdlp_cube = iris.load_cube(file,'Monthly Mean Sea Level Pressure')
iris.coord_categorisation.add_year(hasdlp_cube, 'time', name='year')
hasdlp_cube = hasdlp_cube.aggregated_by('year', iris.analysis.MEAN)
coord = hasdlp_cube.coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])
"""

file = '/data/NAS-geo01/ph290/misc_data/slp.mnmean.nc'
# hadslp2

hasdlp_cube = iris.load_cube(file,'Monthly Mean Sea Level Pressure')
iris.coord_categorisation.add_season_year(hasdlp_cube, 'time', name='season_year')
iris.coord_categorisation.add_season_number(hasdlp_cube, 'time', name='season_number')
hasdlp_cube = hasdlp_cube.aggregated_by(['season_year','season_number'], iris.analysis.MEAN)
iris.coord_categorisation.add_season(hasdlp_cube, 'time', name='season_name')
loc = np.where(hasdlp_cube.coord('season_name').points == 'djf')
hasdlp_cube = hasdlp_cube[loc]
#iris.coord_categorisation.add_year(hasdlp_cube, 'time', name='year')
#hasdlp_cube = hasdlp_cube.aggregated_by('year', iris.analysis.MEAN)
coord = hasdlp_cube.coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])


#######################
# volcanic data
#######################


post_volc_years = np.array([1883,1884,1886,1887,1888,1889,1903,1904,1932,1933,1963,1964,1975,1976,1982,1983,1991,1992])
#Following: http://onlinelibrary.wiley.com/store/10.1029/2012JD017607/asset/jgrd18164.pdf;jsessionid=9FA5552A7B4590F363FB33C5586598EE.f01t01?v=1&t=ijercsq6&s=533a6d76025e2bc2a7770af392b2123c8efbb706
#i.e. two years following a major low latitude volcanic erruption

common_years_high = np.intersect1d(year,post_volc_years)
loc1 = np.where(np.in1d(year,common_years_high))
loc2 = np.where(np.logical_not(np.in1d(year,common_years_high)))


out_cube = hasdlp_cube[loc1].collapsed('time',iris.analysis.MEAN) - hasdlp_cube[loc2].collapsed('time',iris.analysis.MEAN)

cube_region_tmp = out_cube.intersection(longitude=(lon_west, lon_east))
out_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

####
# PSL plotting
####


cube = out_cube_region * 100.0 #conversion from mb to Pa
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
data = cube.data
C2 = ax3a.contourf(lons, lats, data, np.linspace(-150,150,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax3a.coastlines()
ax3a.set_title('Observations: eruption\nminus no-eruption',  fontsize=9)
#HadSLP2.
cb2 = plt.colorbar(C2,ax=ax3a,orientation='horizontal',ticks=[-120,-60, 0, 60,120])
cb2.set_label('Sea Level Pressure (Pa)')

plt.annotate('a', xy=(.15, .93),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=12)

plt.annotate('b', xy=(.75, .93),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=12)



plt.annotate('c', xy=(.15, .485),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=12)

plt.annotate('d', xy=(.57, .485),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=12)




plt.savefig('/home/ph290/Documents/figures/palaeo_amo_denisty_amoc_bivalve_map_correlation_dfj_2.png')
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_denisty_amoc_bivalve_map_correlation_dfj_2.ps')

# plt.show(block = False)

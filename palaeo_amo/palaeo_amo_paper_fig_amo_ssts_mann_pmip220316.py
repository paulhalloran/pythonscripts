######## Remove if running for 1st time
######## Remove if running for 1st time

from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean as rm
import running_mean_post as rmp
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import scipy.interpolate
import gc
import pickle
import biggus
import seawater
import cartopy.feature as cfeature
import scipy.ndimage
import scipy.ndimage.filters
import gsw
import scipy.stats as stats
import time
import matplotlib.patches as mpatches
from scipy.ndimage import gaussian_filter1d
import pandas


def extract_years(cube):
    try:
    	iris.coord_categorisation.add_year(cube, 'time', name='year2')
    except:
    	'already has year2'
    start_year = 850
    end_year = 1850
    loc = np.where((cube.coord('year2').points >= start_year) & (cube.coord('year2').points <= end_year))
    loc2 = cube.coord('time').points[loc[0][-1]]
    cube = cube.extract(iris.Constraint(time = lambda time_tmp: time_tmp <= loc2))
    return cube

def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a



def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


def butter_bandpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


start_year  = 950
#note that it is 1000 because FGOALS-gl starts in the year 1000
end_year = 1850


###
#read in volc data
###

#Crowley
file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)
data3 = np.genfromtxt(file3)
data4 = np.genfromtxt(file4)

data_tmp = np.zeros([data1.shape[0],2])
data_tmp[:,0] = data1[:,1]
data_tmp[:,1] = data2[:,1]
data = np.mean(data_tmp,axis = 1)
voln_n = data1.copy()
voln_n[:,1] = data

data_tmp[:,0] = data3[:,1]
data_tmp[:,1] = data4[:,1]
data = np.mean(data_tmp,axis = 1)
voln_s = data1.copy()
voln_s[:,1] = data

data_tmp[:,0] = data2[:,1]
data_tmp[:,1] = data4[:,1]
data = np.mean(data_tmp,axis = 1)
vol_eq = data1.copy()
vol_eq[:,1] = data

data_tmp = np.zeros([data1.shape[0],4])
data_tmp[:,0] = data2[:,1]
data_tmp[:,1] = data4[:,1]
data_tmp[:,2] = data1[:,1]
data_tmp[:,3] = data3[:,1]
data = np.mean(data_tmp,axis = 1)
vol_globe = data1.copy()
vol_globe[:,1] = data

#Gao-Robock-Ammann
# file = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/IVI2TotalInjection_501-2000Version2.txt'
# data = np.genfromtxt(file,skip_header = 13)
# vol_globe_GRA = np.zeros([data.shape[0],2])
# vol_north_GRA = np.zeros([data.shape[0],2])
# vol_globe_GRA[:,0] = data[:,0]
# vol_globe_GRA[:,1] = data[:,3]
# vol_north_GRA[:,0] = data[:,0]
# vol_north_GRA[:,1] = data[:,1]

#crowley = vol_globe
crowley = voln_n
#GRA = vol_globe_GRA
# GRA = vol_north_GRA
#


with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_VI.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)

models = np.array(models)

models = list(models)
models.remove('FGOALS-gl')
models.remove('HadCM3')
models.remove('CSIRO-Mk3L-1-2')
#Note previously had two FGOALS models, sp removed one so a better ensmeble
#FGOALS-gl chosen, because there seems to be a problem calculating its mixed layer depths due to teh levels being upside down
models = np.array(models)


###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}
pmip3_atl_tos = {}
pmip3_atl_tas = {}

giss_test = 0

for i,model in enumerate(models):
	if model == 'GISS-E2-R':
		if giss_test == 0:
			pmip3_str[model] = max_strm_fun_26[i]
			pmip3_year_str[model] = model_years[i]
			giss_test += 1
	if model <> 'GISS-E2-R':
		pmip3_str[model] = max_strm_fun_26[i]
		pmip3_year_str[model] = model_years[i]


model_data_atlantic = {}

for model in models:
    print model
    model_data_atlantic[model] = {}
    model_data_atlantic[model]['tos'] = extract_years(iris.load_cube(directory+model+'*_tos_past1000_r1i1p1_*.nc'))
    model_data_atlantic[model]['tas'] = extract_years(iris.load_cube(directory+model+'*_tas_past1000_r1i1p1_*.nc'))



##############################################
#            figure                         #
##############################################

plt.rc('legend',**{'fontsize':10})

#read in, linearly detrend and normalise Reynolds d18O data
r_data_file = '/data/NAS-geo01/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=signal.detrend(bivalve_data_initial)

#read in, linearly detrend and normalise Mann data
amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)
amo_yr = amo[:,0]
amo_data = amo[:,1]
amo_data_minus_2sigma = amo[:,2]
amo_data_sigma_diff = amo_data_minus_2sigma + amo_data
loc = np.where((np.logical_not(np.isnan(amo_data_sigma_diff))) & (np.logical_not(np.isnan(amo_data))) & (amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
amo_data_sigma_diff = amo_data_sigma_diff[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data=signal.detrend(amo_data)

#read in, linearly detrend and normalise Mann data (Screened component)
amo_file_screened = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo_screened = np.genfromtxt(amo_file_screened, skip_header = 1)
amo_yr_screened = amo_screened[:,0]
amo_data_screened = amo_screened[:,1]
amo_data_minus_2sigma_screened = amo_screened[:,2]
amo_data_sigma_diff_screened = amo_data_minus_2sigma_screened + amo_data_screened
loc = np.where((np.logical_not(np.isnan(amo_data_screened))) & (amo_yr_screened >= start_year) & (amo_yr_screened <= end_year))
amo_yr_screened = amo_yr_screened[loc]
amo_data_screened = amo_data_screened[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data_screened=signal.detrend(amo_data_screened)

#read in, linearly detrend and normalise Gray AMO data
amo_file_gray = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoscr.txt'
amo_gray = np.genfromtxt(amo_file_gray, skip_header = 101)
amo_yr_gray = amo_gray[:,0]
amo_data_gray = amo_gray[:,1]
amo_data_minus_2sigma_gray = amo_gray[:,2]
loc = np.where((np.logical_not(np.isnan(amo_data_gray))) & (amo_yr_gray >= start_year) & (amo_yr_gray <= end_year))
amo_yr_gray = amo_yr_gray[loc]
amo_data_gray = amo_data_gray[loc]
#amo_data -= np.min(amo_data)
#amo_data_sigma_diff-= np.min(amo_data_sigma_diff)
#amo_data /= np.max(amo_data)
#amo_data_sigma_diff /= np.max(amo_data)
amo_data_gray=signal.detrend(amo_data_gray)

#read in, linearly detrend and normalise Briffa N. Hem data
nh_briff_file = '/data/NAS-geo01/ph290/misc_data/briffa/nhemtemp_data.txt'
nh_briff = np.genfromtxt(nh_briff_file, skip_header = 58)
nh_briff_yr = nh_briff[:,0]
nh_briff_data = nh_briff[:,1]
loc = np.where((np.logical_not(np.isnan(nh_briff_data))) & (nh_briff_yr >= start_year) & (nh_briff_yr <= end_year))
nh_briff_yr = nh_briff_yr[loc]
nh_briff_data = nh_briff_data[loc]
#nh_briff_data -= np.min(nh_briff_data)
#nh_briff_data /= np.max(nh_briff_data)
nh_briff_data=signal.detrend(nh_briff_data)

no_smoothing_steps = 20
coeff_det = np.zeros([np.size(models),no_smoothing_steps])
coeff_det[:] = np.NAN

#f, axarr = plt.subplots(4, 1)
f, axarr = plt.subplots(2, 1)
all_models = models.copy()
all_models= list(all_models)
#make variable to hold multi model mean stream function data
pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(all_models)])
pmip3_model_streamfunction[:] = np.NAN
# and mixed layer density
pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
pmip3_atl_tos = pmip3_model_streamfunction.copy()
pmip3_n_hem_tas = pmip3_model_streamfunction.copy()

#variable holding analysis years
expected_years = start_year+np.arange((end_year-start_year)+1)

#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
for i,model in enumerate(all_models):
		print model
		tmp = pmip3_str[model]
		loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
		tmp = tmp[loc]
		yrs = pmip3_year_str[model][loc]
		data2=signal.detrend(tmp)
		data2 = data2-np.min(data2)
		data3 = data2/(np.max(data2))
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				pmip3_model_streamfunction[index,i] = data3[loc2]


pmip3_multimodel_mean_streamfunction = np.mean(pmip3_model_streamfunction, axis = 1)


west = -75
east = -7.5
south = 0
north = 65
#as above but for AMO box tos:
for i,model in enumerate(all_models):
		print model
		cube = model_data_atlantic[model]['tos']
		cube = cube.intersection(longitude=(west, east))
		cube = cube.intersection(latitude=(south, north))
		try:
			cube.coord('latitude').guess_bounds()
		except:
			print 'cube already has latitude bounds'
		try:
			cube.coord('longitude').guess_bounds()
		except:
			print 'cube already has longitude bounds'
		grid_areas = iris.analysis.cartography.area_weights(cube)
		tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		data2=signal.detrend(tmp)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				pmip3_atl_tos[index,i] = data2[loc2]



west = 0
east = 360
south = 0
north = 90
#as above but for N hem tss:
for i,model in enumerate(all_models):
		print model
		cube = model_data_atlantic[model]['tos']
		cube = cube.intersection(longitude=(west, east))
		cube = cube.intersection(latitude=(south, north))
		try:
			cube.coord('latitude').guess_bounds()
		except:
			print 'cube already has latitude bounds'
		try:
			cube.coord('longitude').guess_bounds()
		except:
			print 'cube already has longitude bounds'
		grid_areas = iris.analysis.cartography.area_weights(cube)
		tmp = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas).data
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		data2=signal.detrend(tmp)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				pmip3_n_hem_tas[index,i] = data2[loc2]


pmip3_multimodel_n_hem_tas = np.mean(pmip3_n_hem_tas, axis = 1)
pmip3_multimodel_n_hem_tas_stdv = np.nanstd(pmip3_n_hem_tas, axis = 1)

######## Remove if running for 1st time
######## Remove if running for 1st time

##############################
# 1st figure mann_amo SST
##############################

plt.close('all')
f, axarr = plt.subplots(1, 1, figsize=(8, 3))

mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
#note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
mean_strm -= np.nanmean(mean_strm)
mean_tos = np.nanmean(pmip3_atl_tos, axis = 1)
stdev_tos = np.nanstd(pmip3_atl_tos, axis = 1)
mean_tos -= np.nanmean(mean_tos)
#mean *= 3.0
X = expected_years
#plt.close('all')
# fig, ax =plt.subplots(1)
steps = 50.0
# for i in np.arange(steps):
#     ax.fill_between(X, rm.running_mean(mean,20)+rm.running_mean(stdev,20)*(i/steps),rm.running_mean(mean,20)-rm.running_mean(stdev,20)*(i/steps), facecolor='k',edgecolor='', alpha=1.0/(i+1.0))
smoothing_per = 5
window_type = 'boxcar'
if smoothing_per > 0:
	axarr.plot(expected_years,rm.running_mean(mean_tos,smoothing_per),'k',linewidth = 2.0,alpha = 0.9,label = 'PMIP3 AMV index')
	y = mean_tos
	y2 = mean_tos + stdev_tos * 1.0
	y3 = mean_tos - stdev_tos * 1.0
	#y = rm.running_mean(y,smoothing_per)
	#y2 = rm.running_mean(y2,smoothing_per)
	#y3 = rm.running_mean(y3,smoothing_per)
	y = pandas.rolling_window(y,smoothing_per,win_type=window_type,center=True)
	y2 = pandas.rolling_window(y2,smoothing_per,win_type=window_type,center=True)
	y3 = pandas.rolling_window(y3,smoothing_per,win_type=window_type,center=True)
	#     y =  gaussian_filter1d(y,smoothing_per)
	#     y2 =  gaussian_filter1d(y2,smoothing_per)
	#     y3 =  gaussian_filter1d(y3,smoothing_per)
	axarr.fill_between(expected_years, y2, y3, color="none", facecolor='k', alpha=0.3)
else:
    axarr.plot(expected_years,mean_tos,'k',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 AMV index')
    y = mean_tos
    y2 = mean_tos + stdev_tos * 1.0
    y3 = mean_tos - stdev_tos * 1.0
    axarr.fill_between(expected_years, y2, y3, color="none", facecolor='k', alpha=0.3)

# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/pmip3_tas.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(y[i])+'\n')

f.close()


if smoothing_per > 0:
    #axarr.plot(amo_yr,rm.running_mean(amo_data,smoothing_per),'b',linewidth = 2.0,alpha = 0.8,label = 'Mann AMO index')
# 	axarr.plot(amo_yr,gaussian_filter1d(amo_data,smoothing_per),'b',linewidth = 2.0,alpha = 0.8,label = 'Proxy reconstructed AMO index')
	axarr.plot(amo_yr,pandas.rolling_window(amo_data,smoothing_per,win_type=window_type,center=True),'b',linewidth = 2.0,alpha = 0.8,label = 'Proxy reconstructed AMV index')
else:
    axarr.plot(amo_yr,amo_data,'b',linewidth = 2.0,alpha = 0.8,label = 'Proxy reconstructed AMV index')


axarr_xtwin = axarr.twinx()
############
# volc_data = -11.3 * (1 - np.exp(-0.164 * crowley[:,1]))
volc_data = crowley[:,1]
##############
p2, = axarr_xtwin.plot(crowley[:,0],volc_data,'orange',linewidth = 1.5,alpha = 0.5,label = 'Volcanic AOD (Crowley et al., 2013)')
axarr_xtwin.yaxis.label.set_color(p2.get_color())
axarr_xtwin.set_ylim([0,1])
# for tl in axarr_xtwin.get_yticklabels():
#     tl.set_color('r')

#GRA


axarr.legend(loc = 2, fancybox=True, framealpha=0.2, ncol=2,frameon=False)
# axarr.legend(loc = 3, fancybox=True, framealpha=0.8)

'''

smoothing_per=20.0
y = amo_data
x1 = rm.running_mean(mean_tos[:-1],smoothing_per)
x2 = rm.running_mean(mean_strm[:-1],smoothing_per)
# x2 = mean_strm[:-1]

x = np.column_stack((x1,x2))
#stack explanatory variables into an array

x = sm.add_constant(x)
#add constant to first column for some reasons

model = sm.OLS(y,x)
results = model.fit()

plt.plot(y)
plt.plot(results.params[2]*x2+results.params[1]*x1+results.params[0])
plt.show()

smoothing_per=10.0

plt.close('all')
fig, ax1 = plt.subplots()
ax1.plot(amo_data,'b',lw = 2)
ax2 = ax1.twinx()
ax2.plot(rm.running_mean(mean_strm[:-1],smoothing_per),'g',lw = 2)
ax3 = ax1.twinx()
#ax2.plot(rm.running_mean(mean_tos[:-1],smoothing_per),'k',lw = 2)
plt.show()

smoothing_per=10.0

plt.close('all')
fig, ax1 = plt.subplots()
ax1.plot(amo_data,'b',lw = 2)
ax2 = ax1.twinx()
ax2.plot(rm.running_mean(mean_strm[:-1],smoothing_per),'g',lw = 2)
ax3 = ax1.twinx()
#ax2.plot(rm.running_mean(mean_tos[:-1],smoothing_per),'k',lw = 2)
plt.show()smoothing_per=10.0

plt.close('all')
fig, ax1 = plt.subplots()
ax1.plot(amo_data,'b',lw = 2)
ax2 = ax1.twinx()
y1 = rm.running_mean(mean_strm[:-1],smoothing_per)*-3.0
y2 = rm.running_mean(mean_tos[:-1],smoothing_per)
ax2.plot(y1 + y2,'g',lw = 2)
plt.show(block = False)

'''


# if smoothing_per > 0:
#     y = amo_data - mean_tos[:-1]
#     y2 = rm.running_mean(y,smoothing_per)
#     axarr[1].plot(expected_years[:-1],y2,'g',linewidth = 2.0,alpha = 0.5,label = 'Mann minus PMIP3 AMO')
# else:
#     y = amo_data - mean_tos[:-1]
#     axarr[1].plot(expected_years[:-1],y2,'g',linewidth = 2.0,alpha = 0.5,label = 'Mann minus PMIP3 AMO')
#
# axarr[1].legend()

axarr.set_xlabel('Calendar Year')
# axarr[1].set_xlabel('Calendar Year')
# axarr[1].set_ylabel('AMO SST anomaly ($^o$C)')
axarr.set_ylabel('AMV SST anomaly ($^o$C)')
axarr_xtwin.set_ylabel('Volcanic aerosol optical depth')
# axarr[1].set_xlim([1000,1850])
axarr.set_xlim([950,1850])
axarr.set_ylim([-0.4,0.35])
plt.tight_layout()
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/amoc_amo_tas3.svg')
plt.savefig('/home/ph290/Documents/figures/amoc_amo_tas3.png')

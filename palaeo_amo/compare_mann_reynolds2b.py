import numpy as np
import matplotlib.pyplot as plt
import running_mean as rm
import running_mean_post as rmp
import scipy
import scipy.signal
import scipy.stats as stats
import matplotlib.gridspec as gridspec
import pickle
import os
import pandas
from timer import Timer


def main_calculations(start_year,end_year,density,amo,r_data,data1,data2,density_smoothing,volc_smoothing,density_factor,volc_factor):
	##############
	# PMIP3 density
	##############
	window_type = 'boxcar'
	density_yr = density[:,0].copy()
	density_data = density[:,1].copy()
	density_data = np.roll(density_data,10,axis = 0)
	density_data[0:11] = np.NAN
	loc = np.where((density_yr >= start_year) & (density_yr <= end_year))
	density_yr = density_yr[loc]
	density_data = density_data[loc]
	#density_data = scipy.signal.detrend(density_data, axis=0)
	#density_data -= np.min(density_data)
	#density_data /= np.max(density_data)
	#density_data = rm.running_mean(density_data,density_smoothing)
	density_data = pandas.rolling_window(density_data,density_smoothing,win_type=window_type,center=True)
	#############
	# Mann AMO
	#############
	amo_yr = amo[:,0].copy()
	amo_data = amo[:,1].copy()
	loc = np.where((amo_yr >= start_year) & (amo_yr <= end_year))
	amo_yr = amo_yr[loc]
	amo_data = amo_data[loc]
	#############
	# Bivalve
	#############
	bivalve_yr = r_data[:,0].copy()
	bivalve_data = r_data[:,1].copy()
	loc = np.where((bivalve_yr >= start_year) & (bivalve_yr <= end_year))
	bivalve_yr = bivalve_yr[loc]
	bivalve_data = bivalve_data[loc]
	#bivalve_data = scipy.signal.detrend(bivalve_data, axis=0)
	bivalve_data = rm.running_mean(bivalve_data,density_smoothing)
	bivalve_yr = bivalve_yr[::-1]
	bivalve_data = bivalve_data[::-1]
	#############
	# Volc. Crow.
	#############
	data_tmp = np.zeros([data1.shape[0],2])
	data_tmp[:,0] = data1[:,1].copy()
	data_tmp[:,1] = data2[:,1].copy()
	data = np.mean(data_tmp,axis = 1)
	voln_n = data1.copy()
	voln_n[:,1] = data
	volc_data = voln_n[:,1]
	volc_yr = voln_n[:,0]
	loc = np.where((volc_yr >= start_year) & (volc_yr <= end_year))
	volc_yr = volc_yr[loc]
	volc_data = volc_data[loc]
	tmp = np.round(volc_yr)
	tmp_unique = np.unique(tmp)
	volc_yr2 = tmp_unique
	volc_data2 = tmp_unique.copy()
	for count,yr in enumerate(volc_yr2):
		loc = np.where(tmp == yr)
		volc_data2[count] = np.mean(volc_data[loc])
	#calculating volcanic forcing from AOD, following Harris and Highwood 2011
	volc_data2 = -11.3 * (1 - np.exp(-0.164*volc_data2))
	#volc_data2 = rm.running_mean(volc_data2,volc_smoothing)
	volc_data2 = pandas.rolling_window(volc_data2,volc_smoothing,win_type=window_type,center=True)
	############
	# Getting rid of nans
	############
	loc_smooth = np.where(np.logical_not((np.isnan(volc_data2)) | (np.isnan(density_data))))
	#amo
	amo_yr = amo_yr[loc_smooth]
	amo_data = amo_data[loc_smooth]
	#bivalve
	bivalve_yr = bivalve_yr[loc_smooth]
	bivalve_data = bivalve_data[loc_smooth]
	bivalve_data = np.roll(bivalve_data,12)
	#volcanic
	volc_yr2 = volc_yr2[loc_smooth]
	volc_data2 = volc_data2[loc_smooth]
	#density
	density_yr = density_yr[loc_smooth]
	density_data = density_data[loc_smooth]
	############
	#Shifting density by 12 years
	############
	density_data = np.roll(density_data,10)
	############
	# Detrending, normalising, and combining
	############
	#amo
	y1 = amo_data
	y1 = scipy.signal.detrend(y1, axis=0)
	y1 -= np.min(y1)
	y1 /= np.max(y1)
	#volcanic
	y2 = volc_data2 * volc_factor
	y2 -= np.min(y2)
	y2 /= np.max(y2)
	#density
	y3 = density_data
	y3 = scipy.signal.detrend(y3, axis=0)
	y3 -= np.min(y3)
	y3 /= np.max(y3)
	#density and volcanic
	y4 = density_data * density_factor + volc_data2 * volc_factor
	y4 = scipy.signal.detrend(y4, axis=0)
# 	y4 -= np.min(y4)
# 	y4 /= np.max(y4)
	#output
	return y1,y2,y3,y4,amo_yr


start_year = 980
end_year = 1849

############
# Files and read in data
############

density = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/n_ice_densnormalised_no_hadcm3_csiro.txt',delimiter=',')
# density = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt',delimiter=',')
#density = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/n_ice_dens.txt',delimiter=',')

amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)

r_data_file = '/data/NAS-geo01/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')

file1 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090N_AOD_c.txt'
file2 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030N_AOD_c.txt'
file3 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_3090S_AOD_c.txt'
file4 = '/data/NAS-geo01/ph290/misc_data/last_millenium_volcanic/ICI5_030S_AOD_c.txt'

data1 = np.genfromtxt(file1)
data2 = np.genfromtxt(file2)


density_smoothings = np.linspace(10,40.0,10)
volc_smoothings = np.linspace(5.0,20.0,10)
volc_factors = np.linspace(0.1,10.0,10)
density_factors  = np.linspace(0.1,10.0,10)

# os.system('rm /home/ph290/Documents/python_scripts/pickles/amo_r_values2.pickle')

try:
 	[r_values2,r_values3,r_values4] = pickle.load( open( "/home/ph290/Documents/python_scripts/pickles/amo_r_values2.pickle", "rb" ) )
except:

	r_values2 = np.zeros([np.size(density_smoothings),np.size(volc_smoothings),np.size(density_factors),np.size(volc_factors)])
	r_values3 = r_values2.copy()
	r_values4 = r_values2.copy()


	for i,density_smoothing in enumerate(density_smoothings):
		print 'i = ',i
		for j,volc_smoothing in enumerate(volc_smoothings):
# 			print 'j = ',j
			for k,density_factor in enumerate(volc_factors):
				for m,volc_factor in enumerate(density_factors):
# 						with Timer() as t:
					y1,y2,y3,y4,yr = main_calculations(start_year,end_year,density,amo,r_data,data1,data2,density_smoothing,volc_smoothing,density_factor,volc_factor)
# 						print "=> elasped lpush: %s s" % t.secs
					r_value2, p_value2 = stats.spearmanr(y1,y2)
					r_value3, p_value3 = stats.spearmanr(y1,y3)
					r_value4, p_value4 = stats.spearmanr(y1,y4)
					r_values2[i,j,k,m] = r_value2
					r_values3[i,j,k,m] = r_value3
					r_values4[i,j,k,m] = r_value4


	pickle.dump([r_values2,r_values3,r_values4], open( "/home/ph290/Documents/python_scripts/pickles/amo_r_values2.pickle", "wb" ) )


x = np.where(r_values4 == np.max(r_values4))
# print density_smoothings[x[0][0]]
# print volc_smoothings[x[1][0]]
# print density_factors[x[2][0]]
# print volc_factors[x[3][0]]


density_smoothing = density_smoothings[x[0][0]]
# density_smoothing = 25.0
volc_smoothing = volc_smoothings[x[1][0]]
# volc_smoothing = 15
density_factor = density_factors[x[2][0]]
# density_factor = 2.5
volc_factor = volc_factors[x[3][0]]
# volc_factor = 3.0
c = 0.75


# density_smoothing =  30.0
# volc_smoothing =  16.25
# density_factor =  3.0
# volc_factor =  7.0
# c = 0.55
#
#
# ###########################
# ###########################
#
#
#
# print 'density_smoothing',density_smoothing
# print 'volc_smoothing',volc_smoothing
# print 'density_factor',density_factor
# print 'volc_factor',volc_factor

density_smoothing = 20
volc_smoothing = 20
density_factor = 5.0
volc_factor = 7.0
c = +0.60

'''
These work well!
density_smoothing 30.0
volc_smoothing 16.25
density_factor 2.575
volc_factor 7.525
a r-value  0.267324429034
b r-value  0.297516474521
c r-value  0.509870593391

density_smoothing 33.3333333333
volc_smoothing 18.3333333333
density_factor 1.2
volc_factor 3.4
a r-value  0.266321393175
b r-value  0.295552216026
c r-value  0.512499793144
'''

y1,y2,y3,y4,yr = main_calculations(start_year,end_year,density,amo,r_data,data1,data2,density_smoothing,volc_smoothing,density_factor,volc_factor)

r_value2, p_value2 = stats.spearmanr(y1,y2)
r_value3, p_value3 = stats.spearmanr(y1,y3)
r_value4, p_value4 = stats.spearmanr(y1,y4)

print 'a r-value ',r_value2
print 'b r-value ',r_value3
print 'c r-value ',r_value4


#############
# Plotting
#############


plt.close('all')
plt.rc('legend',**{'fontsize':10})
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(15)

#gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)
#ax1 = plt.subplot(gs[0:30,0:100])
#ax2 = plt.subplot(gs[35:65:100,0:100])
#ax3 = plt.subplot(gs[70:100,0:100])

gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.1,right=0.95)
ax1 = plt.subplot(gs[0:40,0:45])
ax2 = plt.subplot(gs[60:100,0:45])
ax3 = plt.subplot(gs[30:70,55:100])
ax4 = plt.subplot(gs[80:100,55:100])
ax4.set_frame_on(False)
ax4.axes.get_xaxis().set_visible(False)
ax4.axes.get_yaxis().set_visible(False)



ax1.plot(yr,y1,'b',lw = 3)
ax1.plot(yr,y2,'orange',lw = 3,label = 'volcanic forcing')
ax1.annotate('r-value = '+np.str(np.around(r_value2,2)), xy=(.025, .13), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center',fontsize=10)
ax1.annotate('p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center',fontsize=10)

ax2.plot(yr,y1,'b',lw = 3)
ax2.plot(yr,y3,'k',lw = 3,label = 'PMIP3 ensmble mean high-latitude density offset by +10 years')
#ax2.annotate('r-value = '+np.str(np.around(r_value3,2))+', p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
#                horizontalalignment='left', verticalalignment='center')
ax2.annotate('r-value = '+np.str(np.around(r_value3,2)), xy=(.025, .13), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center',fontsize=10)
ax2.annotate('p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center',fontsize=10)

ax3.plot(yr,y1,'b',lw = 3,label = 'Proxy reconstructed AMO index')
ax3.plot(yr,y4+c,'#663300',lw = 3,label = 'volcanic and PMIP3 high-latitude density')
ax3.annotate('r-value = '+np.str(np.around(r_value4,2)), xy=(.025, .13), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center',fontsize=10)
ax3.annotate('p-value < 0.001', xy=(.025, .05), xycoords='axes fraction',
                horizontalalignment='left', verticalalignment='center',fontsize=10)

ax1.set_title('(a) Direct volcanic forcing')
ax2.set_title('(b) Forced high-latitude density')
ax3.set_title('(c) Combining volcanic and density signals')

ax1.set_ylabel('Normalised anomaly')
ax2.set_ylabel('Normalised anomaly')
ax3.set_ylabel('Normalised anomaly')

ax1.set_xlabel('Calendar year')
ax2.set_xlabel('Calendar year')
ax3.set_xlabel('Calendar year')

ax1.set_xlim([960,end_year])
ax2.set_xlim([960,end_year])
ax3.set_xlim([960,end_year])

ax1.set_ylim([-0.1,1.1])
ax2.set_ylim([-0.1,1.1])
ax3.set_ylim([-0.1,1.1])


lines = ax3.get_lines()
lines.extend(ax2.get_lines())
lines.extend(ax1.get_lines())
del lines[-2]
del lines[-3]
labels = [l.get_label() for l in lines]
plt.figlegend(lines,labels, loc = 'lower right', bbox_to_anchor = (-0.06,0.14,1,1),frameon=False)


#see: http://matplotlib.org/examples/pylab_examples/annotation_demo2.html
ax4.annotate('',xy=(0.6, 0.7), xycoords='figure fraction',
                xytext=(0.5, 0.8), textcoords='figure fraction',
                arrowprops=dict(arrowstyle="->",lw=3,
                                connectionstyle="angle,angleA=0,angleB=90,rad=10"),
                )


ax4.annotate('',xy=(0.6, 0.3), xycoords='figure fraction',
                xytext=(0.5, 0.2), textcoords='figure fraction',
                arrowprops=dict(arrowstyle="->",lw=3,
                                connectionstyle="angle,angleA=0,angleB=90,rad=10"),
                )


fig.canvas.draw()

# plt.tight_layout()
plt.savefig('/home/ph290/Documents/figures/forced_AMO_2.png')
plt.show(block = False)

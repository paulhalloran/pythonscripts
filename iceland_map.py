import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib.offsetbox import AnchoredText

lat = 66.0+(31.59/60.0)
lon = -1.0 * (18.0+(11.74/60.0))

plt.close('all')
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
ax.set_extent([-30, -10, 60, 75], crs=ccrs.PlateCarree())

coasts_10m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',
                                        edgecolor='k',facecolor='none')

land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                        edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])

ax.add_feature(land_10m)
ax.add_feature(coasts_10m)


lon_west1 = -23.75
lon_east1 = -21.75
lat_south = 66.0
lat_north = 70.0

x = [lon_west1,lon_east1,lon_east1,lon_west1,lon_west1]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
ax.plot(x, y,'b')
ax.fill(x, y, color='coral', alpha=0.4)


lon_west2 = -19.75
lon_east2 = -17.75

x = [lon_west2,lon_east2,lon_east2,lon_west2,lon_west2]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
ax.plot(x, y,'c')
ax.fill(x, y, color='coral', alpha=0.4)

lon_west3 = -15.75
lon_east3 = -13.75

x = [lon_west3,lon_east3,lon_east3,lon_west3,lon_west3]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
ax.plot(x, y,'g')
ax.fill(x, y, color='coral', alpha=0.4)


plt.scatter(lon,lat,color='b',marker='*',s=100)


# ax.add_feature(states_provinces, edgecolor='gray')

# Add a text annotation for the license information to the
# the bottom right corner.
# text = AnchoredText(r'$\mathcircled{{c}}$ {}; license: {}'
#                     ''.format(SOURCE, LICENSE),
#                     loc=4, prop={'size': 12}, frameon=True)
# ax.add_artist(text)

plt.show(block=False)

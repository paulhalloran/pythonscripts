#The issue here is that whatever you do, it starts just tracking atm. CO2 very quickly. I gues sthis model needs a dep box as well.

import numpy as np
import mocsy
import matplotlib.pyplot as plt
from scipy import interpolate

######
# Initial Conditions / Setup
######

sal = 35.0
temp = 19.0
alk = 2.44430536287 # mol/m^3 (CMIP5 N. Atl. mean)
initial_dic = 2.14411821259 # mol/m^3 (CMIP5 N. Atl. mean)
dic = initial_dic

alk2 = 2.28539769401 # mol/m^3 (GLODAP N. Atl. mean)
initial_dic2 = 2.00443704456 # mol/m^3 (GLODAP N. atl. mean)
dic2 = initial_dic2

mixed_layer_depth = 50.0 #m
#fraction_dic_lost_by_mixing_per_day = 3.0e-5 # tune this to get the correct avg. air-sea flux
fraction_dic_lost_by_mixing_per_day1 = 1.5e-5 # tune this to get the correct avg. air-sea flux
fraction_dic_lost_by_mixing_per_day2 = 1.0e-5 # tune this to get the correct avg. air-sea flux


loss_by_biological_export = 2.0e-3 #1.0e-3 # mol/m^2/day

U10 = 7.0 # Wind speed (m/s)

spinup_length = 10000 # days

start_yr = 2000
end_yr = 2013

# Atmospheric CO2 data file
cmip5_atm_co2_file = '/data/data0/ph290/cmip5/forcing_data/rcp_forcing_data/historical_and_rcp85_atm_co2.txt'



######
# Constants
######

yearsec = 60.0 * 60.0 * 24.0 * 365.0
daysec = 60.0 * 60.0 * 24.0
depth_top_model_layer = 10.0 #m
patm = 1.0

######
# Functions
######


def calculate_gas_transfer(temp,U10):
    Sc = 2073.1 - (125.62 * temp) + (3.6276*(temp**2)) - (0.043219*(temp**3)) # from Wanninkhof 1992
    kw = (1.0/3.6e+05 * 0.31 *(U10**2)) * (660/Sc)**0.5
    kw660 = kw * (Sc/660)**0.5
    return kw660


def run_integration(timesteps,xco2_ts,gas_trans_velocity, patm, depth_top_model_layer,temp,sal,alk, dic,loss_by_biological_export,fraction_dic_lost_by_mixing_per_day):
    co2flux_array = np.zeros(np.size(timesteps))
    dic_array = np.zeros(np.size(timesteps))
    pco2_array = np.zeros(np.size(timesteps))
    dpco2_array = np.zeros(np.size(timesteps))
    fco2_array = np.zeros(np.size(timesteps))
    revelle_factor_array = np.zeros(np.size(timesteps))
    for i,timestep in enumerate(timesteps):
        xco2 = xco2_ts[i]
        #uses Jim Orr's python/fortran co2 package: http://ocmip5.ipsl.jussieu.fr/mocsy/bkg.html
        co2flux, co2ex, dpco2, ph, pco2, fco2, co2, hco3, co3, OmegaA, OmegaC, BetaD, rhoSW, p, tempis = mocsy.gasx.flxco2(kw660 = gas_trans_velocity, xco2 = xco2, patm = patm, dz1 = depth_top_model_layer, temp = temp, sal = sal,alk = alk, dic = dic, sil = 0, phos = 0, optcon='mol/m3', optt='Tinsitu', optp='db', optb="u74", optk1k2='l', optkf="dg", optgas='Pinsitu')
        #co2flux in mol/(m^2 * s)
        co2flux = co2flux[0]
        co2flux_array[i] = co2flux
        pco2_array[i] = pco2[0]
        dpco2_array[i] = dpco2[0]
        fco2_array[i] = fco2[0]
        dic_array[i] = dic
        mol_co2_m2 = co2flux * daysec
        mol_dic_m2 = dic * mixed_layer_depth
        mol_dic_m2 += mol_co2_m2
        mol_dic_m2 -= mol_dic_m2 * fraction_dic_lost_by_mixing_per_day
        mol_dic_m2 -= loss_by_biological_export
        dic = (mol_dic_m2 / mixed_layer_depth)
        revelle_factor_array[i] = BetaD
    return co2flux_array, pco2_array, dpco2_array, fco2_array, dic_array, revelle_factor_array, dic



######
# Main program
######

# Read in atmospheric CO2 data and associated years
years = np.genfromtxt(cmip5_atm_co2_file,delimiter = ',',skip_header=1)[:,0]
xco2_ts = np.genfromtxt(cmip5_atm_co2_file,delimiter = ',',skip_header=1)[:,1]

# Interpolate atmospheric CO2 data to give daily timestep (probably too small, but annual is I think too big...)
f1 = interpolate.interp1d(years, xco2_ts)
f2 = interpolate.interp1d(years, years)
no_days = (end_yr - start_yr) * 360.0
xco2_ts = f1(np.linspace(start_yr,end_yr,no_days))
days = f2(np.linspace(start_yr,end_yr,no_days))

# #initialise arrays to hold output for analysis/plotting
# co2flux_array = np.zeros(np.size(years))
# dic_array = np.zeros(np.size(years))
# pco2_array = np.zeros(np.size(years))
# dpco2_array = np.zeros(np.size(years))
# fco2_array = np.zeros(np.size(years))

#calculate teh gas transfer velocity
gas_trans_velocity = calculate_gas_transfer(temp,U10)
#gas_trans_velocity = 2.777777777777778e-05
####??? If th gas transfer velocity too low?



#####
# Simulation
#####

# spinup run - to get DIC in equilibrium
spinup_arbitrary_days = np.arange(spinup_length)
xco2_ts_spinup = spinup_arbitrary_days * 0.0 + xco2_ts[0]
#co2flux_array_spinup, pco2_array_spinup, dpco2_array_spinup, fco2_array_spinup, revelle_factor_array_spinup, dic_array_spinup, dic_post_spinup = run_integration(spinup_arbitrary_days,xco2_ts_spinup,gas_trans_velocity, patm, depth_top_model_layer,temp,sal,alk, dic, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day1)

# historical run
dic_post_spinup = dic
co2flux_array, pco2_array, dpco2_array, fco2_array, dic_array, revelle_factor_array, dic_post_historical = run_integration(days,xco2_ts,gas_trans_velocity, patm, depth_top_model_layer,temp,sal,alk, dic_post_spinup, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day1)

#####
# Additional simulation for comparison
#####

# spinup run - to get DIC in equilibrium
spinup_arbitrary_days = np.arange(spinup_length)
xco2_ts_spinup = spinup_arbitrary_days * 0.0 + xco2_ts[0]
#co2flux_array_spinup, pco2_array_spinup, dpco2_array_spinup, fco2_array_spinup, dic_array_spinup, revelle_factor_array_spinup, dic_post_spinup = run_integration(spinup_arbitrary_days,xco2_ts_spinup,gas_trans_velocity, patm, depth_top_model_layer,temp,sal,alk2, dic2, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day2)

# historical run
dic_post_spinup = dic2
co2flux_array2, pco2_array2, dpco2_array2, fco2_array2, dic_array2, revelle_factor_array2, dic_post_historical2 = run_integration(days,xco2_ts,gas_trans_velocity, patm, depth_top_model_layer,temp,sal,alk2, dic_post_spinup, loss_by_biological_export, fraction_dic_lost_by_mixing_per_day2)


#####
# Plotting
#####



plt.close('all')
f, axarr = plt.subplots(1,3, figsize=(20,8))

axarr[0].plot(days,xco2_ts,'b--',label = 'atm. xCO2')
axarr[0].plot(days,pco2_array,'b',label = 'alk = '+str(alk)+' (model)')
axarr[0].plot(days,pco2_array2,'r',label = 'alk = '+str(alk2)+' (obs)')
axarr[0].set_ylabel('pco2 (uatm)')
axarr[0].set_xlabel('Year')
axarr[0].set_xlim([2000,2013])
axarr[0].legend(loc=2)

axarr[1].plot(days,co2flux_array * yearsec,'b')
axarr[1].plot(days,co2flux_array2 * yearsec,'r')
axarr[1].set_ylabel('air-sea flux (mol/m2/yr)')
axarr[1].set_xlabel('Year')
axarr[1].set_xlim([2000,2013])


axarr[2].plot(days,revelle_factor_array,'b')
axarr[2].plot(days,revelle_factor_array2,'r')
axarr[2].set_ylabel('Revelle factor')
axarr[2].set_xlabel('Year')
axarr[2].set_xlim([2000,2013])

plt.show(block = False)

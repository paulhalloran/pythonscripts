import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt

def find_variable_name(variables,name):
    return [s for s in variables if name in s]


def find_variable_code(var):
    cube = iris.load(annual_directory+'ns_bfm_annual_ave.3d.nc',var)
    for c in cube:
        test = np.size(np.shape(c))
        if test == 3:
            return c.var_name.encode('ascii', 'ignore')

annual_directory = '/data/NAS-geo01/ph290/sarah/50_year_hindcast_v2/annual_av/'

monthly_directory = '/data/NAS-geo01/ph290/sarah/50_year_hindcast_v2/monthly_av/'

cube_3d = iris.load(annual_directory+'ns_bfm_annual_ave.3d.nc')

#print what variables we have:

variables = []
for cube in cube_3d:
    test = np.size(np.shape(cube))
    if test == 3:
        variables.append(cube.long_name.encode('ascii', 'ignore'))


#finding a variable name:
x = find_variable_name(variables,'filter')
print x

variables_of_interest = ['temperature (2m)','net filterfeeder produc.','gross primary production','PotentialFoodAvailability for Benthic Fish','Suspension feeders']

# cube = iris.load_cube(annual_directory+'ns_bfm_annual_ave.3d.nc','elevation')
# averaged_cube = cube.collapsed('time',iris.analysis.MEAN)
# qplt.contourf(averaged_cube,31)
# plt.gca().coastlines()
# plt.show()



for var in variables_of_interest:
    tmp = find_variable_code(var)
    cube = iris.load_cube(annual_directory+'ns_bfm_annual_ave.3d.nc',var)
    averaged_cube = cube.collapsed('time',iris.analysis.MEAN)
    qplt.contourf(averaged_cube,31)
    plt.gca().coastlines()
    plt.show()



cube2 = iris.load_cube(annual_directory+'ns_bfm_annual_ave.3d.nc','bathymetry')
#cube = iris.load_cube(annual_directory+'ns_bfm_annual_ave.3d.nc','net filterfeeder produc.')
cube = iris.load_cube(annual_directory+'flux of Diatoms at BOTTOM')
#cube = iris.load_cube(annual_directory+'ns_bfm_annual_ave.3d.nc','PotentialFoodAvailability for Benthic Fish')
averaged_cube = cube.collapsed('time',iris.analysis.MEAN)
qplt.contourf(averaged_cube,51)
qplt.contour(cube2,np.linspace(50,0,10),colors = 'k')
plt.gca().coastlines()
plt.show()


#Looking at a zonal slice
cube = iris.load_cube(annual_directory+'ns_bfm_annual_ave.3d.nc','Chlorophylla')


#taking a zonal slize:
cube_time_mean = cube.collapsed('time',iris.analysis.MEAN)
variable_collapsed_along_longitude = cube_time_mean.collapsed('longitude',iris.analysis.MEAN)


variable_collapsed_along_longitude = cube_time_mean.collapsed('longitude',iris.analysis.MEAN)


my_meridional_slice_variable = cube_time_mean.extract(iris.Constraint(latitude = 55.0))

plt.figure(0)
qplt.contourf(my_meridional_slice_variable,31)
plt.show(block = False)

plt.figure(1)
qplt.contourf(cube_time_mean[1],31)
plt.show(block = False)

#plotting points on a map


plt.figure(1)
qplt.contourf(cube_time_mean[1],31)

longitudes = ([2.0,3.0,4.0,5.0])
latitudes = ([55.0,56.0,57.0,58.0])

plt.scatter(longitudes,latitudes,color = 'r')

plt.show(block = False)


'''
#Download_seawaifs_data
wget 'http://oceandata.sci.gsfc.nasa.gov/cgi/getfile/S20030602003090.L3b_MO_ST92_CHL.nc' .


seawifs_cube = iris.load_cube('S20030602003090.L3b_MO_ST92_CHL.nc')

'''



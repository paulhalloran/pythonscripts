import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import glob
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import pandas as pd
import matplotlib.mlab
import matplotlib.pyplot as plt



def plot_map(x,y,z,variable_to_plot):

    xi = np.linspace(np.nanmin(x), np.nanmax(x),100)
    yi = np.linspace(np.nanmin(y), np.nanmax(y),100)
    zi = matplotlib.mlab.griddata(y, x, z, yi, xi, interp='linear')

    plt.close('all')

    #plt.contourf(yi, xi, zi,100)
    #plt.show(block = False)

    land_50m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                            edgecolor='face',
                                            facecolor=cfeature.COLORS['land'])


    # Choose map projection - see http://scitools.org.uk/cartopy/docs/v0.14/crs/projections.html#cartopy-projections
    ax = plt.axes(projection=ccrs.PlateCarree())
    # Get the data onto the map. Can change things - e.g. colour map used from hot to other - see https://matplotlib.org/examples/color/colormaps_reference.html
    CS = ax.contourf(yi, xi, zi,np.linspace(-20,150,100),
                    transform=ccrs.PlateCarree(),
                    cmap='hot')
    ax.add_feature(land_50m)
    ax.coastlines('10m')
    ax.set_extent([-8, -3.5, 55, 59])
    ax.set_title = variable_to_plot
    plt.colorbar(CS, ax=ax)
    plt.savefig('/home/ph290/Documents/figures/'+variable_to_plot+'_map.png',dpi=600)
    plt.show(block = False)



data = pd.read_csv('s12_m2_s2_n2_h_map.dat_NW_Europe',delim_whitespace=True,skiprows=1,names=['lat','lon','a','b','c','d','e','f','g','h','i','j','depth'])


x = data.lon.values
y = data.lat.values
z = data.depth.values

plot_map(x,y,z,'bathymetry')



x = data.lon.values
y = data.lat.values
z = data.a.values + data.c.values + data.e.values + data.g.values + data.i.values
plot_map(x,y,z,'tides')



import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
import iris
import iris.plot as iplt
import seawater
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
import numpy.ma as ma
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats
import pandas
import iris.coord_categorisation
import iris.analysis
import iris.quickplot as qplt
import pickle
import matplotlib.pyplot as plt
import iris
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib import ticker
from matplotlib.mlab import griddata
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter

def extract_meaned_sections(cube,ann_sal_cube,lon_west1,lon_east1,lat_south,lat_north):

    cube_region_tmp = cube.intersection(longitude=(lon_west1, lon_east1))
    ann_cube_t_1 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

    cube_region_tmp = ann_sal_cube.intersection(longitude=(lon_west1, lon_east1))
    ann_cube_s_1 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

    ann_cube_p_1 = ann_cube_t_1.copy()
    ann_cube_p_1.data = seawater.dens(ann_cube_s_1.data,ann_cube_t_1.data,1)


    grid_areas = iris.analysis.cartography.area_weights(ann_cube_t_1)
    ann_cube_t_1_mean = ann_cube_t_1.collapsed(['longitude'], iris.analysis.MEAN, weights=grid_areas)

    grid_areas = iris.analysis.cartography.area_weights(ann_cube_s_1)
    ann_cube_s_1_mean = ann_cube_s_1.collapsed(['longitude'], iris.analysis.MEAN, weights=grid_areas)

    grid_areas = iris.analysis.cartography.area_weights(ann_cube_p_1)
    ann_cube_p_1_mean = ann_cube_p_1.collapsed(['longitude'], iris.analysis.MEAN, weights=grid_areas)

    return ann_cube_t_1_mean,ann_cube_s_1_mean,ann_cube_p_1_mean

def calc_d18O(T,S):
        #S = ABSOLUTE SALINITY psu
        #T = ABSOLUTE T deg' C
        # d18Osw_synth = ((0.61*S)-21.3)
#       d18Osw_synth = ((3.0*S)-105)
        #R.D. Frew et al. / Deep-Sea Research I 47 (2000) 2265}2286
        #Temperature reconstructions for SW and N Iceland waters over the last 10 cal ka based on d18O records from planktic and benthic Foraminifera, SMITH ET AL., 2005
        d18Osw_synth = ((0.55*S)-18.98)
        #LeGrande, A. N. & Schmidt, G. A. Global gridded data set of the oxygen isotopic composition in seawater. Geophysical Research Letters 33 (2006)
        # d18Oc_synth = np.exp((2.559*(10.0e6 * (T**-2)) + 0.715)/1000.0) * (1000.0 + d18Osw_synth) - 1000
        # d18Oc_synth = ((1000.0 + d18Osw_synth) * np.exp((2.559*(1.0e6 * (T**-2)) + 0.715)/1000.0))-1000.0
        #Controls on the stable isotope composition of seasonal growth bands in aragonitic fresh-water bivalves (unionidae),  David L. Dettmana, Aimee K. Reischea, Kyger C. Lohmanna, GCA, 1999
        d18Oc_synth = ((21.8 - (T-273.15))/4.69) + (d18Osw_synth-0.27)
        #From Reynolds - the -27 is to convert between SMOW and vPDB
        return d18Oc_synth


#data from https://www.nodc.noaa.gov/cgi-bin/OC5/gin-seas-climate/ginregcl.pl
annual_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
winter_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
spring_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
summer_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
autumn_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]

summer_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
winter_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
ann_sal_cube = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]



annual_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_04.nc')[0][0]
winter_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_04.nc')[0][0]
spring_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_04.nc')[0][0]
summer_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_04.nc')[0][0]
autumn_cube_t = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_04.nc')[0][0]

annual_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_04.nc')[5][0]
winter_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_04.nc')[5][0]
spring_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s14_04.nc')[5][0]
summer_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_04.nc')[5][0]
autumn_cube_s = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s16_04.nc')[5][0]


annual_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t00_01.nc')[0][0]
winter_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t13_01.nc')[0][0]
spring_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t14_01.nc')[0][0]
summer_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t15_01.nc')[0][0]
autumn_cube_t_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_t16_01.nc')[0][0]

annual_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s00_01.nc')[5][0]
winter_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s13_01.nc')[5][0]
spring_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s14_01.nc')[5][0]
summer_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s15_01.nc')[5][0]
autumn_cube_s_1deg = iris.load_raw('/data/dataSSD0/ph290/gins_decav_s16_01.nc')[5][0]

# cube =  winter_cube
# cube =  spring_cube
# cube =  summer_cube
# cube =  autumn_cube
cube =  annual_cube

density_cube = cube.copy()
density_cube.standard_name = 'sea_water_density'
density_cube.units = 'kg m-3'

density_cube.data = seawater.dens(ann_sal_cube.data,cube.data,1)

lon_west = -35.0
lon_east = 15.0
lat_south = 60
lat_north = 85.0

cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = ann_sal_cube.intersection(longitude=(lon_west, lon_east))
ann_sal_cube = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = winter_cube.intersection(longitude=(lon_west, lon_east))
winter_cube2 = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

# sections:

lon_west1 = -23.75
lon_east1 = -21.75
lat_south = 66.0
lat_north = 70.0

lon_west2 = -19.75
lon_east2 = -17.75

lon_west3 = -15.75
lon_east3 = -13.75

ann_cube_t_1_mean,ann_cube_s_1_mean,ann_cube_p_1_mean = extract_meaned_sections(annual_cube_t,annual_cube_s,lon_west1,lon_east1,lat_south,lat_north)
ann_cube_t_2_mean,ann_cube_s_2_mean,ann_cube_p_2_mean = extract_meaned_sections(annual_cube_t,annual_cube_s,lon_west2,lon_east2,lat_south,lat_north)
ann_cube_t_3_mean,ann_cube_s_3_mean,ann_cube_p_3_mean = extract_meaned_sections(annual_cube_t,annual_cube_s,lon_west3,lon_east3,lat_south,lat_north)

#ftp://sidads.colorado.edu/pub/DATASETS/NOAA/G10010/
#http://nsidc.org/data/G10010#
seaice_file = '/data/NAS-geo01/ph290/observations/G10010_SIBT1850_v1.1.nc'
ice_cube = iris.load_cube(seaice_file,'Sea_Ice_Concentration')
iris.coord_categorisation.add_month_number(ice_cube, 'time', name='clim_month')
iris.coord_categorisation.add_month(ice_cube, 'time', name='month')
iris.coord_categorisation.add_year(ice_cube, 'time', name='year')
ice_cube = ice_cube[np.where((ice_cube.coord('year').points >= 1955) & (ice_cube.coord('year').points <= 2012))]
ice_cube_monthly_climatology = ice_cube.aggregated_by('clim_month', iris.analysis.MEAN)
# ice_cube_may = ice_cube[ice_cube.coord('month').points == 'May']


########################


plt.close('all')
plt.figure(figsize = (5,12))

my_cmap = 'bwr'


minv=-4.0
maxv=4.0

####
# sections North of iceland
####

p_min = 1027.2
p_max = 1028.2
p_levs = 21
c_levels = np.linspace(p_min,p_max,p_levs)

ax1 = plt.subplot2grid((24,24),(0,0),colspan=11,rowspan=7)
iplt.pcolormesh(ann_cube_t_1_mean,cmap = my_cmap,vmin = minv,vmax=maxv)
CS2 = iplt.contour(ann_cube_p_1_mean,np.linspace(p_min,p_max,p_levs),colors='k',linewidths=0.5)
plt.clabel(CS2,[c_levels[14],c_levels[15],c_levels[16]],inline=1,fmt='%1.2f')
plt.title('West Iceland ')
plt.ylim([1250,0])
plt.ylabel('depth (m)')
plt.xlabel('latitude ($^\circ$N)')
plt.text(66, 1200, 'contour lines = density', fontsize=10)


ax1.spines['bottom'].set_color('indigo')
ax1.spines['top'].set_color('indigo')
ax1.spines['left'].set_color('indigo')
ax1.spines['right'].set_color('indigo')
ax1.spines['left'].set_lw(2)
ax1.spines['right'].set_lw(2)
ax1.spines['top'].set_lw(2)
ax1.spines['bottom'].set_lw(2)


ax3 = plt.subplot2grid((24,24),(0,13),colspan=11,rowspan=7)
cs = iplt.pcolormesh(ann_cube_t_3_mean,cmap = my_cmap,vmin = minv,vmax=maxv)
CS4 = iplt.contour(ann_cube_p_3_mean,np.linspace(p_min,p_max,p_levs),colors='k',linewidths=0.5)
plt.clabel(CS4,[c_levels[14],c_levels[15],c_levels[16]],inline=1,fmt='%1.2f')
plt.title('East Iceland ')
plt.ylim([1250,0])
# plt.ylabel('depth (m)')
plt.xlabel('latitude ($^\circ$N)')

ax3.spines['bottom'].set_color('c')
ax3.spines['top'].set_color('c')
ax3.spines['left'].set_color('c')
ax3.spines['right'].set_color('c')
ax3.spines['left'].set_lw(2)
ax3.spines['right'].set_lw(2)
ax3.spines['top'].set_lw(2)
ax3.spines['bottom'].set_lw(2)

frame1 = plt.gca()
frame1.axes.yaxis.set_ticklabels([])


ax1.set_facecolor((0.85, 0.85, 0.85))
# ax2.set_facecolor((0.85, 0.85, 0.85))
ax3.set_facecolor((0.85, 0.85, 0.85))


#################
# iceland map ###
#################

loc = np.where(cube.coord('depth').points > 50)[0][0]
lat = 66.0+(31.59/60.0)
lon = -1.0 * (18.0+(11.74/60.0))

# plt.close('all')
# fig = plt.figure()
# ax = fig.add_subplot(1, 1, 1)
ax5 = plt.subplot2grid((49,24),(18,0),rowspan=14,colspan=24, projection=ccrs.PlateCarree())


m=iplt.pcolormesh(annual_cube_t[loc],cmap = my_cmap,vmin = minv,vmax=maxv)

iplt.contour(ice_cube_monthly_climatology[8],levels=[33],colors='k',linewidths=2.0)
iplt.contour(ice_cube_monthly_climatology[4],levels=[33],colors='k',linestyles = 'dashed',linewidths=2.0)


coasts_10m = cfeature.NaturalEarthFeature('physical', 'coastline', '10m',
                                        edgecolor='k',facecolor='none')

# coasts_high = cfeature.GSHHSFeature(scale='full', levels=None)

land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                        edgecolor='face',
                                        facecolor=(0.85, 0.85, 0.85))

ax5.add_feature(land_10m)
ax5.add_feature(coasts_10m)


lon_west1 = -23.75
lon_east1 = -21.75
lat_south = 66.0
lat_north = 70.0

x = [lon_west1,lon_east1,lon_east1,lon_west1,lon_west1]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
# ax5.plot(x, y,'k',lw=2)
ax5.plot(x, y,'indigo',lw=2,alpha=0.9)


lon_west3 = -15.75
lon_east3 = -13.75

x = [lon_west3,lon_east3,lon_east3,lon_west3,lon_west3]
y= [lat_south,lat_south,lat_north,lat_north,lat_south]
# ax5.plot(x, y,'k',lw=2)
ax5.plot(x, y,'c',lw=2,alpha=0.9)

ax5.set_extent([-35, 15, 55, 80], crs=ccrs.PlateCarree())

# ax5.plot([-20,15],[74.79,74.79],'darkmagenta',alpha=0.8,lw=3)
ax5.plot([-20,15],[74.79,74.79],'k',alpha=0.5,lw=2)


# plt.scatter(lon,lat,color='greenyellow',edgecolor='k',marker='*',s=400)
ax5.plot(lon, lat, marker='o', color='greenyellow',markeredgecolor='k', markersize=10,
            alpha=0.9, transform=ccrs.Geodetic())

#####
# vertical E-W sections
#####

ax6 = plt.subplot2grid((24,35),(17,10),colspan=25,rowspan=7)


lat_var = 74.79


loc2 = np.where(annual_cube_t.coord('latitude').points > lat_var)[0][0]


cs = iplt.pcolormesh(annual_cube_t[:,loc2,:],cmap = 'bwr',vmin = minv,vmax=maxv)

cs2=iplt.contour(annual_cube_s[:,loc2,:],np.linspace(30,40,81),alpha=0.8,linewidths=0.8,colors='k')
ax6.clabel(cs2,[34.75,34.875,35.0], fmt = '%2.2f', colors = 'k', fontsize=10) #contour line labels

x=[-20,0,0,-20,-20]
y=[300,300,0,0,300]
plt.plot(x,y,'k',lw=2,alpha=0.5)
ax6.fill(x,y, color='k', alpha=0.1)


ax6.set_ylim(1000,0)
ax6.set_xlim(-20,15)
plt.title(str(lat_var)+'$^\circ$ latitude')
ax6.set_ylabel('depth (m)')
ax6.set_xlabel('longitude ($^\circ$E)')
plt.text(-18, 950, 'contour lines = salinity', fontsize=10)

# ax6.spines['bottom'].set_color('darkmagenta')
# ax6.spines['top'].set_color('darkmagenta')
# ax6.spines['left'].set_color('darkmagenta')
# ax6.spines['right'].set_color('darkmagenta')
# ax6.spines['left'].set_lw(2)
# ax6.spines['right'].set_lw(2)
# ax6.spines['top'].set_lw(2)
# ax6.spines['bottom'].set_lw(2)


#####
# colorbar
#####


ax9 = plt.subplot2grid((24,35),(17,0),rowspan=7,colspan=3)
cb1 = plt.colorbar(cs, cax=ax9,extend='both',orientation='vertical')
cb1.set_label('temperature ($^\circ$C)')
ax9.yaxis.set_ticks_position('left')
ax9.yaxis.set_label_position('left')

# plt.tight_layout()

plt.savefig('/home/ph290/Documents/figures/iceland_fig1.png')
plt.savefig('/home/ph290/Documents/figures/iceland_fig1.pdf')
plt.savefig('/home/ph290/Documents/figures/iceland_fig1.svg')

plt.show(block=False)

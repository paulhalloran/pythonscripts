import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np


cube1 = iris.load_cube('/data/NAS-ph290/ph290/cmip5/winds//vas_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc')
cube2 = iris.load_cube('/data/NAS-ph290/ph290/cmip5/winds//uas_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc')
ws_cube = iris.analysis.maths.exponentiate(iris.analysis.maths.exponentiate(cube1,2) + iris.analysis.maths.exponentiate(cube2,2),0.5)
ws_cube_mean=ws_cube.collapsed('time',iris.analysis.MEAN)


cube1b = iris.load_cube('/data/NAS-ph290/ph290/cmip5/winds/sfcWind_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc')
real_ws_cube_mean=cube1b.collapsed('time',iris.analysis.MEAN)

'''
qplt.contourf(ws_cube_mean,np.linspace(0,15,31))
plt.gca().coastlines()
plt.title('HadGEM2-ES mean wind speed 1984-2005')
plt.savefig('/home/ph290/Documents/figures/hadgem2es_winds.png')
plt.show()
'''

'''
obs_u = iris.load_cube('/data/NAS-ph290/ph290/reanalysis/NCEP_v2/uwnd_merged.nc')
obs_v = iris.load_cube('/data/NAS-ph290/ph290/reanalysis/NCEP_v2/vwnd_merged.nc')
ws_obs = iris.analysis.maths.exponentiate(iris.analysis.maths.exponentiate(obs_u,2) + iris.analysis.maths.exponentiate(obs_v,2),0.5)

ws_obs_mean = ws_obs.collapsed('time',iris.analysis.MEAN)
'''

'''
qplt.contourf(ws_obs_mean[0],np.linspace(0,15,31))
plt.gca().coastlines()
plt.title('NCEP mean wind speed 1980-2014')
plt.savefig('/home/ph290/Documents/figures/NCEP_winds.png')
plt.show()
'''


import iris.plot as iplt

fig = plt.figure(figsize=(12, 5))
plt.subplot(121)
qplt.contourf(ws_cube_mean,np.linspace(0,15,31))
plt.gca().coastlines()
plt.title('HadGEM2-ES mean wind speed 1984-2005')

plt.subplot(122)
qplt.contourf(real_ws_cube_mean,np.linspace(0,15,31))
plt.gca().coastlines()  
plt.title('HadGEM2-ES true mean wind speed 1984-2005')
'''
ax = plt.subplot(133)
qplt.contourf(ws_obs_mean[0],np.linspace(0,15,31))
plt.gca().coastlines()
plt.title('NCEP mean wind speed 1980-2014')
plt.gca().coastlines()
'''
plt.savefig('/home/ph290/Documents/figures/NCEP_hadgem_winds.png')
iplt.show()




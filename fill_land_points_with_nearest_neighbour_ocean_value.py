import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import KDTree

def fill_masked_points_with_nearest_neighbour_2d(a):
    x,y=np.mgrid[0:a.shape[0],0:a.shape[1]]
    xygood = np.array((x[~a.mask],y[~a.mask])).T
    xybad = np.array((x[a.mask],y[a.mask])).T
    a[a.mask] = a[~a.mask][KDTree(xygood).query(xybad)[1]]
    #note that the slow part is working oiut the nearest neighbours. Iyt woudl make sense to apply this to all variables you want to process at this stage.
    return a


mask_file = '/data/BatCaveNAS/ph290/tmp/sftlf_fx_MIROC5_rcp85_r0i0p0.nc'
data_file = '/data/BatCaveNAS/ph290/tmp/uas_Amon_MIROC5_rcp85_r1i1p1_200601-210012.nc'

mask_cube = iris.load_cube(mask_file)
data_cube = iris.load_cube(data_file)

########## set land point mask values to True ##########

where_mask = mask_cube.data > 0.0
where_mask_3d = np.repeat(where_mask[np.newaxis, :, :], np.shape(data_cube)[0], axis=0)
data_cube.data.mask = where_mask_3d

########## filling land points with nearest neighbour value ##########

data = data_cube.data
for i in range(data.shape[0]):
    print 'filling ',i,' out of ',data.shape[0],' 2d fields'
    data[i,:,:] = fill_masked_points_with_nearest_neighbour_2d(data[i,:,:])

data_cube.data = data
data_cube.data.mask[:] = False


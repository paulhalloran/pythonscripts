###
#
#  Data initially processed by /home/ph290/Documents/python_scripts/process_monthly_density_past1000.py
#
###

import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models

##########################################
# Mian part of code                      #
##########################################

#start_year = 850
start_year = 953
end_year = 1850
expected_years = start_year+np.arange((end_year-start_year)+1)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=signal.detrend(bivalve_data_initial)


#############################################################################################################################
# Models: remove linear trend and normalise variability, then put all models on same time axis and perform multi-model mean #
#############################################################################################################################


input_directory = '/data/data1/ph290/cmip5/last_1000_density/'

models = model_names(input_directory)

multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()

for i,model in enumerate(models):
	print model
	cube = iris.load_cube(input_directory+model+'_annual_mean_sw_density_*_regridded.nc')
	#cube = iris.load_cube(input_directory+model+'_SON_sw_density_*_regridded.nc')
	try:
		cube = cube.collapsed(['longitude', 'latitude', 'depth'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	except:
		cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	data2=signal.detrend(data)
	data2 = data2-np.min(data2)
	data3 = data2/(np.max(data2))
	data3 -= np.nanmean(data3)
	for index,y in enumerate(expected_years):
	    loc2 = np.where(yrs == y)
	    if np.size(loc2) != 0:
	        multi_model_density[index,i] = data3[loc2]


smoothing_var = 5
for i,model in enumerate(models):
	multi_model_density[:,i] = rm.running_mean(multi_model_density[:,i],smoothing_var)
	#loc = np.where(np.logical_not(np.isnan(multi_model_density[:,i])))
	#multi_model_density[loc,i] = signal.detrend(multi_model_density[loc,i])


multi_model_mean_density = np.nanmean(multi_model_density, axis = 1)
multi_model_std_density = np.nanstd(multi_model_density, axis = 1)

##########################################
# plotting                               #
##########################################

#smoothing_var = 10

plt.close('all')
fig, ax = plt.subplots(figsize = (10,5))
#y = rm.running_mean(multi_model_mean_density,smoothing_var)
#y2 = rm.running_mean(multi_model_std_density,smoothing_var)
y = multi_model_mean_density
y2 = multi_model_std_density
#data2 = y-np.min(y)
#data3 = data2/(np.max(data2))
#y = data3 - np.nanmean(data3)
ax.plot(expected_years,y,'k',lw=2,alpha=0.5)
# ax.fill_between(expected_years, y + y2, y  - y2, color="none", facecolor='blue', alpha=0.2)
steps = 10
for i in range(steps):
	y2b = y2.copy()*(1.0/float(i+1))
	ax.fill_between(expected_years, y + y2b*2.0, y  - y2b*2.0, color="none", facecolor='k', alpha=0.2)
ax2 = ax.twinx()
ax2.plot(bivalve_year,rm.running_mean(bivalve_data_initial,smoothing_var),'r',lw=2,alpha=0.5)
ax.set_ylim(-0.3,0.3)
ax2.set_ylim(-0.5,0.5)
ax.set_xlabel('Calendar Year')
ax.set_ylabel('PMIP3 density anomaly')
ax2.set_ylabel('bivalve d$^{18}$O anomaly')
ax2.yaxis.label.set_color('red')
#plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_ann_avg.png')
print 'grey = 2 standard deviatins (i.e. +/- 2*stdev)'

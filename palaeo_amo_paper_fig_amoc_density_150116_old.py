
from iris.coords import DimCoord
import iris.plot as iplt
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import numpy.ma as ma
import running_mean as rm
import running_mean_post as rmp
from scipy import signal
import scipy
import scipy.stats
import numpy as np
import statsmodels.api as sm
import running_mean_post
from scipy.interpolate import interp1d
from datetime import datetime, timedelta
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import scipy.interpolate
import gc
import pickle
import biggus
import seawater
import cartopy.feature as cfeature
import scipy.ndimage
import scipy.ndimage.filters
import gsw
import scipy.stats as stats
import time
import matplotlib.patches as mpatches
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import pandas

# input_directory = '/data/data1/ph290/cmip5/last1000/mld/spatial_avg/'
input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger/'

start_year = 950
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models


def extract_years(cube):
    try:
    	iris.coord_categorisation.add_year(cube, 'time', name='year2')
    except:
    	'already has year2'
    #start_year = 850
	start_year = 955
    #end_year = 1850
	end_year = 1849
    loc = np.where((cube.coord('year2').points >= start_year) & (cube.coord('year2').points <= end_year))
    loc2 = cube.coord('time').points[loc[0][-1]]
    cube = cube.extract(iris.Constraint(time = lambda time_tmp: time_tmp <= loc2))
    return cube

def butter_lowpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a



def butter_highpass(highcut, fs, order=5):
    nyq = fs
    high = highcut/nyq
    b, a = scipy.signal.butter(order, high , btype='low',analog = False)
    return b, a


def butter_bandpass(lowcut, fs, order=5):
    nyq = fs
    low = lowcut/nyq
    b, a = scipy.signal.butter(order, low , btype='high',analog = False)
    return b, a



'''
#producing an Atlantic mask (mask masked and Atlantic has value of 1, elsewhere zero) to use in the stream function calculation

"""
#/data/NAS-ph290/ph290/cmip5/last1000_vo_amoc
#input_file = '/data/temp/ph290/regridded/cmip5/last1000_vo_amoc_high_res/CCSM4_vo_past1000_r1i1p1_regridded_not_vertically.nc'
input_file = '/media/usb_external1/tmp/MRI-CGCM3_vo_past1000_r1i1p1_regridded_not_vertically.nc'
cube = iris.load_cube(input_file)
cube = cube[0,0]
cube.data = ma.masked_where(cube.data == 0,cube.data)
#tmp = cube.lazy_data()
#tmp = biggus.ma.masked_where(tmp.ndarray() == 0,tmp.masked_array())

resolution = 0.25

start_date = 850
end_date = 1850

tmp_cube = cube.copy()
tmp_cube = tmp_cube*0.0

location = -30/resolution

print 'masking forwards'

for y in np.arange(180/resolution):
    print 'lat: ',y,'of ',180/resolution
    flag = 0
    tmp = tmp_cube.data.mask[y,:]
    tmp2 = tmp_cube.data[y,:]
    for x in np.arange(360/resolution):
        if tmp[location] == True:
            flag = 1
        if ((tmp[location] == False) & (flag == 0)):
            tmp2[location] = 1
        tmp = np.roll(tmp,+1)
        tmp2 = np.roll(tmp2,+1)
    tmp_cube.data.mask[y,:] = tmp
    tmp_cube.data.data[y,:] = tmp2.data

location = location+1

print 'masking backwards'

for y in np.arange(180/resolution):
    print 'lon: ',y,'of ',180/resolution
    flag = 0
    tmp = tmp_cube.data.mask[y,:]
    tmp2 = tmp_cube.data[y,:]
    for x in np.arange(360/resolution):
        if tmp[location] == True:
            flag = 1
        if ((tmp[location] == False) & (flag == 0)):
            tmp2[location] = 1
        tmp = np.roll(tmp,-1)
        tmp2 = np.roll(tmp2,-1)
    tmp_cube.data.mask[y,:] = tmp
    tmp_cube.data.data[y,:] = tmp2.data

tmp_cube.data.data[150/resolution:180/resolution,:] = 0.0
tmp_cube.data.data[0:40/resolution,:] = 0.0
tmp_cube.data.data[:,20/resolution:180/resolution] = 0.0
tmp_cube.data.data[:,180/resolution:280/resolution] = 0.0

loc = np.where(tmp_cube.data.data == 0.0)
tmp_cube.data.mask[loc] = True

mask1 = tmp_cube.data.mask
cube_test = []

f.write('maske created\n')

'''
#calculating stream function
'''

#trying with the 1/4 degree dataset rather than the 1x1 - this should make the stram function calculatoi nmore robust
files = glob.glob('/data/temp/ph290/regridded/cmip5/last1000_vo_amoc_high_res/*_vo_*.nc')
#/media/usb_external1/cmip5/last1000_vo_amoc



models = []
max_strm_fun = []
max_strm_fun_26 = []
max_strm_fun_45 = []
model_years = []

f.write('files working with:\n')
f.write(str(files)+'\n')

for file in files:

    model = file.split('/')[7].split('_')[0]
    print model
    f.write('processing: '+model+'\n')
    models.append(model)
    cube = iris.load_cube(file)

    print 'applying mask'

    try:
                    levels =  np.arange(cube.coord('depth').points.size)
    except:
                    levels = np.arange(cube.coord('ocean sigma over z coordinate').points.size)

	#for level in levels:
#		print 'level: '+str(level)
#		for year in np.arange(cube.coord('time').points.size):
#			#print 'year: '+str(year)
#			tmp = cube.lazy_data()
#			mask2 = tmp[year,level,:,:].masked_array().mask
#			tmp_mask = np.ma.mask_or(mask1, mask2)
#			tmp[year,level,:,:].masked_array().mask = tmp_mask

    #variable to hold data from first year of each model to check
    #that the maskls have been applied appropriately

    cube.coord('latitude').guess_bounds()
    cube.coord('longitude').guess_bounds()
    grid_areas = iris.analysis.cartography.area_weights(cube[0])
    grid_areas = np.sqrt(grid_areas)

    shape = np.shape(cube)
    tmp = cube[0].copy()
    tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
    tmp = tmp.collapsed('longitude',iris.analysis.SUM)
    collapsed_data = np.tile(tmp.data,[shape[0],1,1])

    mask_cube = cube[0].copy()
    tmp_mask = np.tile(mask1,[shape[1],1,1])
    mask_cube.data.mask = tmp_mask
    mask_cube.data.mask[np.where(mask_cube.data.data == mask_cube.data.fill_value)] = True

    print 'collapsing cube along longitude'
    try:
            slices = cube.slices(['depth', 'latitude','longitude'])
    except:
            slices = cube.slices(['ocean sigma over z coordinate', 'latitude','longitude'])
    for i,t_slice in enumerate(slices):
            #print 'year:'+str(i)
        tmp = t_slice.copy()
        tmp.data = ma.masked_where(tmp.data == 0,tmp.data)
        tmp *= grid_areas
        mask_cube_II = tmp.data.mask
        tmp.data.mask = mask_cube.data.mask | mask_cube_II
        #if i == 0:
            #plt.close('all')
            #qplt.contourf(tmp[0])
            #plt.savefig('/home/ph290/Documents/figures/test/'+model+'l1.png')
            #plt.close('all')
            #qplt.contourf(tmp[10])
            #plt.savefig('/home/ph290/Documents/figures/test/'+model+'l10.png')

        collapsed_data[i] = tmp.collapsed('longitude',iris.analysis.SUM).data

    try:
            depths = cube.coord('depth').points*-1.0
            bounds = cube.coord('depth').bounds
    except:
            depths = cube.coord('ocean sigma over z coordinate').points*-1.0
            bounds = cube.coord('ocean sigma over z coordinate').bounds
    thickness = bounds[:,1] - bounds[:,0]
    test = thickness.mean()
    if test > 1:
            thickness = bounds[1:,0] - bounds[0:-1,0]
            thickness = np.append(thickness, thickness[-1])

    thickness = np.flipud(np.rot90(np.tile(thickness,[180/resolution,1])))

    tmp_strm_fun_26 = []
    tmp_strm_fun_45 = []
    tmp_strm_fun = []
    for i in np.arange(np.size(collapsed_data[:,0,0])):
            tmp = collapsed_data[i].copy()
            tmp = tmp*thickness
            tmp = np.cumsum(tmp,axis = 1)
            tmp = tmp*-1.0*1.0e-3
            tmp *= 1029.0 #conversion from m3 to kg
            #tmp = tmp*1.0e-7*0.8 # no idea why I need to do this conversion - check...
            coord = t_slice.coord('latitude').points
            loc = np.where(coord >= 26)[0][0]
            tmp_strm_fun_26 = np.append(tmp_strm_fun_26,np.max(tmp[:,loc]))
            loc = np.where(coord >= 45)[0][0]
            tmp_strm_fun_45 = np.append(tmp_strm_fun_45,np.max(tmp[:,loc]))
            tmp_strm_fun = np.append(tmp_strm_fun,np.max(tmp[:,:]))

    coord = cube.coord('time')
    dt = coord.units.num2date(coord.points)
    years = np.array([coord.units.num2date(value).year for value in coord.points])
    model_years.append(years)

    max_strm_fun_26.append(tmp_strm_fun_26)
    max_strm_fun_45.append(tmp_strm_fun_45)
    max_strm_fun.append(tmp_strm_fun)


f.write('saving output to /home/ph290/Documents/python_scripts/pickles/palaeo_amo_III.pickle\n')
with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_IIII.pickle', 'w') as f:
    pickle.dump([models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location], f)

f.close()

"""

'''
### Remove three quotatoin marks below if running for 1st tim eafter load

### Remove three quotatoin marks above if running for 1st tim eafter load

with open('/home/ph290/Documents/python_scripts/pickles/palaeo_amo_IIII.pickle', 'r') as f:
    models,max_strm_fun,max_strm_fun_26,max_strm_fun_45,model_years,mask1,files,b,a,input_file,resolution,start_date,end_date,location = pickle.load(f)



###########################################
#    restrict loading to top 1000m        #
###########################################

chosen_levels = lambda cell: cell <= 500
level_above_1000 = iris.Constraint(depth=chosen_levels)

# models.remove('MRI-CGCM3') #WOUDL BE GREAT TO SORT THIS OUT!
models.remove('FGOALS-gl')
models = np.array(models)

#models.remove('FGOALS-s2')
# models.remove('bcc-csm1-1')
#T and/or  S filed are funny. making desnity splotch at high lats, and therefor eMLD go screwy
#models.remove('CCSM4')
#Funny last density year - need to resolve...cd Documentspyth
#Had to multiply salinity in last two files by 1000 to get in correct units...

#FGOALS-gl No sea-ice
#models.remove('FGOALS-s2')
#FGOALS models lack the required seaice data...
#and levels are upside down or something odd
#NOTE, now resolved by inverting levels using cdo invertlev ifile ofile

directory = '/data/NAS-ph290/ph290/cmip5/last1000/'


model_data = {}
ensemble = 'r1i1p1'


###
#Construct dictionaries containing the models to use and the associated stream function and tas. Note just taking the 1st ensmeble from GISS, which using on of the volc forcings etc. (other 'ensemble' members use different forcings etc.)
###

pmip3_str = {}
pmip3_year_str = {}

giss_test = 0

for i,model in enumerate(models):
	if model == 'GISS-E2-R':
		if giss_test == 0:
# 			pmip3_str[model] = max_strm_fun_26[i]
			pmip3_str[model] = max_strm_fun_45[i]
			pmip3_year_str[model] = model_years[i]
			giss_test += 1
	if model <> 'GISS-E2-R':
# 		pmip3_str[model] = max_strm_fun_26[i]
		pmip3_str[model] = max_strm_fun_45[i]
		pmip3_year_str[model] = model_years[i]


multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()
multi_model_density_yrs = multi_model_density.copy()

def read_and_process_multimodel_density(input_directory,models,expected_years):
	for i,model in enumerate(models):
		print model
		period = 'JJA'
		# 	period = 'annual_mean'
		cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
		#try:
		cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
		#except:
		#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
		data = cube.data
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		#smoothing_var = 1
		#if smoothing_var > 0:
	#		data = gaussian_filter1d(data,smoothing_var)
		data2 = signal.detrend(data)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		#data3 -= np.nanmean(data3)
		data3 = data2
		data3 -= np.nanmean(data3)
	# 	window_type = 'boxcar'
	# 	data3 = pandas.rolling_window(data3,5,win_type=window_type,center=True)
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				multi_model_density[index,i] = data3[loc2]
				multi_model_density_yrs[index,i] = y
	return multi_model_density,multi_model_density_yrs

		
multi_model_density,multi_model_density_yrs = read_and_process_multimodel_density(input_directory,models,expected_years)

'''

def read_and+process_multimodel_density(input_directory,models,expected_years):
	for i,model in enumerate(models):
		print model
		period = 'JJA'
		# period = 'annual_mean'
		cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
		#try:
		cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
		#except:
		#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
		data = cube.data
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		#smoothing_var = 1
		#if smoothing_var > 0:
	#		data = gaussian_filter1d(data,smoothing_var)
		data2=signal.detrend(data)
		data2 = data2-np.min(data2)
		data3 = data2/(np.max(data2))
		# data3 = data2
		data3 -= np.nanmean(data3)
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				multi_model_density[index,i] = data3[loc2]
				multi_model_density_yrs[index,i] = y
		return multi_model_density,multi_model_density_yrs
'''
'''

##############################################
#            figure                         #
##############################################



'''

##################
# R2 values denisty v. stream function
##################

print 'this takes agaes to run, so commented out for now...'


smoothings = np.linspace(1,300,30)
# smoothings = np.linspace(1,100,10)

max_shift = 50
no_shifts = 5
coeff_det_dens_amoc = np.zeros([no_shifts,np.size(smoothings)])
coeff_det_dens_amoc[:] = np.NAN
coeff_det_dens_amoc2 = coeff_det_dens_amoc.copy()

#f, axarr = plt.subplots(4, 1)

for smoothing_no,smoothing in enumerate(smoothings):
	print smoothing_no,' out of ',np.size(smoothings)
	for i,shift in enumerate(np.round(np.linspace(0,max_shift,no_shifts))):
		smoothing = np.int(smoothing)
		#make variable to hold multi model mean stream function data
		pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
		pmip3_model_streamfunction[:] = np.NAN
		# and mixed layer density
		pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
		pmip3_atl_tas = pmip3_model_streamfunction.copy()
		#variable holding analysis years
		expected_years = start_year+np.arange((end_year-start_year)+1)
		#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
		for j,model in enumerate(models):
			#print model
			tmp = pmip3_str[model]
			loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
			tmp = tmp[loc]
			yrs = pmip3_year_str[model][loc]
			data2=signal.detrend(tmp)
# 			data2 = data2-np.min(data2)
# 			data2 = data2/(np.max(data2))
			window_type = 'boxcar'
			if shift == 0:
				data3 = pandas.rolling_window(data2,smoothing,win_type=window_type,center=True)
			else:
				data3 = pandas.rolling_window(data2[shift::],smoothing,win_type=window_type,center=True)
				yrs = yrs[shift::]
			for index,y in enumerate(expected_years):
				loc2 = np.where(yrs == y)
				if np.size(loc2) != 0:
					pmip3_model_streamfunction[index,j] = data3[loc2]
		####################################
		#as above but for density
		####################################
		for j,model in enumerate(models):
			#print model
			tmp = multi_model_density[:,j]
			#tmp = tmp[1:-1]
			#expected_years2 = expected_years[1:-1]
			yrs = multi_model_density_yrs[:,j]
			data2 = tmp
# 			data2 = data2-np.min(data2)
# 			data2 = data2/(np.max(data2))
			window_type = 'boxcar'
			if shift == 0:
				data3 = pandas.rolling_window(data2,smoothing,win_type=window_type,center=True)
			else:
				data3 = pandas.rolling_window(data2[0:-1*shift],smoothing,win_type=window_type,center=True)
				yrs = yrs[shift::]
			for index,y in enumerate(expected_years):
				loc2 = np.where(yrs == y)
				if np.size(loc2) != 0:
					pmip3_mixed_layer_density[index,j] = data3[loc2]
		#pmip3_multimodel_mixed_layer_density = np.mean(pmip3_mixed_layer_density, axis = 1)
		mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
		stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
		#note NAN required because 'vo_Omon_bcc-csm1-1_past1000_r1i1p1_119001-119912.nc' is missing
		mean_strm -= np.nanmean(mean_strm)
		mean = np.nanmean(pmip3_mixed_layer_density, axis = 1)
		stdev = np.nanstd(pmip3_mixed_layer_density, axis = 1)
		mean -= np.nanmean(mean)
		x = mean
		y = mean_strm
		X = expected_years
		loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
		x = x[loc]
		y = y[loc]
		x = x-np.min(x)
		x = x/(np.max(x))
		y = y-np.min(y)
		y = y/(np.max(y))
		# r = scipy.stats.linregress(x,y)[2]
		slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
		#r2 = r_value**2
		#coeff_det_dens_amoc[i,smoothing_no] = r2
		#if p_value <= 0.001:
		r2 = r_value**2
		coeff_det_dens_amoc2[i,smoothing_no] = r2
		if p_value > 0.001:
			r2 = r_value**2
			coeff_det_dens_amoc[i,smoothing_no] = r2


### Remove three quotatoin marks below if running for 1st time after load

### Remove three quotatoin marks above if running for 1st tim eafter load


#######################
# Contour plot part
#######################


cmap1 = mpl_cm.get_cmap('RdBu_r')
cmap2 = mpl_cm.get_cmap('RdBu_r')
#gray_r
#z = np.nanmean(coeff_det_dens_amoc,axis = 0)
z = np.fliplr(np.rot90(coeff_det_dens_amoc,3))
z2 = np.fliplr(np.rot90(coeff_det_dens_amoc2,3))
zmin = 0.0
zmax = np.max(z2[np.where(np.logical_not(np.isnan(z2)))])
xlab , ylab = np.meshgrid(np.round(np.linspace(0,max_shift,no_shifts)),smoothings)

# fig, axarr = plt.subplots(1, 1, (figsize=(8, 4))
plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(10)
fig.set_figwidth(10)
# ax1 = plt.subplot2grid((2,1),(1, 0))
#gs = gridspec.GridSpec(2, 1,width_ratios=[1,1],height_ratios =[1,2])
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.85)

# ax1 = plt.subplot(gs[1])
# ax2 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[0:44,0:100])
ax1 = plt.subplot(gs[56:100,0:90])
ax3 = plt.subplot(gs[56:100,95:100])


#, rowspan=2)
C = ax1.contourf(xlab,ylab,z2,np.linspace(zmin,zmax,100),cmap=cmap1)
cb2 = plt.colorbar(C,ax=ax1,cax=ax3)
ax1.contourf(xlab,ylab,z,np.linspace(zmin,zmax,100),extend='both',hatches=['.'],cmap=cmap2)
CS = ax1.contour(xlab,ylab,z2,10,colors='k')
ax1.clabel(CS, fontsize=12, inline=1)
ax1.set_xlabel('AMOC lagging density (years)')
ax1.set_ylabel('smoothing (years)')
cb2.set_label('Variance explained (R$^2$)')
# plt.show(block = False)
# plt.savefig('/home/ph290/Documents/figures/amoc_density_lag_fig_111115.ps')
# plt.savefig('/home/ph290/Documents/figures/amoc_density_lag_fig_111115.png')



#######################
# timeseries part
#######################



#f, axarr = plt.subplots(4, 1)
# plt.close('all')
#make variable to hold multi model mean stream function data
pmip3_model_streamfunction = np.zeros([1+end_year-start_year,np.size(models)])
pmip3_model_streamfunction[:] = np.NAN
# and mixed layer density
pmip3_mixed_layer_density = pmip3_model_streamfunction.copy()
pmip3_atl_tas = pmip3_model_streamfunction.copy()
#variable holding analysis years
expected_years = start_year+np.arange((end_year-start_year)+1)
#Process (remove linear trend and normalise variability), then put all models on same time axis and perform multi-model mean
for i,model in enumerate(models):
	print model
	tmp = pmip3_str[model]
	loc = np.where((np.logical_not(np.isnan(tmp))) & (pmip3_year_str[model] <= end_year) & (pmip3_year_str[model] >= start_year))
	tmp = tmp[loc]
	yrs = pmip3_year_str[model][loc]
	data2=signal.detrend(tmp)
	#data2 = data2-np.min(data2)
	#data3 = data2/(np.max(data2))
	for index,y in enumerate(expected_years):
		loc2 = np.where(yrs == y)
		if np.size(loc2) != 0:
			pmip3_model_streamfunction[index,i] = data2[loc2]
	####################################
	#as above but for density
	####################################
	for i,model in enumerate(models):
		#print model
		tmp = multi_model_density[:,i]
		#tmp = tmp[1:-1]
		#expected_years2 = expected_years[1:-1]
		yrs = multi_model_density_yrs[:,i]
		data2=signal.detrend(tmp)
		#data2 = data2-np.min(data2)
		#data3 = data2/(np.max(data2))
		for index,y in enumerate(expected_years):
			loc2 = np.where(yrs == y)
			if np.size(loc2) != 0:
				pmip3_mixed_layer_density[index,i] = data2[loc2]





mean_strm = np.nanmean(pmip3_model_streamfunction, axis = 1)
stdev_strm = np.nanstd(pmip3_model_streamfunction, axis = 1)
mean_dens = np.nanmean(pmip3_mixed_layer_density, axis = 1)
stdev_dens = np.nanstd(pmip3_mixed_layer_density, axis = 1)



# plt.close('all')


mean_dens -= np.nanmean(mean_dens)
mean_strm -= np.nanmean(mean_strm)

# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/pmip3_strm_fun.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(mean_strm[i])+'\n')

f.close()

X = expected_years
smoothing_per = 5
shift = 6

# ax2 = plt.subplot2grid((2,1),(0, 0), rowspan=1)

if smoothing_per > 0:
# 	y1 = rm.running_mean(mean_dens,smoothing_per)
	y1 = pandas.rolling_window(mean_dens,smoothing_per,win_type=window_type,center=True)
# 	y1a = y1 + rm.running_mean(stdev_dens,smoothing_per)
# 	y1b = y1 - rm.running_mean(stdev_dens,smoothing_per)
	y1a = y1 + pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)
	y1b = y1 - pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)
	ax2.plot(expected_years+shift,y1,'k',linewidth = 2.0,alpha = 0.8)
	ax2.fill_between(expected_years+shift, y1a, y1b, color="none", facecolor='k', alpha=0.2)
# 	y1a = y1 + rm.running_mean(stdev_dens,smoothing_per)*0.5
# 	y1b = y1 - rm.running_mean(stdev_dens,smoothing_per)*0.5
# 	y1a = y1 + rm.running_mean(stdev_dens,smoothing_per)*0.5
# 	y1b = y1 - rm.running_mean(stdev_dens,smoothing_per)*0.5
	y1a = y1 + pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)*0.5
	y1b = y1 - pandas.rolling_window(stdev_dens,smoothing_per,win_type=window_type,center=True)*0.5
# 	y1a = y1 + gaussian_filter1d(stdev_dens,smoothing_per)*0.5
# 	y1b = y1 - gaussian_filter1d(stdev_dens,smoothing_per)*0.5
	ax2.fill_between(expected_years, y1a, y1b, color="none", facecolor='k', alpha=0.2)
	ax2b = ax2.twinx()
	mean_strm /= (1029.0*1.0e6) #convert to Sv
	stdev_strm /= (1029.0*1.0e6) #convert to Sv
# 	y2 = rm.running_mean(mean_strm,smoothing_per)
	y2 = pandas.rolling_window(mean_strm,smoothing_per,win_type=window_type,center=True)
# 	y2a = y2 + rm.running_mean(stdev_strm,smoothing_per)
# 	y2b = y2 - rm.running_mean(stdev_strm,smoothing_per)
	y2a = y2 + pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)
	y2b = y2 - pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)
	ax2b.plot(expected_years,y2,'g',linewidth = 2.0,alpha = 0.8,label = 'PMIP3 26N AMOC strength')
	ax2b.fill_between(expected_years, y2a, y2b, color="none", facecolor='g', alpha=0.2)
# 	y2a = y2 + rm.running_mean(stdev_strm,smoothing_per)*0.5
# 	y2b = y2 - rm.running_mean(stdev_strm,smoothing_per)*0.5
	y2a = y2 + pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)*0.5
	y2b = y2 - pandas.rolling_window(stdev_strm,smoothing_per,win_type=window_type,center=True)*0.5
	ax2b.fill_between(expected_years, y2a, y2b, color="none", facecolor='g', alpha=0.2)
#if smoothing_per == 0:
#	ax1.plot(expected_years+30,mean_dens,smoothing_per,'k',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 N. Iceland seawater density')
#	ax2 = ax1.twinx()
#	ax2.plot(expected_years,mean_strm,smoothing_per,'g',linewidth = 2.0,alpha = 0.5,label = 'PMIP3 26N AMOC strength')

#rint 'Do dorrelations with diffreent values for teh offset year and come up with the best lag'




plt.xlabel('Calendar Year')
ax2.set_ylabel('PMIP3 North Iceland density anomaly\n(shifted by '+str(shift)+' years)')
#\n(kgm$^{-3}$, shifted by 12 years)')
ax2b.set_ylabel('AMOC anomaly at 26$^o$N (Sv)')
ax2.set_xlabel('Calendar Year')
plt.xlim([950,1850])
ax2.set_ylim(-0.15,0.15)
ax2b.set_ylim(-0.045,0.045)
ax2b.yaxis.label.set_color('green')


plt.annotate('a', xy=(.15, .925),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)
plt.annotate('b', xy=(.15, .5),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)


plt.savefig('/home/ph290/Documents/figures/amoc_density_fig2_111115.ps')
plt.savefig('/home/ph290/Documents/figures/amoc_density_fig2_111115.png')

plt.show(block = False)

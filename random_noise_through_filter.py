import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import datetime

df_obs = pd.read_csv('/data/NAS-ph290/ph290/observations/amon.sm.long.data',delim_whitespace=True,skiprows=1)
dates_obs = [datetime.datetime(x,1,1) for x in df_obs.iloc[:,0].values]
data_obs = df_obs.iloc[:,[1,2,3,4,5,6,7,8,9,10,11,12]]
data_obs = data_obs.replace(-99.990000,np.NaN)
data_obs = data_obs.mean(axis = 1)
df_obs_amo = pd.Series(data=data_obs.values/np.nanstd(data_obs.values),index=dates_obs)

df_obs = pd.read_csv('/data/NAS-ph290/ph290/observations/table.html',delim_whitespace=True)
dates_obs = [datetime.datetime(x,1,1) for x in df_obs.iloc[:,0].values]
data_obs = df_obs.iloc[:,[1,2,3,4,5,6,7,8,9,10,11,12]]
data_obs = data_obs.replace(-99.990000,np.NaN)
data_obs = data_obs.mean(axis = 1)
df_enso = pd.Series(data=data_obs.values/np.nanstd(data_obs.values),index=dates_obs)


dStart = pd.Timestamp.min
dEnd = datetime.datetime(2018,1,1)
dateIndex = pd.date_range(start=dStart, end=dEnd, freq='A')

# noise = np.random.normal(0,1,len(dateIndex))
# df = pd.Series(data=noise,index=dateIndex)
# df = df.rolling(20).sum()
# df = df.divide(np.nanstd(df.values))


plt.close('all')

f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(df_enso)
start_date = datetime.datetime(1900,1,1)
mask = (df.index > start_date)
axarr[1].plot(df.loc[mask],'r')
# axarr[1].plot(df_obs_amo,'k')
# axarr[0].set_xlabel('year')
axarr[1].set_xlabel('year')
axarr[0].set_ylabel('normalised index')
axarr[1].set_ylabel('normalised index')
plt.xlim([datetime.datetime(1680,1,1),datetime.datetime(2020,1,1)])
plt.ylim(-2.8,2.8)

plt.savefig('/home/ph290/Documents/figures/noise_amo1.pdf')
plt.show(block = True)

plt.close('all')

f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(df_enso)
axarr[1].plot(df,'r')
mask = (df_obs_amo.index > start_date)
axarr[1].plot(df_obs_amo.loc[mask],'k')
# axarr[1].plot(df_obs_amo,'k')
# axarr[0].set_xlabel('year')
axarr[1].set_xlabel('year')
axarr[0].set_ylabel('normalised index')
axarr[1].set_ylabel('normalised index')
plt.xlim([datetime.datetime(1680,1,1),datetime.datetime(2020,1,1)])
plt.ylim(-2.8,2.8)

plt.savefig('/home/ph290/Documents/figures/noise_amo2.pdf')
plt.show(block = False)

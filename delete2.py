

smoothings1 = np.linspace(2,31,30.0)
smoothings2 = smoothings1.copy()


coeff_det = np.zeros([np.size(smoothings1),np.size(smoothings1)])
coeff_det[:] = np.NAN
coeff_det_2 = coeff_det.copy()


for smoothing1_no,smoothing1 in enumerate(smoothings1):
	print smoothing1_no,' out of ',np.size(smoothings1)
	x_in = bivalve_data_initial.copy()
	x_in = x_in[::-1]
# 	x_in = x_in[0:-5]
	x_in = rm.running_mean(x_in,smoothing1)
	x_in = (x_in-np.nanmin(x_in))
	x_in /= np.nanmax(x_in)
	for smoothing2_no,smoothing2 in enumerate(smoothings2):
		y_in = y.copy()
# 		y_in = y_in[0:-5]
		y_in = rm.running_mean(y_in,smoothing2)
		y_in = (y_in-np.nanmin(y_in))
		y_in /= np.nanmax(y_in)	
		loc = np.where(np.logical_not(np.isnan(x_in) | np.isnan(y_in)))
		if np.size(loc) <> 0:
			x_in2 = x_in[loc].copy()
			y_in2 = y_in[loc].copy()
		else:
			x_in2 = x_in.copy()
			y_in2 = y_in.copy()
		#slope, intercept, r_value, p_value, std_err = stats.linregress(x_in2,y_in2)
# 		r_value, p_value = stats.kendalltau(x_in2,y_in2)
		r_value, p_value = stats.spearmanr(x_in2,y_in2)
		#r2 = r_value**2
		#coeff_det_dens_amoc[i,smoothing_no] = r2
		#if p_value <= 0.001:
		r2 = r_value**2
		coeff_det[smoothing1_no,smoothing2_no] = r_value
		if p_value > 0.001:
			r2 = r_value**2
			coeff_det_2[smoothing1_no,smoothing2_no] = r_value




loc1 = np.where(coeff_det == np.max(coeff_det))
smoothing1 = smoothings1[loc1[1]]
smoothing2 = smoothings1[loc1[0]]

smoothing1 = smoothing2 = 20.0

x_in = bivalve_data_initial.copy()
x_in = x_in[::-1]
# 	x_in = x_in[0:-5]
# x_in = gaussian_filter1d(x_in,smoothing1)
x_in = rm.running_mean(x_in,smoothing1)
x_in = (x_in-np.nanmin(x_in))
x_in /= np.nanmax(x_in)

y_in = y.copy()
# 		y_in = y_in[0:-5]
# y_in = gaussian_filter1d(y_in,smoothing2)
y_in = rm.running_mean(y_in,smoothing2)
y_in = (y_in-np.nanmin(y_in))
y_in /= np.nanmax(y_in)	

loc = np.where(np.logical_not(np.isnan(x_in) | np.isnan(y_in)))
x_in2 = x_in[loc].copy()
y_in2 = y_in[loc].copy()

r_value, p_value = stats.spearmanr(x_in2,y_in2)

print smoothings1[loc1[0]]
print r_value, p_value

#######################
# Contour plot part
#######################


cmap1 = mpl_cm.get_cmap('RdBu_r')
cmap2 = mpl_cm.get_cmap('RdBu_r')
#gray_r
#z = np.nanmean(coeff_det_dens_amoc,axis = 0)
z = np.fliplr(np.rot90(coeff_det,3))
z2 = np.fliplr(np.rot90(coeff_det_2,3))
zmin = 0.0
zmax = np.max(z)
xlab , ylab = np.meshgrid(smoothings1,smoothings2)

# fig, axarr = plt.subplots(1, 1, (figsize=(8, 4))
plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(10)
fig.set_figwidth(10)
# ax1 = plt.subplot2grid((2,1),(1, 0))
#gs = gridspec.GridSpec(2, 1,width_ratios=[1,1],height_ratios =[1,2])
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.85)

# ax1 = plt.subplot(gs[1])
# ax2 = plt.subplot(gs[0])
#ax2 = plt.subplot(gs[0:44,0:100])
ax1 = plt.subplot(gs[0:100,0:90])
ax3 = plt.subplot(gs[56:100,95:100])


#, rowspan=2)
C = ax1.contourf(xlab,ylab,z,np.linspace(zmin,zmax,100),extend='both',cmap=cmap1)
cb2 = plt.colorbar(C,ax=ax1,cax=ax3)
ax1.contourf(xlab,ylab,z2,np.linspace(zmin,zmax,100),extend='both',hatches=['.'],cmap=cmap2)
CS = ax1.contour(xlab,ylab,z,10,extend='both',colors='k')
ax1.clabel(CS, fontsize=12, inline=1)
ax1.set_xlabel('Bivalve d$^{18}$O smoothing (years)')
ax1.set_ylabel('PMIP3 N. Iceland density smoothing (years)')
cb2.set_label('Variance explained (R$^2$)')

plt.show(block = True)

'''

smoothing1 = smoothing2 = 7.0

x_in = bivalve_data_initial.copy()
x_in = x_in[::-1]
x_in = x_in[0:-5]
x_in = gaussian_filter1d(x_in,smoothing1)
x_in = (x_in-np.min(x_in))
x_in /= np.max(x_in)

y_in = y.copy()
y_in = y_in[0:-5]
y_in = gaussian_filter1d(y_in,smoothing2)
y_in = (y_in-np.min(y_in))
y_in /= np.max(y_in)	

plt.plot(x_in)
plt.plot(y_in)

plt.show()

plt.scatter(x_in,y_in)
plt.show()

'''

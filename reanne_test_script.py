import iris
import carb_chem_3d_or_4d

#load in your data
directory = '/data/dataSSD0/ph290/reanne/'
temperature_cube = iris.load_cube(directory + 'IPSL-CM5A-MR_thetao_rcp85_regridded.nc')
salinity_cube = iris.load_cube(directory + 'IPSL-CM5A-MR_so_rcp85_regridded.nc')
dissolved_carbon_cube = iris.load_cube(directory + 'OyrIPSL-CM5A-MR_dissic_rcp85_regridded.nc')
alkalinity_cube = iris.load_cube(directory + 'OyrIPSL-CM5A-MR_talk_rcp85_regridded.nc')

#To make this run more qickly I've selected just one year (not that you ,migt have issues with memory if you try to do too many years)
temperature_cube = temperature_cube[0]
salinity_cube = salinity_cube[0]
dissolved_carbon_cube = dissolved_carbon_cube[0]
alkalinity_cube = alkalinity_cube[0]

#call the script to do teh calculation
arag_ss_cube = carb_chem_3d_or_4d.arag_saturation_state(temperature_cube,salinity_cube,dissolved_carbon_cube,alkalinity_cube)


#average longitudes
try:
    arag_ss_cube.coord('latitude').guess_bounds()
except:
    print 'cube already has latitude bounds'


try:
    arag_ss_cube.coord('longitude').guess_bounds()
except:
    print 'cube already has longitude bounds'


grid_areas = iris.analysis.cartography.area_weights(arag_ss_cube)
plotting_cube = arag_ss_cube.collapsed('longitude',iris.analysis.MEAN, weights=grid_areas)

qplt.contourf(plotting_cube,31)
plt.show()

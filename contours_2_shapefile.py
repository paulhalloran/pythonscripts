import iris
import matplotlib.pyplot  as plt
import numpy as np
import shapefile

def get_contour_verts(cn):
    contours = []
    # for each contour line
    for cc in cn.collections:
        paths = []
        # for each separate section of the contour line
        for pp in cc.get_paths():
            xy = []
            # for each segment of that section
            for vv in pp.iter_segments():
                xy.append(vv[0])
            paths.append(np.vstack(xy))
        contours.append(paths)
    return contours


file = '/data/NAS-ph290/ph290/cmip5/historical/regridded/CCSM4_tos_historical_r1i1p1_regridded_not_vertically.nc'

cube = iris.load_cube(file)[0]

cn = plt.contour(cube.data,10) 

contours = get_contour_verts(cn)

w = shapefile.Writer()

for cont in contours:
	w.poly(shapeType=3, parts=cont)
	

w.save('/home/ph290/Downloads/my_shapefile.shp')
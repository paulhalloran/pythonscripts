import iris
import numpy as np
import glob
import os
import matplotlib.pyplot as pyplot
import iris.quickplot as qplt
import matplotlib as mpl
from cdo import *
cdo = Cdo()
import itertools


in_dir = '/data/dataSSD0/ph290/'
out_dir = '/data/dataSSD1/ph290/'

def model_names(directory):
        files = glob.glob(directory+'/talk_Oyr_*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[2])
                        models = np.unique(models_tmp)
        return models


def model_names2(directory):
        files = glob.glob(directory+'/*.nc')
        models_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        models_tmp.append(file.split('/')[-1].split('_')[0])
                        models = np.unique(models_tmp)
        return models


def getLevelThickness(cube):
    """
    Calculate level thicknesses. This is useful as guess_bounds()
    in Iris v1.3 gets the bounds wrong for irregular depth grids
    as it just averages level depths (results in level depth not
    being halfway between bounds)
    """
    depth = 0
    thickness = []
    for level in cube.coord('depth').points:
        thickness.append(2 * (level - depth))
        depth += thickness[-1]
    return thickness


def bounds(cube):
    try:
        cube.coord('latitude').guess_bounds()
    except:
        print 'cube already has latitude bounds'
    try:
        cube.coord('longitude').guess_bounds()
    except:
        print 'cube already has longitude bounds'
    return cube


""""
#####
# two ways to get volume mean:
#####

##
# 1
##

model = 'HadGEM2-ES'
cube = iris.load_cube(in_dir+'talk_Oyr_HadGEM2-ES_historical_r1i1p1_1960-2005.nc')
cube = cube.collapsed('time',iris.analysis.MEAN)

cube = bounds(cube)

grid_areas = iris.analysis.cartography.area_weights(cube)
cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)

thicknesses = getLevelThickness(cube)

data = cube.data
for level in np.arange(np.size(thicknesses)):
        data[level,:,:] = data[level,:,:] * thicknesses[level]


cube.data = data

cube_averaged = cube.collapsed('depth',iris.analysis.MEAN)

##
# 2
##


cdo.fldmean(input= '-vertmean -timmean, '+in_dir+'talk_Oyr_HadGEM2-ES_historical_r1i1p1_1960-2005.nc ', output=out_dir+model+'_talk_global_avg.nc',options = '-P 7')

cube2 = iris.load_cube(out_dir+model+'_talk_global_avg.nc')

##
# results: 2.36574764 and 2.3644342
# so going with second one as should be more versatile!
##


cdo.fldmean(input= '-vertmean -timmean, '+in_dir+'talk_Oyr_HadGEM2-ES_historical_r1i1p1_1960-2005.nc ', output=out_dir+model+'_talk_global_avg.nc',options = '-P 7')

cube2 = iris.load_cube(out_dir+model+'_talk_global_avg.nc')

"""

models = model_names(in_dir)
models = list(models)
# models.remove('MRI-ESM1')


for model in models:
    if not(os.path.isfile(out_dir+model+'_talk_global_avg.nc')):
        file = glob.glob(in_dir+'talk_Oyr_'+model+'_historical_r1i1p1_*.nc')[0]
        cdo.fldmean(input= '-vertmean -timmean, '+file+' ', output=out_dir+model+'_talk_global_avg.nc',options = '-P 7')

for model in models:
    cube = iris.load_cube(out_dir+model+'_talk_global_avg.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
    print model +' '+ str(cube.data[0][0][0])

"""
results:
BNU-ESM 2.2887847
CESM1-BGC 2.4025855
CMCC-CESM 2.3941333
CNRM-CM5 1.2035772
CanESM2 2.4028544
GFDL-ESM2G 2.4349463
GFDL-ESM2M 2.4184883
HadGEM2-CC 2.3640435
HadGEM2-ES 2.3644342
IPSL-CM5A-LR 2.4180658
IPSL-CM5A-MR 2.4186924
IPSL-CM5B-LR 2.4171038
MIROC-ESM 2.358621
MIROC-ESM-CHEM 2.358656
MPI-ESM-LR 2.471904
MPI-ESM-MR 2.4991257
MRI-ESM1 1.1399305
NorESM1-ME 2.4186153
GLODAP 2.3929118949033
"""

#####
# due to a issue with how I've compiled stuff (hdf libraries), I had to do GLODAP on JASMIN like:
# cdo vertmean -fldmean -timmean -selvar,TAlk GLODAPv2.2016b.TAlk.nc GLODAPv2.2016b.TAlk_glob_mean.nc
# result in:
# /data/NAS-ph290/ph290/misc_data/GLODAPv2.2016b.TAlk_glob_mean.nc
# mean is (2332.27280205/1.0e3)*1.026
# GLODAP = mole equivalent per unit mass
# CMIP5 = mole_equivalent / (mol m-3))*1.026
#####

mpl.rcdefaults()

fsize = 12

font = {'family' : 'monospace',
        'weight' : 'bold',
        'family' : 'serif',
        'size'   : fsize}

mpl.rc('font', **font)
marker = itertools.cycle(('o', 'v', '^', '<', '>', 's', '8','p'))


for model in models:
    cube = iris.load_cube(out_dir+model+'_talk_global_avg.nc','sea_water_alkalinity_expressed_as_mole_equivalent')
    plt.scatter(cube.data[0][0][0],1.0,color = 'r')


plt.scatter(2.3929118949033,1,color='k')

plt.savefig('/home/ph290/Documents/figures/cmip5_glodap_global_alk.png')
plt.show(block = False)

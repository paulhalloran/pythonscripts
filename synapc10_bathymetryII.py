import iris
import numpy as np
import matplotlib.pyplot as plt
import iris.plot as iplt
import iris.quickplot as qplt
import cartopy
import matplotlib.cm as mpl_cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


bathy_cube = iris.load_cube('/home/ph290/data0/misc_data/ETOPO1_Bed_c_gmt4.grd')

lon_west = -60.0
lon_east = 0
lat_south = 20.0
lat_north = 90.0 

cube_region_tmp = bathy_cube.intersection(longitude=(lon_west, lon_east))
bathy_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))


plt.close('all')
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')


cmap1 = mpl_cm.get_cmap('seismic')
cmap2 = mpl_cm.get_cmap('terrain')
cmap2 = mpl_cm.get_cmap('gist_rainbow')

land_110m = cartopy.feature.NaturalEarthFeature('physical', 'land', '110m',
                                        edgecolor='face',
                                        facecolor = '#d3d3d3')

lats = bathy_cube_region.coord('latitude').points
lons = bathy_cube_region.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)

ax.plot_surface(lats, lons, bathy_cube_region.data, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)

#iplt.contour(bathy_cube_region,np.linspace(-150,0,4),colors = 'k')
plt.savefig('/home/ph290/Documents/figures/bathy_II.png')
#plt.show()

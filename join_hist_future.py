import glob
import numpy as np
from cdo import *
cdo = Cdo()

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[0])
			models = np.unique(models_tmp)
	return models

input_directory = '/data/dataSSD0/ph290/cmip5/spco2_regridded/'
output_directory = '/data/dataSSD0/ph290/cmip5/spco2_regridded/joined/'

models = model_names(input_directory)

for model in models:
	files = glob.glob(input_directory+model+'*_regridded.nc')
 	files = ' '.join(files)
	cdo.mergetime(input = files,output = output_directory+model+'_hist_plus_rcp_85.nc')

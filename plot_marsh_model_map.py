import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import glob
import pandas as pd
import cartopy.crs as ccrs
import cartopy.feature as cfeature

#########
# Specify the data you want to read in
#########
directory = '/Users/ph290/Downloads/s2p3_reg_v1.0/output/'
file = 'output_map_done'
variable_to_plot = "net heat flux"
column_names = ["longitude", "latitude", "depth", "Hunter-Simpson parameter","stratification","net heat flux","carbon accumulation"]
#Note, this is a 2D dataset, not 3D. Depth is a 2D field of the depth of the bottom of the ocean at that point, not a column of level depths.

#########
# Read data in
#########
df = pd.read_csv(directory + file,header=None, sep=r'\s{1,}',names = column_names)

#########
# Put data into an iris cube (can then be worked with simply)
#########

latitudes = np.unique(df['latitude'].values)
longitudes = np.unique(df['longitude'].values)
depths = np.unique(df['depth'].values)
latitude = iris.coords.DimCoord(latitudes, standard_name='latitude', units='degrees')
longitude = iris.coords.DimCoord(longitudes, standard_name='longitude', units='degrees')
cube = iris.cube.Cube(np.zeros((latitudes.size, longitudes.size), np.float32),standard_name='sea_surface_temperature', long_name='Sea Surface Temperature', var_name='tos', units='K',dim_coords_and_dims=[(latitude, 0), (longitude, 1)])
X,Y = np.meshgrid(cube.coord('longitude').points,cube.coord('latitude').points)
cube.data = df[variable_to_plot].values.reshape(X.shape)


#########
# Plot the data
#########

land_50m = cfeature.NaturalEarthFeature('physical', 'land', '50m',
                                        edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])


# Choose map projection - see http://scitools.org.uk/cartopy/docs/v0.14/crs/projections.html#cartopy-projections
ax = plt.axes(projection=ccrs.PlateCarree())
# Get the data onto the map. Can change things - e.g. colour map used from hot to other - see https://matplotlib.org/examples/color/colormaps_reference.html
CS = ax.pcolormesh(cube.coord('longitude').points, cube.coord('latitude').points, cube.data,
                transform=ccrs.PlateCarree(),
                cmap='hot')
ax.add_feature(land_50m)
ax.coastlines('50m')
plt.title = variable_to_plot
plt.colorbar(CS, ax=ax)
plt.show()

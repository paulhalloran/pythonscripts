import pandas as pd
import iris
import numpy as np
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.plot as iplt
import iris.coord_categorisation


directory = '/data/NAS-ph290/ph290/observations/nw_european_shelf/clim_db_v01-2/'

surf_t_file = directory+'surface_temperature/mon_jun_tempS.txt'
bott_t_file = directory+'nearbed_temperature/mon_jun_tempB.txt'

def put_in_cube(filename):
    file = open(filename, 'r')
    read_in_data = file.read()

    longitudes = np.array(read_in_data.split('\n')[0].split()[3::]).astype(float)
    latitudes = np.zeros(len(read_in_data.split('\n')[1:-2]))
    data = np.zeros([len(latitudes),len(longitudes)])
    data[:] = np.nan

    for i,tmp in enumerate(read_in_data.split('\n')[1:-2]):
        tmp2 = tmp.split()
        latitudes[i] = np.float(tmp.split()[0])
        for j,dummy in enumerate(longitudes):
            data[i,j] = np.float(tmp.split()[j])


    latitude = iris.coords.DimCoord(latitudes, standard_name='latitude', units='degrees')
    longitude = iris.coords.DimCoord(longitudes, standard_name='longitude', units='degrees')
    cube = iris.cube.Cube(np.zeros((latitudes.size, longitudes.size), np.float32),standard_name=None, long_name=None, var_name=None, units=None,dim_coords_and_dims=[(latitude, 0), (longitude, 1)])
    # X,Y = np.meshgrid(cube.coord('longitude').points,cube.coord('latitude').points)
    cube.data = np.ma.masked_invalid(data)
    return cube



surf_t = put_in_cube(surf_t_file)
bott_t = put_in_cube(bott_t_file)

month = 6
surf_t_model = iris.load_cube('/home/ph290/Documents/s2p3_rv2.0_processing/surface_temperature_1988.nc')
iris.coord_categorisation.add_month_number(surf_t_model, 'time', name='month')
surf_t_model = surf_t_model[np.where(surf_t_model.coord('month').points == month)].collapsed('time',iris.analysis.MEAN)
bott_t_model = iris.load_cube('/home/ph290/Documents/s2p3_rv2.0_processing/bottom_temperature_1988.nc')
iris.coord_categorisation.add_month_number(bott_t_model, 'time', name='month')
bott_t_model = bott_t_model[np.where(bott_t_model.coord('month').points == month)].collapsed('time',iris.analysis.MEAN)


lon_west = -12.0
lon_east = 3.0
lat_south = 48
lat_north = 60

cube_region_tmp = surf_t_model.intersection(longitude=(lon_west, lon_east))
surf_t_model = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = bott_t_model.intersection(longitude=(lon_west, lon_east))
bott_t_model = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = surf_t.intersection(longitude=(lon_west, lon_east))
surf_t = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = bott_t.intersection(longitude=(lon_west, lon_east))
bott_t = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cmap = plt.get_cmap('viridis')
levels = np.linspace(-1,5.0,30)
plt.close('all')
plt.figure(1)
plt.subplot(211)
iplt.contourf(surf_t-bott_t,levels,cmap=cmap)
plt.colorbar()
plt.subplot(212)
iplt.contourf(surf_t_model-bott_t_model,levels,cmap=cmap)
plt.colorbar()
plt.savefig('/home/ph290/Documents/figures/jun_surf_bott.png')
plt.show(block = False)

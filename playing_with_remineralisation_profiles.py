import matplotlib.pyplot as plt
import numpy as np

kmax=15
dcaco3 = 6500.0
dz_loc = np.array([50,70,100,140,190,240,290,340,390,440,490,540,590,640,690])
#In MITGCM used for circulatoin here
zw = np.zeros(kmax)
dzt = np.zeros(kmax)
rcak = np.zeros(kmax)
rcab = np.zeros(kmax)

for k in range(0,kmax):
    dzt[k] = dz_loc[k]

zw[0] = dz_loc[0]
for k in range(1,kmax):
        zw[k] = zw[k-1] + dz_loc[k]



# print 'levels: ',zw
#
# rcak[0] = (((np.exp(-zw[0]/dcaco3)-1.0)) * -1.0)/dzt[0]
# rcab[0] = 1./dzt[0]
# for k in range(1,kmax):
#     rcak[k] = (-1.0 * (np.exp((-1.0 * zw[k]/dcaco3)))/dzt[k]) + (np.exp((-1.0 * zw[k-1])/dcaco3)/dzt[k])
#     rcab[k] = 1.0 - (np.exp((-1.0 * zw[k-1])/dcaco3))/dzt[k]



#rcak[kmax-1] = ((-1.0 * (np.exp(-zw[0]/dcaco3)-1.0)))/dzt[kmax-1]
#rcab[kmax-1] = 0.0
#for k in range(1,kmax):
#    rcak[kmax-k-1] = (-1.0 * (np.exp((-1.0 * zw[k]/dcaco3)))/dzt[kmax-k-1]) + (np.exp((-1.0 * zw[k-1])/dcaco3)/dzt[kmax-k-1])
#    rcab[kmax-k-1] = 1.0/dzt[kmax-k-1] - ((np.exp((-1.0 * zw[k-1])/dcaco3))/dzt[kmax-k-1])


rcak[0] = (-1.0 * (np.exp(-zw[0]/dcaco3)-1.0))/dzt[0]
#                   -(exp(-zw(1)/dcaco3)-1.0)/dzt(1)
rcab[0] = 1.0/dzt[0]
for k in range(1,kmax):
    rcak[k] = (-1.0 * (np.exp(-zw[k]/dcaco3)))/dzt[k] + (np.exp(-zw[k-1]/dcaco3))/dzt[k]
    rcab[k] = (np.exp(-1.0 * zw[k-1]/dcaco3))/dzt[k]

"""
  do k=1,km
!     linear increase wd0-200m with depth
	 wd(k) = (wd0+6.0e-2*zt(k))/daylen/dzt(k) ! [s-1]
	 rkwz(k) = 1./(kw*dzt(k))
  enddo
  ztt(1)=0.0
  do k=1,km-1
	 ztt(k+1)=(-1)*zw(k)
  enddo
  ztt(km)=(-1)*zw(km-1)
# if defined O_carbon || defined O_npzd_alk

!---------------------------------------------------------------------
!     calculate variables used in calcite remineralization
!---------------------------------------------------------------------

  rcak(1) = -(exp(-zw(1)/dcaco3)-1.0)/dzt(1)
  rcab(1) = 1./dzt(1)
  do k=2,km
	rcak(k) = -(exp(-zw(k)/dcaco3))/dzt(k)
 &          + (exp(-zw(k-1)/dcaco3))/dzt(k)
	rcab(k) = (exp(-zw(k-1)/dcaco3))/dzt(k)
  enddo
"""


print 'rcak: ',rcak
print 'rcab: ',rcab

plt.close('all')
plt.plot(np.cumsum(rcak*dz_loc),'r',label = 'cumsum rcak')
plt.plot(rcab*dz_loc,'b',label='rcab')
plt.legend()
plt.show(block = False)

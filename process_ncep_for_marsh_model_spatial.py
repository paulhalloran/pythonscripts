##################################
# importing modules we will be using later
##################################

import iris
import numpy as np
import matplotlib.pyplot as plt
import glob
import pandas as pd
import subprocess
import os
import csv
from math import cos, asin, sqrt


##################################
# things that need to be set up for each new set of meterological data you wanyt to produce
##################################

print 'export HDF5_DISABLE_VERSION_CHECK=1'

min_depth_lim = 30.0
max_depth_lim = 100.0

domain_file = '/home/ph290/Documents/tides/tides/s12_m2_s2_n2_h_map.dat'

#Specify where the ncep data is stored on your computer. '/data/dataSSD0/ph290/ncep/' refers to a specific directory on my computer where I downloaded the NCEP data to
directory_containing_files_to_process = '/data/dataSSD0/ph290/ncep/'

#Specify the name of the file you ultimately want to produce, which will hold the meterological data to be read in by Bob's model
output_filename = 'meterological_data'

# just for ref. this is what bob's model requires: idum,wind_speed,wind_dir,cloud,airT,airP,humid

df = pd.read_csv(domain_file,names=['lon','lat','t1','t2','t3','t4','t5','t6','depth'],delim_whitespace=True,skiprows=[0])


##################################
# some functions we will make use of later
##################################

def replace_character_in_file(filename,character1,character2):
    with open(filename, 'r') as infile,open(filename+'cleaned_up', 'w') as outfile:
        data = infile.read()
        data = data.replace(character1, character2)
        outfile.write(data)
    subprocess.call(['mv '+filename+'cleaned_up'+' '+filename], shell=True)

def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
    return 12742 * asin(sqrt(a))

def closest(data, latitude_point,longitude_point):
    return min(data, key=lambda p: distance(latitude_point,longitude_point,p[0],p[1]))




##################################
# Other things that need to be defined, but probably not changed
##################################

input_variables = ['vwnd','uwnd','tcdc','rhum','air.2m','pres'] #these are teh names of the variables in teh NCEP files
# North/South wind vector, East/West wind vector, Total cloud cover, relative humidity, 2m air temperature, sea level pressure

minimum_latitude = str(df['lat'].min())
maximum_latitude = str(df['lat'].max())

minimum_longitude = str(df['lon'].min())
maximum_longitude = str(df['lon'].max())

##################################
# Tidying up any leftover fiels so that the script does not get confused
##################################

# remove temporary file if it exists (i.e. feftover after previous failed attempts)
try:
    os.remove(directory_containing_files_to_process+'temporary_file1.nc')
except:
    print 'no temporary file to remove'


try:
    os.remove(directory_containing_files_to_process+'temporary_file1.nc')
except:
    print 'no temporary file to remove'

##################################
# pre-processing, reading, and storing sensibly the data from each of NCEP variables, extracting data for the location of interest
##################################

#Make a dictionary which will hold all of the data which will be written out to the text file to drive the model
data={}

files = glob.glob(directory_containing_files_to_process + input_variables[0]+'.*.nc')
#join the filenames into a single line of text (required for the next stage)
files = ' '.join(files)
subprocess.call(['cdo mergetime '+files+' '+directory_containing_files_to_process+'temporary_file1.nc'], shell=True)
subprocess.call(['cdo sellonlatbox,'+minimum_longitude+','+maximum_longitude+','+minimum_latitude+','+maximum_latitude+' '+directory_containing_files_to_process+'temporary_file1.nc '+directory_containing_files_to_process+'temporary_file2.nc'], shell=True)
os.remove(directory_containing_files_to_process+'temporary_file1.nc')
#read that single file in to a variable called cube
cube = iris.load_cube(directory_containing_files_to_process+'temporary_file2.nc')
latitude_points = cube.coord('latitude').points
longitude_points = cube.coord('longitude').points
os.remove(directory_containing_files_to_process+'temporary_file2.nc')

lats = []
lons = []

longitude_points = np.array(longitude_points)
longitude_points[np.where(longitude_points > 180.0)] -= 360.0

for latitude_point in latitude_points:
    for longitude_point in longitude_points:
        lats.append(latitude_point)
        lons.append(longitude_point)


lons = np.array(lons)
lons[np.where(lons > 180.0)] -= 360.0

lat_lon_df = pd.DataFrame(
    {'lat': lats,
     'lon': lons
    })


final_lat=[]
final_lon=[]
final_depth=[]

for i in range(df.shape[0]):
    lat = df.loc[i]['lat']
    lon = df.loc[i]['lon']
    tmp = closest(lat_lon_df[['lat','lon']].values,lat ,lon)
    final_lat.append(tmp[0])
    final_lon.append(tmp[1])
    final_depth.append(df.loc[i]['depth'])


lat_lon_depth_df = pd.DataFrame(
    {'lat': final_lat,
     'lon': final_lon,
     'depth': final_depth
    })



for v,latitude_point in enumerate(latitude_points):
    for u,longitude_point in enumerate(longitude_points):
        #avoiding grid boxes where we have no points within the depth-range of interest
        min_depth = lat_lon_depth_df.loc[(lat_lon_depth_df['lat'] == latitude_point) & (lat_lon_depth_df['lon'] == longitude_point)]['depth'].min()
        max_depth = lat_lon_depth_df.loc[(lat_lon_depth_df['lat'] == latitude_point) & (lat_lon_depth_df['lon'] == longitude_point)]['depth'].max()
        if (min_depth < max_depth_lim) & (max_depth > min_depth_lim):
            #next we will loop through the variables one by one and read the data in
            for single_input_variable in input_variables:
                print 'processing: '+single_input_variable
                #read in the names of all the files for that variable
                files = glob.glob(directory_containing_files_to_process + single_input_variable+'.*.nc')
                #join the filenames into a single line of text (required for the next stage)
                files = ' '.join(files)
                #combine the separate files for that variable into a single file so that it is easier to work with
                subprocess.call(['cdo mergetime '+files+' '+directory_containing_files_to_process+'temporary_file1.nc'], shell=True)
                subprocess.call(['cdo sellonlatbox,'+minimum_longitude+','+maximum_longitude+','+minimum_latitude+','+maximum_latitude+' '+directory_containing_files_to_process+'temporary_file1.nc '+directory_containing_files_to_process+'temporary_file2.nc'], shell=True)
                os.remove(directory_containing_files_to_process+'temporary_file1.nc')
                #read that single file in to a variable called cube
                cube = iris.load_cube(directory_containing_files_to_process+'temporary_file2.nc')
                #adds this timeseries data into the dictionary we created
                data[single_input_variable] = cube[:,v,u].data
                #removes the temporary file we created
                os.remove(directory_containing_files_to_process+'temporary_file2.nc')


            ##################################
            # process the read-in data and get in the right format for output
            ##################################

            #To make things neeter, we putting the data, which has so far been stored in a dictionary into a pandas dataframe
            #Once in this format it just makes other thinsg we may want to do easier. See https://www.tutorialspoint.com/python_pandas/python_pandas_dataframe.htm
            df = pd.DataFrame(data=data)
            #this is an annoyingly complicated looking way of getting the list of year numbers in just the same format
            #as they are in the example meterological data file used in the oroginal setup
            #Essentially range(1, len(df) + 1) makes a list of numbers from 1 to x, where x is the length of the dataframe (the len(df) bit), i.e. the length of the meterological data we have
            #the ''[format(x, ' 5d') for x in r...' bit just takes each of those numbers one by one and formats them so istead of bineg '1', '2'... '10' etc. then are '    1', '    2'... '   10' etc. so that the colums line up correctly in the output file
            #This data is then stored in a new column in teh dataframe, called 'day_number'
            df['day_number'] = [format(x, ' 5d') for x in range(1, len(df) + 1)]
            #this simply converst the data in the 2m air temperature column from K to degrees C
            df['air.2m'] = df['air.2m'] - 273.15
            #this gets the pressure in the units used by bob's model
            df['pres'] = df['pres'] / 100.0
            #As far as I could see, NCEP did not supply wind speed, so I've calculated it using pythagoras (square root of the sum of the squares of the x and y vector give teg lenth of the 3rd side of the triangle)
            df['wind_speed'] = np.sqrt(np.square(df['uwnd']) + np.square(df['vwnd']))
            #similarly, wind direction was not supplied, but this can be calculated using the function arctan2, then converted from radians to degrees
            df['wind_direction'] = np.rad2deg((np.arctan2(df['vwnd'],df['uwnd'])) + np.pi)
            #In bob's original meterological file the data is all to two decimal places, so the line below rounds all of teh data to two decimal places.
            df = df.round(2)

            ##################################
            # Write the data out to the file
            ##################################

            #this line simply writes out the olumns we are intersted in, in teh order we are intersted in, in the firmat we are intersted in (2 decomal places, 10 characters between columns) to the file we specified at the start
            df[['day_number','wind_speed','wind_direction','tcdc','air.2m','pres','rhum']].to_csv(directory_containing_files_to_process+output_filename+'lat'+str(latitude_point)+'lon'+str(longitude_point)+'.dat', index=False, header=False, float_format='%10.2f')
            #unfortunately, I could not quickly find a way to successfully write the file without commas between teh columns, so the line below simply strips the columns out from the columns.
            replace_character_in_file(directory_containing_files_to_process+output_filename+'lat'+str(latitude_point)+'lon'+str(longitude_point)+'.dat',',','')

###
#
#  Data initially processed by /home/ph290/Documents/python_scripts/calculate_mld_big_files_no_regridding_deep_capability.py
#/home/ph290/Documents/python_scripts/process_monthly_density_past1000.py
#
###

#uncomment if running for first time
#################

##################

import numpy as np
import iris
import matplotlib.pyplot as plt
import time
import glob
from scipy import signal
import scipy
import scipy.stats
import statsmodels.api as sm
import running_mean_post
import cartopy.crs as ccrs
import iris.analysis.cartography
import numpy.ma as ma
import os
import running_mean as rm
import running_mean_post as rmp
from scipy.ndimage import gaussian_filter1d
import subprocess
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
from scipy import signal
import iris.analysis.stats
import iris.plot as iplt
import matplotlib.cm as mpl_cm
import matplotlib.gridspec as gridspec
from scipy.ndimage import gaussian_filter1d
import scipy.stats as stats



def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[3])
	models = np.unique(models_tmp)
	return models

##########################################
# Mian part of code                      #
##########################################

################################
# produce area means if they don;t already exist
################################

'''
lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

files = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/*.nc')
for i,my_file in enumerate(files):
        print 'processing ',i,' in ',np.size(files)
        name = my_file.split('/')[-1]
        test = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'+name)
        if np.size(test) == 0:
                cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = '/data/data1/ph290/cmip5/last1000/mld/spatial_avg/'+name,  options = '-P 7')
'''

################################
# Prepare the plots
################################



#start_year = 850
start_year = 1000
end_year = 1849
expected_years = start_year+np.arange((end_year-start_year)+1)


##########################################
# Read in Reynolds d18O data and detrend #
##########################################

#r_data_file = '/home/ph290/data0/reynolds/ultra_data.csv' - is this any different?
r_data_file = '/data/NAS-ph290/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
tmp = r_data[:,1]
tmp=signal.detrend(tmp)
loc = np.where((np.logical_not(np.isnan(tmp))) & (r_data[:,0] >= start_year) & (r_data[:,0] <= end_year))
tmp = tmp[loc]
tmp_yr = r_data[loc[0],0]
tmp -= np.min(tmp)
tmp /= np.max(tmp)
bivalve_data_initial = tmp
bivalve_year = tmp_yr
bivalve_data_initial=signal.detrend(bivalve_data_initial)


#############################################################################################################################
# Models: remove linear trend and normalise variability, then put all models on same time axis and perform multi-model mean #
#############################################################################################################################


# input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'
input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_larger/'
# input_directory = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg_no_spikes/'

models = model_names(input_directory)
models = list(models)
#models.remove('FGOALS-gl')
# models.remove('CSIRO-Mk3L-1-2')
# models.remove('HadCM3')
# print '*******************************************'
# print '*******************************************'
# print ' As soon as processed bring MIROC BACK IN! '
# print '*******************************************'
# print '*******************************************'
# models.remove('MIROC-ESM') #temporarily until regridded...
#Note previously had two FGOALS models, sp removed one so a better ensmeble
#FGOALS-gl chosen, because there seems to be a problem calculating its mixed layer depths due to teh levels being upside down
models = np.array(models)

multi_model_density = np.zeros([1+end_year-start_year,np.size(models)])
multi_model_density[:] = np.NAN
multi_model_std_density = multi_model_density.copy()

for i,model in enumerate(models):
	print model
	period = 'JJA'
	# 	period = 'annual_mean'
	cube = iris.load_cube(input_directory+'density_mixed_layer_'+model+'*_'+period+'*.nc')
	#try:
	cube = cube.collapsed(['longitude', 'latitude'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	#except:
	#	cube = cube.collapsed(['longitude', 'latitude', 'ocean sigma over z coordinate'], iris.analysis.MEAN) #just removing extraneous empty dimensions
	data = cube.data
	coord = cube.coord('time')
	dt = coord.units.num2date(coord.points)
	yrs = np.array([coord.units.num2date(value).year for value in coord.points])
	#smoothing_var = 1
	#if smoothing_var > 0:
#		data = gaussian_filter1d(data,smoothing_var)
	data2=signal.detrend(data)
	#data2 = data2-np.min(data2)
	#data3 = data2/(np.max(data2))
	#data3 -= np.nanmean(data3)
	data3 = data2
	data3 -= np.nanmean(data3)
	for index,y in enumerate(expected_years):
	    loc2 = np.where(yrs == y)
	    if np.size(loc2) != 0:
	        multi_model_density[index,i] = data3[loc2]



#smoothing_var = 1
#if smoothing_var > 0:
#	for i,model in enumerate(models):
#		#multi_model_density[:,i] = rm.running_mean(multi_model_density[:,i],smoothing_var)
#		multi_model_density[:,i] = gaussian_filter1d(multi_model_density[:,i],smoothing_var)

multi_model_mean_density = np.nanmean(multi_model_density, axis = 1)
multi_model_std_density = np.nanstd(multi_model_density, axis = 1)



##########################################
# plotting                               #
##########################################

"""
# plt.close('all')
fig, ax = plt.subplots(figsize=(8, 4))
#y = rm.running_mean(multi_model_mean_density,smoothing_var)
#y2 = rm.running_mean(multi_model_std_density,smoothing_var)
y = multi_model_mean_density
y2 = multi_model_std_density
#data2 = y-np.min(y)
#data3 = data2/(np.max(data2))
#y = data3 - np.nanmean(data3)
###
ax.plot(expected_years,y,'k',lw=1.75,alpha=0.5)
# ax.fill_between(expected_years, y + y2, y  - y2, color="none", facecolor='blue', alpha=0.2)
# steps = 1
# stdevs = 1.0
# tmp = np.arange(float(steps))/float(steps)
# for i in range(steps):
# 	y2b = y2.copy()*(1.0/float(i+1))
# 	#y2b = y2.copy()*tmp[i]
# 	ax.fill_between(expected_years, y + y2b*stdevs, y  - y2b*stdevs, color="none", facecolor='k', alpha=1.0/float(steps))
stdevs = 1.0
ax.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
ax.fill_between(expected_years, y + y2*stdevs*0.5, y  - y2*stdevs*0.5, color="none", facecolor='k', alpha=0.25)
ax2 = ax.twinx()
if smoothing_var > 0:
	#y_bivalve = rm.running_mean(bivalve_data_initial,smoothing_var)
	y_bivalve = gaussian_filter1d(bivalve_data_initial,smoothing_var*1.0)
	#NOTE, I've doubled the smoothing on the bivalve data, because it is not getting the smoothing of an ensemble mean - not sure what the right thing to choose here is. Doubling is a bit arbitrary
else:
	y_bivalve = bivalve_data_initial
ax2.plot(bivalve_year,y_bivalve*0.8,'r',lw=2,alpha=0.5)
# ax2.plot(bivalve_year+20,y_bivalve,'g',lw=2,alpha=0.75)
#ax2.plot(bivalve_year[0:700],y_bivalve[0:700]*0.5,'r',lw=1.75,alpha=0.5)
#ax2.plot(bivalve_year[-200::]+20,y_bivalve[-200::]*0.5,'g',lw=1.75,alpha=0.5)
ax.set_ylim(-0.22,0.22)
ax2.set_ylim(-0.5,0.5)
ax.set_xlabel('Calendar Year')
ax.set_ylabel('PMIP3 density anomaly')
ax2.set_ylabel('bivalve d$^{18}$O anomaly')
ax2.yaxis.label.set_color('red')
plt.show(block = False)
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_'+period+'_2a_deep.png')
print 'grey = 2 standard deviatins (i.e. +/- 2*stdev)'

"""
###



#uncomment if running for first time
#################

#################





###
# Composite maps
###

y = multi_model_mean_density.copy()
y2 = multi_model_std_density.copy()

# Write final density timeseries to text file
f = open('/data/NAS-ph290/ph290/misc_data/n_ice_dens.txt', 'w')
for i,dummy in enumerate(expected_years):
	f.write(str(expected_years[i])+','+str(y[i])+'\n')

f.close()


def return_ensmeble_mean(spatial_files,smoothing_var,y):
	tmp_cube = iris.load_cube(spatial_files[0])[0]
	ensmeble_data = np.zeros([np.size(spatial_files),np.shape(tmp_cube.data)[0],np.shape(tmp_cube.data)[1]])
	ensmeble_data[:] = np.nan
	for i,file in enumerate(spatial_files):
		cube = iris.load_cube(file)
		cube.data = scipy.signal.detrend(cube.data, axis=0)
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		loc = np.where(yrs >= start_year)[0]
		cube = cube[loc]
		coord = cube.coord('time')
		dt = coord.units.num2date(coord.points)
		yrs = np.array([coord.units.num2date(value).year for value in coord.points])
		y_tmp = gaussian_filter1d(y,smoothing_var)
# 		y_tmp = y
		loc2 = np.where(y_tmp >= np.nanmean(y_tmp))[0]
		loc3 = np.where(y_tmp < np.nanmean(y_tmp))[0]
		high_yrs = expected_years[loc2]
		low_yrs = expected_years[loc3]
		common_years_high = np.intersect1d(yrs,high_yrs)
		common_years_low = np.intersect1d(yrs,low_yrs)
		loc_high = np.where(np.in1d(yrs,common_years_high))
		loc_low = np.where(np.in1d(yrs,common_years_low))
		cube_high_minus_low = cube[loc_low].collapsed('time',iris.analysis.MEAN) - cube[loc_high].collapsed('time',iris.analysis.MEAN)
 		# qplt.contourf(cube_high_minus_low,np.linspace(-0.2,0.2,21))
#  		plt.show()
		ensmeble_data[i,:,:] = cube_high_minus_low.data
	ensmeble_mean = np.nanmean(ensmeble_data,axis = 0)
	tmp_cube.data = ensmeble_mean
	return tmp_cube



smoothing_var = 10

spatial_directory = '/data/NAS-ph290/ph290/cmip5/last1000/'

spatial_files = []
for model in models:
	try:
		spatial_files.append(glob.glob(spatial_directory+model+'_psl_*.nc')[0])
	except:
		print model+' missing'

spatial_directory = '/data/NAS-ph290/ph290/cmip5/last1000/'
ens_mean = return_ensmeble_mean(spatial_files,smoothing_var,y)
plt.figure(0)
qplt.contourf(ens_mean,21)
#np.linspace(-0.2,0.2,21))
plt.gca().coastlines()
plt.show(block = False)

ncep_mslp = iris.load_cube('/data/NAS-ph290/ph290/reanalysis/NCEP_v2/mslp_mon.nc')
coord = ncep_mslp.coord('time')
dt = coord.units.num2date(coord.points)
yrs = np.array([coord.units.num2date(value).year for value in coord.points])
month = np.array([coord.units.num2date(value).month for value in coord.points])
year_mon = yrs+month/12.0
nao = np.genfromtxt('/data/NAS-ph290/ph290/misc_data/nao_index.tim',skip_header = 10)
nao_year_mon = nao[:,0]+nao[:,1]/12.0
common_dates = np.intersect1d(year_mon,nao_year_mon)
nao_loc = np.where(np.in1d(nao_year_mon,common_dates))
ncep_loc = np.where(np.in1d(year_mon,common_dates))

ts = nao[nao_loc,2]
cube = ncep_mslp[ncep_loc].copy()
cube = iris.analysis.maths.multiply(cube, 0.0)
ts2 = np.swapaxes(np.swapaxes(np.tile(ts,[73,144,1]),1,2),0,1)
cube = iris.analysis.maths.add(cube, ts2)
out_cube = iris.analysis.stats.pearsonr(ncep_mslp[ncep_loc], cube, corr_coords=['time'])

lon_west = -100.0
lon_east = 50.0
lat_south = 15.0
lat_north = 90.0

cube_region_tmp = out_cube.intersection(longitude=(lon_west, lon_east))
out_cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cube_region_tmp = ens_mean.intersection(longitude=(lon_west, lon_east))
ens_mean_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))

cmap1 = mpl_cm.get_cmap('RdBu_r')

plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(7)
fig.set_figwidth(10)
gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)

import cartopy.crs as ccrs

ax2 = plt.subplot(gs[60:100,0:45],projection=ccrs.PlateCarree())
ax1 = plt.subplot(gs[60:100,55:100],projection=ccrs.PlateCarree())
ax3 = plt.subplot(gs[0:45,0:100])


import matplotlib.mlab as ml


# plt.close('all')
# plt.title('PMIP3 Sea Level Pressure composites',  fontsize=10)
# ax = ax1.subplot(122)
cube = ens_mean_region
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
data = cube.data
C1 = ax1.contourf(lons, lats, data, np.linspace(-15,15,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax1.coastlines()
ax1.set_title('PMIP3 Sea Level\nPressure composites',  fontsize=12)
cb1 = plt.colorbar(C1,ax=ax1,orientation='horizontal',ticks=[-15, 0, 15])

#,cax=ax3)
# plt.colorbar(orientation='horizontal')

cube = out_cube_region
lats = cube.coord('latitude').points
lons = cube.coord('longitude').points
lons, lats = np.meshgrid(lons, lats)
data = cube.data
C2 = ax2.contourf(lons, lats, data, np.linspace(-1,1,51),cmap = cmap1, extend='both',transform=ccrs.PlateCarree())
ax2.coastlines()
ax2.set_title('NCEP NAO v. Sea Level\nPressure correlation',  fontsize=12)
cb2 = plt.colorbar(C2,ax=ax2,orientation='horizontal',ticks=[-1, 0, 1])
# plt.colorbar(orientation='horizontal')


smoothing_var = 1.0

y = gaussian_filter1d(y,smoothing_var)
y2 = gaussian_filter1d(y2,smoothing_var)


ax3.plot(expected_years,y,'k',lw=1.75,alpha=0.8)
stdevs = 1.0

ax3.fill_between(expected_years, y + y2*stdevs, y  - y2*stdevs, color="none", facecolor='k', alpha=0.25)
ax3.fill_between(expected_years, y + y2*stdevs*0.5, y  - y2*stdevs*0.5, color="none", facecolor='k', alpha=0.25)
ax3b = ax3.twinx()
# if smoothing_var > 0:
# 	#y_bivalve = rm.running_mean(bivalve_data_initial,smoothing_var)
# 	y_bivalve = gaussian_filter1d(bivalve_data_initial,smoothing_var*1.0)
# 	#NOTE, I've doubled the smoothing on the bivalve data, because it is not getting the smoothing of an ensemble mean - not sure what the right thing to choose here is. Doubling is a bit arbitrary
# else:

# y_bivalve = rm.running_mean(bivalve_data_initial,5)
y_bivalve = gaussian_filter1d(bivalve_data_initial.copy(),smoothing_var)
# y_bivalve = bivalve_data_initial

# y_bivalve -= np.nanmean(y_bivalve)
ax3b.plot(bivalve_year,y_bivalve,'r',lw=2,alpha=0.6)
# ax2.plot(bivalve_year+20,y_bivalve,'g',lw=2,alpha=0.75)
#ax2.plot(bivalve_year[0:700],y_bivalve[0:700]*0.5,'r',lw=1.75,alpha=0.5)
#ax2.plot(bivalve_year[-200::]+20,y_bivalve[-200::]*0.5,'g',lw=1.75,alpha=0.5)
ax3.set_ylim(-0.2,0.2)
ax3b.set_ylim(-0.6,0.6)

ax3.set_xlim(1000,1850)
ax3.set_xlabel('Calendar Year')
ax3.set_ylabel('PMIP3 North Iceland\ndensity anomaly')
# (kgm$^{-3}$)')
ax3b.set_ylabel('bivalve d$^{18}$O anomaly')
ax3b.yaxis.label.set_color('red')

plt.annotate('a', xy=(.15, .925),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)

plt.annotate('b', xy=(.15, .4),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)

plt.annotate('c', xy=(.535, .4),  xycoords='figure fraction',
                horizontalalignment='center', verticalalignment='center', fontsize=20)


plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_with_psl_II_not_normalised.png')
plt.savefig('/home/ph290/Documents/figures/palaeo_amo_density_fig_with_psl_II_normalised.ps')


plt.show(block = False)

'''

y = multi_model_mean_density.copy()

print scipy.stats.pearsonr(y_bivalve[::-1], y)

no = 200
x = np.zeros(no)
for i in np.arange(no)+2:
	x_tmp = rm.running_mean(bivalve_data_initial[::-1],i)
	y_tmp = rm.running_mean(y,j)
	loc = np.where(np.logical_not((np.isnan(x_tmp)) | (np.isnan(y_tmp))))
	x_tmp = x_tmp[loc]
	y_tmp = y_tmp[loc]
	x[i] = scipy.stats.pearsonr(x_tmp,y_tmp)[0]


plt.plot(x)
plt.show()

i=30
y = multi_model_mean_density.copy()
plt.plot(rm.running_mean(bivalve_data_initial[::-1].copy(),i))
plt.plot(rm.running_mean(y,i)*5.0)
plt.show()

scipy.stats.pearsonr(gaussian_filter1d(bivalve_data_initial[::-1],i),gaussian_filter1d(y,i))



max_val = 0.0

for i in np.arange(80)+2:
	for j in np.arange(80)+2:
		x_tmp = rm.running_mean(bivalve_data_initial[::-1],i)
		y_tmp = rm.running_mean(y,j)
		loc = np.where(np.logical_not((np.isnan(x_tmp)) | (np.isnan(y_tmp))))
		x_tmp = x_tmp[loc]
		y_tmp = y_tmp[loc]
		p_r = scipy.stats.pearsonr(x_tmp,y_tmp)
# 		s_r = scipy.stats.spearmanr(x_tmp,y_tmp)
		if p_r > max_val:
			max_val = p_r
			print p_r

'''

'''


smoothings1 = np.linspace(2,31,30.0)
smoothings2 = smoothings1.copy()


coeff_det = np.zeros([np.size(smoothings1),np.size(smoothings1)])
coeff_det[:] = np.NAN
coeff_det_2 = coeff_det.copy()


for smoothing1_no,smoothing1 in enumerate(smoothings1):
	print smoothing1_no,' out of ',np.size(smoothings1)
	x_in = bivalve_data_initial.copy()
	x_in = x_in[::-1]
# 	x_in = x_in[0:-5]
	x_in = rm.running_mean(x_in,smoothing1)
	x_in = (x_in-np.nanmin(x_in))
	x_in /= np.nanmax(x_in)
	for smoothing2_no,smoothing2 in enumerate(smoothings2):
		y_in = y.copy()
# 		y_in = y_in[0:-5]
		y_in = rm.running_mean(y_in,smoothing2)
		y_in = (y_in-np.nanmin(y_in))
		y_in /= np.nanmax(y_in)
		loc = np.where(np.logical_not(np.isnan(x_in) | np.isnan(y_in)))
		if np.size(loc) <> 0:
			x_in2 = x_in[loc].copy()
			y_in2 = y_in[loc].copy()
		else:
			x_in2 = x_in.copy()
			y_in2 = y_in.copy()
		#slope, intercept, r_value, p_value, std_err = stats.linregress(x_in2,y_in2)
# 		r_value, p_value = stats.kendalltau(x_in2,y_in2)
		r_value, p_value = stats.spearmanr(x_in2,y_in2)
		#r2 = r_value**2
		#coeff_det_dens_amoc[i,smoothing_no] = r2
		#if p_value <= 0.001:
		r2 = r_value**2
		coeff_det[smoothing1_no,smoothing2_no] = r_value
		if p_value > 0.001:
			r2 = r_value**2
			coeff_det_2[smoothing1_no,smoothing2_no] = r_value




loc1 = np.where(coeff_det == np.max(coeff_det))
smoothing1 = smoothings1[loc1[1]]
smoothing2 = smoothings1[loc1[0]]

smoothing1 = smoothing2 = 20.0

x_in = bivalve_data_initial.copy()
x_in = x_in[::-1]
# 	x_in = x_in[0:-5]
# x_in = gaussian_filter1d(x_in,smoothing1)
x_in = rm.running_mean(x_in,smoothing1)
x_in = (x_in-np.nanmin(x_in))
x_in /= np.nanmax(x_in)

y_in = y.copy()
# 		y_in = y_in[0:-5]
# y_in = gaussian_filter1d(y_in,smoothing2)
y_in = rm.running_mean(y_in,smoothing2)
y_in = (y_in-np.nanmin(y_in))
y_in /= np.nanmax(y_in)

loc = np.where(np.logical_not(np.isnan(x_in) | np.isnan(y_in)))
x_in2 = x_in[loc].copy()
y_in2 = y_in[loc].copy()

r_value, p_value = stats.spearmanr(x_in2,y_in2)

print smoothings1[loc1[0]]
print r_value, p_value

#######################
# Contour plot part
#######################


cmap1 = mpl_cm.get_cmap('RdBu_r')
cmap2 = mpl_cm.get_cmap('RdBu_r')
#gray_r
#z = np.nanmean(coeff_det_dens_amoc,axis = 0)
z = np.fliplr(np.rot90(coeff_det,3))
z2 = np.fliplr(np.rot90(coeff_det_2,3))
zmin = 0.0
zmax = np.max(z)
xlab , ylab = np.meshgrid(smoothings1,smoothings2)

# fig, axarr = plt.subplots(1, 1, (figsize=(8, 4))
plt.rc('legend',**{'fontsize':10})
plt.close('all')
fig = plt.figure()
fig.set_figheight(10)
fig.set_figwidth(10)
# ax1 = plt.subplot2grid((2,1),(1, 0))
#gs = gridspec.GridSpec(2, 1,width_ratios=[1,1],height_ratios =[1,2])
gs = gridspec.GridSpec(100,100,bottom=0.15,left=0.15,right=0.85)

# ax1 = plt.subplot(gs[1])
# ax2 = plt.subplot(gs[0])
#ax2 = plt.subplot(gs[0:44,0:100])
ax1 = plt.subplot(gs[0:100,0:90])
ax3 = plt.subplot(gs[56:100,95:100])


#, rowspan=2)
C = ax1.contourf(xlab,ylab,z,np.linspace(zmin,zmax,100),extend='both',cmap=cmap1)
cb2 = plt.colorbar(C,ax=ax1,cax=ax3)
ax1.contourf(xlab,ylab,z2,np.linspace(zmin,zmax,100),extend='both',hatches=['.'],cmap=cmap2)
CS = ax1.contour(xlab,ylab,z,10,extend='both',colors='k')
ax1.clabel(CS, fontsize=12, inline=1)
ax1.set_xlabel('Bivalve d$^{18}$O smoothing (years)')
ax1.set_ylabel('PMIP3 N. Iceland density smoothing (years)')
cb2.set_label('Variance explained (R$^2$)')

plt.show(block = True)



'''

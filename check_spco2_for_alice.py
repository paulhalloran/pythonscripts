directory = '/data/NAS-ph290/shared/spco2/'

import glob
import matplotlib.pyplot as plt
import iris
import iris.quickplot as qplt
import iris.coord_categorisation

files = glob.glob(directory+'*historical*r1i1p1*2005*')

lon_west = -75.0
lon_east = 5.0
lat_south = 10.0
lat_north = 70.0 

plt.close('all')
co2 = np.genfromtxt('/data/data0/ph290/observations/historical_and_rcp85_atm_co2.txt',skip_header = 2)

for file in files:
	try:
		cube = iris.load_cube(file)
		iris.coord_categorisation.add_year(cube, 'time', name='year')
		cube = cube.aggregated_by('year', iris.analysis.MEAN)
		yr = cube.coord('year').points
		name1 = cube.coord(dimensions = 1).long_name.encode('ascii', 'ignore')
		name2 = cube.coord(dimensions = 2).long_name.encode('ascii', 'ignore')
		try:
        		cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
        		cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
		except:
			try:
				cube_region_tmp = cube.intersection(grid_longitude=(lon_west, lon_east))
        	       		cube_region = cube_region_tmp.intersection(grid_latitude=(lat_south, lat_north))
			except:
				continue
        	try:
        	        cube.coord(name1).guess_bounds()
        	except:
        	        print 'cube already has latitude bounds'
        	try:
        	        cube.coord(name2).guess_bounds()
        	except:
        	        print 'cube already has longitude bounds'
		try:
        		grid_areas = iris.analysis.cartography.area_weights(cube_region)
			area_avged_cube = cube_region.collapsed([name1,name2], iris.analysis.MEAN, weights=grid_areas)
			plt.plot(yr,area_avged_cube.data / 101.325 * 1000.0,label = file.split('/')[-1].split('_')[2])
		except:
			continue
	except:
		continue



plt.plot(co2[:,0],co2[:,1],'k',linewidth = 3)
plt.legend()
plt.xlim([2000,2005])
plt.ylim([350,400])
plt.savefig('/home/ph290/Documents/figures/alice.png')

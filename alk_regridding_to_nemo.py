'''

NOTE! Run this from the working_directory location...

#template_cube = iris.load_cube('/data/NAS-ph290/shared/ford_data/home/daford/for_alice_paul/mi-aj885_spinup/mi-aj885_fco2_1980.nc')

'''


import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
from cdo import *
cdo = Cdo()
import uuid
import subprocess
import glob
import os
import string
import time
import iris.coord_categorisation as cat
import datetime, calendar


print '#####################################################'
print '#####################################################'
print 'before running, pace the following into the shell:'
print 'export HDF5_DISABLE_VERSION_CHECK=1'
print '#####################################################'
print '#####################################################'


print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print '#############################################################################################'
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

print 'Output will end up in the directory you are running this from. MAKE SURE that this matches'
print 'what you have specified as the variable working_directory (not final_output_directory) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print '#############################################################################################'
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

models_in_list = ['CanESM2','MRI-ESM1','GFDL-ESM2M','IPSL-CM5A-LR','IPSL-CM5A-MR','IPSL-CM5B-LR','GFDL-ESM2G','MPI-ESM-LR','MPI-ESM-MR']

###########################
# SET DIRECTORY LOCATOINS!!
###########################
input_directory = '/data/NAS-ph290/shared/CURBCO2/SIMULATIONS/SURFACE_TA/TA_CORRECTED_1x1_NCFILE/'
working_directory = '/data/NAS-ph290/ph290/nemo/output_forcing_files/'
final_output_directory = '/data/NAS-ph290/shared/CURBCO2/SIMULATIONS/SURFACE_TA/TA_CORRECTED_ORCA_GRID_NCFILE/'
###########################
###########################

template_file = '/data/NAS-ph290/shared/CURBCO2/reference/20140101T0000Z_ocn_hadocc_ptrc.grid_T.nc'
temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'
subprocess.call('cp '+template_file+' '+temporary_file_space1+'tmp.nc', shell=True)

for k,model_list in enumerate(models_in_list):
	# k=2
	# model_list =  models_in_list[k]
	subprocess.call('mkdir '+final_output_directory+model_list, shell=True)

	#steps:
	#Extract relevent years
	# merge and select level 1 (sea level)
	# Regrid to 360x180
	# Extract names required for fortran namelist
	# produce the namelists
	# regrid to orca grid
	# interpolate to 6 hourly data ON 365 DAY REAL Calendar
	# combine into single netcdf

	###########################
	# SET MODEL NAME if testing!!!!!!!!!!!
	###########################
	model_name = model_list
	###########################
	###########################


	###########################
	###########################

	# variable = 'Alkalinity'
	# variable_std_name = 'sea_water_alkalinity_expressed_as_mole_equivalent'
	# variables_time_freq = 'day'



	#test to see if output file already exists
	filetest1 = np.size(glob.glob(final_output_directory+model_list+'/*T0000Z_ocn_hadocc_ptrc.grid_T.nc'))
	if filetest1 > 0:
		print variable+' output files already exists'

	print filetest1
	if filetest1 == 0:
		variable_std_name = variable_std_name
		files1 = glob.glob(input_directory+'*'+model_name+'*.nc')
		sample_cube = iris.load_raw(files1[0],variable_std_name)[0]

		########
		#extract required info
		########

		variable_name = sample_cube.var_name.encode('ascii', 'ignore')
		variable_long_name = sample_cube.standard_name.encode('ascii', 'ignore')
		unit_info = str(sample_cube.units)
		time_coordinate_name = sample_cube.coord(dimensions=0).standard_name.encode('ascii', 'ignore')
		timestep_description = str(int((sample_cube.coord(dimensions=0).points[1] - sample_cube.coord(dimensions=0).points[0])))+' days'
		lon_name = sample_cube.coord('longitude').var_name
		lat_name = sample_cube.coord('latitude').var_name


		# 			os.remove(temporary_file_space1+temp_file1)
		#NOTE - the historical and RCP runs for had gem2 seem to have some differences in teh way they have been processed for CMIP5, so treating separately at this stage...
		#cdo.mergetime(input=[temporary_file_space2+temp_file1,temporary_file_space2+temp_file2],output=temporary_file_space1+temp_file2,options='-P 7')

		########
		# Regridding files to NEMO grid
		########


		intermediate_dir_and_file = files1[0]

		cube_to_regrid = iris.load_cube(intermediate_dir_and_file)
		cat.add_year(cube_to_regrid, 'time', name='year')
		cat.add_month_number(cube_to_regrid, 'time', name='month')
		for m in range(np.shape(cube_to_regrid)[0]):
			tmp_cube_0 = cube_to_regrid[m].copy()
			subprocess.call('cdo seltimestep,'+str(m+1)+' '+intermediate_dir_and_file+' '+temporary_file_space1+'tmp0.nc', shell=True)
			year = tmp_cube_0.coord('year').points[0]
			month = tmp_cube_0.coord('month').points[0]

			########
			# create the namelist file to be used in the regridding to the ORCA1 grid
			########

			f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_sample_file','r')
			contents = f.read()
			f.close()

			name_of_file_to_regrid = temporary_file_space1+'tmp0.nc'
			contents = contents.replace("file_to_regrid","'"+name_of_file_to_regrid+"'")
			contents = contents.replace("variable_name","'"+variable_name+"'")
			contents = contents.replace("variable_long_name","'"+variable_long_name+"'")
			contents = contents.replace("unit_info","'"+unit_info+"'")
			contents = contents.replace("time_coordinate_name","'"+time_coordinate_name+"'")
			contents = contents.replace("timestep_description","'"+timestep_description+"'")
			contents = contents.replace("extra_text","'"+model_name+'_'+variable+"'")
			contents = contents.replace("directory_and_info","'"+variable_name+"'")
			contents = contents.replace("cv_lon_in = 'lon'","cv_lon_in ='"+lon_name+"'")
			contents = contents.replace("cv_lat_in = 'lat'","cv_lat_in = '"+lat_name+"'")


			f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_temporary_file','w')
			f.write(contents)
			f.close()

			subprocess.call('/home/ph290/src/sosie-2.6.4/bin/sosie.x -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_temporary_file', shell=True)

			cube_new = iris.load_cube('talk_360x180-ORCA1_'+model_name+'_Alkalinity.nc4')
			cube_temp = iris.load_cube(template_file)
			tmp_data = cube_temp.data
			tmp_data[0,0,:,:] = cube_new.data
			cube_temp.data = tmp_data

			iris.fileformats.netcdf.save(cube_temp, temporary_file_space1+'tmp2.nc', netcdf_format='NETCDF4')
			subprocess.call('cdo replace '+template_file+' '+temporary_file_space1+'tmp2.nc '+temporary_file_space1+'tmp4.nc', shell=True)

			calendar.monthrange(year, month)[1]
			month_string = "%02d"%month

			for day in np.arange(calendar.monthrange(year, month)[1])+1:
				day_string = "%02d"%day
				subprocess.call('cdo setdate,'+str(year)+'-'+str(month)+'-'+str(day)+' '+temporary_file_space1+'tmp4.nc '+final_output_directory+model_list+'/'+str(year)+month_string+day_string+'T0000Z_ocn_hadocc_ptrc.grid_T.nc', shell=True)


			os.remove(temporary_file_space1+'tmp0.nc')
			os.remove(temporary_file_space1+'tmp4.nc')
			os.remove(temporary_file_space1+'tmp2.nc')
			os.remove('./talk_360x180-ORCA1_'+model_name+'_Alkalinity.nc4')



import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid
from cdo import *
cdo = Cdo()
import numpy.ma as ma
import scipy
import gsw


#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive
def model_names_thetao(directory):
	files = glob.glob(directory+'thetao_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models


def model_names_so(directory):
	files = glob.glob(directory+'so_*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
			models = np.unique(models_tmp)
	return models

'''
Defining directory locations, variable locatoins etc.
You may well need to edit the text within the quotation marks to adapt the script to work with your data
NOTE - this presently seems to be giving very shallow MLDs - check.
'''


'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space1 = '/data/data1/ph290/tmp/'
temporary_file_space2 = '/data/data0/ph290/tmp/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/NAS-ph290/ph290/cmip5/past1000/'
#Directories where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/data1/ph290/cmip5/last1000/t_s/'
#comma separated list of the CMIP5 experiments that you want to process (e.g. 'historical','rcp85' etc.). Names must be as they are referencedwritted in the filename as downloaded from CMIP5
experiment = 'past1000'
#specify the temperal averaging period of the data in your files e.g for an ocean file 'Omon' (Ocean monthly) or 'Oyr' (ocean yearly). Atmosphere would be comething like 'Amon'. Note this just prevents probvlems if you accidently did not specify the time frequency when doenloading the data, so avoids trying to puut (e.g.) daily data and monthly data in the same file.
time_period = 'Omon'

#lats and lons for analysis:
lon1 = -180
lon2 = 180
lat1 = 25
lat2 = 90

'''
Main bit of code follows...
'''

models1 = model_names_thetao(input_directory)
models2 = model_names_so(input_directory)
models = list(np.intersect1d(np.array(models1),np.array(models1)))
models.remove('FGOALS-gl')
models = np.array(models)

# models = ['HadGEM2-ES']

#########
# MLD calculation (currently temperature based, but coudl easily change this to density)
#########

chosen_lev = 1

# seasons = ['JJA']
seasons = ['JJA']

for model in models:
# model = models[0]
	ensemble = 'r1i1p1'
	if model == 'GISS-E2-R':
		ensemble = 'r1i1p121'
	print model
	test3 = 0
# 	test1 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_annual_mean.nc'))
# 	test2 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_annual_mean.nc'))
# 	test3 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[0]+'.nc'))
# 	test4 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[0]+'.nc'))
# 	test5 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[1]+'.nc'))
# 	test6 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[1]+'.nc'))
# 	test7 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[2]+'.nc'))
# 	test8 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[2]+'.nc'))
# 	test9 = np.size(glob.glob(output_directory+'mixed_layer_depth_'+model+'_past1000_'+ensemble+'_'+seasons[3]+'.nc'))
# 	test10 = np.size(glob.glob(output_directory+'density_mixed_layer_'+model+'_past1000_'+ensemble+'_'+seasons[3]+'.nc'))
# 	testing = test1 + test2 + test3 + test4 + test5 + test6 + test7 + test8 + test9 + test10
	if test3 == 0:
		#cleaning up...
		# subprocess.call('rm '+temporary_file_space1+'*.nc', shell=True)
		# subprocess.call('rm '+temporary_file_space2+'*.nc', shell=True)
		# subprocess.call('rm '+temporary_file_space3+'*.nc', shell=True)
		temp_file1 = str(uuid.uuid4())+'.nc'
		temp_file2 = str(uuid.uuid4())+'.nc'
		temp_file3 = str(uuid.uuid4())+'.nc'
		temp_file4 = str(uuid.uuid4())+'.nc'
		temp_file5 = str(uuid.uuid4())+'.nc'
		temp_file6 = str(uuid.uuid4())+'.nc'
		files1 = glob.glob(input_directory+'thetao_Omon_'+model+'_past1000_'+ensemble+'_*.nc')
##################################################
# uncomment for testing with just the 1st file   #
##################################################
#		files1 = files1[0:2]
#		files2 = files2[0:2]
##################################################
		#reads in the files to process
		sizing1 = np.size(files1)
		#checks that we have some files to work with for this model, experiment and variable
		if not (sizing1 == 0):
			#######################################
			#             thetao                  #
			#######################################
			print 'reading in: thetao '+model
			tmp = []
			for file in files1:
				tmp.append(int(file.split('_')[-1].split('-')[0]))
			order = np.argsort(tmp)
			files1_joined = ' '.join(np.array(files1)[order])
			levels = np.array(str(cdo.showlevel(input = files1[0])[0]).split(' ')).astype(np.float)
			loc = np.where(levels == chosen_lev)
			levels = levels[loc]
			levels = ','.join(['%.5f' % num for num in levels])
			if sizing1 > 1:
				#if the data is split across more than one file, it is combined into a single file for ease of processing
				#note 1st we need to order the files in the right order, because we can't use mergetime (it does not work with the level selection), and have to use merge - which merges in the right order so long as the files are initially listed in order
				print 'merging thetao files and selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
				#merge together different files1 from the same experiment
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				except:
					pass
				#note that cdo.select both does the select of levels and the merge in one go (note, not sure if this is a merge or mergetime, so should organise files1 in right order until otherwise tested).
				cdo.select('level='+levels+',name=thetao ',input=files1_joined, output = temporary_file_space1+temp_file1,  options = '-P 7')
			if sizing1 == 1:
				print 'No need to merge files1, but selecting levels for '+model+' '+time.strftime("%a, %d %b %Y %H:%M:%S +0000",time.localtime())
				try:
					subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				except:
					pass
				#min_lev = str(cdo.showlevel(input = files1[0])[0]).split(' ')[0]
				cdo.select('level=1,name=thetao',input = files1[0], output = temporary_file_space1+temp_file1)
			new_levs = np.array([float(str(x)) for x in cdo.showlevel(input = temporary_file_space1+temp_file1)[0].split(' ')])
			if model == 'MRI-CGCM3':
				#Note this is added because MRI-CGCM3 uses 0.0 as missing data. Iris does not appear to be able to handle missing values of 0.0, so setting to a different cdo default value
				print 'resetting MRI-CGCM3s missing data value in SO fields'
				subprocess.call('rm '+temporary_file_space2+temp_file6, shell=True)
				cdo.setctomiss('0.0',input = temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file6, options = '-m -9e+33 -P 7')
				subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
				subprocess.call('mv '+temporary_file_space2+temp_file6+' '+temporary_file_space1+temp_file1, shell=True)
			print 'preparing files for density calculation'
			#cdo.setname('to', input = '-subc,273.15 '+ temporary_file_space1+temp_file1, output = temporary_file_space2+temp_file2,  options = '-P 7')
			#cdo.setname('sao', input = temporary_file_space3+temp_file2, output = temporary_file_space2+temp_file4,  options = '-P 7')
			#tmp = ' '.join([temporary_file_space3+temp_file1,temporary_file_space3+temp_file2])
			season = seasons[0]
			cdo.selseas(season, input = '-seasmean -vertmean -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space1+temp_file1, output = output_directory+'thetao_mean_'+model+'_past1000_'+ensemble+'_'+season+'.nc',options = '-P 7')
			cdo.selseas(season, input = '-seasmean -vertmean -sellonlatbox,'+str(lon1)+','+str(lon2)+','+str(lat1)+','+str(lat2)+' '+temporary_file_space2+temp_file2, output = output_directory+'so_mean_'+model+'_past1000_'+ensemble+'_'+season+'.nc',options = '-P 7')
			subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file2, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file2, shell=True)
			subprocess.call('rm '+temporary_file_space1+temp_file1, shell=True)
			subprocess.call('rm '+temporary_file_space2+temp_file3, shell=True)



'''
lon1a = -24
lon2a = -13
lat1a = 65
lat2a = 67

files = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/*.nc')
for i,my_file in enumerate(files):
	print 'processing ',i,' in ',np.size(files)
	name = my_file.split('/')[-1]
	test = glob.glob('/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'+name)
	if np.size(test) == 0:
		cdo.fldmean(input = '-sellonlatbox,'+str(lon1a)+','+str(lon2a)+','+str(lat1a)+','+str(lat2a)+' -remapbil,r360x180 '+my_file, output = '/data/data1/ph290/cmip5/last1000/mld/deep/spatial_avg/'+name,  options = '-P 7')

'''


import numpy as np
import iris
import glob
import subprocess
import uuid
import os

def model_names(directory):
	files = glob.glob(directory+'/*.nc')
	models_tmp = []
	for file in files:
		statinfo = os.stat(file)
		if statinfo.st_size >= 1:
			models_tmp.append(file.split('/')[-1].split('_')[2])
	models = np.unique(models_tmp)
	return models


temporary_file_space = '/data/dataSSD0/ph290/tmp2/'
directory = '/data/data1/ph290/cmip5/last1000/t_s/area_averaged/'

models=model_names(directory)

for model in models:
	try:
		os.remove(temporary_file_space+'delete.nc')
	except:
		print 'no file to delete'
	subprocess.call(['cdo -P 6  merge -subc,273.15 '+directory+'thetao_mean_'+model+'_past1000_r1i1p1_JJA.nc '+directory+'so_mean_'+model+'_past1000_r1i1p1_JJA.nc '+temporary_file_space+'delete.nc'], shell=True)
	subprocess.call(['cdo -P 6 rhopot -adisit '+temporary_file_space+'delete.nc '+directory+'density_mean_'+model+'_past1000_r1i1p1_JJA.nc'], shell=True)

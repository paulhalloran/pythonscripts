import pandas as pd
import matplotlib.pyplot as plt
import datetime
# from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange
import matplotlib
import netCDF4
import running_mean as rm

names =  ['iday','temp_new(N)','temp_new(1)','x_new(N)','s_new(N)','x_new_max','temp_x_new_max','s_new(1)']
# df=pd.read_csv('/data/NAS-geo01/ph290/one_d_simulations/last1000/output_tim',delim_whitespace=True,names = names)
df=pd.read_csv('/data/NAS-geo01/ph290/one_d_simulations/last1000/output_tim_MIROC-ESM_std',delim_whitespace=True,names = names)
df_with_ice=pd.read_csv('/data/NAS-geo01/ph290/one_d_simulations/last1000/output_tim_MIROC-ESM_sea_ice',delim_whitespace=True,names = names)


min_yr = 900
max_yr=1800

# date1 = datetime.date(min_yr, 01, 01)
# date2 = datetime.date(max_yr, 12, 31)
# delta = datetime.timedelta(days=1)
# dates = drange(date1, date2, delta)

dates = netCDF4.num2date(range(len(df)), 'days since 900-01-01', calendar='365_day')
dates2 = [x._to_real_datetime() for x in dates]
dates_ann = netCDF4.num2date(np.arange(max_yr-min_yr)*365, 'days since 900-01-01', calendar='365_day')
dates2_ann = [x._to_real_datetime() for x in dates_ann]

dates_with_ice = netCDF4.num2date(range(len(df_with_ice)), 'days since 900-01-01', calendar='365_day')
dates2_with_ice = [x._to_real_datetime() for x in dates_with_ice]
dates_ann_with_ice = netCDF4.num2date(np.arange(max_yr-min_yr)*365, 'days since 900-01-01', calendar='365_day')
dates2_ann_with_ice = [x._to_real_datetime() for x in dates_ann_with_ice]

# plt.plot(dates2,df['temp_new(N)'],'r')
# plt.plot(dates2,df['temp_new(1)'],'b')
# plt.show()

dict = {}
output = np.zeros(max_yr-min_yr)
# for name in names[1::]:
# names2 = ['temp_new(N)','temp_new(1)']
names2 = ['temp_new(1)','temp_new(N)']
for name in names2:
    print name
    for i,yr in enumerate(range(min_yr,max_yr)):
        # output[i] = df[name][[(x.year == yr) & (x.month >= 6) & (x.month <= 9) for x in dates2]].mean()
        output[i] = df[name][[(x.year == yr) for x in dates2]].mean()
    dict[name] = output.copy()


df_annual = pd.DataFrame(data=dict)

for name in names2:
    print name
    for i,yr in enumerate(range(min_yr,max_yr)):
        # output[i] = df_with_ice[name][[(x.year == yr) & (x.month >= 6) & (x.month <= 9) for x in dates2_with_ice]].mean()
        output[i] = df_with_ice[name][[(x.year == yr) for x in dates2_with_ice]].mean()
    dict[name] = output.copy()


df_annual_with_ice = pd.DataFrame(data=dict)

r_data_file = '/data/NAS-ph290/ph290/misc_data/raw_Iceland_isotope_data.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')
loc = np.where((r_data[:,0] >= min_yr) & (r_data[:,0] <= 1800))[0]
new_dates = [datetime.datetime(int(x),1,1) for x in r_data[loc,0]]

smooth = 10
y1 = rm.running_mean(df_annual['temp_new(N)'],smooth)
y1 -= np.nanmean(y1)
y1 /= np.nanstd(y1)
y3 = rm.running_mean(df_annual['temp_new(1)'],smooth)
y3 -= np.nanmean(y3)
y3 /= np.nanstd(y3)

y1b = rm.running_mean(df_annual_with_ice['temp_new(N)'],smooth)
y1b -= np.nanmean(y1b)
y1b /= np.nanstd(y1b)
y3b = rm.running_mean(df_annual_with_ice['temp_new(1)'],smooth)
y3b -= np.nanmean(y3b)
y3b /= np.nanstd(y3b)

y2 = rm.running_mean(r_data[loc,2],smooth)
y2 -= np.nanmean(y2)
y2 /= np.nanstd(y2)

# plt.plot(dates2_ann,df_annual['temp_new(N)']-np.mean(df_annual['temp_new(N)']),'r')
plt.plot(dates2_ann,y1,'r',alpha=0.5)
plt.plot(dates2_ann,y3,'b',alpha=0.5)
plt.plot(dates2_ann_with_ice,y1b,'r--')
plt.plot(dates2_ann_with_ice,y3b,'b--')
plt.plot(new_dates,y2* -1.0,'k')
plt.show()

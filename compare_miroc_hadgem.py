# HadGEM2-ES/HADGEM2-ES_huss_rcp85_all.nc
# -rw-r----- 1 jm953 domainusers 3.9G Jan  4 09:33 HadGEM2-ES/HADGEM2-ES_psl_rcp85_all.nc
# -rw-r----- 1 jm953 domainusers 3.9G Jan  4 09:34 HadGEM2-ES/HADGEM2-ES_tas_rcp85_all.nc
# -rw-r----- 1 jm953 domainusers 3.5G Jan  4 09:35 HadGEM2-ES/HADGEM2-ES_uas_rcp85_all.nc
# -rw-r----- 1 jm953 domainusers 3.5G Jan  4 09:36 HadGEM2-ES/HADGEM2-ES_vas_rcp85_all.nc
# -rw-r----- 1 jm953 domainusers 3.6G Mar 25 11:41 HadGEM2-ES/HADGEM2-ES_clt_rcp85_all.nc

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.plot as iplt
import numpy as np

def extract_region(cube):
    lon_west = 140
    lon_east = 160
    lat_south = -35
    lat_north = -7.0
    lon_west = -180
    lon_east = 180
    lat_south = -90
    lat_north = 90
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
    return cube_region

vars = ['huss','psl','tas','uas','vas','clt']

plt.close('all')
for i,v in enumerate(vars):
    m='MIROC5'
    m2='HadGEM2-ES'
    file = m+'/'+m+'_'+v+'_rcp85_all.nc'
    file2 = m2+'/HADGEM2-ES_'+v+'_rcp85_all.nc'
    c = iris.load_cube(file)[0:365]
    c = c.collapsed('time',iris.analysis.MEAN)
    c2= iris.load_cube(file2)[0:365]
    c2 = c2.collapsed('time',iris.analysis.MEAN)
    ax = plt.subplot2grid((7, 2), (i, 0))
    iplt.pcolormesh(extract_region(c))
    plt.colorbar()
    plt.gca().coastlines()
    ax.set_title(m+' '+v)
    ax = plt.subplot2grid((7, 2), (i, 1))
    iplt.pcolormesh(extract_region(c2))
    plt.gca().coastlines()
    plt.colorbar()
    ax.set_title(m2+' '+v)



def ws_data_func(u_data, v_data):
    return np.sqrt( u_data**2 + v_data**2 )

def ws_units_func(u_cube, v_cube):
    if u_cube.units != getattr(v_cube, 'units', u_cube.units):
        raise ValueError("units do not match")
    return u_cube.units

ws_ifunc = iris.analysis.maths.IFunc(ws_data_func, ws_units_func)

m='MIROC5'
file = m+'/'+m+'_uas_rcp85_all.nc'
cu = iris.load_cube(file)[0:365]
file = m+'/'+m+'_vas_rcp85_all.nc'
cv = iris.load_cube(file)[0:365]
wind_speed_cube1 = ws_ifunc(cu, cv, new_name='wind speed')
wind_speed_cube1 = wind_speed_cube1.collapsed('time',iris.analysis.MEAN)

m='HadGEM2-ES'
file = 'HadGEM2-ES/HADGEM2-ES_uas_rcp85_all.nc'
cu = iris.load_cube(file)[0:365]
file = 'HadGEM2-ES/HADGEM2-ES_vas_rcp85_all.nc'

cv = iris.load_cube(file)[0:365]
wind_speed_cube2 = ws_ifunc(cu, cv, new_name='wind speed')
wind_speed_cube2 = wind_speed_cube2.collapsed('time',iris.analysis.MEAN)

ax = plt.subplot2grid((7, 2), (i, 0))
iplt.pcolormesh(extract_region(wind_speed_cube1))
plt.colorbar()
plt.gca().coastlines()
ax.set_title(m+' '+v)

ax = plt.subplot2grid((7, 2), (i, 1))
iplt.pcolormesh(extract_region(wind_speed_cube2))
plt.colorbar()
plt.gca().coastlines()
ax.set_title(m2+' '+v)

plt.show()





---------











import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
import iris.plot as iplt

def extract_region(cube):
    lon_west = 140
    lon_east = 160
    lat_south = -35
    lat_north = -7.0
    cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
    cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))
    return cube_region

vars = ['uas']

plt.close('all')

m='MIROC5'
file = 'ua_day_MIROC5_rcp85_r1i1p1_20060101-20091231.nc'
c = iris.load_cube(file)[0:365]
c = c.collapsed('time',iris.analysis.MEAN)
ax = plt.subplot2grid((1, 2), (i, 0))
iplt.pcolormesh(extract_region(c))
plt.colorbar()
plt.gca().coastlines
ax.set_title(m+' '+v)


plt.show()

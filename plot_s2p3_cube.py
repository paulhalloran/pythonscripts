import pandas as pd
import iris
import numpy as np
import datetime
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import netCDF4
import iris.coord_categorisation
import iris.analysis

lat =     54.95
lon =    4.92

UK_Marsh_grid_path = '/data/NAS-ph290/ph290/s2p3_rv2.0_output/'

# file = UK_Marsh_grid_path + 'bottom_pyto_biomass_oyster_grounds.nc'
file = UK_Marsh_grid_path + 'bottom_temperature_oyster_grounds.nc'
file = UK_Marsh_grid_path + 'surface_temperature_oyster_grounds.nc'

cube = iris.load_cube(file)
iris.coord_categorisation.add_year(cube, 'time', name='year')
cube = cube.aggregated_by('year', iris.analysis.MEAN)

index_lat = cube.coord('latitude').nearest_neighbour_index(lat)
index_lon = cube.coord('longitude').nearest_neighbour_index(lon)

e1_Marsh = cube[:,index_lat, index_lon].data
#temperature difference values from the Marsh model at the mooring location

e1_Marsh_dates = netCDF4.num2date(cube.coord('time').points,cube.coord('time').units.origin,cube.coord('time').units.calendar)
#reformat the time dimension so it matches the csv file format

#####my_plot
plt.close("all")
plt.plot(e1_Marsh_dates, e1_Marsh, alpha = .5, label = "Model")
plt.title("E1 Mooring and Model Validation")
plt.legend()
plt.show(block = False)



#
# e1_Marsh_dates_ordinal = [i.toordinal() for i in e1_Marsh_dates]
# marsh_value_for_date = [np.interp(x.toordinal(),e1_Marsh_dates_ordinal,e1_Marsh) for x in data['Date'].dropna()]
#
# plt.close("all")
# plt.scatter(data['Date'].values, data['surf_bottom_diff'].values,color='red', alpha = .5, label = "Observations")
# plt.plot(e1_Marsh_dates, e1_Marsh, alpha = .5, label = "Model")
# #plt.scatter(data['Date'].values, marsh_value_for_date, color='blue',alpha = .5, label = "Model")
# plt.title("E1 Mooring and Model Validation")
# plt.legend()
# # plt.xlim([datetime.date(2002, 1, 1), datetime.date(2016, 1, 1)])
# plt.show(block = False)

import pandas as pd
import datetime
import matplotlib.pyplot as plt

#Read the data in (I first imported this in libreoffice (or excel) and saved as a comma separated file (with commas separating coluns correctly))
df = pd.read_csv('/data/NAS-geo01/ph290/observations/wco_2002-2013_ctd_profiles.csv',header = 83)

#this section loops through from the top to the bottom and fills empty rows in the 'station' column with the prreviois station number we had (a bit like fill down in excel)
tmp = df['Station'].values

is_it_nan = df['Station'].notna()

site = ''

for i,t in enumerate(tmp):
    if is_it_nan.iloc[i]:
        site = tmp[i]
    else:
        tmp[i] = site


#This pus the completed station column back into our dataset
df['Station'] = tmp

#remove rows containing the text 'history' in the Cruise column
df = df[df['Cruise'].str.contains("History") == False]

#convert the dates in the date columns into a format that python understands
dates_list = [datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f").date() for date in df['yyyy-mm-ddThh:mm:ss.sss']]
df['datetime'] = dates_list
df = df.sort_values('datetime')

#pull out all of the rows where the 'station' column contains the text E1 so we have a subset of teh data that is just the e1 data
e1 = df[df['Station'].str.contains("E1") == True]

#as above but fpr L4
l4 = df[df['Station'].str.contains("L4") == True]

#exclude roes that contain nan values in the WC_temp_CTD [degrees C] column
e1_no_nans = e1[e1['WC_temp_CTD [degrees C]'].notna()]

#plot the E1 data
plt.plot(e1_no_nans['datetime'],e1_no_nans['WC_temp_CTD [degrees C]'])
plt.show()

#remove nans and plot the l4 data
l4_no_nans = l4[l4['WC_temp_CTD [degrees C]'].notna()]

plt.plot(l4_no_nans['datetime'],l4_no_nans['WC_temp_CTD [degrees C]'])
plt.show()

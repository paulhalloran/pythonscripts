'''
This script processes ocean data (and I think it should work with at least surface level atmospheric data) from teh CMIP5 archive to put it all on the same grid (same horizontal and vertical resolution (nominally 360x180 i.e. 1 degree horizontal, and every 10m in the shallow ocean, and every 500m in the deep ocean - see later if you want to adjust this)). It also convertsaverages monthly data to annual data - again, you could change this.
'''

#First we need to import the modules required to do the analysis
import time
import numpy as np
import glob
import iris
import iris.coord_categorisation
import iris.analysis
import subprocess
import os
import uuid


#this is a simple function that we call later to look at the file names and extarct from them a unique list of models to process
#note that the model name is in the filename when downlaode ddirectly from the CMIP5 archive

def ensemble_names(directory):
        files = glob.glob(directory+'/*.nc')
        ensembles_tmp = []
        for file in files:
                statinfo = os.stat(file)
                if statinfo.st_size >= 1:
                        ensembles_tmp.append(file.split('/')[-1].split('.')[4])
                        ensemble = np.unique(ensembles_tmp)
        return ensemble


'''
EDIT THE FOLLOWING TEXT
'''
#the lcoation of some temporart disk space that can be used for the processing. You'll want to point to an area with plenty of free space (Many Gbs)
temporary_file_space = '/data/dataSSD0/ph290/tmp2/'
#Directory containing the datasets you want to process onto a simply grid
input_directory = '/data/NAS-geo01/ph290/CESM1/'
#Directory where you want to put the processed data. Make sure you have the correct file permissions to write here (e.g. test hat you can make a text file there). Also make sure that you have enough space to save the files (the saved files will probably be of a similar size to what they were before processing).
output_directory = '/data/NAS-geo01/ph290/CESM1/merged/'
#comma separated list of the CMIP5 variables that you want to process (e.g. 'tos','fgco2' etc.)
variables = np.array(['TEMP','N_SALT'])
model = 'CESM1'
experiment = 'past1000'

past1000_r1i1p1_regridded.nc

directory = input_directory

ensembles = ensemble_names(input_directory)


#These lines (and similar later on) just create unique random filenames to be used as temporary filenames during the processing
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'
temp_file3 = str(uuid.uuid4())+'.nc'
temp_file4 = str(uuid.uuid4())+'.nc'


#Looping through each model we have
for variable in variables:
	for ensemble in ensembles:
		print ensemble
		files = glob.glob(input_directory+'*.'+ensemble+'.*.'+variable+'.*'.nc')
		tmp = glob.glob(output_directory+model+'_'+variable+'_'+experiment+'*'+ensemble+'*'+'.nc')
		temp_file1 = str(uuid.uuid4())+'.nc'
		temp_file2 = str(uuid.uuid4())+'.nc'
		temp_file3 = str(uuid.uuid4())+'.nc'
		temp_file4 = str(uuid.uuid4())+'.nc'
		if np.size(tmp) == 0:
			#reads in the files to process
			print 'reading in: '+var+'_'+time_period+'_'+model
			files = glob.glob(dir1+'/'+var+'*'+time_period+'*_'+model+'_*'+experiment+'*'+ensemble+'*.nc')
			sizing = np.size(files)
			#checks that we have some files to work with for this model, experiment and variable
			if not sizing == 0:
				if sizing > 1:
					#if the data is split across more than one file, it is combined into a single file for ease of processing
					files = ' '.join(files)
					print 'merging files'
					#merge together different files from the same experiment
					subprocess.call(['cdo -P 8 mergetime '+files+' '+temporary_file_space+temp_file1], shell=True)
				if sizing == 1:
					print 'no need to merge - only one file present'
					subprocess.call(['cp '+files[0]+' '+temporary_file_space+temp_file1], shell=True)
				#print 'time-meaning data (to annual means if required)'
				#subprocess.call('cdo -P 8  yearmean '+temporary_file_space+temp_file1+' '+temporary_file_space+temp_file2, shell=True)
				#subprocess.call('rm '+temporary_file_space+temp_file1, shell=True)
				#print 'interpolating latitudes'
				subprocess.call(['cdo -P 8 remapbil,r360x180 -yearmean '+temporary_file_space+temp_file1+' '+dir2+model+'_'+variables[i]+'_'+experiment+'_'+ensemble+'_regridded_not_vertically.nc'], shell=True)
				#print 'regridding files vertically'
				#then regrid data onto a 360x180 grid - you coudl change these values if you wanted to work with different resolurtoin data (lower resolution would make smaller files that would be quicker to work with)
				#subprocess.call(['cdo intlevel,6000,5000,4000,3000,2000,1500,1000,500,400,300,200,100,80,60,40,20,10,0 '+temporary_file_space+temp_file1+' '+dir2+model+'_'+variables[i]+'_'+experiment+'_'+ensemble+'_regridded_not_vertically.nc'], shell=True)
				subprocess.call('rm '+temporary_file_space+temp_file1, shell=True)
                                        #subprocess.call('rm '+temporary_file_space+temp_file2, shell=True)

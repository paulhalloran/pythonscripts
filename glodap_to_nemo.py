'''

execfile('/home/ph290/Documents/python_scripts/glodap_to_nemo.py')

NOTE! Run this from the working_directory location...


'''


import iris
import iris.quickplot as qplt
import numpy as np
import matplotlib.pyplot as plt
from cdo import *
cdo = Cdo()
import uuid
import subprocess
import glob
import os
import string
import time

print '#####################################################'
print '#####################################################'
print 'before running, pace the following into the shell:'
print 'export HDF5_DISABLE_VERSION_CHECK=1'
print '#####################################################'
print '#####################################################'


print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print '#############################################################################################'
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'

print 'Output will end up in the directory you are running this from. MAKE SURE that this matches'
print 'what you have specified as the variable working_directory (not final_output_directory) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
print '#############################################################################################'
print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'



temporary_file_space1 = '/data/dataSSD1/ph290/tmp/'
temporary_file_space2 = '/data/dataSSD0/ph290/tmp/'
temp_file1 = str(uuid.uuid4())+'.nc'
temp_file2 = str(uuid.uuid4())+'.nc'

files = ['/data/NAS-ph290/shared/CURBCO2/SIMULATIONS/SURFACE_TA/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TAlk.nc','/data/NAS-ph290/shared/CURBCO2/SIMULATIONS/SURFACE_TA/GLODAPv2.2016b_MappedClimatologies/GLODAPv2.2016b.TCO2.nc']

mesh_cube = iris.load('/home/ph290/src/sosie-2.6.4/examples/data/mesh_mask_ORCA1_light.nc')
t_cube = iris.load('/data/NAS-ph290/ph290/observations/GLODAPv2_Mapped_Climatologies/woa99_temperature_annual_1deg.nc')
# nemo_cube = iris.load('/data/NAS-ph290/ph290/from_monsoon/restart_trc.nc')

variable_std_names = ['seawater alkalinity expressed as mole equivalent per unit mass','moles of dissolved inorganic carbon per unit mass in seawater']
vs=['Alkalini','DIC']

for i,variable_std_name in enumerate(variable_std_names):
	template_cube = iris.load_cube('/data/NAS-ph290/shared/curbco2/'+vs[i]+'_restart.nc')
	sample_cube = iris.load_raw(files[i],variable_std_name)[0]
	data = np.roll(sample_cube.data.copy(),20,axis=2) # glodap does not start its longs at 0 degs rather at 20 deg
	sample_cube.data = data *1.027 # multiplication to get it in mmol m-3 for Nemo hadocc 
	cube = sample_cube

	depth = iris.coords.DimCoord(t_cube[0].coord('depth').points, standard_name='depth', units='meters')
	time = iris.coords.DimCoord(range(0, 1, 1), standard_name='time', units='seconds')
	latitude = iris.coords.DimCoord(range(-90, 90, 1), standard_name='latitude', units='degrees')
	longitude = iris.coords.DimCoord(range(0, 360, 1), standard_name='longitude', units='degrees')
	if i==0:
		new_cube = iris.cube.Cube(np.ma.masked_array(np.zeros((1,33,180, 360), np.float32)),standard_name='sea_water_alkalinity_expressed_as_mole_equivalent', long_name='Alkalinity', var_name='Alkalini', units='mol m-3',dim_coords_and_dims=[(time,0), (depth,1), (latitude, 2), (longitude, 3)])
	if i==1:
		new_cube = iris.cube.Cube(np.ma.masked_array(np.zeros((1,33,180, 360), np.float32)),standard_name='mole_concentration_of_dissolved_inorganic_carbon_in_sea_water', long_name='Dissolved Inorganic Carbon', var_name='DIC', units='mol m-3',dim_coords_and_dims=[(time,0), (depth,1), (latitude, 2), (longitude, 3)])

	new_cube.data[0] = np.ma.masked_array(cube.data)
	iris.fileformats.netcdf.save(new_cube,temporary_file_space1+temp_file1)

	'''
	Dissolved Inorganic Carbon
	DIC
	'''
	########
	#extract required info
	########

	file_to_regrid = temporary_file_space1+temp_file1
	sample_cube = iris.load_cube(file_to_regrid)


	#variable_name = sample_cube.var_name.encode('ascii', 'ignore')
	variable_name = sample_cube.var_name.encode('ascii', 'ignore')
	variable_long_name = sample_cube.standard_name.encode('ascii', 'ignore')
	# variable_long_name = 'seawater alkalinity expressed as mole equivalent per unit mass'
	unit_info = str(sample_cube.units)
	time_coordinate_name = sample_cube.coord(dimensions=0).standard_name.encode('ascii', 'ignore')
	# time_coordinate_name ='time'
	# timestep_description = str(int((sample_cube.coord(dimensions=0).points[1] - sample_cube.coord(dimensions=0).points[0])))+' days'
	timestep_description='0 days'
	lon_name = sample_cube.coord(dimensions=3).var_name
	lat_name = sample_cube.coord(dimensions=2).var_name

	#for all other variables
	# f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_sample_file_3d','r')
	f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.3D_temperature_WOA2009_to_ORCA1.L46','r')
	contents = f.read()
	f.close()


	contents = contents.replace("file_to_regrid","'"+file_to_regrid+"'")
	contents = contents.replace("variable_name","'"+variable_name+"'")
	contents = contents.replace("variable_long_name","'"+variable_long_name+"'")
	contents = contents.replace("unit_info","'"+unit_info+"'")
	contents = contents.replace("time_coordinate_name","'"+time_coordinate_name+"'")
	contents = contents.replace("timestep_description","'"+timestep_description+"'")
	contents = contents.replace("extra_text","'glodapv2'")
	contents = contents.replace("directory_and_info","'"+variable_name+"'")
	contents = contents.replace("cv_lon_in = 'lon'","cv_lon_in = '"+lon_name+"'")
	contents = contents.replace("cv_lat_in = 'lat'","cv_lat_in = '"+lat_name+"'")
	contents = contents.replace("jplev     = 1","jplev     = 0") # makes it do 3d regriddiong
	contents = contents.replace("cf_z_in  = 'data/T_levitus_march.nc'","cf_z_in  = '"+temporary_file_space1+temp_file1+"'")
	contents = contents.replace("cv_z_in  = 'level'","cv_z_in  = 'depth'")
	contents = contents.replace("cf_z_out = 'data/mesh_mask_ORCA1_light.nc'","cf_z_out = '/data/NAS-ph290/ph290/from_monsoon/restart_trc.nc'")
	contents = contents.replace("cv_z_out_name = 'depth'","cv_z_out_name = 'z'")

	f = open('/home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_temporary_file2','w')
	f.write(contents)
	f.close()

	subprocess.call('/home/ph290/src/sosie-2.6.4/bin/sosie.x -f /home/ph290/Documents/python_scripts/SOSIE/namelist.360x180_2_ORCA1_temporary_file2', shell=True)

	#getting data in just the right format
	tc = iris.load_cube(vs[i]+'_1degx1deg-ORCA1.L75_glodapv2.nc')
	data = tc.data.copy()
	data[np.where(template_cube.data == 0.0)]=0.0
	tc.data=data
	os.remove(vs[i]+'_1degx1deg-ORCA1.L75_glodapv2.nc')
	template_cube.data = tc.data
	iris.fileformats.netcdf.save(template_cube,vs[i]+'_1degx1deg-ORCA1.L75_glodapv2.nc')

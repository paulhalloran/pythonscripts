

import numpy as np
import matplotlib.pyplot as plt
import running_mean as rm
import running_mean_post as rmp
import scipy
import scipy.signal
import scipy.stats as stats
import matplotlib.gridspec as gridspec
import pickle
import os
from timer import Timer
import pandas

start_year = 953
end_year = 1849

window_type = 'boxcar'

amo_file = '/data/NAS-geo01/ph290/misc_data/mann_2009/amoall.txt'
amo = np.genfromtxt(amo_file, skip_header = 2)

r_data_file = '/data/NAS-geo01/ph290/misc_data/ultra_data_may_2014.csv'
r_data = np.genfromtxt(r_data_file,skip_header = 1,delimiter = ',')


#############
# Mann AMO
#############
amo_yr = amo[:,0].copy()
amo_data = amo[:,1].copy()
# 	amo_yr = amo_yr[6::]
# 	amo_data = amo_data[6::]
loc = np.where((amo_yr >= start_year) & (amo_yr <= end_year))
amo_yr = amo_yr[loc]
amo_data = amo_data[loc]
# amo_data = scipy.signal.detrend(amo_data, axis=0)
amo_data -= np.min(amo_data)
amo_data /= np.max(amo_data)


#############
# Bivalve
#############
smoothing = 10
shift1 = 15
bivalve_yr = r_data[:,0].copy()
bivalve_data = r_data[:,1].copy()
bivalve_yr = bivalve_yr[::-1]
bivalve_data = bivalve_data[::-1]
# 	bivalve_data = np.roll(bivalve_data,10)
loc = np.where((bivalve_yr >= start_year) & (bivalve_yr <= end_year))
bivalve_yr = bivalve_yr[loc]
bivalve_data = bivalve_data[loc]
bivalve_data = np.roll(bivalve_data,shift1)
bivalve_data[0:shift1] = np.nan
bivalve_data[-1.0*shift1::] = np.nan
#bivalve_data = scipy.signal.detrend(bivalve_data, axis=0)
bivalve_data = pandas.rolling_window(bivalve_data,smoothing,win_type=window_type,center=True)
bivalve_data -= np.nanmin(bivalve_data)
bivalve_data /= np.nanmax(bivalve_data)

############
# contour plot data
############

smoothings = np.linspace(1,30,30)

max_shift = 40
no_shifts = 40
corr_coeff = np.zeros([no_shifts,np.size(smoothings)])
corr_coeff[:] = np.NAN
corr_coeff2 = corr_coeff.copy()

amo_yr2 = amo[:,0].copy()
amo_data2 = amo[:,1].copy()
# 	amo_yr = amo_yr[6::]
# 	amo_data = amo_data[6::]
loc = np.where((amo_yr2 >= start_year) & (amo_yr2 <= end_year))
amo_yr2 = amo_yr2[loc]
amo_data2 = amo_data2[loc]
# amo_data = scipy.signal.detrend(amo_data, axis=0)

bivalve_yr2 = r_data[:,0].copy()
bivalve_data2 = r_data[:,1].copy()
bivalve_yr2 = bivalve_yr2[::-1]
bivalve_data2 = bivalve_data2[::-1]
# 	bivalve_data = np.roll(bivalve_data,10)
loc = np.where((bivalve_yr2 >= start_year) & (bivalve_yr2 <= end_year))
bivalve_yr2 = bivalve_yr2[loc]
bivalve_data2 = bivalve_data2[loc]


for smoothing_no,smoothing in enumerate(smoothings):
	smoothing = np.int(smoothing)
	print smoothing_no,' out of ',np.size(smoothings)
	#AMO data
	amo_yr2 = amo[:,0].copy()
	amo_data2 = amo[:,1].copy()
	loc = np.where((amo_yr2 >= start_year) & (amo_yr2 <= end_year))
	amo_yr2 = amo_yr2[loc]
	amo_data2 = amo_data2[loc]
	# amo_data = scipy.signal.detrend(amo_data, axis=0)
	#Bivalve data
	bivalve_yr2 = r_data[:,0].copy()
	bivalve_data2 = r_data[:,1].copy()
	bivalve_yr2 = bivalve_yr2[::-1]
	bivalve_data2 = bivalve_data2[::-1]
	loc = np.where((bivalve_yr2 >= start_year) & (bivalve_yr2 <= end_year))
	bivalve_yr2 = bivalve_yr2[loc]
	bivalve_data2 = bivalve_data2[loc]
	#analysis
	#variable holding analysis years
	window_type = 'boxcar'
	for i,shift in enumerate(np.round(np.linspace(0,max_shift,no_shifts))):
		if shift == 0:
			x = pandas.rolling_window(bivalve_data2[0::],smoothing,win_type=window_type,center=True)
		else:
			x = pandas.rolling_window(bivalve_data2[0:-1*shift],smoothing,win_type=window_type,center=True)
		y = pandas.rolling_window(amo_data2[shift::],smoothing,win_type=window_type,center=True)
		loc = np.where(np.logical_not(np.isnan(x) | np.isnan(y)))
		x = x[loc]
		y = y[loc]
		x = scipy.signal.detrend(x, axis=0)
		y = scipy.signal.detrend(y, axis=0)
		x = x - np.min(x)
		x = x / (np.max(x))
		y = y - np.min(y)
		y = y / (np.max(y))
# 		slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
		r_value = scipy.stats.spearmanr(x,y)[0]
		#r2 = r_value**2
		#coeff_det_dens_amoc[i,smoothing_no] = r2
		#if p_value <= 0.001:
		r2 = r_value**2
		corr_coeff[i,smoothing_no] = r_value
			
##########

loc2 = np.where(np.logical_not((np.isnan(amo_data)) | (np.isnan(bivalve_data))))

amo_yr = amo_yr[loc2]
amo_data = amo_data[loc2]
bivalve_yr = bivalve_yr[loc2]
bivalve_data = bivalve_data[loc2]

amo_data = scipy.signal.detrend(amo_data, axis=0)
bivalve_data = scipy.signal.detrend(bivalve_data, axis=0)



r_values, p_value = stats.spearmanr(amo_data,bivalve_data)
#
r_valuep, p_value2 = stats.pearsonr(amo_data,bivalve_data)

print 's r-value ',r_values
print 'p r-value ',r_valuep


#############
# Plotting
#############



plt.close('all')
plt.rc('legend',**{'fontsize':8})
fig = plt.figure()
fig.set_figheight(4)
fig.set_figwidth(10)

#gs = gridspec.GridSpec(100,100,bottom=0.05,left=0.15,right=0.85)
#ax1 = plt.subplot(gs[0:30,0:100])
#ax2 = plt.subplot(gs[35:65:100,0:100])
#ax3 = plt.subplot(gs[70:100,0:100])

gs = gridspec.GridSpec(100,100,bottom=0.2,left=0.1,right=0.95)
ax1 = plt.subplot(gs[0:100,0:60])
ax2 = plt.subplot(gs[0:100,70:100])



ax1.plot(amo_yr,amo_data,'b',lw = 2,label = 'Reconstructed AMV index')
ax1.plot(amo_yr,bivalve_data,'r',lw = 2,label = 'Bivalve $\delta^{18}$O, offset by +'+str(shift1)+' years')
# ax1.annotate('r-value = '+np.str(np.around(r_values ,2))+', p-value < 0.001', xy=(.025, .95), xycoords='axes fraction',horizontalalignment='left', verticalalignment='center')

ax1.legend(loc = 3)

# plt.tight_layout()
ax1.set_xlim([950,1850])
ax1.set_xlabel('Calendar Year')
ax1.set_ylabel('Normalised anomaly')

i=7
for i in [5,7,9,11,13]:
	ax2.plot(corr_coeff[:,i],label = str(smoothings[i])+' year running mean')


ax2.set_xlabel('Bivalve $\delta^{18}$O offset (years)')
ax2.set_ylabel('Correlation coefficient')
ax2.set_xlim([1,40])
leg = ax2.legend(loc=3, fancybox=True)
leg.get_frame().set_alpha(0.3)

plt.savefig('/home/ph290/Documents/figures/forced_AMO_190116.png')
plt.show(block = False)

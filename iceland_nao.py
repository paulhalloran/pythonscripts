#whatever

# cdo mergetime /data/NAS-ph290/ph290/s2p3_rv2.0_output/surface_temperature_iceland_site_????.nc /data/NAS-ph290/ph290/s2p3_rv2.0_output/surface_temperature_iceland_site.nc
# cdo mergetime /data/NAS-ph290/ph290/s2p3_rv2.0_output/bottom_temperature_iceland_site_????.nc '/data/NAS-ph290/ph290/s2p3_rv2.0_output/bottom_temperature_iceland_site.nc'


import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import glob
import subprocess
import datetime
import iris.coord_categorisation
import pandas as pd
import scipy
from scipy import signal
import time

# time.sleep(60.0*60.0*3.0)

def return_datetime_from_cube(cube):
    #Add year month and day number for each record in the cube
    try:
        iris.coord_categorisation.add_year(cube, 'time', name='year')
    except:
        print 'already have'
    try:
        iris.coord_categorisation.add_month_number(cube, 'time', name='month')
    except:
        print 'already have'
    try:
        iris.coord_categorisation.add_day_of_month(cube, 'time', name='day')
    except:
        print 'already have'
    #Get the dates in 'datetime' format so it will be easy to plot against observations (see https://pymotw.com/2/datetime/)
    dates=[]
    for i in range(np.shape(cube)[0]):
        try:
            dates.append(datetime.datetime(cube.coord('year').points[i], cube.coord('month').points[i], cube.coord('day').points[i]))
        except:
            dates.append(datetime.datetime(cube.coord('year').points[i], 1, 1))
    return dates


def detrend_cube(cube):
    cube2 = cube.collapsed('time',iris.analysis.MEAN)
    cube_data = cube.data
    cube_data_detrended = scipy.signal.detrend(cube_data, axis=0)
    cube.data = cube_data_detrended
    return cube+cube2

def read_model_data(lat,lon,file):
    #load the files
    cube1 = iris.load_cube(file)[spinup_years::]
    #Add year month and day number for each record in the cube
    iris.coord_categorisation.add_year(cube1, 'time', name='year')
    iris.coord_categorisation.add_month_number(cube1, 'time', name='month')
    iris.coord_categorisation.add_day_of_month(cube1, 'time', name='day')
    #Get the dates in 'datetime' format so it will be easy to plot against observations (see https://pymotw.com/2/datetime/)
    dates=[]
    for i in range(np.shape(cube1)[0]):
        dates.append(datetime.datetime(cube1.coord('year').points[i], cube1.coord('month').points[i], cube1.coord('day').points[i]))
    #find the indexs we will need to use to pull out data from teh cubes for specific latitudes and longitudes
    index_lat = cube1.coord('latitude').nearest_neighbour_index(lat)
    index_lon = cube1.coord('longitude').nearest_neighbour_index(lon)
    return dates,cube1[:,index_lat,index_lon]


#The lat/lon I'm interested in converted from degrees and minutes to devimal degrees, and degrees W to negative degrees E
#Scotland
# lat = 56.0+(37.75/60.0)
# lon = -1.0 * (6.0+(24.00/60.0))
#iceland
lat = 66.0+(31.59/60.0)
lon = -1.0 * (18.0+(11.74/60.0))

######################### climatological years #################

# file1 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/scotland_surface_temperature.nc')
# file2 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/scotland_bottom_temperature.nc')
# file3 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/scotland_chlorophyll.nc')
file1 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/surface_temperature_iceland_site.nc')
file2 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/bottom_temperature_iceland_site.nc')
file3 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/surface_chlorophyll_iceland_site.nc')
# file3 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/bottom_pyto_biomass_tiree_0point02.nc')

dates_my_latlon,s_cube_my_latlon = read_model_data(lat,lon,file1)
dates_my_latlon,b_cube_my_latlon = read_model_data(lat,lon,file2)
dates_my_latlon,chl_cube_my_latlon = read_model_data(lat,lon,file3)

import iris.coord_categorisation
import iris.analysis

iris.coord_categorisation.add_day_of_year(s_cube_my_latlon, 'time', name='clim_day')
s_cube_my_latlon_annual_climatology = s_cube_my_latlon.aggregated_by('clim_day', iris.analysis.MEAN)
iris.coord_categorisation.add_day_of_year(b_cube_my_latlon, 'time', name='clim_day')
b_cube_my_latlon_annual_climatology = b_cube_my_latlon.aggregated_by('clim_day', iris.analysis.MEAN)
iris.coord_categorisation.add_day_of_year(chl_cube_my_latlon, 'time', name='clim_day')
chl_cube_my_latlon_annual_climatology = chl_cube_my_latlon.aggregated_by('clim_day', iris.analysis.MEAN)

qplt.plot((s_cube_my_latlon_annual_climatology-b_cube_my_latlon_annual_climatology),label='climatological surface/bottom T diff')
qplt.plot(chl_cube_my_latlon_annual_climatology,label='climatological surface chlorophyll')
plt.legend()
plt.xlabel('day of year')
plt.savefig('/home/ph290/Documents/figures/climatological_t_v_chl_iceland.png')
plt.show()

###

dates = return_datetime_from_cube(s_cube_my_latlon)

years = [tmp.year for tmp in dates]

direct = '/data/NAS-ph290/ph290/observations/'
nao_file = direct+'nao_station_djfm.txt'
# nao_file = direct+'nao_station_annual.txt'
nao = pd.read_csv(nao_file,delim_whitespace=True,names=['year','value'],skiprows=[0])
nao_date = pd.DataFrame(data=nao.year.values,columns=['year'])
nao_date['month'] = (nao.year*0)+1
nao_date['day'] = (nao.year*0)+1
nao.date = pd.to_datetime(nao_date)

positive_years = nao_date.year.values[np.where(nao.value.values > 1.5)]
negative_years = nao_date.year.values[np.where(nao.value.values < -1.5)]

positive_years_common = set(positive_years) & set(years)
negative_years_common = set(negative_years) & set(years)

positive_index = np.in1d(years,list(positive_years_common))
negative_index = np.in1d(years,list(negative_years_common))

# ##############
# #messing around - just indexing by warm and cool years
# ##############
# #The 'sea ice years'
# #http://en.vedur.is/climatology/articles/nr/1213
#
# # x = s_cube_my_latlon.aggregated_by('year', iris.analysis.MEAN)
# # negative_years_common = x.coord('year')[np.where(x.data < np.mean(x.data))].points
# # positive_years_common = x.coord('year')[np.where(x.data > np.mean(x.data))].points
# #
# # positive_index = np.in1d(years,list(positive_years_common))
# # negative_index = np.in1d(years,list(negative_years_common))
#
# #or
#
# sea_ice_years = [1965,1966,1967,1968,1969,1970,1971,1979,1980,1981,1982,1983,1984,1985,1986]
# negative_index = np.isin(years,list(sea_ice_years))
# positive_index = np.isin(years,list(sea_ice_years),invert = True)

##############
#end messing around - just indexing by warm and cool years
##############

####
# climatologies from NAO positive and negative years
####

# s_cube_my_latlon = detrend_cube(s_cube_my_latlon)
# b_cube_my_latlon = detrend_cube(b_cube_my_latlon)
# chl_cube_my_latlon = detrend_cube(chl_cube_my_latlon)

#positive
s_cube_my_latlon_annual_climatology_nao_positive = s_cube_my_latlon[positive_index].aggregated_by('clim_day', iris.analysis.MEAN)
b_cube_my_latlon_annual_climatology_nao_positive = b_cube_my_latlon[positive_index].aggregated_by('clim_day', iris.analysis.MEAN)
chl_cube_my_latlon_annual_climatology_nao_positive = chl_cube_my_latlon[positive_index].aggregated_by('clim_day', iris.analysis.MEAN)
#negative
s_cube_my_latlon_annual_climatology_nao_negative = s_cube_my_latlon[negative_index].aggregated_by('clim_day', iris.analysis.MEAN)
b_cube_my_latlon_annual_climatology_nao_negative = b_cube_my_latlon[negative_index].aggregated_by('clim_day', iris.analysis.MEAN)
chl_cube_my_latlon_annual_climatology_nao_negative = chl_cube_my_latlon[negative_index].aggregated_by('clim_day', iris.analysis.MEAN)

# qplt.plot((s_cube_my_latlon_annual_climatology-b_cube_my_latlon_annual_climatology),'k',alpha=0.5,label='climatological surface/bottom T diff')
# qplt.plot((s_cube_my_latlon_annual_climatology_nao_positive-b_cube_my_latlon_annual_climatology_nao_positive),'r',alpha=0.5,label='NAO positive climatological surface/bottom T diff')
# qplt.plot((s_cube_my_latlon_annual_climatology_nao_negative-b_cube_my_latlon_annual_climatology_nao_negative),'b',alpha=0.5,label='NAO negative climatological surface/bottom T diff')

qplt.plot((s_cube_my_latlon_annual_climatology_nao_positive),'r--',alpha=0.5,label='NAO positive climatological surface T ')
qplt.plot((s_cube_my_latlon_annual_climatology_nao_negative),'b--',alpha=0.5,label='NAO negative climatological surface T ')

qplt.plot((b_cube_my_latlon_annual_climatology_nao_positive),'r',alpha=0.5,label='NAO positive climatological bottom T ')
qplt.plot((b_cube_my_latlon_annual_climatology_nao_negative),'b',alpha=0.5,label='NAO negative climatological bottom T ')

qplt.plot((chl_cube_my_latlon_annual_climatology),'g',label='climatological chl')
qplt.plot((chl_cube_my_latlon_annual_climatology_nao_positive),'c',label='NAO positive climatological chl')
qplt.plot((chl_cube_my_latlon_annual_climatology_nao_negative),'m',label='NAO negative climatological chl')

plt.legend(loc=1,fontsize='xx-small')
plt.xlabel('day of year')
plt.savefig('/home/ph290/Documents/figures/climatological_t_splot_by_NAO_phase_iceland.png')
plt.show()

##############################


spinup_years = 3
file1 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/surface_temperature_iceland_site.nc')
file2 = glob.glob('/data/NAS-ph290/ph290/s2p3_rv2.0_output/bottom_temperature_iceland_site.nc')
ts_cube = iris.load_cube(file1)[spinup_years::]
tb_cube = iris.load_cube(file2)[spinup_years::]


try:
    iris.coord_categorisation.add_season(ts_cube, 'time', name='season_name')
except:
    print 'already got'



try:
    iris.coord_categorisation.add_season(tb_cube, 'time', name='season_name')
except:
    print 'already got'


# try:
#     iris.coord_categorisation.add_month(ts_cube, 'time', name='month_name')
# except:
#     print 'already got'
#
#
#
# try:
#     iris.coord_categorisation.add_month(tb_cube, 'time', name='month_name')
# except:
#     print 'already got'


ts_cube = ts_cube.extract(iris.Constraint(season_name = 'jja'))
tb_cube = tb_cube.extract(iris.Constraint(season_name = 'jja'))
# ts_cube = ts_cube.extract(iris.Constraint(month_name = 'Dec'))
# tb_cube = tb_cube.extract(iris.Constraint(month_name = 'Dec'))


try:
    iris.coord_categorisation.add_year(ts_cube, 'time', name='year')
except:
    print 'already got'


ts_cube = ts_cube.aggregated_by('year', iris.analysis.MEAN)



try:
    iris.coord_categorisation.add_year(tb_cube, 'time', name='year')
except:
    print 'already got'


tb_cube = tb_cube.aggregated_by('year', iris.analysis.MEAN)



tb_cube = detrend_cube(tb_cube)
ts_cube = detrend_cube(ts_cube)


dates = return_datetime_from_cube(ts_cube)
dates = return_datetime_from_cube(tb_cube)

years = [tmp.year for tmp in dates]

direct = '/data/NAS-ph290/ph290/observations/'
nao_file = direct+'nao_station_djfm.txt'
# nao_file = direct+'nao_station_annual.txt'
nao = pd.read_csv(nao_file,delim_whitespace=True,names=['year','value'],skiprows=[0])
nao_date = pd.DataFrame(data=nao.year.values,columns=['year'])
nao_date['month'] = (nao.year*0)+1
nao_date['day'] = (nao.year*0)+1
nao.date = pd.to_datetime(nao_date)

positive_years = nao_date.year.values[np.where(nao.value.values > 1.5)]
negative_years = nao_date.year.values[np.where(nao.value.values < -1.5)]

positive_years_common = set(positive_years) & set(years)
negative_years_common = set(negative_years) & set(years)

positive_index = np.in1d(years,list(positive_years_common))
negative_index = np.in1d(years,list(negative_years_common))



###############
# plotting    #
###############

# diff_cube = ts_cube - tb_cube
diff_cube = tb_cube
diff_cube_positive = diff_cube[positive_index].collapsed('time',iris.analysis.MEAN)
diff_cube_negative = diff_cube[negative_index].collapsed('time',iris.analysis.MEAN)
cube = diff_cube_positive - diff_cube_negative

import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator

levels = MaxNLocator(nbins=50).tick_values(-2.0,2.0)
cmap = plt.get_cmap('seismic')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

land_10m = cfeature.NaturalEarthFeature('physical', 'land', '10m',
                                        edgecolor='face',
                                        facecolor=cfeature.COLORS['land'])

plt.close('all')
fig = plt.figure(figsize=(8, 8), dpi=100)
# ax = plt.axes(projection=ccrs.PlateCarree())
ax = plt.subplot2grid((2, 2), (0, 0),projection=ccrs.PlateCarree())

lons, lats, data = cube.coord('latitude').points,cube.coord('longitude').points,cube.data

#pannel 1
img1 = ax.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax.add_feature(land_10m, edgecolor='gray')
cbar.set_label('+ve minus -ve NAO\nbottom temp diff')

#pannel 2
diff_cube = ts_cube
diff_cube_positive = diff_cube[positive_index].collapsed('time',iris.analysis.MEAN)
diff_cube_negative = diff_cube[negative_index].collapsed('time',iris.analysis.MEAN)
cube = diff_cube_positive - diff_cube_negative
data = cube.data

ax = plt.subplot2grid((2, 2), (1, 0),projection=ccrs.PlateCarree())

img1 = ax.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax.add_feature(land_10m, edgecolor='gray')
cbar.set_label('+ve minus -ve NAO\nsurface temp diff')

#pannel 3
cmap = plt.get_cmap('hot')

levels = MaxNLocator(nbins=50).tick_values(0.0,7.0)
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)


data = (ts_cube.collapsed('time',iris.analysis.MEAN) -  tb_cube.collapsed('time',iris.analysis.MEAN)).data
ax2 = plt.subplot2grid((2, 2), (0, 1),projection=ccrs.PlateCarree())
img1 = ax2.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax2.add_feature(land_10m, edgecolor='gray')
cbar.set_label('mean surface bottom T\ndiff. black=unstratified')

#pannel 3

levels = MaxNLocator(nbins=50).tick_values(-2.0,10.0)
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

data = diff_cube_positive.data
data = (tb_cube.collapsed('time',iris.analysis.MEAN)).data
ax3 = plt.subplot2grid((2, 2), (1, 1),projection=ccrs.PlateCarree())
img1 = ax3.pcolormesh(lats, lons, data,
            transform=ccrs.PlateCarree(),
            cmap=cmap, norm=norm)
# img1 = ax.contour(lats, lons, depth.data,30,alpha=0.5)
cbar = plt.colorbar(img1)
plt.scatter(lon,lat,color='b',marker='*',s=100)
ax3.add_feature(land_10m, edgecolor='gray')
cbar.set_label('mean bottom water\ntemperature')

plt.savefig('/home/ph290/Documents/figures/iceland_nao_impacts.png')
plt.show(block = False)

"""
execfile('lowest_level_v7_paul.py')
"""

##################################
# eReefs Download and processing #
##################################

# import subprocess
# import datetime
# import pandas as pd
#
# date1 = '2015-07-15'
# date2 = '2015-07-15'
# mydates = pd.date_range(date1, date2).tolist()
#
# for date in mydates:
#     subprocess.call(['wget http://dapds00.nci.org.au/thredds/ncss/fx3/gbr1_2.0/gbr1_simple_'+date.strftime('%Y-%m-%d')+'.nc?var=temp&disableLLSubset=on&disableProjSubset=on&horizStride=1&time_start='+date.strftime('%Y-%m-%d')+'time_end='+(date+1).strftime('%Y-%m-%d')+'timeStride=1&vertCoord= /data/NAS-geo01/jm953/eReefs/'], shell=True)
#I suggest you modify the script so it just loops through all of the file names (look up glob as a way to read them in). The script
#then writes out a new file for each date, so you could then use cdo mergetime after that to create one file holding all
#of the bottom temperatures.

import pandas as pd
import iris
import subprocess
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import glob
import os
from iris.experimental.equalise_cubes import equalise_attributes

directory = '/data/NAS-geo01/jm953/eReefs/2015_eReefsRaw/'
file = 'gbr1_simple_2015-*'

inputfile = glob.glob(directory + file)

# loc_out = '/data/NAS-geo01/jm953/eReefs/2015_eReefsRaw/reformat_2015/'
loc_out = '/home/ph290/Downloads/'

#for x in input_file:
#    name, num = x.split('.')
#    num = str(num)
#    os.rename(x, [0]+'_bottom_temperature.nc')
#    print input_file


# Here I'm making new file names based on the original filename - play about with the bits of code here to see how they are working so that you understand them.

#Ignore
# It seems like we need to regrid the file onto a simple lat-lon grid before easily working with it
#resolution_fraction_of_degree = 3
#lats = 360 * resolution_fraction_of_degree
#lons = 180 * resolution_fraction_of_degree
#subprocess.call(['cdo  -P 6 remapbil,r'+str(lats)+'x'+str(lons)+' '+inputfile+' '+directory+output_filename1], shell=True)

#Read in the file:


#just for testing - just doing the fiat 10 files:
inputfile = inputfile[0:10]

curfile = inputfile[0]
# print curfile
cube = iris.load_cube(curfile)
# Take just the first depth level, so that the cube only had latitude and longitude informatoin (we can then add the bopttom water temperatuer data into that single level)
cube1 = cube.collapsed('time',iris.analysis.MEAN)

# Make a copy of the dataset
cube2 = cube[:,43,:,:].copy() # level 43 is the surface ocean (not 0 as is typical)
# Removing the dept dimension in above
# cube2 = cube2[:,43,:,:] # level 43 is the surface ocean (not 0 as is typical)
# Fill the new cube with whatever the value is to indicate missing data, so that we are starting from a blank slate
cube2.data = np.ma.masked_array(cube2.data)
cube2.data.fill_value = cube.data.fill_value
cube2.data.data[:]=cube.data.fill_value

#make a variable holding just the data component of teh cube - not the metadata
new_data = cube2.data

# extract the data from the cube into an array
data = cube1.data.data.copy()

#The data below teh bottom of the sea is masked out. We want to start by setting all of teh data to a value higher than any we
#would physically see in our dataset
data[cube1.data.mask] = cube1.data.fill_value

data = np.flip(data,0)
#find the index of the 1st occurance of the maximum value (the missing value is 1e+20, so will always the teh largest value) in
#every column of water
max_index = np.argmax(data, axis=0)
#we want the index of the level above the maximum value, so we take one off
max_index = max_index-1
#we don;t want to index levels with 0 -1 , so we set the -1 values to zero
low_values_flags = max_index < 0.0
max_index[low_values_flags] = 0.0

#this just allows us to use our 2D array of indexes to index the dataset
m,n = max_index.shape
I,J = np.ogrid[:m,:n]

for curfile in inputfile:
    print curfile
    output_filename = curfile.split('.')[0]+'_bottom_temperature.nc'
    output_filename = output_filename.split('/')[-1]

    cube = iris.load_cube(curfile)
    # Take just the first depth level, so that the cube only had latitude and longitude information (we can then add the bopttom water temperatuer data into that single level)
    cube1 = cube.collapsed('time',iris.analysis.MEAN)

    # Make a copy of the dataset
    cube2 = cube[:,43,:,:].copy() # level 43 is the surface ocean (not 0 as is typical)
    # Removing the dept dimension in above
    # Fill the new cube with whatever the value is to indicate missing data, so that we are starting from a blank slate
    cube2.data = np.ma.masked_array(cube2.data)
    cube2.data.fill_value = cube.data.fill_value
    cube2.data.data[:]=cube.data.fill_value

    #make a variable holding just the data component of teh cube - not the metadata
    new_data = cube2[0].data

    # extract the data from the cube into an array
    data = cube1.data.data.copy()
    data = np.flip(data,0)

    new_data = data[max_index, I, J]
    """
    plt.pcolormesh(max_index)
    plt.colorbar()
    plt.show()


    plt.figure(0)
    plt.pcolormesh(new_data,vmin=20,vmax=35)
    plt.colorbar()
    plt.show(block = False)

    # data = np.flip(data,0)
    plt.figure(1)
    plt.pcolormesh(data[:,500,:],vmin=20,vmax=35)
    plt.colorbar()
    plt.show(block = False)
    """
    new_data = np.expand_dims(np.ma.masked_array(new_data),axis=0)
    new_data = np.ma.masked_where(new_data == 0.0,new_data,copy=True)
    cube2.data =  new_data
    cube2.data = np.ma.masked_where(cube2.data == cube1.data.fill_value, cube2.data)
    # cube2.data.mask = cube[0].data.mask

    # Save the netcdf file
    iris.fileformats.netcdf.save(cube2, loc_out + output_filename)

"""
cube1 = iris.load_cube(loc_out+'gbr1_simple_2015-01-09_bottom_temperature.nc')[0]
cube2 = iris.load_cube(loc_out+'gbr1_simple_2015-06-17_bottom_temperature.nc')[0]
# cube = iris.load_cube('/data/NAS-geo01/jm953/eReefs/2015_eReefsRaw/gbr1_simple_2015-02-11.nc?var=temp')[0][43]
#An example of how to plot these files:
lats = cube1.coord('latitude').points
# lats[np.where(np.logical_not(np.isfinite(lats)))] = 0.0
lons = cube1.coord('longitude').points
# lons[np.where(np.logical_not(np.isfinite(lons)))] = 0.0

plotting_data = cube1.data - cube2.data

plt.close('all')
ax = plt.axes(projection=ccrs.Mollweide())
plot_data = ax.contourf(lons, lats, plotting_data,30,
            transform=ccrs.PlateCarree(),
            cmap='magma')
bar = plt.colorbar(plot_data, orientation='horizontal', extend='both')
ax.coastlines('10m')

plt.show()
"""

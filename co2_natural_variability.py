import iris
import iris.coord_categorisation
import iris.analysis
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import statsmodels.api as sm
import scipy
from scipy.stats import gaussian_kde




directory = '/data/NAS-ph290/ph290/cmip5/piControl_spco2/'
file = 'spco2_Omon_HadGEM2-ES_piControl_r1i1p1.nc'

cube = iris.load_cube(directory + file)

iris.coord_categorisation.add_year(cube, 'time', name='year')
cube = cube.aggregated_by('year', iris.analysis.MEAN)

cube_data = cube.data
cube_data_det = scipy.signal.detrend(cube_data, axis=0)
cube.data = cube_data_det

coord = cube.coord('time')
dt = coord.units.num2date(coord.points)
year = np.array([coord.units.num2date(value).year for value in coord.points])

lon_west = -75.0
lon_east = -7.5
lat_south = 0.0
lat_north = 60.0 

cube_region_tmp = cube.intersection(longitude=(lon_west, lon_east))
cube_region = cube_region_tmp.intersection(latitude=(lat_south, lat_north))


try:
    cube_region.coord('latitude').guess_bounds()
except:
    print 'cube already has latitude bounds'


try:
    cube_region.coord('longitude').guess_bounds()
except:
    print 'cube already has longitude bounds' 


grid_areas = iris.analysis.cartography.area_weights(cube_region)
area_avged_cube_region = cube_region.collapsed(['longitude', 'latitude'], iris.analysis.MEAN, weights=grid_areas)


segment_length = 23
length = np.size(year) - (segment_length + 1)



no_ensembles = 100
no_repeats = 500

stdevs = np.zeros([no_repeats,no_ensembles])
stdevs[:] = np.nan

for k in np.arange(no_repeats):
	for i in range(no_ensembles):
		print i
		trends = []
		for j in np.arange(i+3):
			start = np.random.random_integers(0, length)
			Y = area_avged_cube_region[start:start+segment_length].data
			X = np.arange(1,segment_length+1)
			X2 = sm.add_constant(X)
			model = sm.OLS(Y,X2)
			results = model.fit()
		# 		results.params
			trends = np.append(trends,results.params[1])
		# 		print np.size(trends)
		stdevs[k,i] = np.std(trends)






plt.plot(stdevs)




import numpy as np
x = np.zeros((no_repeats,no_ensembles),dtype=np.float64)
y = np.array(np.arange(no_ensembles),dtype=np.float64)
for i in range(x.shape[0]):
	x[i,:] = y


x = np.reshape(x,no_repeats*no_ensembles)
y = np.reshape(stdevs,no_repeats*no_ensembles)

# Calculate the point density
xy = np.vstack([x,y])
z = gaussian_kde(xy)(xy)

# Sort the points by density, so that the densest points are plotted last
idx = z.argsort()
x, y, z = x[idx], y[idx], z[idx]

fig, ax = plt.subplots()
ax.scatter(x, y, c=z, s=50, edgecolor='')
plt.xlabel('ensemble size')
plt.ylabel('standard deviation')
plt.savefig('/home/ph290/Documents/figures/alice_natural_variability.png')
# plt.show()



